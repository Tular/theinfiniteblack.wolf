      // HEALING SECTION
      this.ShowAlert("[g]START of the HEALING section!");
      TimeSpan timeSpan;
      int num1;
      timeSpan = DateTime.Now - this._lastRepair;  // moved to before healing statements. commented for selfheal, degrapple, friendheal.
      num1 = timeSpan.TotalMilliseconds > (double)this._autoHealTime ? 0 : 1;
      if (num1 != 0)
      {
        this.ShowAlert("[g]Timespan too short!");
        goto label_99;
      }
      if (this.AutoSelfHeal && this._myShip.Hull < this.SelfHealAmount && ((int) this._myShip.Metals > 0 && this._myShip.Hull < this._myShip.MaxHull))
      {
//        timeSpan = DateTime.Now - this._lastRepair;
//        num1 = timeSpan.TotalMilliseconds <= (double) this._autoHealTime ? 0 : 1; // switched 1 and 0 values. True was 1, now 0
        this._lastRepair = DateTime.Now;
        if (this._repairCooldown.IsFinished && this._myLoc.Status == SectorStatus.ENGAGED)
        {
          this._repairCooldown.Set(3000L);
          this.ShowAlert("[g]Requesting Full Repair...");
          this._net.Send(RequestRepairBlackDollar.Create());
        }
        else
          {
            this.DoRepair((PlayerCombatEntity) this._myShip);
          }
        goto label_99; // heal executed so jump past degrapple and friendheal.
      }
      this.ShowAlert("[g]Check on TECH Special!");
      if (this._myShip.Special.Class != SpecialClass.TECHNICIAN)
      {
        goto label_99; // No TECHNICIAN special equipped so jumping past healing section.
      }
      // DEGRAPPLE SECTION
      if (this.AutoDegrapple && (int)this._myShip.Metals > 0)
      {
        this.ShowAlert("[g]DEGRAPPLE Section!");
        IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>)(x => x.ShouldBeDegrappled(this._worldTimeMS)));
        if (source.Any<Ship>())
        {
          this.ShowAlert("[g]executing DeGrapple");
          this._lastRepair = DateTime.Now;
          this.DoRepair((PlayerCombatEntity)source.First<Ship>());
          //          goto label_53;   // LOOP removed to prevent hitting the nerf.
          goto label_99; // EXIT Healing.
        }
      }
      if (this.AutoFriendHeal && (int)this._myShip.Metals > 0)
      {
        this.ShowAlert("[g]FRIENDheal Section!");
        IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>)(x => x.Hull < this.FriendHealAmount && x.MaxHull - x.Hull > 0));
        if (source.Any<Ship>())
        {   
          // suppress on cluster
          if (this._local.Garrison != null)
          {
            if (!(this._local.Garrison.Relation == RelationType.FRIEND && _disableHealInCluster))
            {
              this.ShowAlert("no friendly cluster!");
              this.DoRepair((PlayerCombatEntity)source.OrderByDescending<Ship, int>((Func<Ship, int>)(x => x.MaxHull - x.Hull)).First<Ship>());
              this._lastRepair = DateTime.Now;
            }
          }
          else
          {
            this.DoRepair((PlayerCombatEntity)source.OrderByDescending<Ship, int>((Func<Ship, int>)(x => x.MaxHull - x.Hull)).First<Ship>());
            this._lastRepair = DateTime.Now;
          }
        }
      } // END OF HEALING SECTION
      label_99: // exit when 1 of the healing items has been executed. No need to check the others.
      this.ShowAlert("[g]END of HEALING Section!");