﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.IByteBuffer
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public interface IByteBuffer
  {
    int Position { get; }

    sbyte[] Data { get; }

    void Get(sbyte[] data);

    sbyte Get();

    short GetShort();

    int GetInt();

    float GetFloat();

    bool GetBool();

    string GetString();

    void Write(sbyte value);

    void Write(short value);

    void Write(int value);

    void Write(float value);

    void Write(bool value);

    void Write(string value);

    void Write(sbyte[] data);

    void Write(byte[] data);
  }
}
