﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.RNG
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;

namespace TheInfiniteBlack.Library
{
  public static class RNG
  {
    public static readonly Random R = new Random();

    public static sbyte RandomDirection()
    {
      return (sbyte) RNG.R.Next(8);
    }

    public static float d100()
    {
      return (float) (RNG.R.NextDouble() * 100.0);
    }

    public static float d100(float bonus)
    {
      return (float) (RNG.R.NextDouble() * (100.0 + (double) bonus));
    }

    public static float dX(float value)
    {
      return (float) RNG.R.NextDouble() * value;
    }
  }
}
