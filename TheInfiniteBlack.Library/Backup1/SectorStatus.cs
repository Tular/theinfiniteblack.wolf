﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.SectorStatus
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum SectorStatus
  {
    NULL = -128,
    NORMAL = 0,
    PRE_ENGAGE = 1,
    ENGAGED = 2,
    POST_ENGAGE = 3,
    PIRATE = 4,
    WYRD = 5,
    HETEROCLITE = 6,
  }
}
