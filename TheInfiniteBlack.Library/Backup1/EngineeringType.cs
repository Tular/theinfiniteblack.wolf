﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.EngineeringType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum EngineeringType
  {
    NULL = -128,
    DECONSTRUCT = 0,
    REPAIR = 1,
    RANK_UP = 2,
    RANK_DOWN = 3,
    CLASS_UP = 4,
    CLASS_DOWN = 5,
    RARITY = 6,
    UNBIND = 7,
  }
}
