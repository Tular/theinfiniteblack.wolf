﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.ChatType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum ChatType
  {
    NULL = -128,
    SERVER = 0,
    CORP = 1,
    SECTOR = 2,
    PRIVATE = 3,
    ALLIANCE = 4,
    CONSOLE = 5,
    EVENT = 6,
    ALERT = 7,
    MARKET = 8,
    MARKET_EVENT = 9,
    UNIVERSE = 10,
  }
}
