﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.RequestBuffer
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Net.Sockets;
using TheInfiniteBlack.Library.Network;

namespace TheInfiniteBlack.Library
{
  public class RequestBuffer : ByteBuffer, INetworkData, IByteBuffer
  {
    private readonly sbyte _type;
    private byte[] _bytes;

    public RequestBuffer(sbyte type, int capacity)
      : base(new sbyte[capacity + 3])
    {
      this._type = type;
      this._index = 3;
    }

    public void Set(SocketAsyncEventArgs args)
    {
      if (this._bytes == null)
      {
        int num = this._index - 2;
        this._data[0] = (sbyte) (num >> 8);
        this._data[1] = (sbyte) num;
        this._data[2] = this._type;
        this._bytes = new byte[this._index];
        Buffer.BlockCopy((Array) this._data, 0, (Array) this._bytes, 0, this._index);
      }
      args.SetBuffer(this._bytes, 0, this._bytes.Length);
    }
  }
}
