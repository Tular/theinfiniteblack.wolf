﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.TechnologyType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum TechnologyType
  {
    AdvancedDefense,
    Scanners,
    DefenseTactics,
    Marauder,
    Defender,
    Scavenger,
    Analysis,
    Critical,
    Splash,
    Evasion,
    ToHit,
    AdvancedWeapons,
    AdvancedHulls,
    AdvancedStorage,
    AdvancedEngines,
    AdvancedDrones,
    Nanorobotics,
  }
}
