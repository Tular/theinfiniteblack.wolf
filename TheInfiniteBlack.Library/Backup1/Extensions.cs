﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Extensions
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public static class Extensions
  {
    public static string Name(this TechnologyType type)
    {
      switch (type)
      {
        case TechnologyType.AdvancedDefense:
          return "Advanced Defense";
        case TechnologyType.Scanners:
          return "Scanners";
        case TechnologyType.DefenseTactics:
          return "Defense Tactics";
        case TechnologyType.Marauder:
          return "Marauder";
        case TechnologyType.Defender:
          return "Defender";
        case TechnologyType.Scavenger:
          return "Scavenger";
        case TechnologyType.Analysis:
          return "Analysis";
        case TechnologyType.Critical:
          return "Critical";
        case TechnologyType.Splash:
          return "Splash";
        case TechnologyType.Evasion:
          return "Evasion";
        case TechnologyType.ToHit:
          return "ToHit";
        case TechnologyType.AdvancedWeapons:
          return "Advanced Weapons";
        case TechnologyType.AdvancedHulls:
          return "Advanced Hulls";
        case TechnologyType.AdvancedStorage:
          return "Advanced Storage";
        case TechnologyType.AdvancedEngines:
          return "Advanced Engines";
        case TechnologyType.AdvancedDrones:
          return "Advanced Drones";
        case TechnologyType.Nanorobotics:
          return "Nanorobotics";
        default:
          return "?ERROR";
      }
    }

    public static string Description(this TechnologyType type)
    {
      switch (type)
      {
        case TechnologyType.AdvancedDefense:
          return "Defensive engagements spawn an additional Defense Platform";
        case TechnologyType.Scanners:
          return "See all ships in sectors near garrison";
        case TechnologyType.DefenseTactics:
          return "Defensive engagements are 20 seconds faster";
        case TechnologyType.Marauder:
          return "+10% ship damage VS defensive units";
        case TechnologyType.Defender:
          return "+10% ship damage VS engagement attackers";
        case TechnologyType.Scavenger:
          return "Increased resource and loot drop rates";
        case TechnologyType.Analysis:
          return "+10% XP bonus";
        case TechnologyType.Critical:
          return "+5 ship's critical chance";
        case TechnologyType.Splash:
          return "+5 ship's splash chance";
        case TechnologyType.Evasion:
          return "+5 ship's evasion chance";
        case TechnologyType.ToHit:
          return "+8 ship's hit chance";
        case TechnologyType.AdvancedWeapons:
          return "+1 ship's weapon rank";
        case TechnologyType.AdvancedHulls:
          return "+310 ship's hull maximum";
        case TechnologyType.AdvancedStorage:
          return "+10 ship's storage maximum";
        case TechnologyType.AdvancedEngines:
          return "-1 second ship's movement cooldown";
        case TechnologyType.AdvancedDrones:
          return "+100% hull, damage, and healing for Fighters and Drones";
        case TechnologyType.Nanorobotics:
          return "Equipped items repair 1% durability every 24 hours";
        default:
          return "?ERROR";
      }
    }

    public static string Name(this EngineeringType type)
    {
      switch (type)
      {
        case EngineeringType.DECONSTRUCT:
          return "Deconstruct";
        case EngineeringType.REPAIR:
          return "Repair";
        case EngineeringType.RANK_UP:
          return "Rank Up";
        case EngineeringType.RANK_DOWN:
          return "Rank Down";
        case EngineeringType.CLASS_UP:
          return "Ship Class Up";
        case EngineeringType.CLASS_DOWN:
          return "Ship Class Down";
        case EngineeringType.RARITY:
          return "Upgrade Rarity";
        case EngineeringType.UNBIND:
          return "Unbind No Drop";
        default:
          return "ERROR";
      }
    }

    public static string Name(this CorpRankType rank)
    {
      switch (rank)
      {
        case CorpRankType.RECRUIT:
          return "Recruit";
        case CorpRankType.MEMBER:
          return "Member";
        case CorpRankType.OFFICER:
          return "Officer";
        case CorpRankType.SUPEROFFICER:
          return "SuperOfficer";
        case CorpRankType.LEADER:
          return "Leader";
        default:
          return "Unknown?";
      }
    }

    public static bool CanInvite(this CorpRankType rank)
    {
      return rank >= CorpRankType.OFFICER;
    }

    public static bool CanKick(this CorpRankType rank)
    {
      return rank >= CorpRankType.OFFICER;
    }

    public static bool CanDemote(this CorpRankType rank)
    {
      return rank >= CorpRankType.OFFICER;
    }

    public static bool CanPromote(this CorpRankType rank)
    {
      return rank >= CorpRankType.OFFICER;
    }

    public static bool CanWithdraw(this CorpRankType rank)
    {
      return rank >= CorpRankType.SUPEROFFICER;
    }

    public static string Name(this Direction dir)
    {
      switch (dir)
      {
        case Direction.NW:
          return "northwest";
        case Direction.N:
          return "north";
        case Direction.NE:
          return "northeast";
        case Direction.E:
          return "east";
        case Direction.SE:
          return "southeast";
        case Direction.S:
          return "south";
        case Direction.SW:
          return "southwest";
        case Direction.W:
          return "west";
        default:
          return "err";
      }
    }

    public static Direction Inverse(Direction dir)
    {
      switch (dir)
      {
        case Direction.NW:
          return Direction.SE;
        case Direction.N:
          return Direction.S;
        case Direction.NE:
          return Direction.SW;
        case Direction.E:
          return Direction.W;
        case Direction.SE:
          return Direction.NW;
        case Direction.S:
          return Direction.N;
        case Direction.SW:
          return Direction.NE;
        case Direction.W:
          return Direction.E;
        default:
          return Direction.None;
      }
    }

    public static string GetMarkup(this ChatType type)
    {
      switch (type)
      {
        case ChatType.CORP:
          return "[b]";
        case ChatType.SECTOR:
          return "[gr]";
        case ChatType.PRIVATE:
          return "[v]";
        case ChatType.ALLIANCE:
          return "[o]";
        case ChatType.CONSOLE:
          return "[y]";
        case ChatType.MARKET:
        case ChatType.MARKET_EVENT:
          return "[lg]";
        default:
          return string.Empty;
      }
    }

    public static bool CanClientSend(this ChatType type)
    {
      switch (type)
      {
        case ChatType.SERVER:
        case ChatType.CORP:
        case ChatType.SECTOR:
        case ChatType.PRIVATE:
        case ChatType.ALLIANCE:
        case ChatType.MARKET:
        case ChatType.UNIVERSE:
          return true;
        default:
          return false;
      }
    }

    public static string Description(this NoticeType type)
    {
      switch (type)
      {
        case NoticeType.LoginInvalid:
          return "Login Invalid";
        case NoticeType.NameUnavailable:
          return "Name Unavailable";
        case NoticeType.PlayerNotFound:
          return "Player Not Found";
        case NoticeType.ExploreBonus:
          return "Exploration Bonus!";
        case NoticeType.RequestFailed:
          return "Request Failed";
        case NoticeType.TradeFailCancel:
          return "Trade Failed - Canceled";
        case NoticeType.Banned:
          return "Error! Email Contact@Spellbook.com";
        case NoticeType.AccountCreateSuccess:
          return "Account Created! Please Log In!";
        default:
          return "NOTICE ERROR";
      }
    }

    public static string GetMarkup(this RelationType type)
    {
      switch (type)
      {
        case RelationType.NEUTRAL:
          return "[y]";
        case RelationType.SELF:
          return "[lg]";
        case RelationType.FRIEND:
          return "[lb]";
        case RelationType.ENEMY:
          return "[r]";
        default:
          return "[w]";
      }
    }

    public static string Name(this ResourceType type)
    {
      switch (type)
      {
        case ResourceType.ORGANIC:
          return "Organics";
        case ResourceType.GAS:
          return "Gas";
        case ResourceType.METAL:
          return "Metal";
        case ResourceType.RADIOACTIVE:
          return "Radioactive";
        case ResourceType.DARKMATTER:
          return "DarkMatter";
        default:
          return "?";
      }
    }

    public static int BuyCost(this ResourceType type, int count)
    {
      return type.SellValue(count) * 2;
    }

    public static int SellValue(this ResourceType type, int count)
    {
      switch (type)
      {
        case ResourceType.ORGANIC:
          return 25 * count;
        case ResourceType.GAS:
          return 50 * count;
        case ResourceType.METAL:
          return 75 * count;
        case ResourceType.RADIOACTIVE:
          return 100 * count;
        case ResourceType.DARKMATTER:
          return 150 * count;
        default:
          return 0;
      }
    }

    public static int ResearchUnitValue(this ResourceType type, int count)
    {
      switch (type)
      {
        case ResourceType.ORGANIC:
          return count;
        case ResourceType.GAS:
          return count * 2;
        case ResourceType.METAL:
          return count * 3;
        case ResourceType.RADIOACTIVE:
          return count * 4;
        case ResourceType.DARKMATTER:
          return count * 6;
        default:
          return 0;
      }
    }

    public static bool HasStatusTimer(this SectorStatus status)
    {
      switch (status)
      {
        case SectorStatus.PRE_ENGAGE:
        case SectorStatus.ENGAGED:
        case SectorStatus.POST_ENGAGE:
          return true;
        default:
          return false;
      }
    }
  }
}
