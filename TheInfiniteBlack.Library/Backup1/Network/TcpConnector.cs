﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.TcpConnector
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Net;
using System.Net.Sockets;

namespace TheInfiniteBlack.Library.Network
{
  public class TcpConnector
  {
    private readonly object _mutex = new object();
    private int _lastStartTimeMS = int.MinValue;
    private readonly string _hostname;
    private readonly int _port;
    private readonly int _timeoutMS;
    private Socket _socket;

    public TcpConnector(string hostname, int port, int timeoutMS = 5000)
    {
      this._hostname = hostname;
      this._port = port;
      this._timeoutMS = timeoutMS;
    }

    public void Disconnect()
    {
      lock (this._mutex)
      {
        this._lastStartTimeMS = int.MinValue;
        SocketHelper.Close(this._socket);
        this._socket = (Socket) null;
      }
    }

    public Socket TryConnect()
    {
      lock (this._mutex)
      {
        if (this._socket == null)
        {
          this.Connect();
        }
        else
        {
          if (this._socket.Connected)
          {
            Socket socket = this._socket;
            this._socket = (Socket) null;
            return socket;
          }
          if (Environment.TickCount - this._lastStartTimeMS >= this._timeoutMS)
            this.Disconnect();
        }
      }
      return (Socket) null;
    }

    private void Connect()
    {
      try
      {
        this._lastStartTimeMS = Environment.TickCount;
        SocketAsyncEventArgs e = new SocketAsyncEventArgs();
        this._socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        {
          NoDelay = true
        };
        IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(this._hostname), this._port);
        e.RemoteEndPoint = (EndPoint) ipEndPoint;
        this._socket.ConnectAsync(e);
      }
      catch (Exception ex)
      {
        Log.E((object) this, nameof (Connect), ex);
        this.Disconnect();
      }
    }
  }
}
