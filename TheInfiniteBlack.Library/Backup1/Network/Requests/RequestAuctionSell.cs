﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestAuctionSell
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestAuctionSell
  {
    public static INetworkData Create(EquipmentItem item, int bid, int buyout)
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) 47, 32);
      item.Write((IByteBuffer) networkData);
      networkData.Write(bid);
      networkData.Write(buyout);
      return networkData;
    }
  }
}
