﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestDevelopResources
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestDevelopResources
  {
    public static INetworkData Create(int targetId, ResourceType resource)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 3, 5);
      int num1 = targetId;
      requestBuffer.Write(num1);
      int num2 = (int) (sbyte) resource;
      requestBuffer.Write((sbyte) num2);
      return (INetworkData) requestBuffer;
    }
  }
}
