﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestItemEngineer
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestItemEngineer
  {
    public static INetworkData Create(EquipmentItem item, EngineeringType type)
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) 31, 20);
      item.Write((IByteBuffer) networkData);
      networkData.Write((sbyte) type);
      return networkData;
    }
  }
}
