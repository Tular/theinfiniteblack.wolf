﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestBuyShip
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestBuyShip
  {
    public static INetworkData Create(ShipClass ship, bool useBlackDollars)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -46, 2);
      int num1 = (int) (sbyte) ship;
      requestBuffer.Write((sbyte) num1);
      int num2 = useBlackDollars ? 1 : 0;
      requestBuffer.Write(num2 != 0);
      return (INetworkData) requestBuffer;
    }
  }
}
