﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestSetAccountVariables
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestSetAccountVariables
  {
    public static INetworkData Create(sbyte shipSkin, sbyte reserved2, sbyte reserved3, sbyte reserved4, sbyte reserved5, sbyte reserved6, sbyte reserved7, sbyte reserved8)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 32, 64);
      int num1 = (int) shipSkin;
      requestBuffer.Write((sbyte) num1);
      int num2 = (int) reserved2;
      requestBuffer.Write((sbyte) num2);
      int num3 = (int) reserved3;
      requestBuffer.Write((sbyte) num3);
      int num4 = (int) reserved4;
      requestBuffer.Write((sbyte) num4);
      int num5 = (int) reserved5;
      requestBuffer.Write((sbyte) num5);
      int num6 = (int) reserved6;
      requestBuffer.Write((sbyte) num6);
      int num7 = (int) reserved7;
      requestBuffer.Write((sbyte) num7);
      int num8 = (int) reserved8;
      requestBuffer.Write((sbyte) num8);
      return (INetworkData) requestBuffer;
    }
  }
}
