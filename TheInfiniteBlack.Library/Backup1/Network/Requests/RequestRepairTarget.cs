﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestRepairTarget
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestRepairTarget
  {
    public static INetworkData Create(CombatEntity target)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -51, 4);
      int id = target.ID;
      requestBuffer.Write(id);
      return (INetworkData) requestBuffer;
    }
  }
}
