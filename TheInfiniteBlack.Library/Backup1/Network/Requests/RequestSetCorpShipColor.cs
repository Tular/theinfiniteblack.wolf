﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestSetCorpShipColor
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestSetCorpShipColor
  {
    public static INetworkData Create(sbyte value)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 33, 1);
      int num = (int) value;
      requestBuffer.Write((sbyte) num);
      return (INetworkData) requestBuffer;
    }
  }
}
