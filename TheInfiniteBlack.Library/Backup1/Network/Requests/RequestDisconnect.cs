﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestDisconnect
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestDisconnect
  {
    public static INetworkData Create(bool defendSector)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -126, 1);
      int num = defendSector ? 1 : 0;
      requestBuffer.Write(num != 0);
      return (INetworkData) requestBuffer;
    }
  }
}
