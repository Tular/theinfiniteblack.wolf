﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestItemBuy
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestItemBuy
  {
    public static INetworkData Create(EquipmentItem item, StarPort starport, bool useBlackDollars)
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) 29, 20);
      item.Write((IByteBuffer) networkData);
      networkData.Write(starport.ID);
      networkData.Write(useBlackDollars);
      return networkData;
    }
  }
}
