﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestItemSell
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestItemSell
  {
    public static INetworkData Create(EquipmentItem item, StarPort starport)
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) 30, 20);
      item.Write((IByteBuffer) networkData);
      networkData.Write(starport == null ? int.MinValue : starport.ID);
      return networkData;
    }
  }
}
