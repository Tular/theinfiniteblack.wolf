﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.SocketHelper
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Net.Sockets;

namespace TheInfiniteBlack.Library.Network
{
  public static class SocketHelper
  {
    private static readonly object _mutex = new object();
    private static string _lastIp;
    private static int _lastPort;
    private static TcpConnector _connector;

    public static void Close(Socket socket)
    {
      try
      {
        if (socket != null)
          socket.Shutdown(SocketShutdown.Both);
      }
      catch
      {
      }
      try
      {
        if (socket == null)
          return;
        socket.Close();
      }
      catch
      {
      }
    }

    public static Socket Connect(string ip, int port, int timeoutMS = 5000)
    {
      lock (SocketHelper._mutex)
      {
        if (SocketHelper._lastIp != ip || port != SocketHelper._lastPort || SocketHelper._connector == null)
        {
          SocketHelper._lastIp = ip;
          SocketHelper._lastPort = port;
          if (SocketHelper._connector != null)
            SocketHelper._connector.Disconnect();
          SocketHelper._connector = new TcpConnector(ip, port, timeoutMS);
        }
        return SocketHelper._connector.TryConnect();
      }
    }
  }
}
