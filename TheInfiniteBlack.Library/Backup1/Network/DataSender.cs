﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.DataSender
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace TheInfiniteBlack.Library.Network
{
  public class DataSender
  {
    private readonly Queue<INetworkData> _send = new Queue<INetworkData>();
    private readonly Socket _socket;
    private SocketAsyncEventArgs _sendArgs;

    public DataSender(Socket socket)
    {
      if (socket == null)
        throw new ArgumentNullException(nameof (socket));
      this._socket = socket;
      this._sendArgs = new SocketAsyncEventArgs();
      this._sendArgs.Completed += new EventHandler<SocketAsyncEventArgs>(this.OnSend);
    }

    public void Disconnect(string reason)
    {
      SocketHelper.Close(this._socket);
      SocketAsyncEventArgs sendArgs = this._sendArgs;
      this._sendArgs = (SocketAsyncEventArgs) null;
      if (sendArgs != null)
        sendArgs.Dispose();
      Log.I((object) this, "Close", "Disconnected (" + reason + ")");
    }

    public bool Send(INetworkData data)
    {
      if (!this._socket.Connected)
        return false;
      SocketAsyncEventArgs socketAsyncEventArgs = (SocketAsyncEventArgs) null;
      try
      {
        lock (this._send)
        {
          socketAsyncEventArgs = this._sendArgs;
          if (socketAsyncEventArgs != null)
            this._sendArgs = (SocketAsyncEventArgs) null;
          else
            this._send.Enqueue(data);
        }
        if (socketAsyncEventArgs != null)
        {
          data.Set(socketAsyncEventArgs);
          if (!this._socket.SendAsync(socketAsyncEventArgs))
            this.OnSend((object) null, socketAsyncEventArgs);
        }
      }
      catch (Exception ex)
      {
        Log.E((object) this, nameof (Send), ex);
        this.Disconnect("Error: " + (object) ex);
        if (socketAsyncEventArgs != null)
          socketAsyncEventArgs.Dispose();
      }
      return true;
    }

    private void OnSend(object sender, SocketAsyncEventArgs args)
    {
      try
      {
        if (Log.Debug)
          Log.D((object) this, nameof (OnSend), ((int) args.SocketError).ToString() + " (" + (object) args.BytesTransferred + " bytes)");
        if (this._socket.Connected && args.SocketError == SocketError.Success)
        {
          INetworkData networkData;
          lock (this._send)
            networkData = this._send.Count > 0 ? this._send.Dequeue() : (INetworkData) null;
          if (networkData != null)
          {
            networkData.Set(args);
            if (this._socket.SendAsync(args))
              return;
            this.OnSend((object) null, args);
          }
          else
            this._sendArgs = args;
        }
        else
        {
          this.Disconnect("Data Send Failed");
          args.Dispose();
        }
      }
      catch (ObjectDisposedException ex)
      {
      }
      catch (Exception ex)
      {
        Log.E((object) this, nameof (OnSend), ex);
        this.Disconnect("Error: " + (object) ex);
        args.Dispose();
      }
    }
  }
}
