﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowAttackCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowAttackCommand : Command
  {
    private readonly int _sourceId;
    private readonly int _targetID;
    private readonly int _damage;
    private readonly AttackEventType _type;

    protected internal ShowAttackCommand(IByteBuffer input)
    {
      this._sourceId = input.GetInt();
      this._targetID = input.GetInt();
      this._damage = (int) input.GetShort();
      this._type = (AttackEventType) input.Get();
    }

    public override void Execute(GameState state)
    {
      Entity source = state.Entities.Get<Entity>(this._sourceId);
      Entity target1 = state.Entities.Get<Entity>(this._targetID);
      CombatEntity target2 = target1 as CombatEntity;
      switch (this._type)
      {
        case AttackEventType.MISS:
          if (source == state.MyShip)
          {
            state.SetLastAttackTime();
            break;
          }
          break;
        case AttackEventType.GRAZE:
        case AttackEventType.HIT:
        case AttackEventType.CRITICAL:
        case AttackEventType.SPLASH:
          if (source == state.MyShip)
            state.SetLastAttackTime();
          if (target2 != null)
          {
            target2.Hull -= this._damage;
            break;
          }
          break;
        case AttackEventType.REPAIR:
          if (target2 != null)
          {
            target2.Hull += this._damage;
            break;
          }
          break;
        case AttackEventType.STUN:
          if (target2 != null)
          {
            target2.SetStunned(this._damage);
            break;
          }
          break;
        case AttackEventType.COLLIDE:
          CombatEntity combatEntity = source as CombatEntity;
          if (combatEntity != null)
            combatEntity.Hull -= this._damage;
          if (target2 != null)
          {
            target2.Hull -= this._damage;
            break;
          }
          break;
        case AttackEventType.DEFLECT:
        case AttackEventType.RAM:
          if (target2 != null)
          {
            target2.Hull -= this._damage;
            break;
          }
          break;
        case AttackEventType.GRAPPLE:
          if (target2 != null)
          {
            target2.Grappled = true;
            break;
          }
          break;
        case AttackEventType.DEGRAPPLE:
          if (target2 != null)
          {
            target2.Grappled = false;
            break;
          }
          break;
      }
      switch (this._type)
      {
        case AttackEventType.GRAZE:
        case AttackEventType.HIT:
        case AttackEventType.CRITICAL:
        case AttackEventType.COLLIDE:
        case AttackEventType.DEFLECT:
        case AttackEventType.RAM:
          ShowAttackCommand.DoDamagedSound(state, target2);
          break;
      }
      state.UI.Show(new AttackEventArgs((IGameState) state, source, target1, this._damage, this._type));
      if (target1 != state.MyShip || !state.Accounts.System.CombatFlash)
        return;
      state.UI.Show(new CombatFlashEventArgs((IGameState) state, this._type));
    }

    private static void DoDamagedSound(GameState state, CombatEntity target)
    {
      if (target == null || state.MySettings == null || (double) state.Accounts.System.CombatSound <= 1.0000000116861E-07)
        return;
      EventSoundEffect damagedSound = SoundHelper.GetDamagedSound((Entity) target);
      if (damagedSound == null)
        return;
      state.UI.Show(new SoundEventArgs((IGameState) state, damagedSound));
    }
  }
}
