﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerLoad
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerLoad : Command
  {
    private readonly int _myPlayerId;
    private readonly int _myShipId;

    protected internal UpdatePlayerLoad(IByteBuffer input)
    {
      this._myPlayerId = input.GetInt();
      this._myShipId = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      ClientPlayer myPlayer = state.Players.Get(this._myPlayerId);
      Ship myShip = state.Entities.Get<Ship>(this._myShipId);
      if (myShip == null)
      {
        myShip = new Ship((IGameState) state, this._myShipId);
        state.Entities.Add((Entity) myShip);
      }
      state.OnLogin(myPlayer, myShip);
    }
  }
}
