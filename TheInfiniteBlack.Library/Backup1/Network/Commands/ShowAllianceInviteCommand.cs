﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowAllianceInviteCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowAllianceInviteCommand : Command
  {
    private readonly short _allianceId;

    protected internal ShowAllianceInviteCommand(IByteBuffer input)
    {
      this._allianceId = input.GetShort();
    }

    public override void Execute(GameState state)
    {
      INetworkData networkData = RequestAllianceRejectInvite.Create();
      if (state.MySettings != null && state.MySettings.Social.RejectInvites)
      {
        state.Net.Send(networkData);
        state.ShowEvent("[y]Alliance Invite Auto-Rejected");
      }
      else
      {
        ClientAlliance clientAlliance = state.Alliances.Get(this._allianceId);
        INetworkData accept = RequestAllianceAcceptInvite.Create();
        StringBuilder stringBuilder = new StringBuilder(100);
        StringBuilder sb = stringBuilder;
        int num = 0;
        clientAlliance.AppendName(sb, num != 0);
        stringBuilder.Append(" invites your corporation to their Alliance!");
        state.ShowAcceptDecline("Alliance - Invite", stringBuilder.ToString(), accept, networkData);
      }
    }
  }
}
