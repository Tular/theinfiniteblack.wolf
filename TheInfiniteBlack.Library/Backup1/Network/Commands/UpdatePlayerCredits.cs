﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerCredits
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerCredits : Command
  {
    private readonly int _credits;
    private readonly int _blackDollars;
    private readonly float _rewardPoints;
    private readonly int _combatPoints;

    protected internal UpdatePlayerCredits(IByteBuffer input)
    {
      this._credits = input.GetInt();
      this._blackDollars = input.GetInt();
      this._rewardPoints = input.GetFloat();
      this._combatPoints = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      ClientBank bank = state.Bank;
      int credits = this._credits;
      bank.Credits = credits;
      int blackDollars = this._blackDollars;
      bank.BlackDollars = blackDollars;
      double rewardPoints = (double) this._rewardPoints;
      bank.RewardPoints = (float) rewardPoints;
      int combatPoints = this._combatPoints;
      bank.CombatPoints = combatPoints;
    }
  }
}
