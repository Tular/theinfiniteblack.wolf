﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerBuyables
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerBuyables : Command
  {
    private readonly short _hyperDrive;
    private readonly short _superCharge;
    private readonly short _tactics;
    private readonly short _mindSurge;
    private readonly short _accelerate;
    private readonly sbyte _bankCap;
    private readonly sbyte _inventoryCap;
    private readonly bool _suspendHyperDrive;
    private readonly bool _suspendSuperCharge;
    private readonly bool _suspendTactics;
    private readonly bool _suspendMindSurge;

    protected internal UpdatePlayerBuyables(IByteBuffer input)
    {
      this._hyperDrive = input.GetShort();
      this._superCharge = input.GetShort();
      this._tactics = input.GetShort();
      this._mindSurge = input.GetShort();
      this._accelerate = input.GetShort();
      this._bankCap = input.Get();
      this._inventoryCap = input.Get();
      this._suspendHyperDrive = input.GetBool();
      this._suspendSuperCharge = input.GetBool();
      this._suspendTactics = input.GetBool();
      this._suspendMindSurge = input.GetBool();
    }

    public override void Execute(GameState state)
    {
      ClientBuyables buyables = state.Buyables;
      int hyperDrive = (int) this._hyperDrive;
      buyables.HyperDrive = hyperDrive;
      int superCharge = (int) this._superCharge;
      buyables.SuperCharge = superCharge;
      int tactics = (int) this._tactics;
      buyables.Tactics = tactics;
      int mindSurge = (int) this._mindSurge;
      buyables.MindSurge = mindSurge;
      int accelerate = (int) this._accelerate;
      buyables.Accelerate = accelerate;
      int bankCap = (int) this._bankCap;
      buyables.BankCap = bankCap;
      int inventoryCap = (int) this._inventoryCap;
      buyables.InventoryCap = inventoryCap;
      int num1 = this._suspendHyperDrive ? 1 : 0;
      buyables.SetHyperDrive(num1 != 0);
      int num2 = this._suspendSuperCharge ? 1 : 0;
      buyables.SetSuperCharge(num2 != 0);
      int num3 = this._suspendTactics ? 1 : 0;
      buyables.SetTactics(num3 != 0);
      int num4 = this._suspendMindSurge ? 1 : 0;
      buyables.SetMindSurge(num4 != 0);
    }
  }
}
