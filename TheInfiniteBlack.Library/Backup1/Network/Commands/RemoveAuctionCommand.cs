﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.RemoveAuctionCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class RemoveAuctionCommand : Command
  {
    private readonly int _id;

    protected internal RemoveAuctionCommand(IByteBuffer input)
    {
      this._id = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      if (state.MySettings != null)
        state.MySettings.AuctionWatch.Remove(this._id, state.Login.ServerID);
      ClientAuction auction = state.Auctions.Remove(this._id);
      if (auction == null)
        return;
      state.UI.Show(new AuctionRemovedEventArgs((IGameState) state, auction));
    }
  }
}
