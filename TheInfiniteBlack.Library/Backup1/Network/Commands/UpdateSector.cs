﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdateSector
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdateSector : Command
  {
    private readonly bool _rift;
    private readonly sbyte _x;
    private readonly sbyte _y;
    private readonly sbyte _linkConfig;
    private readonly bool _starPort;
    private readonly int _statusTimeMS;
    private readonly SectorStatus _status;
    private readonly AsteroidClass _asteroid;
    private readonly bool _capturePoint;
    private readonly short _capturePointOwner;

    protected internal UpdateSector(IByteBuffer input)
    {
      this._x = input.Get();
      this._y = input.Get();
      this._linkConfig = input.Get();
      sbyte num = input.Get();
      sbyte minValue = input.Get();
      if ((int) num >= 100)
      {
        this._rift = true;
        num -= (sbyte) 100;
      }
      else
        this._rift = false;
      if ((int) minValue < 0)
      {
        if ((int) minValue != (int) sbyte.MinValue)
        {
          minValue = sbyte.MinValue;
          this._starPort = true;
        }
        else
          this._starPort = false;
      }
      else if ((int) minValue >= 100)
      {
        this._starPort = true;
        minValue -= (sbyte) 100;
      }
      else
        this._starPort = false;
      this._status = (SectorStatus) num;
      this._asteroid = (AsteroidClass) minValue;
      this._statusTimeMS = this._status.HasStatusTimer() ? input.GetInt() : 0;
      this._capturePoint = (int) input.Get() == (int) sbyte.MaxValue;
      this._capturePointOwner = this._capturePoint ? input.GetShort() : short.MinValue;
    }

    public override void Execute(GameState state)
    {
      Sector sector1 = state.Map.Get(this._rift, (int) this._x, (int) this._y);
      bool[] flagArray = SectorLink.Get(this._linkConfig);
      Sector sector2 = flagArray[0] ? state.Map.Get(this._rift, (int) this._x - 1, (int) this._y - 1) : (Sector) null;
      sector1.NW = sector2;
      Sector sector3 = flagArray[1] ? state.Map.Get(this._rift, (int) this._x, (int) this._y - 1) : (Sector) null;
      sector1.N = sector3;
      Sector sector4 = flagArray[2] ? state.Map.Get(this._rift, (int) this._x + 1, (int) this._y - 1) : (Sector) null;
      sector1.NE = sector4;
      Sector sector5 = flagArray[3] ? state.Map.Get(this._rift, (int) this._x + 1, (int) this._y) : (Sector) null;
      sector1.E = sector5;
      Sector sector6 = flagArray[4] ? state.Map.Get(this._rift, (int) this._x + 1, (int) this._y + 1) : (Sector) null;
      sector1.SE = sector6;
      Sector sector7 = flagArray[5] ? state.Map.Get(this._rift, (int) this._x, (int) this._y + 1) : (Sector) null;
      sector1.S = sector7;
      Sector sector8 = flagArray[6] ? state.Map.Get(this._rift, (int) this._x - 1, (int) this._y + 1) : (Sector) null;
      sector1.SW = sector8;
      Sector sector9 = flagArray[7] ? state.Map.Get(this._rift, (int) this._x - 1, (int) this._y) : (Sector) null;
      sector1.W = sector9;
      int num1 = this._starPort ? 1 : 0;
      sector1.StarPort = num1 != 0;
      int num2 = this._statusTimeMS / 1000;
      sector1.StatusTimeSeconds = num2;
      int status = (int) this._status;
      sector1.Status = (SectorStatus) status;
      int asteroid = (int) this._asteroid;
      sector1.Asteroid = (AsteroidClass) asteroid;
      int num3 = this._capturePoint ? 1 : 0;
      sector1.CapturePoint = num3 != 0;
      ClientAlliance clientAlliance = state.Alliances.Get(this._capturePointOwner);
      sector1.CapturePointOwner = clientAlliance;
    }
  }
}
