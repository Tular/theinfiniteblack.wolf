﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.CreateAuctionCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class CreateAuctionCommand : Command
  {
    private readonly int _id;
    private readonly EquipmentItem _item;
    private readonly int _ownerId;
    private readonly int _bid;
    private readonly int _buyout;
    private readonly short _minutes;
    private readonly int _lastBidderId;

    public CreateAuctionCommand(IByteBuffer input)
    {
      this._id = input.GetInt();
      this._item = EquipmentItem.Execute(input);
      this._ownerId = input.GetInt();
      this._bid = input.GetInt();
      this._buyout = input.GetInt();
      this._minutes = input.GetShort();
      this._lastBidderId = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      ClientAuction auction = state.Auctions.Get(this._id);
      auction.Item = this._item;
      auction.Owner = state.Players.Get(this._ownerId);
      auction.Bid = this._bid;
      auction.BuyoutCredits = this._buyout;
      auction.Time.Set((long) ((int) this._minutes * 60 * 1000));
      auction.Bidder = state.Players.Get(this._lastBidderId);
      if (auction.Owner == state.MyPlayer || auction.Bidder == state.MyPlayer)
        state.MySettings.AuctionWatch.Add(auction);
      state.UI.Show(new AuctionUpdatedEventArgs((IGameState) state, auction));
    }
  }
}
