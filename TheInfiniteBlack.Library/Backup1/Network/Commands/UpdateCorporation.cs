﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdateCorporation
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdateCorporation : Command
  {
    private readonly string _name;
    private readonly short _id;

    protected internal UpdateCorporation(IByteBuffer input)
    {
      this._name = input.GetString();
      this._id = input.GetShort();
    }

    public override void Execute(GameState state)
    {
      state.Corporations.Get(this._id).SetName(this._name);
    }
  }
}
