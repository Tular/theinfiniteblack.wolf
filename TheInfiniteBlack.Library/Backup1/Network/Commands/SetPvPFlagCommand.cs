﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.SetPvPFlagCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class SetPvPFlagCommand : Command
  {
    private readonly int _shipId;
    private readonly bool _flag;

    protected internal SetPvPFlagCommand(IByteBuffer input)
    {
      this._shipId = input.GetInt();
      this._flag = input.GetBool();
    }

    public override void Execute(GameState state)
    {
      Ship ship = state.Entities.Get<Ship>(this._shipId);
      bool pvPflag = ship.PvPFlag;
      int num = this._flag ? 1 : 0;
      ship.PvPFlag = num != 0;
      Ship myShip = state.MyShip;
      if (ship != myShip || (!pvPflag || this._flag) && (pvPflag || !this._flag))
        return;
      state.UI.Show(new PvPFlagChangeEventArgs((IGameState) state, this._flag));
    }
  }
}
