﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdateCurrentSector
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdateCurrentSector : Command
  {
    private readonly IByteBuffer _input;

    protected internal UpdateCurrentSector(IByteBuffer input)
    {
      this._input = input;
    }

    public override void Execute(GameState state)
    {
      bool rift = (uint) this._input.Get() > 0U;
      int x = (int) this._input.Get();
      int y = (int) this._input.Get();
      int capacity = (int) this._input.GetShort();
      Sector myLoc = state.Map.Get(rift, x, y);
      List<Entity> entities = new List<Entity>(capacity);
      for (int index = 0; index < capacity; ++index)
      {
        Entity entity = Entity.Execute((IGameState) state, this._input);
        if (entity != null)
          entities.Add(entity);
      }
      state.OnMove(myLoc, entities);
    }
  }
}
