﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.RequestType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network
{
  public static class RequestType
  {
    public const sbyte DISCONNECT = -126;
    public const sbyte ALIVE = -122;
    public const sbyte CHAT_MESSAGE = -66;
    public const sbyte REQUEST_LOGIN = -67;
    public const sbyte REQUEST_PLAYER = -65;
    public const sbyte REQUEST_PLAYER_STATS = -64;
    public const sbyte REQUEST_CORP_ROSTER = -63;
    public const sbyte REQUEST_INVENTORY = -62;
    public const sbyte REQUEST_MOVE = -58;
    public const sbyte REQUEST_HARVEST = -56;
    public const sbyte REQUEST_LOOT = -54;
    public const sbyte REQUEST_FOLLOW = -53;
    public const sbyte REQUEST_ATTACK = -52;
    public const sbyte REQUEST_REPAIR_TARGET = -51;
    public const sbyte REQUEST_REPAIR_BLACKDOLLAR = -50;
    public const sbyte REQUEST_REPAIR_STARPORT = -49;
    public const sbyte REQUEST_EARTH_JUMP = -48;
    public const sbyte REQUEST_BUY_SHIP = -46;
    public const sbyte REQUEST_SWAP_SHIP = -45;
    public const sbyte REQUEST_BD_ITEM_PURCHASE = -44;
    public const sbyte REQUEST_TRADE = -40;
    public const sbyte REQUEST_CANCEL_TRADE = -39;
    public const sbyte REQUEST_CORP_ACCEPT_INVITE = -38;
    public const sbyte REQUEST_CORP_REJECT_INVITE = -37;
    public const sbyte REQUEST_ALLIANCE_ACCEPT_INVITE = -36;
    public const sbyte REQUEST_ALLIANCE_REJECT_INVITE = -35;
    public const sbyte REQUEST_SET_TECHNOLOGY = -34;
    public const sbyte REQUEST_DEVELOP_BLACKDOLLARS = 2;
    public const sbyte REQUEST_DEVELOP_RESOURCES = 3;
    public const sbyte REQUEST_JETTISON = 12;
    public const sbyte REQUEST_RESOURCE_BUY = 13;
    public const sbyte REQUEST_RESOURCE_SELL = 14;
    public const sbyte REQUEST_RESOURCE_TRANSFER = 16;
    public const sbyte REQUEST_ITEM_JETTISON = 22;
    public const sbyte REQUEST_ITEM_EQUIP = 23;
    public const sbyte REQUEST_ITEM_UNEQUIP = 24;
    public const sbyte REQUEST_ITEM_MOVE_TO_BANK = 25;
    public const sbyte REQUEST_ITEM_REMOVE_FROM_BANK = 26;
    public const sbyte REQUEST_ITEM_MOVE_TO_GARRISON = 27;
    public const sbyte REQUEST_ITEM_REMOVE_FROM_GARRISON = 28;
    public const sbyte REQUEST_ITEM_BUY = 29;
    public const sbyte REQUEST_ITEM_SELL = 30;
    public const sbyte REQUEST_ITEM_ENGINEER = 31;
    public const sbyte ACCOUNT_VARIABLES = 32;
    public const sbyte REQUEST_SET_CORP_SHIP_COLOR = 33;
    public const sbyte AH_ITEM_REQUEST_PAGE = 44;
    public const sbyte AH_ITEM_REQUEST_BUYOUT = 45;
    public const sbyte AH_ITEM_REQUEST_BID = 46;
    public const sbyte AH_ITEM_REQUEST_SELL = 47;
    public const sbyte AH_ITEM_REQUEST_REMOVE = 48;
    public const sbyte AH_ITEM_REQUEST_ITEM = 49;
  }
}
