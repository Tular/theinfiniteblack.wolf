﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.TradeComponent
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library
{
  public class TradeComponent
  {
    public ClientPlayer Player { get; set; }

    public int BlackDollars { get; set; }

    public int Credits { get; set; }

    public EquipmentItem Item { get; set; }

    public bool Equals(TradeComponent other)
    {
      if (this.Player == other.Player && this.BlackDollars == other.BlackDollars && this.Credits == other.Credits)
        return this.Item == other.Item;
      return false;
    }
  }
}
