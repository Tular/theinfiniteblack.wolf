﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.DiscordBot
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library
{
  public static class DiscordBot
  {
    public static Dictionary<string, string> WebHooks = new Dictionary<string, string>()
    {
      {
        "Kieran9316",
        "https://discordapp.com/api/webhooks/253852629896003585/P-cZNinZO4xTKsfWnGt8p5qmSkFFi3p5kefcOs7RuRtobtZlrAsz15470dhK8yaYWHoh"
      },
      {
        "Zimrathon",
        "https://discordapp.com/api/webhooks/253852629896003585/P-cZNinZO4xTKsfWnGt8p5qmSkFFi3p5kefcOs7RuRtobtZlrAsz15470dhK8yaYWHoh"
      },
      {
        "Valdoartus",
        "https://discordapp.com/api/webhooks/253852629896003585/P-cZNinZO4xTKsfWnGt8p5qmSkFFi3p5kefcOs7RuRtobtZlrAsz15470dhK8yaYWHoh"
      },
      {
        "GreenGrouch",
        "https://discordapp.com/api/webhooks/253981340205187073/GDRN8mJuTGIeCFFGlWTt6LoZgOEtu35_KML4cWDV8upcIXVo--TVsEryg62xHAFnfzuf"
      },
      {
        "NanoGrouch",
        "https://discordapp.com/api/webhooks/253981340205187073/GDRN8mJuTGIeCFFGlWTt6LoZgOEtu35_KML4cWDV8upcIXVo--TVsEryg62xHAFnfzuf"
      },
      {
        "HITBOT",
        "https://discordapp.com/api/webhooks/254298747142537216/oHKJF0NkgsTcWi22aVObs_aU61_8LzxucxdA31drkaoEPP4DoYSPpo-KZHH37Tpgst7g"
      }
    };
    private static DiscordMessage _lastMessage = new DiscordMessage();
    private static HttpWebRequest _request;

    public static void SendPM(DiscordMessage message)
    {
      try
      {
        if (object.Equals((object) DiscordBot._lastMessage, (object) message))
          return;
        DiscordBot.UseDiscordWebhook(message.webook, message.username, message.icon_url, message.text);
        DiscordBot._lastMessage = message;
      }
      catch (Exception ex)
      {
        GameState.LogError(ex);
      }
    }

    private static void UseDiscordWebhook(string webhook, string username, string icon_url, string text)
    {
      ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(Certification.MyRemoteCertificateValidationCallback);
      DiscordBot._request = (HttpWebRequest) WebRequest.Create(webhook + "/slack");
      DiscordBot._request.Method = "POST";
      using (Stream requestStream = DiscordBot._request.GetRequestStream())
      {
        using (StreamWriter streamWriter = new StreamWriter(requestStream))
        {
          string str = string.Format("\"username\": \"{0}\", \"icon_url\": \"{1}\", \"text\":\"{2}\"", (object) username, (object) icon_url, (object) text);
          streamWriter.Write("{" + str + "}");
        }
      }
      DiscordBot._request.BeginGetResponse(new AsyncCallback(DiscordBot.FinishRequest), (object) null);
    }

    private static void FinishRequest(IAsyncResult result)
    {
      DiscordBot._request.EndGetResponse(result);
    }
  }
}
