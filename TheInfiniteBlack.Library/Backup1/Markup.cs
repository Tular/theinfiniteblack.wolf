﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Markup
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library
{
  public static class Markup
  {
    public const string BLACK = "[?]";
    public const string WHITE = "[w]";
    public const string RED = "[r]";
    public const string GREEN = "[g]";
    public const string LIGHT_BLUE = "[lb]";
    public const string LIGHT_GREEN = "[lg]";
    public const string BLUE = "[b]";
    public const string YELLOW = "[y]";
    public const string VIOLET = "[v]";
    public const string GREY = "[gr]";
    public const string ORANGE = "[o]";
    private const string HEX_BLACK = "#000000";
    private const string HEX_WHITE = "#FFFFFF";
    private const string HEX_RED = "#FF4400";
    private const string HEX_GREEN = "#00ff00";
    private const string HEX_LIGHT_BLUE = "#d1f1ff";
    private const string HEX_LIGHT_GREEN = "#dbff84";
    private const string HEX_BLUE = "#8dc6ff";
    private const string HEX_YELLOW = "#FFFF00";
    private const string HEX_VIOLET = "#ffabe6";
    private const string HEX_GREY = "#c5c5c5";
    private const string HEX_ORANGE = "#FFC469";
    private const string NGUI_BLACK = "[000000]";
    private const string NGUI_WHITE = "[FFFFFF]";
    private const string NGUI_RED = "[FF4400]";
    private const string NGUI_GREEN = "[00ff00]";
    private const string NGUI_LIGHT_BLUE = "[d1f1ff]";
    private const string NGUI_LIGHT_GREEN = "[dbff84]";
    private const string NGUI_BLUE = "[8dc6ff]";
    private const string NGUI_YELLOW = "[FFFF00]";
    private const string NGUI_VIOLET = "[ffabe6]";
    private const string NGUI_GREY = "[c5c5c5]";
    private const string NGUI_ORANGE = "[FFC469]";
    public const string BREAK_COLOR = "[-]";
    public const string BREAK_LINE_HTML = "<br/>";

    public static string GetHex(string markup)
    {
      uint num = 0;
      if (num <= 3064033068U)
      {
        if (num <= 1993767119U)
        {
          if ((int) num != 1991104095)
          {
            if ((int) num == 1993767119 && markup == "[b]")
              return "#8dc6ff";
          }
          else if (markup == "[r]")
            return "#FF4400";
        }
        else if ((int) num != 2047327526)
        {
          if ((int) num != 2060333142)
          {
            if ((int) num == -1230934228 && markup == "[y]")
              return "#FFFF00";
          }
          else if (markup == "[o]")
            return "#FFC469";
        }
        else if (markup == "[?]")
          return "#000000";
      }
      else if (num <= 3135380686U)
      {
        if ((int) num != -1229404037)
        {
          if ((int) num == -1159586610 && markup == "[g]")
            return "#00ff00";
        }
        else if (markup == "[v]")
          return "#ffabe6";
      }
      else if ((int) num != -996667242)
      {
        if ((int) num != -970871833)
        {
          if ((int) num == -903025882 && markup == "[lg]")
            return "#dbff84";
        }
        else if (markup == "[lb]")
          return "#d1f1ff";
      }
      else if (markup == "[gr]")
        return "#c5c5c5";
      return "#FFFFFF";
    }

    public static string GetPvPRatingColor(int rating)
    {
      if (rating <= 1000)
        return "[gr]";
      if (rating < 1200)
        return "[lg]";
      if (rating < 1400)
        return "[g]";
      if (rating < 1600 || rating < 1800)
        return "[y]";
      if (rating < 2000 || rating < 2200)
        return "[b]";
      if (rating < 2400)
        return "[v]";
      return rating < 2600 ? "[o]" : "[r]";
    }

    public static void GetHtml(StringBuilder sb)
    {
      sb.Replace("[-]", "</font>");
      sb.Replace("[w]", "<font color='#FFFFFF'>");
      sb.Replace("[r]", "<font color='#FF4400'>");
      sb.Replace("[g]", "<font color='#00ff00'>");
      sb.Replace("[lb]", "<font color='#d1f1ff'>");
      sb.Replace("[lg]", "<font color='#dbff84'>");
      sb.Replace("[b]", "<font color='#8dc6ff'>");
      sb.Replace("[y]", "<font color='#FFFF00'>");
      sb.Replace("[v]", "<font color='#ffabe6'>");
      sb.Replace("[gr]", "<font color='#c5c5c5'>");
      sb.Replace("[o]", "<font color='#FFC469'>");
    }

    public static void GetNGUI(StringBuilder sb)
    {
      sb.Replace("[w]", "[FFFFFF]");
      sb.Replace("[r]", "[FF4400]");
      sb.Replace("[g]", "[00ff00]");
      sb.Replace("[lb]", "[d1f1ff]");
      sb.Replace("[lg]", "[dbff84]");
      sb.Replace("[b]", "[8dc6ff]");
      sb.Replace("[y]", "[FFFF00]");
      sb.Replace("[v]", "[ffabe6]");
      sb.Replace("[gr]", "[c5c5c5]");
      sb.Replace("[o]", "[FFC469]");
      sb.Replace("<B>", "[b");
      sb.Replace("<b>", "[b]");
      sb.Replace("</B>", "[/b]");
      sb.Replace("</b>", "[/b]");
      sb.Replace("<U>", "[u]");
      sb.Replace("<u>", "[u]");
      sb.Replace("</U>", "[/u]");
      sb.Replace("</u>", "[/u]");
      sb.Replace("<I>", "[i]");
      sb.Replace("<i>", "[i]");
      sb.Replace("</I>", "[/i]");
      sb.Replace("</i>", "[/i]");
      sb.Replace("<S>", "[s]");
      sb.Replace("<s>", "[s]");
      sb.Replace("<Ss>", "[/s]");
      sb.Replace("</s>", "[/s]");
      sb.Replace("<url>", "[url]");
      sb.Replace("</url>", "[/url]");
    }
  }
}
