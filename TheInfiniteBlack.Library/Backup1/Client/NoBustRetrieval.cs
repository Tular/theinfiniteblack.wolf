﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.NoBustRetrieval
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace TheInfiniteBlack.Library.Client
{
  public class NoBustRetrieval
  {
    public static List<string> GetNoBustList()
    {
      using (StreamReader streamReader = new StreamReader((WebRequest.Create("http://typhlosion.co.uk/map/nobust") as HttpWebRequest).GetResponse().GetResponseStream()))
        return ((IEnumerable<string>) streamReader.ReadToEnd().Replace("[", "").Replace("]", "").Replace("\"", "").Split(',')).Select<string, string>((Func<string, string>) (x => x.Trim())).Where<string>((Func<string, bool>) (x => x != "Garry Name")).ToList<string>();
    }
  }
}
