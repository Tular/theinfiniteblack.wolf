﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.NotStunned
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  internal class NotStunned : NpcAttackOrder
  {
    public override string Description
    {
      get
      {
        return "Not Stunned or Top";
      }
    }

    public override Npc SelectTargetFrom(IEnumerable<Npc> npcs)
    {
      Npc npc = (Npc) null;
      if (npcs.Any<Npc>((Func<Npc, bool>) (x => !x.Stunned)))
        npc = npcs.First<Npc>((Func<Npc, bool>) (x => !x.Stunned));
      if (npc == null)
        npc = npcs.First<Npc>();
      return npc;
    }

    public override NpcAttackOrder Next()
    {
      return (NpcAttackOrder) new LowestHullFirst();
    }
  }
}
