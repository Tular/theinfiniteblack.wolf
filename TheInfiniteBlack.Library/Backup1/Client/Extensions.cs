﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Extensions
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Client
{
  public static class Extensions
  {
    public static string Name(this CurrencyType type)
    {
      switch (type)
      {
        case CurrencyType.Credits:
          return "Credits";
        case CurrencyType.BlackDollars:
          return "BlackDollars";
        case CurrencyType.RewardPoints:
          return "Reward Points";
        case CurrencyType.CombatPoints:
          return "Combat Points";
        default:
          return "ERR";
      }
    }
  }
}
