﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientNet
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using TheInfiniteBlack.Library.Network;
using TheInfiniteBlack.Library.Network.Commands;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client
{
  internal sealed class ClientNet : DataReceiver, INetwork
  {
    private readonly Queue<Command> _queue = new Queue<Command>();
    private readonly INetworkData _alive = RequestAlive.Create();
    private int _ping;
    private long _lastPingTimeMS;
    private DataSender _sender;
    private long _bytesSent;
    private long _commandsSent;
    private long _bytesReceived;
    private long _commandsReceived;
    private long _nextAliveMS;

    public byte[] IV { get; set; }

    public int Ping
    {
      get
      {
        return this._ping;
      }
    }

    public long BytesSent
    {
      get
      {
        return this._bytesSent;
      }
    }

    public long CommandsSent
    {
      get
      {
        return this._commandsSent;
      }
    }

    public long BytesReceived
    {
      get
      {
        return this._bytesReceived;
      }
    }

    public long CommandsReceived
    {
      get
      {
        return this._commandsReceived;
      }
    }

    public void TryDoAlive(IGameState state)
    {
      long worldTimeMs = state.WorldTimeMS;
      if (worldTimeMs < this._nextAliveMS)
        return;
      this._nextAliveMS = worldTimeMs + 15000L;
      this._ping = -1;
      this._lastPingTimeMS = worldTimeMs;
      this.Send(this._alive);
    }

    public void GotAlive(IGameState state)
    {
      if (this._ping == -1)
        this._ping = (int) (state.WorldTimeMS - this._lastPingTimeMS);
      Log.I((object) this, nameof (GotAlive), "Ping = " + (object) this._ping + " ms");
    }

    public override void Connect(Socket socket)
    {
      this.IV = (byte[]) null;
      base.Connect(socket);
      this._bytesSent = 0L;
      this._commandsSent = 0L;
      this._bytesReceived = 0L;
      this._commandsReceived = 0L;
      this._sender = new DataSender(socket);
    }

    public Command GetNextCommand()
    {
      if (!Monitor.TryEnter((object) this._queue, 0))
        return (Command) null;
      if (this._queue.Count == 0)
      {
        Monitor.Exit((object) this._queue);
        return (Command) null;
      }
      Command command = this._queue.Dequeue();
      Monitor.Exit((object) this._queue);
      return command;
    }

    protected override void OnData(sbyte[] data)
    {
      this._commandsReceived = this._commandsReceived + 1L;
      this._bytesReceived = this._bytesReceived + (long) data.Length;
      Command command = Command.Create((IByteBuffer) new ByteBuffer(data));
      if (command == null)
        throw new Exception("NULL COMMAND");
      lock (this._queue)
        this._queue.Enqueue(command);
    }

    public override void Disconnect(string reason)
    {
      this.IV = (byte[]) null;
      base.Disconnect(reason);
      lock (this._queue)
        this._queue.Clear();
      DataSender sender = this._sender;
      this._sender = (DataSender) null;
      if (sender == null)
        return;
      sender.Disconnect(reason);
    }

    public void Send(INetworkData data)
    {
      DataSender sender = this._sender;
      if (sender == null)
        return;
      sender.Send(data);
      this._commandsSent = this._commandsSent + 1L;
      this._bytesSent = this._bytesSent + (long) data.Position;
    }
  }
}
