﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientBank
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client
{
  public class ClientBank
  {
    private List<EquipmentItem> _items = new List<EquipmentItem>();
    private int _credits;
    private int _blackDollars;
    private int _combatPoints;
    private float _rewardPoints;
    private int _lastChangeFlagId;

    public float this[CurrencyType type]
    {
      get
      {
        switch (type)
        {
          case CurrencyType.Credits:
            return (float) this._credits;
          case CurrencyType.BlackDollars:
            return (float) this._blackDollars;
          case CurrencyType.RewardPoints:
            return this._rewardPoints;
          case CurrencyType.CombatPoints:
            return (float) this._combatPoints;
          default:
            return 0.0f;
        }
      }
    }

    public int LastChangeFlagID
    {
      get
      {
        return this._lastChangeFlagId;
      }
    }

    public int Credits
    {
      get
      {
        return this._credits;
      }
      set
      {
        if (this._credits == value)
          return;
        this._credits = value;
        this.OnChanged();
      }
    }

    public int BlackDollars
    {
      get
      {
        return this._blackDollars;
      }
      set
      {
        if (this._blackDollars == value)
          return;
        this._blackDollars = value;
        this.OnChanged();
      }
    }

    public int CombatPoints
    {
      get
      {
        return this._combatPoints;
      }
      set
      {
        if (this._combatPoints == value)
          return;
        this._combatPoints = value;
        this.OnChanged();
      }
    }

    public float RewardPoints
    {
      get
      {
        return this._rewardPoints;
      }
      set
      {
        if ((double) Math.Abs(this._rewardPoints - value) < 1.0000000116861E-07)
          return;
        this._rewardPoints = value;
        this.OnChanged();
      }
    }

    public int ItemCount
    {
      get
      {
        return this._items.Count;
      }
    }

    public IEnumerable<EquipmentItem> Items
    {
      get
      {
        return (IEnumerable<EquipmentItem>) this._items;
      }
    }

    protected internal void Set(List<EquipmentItem> items)
    {
      this._items = items;
      this.OnChanged();
    }

    private void OnChanged()
    {
      this._lastChangeFlagId = this._lastChangeFlagId + 1;
    }
  }
}
