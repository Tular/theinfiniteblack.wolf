﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.INetwork
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Net.Sockets;
using TheInfiniteBlack.Library.Network;
using TheInfiniteBlack.Library.Network.Commands;

namespace TheInfiniteBlack.Library.Client
{
  public interface INetwork
  {
    byte[] IV { get; set; }

    bool Connected { get; }

    int Ping { get; }

    void TryDoAlive(IGameState state);

    void GotAlive(IGameState state);

    void Connect(Socket socket);

    void Disconnect(string reason);

    void Send(INetworkData data);

    Command GetNextCommand();
  }
}
