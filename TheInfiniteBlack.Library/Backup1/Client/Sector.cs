﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Sector
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public abstract class Sector
  {
    private AsteroidClass _asteroid = AsteroidClass.NULL;
    private List<ClientPlayer> _players = new List<ClientPlayer>();
    private SectorStatus _status = SectorStatus.NULL;
    private bool _marked = false;
    public bool Alerted = false;
    private DateTime _roidChange = DateTime.MinValue;
    private readonly SectorClass _class;
    private readonly int _x;
    private readonly int _y;
    protected internal int F;
    protected internal int G;
    protected internal Sector Parent;
    private ClientAlliance _alliance;
    private bool _capturePoint;
    private ClientAlliance _capturePointOwner;
    private ClientCorporation _corporation;
    private Sector _e;
    private int _lastChangeFlagId;
    private Sector _n;
    private Sector _ne;
    private Sector _nw;
    private Sector _s;
    private Sector _se;
    private bool _starPort;
    private int _statusTimeSeconds;
    private Sector _sw;
    private Sector _w;

    public bool Marked
    {
      get
      {
        return this._marked;
      }
    }

    public int LastChangeFlagID
    {
      get
      {
        return this._lastChangeFlagId;
      }
    }

    public bool IsSol
    {
      get
      {
        if (!this.Rift && this._x == 39)
          return this._y == 39;
        return false;
      }
    }

    public abstract bool Rift { get; }

    public int X
    {
      get
      {
        return this._x;
      }
    }

    public int Y
    {
      get
      {
        return this._y;
      }
    }

    public SectorClass Class
    {
      get
      {
        if (this._marked)
          return SectorClass.NEUTRAL;
        if ((DateTime.Now - this._roidChange).TotalSeconds < 120.0 && GameState.ShowAsteroidChanges)
          return SectorClass.PROTECTED;
        return this._class;
      }
    }

    public int PlayerCount
    {
      get
      {
        return this._players.Count;
      }
    }

    public IEnumerable<ClientPlayer> Players
    {
      get
      {
        return (IEnumerable<ClientPlayer>) this._players;
      }
    }

    public SectorVisibility Visibility
    {
      get
      {
        if (this.Status != SectorStatus.NULL)
          return SectorVisibility.Explored;
        return this._nw != null || this._n != null || (this._ne != null || this._e != null) || (this._se != null || this._s != null || (this._sw != null || this._w != null)) ? SectorVisibility.Encountered : SectorVisibility.Unexplored;
      }
    }

    public SectorStatus Status
    {
      get
      {
        return this._status;
      }
      set
      {
        if (this._status == value)
          return;
        this._status = value;
        this.OnChanged();
      }
    }

    public int StatusTimeSeconds
    {
      get
      {
        if (GameState.NoBust && (this.Corporation != null && !GameState.NoBustList.Contains(this.Corporation.Name)))
          return (int) this.Corporation.GarrisonRank;
        return this._statusTimeSeconds;
      }
      set
      {
        if (this._statusTimeSeconds == value)
          return;
        this._statusTimeSeconds = value;
        this.OnChanged();
      }
    }

    public AsteroidClass Asteroid
    {
      get
      {
        return this._asteroid;
      }
      set
      {
        if (this._asteroid == value)
          return;
        this._asteroid = value;
        if (this._asteroid == AsteroidClass.NULL)
          this._roidChange = DateTime.Now;
        this.OnChanged();
      }
    }

    public bool StarPort
    {
      get
      {
        return this._starPort;
      }
      set
      {
        if (this._starPort == value)
          return;
        this._starPort = value;
        this.OnChanged();
      }
    }

    public bool CapturePoint
    {
      get
      {
        return this._capturePoint;
      }
      set
      {
        if (this._capturePoint == value)
          return;
        this._capturePoint = value;
        this.OnChanged();
      }
    }

    public ClientAlliance CapturePointOwner
    {
      get
      {
        return this._capturePointOwner;
      }
      set
      {
        if (this._capturePointOwner == value)
          return;
        this._capturePointOwner = value;
        this.OnChanged();
      }
    }

    public ClientAlliance Alliance
    {
      get
      {
        ClientAlliance alliance = this._alliance;
        if (alliance != null)
          return alliance;
        if (this._corporation == null)
          return (ClientAlliance) null;
        return this._corporation.Alliance;
      }
      set
      {
        this._alliance = value;
        this.OnChanged();
      }
    }

    public ClientCorporation Corporation
    {
      get
      {
        return this._corporation;
      }
      set
      {
        this._corporation = value;
        this.OnChanged();
      }
    }

    public Sector NW
    {
      get
      {
        return this._nw;
      }
      set
      {
        if (this._nw == value)
          return;
        if (this._nw != null && this._nw._se == this)
          this._nw.SE = (Sector) null;
        this._nw = value;
        if (this._nw != null && this._nw._se != this)
          this._nw.SE = this;
        this.OnChanged();
      }
    }

    public Sector N
    {
      get
      {
        return this._n;
      }
      set
      {
        if (this._n == value)
          return;
        if (this._n != null && this._n._s == this)
          this._n.S = (Sector) null;
        this._n = value;
        if (this._n != null && this._n._s != this)
          this._n.S = this;
        this.OnChanged();
      }
    }

    public Sector NE
    {
      get
      {
        return this._ne;
      }
      set
      {
        if (this._ne == value)
          return;
        if (this._ne != null && this._ne._sw == this)
          this._ne.SW = (Sector) null;
        this._ne = value;
        if (this._ne != null && this._ne._sw != this)
          this._ne.SW = this;
        this.OnChanged();
      }
    }

    public Sector E
    {
      get
      {
        return this._e;
      }
      set
      {
        if (this._e == value)
          return;
        if (this._e != null && this._e._w == this)
          this._e.W = (Sector) null;
        this._e = value;
        if (this._e != null && this._e._w != this)
          this._e.W = this;
        this.OnChanged();
      }
    }

    public Sector SE
    {
      get
      {
        return this._se;
      }
      set
      {
        if (this._se == value)
          return;
        if (this._se != null && this._se._nw == this)
          this._se.NW = (Sector) null;
        this._se = value;
        if (this._se != null && this._se._nw != this)
          this._se.NW = this;
        this.OnChanged();
      }
    }

    public Sector S
    {
      get
      {
        return this._s;
      }
      set
      {
        if (this._s == value)
          return;
        if (this._s != null && this._s._n == this)
          this._s.N = (Sector) null;
        this._s = value;
        if (this._s != null && this._s._n != this)
          this._s.N = this;
        this.OnChanged();
      }
    }

    public Sector SW
    {
      get
      {
        return this._sw;
      }
      set
      {
        if (this._sw == value)
          return;
        if (this._sw != null && this._sw._ne == this)
          this._sw.NE = (Sector) null;
        this._sw = value;
        if (this._sw != null && this._sw._ne != this)
          this._sw.NE = this;
        this.OnChanged();
      }
    }

    public Sector W
    {
      get
      {
        return this._w;
      }
      set
      {
        if (this._w == value)
          return;
        if (this._w != null && this._w._e == this)
          this._w.E = (Sector) null;
        this._w = value;
        if (this._w != null && this._w._e != this)
          this._w.E = this;
        this.OnChanged();
      }
    }

    protected Sector(int x, int y)
    {
      this._x = x;
      this._y = y;
      this._class = Sector.GetClass(this.Rift, x, y);
    }

    public Sector GetNeighbor(Direction dir)
    {
      switch (dir)
      {
        case Direction.NW:
          return this._nw;
        case Direction.N:
          return this._n;
        case Direction.NE:
          return this._ne;
        case Direction.E:
          return this._e;
        case Direction.SE:
          return this._se;
        case Direction.S:
          return this._s;
        case Direction.SW:
          return this._sw;
        case Direction.W:
          return this._w;
        default:
          return (Sector) null;
      }
    }

    public bool Contains(ClientPlayer player)
    {
      return this._players.Contains(player);
    }

    protected internal void Add(ClientPlayer player)
    {
      List<ClientPlayer> clientPlayerList = new List<ClientPlayer>((IEnumerable<ClientPlayer>) this._players);
      clientPlayerList.Remove(player);
      clientPlayerList.Add(player);
      this._players = clientPlayerList;
      this.OnChanged();
    }

    protected internal void Remove(ClientPlayer player)
    {
      List<ClientPlayer> clientPlayerList = new List<ClientPlayer>((IEnumerable<ClientPlayer>) this._players);
      clientPlayerList.Remove(player);
      this._players = clientPlayerList;
      this.OnChanged();
    }

    protected internal void ClearPathfinding()
    {
      this.G = 0;
      this.F = 0;
      this.Parent = (Sector) null;
    }

    public void AppendName(StringBuilder sb)
    {
      sb.Append(this.Rift ? "Rift Sector " : "Sector ");
      sb.Append(this._x + 1);
      sb.Append(" / ");
      sb.Append(this._y + 1);
    }

    public bool IsConnected(Sector target)
    {
      return target != null && (this._nw == target || this._n == target || (this._ne == target || this._e == target) || (this._se == target || this._s == target || (this._sw == target || this._w == target)));
    }

    public IList<Sector> Connections()
    {
      List<Sector> sectorList = new List<Sector>();
      if (this._nw != null)
        sectorList.Add(this._nw);
      if (this._n != null)
        sectorList.Add(this._n);
      if (this._ne != null)
        sectorList.Add(this._ne);
      if (this._e != null)
        sectorList.Add(this._e);
      if (this._se != null)
        sectorList.Add(this._se);
      if (this._s != null)
        sectorList.Add(this._s);
      if (this._sw != null)
        sectorList.Add(this._sw);
      if (this._w != null)
        sectorList.Add(this._w);
      return (IList<Sector>) sectorList;
    }

    public int InvasionFreeConnections()
    {
      int num = 0;
      if (this._nw != null && !this._nw.IsInvasion())
        ++num;
      if (this._n != null && !this._n.IsInvasion())
        ++num;
      if (this._ne != null && !this._ne.IsInvasion())
        ++num;
      if (this._e != null && !this._e.IsInvasion())
        ++num;
      if (this._se != null && !this._se.IsInvasion())
        ++num;
      if (this._s != null && !this._s.IsInvasion())
        ++num;
      if (this._sw != null && !this._sw.IsInvasion())
        ++num;
      if (this._w != null && !this._w.IsInvasion())
        ++num;
      return num;
    }

    public int NonShallowsConnections()
    {
      int num = 0;
      if (this._nw != null && !this._nw.IsGreyShallows())
        ++num;
      if (this._n != null && !this._n.IsGreyShallows())
        ++num;
      if (this._ne != null && !this._ne.IsGreyShallows())
        ++num;
      if (this._e != null && !this._e.IsGreyShallows())
        ++num;
      if (this._se != null && !this._se.IsGreyShallows())
        ++num;
      if (this._s != null && !this._s.IsGreyShallows())
        ++num;
      if (this._sw != null && !this._sw.IsGreyShallows())
        ++num;
      if (this._w != null && !this._w.IsGreyShallows())
        ++num;
      return num;
    }

    public Direction GetDirection(Sector target)
    {
      if (target == null)
        return Direction.None;
      if (this._nw == target)
        return Direction.NW;
      if (this._n == target)
        return Direction.N;
      if (this._ne == target)
        return Direction.NE;
      if (this._e == target)
        return Direction.E;
      if (this._se == target)
        return Direction.SE;
      if (this._s == target)
        return Direction.S;
      if (this._sw == target)
        return Direction.SW;
      return this._w == target ? Direction.W : Direction.None;
    }

    public override string ToString()
    {
      StringBuilder sb = new StringBuilder(100);
      this.AppendName(sb);
      return sb.ToString();
    }

    protected internal void OnChanged()
    {
      this._lastChangeFlagId = this._lastChangeFlagId + 1;
    }

    public static SectorClass GetClass(bool rift, int x, int y)
    {
      switch (x)
      {
        case 38:
        case 39:
        case 40:
          switch (y)
          {
            case 38:
            case 39:
            case 40:
              return SectorClass.PROTECTED;
          }
      }
      if (rift)
        return SectorClass.CONTESTED;
      double num1 = (double) (39 - x);
      double num2 = (double) (39 - y);
      return Math.Sqrt(num1 * num1 + num2 * num2) > 10.0 ? SectorClass.CONTESTED : SectorClass.NEUTRAL;
    }

    public void Mark()
    {
      if (!this.IsInvasion())
        return;
      this._marked = true;
      this.Alerted = false;
    }

    public void UnMark()
    {
      this._marked = false;
      this.Alerted = false;
    }

    public bool IsInvasion()
    {
      return this._status == SectorStatus.HETEROCLITE || this._status == SectorStatus.WYRD || this._status == SectorStatus.PIRATE;
    }

    public bool IsGreyShallows()
    {
      return !this.Rift && this._x > 20 && (this._y > 20 && this._x < 59) && this._y < 59;
    }
  }
}
