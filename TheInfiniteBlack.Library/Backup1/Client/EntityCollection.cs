﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.EntityCollection
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public class EntityCollection
  {
    private readonly Dictionary<int, Entity> _values = new Dictionary<int, Entity>();

    public IEnumerable<Entity> Values
    {
      get
      {
        return (IEnumerable<Entity>) this._values.Values;
      }
    }

    public void Add(Entity value)
    {
      this._values[value.ID] = value;
    }

    public T Get<T>(int id) where T : Entity
    {
      if (id == int.MinValue)
        return default (T);
      if (!this._values.ContainsKey(id))
        return default (T);
      return this._values[id] as T;
    }
  }
}
