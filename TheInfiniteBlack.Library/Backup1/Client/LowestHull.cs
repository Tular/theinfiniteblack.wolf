﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.LowestHull
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public class LowestHull : PlayerAttackOrder
  {
    public override string Description
    {
      get
      {
        return "Lowest Hull";
      }
    }

    public override Ship SelectTarget(IEnumerable<Ship> enemies)
    {
      return enemies.OrderBy<Ship, int>((Func<Ship, int>) (x => x.Hull)).First<Ship>();
    }

    public override PlayerAttackOrder Next()
    {
      return (PlayerAttackOrder) new LowestGrappleResist();
    }
  }
}
