﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.LinqExtension
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace TheInfiniteBlack.Library.Client
{
  public static class LinqExtension
  {
    public static IQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string orderByProperty, bool desc)
    {
      string methodName = desc ? "OrderByDescending" : nameof (OrderBy);
      Type type = typeof (TEntity);
      PropertyInfo property = type.GetProperty(orderByProperty);
      ParameterExpression parameterExpression = Expression.Parameter(type, "p");
      LambdaExpression lambdaExpression = Expression.Lambda((Expression) Expression.MakeMemberAccess((Expression) parameterExpression, (MemberInfo) property), new ParameterExpression[1]
      {
        parameterExpression
      });
      MethodCallExpression methodCallExpression = Expression.Call(typeof (Queryable), methodName, new Type[2]
      {
        type,
        property.PropertyType
      }, source.Expression, (Expression) Expression.Quote((Expression) lambdaExpression));
      return source.Provider.CreateQuery<TEntity>((Expression) methodCallExpression);
    }
  }
}
