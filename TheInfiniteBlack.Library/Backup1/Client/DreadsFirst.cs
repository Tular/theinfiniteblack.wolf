﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.DreadsFirst
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  internal class DreadsFirst : NpcAttackOrder
  {
    public override string Description
    {
      get
      {
        return "Dread Reaper, Assassin, King, Queen, Enforcer";
      }
    }

    public override Npc SelectTargetFrom(IEnumerable<Npc> npcs)
    {
      Npc npc = (Npc) null;
      if (npcs.Any<Npc>((Func<Npc, bool>) (x => x.Level == NpcLevel.DREAD)))
      {
        if (npcs.Any<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Reaper && x.Level == NpcLevel.DREAD)))
          npc = npcs.First<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Reaper && x.Level == NpcLevel.DREAD));
        else if (npcs.Any<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Assassin && x.Level == NpcLevel.DREAD)))
          npc = npcs.First<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Assassin && x.Level == NpcLevel.DREAD));
        else if (npcs.Any<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.King && x.Level == NpcLevel.DREAD)))
          npc = npcs.First<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.King && x.Level == NpcLevel.DREAD));
        else if (npcs.Any<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Queen && x.Level == NpcLevel.DREAD)))
          npc = npcs.First<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Queen && x.Level == NpcLevel.DREAD));
        else if (npcs.Any<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Enforcer && x.Level == NpcLevel.DREAD)))
          npc = npcs.First<Npc>((Func<Npc, bool>) (x => x.Class == NpcClass.Enforcer && x.Level == NpcLevel.DREAD));
      }
      if (npc == null)
        npc = npcs.First<Npc>();
      return npc;
    }

    public override NpcAttackOrder Next()
    {
      return (NpcAttackOrder) new NotStunned();
    }
  }
}
