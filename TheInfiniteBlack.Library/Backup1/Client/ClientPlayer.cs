﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientPlayer
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library.Client
{
  public class ClientPlayer : NamedClientObject
  {
    private string _name = "Unknown?";
    private readonly int _id;
    private bool _hardcore;
    private bool _online;
    private ClientCorporation _corporation;
    private CorpRankType _rank;
    private Sector _location;
    private short _kills;
    private short _deaths;
    private short _garrisonKills;
    private short _garrisonDefends;
    private short _pvpRating;
    private int _bounty;
    private sbyte _shipSkin;

    public int ID
    {
      get
      {
        return this._id;
      }
    }

    public bool Hardcore
    {
      get
      {
        return this._hardcore;
      }
      set
      {
        if (this._hardcore == value)
          return;
        this._hardcore = value;
        this.OnChanged();
      }
    }

    public bool Online
    {
      get
      {
        return this._online;
      }
      set
      {
        if (this._online == value)
          return;
        this._online = value;
        this.OnChanged();
      }
    }

    public bool IsCorpLeader
    {
      get
      {
        if (this._corporation != null)
          return this._rank >= CorpRankType.LEADER;
        return false;
      }
    }

    public bool IsAllianceLeader
    {
      get
      {
        if (this._corporation != null && this._corporation.IsAllianceLeader)
          return this._rank >= CorpRankType.LEADER;
        return false;
      }
    }

    public ClientAlliance MyAlliance
    {
      get
      {
        if (this._corporation != null)
          return this._corporation.Alliance;
        return (ClientAlliance) null;
      }
    }

    public ClientCorporation MyCorporation
    {
      get
      {
        return this._corporation;
      }
      set
      {
        if (this._corporation == value)
          return;
        if (this._corporation != null)
          this._corporation.Remove(this);
        this._corporation = value;
        if (this._corporation != null)
          this._corporation.Add(this);
        this.OnChanged();
      }
    }

    public CorpRankType Rank
    {
      get
      {
        return this._rank;
      }
      set
      {
        if (this._rank == value)
          return;
        this._rank = value;
        this.OnChanged();
      }
    }

    public Sector Location
    {
      get
      {
        return this._location;
      }
      set
      {
        if (this._location == value)
          return;
        if (this._location != null)
          this._location.Remove(this);
        this._location = value;
        if (this._location != null)
          this._location.Add(this);
        this.OnChanged();
      }
    }

    public RelationType Relation
    {
      get
      {
        return ClientPlayer.GetRelation(this, this.State.MyPlayer);
      }
    }

    public short Kills
    {
      get
      {
        return this._kills;
      }
      set
      {
        this._kills = value;
        this.OnChanged();
      }
    }

    public short Deaths
    {
      get
      {
        return this._deaths;
      }
      set
      {
        this._deaths = value;
        this.OnChanged();
      }
    }

    public short GarrisonKills
    {
      get
      {
        return this._garrisonKills;
      }
      set
      {
        this._garrisonKills = value;
        this.OnChanged();
      }
    }

    public short GarrisonDefends
    {
      get
      {
        return this._garrisonDefends;
      }
      set
      {
        this._garrisonDefends = value;
        this.OnChanged();
      }
    }

    public short PvpRating
    {
      get
      {
        return this._pvpRating;
      }
      set
      {
        this._pvpRating = value;
        this.OnChanged();
      }
    }

    public int Bounty
    {
      get
      {
        return this._bounty;
      }
      set
      {
        this._bounty = value;
        this.OnChanged();
      }
    }

    public sbyte ShipSkin
    {
      get
      {
        return this._shipSkin;
      }
      set
      {
        this._shipSkin = value;
        this.OnChanged();
      }
    }

    public ClientPlayer(IGameState state, int id)
      : base(state)
    {
      this._id = id;
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this._name);
      if (!markup)
        return;
      sb.Append("[-]");
    }

    protected internal void SetName(string value)
    {
      if (this._name == value)
        return;
      this._name = value;
      this.OnChanged();
    }

    public static RelationType GetRelation(ClientPlayer a, ClientPlayer b)
    {
      if (a == null || b == null)
        return RelationType.NULL;
      if (a == b)
        return RelationType.SELF;
      return a._corporation != null && a._corporation == b._corporation || a.MyAlliance != null && a.MyAlliance == b.MyAlliance ? RelationType.FRIEND : RelationType.ENEMY;
    }
  }
}
