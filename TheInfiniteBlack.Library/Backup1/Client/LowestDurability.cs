﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.LowestDurability
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client
{
  public class LowestDurability : PlayerAttackOrder
  {
    public override string Description
    {
      get
      {
        return "Lowest Durability";
      }
    }

    public override Ship SelectTarget(IEnumerable<Ship> enemies)
    {
      return enemies.OrderBy<Ship, int>((Func<Ship, int>) (x => x.AllEquippedItems.Sum<EquipmentItem>((Func<EquipmentItem, int>) (i => (int) i.Durability)) / x.AllEquippedItems.Count)).First<Ship>();
    }

    public override PlayerAttackOrder Next()
    {
      return (PlayerAttackOrder) new HighestDurability();
    }
  }
}
