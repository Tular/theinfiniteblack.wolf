﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Mods.Emulator
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Mods
{
  public static class Emulator
  {
    private static Random _random = new Random();

    public static List<VolleyResult> Emulate(IList<Ship> dTeam, IEnumerable<Ship> enemyShips)
    {
      List<VolleyResult> volleyResultList = new List<VolleyResult>();
      foreach (Ship enemyShip in enemyShips)
        volleyResultList.Add(Emulator.Volley(dTeam, enemyShip, 100));
      return volleyResultList;
    }

    private static VolleyResult Volley(IList<Ship> dTeam, Ship enemyShip, int iterations)
    {
      int num1 = 0;
      for (int index = 0; index < iterations; ++index)
      {
        int num2 = 0;
        foreach (Ship attacker in (IEnumerable<Ship>) dTeam)
          num2 += Emulator.DealDamage(enemyShip, attacker);
        if (num2 > enemyShip.MaxHull)
          ++num1;
      }
      return new VolleyResult()
      {
        PlayerName = enemyShip.Player.Name,
        SuccessChance = (double) num1 / (double) iterations
      };
    }

    private static int AtLeastZero(int value)
    {
      return value < 0 ? 0 : value;
    }

    private static int DealDamage(Ship target, Ship attacker)
    {
      int num1 = Emulator._random.Next(0, (int) ((double) attacker.HitChance + 100.0));
      if (Emulator._random.Next(0, Emulator.AtLeastZero((int) target.EvasionChance)) >= num1 && (double) attacker.HitChance < 100.0)
        return 0;
      int num2 = Emulator._random.Next(0, Emulator.AtLeastZero((int) attacker.CriticalChance));
      int num3 = Emulator._random.Next(0, (int) ((double) target.CriticalResist + 100.0));
      int num4 = Emulator._random.Next(0, (int) ((double) attacker.HitChance + 100.0));
      int num5 = Emulator._random.Next(0, Emulator.AtLeastZero((int) target.EvasionChance));
      float num6 = num2 <= num3 ? (num5 < num4 ? 1f : 0.5f) : 2f + attacker.CriticalDamageMod;
      int num7 = (int) ((double) Emulator._random.Next(attacker.MinimumDamage, attacker.MinimumDamage * 2) * (double) num6);
      if (attacker.Tech.Defender)
        num7 = (int) ((double) num7 * 1.1);
      return (int) ((double) num7 * (1.0 + (double) target.IncomingDamageMod));
    }
  }
}
