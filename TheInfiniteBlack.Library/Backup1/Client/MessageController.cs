﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.MessageController
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.IO;
using System.Net;

namespace TheInfiniteBlack.Library.Client
{
  public class MessageController : IMessageController
  {
    private HttpWebRequest _heartbeatRequest;
    private HttpWebRequest _messageRequest;
    private IGameState _state;

    public void SendHeartBeat(string player)
    {
      this._heartbeatRequest = WebRequest.Create("http://typhlosion.co.uk/message/registerplayer?player=" + player) as HttpWebRequest;
      this._heartbeatRequest.BeginGetResponse(new AsyncCallback(this.FinishHeartbeat), (object) null);
    }

    private void FinishHeartbeat(IAsyncResult result)
    {
      this._heartbeatRequest.EndGetResponse(result);
    }

    public void GetMessage(string player, IGameState state)
    {
      this._state = state;
      this._messageRequest = WebRequest.Create("http://typhlosion.co.uk/message/get?player=" + player) as HttpWebRequest;
      this._messageRequest.BeginGetResponse(new AsyncCallback(this.FinishPlayerMessage), (object) null);
    }

    public void FinishPlayerMessage(IAsyncResult result)
    {
      HttpWebResponse response = this._messageRequest.EndGetResponse(result) as HttpWebResponse;
      if (response.StatusCode == HttpStatusCode.NoContent)
        return;
      using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
      {
        string str1 = streamReader.ReadToEnd().Replace("{", "").Replace("}", "").Replace("\"", "");
        if (str1.IndexOf("Channel") == -1 || str1.IndexOf("Message") == -1)
          return;
        string str2 = str1.Remove(0, str1.IndexOf("Channel:") + 8);
        string s = str2.Substring(0, str2.IndexOf(","));
        string str3 = str1.Remove(0, str1.IndexOf("Message:") + 8);
        PlayerMessage playerMessage = new PlayerMessage()
        {
          Channel = (ChatType) int.Parse(s),
          Message = str3
        };
        this._state.DoChat(playerMessage.Message, playerMessage.Channel);
      }
    }
  }
}
