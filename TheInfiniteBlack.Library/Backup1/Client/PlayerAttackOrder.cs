﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.PlayerAttackOrder
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public abstract class PlayerAttackOrder
  {
    public abstract string Description { get; }

    public abstract Ship SelectTarget(IEnumerable<Ship> enemies);

    public abstract PlayerAttackOrder Next();
  }
}
