﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.SystemSettings
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class SystemSettings
  {
    public float CombatSound { get; set; }

    public float UiSound { get; set; }

    public float Music { get; set; }

    public bool Visuals { get; set; }

    public bool Background { get; set; }

    public bool CombatFlash { get; set; }

    public float UiScale { get; set; }

    public sbyte LastServerID { get; set; }

    public bool ShowTutorial { get; set; }

    public float DragSensitivity { get; set; }

    public bool RememberPassword { get; set; }

    public bool Tooltips { get; set; }

    public bool Gridlines { get; set; }

    public bool ShowPortraits { get; set; }

    public bool AllowPortraitScreenMode { get; set; }

    public SystemSettings()
    {
      this.Default();
    }

    public void Default()
    {
      this.CombatSound = 0.5f;
      this.UiSound = 0.5f;
      this.Music = 0.5f;
      this.Visuals = true;
      this.Background = true;
      this.CombatFlash = true;
      this.UiScale = 1.5f;
      this.LastServerID = (sbyte) 3;
      this.ShowTutorial = true;
      this.DragSensitivity = 0.0f;
      this.RememberPassword = true;
      this.Tooltips = true;
      this.Gridlines = true;
      this.ShowPortraits = true;
      this.AllowPortraitScreenMode = false;
    }
  }
}
