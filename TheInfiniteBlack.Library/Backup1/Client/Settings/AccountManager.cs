﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.AccountManager
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class AccountManager
  {
    private const string FileName_Accounts = "accounts.xml";
    private const string FileName_Settings = "settings.xml";

    public List<Account> Accounts { get; set; }

    public SystemSettings System { get; set; }

    public AccountManager()
    {
      this.Accounts = new List<Account>();
      this.System = new SystemSettings();
    }

    public void Load(IFileManager files)
    {
      AccountManager accountManager = XmlFileHelper.Load<AccountManager>(files, "accounts.xml");
      if (accountManager != null)
      {
        this.Accounts = accountManager.Accounts;
        this.System = accountManager.System;
      }
      foreach (Account account in this.Accounts)
      {
        AccountSettings accountSettings = XmlFileHelper.Load<AccountSettings>(files, account.Name + "_settings.xml");
        if (accountSettings != null)
        {
          account.Settings = accountSettings;
          if (accountSettings.Social != null)
            accountSettings.Social.PurgeDuplicates();
        }
      }
    }

    public void Save(IFileManager files)
    {
      XmlFileHelper.Save<AccountManager>(files, "accounts.xml", this);
      foreach (Account account in this.Accounts)
        XmlFileHelper.Save<AccountSettings>(files, account.Name + "_settings.xml", account.Settings);
    }

    public Account Get(Character character)
    {
      return this.Accounts.FirstOrDefault<Account>((Func<Account, bool>) (a => a.Characters.Contains(character)));
    }

    public Account Get(ClientPlayer player)
    {
      if (player == null)
        return (Account) null;
      Account account = this.Accounts.FirstOrDefault<Account>((Func<Account, bool>) (a => a.Name.Equals(player.Name, StringComparison.OrdinalIgnoreCase)));
      if (account == null)
      {
        account = new Account() { Name = player.Name };
        this.Accounts = new List<Account>((IEnumerable<Account>) this.Accounts)
        {
          account
        };
      }
      return account;
    }

    protected internal Account Refresh(ClientPlayer player, Ship ship, string password, sbyte serverId)
    {
      if (player == null || ship == null)
        return (Account) null;
      Account account = this.Get(player);
      ClientPlayer player1 = player;
      Ship ship1 = ship;
      string password1 = password;
      int num = (int) serverId;
      account.Refresh(player1, ship1, password1, (sbyte) num);
      return account;
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
