﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.StringFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;

namespace TheInfiniteBlack.Library.Client.Settings
{
  public class StringFilter : IEquatable<StringFilter>
  {
    private string _value = string.Empty;

    public string Value
    {
      get
      {
        return this._value;
      }
      set
      {
        this._value = value;
      }
    }

    public bool Equals(StringFilter other)
    {
      if (other == null)
        return false;
      if (this == other)
        return true;
      return string.Equals(this._value, other._value);
    }

    public override int GetHashCode()
    {
      if (this._value == null)
        return 0;
      return this._value.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      StringFilter other = obj as StringFilter;
      if (other != null)
        return this.Equals(other);
      return false;
    }

    public override string ToString()
    {
      return this._value;
    }
  }
}
