﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.GameEventsFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class GameEventsFilter
  {
    public CombatFilter Combat { get; set; }

    public ChatFilter Chat { get; set; }

    public MiscFilter Misc { get; set; }

    public GameEventsFilter()
    {
      this.Combat = new CombatFilter();
      this.Chat = new ChatFilter();
      this.Misc = new MiscFilter();
    }

    public void SetAll(bool value)
    {
      this.Combat.SetAll(value);
      this.Chat.SetAll(value);
      this.Misc.SetAll(value);
    }

    public void Default(ChatType type)
    {
      this.Combat.Default(type);
      this.Chat.Default(type);
      this.Misc.Default(type);
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
