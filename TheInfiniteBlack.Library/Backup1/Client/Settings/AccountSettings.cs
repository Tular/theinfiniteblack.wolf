﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.AccountSettings
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class AccountSettings
  {
    public EntityListSettings EntityList { get; set; }

    public GameEventsFilter EventLog { get; set; }

    public SocialSettings Social { get; set; }

    public AuctionWatchList AuctionWatch { get; set; }

    public AccountSettings()
    {
      this.EntityList = new EntityListSettings();
      this.EventLog = new GameEventsFilter();
      this.Social = new SocialSettings();
      this.AuctionWatch = new AuctionWatchList();
      this.Default();
    }

    public void Default()
    {
      this.EntityList.Default();
      this.EventLog.Default(ChatType.EVENT);
      this.Social.Default();
      this.AuctionWatch.Clear();
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
