﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.ChatFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class ChatFilter
  {
    public bool this[ChatType type]
    {
      get
      {
        switch (type)
        {
          case ChatType.SERVER:
            return this.Server;
          case ChatType.CORP:
            return this.Corp;
          case ChatType.SECTOR:
            return this.Sector;
          case ChatType.PRIVATE:
            return this.Private;
          case ChatType.ALLIANCE:
            return this.Alliance;
          case ChatType.EVENT:
            return this.Event;
          case ChatType.ALERT:
            return this.Alert;
          case ChatType.MARKET:
            return this.Market;
          case ChatType.MARKET_EVENT:
            return this.MarketEvent;
          case ChatType.UNIVERSE:
            return this.Universe;
          default:
            return true;
        }
      }
      set
      {
        switch (type)
        {
          case ChatType.SERVER:
            this.Server = value;
            break;
          case ChatType.CORP:
            this.Corp = value;
            break;
          case ChatType.SECTOR:
            this.Sector = value;
            break;
          case ChatType.PRIVATE:
            this.Private = value;
            break;
          case ChatType.ALLIANCE:
            this.Alliance = value;
            break;
          case ChatType.EVENT:
            this.Event = value;
            break;
          case ChatType.ALERT:
            this.Alert = value;
            break;
          case ChatType.MARKET:
            this.Market = value;
            break;
          case ChatType.MARKET_EVENT:
            this.MarketEvent = value;
            break;
          case ChatType.UNIVERSE:
            this.Universe = value;
            break;
        }
      }
    }

    public bool Server { get; set; }

    public bool Corp { get; set; }

    public bool Sector { get; set; }

    public bool Private { get; set; }

    public bool Alliance { get; set; }

    public bool Event { get; set; }

    public bool Alert { get; set; }

    public bool Market { get; set; }

    public bool MarketEvent { get; set; }

    public bool Universe { get; set; }

    public void SetAll(bool value)
    {
      this.Server = value;
      this.Corp = value;
      this.Sector = value;
      this.Private = value;
      this.Alliance = value;
      this.Event = value;
      this.Alert = value;
      this.Market = value;
      this.MarketEvent = value;
      this.Universe = value;
    }

    public void Default(ChatType type)
    {
      switch (type)
      {
        case ChatType.SERVER:
          this.SetAll(false);
          this.Alert = true;
          this.Private = true;
          this.Server = true;
          this.Corp = true;
          this.Sector = true;
          this.Alliance = true;
          break;
        case ChatType.CORP:
          this.SetAll(false);
          this.Alert = true;
          this.Private = true;
          this.Corp = true;
          break;
        case ChatType.SECTOR:
          this.SetAll(false);
          this.Alert = true;
          this.Private = true;
          this.Sector = true;
          break;
        case ChatType.ALLIANCE:
          this.SetAll(false);
          this.Alert = true;
          this.Private = true;
          this.Alliance = true;
          break;
        case ChatType.MARKET:
        case ChatType.MARKET_EVENT:
          this.SetAll(false);
          this.Alert = true;
          this.Private = true;
          this.Market = true;
          this.MarketEvent = true;
          break;
        case ChatType.UNIVERSE:
          this.SetAll(false);
          this.Alert = true;
          this.Private = true;
          this.Universe = true;
          break;
        default:
          this.SetAll(true);
          break;
      }
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
