﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.AttackFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class AttackFilter
  {
    public RelationFilter this[AttackEventType type]
    {
      get
      {
        switch (type)
        {
          case AttackEventType.MISS:
            return this.Miss;
          case AttackEventType.GRAZE:
            return this.Graze;
          case AttackEventType.HIT:
            return this.Hit;
          case AttackEventType.CRITICAL:
            return this.Critical;
          case AttackEventType.SPLASH:
            return this.Splash;
          case AttackEventType.REPAIR:
            return this.Repair;
          case AttackEventType.STUN:
            return this.Stun;
          case AttackEventType.COLLIDE:
            return this.Collide;
          case AttackEventType.DEFLECT:
            return this.Deflect;
          case AttackEventType.RAM:
            return this.Ram;
          case AttackEventType.GRAPPLE:
            return this.Grapple;
          case AttackEventType.DEGRAPPLE:
            return this.DeGrapple;
          default:
            return (RelationFilter) null;
        }
      }
    }

    public RelationFilter Miss { get; set; }

    public RelationFilter Graze { get; set; }

    public RelationFilter Hit { get; set; }

    public RelationFilter Critical { get; set; }

    public RelationFilter Splash { get; set; }

    public RelationFilter Repair { get; set; }

    public RelationFilter Stun { get; set; }

    public RelationFilter Collide { get; set; }

    public RelationFilter Deflect { get; set; }

    public RelationFilter Ram { get; set; }

    public RelationFilter Grapple { get; set; }

    public RelationFilter DeGrapple { get; set; }

    public AttackFilter()
    {
      this.DeGrapple = new RelationFilter();
      this.Grapple = new RelationFilter();
      this.Ram = new RelationFilter();
      this.Deflect = new RelationFilter();
      this.Collide = new RelationFilter();
      this.Stun = new RelationFilter();
      this.Repair = new RelationFilter();
      this.Splash = new RelationFilter();
      this.Critical = new RelationFilter();
      this.Hit = new RelationFilter();
      this.Graze = new RelationFilter();
      this.Miss = new RelationFilter();
    }

    public void SetAll(bool value)
    {
      this.Miss.SetAll(value);
      this.Graze.SetAll(value);
      this.Hit.SetAll(value);
      this.Critical.SetAll(value);
      this.Splash.SetAll(value);
      this.Repair.SetAll(value);
      this.Stun.SetAll(value);
      this.Collide.SetAll(value);
      this.Deflect.SetAll(value);
      this.Ram.SetAll(value);
      this.Grapple.SetAll(value);
      this.DeGrapple.SetAll(value);
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
