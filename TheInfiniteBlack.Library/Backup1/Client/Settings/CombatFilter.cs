﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.CombatFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class CombatFilter
  {
    public UnitFilter Units { get; set; }

    public AttackFilter Attacks { get; set; }

    public CombatFilter()
    {
      this.Units = new UnitFilter();
      this.Attacks = new AttackFilter();
    }

    public void SetAll(bool value)
    {
      this.Units.SetAll(value);
      this.Attacks.SetAll(value);
    }

    public void Default(ChatType type)
    {
      if (type == ChatType.EVENT)
        this.SetAll(true);
      else
        this.SetAll(false);
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
