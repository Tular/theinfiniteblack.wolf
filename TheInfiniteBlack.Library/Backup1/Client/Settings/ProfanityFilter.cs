﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.ProfanityFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Text;

namespace TheInfiniteBlack.Library.Client.Settings
{
  public static class ProfanityFilter
  {
    private static readonly string[] _filter = new string[35]
    {
      "fuck",
      "fuk",
      "shit",
      "cock",
      "cok",
      "whore",
      "niger",
      "nigg",
      "niga",
      "niq",
      "ni6",
      "cunt",
      "bitch",
      "cum",
      "dick",
      "dildo",
      "fag",
      "homo",
      "jiz",
      "jis",
      "kike",
      "nazi",
      "hitler",
      "penis",
      "vagina",
      "pusy",
      "pussy",
      "pussie",
      "pusie",
      "gook",
      "chink",
      "shit",
      "slut",
      "snatch",
      "twat"
    };
    private static readonly string[] _stars = new string[11]
    {
      "",
      "*",
      "**",
      "***",
      "****",
      "*****",
      "******",
      "*******",
      "********",
      "*********",
      "**********"
    };

    public static string Clean(string str)
    {
      if (string.IsNullOrEmpty(str))
        return str;
      StringBuilder stringBuilder = new StringBuilder(str);
      foreach (string str1 in ProfanityFilter._filter)
      {
        for (int index = str.IndexOf(str1, StringComparison.OrdinalIgnoreCase); index >= 0; index = str.IndexOf(str1, index + str1.Length, StringComparison.OrdinalIgnoreCase))
        {
          stringBuilder.Remove(index, str1.Length);
          stringBuilder.Insert(index, ProfanityFilter._stars[str1.Length]);
        }
      }
      return stringBuilder.ToString();
    }
  }
}
