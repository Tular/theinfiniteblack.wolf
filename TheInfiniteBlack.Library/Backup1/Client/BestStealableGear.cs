﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.BestStealableGear
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client
{
  public class BestStealableGear : PlayerAttackOrder
  {
    public override string Description
    {
      get
      {
        return "Best Stealable Gear";
      }
    }

    public override Ship SelectTarget(IEnumerable<Ship> enemies)
    {
      return enemies.Where<Ship>((Func<Ship, bool>) (x => x.AllEquippedItems.Count<EquipmentItem>((Func<EquipmentItem, bool>) (y => (int) y.Durability < 25)) > 0)).OrderByDescending<Ship, int>((Func<Ship, int>) (x => x.AllEquippedItems.Where<EquipmentItem>((Func<EquipmentItem, bool>) (i => (int) i.Durability < 25)).OrderByDescending<EquipmentItem, int>((Func<EquipmentItem, int>) (i => i.ActualCreditValue)).First<EquipmentItem>().ActualCreditValue)).First<Ship>();
    }

    public override PlayerAttackOrder Next()
    {
      return (PlayerAttackOrder) new LowestHull();
    }
  }
}
