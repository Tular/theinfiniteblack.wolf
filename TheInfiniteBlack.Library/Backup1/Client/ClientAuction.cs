﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientAuction
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client
{
  public class ClientAuction : NamedClientObject
  {
    private readonly int _id;
    private readonly TimeComponent _time;
    private EquipmentItem _item;
    private ClientPlayer _owner;
    private int _bid;
    private int _buyoutCredits;
    private ClientPlayer _bidder;

    public int ID
    {
      get
      {
        return this._id;
      }
    }

    public TimeComponent Time
    {
      get
      {
        return this._time;
      }
    }

    public sbyte ServerID
    {
      get
      {
        return this.State.Login.ServerID;
      }
    }

    public EquipmentItem Item
    {
      get
      {
        return this._item;
      }
      set
      {
        if (this._item == value)
          return;
        this._item = value;
        this.OnChanged();
      }
    }

    public ClientPlayer Owner
    {
      get
      {
        return this._owner;
      }
      set
      {
        if (this._owner == value)
          return;
        this._owner = value;
        this.OnChanged();
      }
    }

    public int Bid
    {
      get
      {
        return this._bid;
      }
      set
      {
        if (this._bid == value)
          return;
        this._bid = value;
        this.OnChanged();
      }
    }

    public int NextBid
    {
      get
      {
        if (this._bidder != null)
          return this._bid + this._bid / 10;
        return this._bid;
      }
    }

    public int BuyoutCredits
    {
      get
      {
        return this._buyoutCredits;
      }
      set
      {
        if (this._buyoutCredits == value)
          return;
        this._buyoutCredits = value;
        this.OnChanged();
      }
    }

    public int BuyoutBlackDollars
    {
      get
      {
        if (this._buyoutCredits > 0)
          return EquipmentItem.GetBlackDollarValue((float) this._buyoutCredits);
        return 0;
      }
    }

    public ClientPlayer Bidder
    {
      get
      {
        return this._bidder;
      }
      set
      {
        if (this._bidder == value)
          return;
        this._bidder = value;
        this.OnChanged();
      }
    }

    public bool IsWatched
    {
      get
      {
        if (this.State != null && this.State.MySettings != null)
          return this.State.MySettings.AuctionWatch.Contains(this);
        return false;
      }
      set
      {
        if (this.State == null || this.State.MySettings == null)
          return;
        if (value)
          this.State.MySettings.AuctionWatch.Add(this);
        else
          this.State.MySettings.AuctionWatch.Remove(this);
      }
    }

    public bool CanBid
    {
      get
      {
        if (this._owner == this.State.MyPlayer || this._bidder == this.State.MyPlayer)
          return false;
        int nextBid = this.NextBid;
        return (this._buyoutCredits <= 0 || nextBid < this._buyoutCredits) && nextBid <= this.State.Bank.Credits;
      }
    }

    public bool CanCancel
    {
      get
      {
        return this._owner == this.State.MyPlayer;
      }
    }

    public ClientAuction(IGameState state, int id)
      : base(state)
    {
      this._time = new TimeComponent(state);
      this._id = id;
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (this._item == null)
        return;
      this._item.AppendLongName(sb, markup, true);
    }

    public void DoBid()
    {
      this.State.DoAuctionBid(this);
    }

    public bool CanBuyout(CurrencyType currency)
    {
      if (currency != CurrencyType.Credits)
      {
        if (currency != CurrencyType.BlackDollars)
          return false;
        int buyoutBlackDollars = this.BuyoutBlackDollars;
        if (buyoutBlackDollars <= 0 || buyoutBlackDollars > this.State.Bank.BlackDollars)
          return false;
        return this._owner != this.State.MyPlayer;
      }
      if (this._buyoutCredits <= 0 || this._owner == this.State.MyPlayer)
        return false;
      return this._buyoutCredits <= this.State.Bank.Credits;
    }

    public void DoBuyout(CurrencyType currency)
    {
      this.State.DoAuctionBuyout(this, currency);
    }

    public void DoCancel()
    {
      this.State.DoAuctionCancel(this);
    }
  }
}
