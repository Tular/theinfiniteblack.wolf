﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.CourseController
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client
{
  public class CourseController
  {
    private List<Sector> _values = new List<Sector>();
    private readonly IGameState _state;
    private int _lastChangeFlagId;
    private bool _paused;

    public int LastChangeFlagID
    {
      get
      {
        return this._lastChangeFlagId;
      }
    }

    public int Count
    {
      get
      {
        return this._values.Count;
      }
    }

    public IEnumerable<Sector> Values
    {
      get
      {
        return (IEnumerable<Sector>) this._values;
      }
    }

    public Sector Destination
    {
      get
      {
        return this._values.LastOrDefault<Sector>();
      }
    }

    public Sector Next
    {
      get
      {
        return this._values.FirstOrDefault<Sector>();
      }
    }

    public bool Paused
    {
      get
      {
        return this._paused;
      }
    }

    public CourseController(IGameState state)
    {
      this._state = state;
    }

    public bool IsInCourse(Sector sector)
    {
      return this._values.Contains(sector);
    }

    protected internal void OnUpdate()
    {
      Ship myShip = this._state.MyShip;
      Sector myLoc = this._state.MyLoc;
      Sector sector = myLoc;
      List<Sector> course = (List<Sector>) null;
      foreach (Sector target in this._values)
      {
        if (!sector.IsConnected(target))
        {
          if (course == null)
            course = new List<Sector>((IEnumerable<Sector>) this._values);
          course.Remove(target);
        }
        else
          sector = target;
      }
      if (course != null)
        this.Set(course);
      else if (this._values.Count == 0)
      {
        this.Pause();
      }
      else
      {
        if (this._paused || !this._state.MoveCooldown.IsFinished)
          return;
        Sector target = this._values[0];
        Direction direction = myLoc.GetDirection(target);
        if (direction == Direction.None)
        {
          this.Reset();
        }
        else
        {
          int moveSpeedMs = myShip.MoveSpeedMS;
          if (this._state.Buyables.HyperDrive > 0 && !this._state.Buyables.SuspendHyperDrive)
            moveSpeedMs /= 2;
          this._state.MoveCooldown.Set((long) moveSpeedMs);
          this._state.Net.Send(RequestMove.Create(direction));
        }
      }
    }

    public void Set(Sector target)
    {
      Sector myLoc = this._state.MyLoc;
      if (myLoc != null && target != null)
        this.Set(this._state.Map.GetPath(myLoc, target));
      else
        this.Reset();
      this._state.Play(SoundEffectType.ui_button, 0.9f);
    }

    public void Set(int x, int y)
    {
      Sector myLoc = this._state.MyLoc;
      if (myLoc != null)
        this.Set(this._state.Map.Get(myLoc.Rift, x - 1, y - 1));
      else
        this.Reset();
    }

    public void Reset()
    {
      if (this._values.Count <= 0)
        return;
      this.Set(new List<Sector>());
    }

    public void Pause()
    {
      if (this._paused)
        return;
      this._paused = true;
      this._state.UI.Show(new MovementStatusChangedEventArgs(this._state, false));
      this._state.Play(SoundEffectType.ui_dialog_collapse, 1.15f);
    }

    public void Resume()
    {
      if (this._values.Count == 0)
      {
        this.Pause();
      }
      else
      {
        this._paused = false;
        this._state.UI.Show(new MovementStatusChangedEventArgs(this._state, true));
        this._state.Play(SoundEffectType.ui_dialog_expand, 1.15f);
      }
    }

    private void Set(List<Sector> course)
    {
      this._state.MyLoc.OnChanged();
      this._values.ForEach((Action<Sector>) (s => s.OnChanged()));
      course.ForEach((Action<Sector>) (s => s.OnChanged()));
      this._values = course;
      this.OnChanged();
      this._state.UI.Show(new CourseSetEventArgs(this._state, this._values));
      if (this._values.Count != 0)
        return;
      this.Pause();
    }

    protected internal void OnChanged()
    {
      this._lastChangeFlagId = this._lastChangeFlagId + 1;
    }
  }
}
