﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.PopupWindowEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class PopupWindowEventArgs : GameEventArgs
  {
    private readonly string _text;
    private readonly bool _canClose;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.PopupWindow;
      }
    }

    public string Text
    {
      get
      {
        return this._text;
      }
    }

    public bool CanClose
    {
      get
      {
        return this._canClose;
      }
    }

    public PopupWindowEventArgs(IGameState state, string text, bool canClose)
      : base(state)
    {
      this._text = text;
      this._canClose = canClose;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append(this._text);
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return true;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
