﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.LoginSuccessEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class LoginSuccessEventArgs : GameEventArgs
  {
    private readonly ClientPlayer _myPlayer;
    private readonly Ship _myShip;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.LoginSuccess;
      }
    }

    public ClientPlayer MyPlayer
    {
      get
      {
        return this._myPlayer;
      }
    }

    public Ship MyShip
    {
      get
      {
        return this._myShip;
      }
    }

    public LoginSuccessEventArgs(IGameState state, ClientPlayer myPlayer, Ship myShip)
      : base(state)
    {
      this._myPlayer = myPlayer;
      this._myShip = myShip;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append("Logged in as ");
      this._myPlayer.AppendName(sb, true);
      sb.Append(" (");
      this._myShip.AppendName(sb, true);
      sb.Append(")");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return true;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
