﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.EventSoundEffect
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Client.Events
{
  public class EventSoundEffect
  {
    private readonly SoundEffectType _type;
    private readonly float _rate;
    private readonly float _delay;

    public SoundEffectType Type
    {
      get
      {
        return this._type;
      }
    }

    public float Rate
    {
      get
      {
        return this._rate;
      }
    }

    public float Delay
    {
      get
      {
        return this._delay;
      }
    }

    public EventSoundEffect(SoundEffectType type, float rate, float delay)
    {
      this._type = type;
      this._rate = rate;
      this._delay = delay;
    }

    public EventSoundEffect(SoundEffectType type, float rate)
      : this(type, rate, 0.0f)
    {
    }

    public EventSoundEffect(SoundEffectType type)
      : this(type, 1f, 0.0f)
    {
    }
  }
}
