﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.MoneyChangeEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class MoneyChangeEventArgs : GameEventArgs
  {
    private readonly Entity _source;
    private readonly int _credits;
    private readonly int _blackDollars;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.MoneyChanged;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public int Credits
    {
      get
      {
        return this._credits;
      }
    }

    public int BlackDollars
    {
      get
      {
        return this._blackDollars;
      }
    }

    public MoneyChangeEventArgs(IGameState state, Entity source, int credits, int blackDollars)
      : base(state)
    {
      this._source = source;
      this._credits = credits;
      this._blackDollars = blackDollars;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      if (this._credits != 0)
      {
        sb.Append(this._credits >= 0 ? " +$" : " -$");
        sb.Append(Math.Abs(this._credits).ToString("#,#"));
        sb.Append(" Credits");
        if (this._blackDollars != 0)
          sb.Append(" |");
      }
      if (this._blackDollars == 0)
        return;
      sb.Append(this._blackDollars >= 0 ? " +" : " -");
      sb.Append(Math.Abs(this._blackDollars).ToString("#,#"));
      sb.Append(" BlackDollars");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.Gains[this._source.Relation];
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if (this._source != this.State.MyShip || (double) systemSettings.UiSound <= 1.0000000116861E-07 && (double) systemSettings.CombatSound <= 1.0000000116861E-07)
        return (EventSoundEffect) null;
      return new EventSoundEffect(SoundEffectType.ui_credits, this._blackDollars != 0 ? 0.9f : 1f);
    }
  }
}
