﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.EntityExitEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class EntityExitEventArgs : GameEventArgs
  {
    private readonly Entity _source;
    private readonly EntityExitType _exitType;
    private readonly Direction _dir;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.EntityExit;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public EntityExitType ExitType
    {
      get
      {
        return this._exitType;
      }
    }

    public Direction Dir
    {
      get
      {
        return this._dir;
      }
    }

    public EntityExitEventArgs(IGameState state, Entity source, EntityExitType exitType, Direction dir)
      : base(state)
    {
      this._source = source;
      this._exitType = exitType;
      this._dir = dir;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      switch (this._exitType)
      {
        case EntityExitType.SILENT:
          if (this._dir != Direction.None)
          {
            sb.Append(" exits ");
            sb.Append(this._dir.Name());
            sb.Append(" (Silent).");
            break;
          }
          sb.Append(" exits (Silent).");
          break;
        case EntityExitType.DESTROYED:
          sb.Append(" was destroyed!");
          break;
        case EntityExitType.MOVED:
          if (this._dir != Direction.None)
          {
            sb.Append(" jumped ");
            sb.Append(this._dir.Name());
            sb.Append(".");
            break;
          }
          sb.Append(" slips away into the void!");
          break;
        case EntityExitType.EARTH_JUMP:
        case EntityExitType.CORP_JUMP:
        case EntityExitType.RIFT_JUMP:
          sb.Append(" escapes through a wormhole!");
          break;
        case EntityExitType.LOGOUT:
          sb.Append(" logged out.");
          break;
        case EntityExitType.DRAGGED:
          if (this._dir != Direction.None)
          {
            sb.Append(" was dragged ");
            sb.Append(this._dir.Name());
            sb.Append("!");
            break;
          }
          sb.Append(" was dragged into the void!");
          break;
      }
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      switch (this._exitType)
      {
        case EntityExitType.SILENT:
          if (filter.Misc.Debug)
            return filter.Misc.Departures[this._source.Relation];
          return false;
        case EntityExitType.MOVED:
          switch (this._source.Type)
          {
            case EntityType.SHIP:
            case EntityType.NPC:
              return filter.Misc.Departures[this._source.Relation];
            case EntityType.FIGHTER:
            case EntityType.REPAIR_DRONE:
              if (filter.Misc.Debug)
                return filter.Misc.Departures[this._source.Relation];
              return false;
          }
      }
      return !filter.Misc.Departures.Never;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if ((double) systemSettings.CombatSound > 1.0000000116861E-07)
      {
        switch (this._exitType)
        {
          case EntityExitType.DESTROYED:
            return SoundHelper.GetDeathSound(this._source);
          case EntityExitType.MOVED:
            return SoundHelper.GetMoveSound(this._source);
          case EntityExitType.EARTH_JUMP:
          case EntityExitType.CORP_JUMP:
          case EntityExitType.RIFT_JUMP:
            return new EventSoundEffect(SoundEffectType.earth_jump);
        }
      }
      return (EventSoundEffect) null;
    }
  }
}
