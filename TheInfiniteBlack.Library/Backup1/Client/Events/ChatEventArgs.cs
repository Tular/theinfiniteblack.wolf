﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.ChatEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class ChatEventArgs : GameEventArgs
  {
    private static string _lastLog = "";
    private DateTime _lastHitTime = DateTime.MinValue;
    private readonly string _receiver;
    private readonly string _sender;
    private readonly ChatType _type;
    private string _text;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.Chat;
      }
    }

    public string Text
    {
      get
      {
        return this._text;
      }
    }

    public string Receiver
    {
      get
      {
        return this._receiver;
      }
    }

    public string Sender
    {
      get
      {
        return this._sender;
      }
    }

    public ChatType Type
    {
      get
      {
        return this._type;
      }
    }

    public ChatEventArgs(IGameState state, string text, string receiver, string sender, ChatType type)
      : base(state)
    {
      this._text = text;
      this._receiver = receiver;
      this._sender = sender;
      this._type = type;
    }

    public override void AppendText(StringBuilder sb)
    {
      if (GameSettings.IsModerator(this._sender))
      {
        sb.Append("[o]");
        sb.Append("[MOD]");
        sb.Append("[g]");
      }
      else
        sb.Append(this._type.GetMarkup());
      if (this.State.MySettings != null && this.State.MySettings.Social.FilterProfanity)
      {
        switch (this._type)
        {
          case ChatType.CONSOLE:
          case ChatType.EVENT:
          case ChatType.ALERT:
          case ChatType.MARKET_EVENT:
            break;
          default:
            this._text = ProfanityFilter.Clean(this._text);
            goto case ChatType.CONSOLE;
        }
      }
      if (this.State.TimeStamp)
        sb.Append(this.Time.ToString("(HH:mm)"));
      switch (this._type)
      {
        case ChatType.SERVER:
          sb.Append("[SERVER] ");
          sb.Append(this._sender);
          sb.Append(": ");
          sb.Append(this._text);
          break;
        case ChatType.CORP:
          sb.Append("[CORP] ");
          sb.Append(this._sender);
          sb.Append(": ");
          sb.Append(this._text);
          break;
        case ChatType.SECTOR:
          sb.Append("[SECTOR] ");
          sb.Append(this._sender);
          sb.Append(": ");
          sb.Append(this._text);
          break;
        case ChatType.PRIVATE:
          sb.Append(this._sender);
          sb.Append("->");
          sb.Append(this._receiver);
          sb.Append(": ");
          sb.Append(this._text);
          break;
        case ChatType.ALLIANCE:
          sb.Append("[ALLIANCE] ");
          sb.Append(this._sender);
          sb.Append(": ");
          sb.Append(this._text);
          break;
        case ChatType.CONSOLE:
          sb.Append("[SERVER]: ");
          sb.Append(this._text);
          break;
        case ChatType.EVENT:
        case ChatType.ALERT:
          sb.Append(this._text);
          break;
        case ChatType.MARKET:
        case ChatType.MARKET_EVENT:
          sb.Append("[MARKET] ");
          sb.Append(this._sender);
          sb.Append(": ");
          sb.Append(this._text);
          break;
        case ChatType.UNIVERSE:
          sb.Append("[");
          sb.Append(this._receiver);
          sb.Append("] ");
          sb.Append(this._sender);
          sb.Append(": ");
          sb.Append(this._text);
          break;
      }
      if (this._type == ChatType.PRIVATE && GameState.IsOnDiscord && DiscordBot.WebHooks.ContainsKey(this._receiver))
        DiscordBot.SendPM(new DiscordMessage()
        {
          webook = DiscordBot.WebHooks[this._receiver],
          icon_url = "",
          text = this._text,
          username = this._sender
        });
      if (this._text.IndexOf("location revealed on map") > -1)
      {
        foreach (string str in this.State.Accounts.Accounts.Select<Account, string>((Func<Account, string>) (x => x.Name)))
        {
          if (this._text.IndexOf(str) > -1)
            this.State.Panic = true;
        }
      }
      if (this.State.RecordKills && this._text.IndexOf("killed by") > 0 && (this._text.IndexOf("BAMF") > 0 && this._text.IndexOf("HITSQUAD") > 0) && this.State.LastKillString != this._text)
      {
        if (this._text.IndexOf("BAMF") > this._text.IndexOf("HITSQUAD"))
          ++this.State.KillsAgainst;
        if (this._text.IndexOf("HITSQUAD") > this._text.IndexOf("BAMF"))
          ++this.State.KillsFor;
        this.State.LastKillString = this._text;
        this.State.ShowAlert(string.Format("<b>[g]{0} - {1}", (object) this.State.KillsFor, (object) this.State.KillsAgainst));
      }
      if (!GameState.DoLog || !(GameState.LastLog != sb.ToString()))
        return;
      this.Log(sb);
      GameState.LastLog = sb.ToString();
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      switch (this._type)
      {
        case ChatType.CONSOLE:
        case ChatType.EVENT:
        case ChatType.ALERT:
        case ChatType.MARKET_EVENT:
          return filter.Chat[this._type];
        default:
          if (this.State.MySettings != null && this.State.MySettings.Social.IsIgnore(this._sender))
            return false;
          return filter.Chat[this._type];
      }
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if (accountSettings != null)
      {
        SocialSettings social = accountSettings.Social;
        if ((this.IsVisible(accountSettings.EventLog) || this.IsVisible(social.UniverseChat) || (this.IsVisible(social.ServerChat) || this.IsVisible(social.SectorChat)) || (this.IsVisible(social.CorpChat) || this.IsVisible(social.AllianceChat) || this.IsVisible(social.MarketChat)) ? 1 : (this.IsVisible(social.EventsChat) ? 1 : 0)) == 0)
          return (EventSoundEffect) null;
      }
      if ((double) systemSettings.UiSound > 1.0000000116861E-07)
      {
        switch (this._type)
        {
          case ChatType.SERVER:
            return new EventSoundEffect(SoundEffectType.ui_chat, 1f);
          case ChatType.CORP:
            return new EventSoundEffect(SoundEffectType.ui_chat, 0.96f);
          case ChatType.SECTOR:
            return new EventSoundEffect(SoundEffectType.ui_chat, 0.94f);
          case ChatType.PRIVATE:
            return new EventSoundEffect(SoundEffectType.ui_chat, 0.92f);
          case ChatType.ALLIANCE:
            return new EventSoundEffect(SoundEffectType.ui_chat, 0.98f);
          case ChatType.CONSOLE:
            return new EventSoundEffect(SoundEffectType.ui_info, 0.8f);
          case ChatType.EVENT:
            return new EventSoundEffect(SoundEffectType.ui_chat, 0.88f);
          case ChatType.ALERT:
            return new EventSoundEffect(SoundEffectType.ui_chat, 0.86f);
          case ChatType.MARKET:
            return new EventSoundEffect(SoundEffectType.ui_chat, 1.02f);
          case ChatType.MARKET_EVENT:
            return new EventSoundEffect(SoundEffectType.ui_chat, 1.03f);
          case ChatType.UNIVERSE:
            return new EventSoundEffect(SoundEffectType.ui_chat, 1.01f);
        }
      }
      return (EventSoundEffect) null;
    }
  }
}
