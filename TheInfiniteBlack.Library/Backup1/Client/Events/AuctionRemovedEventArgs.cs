﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.AuctionRemovedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class AuctionRemovedEventArgs : GameEventArgs
  {
    private readonly ClientAuction _auction;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.AuctionRemoved;
      }
    }

    public ClientAuction Auction
    {
      get
      {
        return this._auction;
      }
    }

    public AuctionRemovedEventArgs(IGameState state, ClientAuction auction)
      : base(state)
    {
      this._auction = auction;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append("Auction Closed! ");
      this._auction.AppendName(sb, true);
      sb.Append(" - Seller: ");
      if (this._auction.Owner != null)
        this._auction.Owner.AppendName(sb, false);
      sb.Append(" - Bid: $");
      StringBuilder stringBuilder1 = sb;
      int num = this._auction.Bid;
      string str1 = num.ToString("#,#");
      stringBuilder1.Append(str1);
      if (this._auction.BuyoutCredits <= 0)
        return;
      sb.Append(" - Buyout: $");
      StringBuilder stringBuilder2 = sb;
      num = this._auction.BuyoutCredits;
      string str2 = num.ToString("#,#");
      stringBuilder2.Append(str2);
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.AllAuctions || filter.Misc.MyAuctions && (this._auction.Owner != null && this._auction.Owner.Relation == RelationType.SELF || this._auction.Bidder != null && this._auction.Bidder.Relation == RelationType.SELF);
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
