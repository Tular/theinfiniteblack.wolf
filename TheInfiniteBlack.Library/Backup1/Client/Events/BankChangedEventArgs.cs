﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.BankChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class BankChangedEventArgs : GameEventArgs
  {
    private readonly List<EquipmentItem> _items;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.BankChanged;
      }
    }

    public List<EquipmentItem> Items
    {
      get
      {
        return this._items;
      }
    }

    public BankChangedEventArgs(IGameState state, List<EquipmentItem> items)
      : base(state)
    {
      this._items = items;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append("Bank Changed (");
      sb.Append(this._items.Count);
      sb.Append(" Items)");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.Debug;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
