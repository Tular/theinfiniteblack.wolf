﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.GameEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.IO;
using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public abstract class GameEventArgs : EventArgs
  {
    private static DateTime NineteenSeventy = new DateTime(1970, 1, 1, 0, 0, 0);
    private readonly int _eventOrderID = GameEventArgs._orderInc++;
    private readonly DateTime _time = DateTime.Now;
    private static int _orderInc;
    private readonly IGameState _state;

    public abstract GameEventType EventType { get; }

    public DateTime Time
    {
      get
      {
        return this._time;
      }
    }

    public int EventOrderID
    {
      get
      {
        return this._eventOrderID;
      }
    }

    public IGameState State
    {
      get
      {
        return this._state;
      }
    }

    protected GameEventArgs(IGameState state)
    {
      this._state = state;
    }

    public abstract void AppendText(StringBuilder sb);

    public abstract bool IsVisible(GameEventsFilter filter);

    public EventSoundEffect GetSound(SystemSettings systemSettings)
    {
      return this.GetSound(systemSettings, (AccountSettings) null);
    }

    public abstract EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings);

    public void Log(StringBuilder sb)
    {
      string str1 = "[" + (object) (int) (this.Time - GameEventArgs.NineteenSeventy).TotalSeconds + " " + this.Time.ToString("dd/M/yyyy HH:mm:ss") + "] " + sb.ToString();
      try
      {
        string str2 = "tib";
        if (this._state.MyPlayer != null && !string.IsNullOrEmpty(this._state.MyPlayer.Name))
          str2 = this._state.MyPlayer.Name;
        using (StreamWriter streamWriter = File.AppendText(string.Format("C:\\logs\\{0}.txt", (object) str2)))
          streamWriter.WriteLine(str1);
      }
      catch (Exception ex)
      {
      }
    }

    public override string ToString()
    {
      StringBuilder sb = new StringBuilder(1000);
      this.AppendText(sb);
      return sb.ToString();
    }
  }
}
