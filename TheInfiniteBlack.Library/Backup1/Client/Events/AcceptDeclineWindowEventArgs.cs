﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.AcceptDeclineWindowEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Network;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class AcceptDeclineWindowEventArgs : GameEventArgs
  {
    private readonly string _title;
    private readonly string _text;
    private readonly INetworkData _accept;
    private readonly INetworkData _decline;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.AcceptDeclineWindow;
      }
    }

    public string Title
    {
      get
      {
        return this._title;
      }
    }

    public string Text
    {
      get
      {
        return this._text;
      }
    }

    public AcceptDeclineWindowEventArgs(IGameState state, string title, string text, INetworkData accept, INetworkData decline)
      : base(state)
    {
      this._title = title;
      this._text = text;
      this._accept = accept;
      this._decline = decline;
    }

    public void Accept()
    {
      if (this._accept == null)
        return;
      this.State.Net.Send(this._accept);
    }

    public void Decline()
    {
      if (this._decline == null)
        return;
      this.State.Net.Send(this._decline);
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append(this._text);
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.NonCombat[RelationType.SELF];
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
