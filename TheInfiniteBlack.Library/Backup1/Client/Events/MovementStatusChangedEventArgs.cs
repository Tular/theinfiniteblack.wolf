﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.MovementStatusChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class MovementStatusChangedEventArgs : GameEventArgs
  {
    private readonly bool _isMoving;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.MovementChanged;
      }
    }

    public bool IsMoving
    {
      get
      {
        return this._isMoving;
      }
    }

    public MovementStatusChangedEventArgs(IGameState state, bool isMoving)
      : base(state)
    {
      this._isMoving = isMoving;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append(this._isMoving ? "Movement Resumed" : "Movement Paused");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.MovementStatus;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
