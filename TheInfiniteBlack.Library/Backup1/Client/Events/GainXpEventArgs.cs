﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.GainXpEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class GainXpEventArgs : GameEventArgs
  {
    private readonly Ship _source;
    private readonly int _xp;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.GainXp;
      }
    }

    public Ship Source
    {
      get
      {
        return this._source;
      }
    }

    public int XP
    {
      get
      {
        return this._xp;
      }
    }

    public GainXpEventArgs(IGameState state, Ship source, int xp)
      : base(state)
    {
      this._source = source;
      this._xp = xp;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      sb.Append(" +");
      sb.Append(this._xp.ToString("#,#"));
      sb.Append(" XP");
      if (this._source != this.State.MyShip || this.State.Buyables.MindSurge <= 0 || this.State.Buyables.SuspendMindSurge)
        return;
      sb.Append(" (");
      sb.Append(this.State.Buyables.MindSurge.ToString("#,0"));
      sb.Append(" Mind Surge remaining)");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.Gains[this._source.Relation];
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
