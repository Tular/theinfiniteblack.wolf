﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.EntityArriveEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class EntityArriveEventArgs : GameEventArgs
  {
    private readonly Entity _source;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.EntityEnter;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public EntityArriveEventArgs(IGameState state, Entity source)
      : base(state)
    {
      this._source = source;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      sb.Append(this._source is CargoEntity ? " was dropped!" : " arrived!");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      switch (this._source.Type)
      {
        case EntityType.SHIP:
        case EntityType.NPC:
          return filter.Misc.Arrivals[this._source.Relation];
        case EntityType.FIGHTER:
        case EntityType.REPAIR_DRONE:
          if (filter.Misc.Debug)
            return filter.Misc.Arrivals[this._source.Relation];
          return false;
        default:
          return !filter.Misc.Arrivals.Never;
      }
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if ((double) systemSettings.UiSound > 1.0000000116861E-07)
        return SoundHelper.GetMoveSound(this._source);
      return (EventSoundEffect) null;
    }
  }
}
