﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.GainItemEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class GainItemEventArgs : GameEventArgs
  {
    private readonly Entity _source;
    private readonly EquipmentItem _item;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.GainItem;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public EquipmentItem Item
    {
      get
      {
        return this._item;
      }
    }

    public GainItemEventArgs(IGameState state, Entity source, EquipmentItem item)
      : base(state)
    {
      this._source = source;
      this._item = item;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      sb.Append(" +");
      this._item.AppendLongName(sb, true, false);
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.Gains[this._source.Relation];
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
