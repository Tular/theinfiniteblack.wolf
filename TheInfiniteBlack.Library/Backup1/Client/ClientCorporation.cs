﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientCorporation
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client
{
  public class ClientCorporation : NamedClientObject
  {
    private readonly Technology _tech = new Technology();
    private HashSet<ClientPlayer> _members = new HashSet<ClientPlayer>();
    private string _name = "Unknown?";
    private readonly short _id;
    private ClientAlliance _alliance;
    private Sector _location;
    private sbyte _garrisonRank;
    private byte _shipColor;
    private bool[] _requestedRoster;

    public short ID
    {
      get
      {
        return this._id;
      }
    }

    public ClientAlliance Alliance
    {
      get
      {
        return this._alliance;
      }
      set
      {
        if (this._alliance == value)
          return;
        if (this._alliance != null)
          this._alliance.Remove(this);
        this._alliance = value;
        if (this._alliance != null)
          this._alliance.Add(this);
        this.OnChanged();
      }
    }

    public sbyte GarrisonRank
    {
      get
      {
        return this._garrisonRank;
      }
      set
      {
        if ((int) this._garrisonRank == (int) value)
          return;
        this._garrisonRank = value;
        this.OnChanged();
      }
    }

    public byte ShipColor
    {
      get
      {
        return this._shipColor;
      }
      set
      {
        if ((int) this._shipColor == (int) value)
          return;
        this._shipColor = value;
        this.OnChanged();
      }
    }

    public bool IsAllianceLeader
    {
      get
      {
        if (this._alliance != null)
          return this._alliance.Leader == this;
        return false;
      }
    }

    public Sector Location
    {
      get
      {
        return this._location;
      }
    }

    public Technology Tech
    {
      get
      {
        return this._tech;
      }
    }

    public IEnumerable<ClientPlayer> Members
    {
      get
      {
        return (IEnumerable<ClientPlayer>) this._members;
      }
    }

    public int MemberCount
    {
      get
      {
        return this._members.Count;
      }
    }

    public ClientCorporation(IGameState state, short id)
      : base(state)
    {
      this._id = id;
    }

    public IEnumerable<ClientPlayer> GetRoster(CorpRankType rank, bool includeOffline)
    {
      if (this._requestedRoster == null)
        this._requestedRoster = new bool[6];
      if (!this._requestedRoster[(int) rank])
      {
        this._requestedRoster[(int) rank] = true;
        this.State.Net.Send(RequestCorpRoster.Create(this._id, rank));
      }
      if (includeOffline)
        return (IEnumerable<ClientPlayer>) this._members.Where<ClientPlayer>((Func<ClientPlayer, bool>) (m => m.Rank == rank)).OrderBy<ClientPlayer, bool>((Func<ClientPlayer, bool>) (m => !m.Online)).ThenBy<ClientPlayer, string>((Func<ClientPlayer, string>) (m => m.Name));
      return (IEnumerable<ClientPlayer>) this._members.Where<ClientPlayer>((Func<ClientPlayer, bool>) (m =>
      {
        if (m.Rank == rank)
          return m.Online;
        return false;
      })).OrderBy<ClientPlayer, string>((Func<ClientPlayer, string>) (m => m.Name));
    }

    public bool Contains(ClientPlayer player)
    {
      return this._members.Contains(player);
    }

    protected internal void Add(ClientPlayer player)
    {
      if (this._members.Contains(player))
        return;
      this._members = new HashSet<ClientPlayer>((IEnumerable<ClientPlayer>) this._members)
      {
        player
      };
      this.OnChanged();
    }

    protected internal void Remove(ClientPlayer player)
    {
      if (!this._members.Contains(player))
        return;
      HashSet<ClientPlayer> clientPlayerSet = new HashSet<ClientPlayer>((IEnumerable<ClientPlayer>) this._members);
      clientPlayerSet.Remove(player);
      this._members = clientPlayerSet;
      this.OnChanged();
    }

    protected internal void SetLocation(Sector location)
    {
      Sector location1 = this._location;
      this._location = (Sector) null;
      if (location1 != null && location1.Corporation == this)
      {
        location1.Alliance = (ClientAlliance) null;
        location1.Corporation = (ClientCorporation) null;
      }
      this._location = location;
      if (this._location != null)
      {
        this._location.Alliance = this._alliance;
        this._location.Corporation = this;
      }
      this.OnChanged();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      sb.Append(this._name);
    }

    protected internal void SetName(string value)
    {
      if (this._name == value)
        return;
      this._name = value;
      this.OnChanged();
    }
  }
}
