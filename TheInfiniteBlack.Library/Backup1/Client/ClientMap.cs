﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientMap
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public class ClientMap
  {
    private readonly Sector[,] _normal = new Sector[80, 80];
    private readonly HashSet<Sector> _pathClosed = new HashSet<Sector>();
    private readonly HashSet<Sector> _pathOpen = new HashSet<Sector>();
    private readonly Sector[,] _rift = new Sector[80, 80];
    public const int SIZE = 80;
    public const int CENTER = 39;

    public Sector[,] NormalSpace
    {
      get
      {
        return this._normal;
      }
    }

    public Sector[,] RiftSpace
    {
      get
      {
        return this._rift;
      }
    }

    public Sector Sol
    {
      get
      {
        return this._normal[39, 39];
      }
    }

    public ClientMap()
    {
      for (int x = 0; x < 80; ++x)
      {
        for (int y = 0; y < 80; ++y)
        {
          this._normal[x, y] = (Sector) new ClientMap.NormalSector(x, y);
          this._rift[x, y] = (Sector) new ClientMap.RiftSector(x, y);
        }
      }
    }

    protected internal void OnSecond()
    {
      for (int index1 = 0; index1 < 80; ++index1)
      {
        for (int index2 = 0; index2 < 80; ++index2)
        {
          Sector sector1 = this._normal[index1, index2];
          Sector sector2 = this._rift[index1, index2];
          if (sector1.StatusTimeSeconds > 0 && !GameState.NoBust)
            --sector1.StatusTimeSeconds;
          if (sector2.StatusTimeSeconds > 0 && !GameState.NoBust)
            --sector2.StatusTimeSeconds;
        }
      }
    }

    public Sector Get(bool rift, int x, int y)
    {
      if (x < 0 || x >= 80 || (y < 0 || y >= 80))
        return (Sector) null;
      if (!rift)
        return this._normal[x, y];
      return this._rift[x, y];
    }

    public IEnumerable<Sector> Select(bool rift, int left, int top, int right, int bottom)
    {
      if (left >= 0 && left <= right && (top >= 0 && top <= bottom) && (right < 80 && bottom < 80))
      {
        Sector[,] map = rift ? this._rift : this._normal;
        for (; left <= right; ++left)
        {
          for (; top <= bottom; ++top)
            yield return map[left, top];
        }
      }
    }

    public List<Sector> GetPath(Sector source, Sector target)
    {
      List<Sector> sectorList = new List<Sector>(100);
      if (source != null && target != null && source != target)
      {
        if (source.Rift == target.Rift)
        {
          try
          {
            this._pathOpen.Add(source);
            int num = 6400;
            while (num > 0)
            {
              --num;
              Sector current = (Sector) null;
              foreach (Sector sector in this._pathOpen)
              {
                if (current == null || sector.F < current.F || sector.F == current.F && sector.Asteroid != AsteroidClass.NULL)
                  current = sector;
              }
              if (current != null && current != target)
              {
                this._pathOpen.Remove(current);
                this._pathClosed.Add(current);
                if (current.NW != null && (!GameState.InvasionFreeRouting || !current.NW.IsInvasion()))
                  this.Test(current.NW, current, target);
                if (current.N != null && (!GameState.InvasionFreeRouting || !current.N.IsInvasion()))
                  this.Test(current.N, current, target);
                if (current.NE != null && (!GameState.InvasionFreeRouting || !current.NE.IsInvasion()))
                  this.Test(current.NE, current, target);
                if (current.E != null && (!GameState.InvasionFreeRouting || !current.E.IsInvasion()))
                  this.Test(current.E, current, target);
                if (current.SE != null && (!GameState.InvasionFreeRouting || !current.SE.IsInvasion()))
                  this.Test(current.SE, current, target);
                if (current.S != null && (!GameState.InvasionFreeRouting || !current.S.IsInvasion()))
                  this.Test(current.S, current, target);
                if (current.SW != null && (!GameState.InvasionFreeRouting || !current.SW.IsInvasion()))
                  this.Test(current.SW, current, target);
                if (current.W != null && (!GameState.InvasionFreeRouting || !current.W.IsInvasion()))
                  this.Test(current.W, current, target);
              }
              else
                break;
            }
            if (target.Parent != null)
            {
              for (Sector sector = target; sector != source; sector = sector.Parent)
                sectorList.Add(sector);
              sectorList.Reverse();
            }
          }
          catch (Exception ex)
          {
            Log.E((object) this, nameof (GetPath), ex);
          }
          this._pathClosed.Clear();
          this._pathOpen.Clear();
          for (int index1 = 0; index1 < 80; ++index1)
          {
            for (int index2 = 0; index2 < 80; ++index2)
            {
              this._normal[index1, index2].ClearPathfinding();
              this._rift[index1, index2].ClearPathfinding();
            }
          }
        }
      }
      return sectorList;
    }

    private void Test(Sector neighbor, Sector current, Sector target)
    {
      if (this._pathOpen.Contains(neighbor))
      {
        if (current.G + 1 >= neighbor.G)
          return;
        int num = Math.Max(Math.Abs(neighbor.X - target.X), Math.Abs(neighbor.Y - target.Y));
        neighbor.G = current.G + 1;
        neighbor.F = num + neighbor.G;
        neighbor.Parent = current;
      }
      else
      {
        if (this._pathClosed.Contains(neighbor))
          return;
        int num = Math.Max(Math.Abs(neighbor.X - target.X), Math.Abs(neighbor.Y - target.Y));
        neighbor.G = current.G + 1;
        neighbor.F = num + neighbor.G;
        neighbor.Parent = current;
        this._pathOpen.Add(neighbor);
      }
    }

    private class NormalSector : Sector
    {
      public override bool Rift
      {
        get
        {
          return false;
        }
      }

      public NormalSector(int x, int y)
        : base(x, y)
      {
      }
    }

    private class RiftSector : Sector
    {
      public override bool Rift
      {
        get
        {
          return true;
        }
      }

      public RiftSector(int x, int y)
        : base(x, y)
      {
      }
    }
  }
}
