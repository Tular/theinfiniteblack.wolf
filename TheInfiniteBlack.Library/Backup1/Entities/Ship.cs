﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Ship
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Entities
{
  public class Ship : PlayerCombatEntity
  {
    private long _lastTimeWasntGrappled = 0;
    private ShipClass _class;
    private bool _pvpFlag;
    private int _xp;
    private WeaponItem _weapon;
    private ArmorItem _armor;
    private StorageItem _storage;
    private HarvesterItem _harvester;
    private EngineItem _engine;
    private ComputerItem _computer;
    private SpecialItem _special;
    private sbyte _organics;
    private sbyte _gas;
    private sbyte _metals;
    private sbyte _radioactives;
    private sbyte _darkMatter;
    private List<EquipmentItem> _inventory;

    public override EntityType Type
    {
      get
      {
        return EntityType.SHIP;
      }
    }

    public override string Title
    {
      get
      {
        if (this.Player != null)
          return this.Player.Name;
        return "?Unknown";
      }
    }

    public int EffectiveHull
    {
      get
      {
        return (int) ((double) this._hull / (1.0 + (double) this.IncomingDamageMod));
      }
    }

    public int EffectiveMaxHull
    {
      get
      {
        return (int) ((double) this.MaxHull / (1.0 + (double) this.IncomingDamageMod));
      }
    }

    public List<EquipmentItem> AllEquippedItems
    {
      get
      {
        List<EquipmentItem> equipmentItemList = new List<EquipmentItem>();
        if (this.Weapon != null)
          equipmentItemList.Add((EquipmentItem) this.Weapon);
        if (this.Armor != null)
          equipmentItemList.Add((EquipmentItem) this.Armor);
        if (this.Storage != null)
          equipmentItemList.Add((EquipmentItem) this.Storage);
        if (this.Engine != null)
          equipmentItemList.Add((EquipmentItem) this.Engine);
        if (this.Computer != null)
          equipmentItemList.Add((EquipmentItem) this.Computer);
        if (this.Harvester != null)
          equipmentItemList.Add((EquipmentItem) this.Harvester);
        if (this.Special != null)
          equipmentItemList.Add((EquipmentItem) this.Special);
        return equipmentItemList;
      }
    }

    public int TotalItemCost
    {
      get
      {
        return this.AllEquippedItems.Sum<EquipmentItem>((Func<EquipmentItem, int>) (x => x.ActualCreditValue));
      }
    }

    public override string SubTitle
    {
      get
      {
        try
        {
          if (!string.IsNullOrEmpty(GameState.SubtitleStat))
            return typeof (Ship).GetProperty(GameState.SubtitleStat).GetValue((object) this, (object[]) null).ToString();
        }
        catch (Exception ex)
        {
          return "";
        }
        StringBuilder sb = new StringBuilder(50);
        sb.Append("<");
        switch (this.Relation)
        {
          case RelationType.SELF:
          case RelationType.FRIEND:
            if (this.Corporation == null)
            {
              if (this.Alliance != null)
              {
                this.Alliance.AppendName(sb, false);
                break;
              }
              sb.Append("Privateer");
              break;
            }
            this.Corporation.AppendName(sb, false);
            break;
          default:
            if (this.Alliance != null)
            {
              this.Alliance.AppendName(sb, false);
              break;
            }
            if (this.Corporation != null)
            {
              this.Corporation.AppendName(sb, false);
              break;
            }
            sb.Append("Privateer");
            break;
        }
        sb.Append(">");
        return sb.ToString();
      }
    }

    public float Level
    {
      get
      {
        return (float) ((Math.Sqrt(12.25 - 2.0 * (-4.0 - (double) this._xp / 10.0)) - 3.5) / 10.0);
      }
    }

    public int AvailableEP
    {
      get
      {
        return this.MaxEP - this.UsedEP;
      }
    }

    public int UsedEP
    {
      get
      {
        int num = 0;
        if (this._weapon != null)
          num += this._weapon.EPCost;
        if (this._armor != null)
          num += this._armor.EPCost;
        if (this._storage != null)
          num += this._storage.EPCost;
        if (this._harvester != null)
          num += this._harvester.EPCost;
        if (this._engine != null)
          num += this._engine.EPCost;
        if (this._computer != null)
          num += this._computer.EPCost;
        if (this._special != null)
          num += this._special.EPCost;
        return num;
      }
    }

    public int MaxEP
    {
      get
      {
        int num = this._class.BaseEquipPoints();
        if (this._weapon != null)
          num += this._weapon.EPBonus;
        if (this._armor != null)
          num += this._armor.EPBonus;
        if (this._storage != null)
          num += this._storage.EPBonus;
        if (this._harvester != null)
          num += this._harvester.EPBonus;
        if (this._engine != null)
          num += this._engine.EPBonus;
        if (this._computer != null)
          num += this._computer.EPBonus;
        if (this._special != null)
          num += this._special.EPBonus;
        return num;
      }
    }

    public int HarvestSpeedMS
    {
      get
      {
        if (this._harvester != null)
          return this._harvester.HarvestSpeedMS;
        return 90000;
      }
    }

    public int HarvestCapacity
    {
      get
      {
        if (this._harvester != null)
          return this._harvester.HarvestCapacity;
        return 1;
      }
    }

    public int ResourceCapacity
    {
      get
      {
        int num = 10;
        if (this.Tech != null && this.Tech.AdvancedStorage)
          num += 10;
        if (this._weapon != null)
          num += this._weapon.ResourceCapacity;
        if (this._armor != null)
          num += this._armor.ResourceCapacity;
        if (this._storage != null)
          num += this._storage.ResourceCapacity;
        if (this._harvester != null)
          num += this._harvester.ResourceCapacity;
        if (this._engine != null)
          num += this._engine.ResourceCapacity;
        if (this._computer != null)
          num += this._computer.ResourceCapacity;
        if (this._special != null)
          num += this._special.ResourceCapacity;
        if (num <= 0)
          return 0;
        if (num <= (int) sbyte.MaxValue)
          return num;
        return (int) sbyte.MaxValue;
      }
    }

    public float SplashChance
    {
      get
      {
        float num = 0.05f * this.Level;
        if (this.Tech != null && this.Tech.Splash)
          num += 5f;
        if (this._class == ShipClass.WyrdInvader)
          num += 5f;
        if (this._weapon != null)
          num += this._weapon.SplashChance;
        if (this._armor != null)
          num += this._armor.SplashChance;
        if (this._storage != null)
          num += this._storage.SplashChance;
        if (this._harvester != null)
          num += this._harvester.SplashChance;
        if (this._engine != null)
          num += this._engine.SplashChance;
        if (this._computer != null)
          num += this._computer.SplashChance;
        if (this._special != null)
          num += this._special.SplashChance;
        return num;
      }
    }

    public override int MaxHull
    {
      get
      {
        int num = (int) (1400.0 + Math.Round((double) this.Level * 3.0));
        if (this.Tech != null && this.Tech.AdvancedHulls)
          num += 310;
        if (this._weapon != null)
          num += this._weapon.MaxHullBonus;
        if (this._armor != null)
          num += this._armor.MaxHullBonus;
        if (this._storage != null)
          num += this._storage.MaxHullBonus;
        if (this._harvester != null)
          num += this._harvester.MaxHullBonus;
        if (this._engine != null)
          num += this._engine.MaxHullBonus;
        if (this._computer != null)
          num += this._computer.MaxHullBonus;
        if (this._special != null)
          num += this._special.MaxHullBonus;
        if (num > 0)
          return num;
        return 1;
      }
    }

    public float OutgoingDamageMod
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.OutgoingDamageMod;
        if (this._armor != null)
          num += this._armor.OutgoingDamageMod;
        if (this._storage != null)
          num += this._storage.OutgoingDamageMod;
        if (this._harvester != null)
          num += this._harvester.OutgoingDamageMod;
        if (this._engine != null)
          num += this._engine.OutgoingDamageMod;
        if (this._computer != null)
          num += this._computer.OutgoingDamageMod;
        if (this._special != null)
          num += this._special.OutgoingDamageMod;
        return num;
      }
    }

    public float IncomingDamageMod
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.IncomingDamageMod;
        if (this._armor != null)
          num += this._armor.IncomingDamageMod;
        if (this._storage != null)
          num += this._storage.IncomingDamageMod;
        if (this._harvester != null)
          num += this._harvester.IncomingDamageMod;
        if (this._engine != null)
          num += this._engine.IncomingDamageMod;
        if (this._computer != null)
          num += this._computer.IncomingDamageMod;
        if (this._special != null)
          num += this._special.IncomingDamageMod;
        return num;
      }
    }

    public float MetalRepairMod
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.MetalRepairMod;
        if (this._armor != null)
          num += this._armor.MetalRepairMod;
        if (this._storage != null)
          num += this._storage.MetalRepairMod;
        if (this._harvester != null)
          num += this._harvester.MetalRepairMod;
        if (this._engine != null)
          num += this._engine.MetalRepairMod;
        if (this._computer != null)
          num += this._computer.MetalRepairMod;
        if (this._special != null)
          num += this._special.MetalRepairMod;
        return num;
      }
    }

    public float XpMod
    {
      get
      {
        float num = 0.0f;
        if (this.Tech != null && this.Tech.Analysis)
          num += 0.1f;
        if (this._weapon != null)
          num += this._weapon.XpMod;
        if (this._armor != null)
          num += this._armor.XpMod;
        if (this._storage != null)
          num += this._storage.XpMod;
        if (this._harvester != null)
          num += this._harvester.XpMod;
        if (this._engine != null)
          num += this._engine.XpMod;
        if (this._computer != null)
          num += this._computer.XpMod;
        if (this._special != null)
          num += this._special.XpMod;
        return num;
      }
    }

    public int MinimumDamage
    {
      get
      {
        float outgoingDamageMod = this.OutgoingDamageMod;
        double playerMinDamage = (double) CombatHelper.getPlayerMinDamage(this._weapon == null ? 1 : (this.Tech == null || !this.Tech.AdvancedWeapons ? (int) this._weapon.BaseEP : (int) this._weapon.BaseEP + 1), this.AttackSpeedMS);
        double num1 = (double) outgoingDamageMod;
        double num2 = playerMinDamage * num1;
        return (int) Math.Round(playerMinDamage + num2 + (double) this.Level / 5.0);
      }
    }

    public int AttackSpeedMS
    {
      get
      {
        if (this._weapon != null)
          return this._weapon.AttackSpeedMS;
        return 5000;
      }
    }

    public int MoveSpeedMS
    {
      get
      {
        int num = 8000 + this._class.MoveSpeedPenalty();
        if (this.Tech != null && this.Tech.AdvancedEngines)
          num -= 1000;
        if (this._weapon != null)
          num += this._weapon.MoveSpeedMSAdjust;
        if (this._armor != null)
          num += this._armor.MoveSpeedMSAdjust;
        if (this._storage != null)
          num += this._storage.MoveSpeedMSAdjust;
        if (this._harvester != null)
          num += this._harvester.MoveSpeedMSAdjust;
        if (this._engine != null)
          num += this._engine.MoveSpeedMSAdjust;
        if (this._computer != null)
          num += this._computer.MoveSpeedMSAdjust;
        if (this._special != null)
          num += this._special.MoveSpeedMSAdjust;
        if (num > 4000)
          return num;
        return 4000;
      }
    }

    public float CriticalChance
    {
      get
      {
        float num = 0.05f * this.Level;
        if (this.Tech != null && this.Tech.Critical)
          num += 5f;
        if (this._weapon != null)
          num += this._weapon.CriticalChance;
        if (this._armor != null)
          num += this._armor.CriticalChance;
        if (this._storage != null)
          num += this._storage.CriticalChance;
        if (this._harvester != null)
          num += this._harvester.CriticalChance;
        if (this._engine != null)
          num += this._engine.CriticalChance;
        if (this._computer != null)
          num += this._computer.CriticalChance;
        if (this._special != null)
          num += this._special.CriticalChance;
        return num;
      }
    }

    public float EvasionChance
    {
      get
      {
        float num = 0.1f * this.Level + this._class.EvasionBonus();
        if (this.Tech != null && this.Tech.Evasion)
          num += 5f;
        if (this._weapon != null)
          num += this._weapon.EvasionChance;
        if (this._armor != null)
          num += this._armor.EvasionChance;
        if (this._storage != null)
          num += this._storage.EvasionChance;
        if (this._harvester != null)
          num += this._harvester.EvasionChance;
        if (this._engine != null)
          num += this._engine.EvasionChance;
        if (this._computer != null)
          num += this._computer.EvasionChance;
        if (this._special != null)
          num += this._special.EvasionChance;
        return num;
      }
    }

    public float HitChance
    {
      get
      {
        float num = 0.1f * this.Level;
        if (this.Tech != null && this.Tech.ToHit)
          num += 8f;
        if (this._weapon != null)
          num += this._weapon.HitChance;
        if (this._armor != null)
          num += this._armor.HitChance;
        if (this._storage != null)
          num += this._storage.HitChance;
        if (this._harvester != null)
          num += this._harvester.HitChance;
        if (this._engine != null)
          num += this._engine.HitChance;
        if (this._computer != null)
          num += this._computer.HitChance;
        if (this._special != null)
          num += this._special.HitChance;
        return num;
      }
    }

    public int StunDurationMSAdjust
    {
      get
      {
        int num = this._class == ShipClass.WyrdReaper ? 1000 : 0;
        if (this._weapon != null)
          num += this._weapon.StunDurationMSAdjust;
        if (this._armor != null)
          num += this._armor.StunDurationMSAdjust;
        if (this._storage != null)
          num += this._storage.StunDurationMSAdjust;
        if (this._harvester != null)
          num += this._harvester.StunDurationMSAdjust;
        if (this._engine != null)
          num += this._engine.StunDurationMSAdjust;
        if (this._computer != null)
          num += this._computer.StunDurationMSAdjust;
        if (this._special != null)
          num += this._special.StunDurationMSAdjust;
        return num;
      }
    }

    public float CriticalDamageMod
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.CriticalDamageMod;
        if (this._armor != null)
          num += this._armor.CriticalDamageMod;
        if (this._storage != null)
          num += this._storage.CriticalDamageMod;
        if (this._harvester != null)
          num += this._harvester.CriticalDamageMod;
        if (this._engine != null)
          num += this._engine.CriticalDamageMod;
        if (this._computer != null)
          num += this._computer.CriticalDamageMod;
        if (this._special != null)
          num += this._special.CriticalDamageMod;
        return num;
      }
    }

    public int MaxCrit
    {
      get
      {
        return (int) ((double) (this.MinimumDamage * 2) * (2.0 + (double) this.CriticalDamageMod));
      }
    }

    public double DPS
    {
      get
      {
        return CombatHelper.CalculateDPS(this.MinimumDamage, this.AttackSpeedMS / 1000, (double) this.HitChance, (1.0 + (double) this.CriticalDamageMod) * 100.0, (double) this.CriticalChance, 1.0, 0.0);
      }
    }

    public float StunChance
    {
      get
      {
        float num = this._class == ShipClass.WyrdReaper ? 8f : 0.0f;
        if (this._weapon != null)
          num += this._weapon.StunChance;
        if (this._armor != null)
          num += this._armor.StunChance;
        if (this._storage != null)
          num += this._storage.StunChance;
        if (this._harvester != null)
          num += this._harvester.StunChance;
        if (this._engine != null)
          num += this._engine.StunChance;
        if (this._computer != null)
          num += this._computer.StunChance;
        if (this._special != null)
          num += this._special.StunChance;
        return num;
      }
    }

    public float GrappleChance
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.GrappleChance;
        if (this._armor != null)
          num += this._armor.GrappleChance;
        if (this._storage != null)
          num += this._storage.GrappleChance;
        if (this._harvester != null)
          num += this._harvester.GrappleChance;
        if (this._engine != null)
          num += this._engine.GrappleChance;
        if (this._computer != null)
          num += this._computer.GrappleChance;
        if (this._special != null)
          num += this._special.GrappleChance;
        return num;
      }
    }

    public float StunResist
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.StunResist;
        if (this._armor != null)
          num += this._armor.StunResist;
        if (this._storage != null)
          num += this._storage.StunResist;
        if (this._harvester != null)
          num += this._harvester.StunResist;
        if (this._engine != null)
          num += this._engine.StunResist;
        if (this._computer != null)
          num += this._computer.StunResist;
        if (this._special != null)
          num += this._special.StunResist;
        return num;
      }
    }

    public float GrappleResist
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.GrappleResist;
        if (this._armor != null)
          num += this._armor.GrappleResist;
        if (this._storage != null)
          num += this._storage.GrappleResist;
        if (this._harvester != null)
          num += this._harvester.GrappleResist;
        if (this._engine != null)
          num += this._engine.GrappleResist;
        if (this._computer != null)
          num += this._computer.GrappleResist;
        if (this._special != null)
          num += this._special.GrappleResist;
        return num;
      }
    }

    public float CriticalResist
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.CriticalResist;
        if (this._armor != null)
          num += this._armor.CriticalResist;
        if (this._storage != null)
          num += this._storage.CriticalResist;
        if (this._harvester != null)
          num += this._harvester.CriticalResist;
        if (this._engine != null)
          num += this._engine.CriticalResist;
        if (this._computer != null)
          num += this._computer.CriticalResist;
        if (this._special != null)
          num += this._special.CriticalResist;
        return num;
      }
    }

    public float SplashResist
    {
      get
      {
        float num = 0.0f;
        if (this._weapon != null)
          num += this._weapon.SplashResist;
        if (this._armor != null)
          num += this._armor.SplashResist;
        if (this._storage != null)
          num += this._storage.SplashResist;
        if (this._harvester != null)
          num += this._harvester.SplashResist;
        if (this._engine != null)
          num += this._engine.SplashResist;
        if (this._computer != null)
          num += this._computer.SplashResist;
        if (this._special != null)
          num += this._special.SplashResist;
        return num;
      }
    }

    public int StealthPower
    {
      get
      {
        if (this._class == ShipClass.WyrdAssassin)
          return 4;
        if (this._engine == null || this._engine.Class != EngineClass.STEALTH)
          return 0;
        return (int) this._engine.Rarity;
      }
    }

    public int ScanPower
    {
      get
      {
        if (this._special == null || this._special.Class != SpecialClass.SCOUT)
          return 0;
        return (int) this._special.Rarity;
      }
    }

    public ShipClass Class
    {
      get
      {
        return this._class;
      }
    }

    public bool PvPFlag
    {
      get
      {
        return this._pvpFlag;
      }
      set
      {
        if (this._pvpFlag == value)
          return;
        this._pvpFlag = value;
        this.OnChanged();
      }
    }

    public int XP
    {
      get
      {
        return this._xp;
      }
      set
      {
        if (this._xp == value)
          return;
        this._xp = value;
        this.OnChanged();
      }
    }

    public WeaponItem Weapon
    {
      get
      {
        return this._weapon;
      }
    }

    public ArmorItem Armor
    {
      get
      {
        return this._armor;
      }
    }

    public StorageItem Storage
    {
      get
      {
        return this._storage;
      }
    }

    public HarvesterItem Harvester
    {
      get
      {
        return this._harvester;
      }
    }

    public EngineItem Engine
    {
      get
      {
        return this._engine;
      }
    }

    public ComputerItem Computer
    {
      get
      {
        return this._computer;
      }
    }

    public SpecialItem Special
    {
      get
      {
        return this._special;
      }
    }

    public sbyte Organics
    {
      get
      {
        return this._organics;
      }
      set
      {
        if ((int) this._organics == (int) value)
          return;
        this._organics = value;
        this.OnChanged();
      }
    }

    public sbyte Gas
    {
      get
      {
        return this._gas;
      }
      set
      {
        if ((int) this._gas == (int) value)
          return;
        this._gas = value;
        this.OnChanged();
      }
    }

    public sbyte Metals
    {
      get
      {
        return this._metals;
      }
      set
      {
        if ((int) this._metals == (int) value)
          return;
        this._metals = value;
        this.OnChanged();
      }
    }

    public sbyte Radioactives
    {
      get
      {
        return this._radioactives;
      }
      set
      {
        if ((int) this._radioactives == (int) value)
          return;
        this._radioactives = value;
        this.OnChanged();
      }
    }

    public sbyte DarkMatter
    {
      get
      {
        return this._darkMatter;
      }
      set
      {
        if ((int) this._darkMatter == (int) value)
          return;
        this._darkMatter = value;
        this.OnChanged();
      }
    }

    public List<EquipmentItem> Inventory
    {
      get
      {
        return this._inventory;
      }
      set
      {
        this._inventory = value;
        this.OnChanged();
      }
    }

    public Ship(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      RelationType relation = this.Relation;
      if (this.State.MySettings.EntityList.TapToRepair)
      {
        switch (relation)
        {
          case RelationType.NEUTRAL:
          case RelationType.SELF:
          case RelationType.FRIEND:
            this.State.DoRepair((PlayerCombatEntity) this);
            return;
        }
      }
      if (this.State.MyAttackTarget == this)
      {
        if (!this.State.MySettings.EntityList.TapToAttack || relation != RelationType.ENEMY)
        {
          this.State.DoAttack((CombatEntity) null);
          if (this.State.MyFollowTarget != this)
            return;
          this.State.DoFollow((CombatEntity) null);
        }
        else
          this.State.UI.Show(new ShowEntityDetailEventArgs(this.State, (Entity) this));
      }
      else if (this.State.MySettings.EntityList.TapToAttack && relation == RelationType.ENEMY)
        this.State.DoAttack((CombatEntity) this);
      else
        this.State.UI.Show(new ShowEntityDetailEventArgs(this.State, (Entity) this));
    }

    public override void OnLongPress()
    {
      switch (this.Relation)
      {
        case RelationType.NEUTRAL:
        case RelationType.SELF:
        case RelationType.FRIEND:
          if (this.State.MySettings.EntityList.TapToRepair)
          {
            this.State.UI.Show(new ShowEntityDetailEventArgs(this.State, (Entity) this));
            break;
          }
          this.State.DoRepair((PlayerCombatEntity) this);
          break;
        default:
          if (this.State.MyAttackTarget != this)
            this.State.DoAttack((CombatEntity) this);
          if (this.State.MyFollowTarget == this || this.State.MyFollowTarget != null && this.State.MyFollowTarget.Relation == RelationType.FRIEND)
            break;
          this.State.DoFollow((CombatEntity) this);
          break;
      }
    }

    protected internal override void Set(IByteBuffer input)
    {
      base.Set(input);
      this._class = (ShipClass) input.Get();
      this._darkMatter = input.Get();
      this._radioactives = input.Get();
      this._metals = input.Get();
      this._gas = input.Get();
      this._organics = input.Get();
      this._pvpFlag = input.GetBool();
      this._xp = input.GetInt();
      this._weapon = EquipmentItem.Execute(input) as WeaponItem;
      this._armor = EquipmentItem.Execute(input) as ArmorItem;
      this._storage = EquipmentItem.Execute(input) as StorageItem;
      this._harvester = EquipmentItem.Execute(input) as HarvesterItem;
      this._engine = EquipmentItem.Execute(input) as EngineItem;
      this._computer = EquipmentItem.Execute(input) as ComputerItem;
      this._special = EquipmentItem.Execute(input) as SpecialItem;
      int capacity = (int) input.Get();
      List<EquipmentItem> equipmentItemList = new List<EquipmentItem>(capacity);
      for (int index = 0; index < capacity; ++index)
      {
        EquipmentItem equipmentItem = EquipmentItem.Execute(input);
        if (equipmentItem != null)
          equipmentItemList.Add(equipmentItem);
      }
      this._inventory = equipmentItemList;
    }

    public int GetResourceCount(ResourceType resource)
    {
      switch (resource)
      {
        case ResourceType.ORGANIC:
          return (int) this._organics;
        case ResourceType.GAS:
          return (int) this._gas;
        case ResourceType.METAL:
          return (int) this._metals;
        case ResourceType.RADIOACTIVE:
          return (int) this._radioactives;
        case ResourceType.DARKMATTER:
          return (int) this._darkMatter;
        default:
          return 0;
      }
    }

    public void SetResourceCount(ResourceType resource, sbyte count)
    {
      switch (resource)
      {
        case ResourceType.ORGANIC:
          this._organics = count;
          break;
        case ResourceType.GAS:
          this._gas = count;
          break;
        case ResourceType.METAL:
          this._metals = count;
          break;
        case ResourceType.RADIOACTIVE:
          this._radioactives = count;
          break;
        case ResourceType.DARKMATTER:
          this._darkMatter = count;
          break;
      }
    }

    public EquipmentItem GetEquippedItem(ItemType type)
    {
      switch (type)
      {
        case ItemType.WEAPON:
          return (EquipmentItem) this._weapon;
        case ItemType.ARMOR:
          return (EquipmentItem) this._armor;
        case ItemType.STORAGE:
          return (EquipmentItem) this._storage;
        case ItemType.HARVESTER:
          return (EquipmentItem) this._harvester;
        case ItemType.ENGINE:
          return (EquipmentItem) this._engine;
        case ItemType.COMPUTER:
          return (EquipmentItem) this._computer;
        case ItemType.SPECIAL:
          return (EquipmentItem) this._special;
        default:
          return (EquipmentItem) null;
      }
    }

    public bool Has(SpecialClass special)
    {
      if (this._special != null)
        return this._special.Class == special;
      return false;
    }

    public bool Has(EngineClass engine)
    {
      if (this._engine != null)
        return this._engine.Class == engine;
      return false;
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this.Player == null ? "Unknown?" : this.Player.Name);
      sb.Append("'s ");
      sb.Append(this._class.Name());
      if (!markup)
        return;
      sb.Append("[-]");
    }

    public bool ShouldBeDegrappled(long worldTimeMS)
    {
      if (this.Grappled)
        return worldTimeMS - this._lastTimeWasntGrappled > 500L;
      this._lastTimeWasntGrappled = worldTimeMS;
      return false;
    }
  }
}
