﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.CargoMoney
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class CargoMoney : CargoEntity
  {
    private int _credits;
    private int _blackDollars;

    public override EntityType Type
    {
      get
      {
        return EntityType.CARGO_MONEY;
      }
    }

    public override string Title
    {
      get
      {
        return "";
      }
    }

    public override string SubTitle
    {
      get
      {
        if (this._credits <= 0)
          return this._blackDollars.ToString("#,0") + " BlackDollars";
        return this._credits.ToString("$#,0") + " Credits";
      }
    }

    public int Credits
    {
      get
      {
        return this._credits;
      }
    }

    public int BlackDollars
    {
      get
      {
        return this._blackDollars;
      }
    }

    public CargoMoney(IGameState state, int id)
      : base(state, id)
    {
    }

    protected internal override void Set(IByteBuffer input)
    {
      this._credits = input.GetInt();
      this._blackDollars = input.GetInt();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append("Cargo");
      if (!markup)
        return;
      sb.Append("[-]");
    }

    public override bool CanLoot(Ship ship)
    {
      if (this._credits <= 0)
        return this._blackDollars > 0;
      return true;
    }
  }
}
