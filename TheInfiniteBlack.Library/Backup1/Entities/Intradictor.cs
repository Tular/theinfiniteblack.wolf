﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Intradictor
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class Intradictor : DefenseEntity
  {
    public override EntityType Type
    {
      get
      {
        return EntityType.INTRADICTOR;
      }
    }

    public override string Title
    {
      get
      {
        StringBuilder sb = new StringBuilder(100);
        this.AppendName(sb, false);
        return sb.ToString();
      }
    }

    public override int MaxHull
    {
      get
      {
        return (24 + this.Level) * 160;
      }
    }

    public Intradictor(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      this.State.DoAttack((CombatEntity) this);
    }

    public override void OnLongPress()
    {
      this.OnClick();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this.Tech == null || !this.Tech.AdvancedDefense ? "Intradictor " : "ADV Intradictor ");
      sb.Append(Util.NUMERALS[this.Level]);
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
