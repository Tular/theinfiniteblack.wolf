﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.NonCombatEntity
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public abstract class NonCombatEntity : Entity
  {
    protected NonCombatEntity(IGameState state, int id)
      : base(state, id)
    {
    }
  }
}
