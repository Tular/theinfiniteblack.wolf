﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.CargoItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Entities
{
  public class CargoItem : CargoEntity
  {
    private EquipmentItem _item;

    public override EntityType Type
    {
      get
      {
        return EntityType.CARGO_ITEM;
      }
    }

    public override string Title
    {
      get
      {
        StringBuilder sb = new StringBuilder(200);
        this._item.AppendName(sb);
        return sb.ToString();
      }
    }

    public override string SubTitle
    {
      get
      {
        return this._item.Rarity.Name() + " (" + (object) this._item.Durability + "%)";
      }
    }

    public CargoItem(IGameState state, int id)
      : base(state, id)
    {
    }

    protected internal override void Set(IByteBuffer input)
    {
      this._item = EquipmentItem.Execute(input);
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      this._item.AppendLongName(sb, markup, false);
    }

    public override bool CanLoot(Ship ship)
    {
      return this._item != null;
    }
  }
}
