﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.EntityType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Entities
{
  public enum EntityType
  {
    NULL = -128,
    PLANET = 1,
    ASTEROID = 2,
    STARPORT = 3,
    SHIP = 4,
    NPC = 5,
    FIGHTER = 7,
    DEFENSE_PLATFORM = 12,
    MINES = 13,
    INTRADICTOR = 14,
    REPAIR_DRONE = 15,
    GARRISON = 16,
    CARGO_MONEY = 17,
    CARGO_ITEM = 18,
    CARGO_RESOURCE = 19,
    CAPTURE_POINT = 20,
  }
}
