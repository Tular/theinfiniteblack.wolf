﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Planet
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Library.Entities
{
  public class Planet : PlayerCombatEntity
  {
    private PlanetClass _class;
    private float _progress;

    public override EntityType Type
    {
      get
      {
        return EntityType.PLANET;
      }
    }

    public override string Title
    {
      get
      {
        if (this.ID != 0)
          return this._class.Name();
        return "Earth";
      }
    }

    public override string SubTitle
    {
      get
      {
        if (this.ID == 0)
          return "Human Home World";
        StringBuilder sb = new StringBuilder(50);
        sb.Append("<");
        if (this.Alliance != null)
          this.Alliance.AppendName(sb, false);
        else if (this.Corporation != null)
          this.Corporation.AppendName(sb, false);
        else if (this.Player != null)
          this.Player.AppendName(sb, false);
        else
          sb.Append("?Unknown");
        sb.Append(">");
        return sb.ToString();
      }
    }

    public bool IsEarth
    {
      get
      {
        return this.ID == 0;
      }
    }

    public override int MaxHull
    {
      get
      {
        return this.EntityEPValue * 480;
      }
    }

    public int EntityEPValue
    {
      get
      {
        return (int) (38 + this._class);
      }
    }

    public PlanetClass Class
    {
      get
      {
        return this._class;
      }
    }

    public float Progress
    {
      get
      {
        return this._progress;
      }
    }

    public Planet(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      this.State.UI.Show(new ShowEntityDetailEventArgs(this.State, (Entity) this));
    }

    public override void OnLongPress()
    {
      switch (this.Relation)
      {
        case RelationType.SELF:
        case RelationType.FRIEND:
          this.OnClick();
          break;
        default:
          this.State.DoAttack((CombatEntity) this);
          break;
      }
    }

    protected internal override void Set(IByteBuffer input)
    {
      base.Set(input);
      this._class = (PlanetClass) input.Get();
      this._progress = input.GetFloat();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this.IsEarth ? "Earth" : this._class.Name());
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
