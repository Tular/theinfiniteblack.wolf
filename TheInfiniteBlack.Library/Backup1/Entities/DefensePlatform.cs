﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.DefensePlatform
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class DefensePlatform : DefenseEntity
  {
    public override EntityType Type
    {
      get
      {
        return EntityType.DEFENSE_PLATFORM;
      }
    }

    public override string Title
    {
      get
      {
        StringBuilder sb = new StringBuilder(100);
        this.AppendName(sb, false);
        return sb.ToString();
      }
    }

    public override int MaxHull
    {
      get
      {
        return (28 + this.Level) * 315;
      }
    }

    public DefensePlatform(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      this.State.DoAttack((CombatEntity) this);
    }

    public override void OnLongPress()
    {
      this.OnClick();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this.Tech == null || !this.Tech.AdvancedDefense ? "Defense Platform " : "ADV Defense Platform ");
      sb.Append(Util.NUMERALS[this.Level]);
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
