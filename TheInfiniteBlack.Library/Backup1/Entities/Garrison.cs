﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Garrison
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Entities
{
  public class Garrison : DefenseEntity
  {
    private List<EquipmentItem> _inventory;
    private float _progress;

    public override EntityType Type
    {
      get
      {
        return EntityType.GARRISON;
      }
    }

    public override string Title
    {
      get
      {
        StringBuilder sb = new StringBuilder(100);
        this.AppendName(sb, false);
        return sb.ToString();
      }
    }

    public override int MaxHull
    {
      get
      {
        return (36 + this.Level) * 310;
      }
    }

    public float Progress
    {
      get
      {
        return this._progress;
      }
    }

    public List<EquipmentItem> Inventory
    {
      get
      {
        return this._inventory;
      }
      set
      {
        this._inventory = value;
        this.OnChanged();
      }
    }

    public Garrison(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      this.State.UI.Show(new ShowEntityDetailEventArgs(this.State, (Entity) this));
    }

    public override void OnLongPress()
    {
      switch (this.Relation)
      {
        case RelationType.SELF:
        case RelationType.FRIEND:
          this.OnClick();
          break;
        default:
          this.State.DoAttack((CombatEntity) this);
          break;
      }
    }

    protected internal override void Set(IByteBuffer input)
    {
      base.Set(input);
      this.Level = (int) input.Get();
      this._progress = input.GetFloat();
      int capacity = (int) input.Get();
      List<EquipmentItem> equipmentItemList = new List<EquipmentItem>(capacity);
      for (int index = 0; index < capacity; ++index)
      {
        EquipmentItem equipmentItem = EquipmentItem.Execute(input);
        if (equipmentItem != null)
          equipmentItemList.Add(equipmentItem);
      }
      this._inventory = equipmentItemList;
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this.Tech == null || !this.Tech.AdvancedDefense ? "Garrison " : "ADV Garrison ");
      sb.Append(Util.NUMERALS[this.Level]);
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
