﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Entity
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public abstract class Entity : NamedClientObject
  {
    private readonly int _id;

    public abstract EntityType Type { get; }

    public abstract string Title { get; }

    public abstract string SubTitle { get; }

    public int ID
    {
      get
      {
        return this._id;
      }
    }

    public RelationType Relation
    {
      get
      {
        return Entity.GetRelation(this, (Entity) this.State.MyShip);
      }
    }

    protected Entity(IGameState state, int id)
      : base(state)
    {
      this._id = id;
    }

    public abstract void OnClick();

    public abstract void OnLongPress();

    protected internal abstract void Set(IByteBuffer input);

    public static Entity Execute(IGameState state, IByteBuffer input)
    {
      EntityType type = (EntityType) input.Get();
      int id = input.GetInt();
      Entity entity = state.Entities.Get<Entity>(id);
      if (entity == null || entity.Type != type)
      {
        entity = type.Create(state, id);
        state.Entities.Add(entity);
      }
      entity.Set(input);
      entity.OnChanged();
      return entity;
    }

    public static RelationType GetRelation(Entity a, Entity b)
    {
      if (a == null || b == null)
        return RelationType.NULL;
      if (a._id == b._id)
        return RelationType.SELF;
      if (a is NonCombatEntity || b is NonCombatEntity)
        return RelationType.NEUTRAL;
      if (a.Type == EntityType.NPC)
        return b.Type != EntityType.NPC ? RelationType.ENEMY : RelationType.FRIEND;
      if (b.Type == EntityType.NPC)
        return RelationType.ENEMY;
      PlayerCombatEntity playerCombatEntity1 = a as PlayerCombatEntity;
      if (playerCombatEntity1 == null)
        return RelationType.NEUTRAL;
      if (playerCombatEntity1._id == 0)
        return RelationType.FRIEND;
      PlayerCombatEntity playerCombatEntity2 = b as PlayerCombatEntity;
      if (playerCombatEntity2 == null)
        return RelationType.NEUTRAL;
      if (playerCombatEntity2._id == 0)
        return RelationType.FRIEND;
      if (playerCombatEntity1.Player != null && playerCombatEntity1.Player == playerCombatEntity2.Player)
        return RelationType.SELF;
      if (playerCombatEntity1.Alliance != null && playerCombatEntity1.Alliance == playerCombatEntity2.Alliance || playerCombatEntity1.Corporation != null && playerCombatEntity1.Corporation == playerCombatEntity2.Corporation)
        return RelationType.FRIEND;
      Ship ship1 = playerCombatEntity1 as Ship;
      if (ship1 != null)
      {
        Ship ship2 = playerCombatEntity2 as Ship;
        if (ship2 != null && !ship1.PvPFlag && !ship2.PvPFlag)
          return RelationType.NEUTRAL;
      }
      return RelationType.ENEMY;
    }
  }
}
