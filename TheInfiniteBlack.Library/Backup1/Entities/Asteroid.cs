﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Asteroid
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class Asteroid : NonCombatEntity
  {
    private AsteroidClass _class;

    public override EntityType Type
    {
      get
      {
        return EntityType.ASTEROID;
      }
    }

    public override string Title
    {
      get
      {
        return this._class.Name();
      }
    }

    public override string SubTitle
    {
      get
      {
        return this._class.Size();
      }
    }

    public AsteroidClass Class
    {
      get
      {
        return this._class;
      }
    }

    public Asteroid(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      this.State.DoHarvest(this);
    }

    public override void OnLongPress()
    {
      this.OnClick();
    }

    protected internal override void Set(IByteBuffer input)
    {
      this._class = (AsteroidClass) input.Get();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this._class.Size());
      sb.Append(" ");
      sb.Append(this._class.Name());
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
