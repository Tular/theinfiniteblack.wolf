﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Fighter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class Fighter : PlayerCombatEntity
  {
    public override EntityType Type
    {
      get
      {
        return EntityType.FIGHTER;
      }
    }

    public override string Title
    {
      get
      {
        return this.Tech == null || !this.Tech.AdvancedDrones ? nameof (Fighter) : "ADV Fighter";
      }
    }

    public override string SubTitle
    {
      get
      {
        StringBuilder sb = new StringBuilder(50);
        sb.Append("<");
        if (this.Player != null)
          this.Player.AppendName(sb, false);
        else
          sb.Append("?Unknown");
        sb.Append(">");
        return sb.ToString();
      }
    }

    public override int MaxHull
    {
      get
      {
        return this.Tech == null || !this.Tech.AdvancedDrones ? 1500 : 3000;
      }
    }

    public Fighter(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      switch (this.Relation)
      {
        case RelationType.SELF:
        case RelationType.FRIEND:
          this.State.DoRepair((PlayerCombatEntity) this);
          break;
        default:
          this.State.DoAttack((CombatEntity) this);
          break;
      }
    }

    public override void OnLongPress()
    {
      this.OnClick();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      if (this.Player != null)
      {
        this.Player.AppendName(sb, false);
        sb.Append("'s ");
      }
      else if (this.Alliance != null)
      {
        this.Alliance.AppendName(sb, false);
        sb.Append("'s ");
      }
      else if (this.Corporation != null)
      {
        this.Corporation.AppendName(sb, false);
        sb.Append("'s ");
      }
      sb.Append(this.Tech == null || !this.Tech.AdvancedDrones ? nameof (Fighter) : "ADV Fighter");
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
