﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.CargoResource
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class CargoResource : CargoEntity
  {
    private ResourceType _resource;
    private int _count;

    public override EntityType Type
    {
      get
      {
        return EntityType.CARGO_RESOURCE;
      }
    }

    public override string Title
    {
      get
      {
        return this._count.ToString() + " " + this._resource.Name();
      }
    }

    public override string SubTitle
    {
      get
      {
        return string.Empty;
      }
    }

    public ResourceType Resource
    {
      get
      {
        return this._resource;
      }
    }

    public int Count
    {
      get
      {
        return this._count;
      }
    }

    public CargoResource(IGameState state, int id)
      : base(state, id)
    {
    }

    protected internal override void Set(IByteBuffer input)
    {
      this._resource = (ResourceType) input.Get();
      this._count = (int) input.GetShort();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(this._resource.Name());
      sb.Append(" x");
      sb.Append(this._count);
      if (!markup)
        return;
      sb.Append("[-]");
    }

    public override bool CanLoot(Ship ship)
    {
      if (this._count <= 0 || this._resource == ResourceType.NULL)
        return false;
      int resourceCapacity = ship.ResourceCapacity;
      return ship.GetResourceCount(this._resource) < resourceCapacity;
    }
  }
}
