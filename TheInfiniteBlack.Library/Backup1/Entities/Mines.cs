﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Mines
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class Mines : PlayerCombatEntity
  {
    public override EntityType Type
    {
      get
      {
        return EntityType.MINES;
      }
    }

    public override string Title
    {
      get
      {
        return nameof (Mines);
      }
    }

    public override string SubTitle
    {
      get
      {
        StringBuilder sb = new StringBuilder(50);
        sb.Append("<");
        if (this.Alliance != null)
          this.Alliance.AppendName(sb, false);
        else if (this.Corporation != null)
          this.Corporation.AppendName(sb, false);
        else if (this.Player != null)
          this.Player.AppendName(sb, false);
        else
          sb.Append("?Unknown");
        sb.Append(">");
        return sb.ToString();
      }
    }

    public override int MaxHull
    {
      get
      {
        return 100;
      }
    }

    public Mines(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
    }

    public override void OnLongPress()
    {
      this.OnClick();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      sb.Append(nameof (Mines));
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
