﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Certification
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace TheInfiniteBlack.Library
{
  public static class Certification
  {
    public static bool MyRemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      bool flag = true;
      if (sslPolicyErrors != SslPolicyErrors.None)
      {
        for (int index = 0; index < chain.ChainStatus.Length; ++index)
        {
          if (chain.ChainStatus[index].Status != X509ChainStatusFlags.RevocationStatusUnknown)
          {
            chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
            chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
            chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
            chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
            if (!chain.Build((X509Certificate2) certificate))
              flag = false;
          }
        }
      }
      return flag;
    }
  }
}
