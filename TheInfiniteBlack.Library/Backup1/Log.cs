﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Log
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;

namespace TheInfiniteBlack.Library
{
  public static class Log
  {
    public static ILogger Instance;

    public static bool Debug
    {
      get
      {
        return Log.Instance.Debug;
      }
    }

    public static void I(object sender, string callerName, string log)
    {
      Log.Instance.I(sender, callerName, log);
    }

    public static void D(object sender, string callerName, string log)
    {
      Log.Instance.D(sender, callerName, log);
    }

    public static void W(object sender, string callerName, string log)
    {
      Log.Instance.W(sender, callerName, log);
    }

    public static void E(object sender, string callerName, Exception e)
    {
      Log.Instance.E(sender, callerName, e);
    }
  }
}
