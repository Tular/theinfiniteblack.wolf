﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.AllowedPlayers
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace TheInfiniteBlack.Library
{
  public class AllowedPlayers
  {
    public static List<string> Players()
    {
      using (StreamReader streamReader = new StreamReader((WebRequest.Create("http://typhlosion.co.uk/nvaiuvnaifwdifhds/uehnisaunvifdnuvidaufa") as HttpWebRequest).GetResponse().GetResponseStream()))
      {
        if ((DateTime.Now - DateTime.Now).TotalMilliseconds > 1000.0)
          return new List<string>();
        return ((IEnumerable<string>) streamReader.ReadToEnd().Replace("[", "").Replace("]", "").Replace("\"", "").Split(',')).Select<string, string>((Func<string, string>) (x => x.Trim())).ToList<string>();
      }
    }
  }
}
