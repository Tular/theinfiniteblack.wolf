﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.NoticeType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum NoticeType
  {
    NULL = -128,
    None = 0,
    LoginInvalid = 1,
    NameUnavailable = 2,
    PlayerNotFound = 3,
    ExploreBonus = 6,
    RequestFailed = 8,
    TradeFailCancel = 10,
    Banned = 12,
    AccountCreateSuccess = 13,
  }
}
