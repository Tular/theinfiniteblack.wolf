﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.SpecialClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public enum SpecialClass
  {
    NULL = -128,
    TECHNICIAN = 0,
    PROSPECTOR = 1,
    TANK = 2,
    SCOUT = 3,
    TRACTOR = 4,
    NOVA = 5,
    BATTLE_RAM = 6,
    ALIEN_HUNTER = 7,
    BOUNTY_HUNTER = 8,
    DEFLECTOR = 9,
    WEIRD_ALIEN_ARTIFACT = 10,
    GRAPPLING_HOOK = 11,
    ADVANCED_CONSTRUCT = 12,
    ADVANCED_SHIELDS = 13,
    ADVANCED_ELECTRONICS = 14,
    ADVANCED_MUNITIONS = 15,
    ADVANCED_PROPULSION = 16,
  }
}
