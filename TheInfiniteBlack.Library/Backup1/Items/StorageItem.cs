﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.StorageItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library.Items
{
  public class StorageItem : EquipmentItem
  {
    private readonly StorageClass _class;
    private readonly sbyte _baseEp;

    public override ItemType Type
    {
      get
      {
        return ItemType.STORAGE;
      }
    }

    public override int EPCost
    {
      get
      {
        int num = (int) (1 + (int) this._baseEp - this.Rarity);
        if (num > 0)
          return num;
        return 0;
      }
    }

    public override int MaxCreditValue
    {
      get
      {
        if (this.Rarity == ItemRarity.COMMON)
          return 800 + (int) this._baseEp * 50;
        int rarity = (int) this.Rarity;
        return rarity * rarity * rarity * 1600 + (int) this._baseEp * 100 * rarity;
      }
    }

    public override string IconTag
    {
      get
      {
        return this._class.IconTag((int) this._baseEp);
      }
    }

    public StorageClass Class
    {
      get
      {
        return this._class;
      }
    }

    public sbyte BaseEP
    {
      get
      {
        return this._baseEp;
      }
    }

    public override int EPBonus
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.BasicStorage:
            return this.Rarity != ItemRarity.ULTIMATE ? 0 : 1;
          case StorageClass.HoldH2A:
          case StorageClass.HoldH2B:
          case StorageClass.HoldH2G:
          case StorageClass.HoldP2A:
          case StorageClass.HoldP2B:
          case StorageClass.HoldP2G:
          case StorageClass.HoldW2A:
          case StorageClass.HoldW2B:
          case StorageClass.HoldW2G:
          case StorageClass.HoldR2A:
          case StorageClass.HoldR2B:
          case StorageClass.HoldR2G:
          case StorageClass.HoldH202:
          case StorageClass.HoldH203:
          case StorageClass.HoldH204:
          case StorageClass.HoldH205:
          case StorageClass.HoldH206:
          case StorageClass.HoldH207:
          case StorageClass.HoldH208:
          case StorageClass.HoldH209:
          case StorageClass.HoldH210:
          case StorageClass.HoldH211:
          case StorageClass.HoldH212:
          case StorageClass.HoldH213:
          case StorageClass.HoldH214:
          case StorageClass.HoldH215:
          case StorageClass.HoldH216:
          case StorageClass.HoldH217:
          case StorageClass.HoldH218:
          case StorageClass.HoldH219:
          case StorageClass.HoldH220:
          case StorageClass.HoldH221:
          case StorageClass.HoldH222:
          case StorageClass.HoldH223:
          case StorageClass.HoldH224:
          case StorageClass.HoldH225:
          case StorageClass.HoldP202:
          case StorageClass.HoldP203:
          case StorageClass.HoldP204:
          case StorageClass.HoldP205:
          case StorageClass.HoldP206:
          case StorageClass.HoldP207:
          case StorageClass.HoldP208:
          case StorageClass.HoldP209:
          case StorageClass.HoldP210:
          case StorageClass.HoldP211:
          case StorageClass.HoldP212:
          case StorageClass.HoldP213:
          case StorageClass.HoldP214:
          case StorageClass.HoldP215:
          case StorageClass.HoldP216:
          case StorageClass.HoldP217:
          case StorageClass.HoldP218:
          case StorageClass.HoldP219:
          case StorageClass.HoldP220:
          case StorageClass.HoldP221:
          case StorageClass.HoldP222:
          case StorageClass.HoldP223:
          case StorageClass.HoldP224:
          case StorageClass.HoldP225:
          case StorageClass.HoldW202:
          case StorageClass.HoldW203:
          case StorageClass.HoldW204:
          case StorageClass.HoldW205:
          case StorageClass.HoldW206:
          case StorageClass.HoldW207:
          case StorageClass.HoldW208:
          case StorageClass.HoldW209:
          case StorageClass.HoldW210:
          case StorageClass.HoldW211:
          case StorageClass.HoldW212:
          case StorageClass.HoldW213:
          case StorageClass.HoldW214:
          case StorageClass.HoldW215:
          case StorageClass.HoldW216:
          case StorageClass.HoldW217:
          case StorageClass.HoldW218:
          case StorageClass.HoldW219:
          case StorageClass.HoldW220:
          case StorageClass.HoldW221:
          case StorageClass.HoldW222:
          case StorageClass.HoldW223:
          case StorageClass.HoldW224:
          case StorageClass.HoldW225:
          case StorageClass.HoldR202:
          case StorageClass.HoldR203:
          case StorageClass.HoldR204:
          case StorageClass.HoldR205:
          case StorageClass.HoldR206:
          case StorageClass.HoldR207:
          case StorageClass.HoldR208:
          case StorageClass.HoldR209:
          case StorageClass.HoldR210:
          case StorageClass.HoldR211:
          case StorageClass.HoldR212:
          case StorageClass.HoldR213:
          case StorageClass.HoldR214:
          case StorageClass.HoldR215:
          case StorageClass.HoldR216:
          case StorageClass.HoldR217:
          case StorageClass.HoldR218:
          case StorageClass.HoldR219:
          case StorageClass.HoldR220:
          case StorageClass.HoldR221:
          case StorageClass.HoldR222:
          case StorageClass.HoldR223:
          case StorageClass.HoldR224:
          case StorageClass.HoldR225:
            if (this.Rarity == ItemRarity.PRECURSOR)
              return 2;
            return this.Rarity != ItemRarity.ULTIMATE ? 1 : 3;
          case StorageClass.BigCrate:
            if (this.Rarity == ItemRarity.PRECURSOR)
              return 3;
            return this.Rarity != ItemRarity.ULTIMATE ? 2 : 4;
          case StorageClass.ReallyBigCrate:
            if (this.Rarity == ItemRarity.PRECURSOR)
              return 4;
            return this.Rarity != ItemRarity.ULTIMATE ? 3 : 5;
          default:
            if (this.Rarity == ItemRarity.PRECURSOR)
              return 1;
            return this.Rarity != ItemRarity.ULTIMATE ? 0 : 2;
        }
      }
    }

    public override int ResourceCapacity
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldH2A:
          case StorageClass.HoldH2B:
          case StorageClass.HoldH2G:
          case StorageClass.HoldP2A:
          case StorageClass.HoldP2B:
          case StorageClass.HoldP2G:
          case StorageClass.HoldW2A:
          case StorageClass.HoldW2B:
          case StorageClass.HoldW2G:
          case StorageClass.HoldR2A:
          case StorageClass.HoldR2B:
          case StorageClass.HoldR2G:
          case StorageClass.HoldH202:
          case StorageClass.HoldH203:
          case StorageClass.HoldH204:
          case StorageClass.HoldH205:
          case StorageClass.HoldH206:
          case StorageClass.HoldH207:
          case StorageClass.HoldH208:
          case StorageClass.HoldH209:
          case StorageClass.HoldH210:
          case StorageClass.HoldH211:
          case StorageClass.HoldH212:
          case StorageClass.HoldH213:
          case StorageClass.HoldH214:
          case StorageClass.HoldH215:
          case StorageClass.HoldH216:
          case StorageClass.HoldH217:
          case StorageClass.HoldH220:
          case StorageClass.HoldH221:
          case StorageClass.HoldH222:
          case StorageClass.HoldH223:
          case StorageClass.HoldH224:
          case StorageClass.HoldH225:
          case StorageClass.HoldP202:
          case StorageClass.HoldP203:
          case StorageClass.HoldP204:
          case StorageClass.HoldP205:
          case StorageClass.HoldP206:
          case StorageClass.HoldP207:
          case StorageClass.HoldP208:
          case StorageClass.HoldP209:
          case StorageClass.HoldP210:
          case StorageClass.HoldP211:
          case StorageClass.HoldP212:
          case StorageClass.HoldP213:
          case StorageClass.HoldP214:
          case StorageClass.HoldP215:
          case StorageClass.HoldP216:
          case StorageClass.HoldP217:
          case StorageClass.HoldP220:
          case StorageClass.HoldP221:
          case StorageClass.HoldP222:
          case StorageClass.HoldP223:
          case StorageClass.HoldP224:
          case StorageClass.HoldP225:
          case StorageClass.HoldW202:
          case StorageClass.HoldW203:
          case StorageClass.HoldW204:
          case StorageClass.HoldW205:
          case StorageClass.HoldW206:
          case StorageClass.HoldW207:
          case StorageClass.HoldW208:
          case StorageClass.HoldW209:
          case StorageClass.HoldW210:
          case StorageClass.HoldW211:
          case StorageClass.HoldW212:
          case StorageClass.HoldW213:
          case StorageClass.HoldW214:
          case StorageClass.HoldW215:
          case StorageClass.HoldW216:
          case StorageClass.HoldW217:
          case StorageClass.HoldW220:
          case StorageClass.HoldW221:
          case StorageClass.HoldW222:
          case StorageClass.HoldW223:
          case StorageClass.HoldW224:
          case StorageClass.HoldW225:
          case StorageClass.HoldR202:
          case StorageClass.HoldR203:
          case StorageClass.HoldR204:
          case StorageClass.HoldR205:
          case StorageClass.HoldR206:
          case StorageClass.HoldR207:
          case StorageClass.HoldR208:
          case StorageClass.HoldR209:
          case StorageClass.HoldR210:
          case StorageClass.HoldR211:
          case StorageClass.HoldR212:
          case StorageClass.HoldR213:
          case StorageClass.HoldR214:
          case StorageClass.HoldR215:
          case StorageClass.HoldR216:
          case StorageClass.HoldR217:
          case StorageClass.HoldR220:
          case StorageClass.HoldR221:
          case StorageClass.HoldR222:
          case StorageClass.HoldR223:
          case StorageClass.HoldR224:
          case StorageClass.HoldR225:
            return (int) this._baseEp * 3 + (int) (this.Rarity - 1) * 5;
          case StorageClass.HoldH118:
          case StorageClass.HoldP118:
          case StorageClass.HoldW118:
          case StorageClass.HoldR118:
            return (int) this._baseEp * 5 + (int) (this.Rarity - 1) * 5 + (int) this.Rarity * 4;
          case StorageClass.HoldH119:
          case StorageClass.HoldP119:
          case StorageClass.HoldW119:
          case StorageClass.HoldR119:
            return (int) this._baseEp * 5 + (int) (this.Rarity - 1) * 5 + (int) this.Rarity * 2;
          case StorageClass.HoldH218:
          case StorageClass.HoldP218:
          case StorageClass.HoldW218:
          case StorageClass.HoldR218:
            return (int) this._baseEp * 3 + (int) (this.Rarity - 1) * 5 + (int) this.Rarity * 3;
          case StorageClass.HoldH219:
          case StorageClass.HoldP219:
          case StorageClass.HoldW219:
          case StorageClass.HoldR219:
            return (int) ((int) this._baseEp * 3 + (int) (this.Rarity - 1) * 5 + this.Rarity);
          default:
            return (int) this._baseEp * 5 + (int) (this.Rarity - 1) * 5;
        }
      }
    }

    public override int MoveSpeedMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR121:
          case StorageClass.HoldR221:
          case StorageClass.HoldW221:
          case StorageClass.HoldW121:
          case StorageClass.HoldP221:
          case StorageClass.HoldP121:
          case StorageClass.HoldH221:
          case StorageClass.HoldH121:
            return 3100 - (int) this.Rarity * 300;
          case StorageClass.HoldR122:
          case StorageClass.HoldR222:
          case StorageClass.HoldW222:
          case StorageClass.HoldW122:
          case StorageClass.HoldP222:
          case StorageClass.HoldP122:
          case StorageClass.HoldH222:
          case StorageClass.HoldH122:
            return (int) this.Rarity * -200;
          case StorageClass.HoldR203:
          case StorageClass.HoldR103:
          case StorageClass.HoldW203:
          case StorageClass.HoldW103:
          case StorageClass.HoldP203:
          case StorageClass.HoldP103:
          case StorageClass.HoldH203:
          case StorageClass.HoldH103:
            return (int) this.Rarity * -150;
          case StorageClass.HoldR1A:
          case StorageClass.HoldR2A:
            return (int) this.Rarity * -250;
          default:
            return 0;
        }
      }
    }

    public override float MetalRepairMod
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR219:
          case StorageClass.HoldR225:
          case StorageClass.HoldR125:
          case StorageClass.HoldW225:
          case StorageClass.HoldR119:
          case StorageClass.HoldW125:
          case StorageClass.HoldW219:
          case StorageClass.HoldP225:
          case StorageClass.HoldW119:
          case StorageClass.HoldP125:
          case StorageClass.HoldP219:
          case StorageClass.HoldH225:
          case StorageClass.HoldP119:
          case StorageClass.HoldH125:
          case StorageClass.HoldH219:
          case StorageClass.HoldH119:
            return (float) this.Rarity * 0.005f;
          case StorageClass.HoldR204:
          case StorageClass.HoldR104:
          case StorageClass.HoldW204:
          case StorageClass.HoldW104:
          case StorageClass.HoldP204:
          case StorageClass.HoldP104:
          case StorageClass.HoldH204:
          case StorageClass.HoldH104:
            return (float) this.Rarity * 0.01f;
          case StorageClass.HoldH1B:
          case StorageClass.HoldH2B:
            return (float) this.Rarity * 0.0125f;
          default:
            return 0.0f;
        }
      }
    }

    public override float OutgoingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR124:
          case StorageClass.HoldR224:
          case StorageClass.HoldW224:
          case StorageClass.HoldW124:
          case StorageClass.HoldP224:
          case StorageClass.HoldP124:
          case StorageClass.HoldH224:
          case StorageClass.HoldH124:
            return (float) this.Rarity * (11f / 1000f);
          case StorageClass.HoldR210:
          case StorageClass.HoldR110:
          case StorageClass.HoldW210:
          case StorageClass.HoldW110:
          case StorageClass.HoldP210:
          case StorageClass.HoldP110:
          case StorageClass.HoldH210:
          case StorageClass.HoldH110:
            return (float) this.Rarity * 0.007f;
          case StorageClass.HoldR1G:
          case StorageClass.HoldR2G:
            return (float) this.Rarity * 0.013f;
          default:
            return 0.0f;
        }
      }
    }

    public override float IncomingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR118:
          case StorageClass.HoldR218:
          case StorageClass.HoldW218:
          case StorageClass.HoldW118:
          case StorageClass.HoldP218:
          case StorageClass.HoldP118:
          case StorageClass.HoldH218:
          case StorageClass.HoldH118:
            return (float) this.Rarity * -0.01f;
          case StorageClass.HoldR211:
          case StorageClass.HoldR111:
          case StorageClass.HoldW211:
          case StorageClass.HoldW111:
          case StorageClass.HoldP211:
          case StorageClass.HoldP111:
          case StorageClass.HoldH211:
          case StorageClass.HoldH111:
            return (float) this.Rarity * -0.01f;
          case StorageClass.HoldW1B:
          case StorageClass.HoldW2B:
            return (float) this.Rarity * -0.014f;
          default:
            return 0.0f;
        }
      }
    }

    public override float XpMod
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR125:
          case StorageClass.HoldR225:
          case StorageClass.HoldW225:
          case StorageClass.HoldW125:
          case StorageClass.HoldP225:
          case StorageClass.HoldP125:
          case StorageClass.HoldH225:
          case StorageClass.HoldH125:
            return (float) this.Rarity * 0.005f;
          case StorageClass.HoldR212:
          case StorageClass.HoldR112:
          case StorageClass.HoldW212:
          case StorageClass.HoldW112:
          case StorageClass.HoldP212:
          case StorageClass.HoldP112:
          case StorageClass.HoldH212:
          case StorageClass.HoldH112:
            return (float) (0.00999999977648258 + (double) this.Rarity * 0.00999999977648258);
          case StorageClass.HoldP1A:
          case StorageClass.HoldP2A:
            return (float) (0.0299999993294477 + (double) this.Rarity * 0.00999999977648258);
          default:
            return 0.0f;
        }
      }
    }

    public override int MaxHullBonus
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR124:
          case StorageClass.HoldR224:
          case StorageClass.HoldW224:
          case StorageClass.HoldW124:
          case StorageClass.HoldP224:
          case StorageClass.HoldP124:
          case StorageClass.HoldH224:
          case StorageClass.HoldH124:
            return (int) this.Rarity * 40 - 320;
          case StorageClass.HoldR202:
          case StorageClass.HoldR102:
          case StorageClass.HoldW202:
          case StorageClass.HoldW102:
          case StorageClass.HoldP202:
          case StorageClass.HoldP102:
          case StorageClass.HoldH202:
          case StorageClass.HoldH102:
            return (int) this.Rarity * 75;
          case StorageClass.HoldR220:
          case StorageClass.HoldR120:
          case StorageClass.HoldW220:
          case StorageClass.HoldW120:
          case StorageClass.HoldP120:
          case StorageClass.HoldP220:
          case StorageClass.HoldH220:
          case StorageClass.HoldH120:
            return (int) this.Rarity * 125;
          case StorageClass.BasicStorage:
            if (this.Rarity > ItemRarity.ULTRA_RARE)
              return (int) (this.Rarity - 4) * 150;
            return 0;
          case StorageClass.HoldH1A:
          case StorageClass.HoldH2A:
            return (int) this.Rarity * 150;
          default:
            return 0;
        }
      }
    }

    public override float CriticalDamageMod
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR123:
          case StorageClass.HoldR223:
          case StorageClass.HoldW223:
          case StorageClass.HoldW123:
          case StorageClass.HoldP223:
          case StorageClass.HoldP123:
          case StorageClass.HoldH223:
          case StorageClass.HoldH123:
          case StorageClass.HoldR1B:
          case StorageClass.HoldR2B:
            return (float) this.Rarity * 0.03f;
          case StorageClass.HoldR206:
          case StorageClass.HoldR106:
          case StorageClass.HoldW206:
          case StorageClass.HoldW106:
          case StorageClass.HoldP206:
          case StorageClass.HoldP106:
          case StorageClass.HoldH206:
          case StorageClass.HoldH106:
            return (float) this.Rarity * 0.02f;
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalChance
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR121:
          case StorageClass.HoldR221:
          case StorageClass.HoldW221:
          case StorageClass.HoldW121:
          case StorageClass.HoldP221:
          case StorageClass.HoldP121:
          case StorageClass.HoldH221:
          case StorageClass.HoldH121:
            return (float) this.Rarity * 2.5f;
          case StorageClass.HoldR123:
          case StorageClass.HoldR223:
          case StorageClass.HoldW223:
          case StorageClass.HoldW123:
          case StorageClass.HoldP223:
          case StorageClass.HoldP123:
          case StorageClass.HoldH223:
          case StorageClass.HoldH123:
            return (float) this.Rarity * 0.5f;
          case StorageClass.HoldR205:
          case StorageClass.HoldR105:
          case StorageClass.HoldW205:
          case StorageClass.HoldW105:
          case StorageClass.HoldP105:
          case StorageClass.HoldP205:
          case StorageClass.HoldH205:
          case StorageClass.HoldH105:
            return (float) this.Rarity * 1.25f;
          case StorageClass.HoldW1A:
          case StorageClass.HoldW2A:
            return (float) this.Rarity * 2f;
          case StorageClass.HoldR1B:
          case StorageClass.HoldR2B:
            return (float) this.Rarity * 1f;
          default:
            return 0.0f;
        }
      }
    }

    public override float EvasionChance
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldP107:
          case StorageClass.HoldP207:
          case StorageClass.HoldW107:
          case StorageClass.HoldW207:
          case StorageClass.HoldH207:
          case StorageClass.HoldH107:
            return (float) this.Rarity * 3f;
          case StorageClass.HoldP120:
          case StorageClass.HoldP122:
          case StorageClass.HoldP220:
          case StorageClass.HoldP222:
          case StorageClass.HoldW120:
          case StorageClass.HoldW122:
          case StorageClass.HoldW220:
          case StorageClass.HoldW222:
          case StorageClass.HoldH220:
          case StorageClass.HoldH222:
          case StorageClass.HoldH120:
          case StorageClass.HoldH122:
            return (float) ((double) this.Rarity * 1.0 - 11.0);
          case StorageClass.HoldP123:
          case StorageClass.HoldP223:
          case StorageClass.HoldW123:
          case StorageClass.HoldW223:
          case StorageClass.HoldH223:
          case StorageClass.HoldH123:
            return (float) ((double) this.Rarity * 1.0 - 15.0);
          case StorageClass.HoldR102:
          case StorageClass.HoldR103:
          case StorageClass.HoldR104:
          case StorageClass.HoldR105:
          case StorageClass.HoldR106:
          case StorageClass.HoldR108:
          case StorageClass.HoldR109:
          case StorageClass.HoldR110:
          case StorageClass.HoldR111:
          case StorageClass.HoldR112:
          case StorageClass.HoldR113:
          case StorageClass.HoldR114:
          case StorageClass.HoldR115:
          case StorageClass.HoldR116:
          case StorageClass.HoldR117:
          case StorageClass.HoldR118:
          case StorageClass.HoldR119:
          case StorageClass.HoldR121:
          case StorageClass.HoldR124:
          case StorageClass.HoldR125:
          case StorageClass.HoldR202:
          case StorageClass.HoldR203:
          case StorageClass.HoldR204:
          case StorageClass.HoldR205:
          case StorageClass.HoldR206:
          case StorageClass.HoldR208:
          case StorageClass.HoldR209:
          case StorageClass.HoldR210:
          case StorageClass.HoldR211:
          case StorageClass.HoldR212:
          case StorageClass.HoldR213:
          case StorageClass.HoldR214:
          case StorageClass.HoldR215:
          case StorageClass.HoldR216:
          case StorageClass.HoldR217:
          case StorageClass.HoldR218:
          case StorageClass.HoldR219:
          case StorageClass.HoldR221:
          case StorageClass.HoldR224:
          case StorageClass.HoldR225:
            return (float) (1.0 + (double) this.Rarity * 1.0);
          case StorageClass.HoldR107:
          case StorageClass.HoldR207:
            return (float) this.Rarity * 4f;
          case StorageClass.HoldR120:
          case StorageClass.HoldR122:
          case StorageClass.HoldR220:
          case StorageClass.HoldR222:
            return (float) ((double) this.Rarity * 1.0 - 7.0);
          case StorageClass.HoldR123:
          case StorageClass.HoldR223:
            return (float) ((double) this.Rarity * 1.0 - 10.0);
          case StorageClass.BasicStorage:
            return (float) (((double) this.Rarity - 1.0) * 2.5);
          case StorageClass.HoldP1G:
          case StorageClass.HoldP2G:
            return (float) this.Rarity * 2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashChance
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldW209:
          case StorageClass.HoldR109:
          case StorageClass.HoldR209:
          case StorageClass.HoldP209:
          case StorageClass.HoldW109:
          case StorageClass.HoldH209:
          case StorageClass.HoldP109:
          case StorageClass.HoldH109:
            return (float) this.Rarity * 3.5f;
          case StorageClass.BasicStorage:
            if (this.Rarity > ItemRarity.RARE)
              return (float) (((double) this.Rarity - 3.0) * 2.0);
            return 0.0f;
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleChance
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldW215:
          case StorageClass.HoldR115:
          case StorageClass.HoldR215:
          case StorageClass.HoldP215:
          case StorageClass.HoldW115:
          case StorageClass.HoldH215:
          case StorageClass.HoldP115:
          case StorageClass.HoldH115:
            return (float) this.Rarity;
          case StorageClass.HoldP1B:
          case StorageClass.HoldP2B:
            return 3f + (float) this.Rarity;
          default:
            return 0.0f;
        }
      }
    }

    public override float StunChance
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldW213:
          case StorageClass.HoldR113:
          case StorageClass.HoldR213:
          case StorageClass.HoldP213:
          case StorageClass.HoldW113:
          case StorageClass.HoldH213:
          case StorageClass.HoldP113:
          case StorageClass.HoldH113:
            return (float) (1.0 + (double) this.Rarity * 0.5);
          case StorageClass.HoldW1G:
          case StorageClass.HoldW2G:
            return (float) this.Rarity;
          default:
            return 0.0f;
        }
      }
    }

    public override int StunDurationMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR114:
          case StorageClass.HoldR214:
          case StorageClass.HoldW114:
          case StorageClass.HoldW214:
          case StorageClass.HoldP114:
          case StorageClass.HoldP214:
          case StorageClass.HoldH114:
          case StorageClass.HoldH214:
            return (int) this.Rarity * 500 + 500;
          default:
            return 0;
        }
      }
    }

    public override float HitChance
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR122:
          case StorageClass.HoldR222:
          case StorageClass.HoldW222:
          case StorageClass.HoldW122:
          case StorageClass.HoldP222:
          case StorageClass.HoldP122:
          case StorageClass.HoldH222:
          case StorageClass.HoldH122:
          case StorageClass.HoldP1G:
          case StorageClass.HoldP2G:
            return (float) this.Rarity * 2f;
          case StorageClass.HoldR208:
          case StorageClass.HoldR108:
          case StorageClass.HoldW208:
          case StorageClass.HoldW108:
          case StorageClass.HoldP108:
          case StorageClass.HoldP208:
          case StorageClass.HoldH208:
          case StorageClass.HoldH108:
            return (float) this.Rarity * 2.5f;
          case StorageClass.HoldH1G:
          case StorageClass.HoldH2G:
            return (float) this.Rarity * 4f;
          default:
            return 0.0f;
        }
      }
    }

    public override float StunResist
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldP117:
          case StorageClass.HoldP217:
          case StorageClass.HoldR117:
          case StorageClass.HoldR217:
          case StorageClass.HoldP1G:
          case StorageClass.HoldH117:
          case StorageClass.HoldH217:
            return (float) (2.0 + (double) this._baseEp * 2.0);
          case StorageClass.HoldW102:
          case StorageClass.HoldW103:
          case StorageClass.HoldW104:
          case StorageClass.HoldW105:
          case StorageClass.HoldW106:
          case StorageClass.HoldW107:
          case StorageClass.HoldW108:
          case StorageClass.HoldW109:
          case StorageClass.HoldW110:
          case StorageClass.HoldW111:
          case StorageClass.HoldW112:
          case StorageClass.HoldW113:
          case StorageClass.HoldW114:
          case StorageClass.HoldW115:
          case StorageClass.HoldW116:
          case StorageClass.HoldW118:
          case StorageClass.HoldW119:
          case StorageClass.HoldW120:
          case StorageClass.HoldW121:
          case StorageClass.HoldW122:
          case StorageClass.HoldW123:
          case StorageClass.HoldW124:
          case StorageClass.HoldW125:
          case StorageClass.HoldW202:
          case StorageClass.HoldW203:
          case StorageClass.HoldW204:
          case StorageClass.HoldW205:
          case StorageClass.HoldW206:
          case StorageClass.HoldW207:
          case StorageClass.HoldW208:
          case StorageClass.HoldW209:
          case StorageClass.HoldW210:
          case StorageClass.HoldW211:
          case StorageClass.HoldW212:
          case StorageClass.HoldW213:
          case StorageClass.HoldW214:
          case StorageClass.HoldW215:
          case StorageClass.HoldW216:
          case StorageClass.HoldW218:
          case StorageClass.HoldW219:
          case StorageClass.HoldW220:
          case StorageClass.HoldW221:
          case StorageClass.HoldW222:
          case StorageClass.HoldW223:
          case StorageClass.HoldW224:
          case StorageClass.HoldW225:
            return 1f + (float) this._baseEp;
          case StorageClass.HoldW117:
          case StorageClass.HoldW217:
            return (float) (3.0 + (double) this._baseEp * 3.0);
          case StorageClass.HoldH1A:
          case StorageClass.HoldH2A:
          case StorageClass.HoldH1B:
          case StorageClass.HoldH2B:
          case StorageClass.HoldH1G:
          case StorageClass.HoldH2G:
          case StorageClass.HoldP1A:
          case StorageClass.HoldP2A:
          case StorageClass.HoldP1B:
          case StorageClass.HoldP2B:
          case StorageClass.HoldP2G:
          case StorageClass.HoldW1A:
          case StorageClass.HoldW2A:
          case StorageClass.HoldW1B:
          case StorageClass.HoldW2B:
          case StorageClass.HoldW1G:
          case StorageClass.HoldW2G:
          case StorageClass.HoldR1A:
          case StorageClass.HoldR2A:
          case StorageClass.HoldR1B:
          case StorageClass.HoldR2B:
          case StorageClass.HoldR1G:
          case StorageClass.HoldR2G:
            return 3f + (float) this._baseEp;
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleResist
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR116:
          case StorageClass.HoldR216:
          case StorageClass.HoldP2G:
          case StorageClass.HoldH116:
          case StorageClass.HoldH216:
          case StorageClass.HoldW116:
          case StorageClass.HoldW216:
            return (float) (2.0 + (double) this._baseEp * 2.0);
          case StorageClass.HoldH1A:
          case StorageClass.HoldH2A:
          case StorageClass.HoldH1B:
          case StorageClass.HoldH2B:
          case StorageClass.HoldH1G:
          case StorageClass.HoldH2G:
          case StorageClass.HoldP1A:
          case StorageClass.HoldP2A:
          case StorageClass.HoldP1B:
          case StorageClass.HoldP2B:
          case StorageClass.HoldP1G:
          case StorageClass.HoldW1A:
          case StorageClass.HoldW2A:
          case StorageClass.HoldW1B:
          case StorageClass.HoldW2B:
          case StorageClass.HoldW1G:
          case StorageClass.HoldW2G:
          case StorageClass.HoldR1A:
          case StorageClass.HoldR2A:
          case StorageClass.HoldR1B:
          case StorageClass.HoldR2B:
          case StorageClass.HoldR1G:
          case StorageClass.HoldR2G:
            return 3f + (float) this._baseEp;
          case StorageClass.HoldP102:
          case StorageClass.HoldP103:
          case StorageClass.HoldP104:
          case StorageClass.HoldP105:
          case StorageClass.HoldP106:
          case StorageClass.HoldP107:
          case StorageClass.HoldP108:
          case StorageClass.HoldP109:
          case StorageClass.HoldP110:
          case StorageClass.HoldP111:
          case StorageClass.HoldP112:
          case StorageClass.HoldP113:
          case StorageClass.HoldP114:
          case StorageClass.HoldP115:
          case StorageClass.HoldP117:
          case StorageClass.HoldP118:
          case StorageClass.HoldP119:
          case StorageClass.HoldP120:
          case StorageClass.HoldP121:
          case StorageClass.HoldP122:
          case StorageClass.HoldP123:
          case StorageClass.HoldP124:
          case StorageClass.HoldP125:
          case StorageClass.HoldP202:
          case StorageClass.HoldP203:
          case StorageClass.HoldP204:
          case StorageClass.HoldP205:
          case StorageClass.HoldP206:
          case StorageClass.HoldP207:
          case StorageClass.HoldP208:
          case StorageClass.HoldP209:
          case StorageClass.HoldP210:
          case StorageClass.HoldP211:
          case StorageClass.HoldP212:
          case StorageClass.HoldP213:
          case StorageClass.HoldP214:
          case StorageClass.HoldP215:
          case StorageClass.HoldP217:
          case StorageClass.HoldP218:
          case StorageClass.HoldP219:
          case StorageClass.HoldP220:
          case StorageClass.HoldP221:
          case StorageClass.HoldP222:
          case StorageClass.HoldP223:
          case StorageClass.HoldP224:
          case StorageClass.HoldP225:
            return 1f + (float) this._baseEp;
          case StorageClass.HoldP116:
          case StorageClass.HoldP216:
            return (float) (3.0 + (double) this._baseEp * 3.0);
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalResist
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldP116:
          case StorageClass.HoldP216:
          case StorageClass.HoldW116:
          case StorageClass.HoldW216:
          case StorageClass.HoldP1G:
          case StorageClass.HoldH116:
          case StorageClass.HoldH216:
            return (float) (2.0 + (double) this._baseEp * 2.0);
          case StorageClass.HoldR102:
          case StorageClass.HoldR103:
          case StorageClass.HoldR104:
          case StorageClass.HoldR105:
          case StorageClass.HoldR106:
          case StorageClass.HoldR107:
          case StorageClass.HoldR108:
          case StorageClass.HoldR109:
          case StorageClass.HoldR110:
          case StorageClass.HoldR111:
          case StorageClass.HoldR112:
          case StorageClass.HoldR113:
          case StorageClass.HoldR114:
          case StorageClass.HoldR115:
          case StorageClass.HoldR117:
          case StorageClass.HoldR118:
          case StorageClass.HoldR119:
          case StorageClass.HoldR120:
          case StorageClass.HoldR121:
          case StorageClass.HoldR122:
          case StorageClass.HoldR123:
          case StorageClass.HoldR124:
          case StorageClass.HoldR125:
          case StorageClass.HoldR202:
          case StorageClass.HoldR203:
          case StorageClass.HoldR204:
          case StorageClass.HoldR205:
          case StorageClass.HoldR206:
          case StorageClass.HoldR207:
          case StorageClass.HoldR208:
          case StorageClass.HoldR209:
          case StorageClass.HoldR210:
          case StorageClass.HoldR211:
          case StorageClass.HoldR212:
          case StorageClass.HoldR213:
          case StorageClass.HoldR214:
          case StorageClass.HoldR215:
          case StorageClass.HoldR217:
          case StorageClass.HoldR218:
          case StorageClass.HoldR219:
          case StorageClass.HoldR220:
          case StorageClass.HoldR221:
          case StorageClass.HoldR222:
          case StorageClass.HoldR223:
          case StorageClass.HoldR224:
          case StorageClass.HoldR225:
            return 1f + (float) this._baseEp;
          case StorageClass.HoldR116:
          case StorageClass.HoldR216:
            return (float) (3.0 + (double) this._baseEp * 3.0);
          case StorageClass.HoldH1A:
          case StorageClass.HoldH2A:
          case StorageClass.HoldH1B:
          case StorageClass.HoldH2B:
          case StorageClass.HoldH1G:
          case StorageClass.HoldH2G:
          case StorageClass.HoldP1A:
          case StorageClass.HoldP2A:
          case StorageClass.HoldP1B:
          case StorageClass.HoldP2B:
          case StorageClass.HoldP2G:
          case StorageClass.HoldW1A:
          case StorageClass.HoldW2A:
          case StorageClass.HoldW1B:
          case StorageClass.HoldW2B:
          case StorageClass.HoldW1G:
          case StorageClass.HoldW2G:
          case StorageClass.HoldR1A:
          case StorageClass.HoldR2A:
          case StorageClass.HoldR1B:
          case StorageClass.HoldR2B:
          case StorageClass.HoldR1G:
          case StorageClass.HoldR2G:
            return 3f + (float) this._baseEp;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashResist
    {
      get
      {
        switch (this._class)
        {
          case StorageClass.HoldR117:
          case StorageClass.HoldR217:
          case StorageClass.HoldP2G:
          case StorageClass.HoldP117:
          case StorageClass.HoldP217:
          case StorageClass.HoldW117:
          case StorageClass.HoldW217:
            return (float) (2.0 + (double) this._baseEp * 2.0);
          case StorageClass.HoldH1A:
          case StorageClass.HoldH2A:
          case StorageClass.HoldH1B:
          case StorageClass.HoldH2B:
          case StorageClass.HoldH1G:
          case StorageClass.HoldH2G:
          case StorageClass.HoldP1A:
          case StorageClass.HoldP2A:
          case StorageClass.HoldP1B:
          case StorageClass.HoldP2B:
          case StorageClass.HoldP1G:
          case StorageClass.HoldW1A:
          case StorageClass.HoldW2A:
          case StorageClass.HoldW1B:
          case StorageClass.HoldW2B:
          case StorageClass.HoldW1G:
          case StorageClass.HoldW2G:
          case StorageClass.HoldR1A:
          case StorageClass.HoldR2A:
          case StorageClass.HoldR1B:
          case StorageClass.HoldR2B:
          case StorageClass.HoldR1G:
          case StorageClass.HoldR2G:
            return 3f + (float) this._baseEp;
          case StorageClass.HoldH102:
          case StorageClass.HoldH103:
          case StorageClass.HoldH104:
          case StorageClass.HoldH105:
          case StorageClass.HoldH106:
          case StorageClass.HoldH107:
          case StorageClass.HoldH108:
          case StorageClass.HoldH109:
          case StorageClass.HoldH110:
          case StorageClass.HoldH111:
          case StorageClass.HoldH112:
          case StorageClass.HoldH113:
          case StorageClass.HoldH114:
          case StorageClass.HoldH115:
          case StorageClass.HoldH116:
          case StorageClass.HoldH118:
          case StorageClass.HoldH119:
          case StorageClass.HoldH120:
          case StorageClass.HoldH121:
          case StorageClass.HoldH122:
          case StorageClass.HoldH123:
          case StorageClass.HoldH124:
          case StorageClass.HoldH125:
          case StorageClass.HoldH202:
          case StorageClass.HoldH203:
          case StorageClass.HoldH204:
          case StorageClass.HoldH205:
          case StorageClass.HoldH206:
          case StorageClass.HoldH207:
          case StorageClass.HoldH208:
          case StorageClass.HoldH209:
          case StorageClass.HoldH210:
          case StorageClass.HoldH211:
          case StorageClass.HoldH212:
          case StorageClass.HoldH213:
          case StorageClass.HoldH214:
          case StorageClass.HoldH215:
          case StorageClass.HoldH216:
          case StorageClass.HoldH218:
          case StorageClass.HoldH219:
          case StorageClass.HoldH220:
          case StorageClass.HoldH221:
          case StorageClass.HoldH222:
          case StorageClass.HoldH223:
          case StorageClass.HoldH224:
          case StorageClass.HoldH225:
            return 1f + (float) this._baseEp;
          case StorageClass.HoldH117:
          case StorageClass.HoldH217:
            return (float) (3.0 + (double) this._baseEp * 3.0);
          default:
            return 0.0f;
        }
      }
    }

    protected internal StorageItem(StorageClass storageClass, sbyte baseEp, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(rarity, durability, noDrop, bindOnEquip)
    {
      this._class = storageClass;
      this._baseEp = baseEp;
    }

    public override void Write(IByteBuffer output)
    {
      base.Write(output);
      output.Write((sbyte) this._class);
      output.Write(this._baseEp);
    }

    public override void AppendName(StringBuilder sb)
    {
      sb.Append(this._class.Name());
      sb.Append(" ");
      sb.Append(Util.NUMERALS[(int) this._baseEp]);
    }

    public override EquipmentItem GetUpgrade(EngineeringType type)
    {
      return (EquipmentItem) StorageCache.GetUpgrade(type, this);
    }

    public override bool CanUpgrade(EngineeringType type)
    {
      if (this.Rarity <= ItemRarity.COMMON)
        return false;
      switch (type)
      {
        case EngineeringType.DECONSTRUCT:
          return true;
        case EngineeringType.REPAIR:
          if ((int) this.Durability > 1)
            return (int) this.Durability < 100;
          return false;
        case EngineeringType.RANK_UP:
          return (int) this._baseEp < 30;
        case EngineeringType.RANK_DOWN:
          return this.EPCost > 1;
        case EngineeringType.RARITY:
          return this.Rarity < ItemRarity.ULTIMATE;
        case EngineeringType.UNBIND:
          return this.NoDrop;
        default:
          return false;
      }
    }

    public override void AppendSubTitle(StringBuilder sb)
    {
      sb.Append(this.Rarity.Name());
      sb.Append(" Storage");
    }

    public override void AppendDescription(StringBuilder sb)
    {
    }
  }
}
