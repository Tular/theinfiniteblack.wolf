﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.HarvesterCache
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Library.Items
{
  public static class HarvesterCache
  {
    private static readonly Dictionary<HarvesterClass, Dictionary<ItemRarity, Dictionary<sbyte, HarvesterItem[]>>> _cache = new Dictionary<HarvesterClass, Dictionary<ItemRarity, Dictionary<sbyte, HarvesterItem[]>>>();

    public static HarvesterItem Get(IByteBuffer input)
    {
      ItemRarity rarity = (ItemRarity) input.Get();
      sbyte durability = input.Get();
      bool noDrop = input.GetBool();
      bool bindOnEquip = !noDrop && input.GetBool();
      return HarvesterCache.Get((HarvesterClass) input.Get(), rarity, durability, noDrop, bindOnEquip);
    }

    public static HarvesterItem Get(HarvesterClass type, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      lock (HarvesterCache._cache)
      {
        Dictionary<ItemRarity, Dictionary<sbyte, HarvesterItem[]>> dictionary1;
        if (!HarvesterCache._cache.TryGetValue(type, out dictionary1))
        {
          dictionary1 = new Dictionary<ItemRarity, Dictionary<sbyte, HarvesterItem[]>>();
          HarvesterCache._cache[type] = dictionary1;
        }
        Dictionary<sbyte, HarvesterItem[]> dictionary2;
        if (!dictionary1.TryGetValue(rarity, out dictionary2))
        {
          dictionary2 = new Dictionary<sbyte, HarvesterItem[]>();
          dictionary1[rarity] = dictionary2;
        }
        HarvesterItem[] harvesterItemArray;
        if (!dictionary2.TryGetValue(durability, out harvesterItemArray))
        {
          harvesterItemArray = new HarvesterItem[4];
          dictionary2[durability] = harvesterItemArray;
        }
        int index = noDrop ? (bindOnEquip ? 0 : 1) : (bindOnEquip ? 2 : 3);
        HarvesterItem harvesterItem = harvesterItemArray[index];
        if (harvesterItem == null)
        {
          harvesterItem = new HarvesterItem(type, rarity, durability, noDrop, bindOnEquip);
          harvesterItemArray[index] = harvesterItem;
        }
        return harvesterItem;
      }
    }

    public static HarvesterItem GetUpgrade(EngineeringType type, HarvesterItem item)
    {
      if (!item.CanUpgrade(type))
        return (HarvesterItem) null;
      if (type == EngineeringType.REPAIR)
        return HarvesterCache.Get(item.Class, item.Rarity, (int) item.Durability + 10 >= 100 ? (sbyte) 100 : (sbyte) ((int) item.Durability + 10), item.NoDrop, item.BindOnEquip);
      if (type != EngineeringType.RARITY)
        return (HarvesterItem) null;
      return HarvesterCache.Get(item.Class, item.Rarity + 1, item.Durability, item.NoDrop, item.BindOnEquip);
    }
  }
}
