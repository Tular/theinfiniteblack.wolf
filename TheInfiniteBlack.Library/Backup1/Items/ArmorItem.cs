﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ArmorItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library.Items
{
  public class ArmorItem : EquipmentItem
  {
    private readonly ArmorClass _class;
    private readonly sbyte _baseEp;

    public override ItemType Type
    {
      get
      {
        return ItemType.ARMOR;
      }
    }

    public override int MaxCreditValue
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.CITADEL_GAMBIT:
          case ArmorClass.AJAX_GAMBIT:
          case ArmorClass.AEGIS_GAMBIT:
          case ArmorClass.KISMET_GAMBIT:
            if (this.Rarity == ItemRarity.COMMON)
              return 2200 + (int) this._baseEp * 50;
            int rarity1 = (int) this.Rarity;
            return rarity1 * rarity1 * rarity1 * 4400 + (int) this._baseEp * 100 * rarity1;
          default:
            if (this.Rarity == ItemRarity.COMMON)
              return 1200 + (int) this._baseEp * 50;
            int rarity2 = (int) this.Rarity;
            return rarity2 * rarity2 * rarity2 * 2400 + (int) this._baseEp * 100 * rarity2;
        }
      }
    }

    public override int EPCost
    {
      get
      {
        int num = (int) ((int) this._baseEp - this.Rarity + 1);
        if (num > 0)
          return num;
        return 0;
      }
    }

    public override string IconTag
    {
      get
      {
        return this._class.IconTag((int) this._baseEp);
      }
    }

    public override int EPBonus
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.CITADEL_GAMBIT:
          case ArmorClass.AJAX_GAMBIT:
          case ArmorClass.AEGIS_GAMBIT:
          case ArmorClass.KISMET_GAMBIT:
            return 1;
          default:
            return 0;
        }
      }
    }

    public override int MoveSpeedMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.TITANIUM_HULL:
          case ArmorClass.EXOPRENE_SHIELD:
          case ArmorClass.DIAMOND_ARMOR:
            return (int) this._baseEp * -250;
          case ArmorClass.CARBIDE_HULL:
          case ArmorClass.HOLOCRINE_SHIELD:
          case ArmorClass.OSMIUM_ARMOR:
            return (int) this._baseEp * 250;
          default:
            return 0;
        }
      }
    }

    public override float MetalRepairMod
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.EXOPRENE_SHIELD:
          case ArmorClass.CYTOPLAST_SHIELD:
          case ArmorClass.HOLOCRINE_SHIELD:
            return (float) this.Rarity * 0.07f;
          default:
            return 0.0f;
        }
      }
    }

    public override float OutgoingDamageMod
    {
      get
      {
        if (this._class != ArmorClass.AEGIS_GAMBIT)
          return 0.0f;
        return (float) (((double) this._baseEp + (double) this.Rarity) / 270.0);
      }
    }

    public override float IncomingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.TITANIUM_HULL:
          case ArmorClass.COMPOSITE_HULL:
          case ArmorClass.CARBIDE_HULL:
            if ((double) this.Rarity > 3.0)
              return (float) (((double) this.Rarity - 3.0) * -0.0199999995529652);
            return 0.0f;
          case ArmorClass.CITADEL_GAMBIT:
          case ArmorClass.KISMET_GAMBIT:
            float num1 = (float) this._baseEp * 2f - (float) this.Rarity;
            if ((double) num1 > 1.0000000116861E-07)
              return num1 * 0.01f;
            return 0.0f;
          case ArmorClass.AJAX_GAMBIT:
          case ArmorClass.AEGIS_GAMBIT:
            float num2 = (float) this._baseEp * 2f - (float) this.Rarity;
            if ((double) num2 > 1.0000000116861E-07)
              return num2 * 0.005f;
            return 0.0f;
          default:
            return 0.0f;
        }
      }
    }

    public override int MaxHullBonus
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.DIAMOND_ARMOR:
            return 220 * (int) this._baseEp;
          case ArmorClass.THORIUM_ARMOR:
            return 315 * (int) this._baseEp;
          case ArmorClass.OSMIUM_ARMOR:
            return 410 * (int) this._baseEp;
          case ArmorClass.CITADEL_GAMBIT:
          case ArmorClass.AJAX_GAMBIT:
          case ArmorClass.CYTOPLAST_SHIELD:
            return 305 * (int) this._baseEp;
          case ArmorClass.AEGIS_GAMBIT:
            return 275 * (int) this._baseEp;
          case ArmorClass.KISMET_GAMBIT:
            return 50 * (int) this._baseEp;
          case ArmorClass.TITANIUM_HULL:
            return 215 * (int) this._baseEp;
          case ArmorClass.COMPOSITE_HULL:
            return 310 * (int) this._baseEp;
          case ArmorClass.CARBIDE_HULL:
            return 405 * (int) this._baseEp;
          case ArmorClass.EXOPRENE_SHIELD:
            return 210 * (int) this._baseEp;
          case ArmorClass.HOLOCRINE_SHIELD:
            return 400 * (int) this._baseEp;
          default:
            return 0;
        }
      }
    }

    public override float CriticalDamageMod
    {
      get
      {
        if (this._class != ArmorClass.KISMET_GAMBIT)
          return 0.0f;
        return (float) (((double) this._baseEp + (double) this.Rarity) * 0.0299999993294477);
      }
    }

    public override float CriticalChance
    {
      get
      {
        if (this._class != ArmorClass.CITADEL_GAMBIT)
          return 0.0f;
        return (float) this._baseEp + (float) this.Rarity;
      }
    }

    public override float EvasionChance
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.TITANIUM_HULL:
          case ArmorClass.EXOPRENE_SHIELD:
          case ArmorClass.DIAMOND_ARMOR:
            return (float) this._baseEp / 2f;
          case ArmorClass.CARBIDE_HULL:
          case ArmorClass.HOLOCRINE_SHIELD:
          case ArmorClass.OSMIUM_ARMOR:
            return (float) -this._baseEp;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashChance
    {
      get
      {
        if (this._class != ArmorClass.AEGIS_GAMBIT)
          return 0.0f;
        return (float) this._baseEp + (float) this.Rarity;
      }
    }

    public override float GrappleChance
    {
      get
      {
        if (this._class != ArmorClass.AJAX_GAMBIT)
          return 0.0f;
        return (float) (((double) this._baseEp + (double) this.Rarity) / 2.0);
      }
    }

    public override float StunChance
    {
      get
      {
        if (this._class != ArmorClass.AJAX_GAMBIT)
          return 0.0f;
        return (float) (((double) this._baseEp + (double) this.Rarity) / 2.5);
      }
    }

    public override float HitChance
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.CITADEL_GAMBIT:
          case ArmorClass.KISMET_GAMBIT:
            return (float) (((double) this._baseEp + (double) this.Rarity) / 2.0);
          default:
            return 0.0f;
        }
      }
    }

    public override float StunResist
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.DIAMOND_ARMOR:
          case ArmorClass.THORIUM_ARMOR:
          case ArmorClass.OSMIUM_ARMOR:
            return (float) (((double) this._baseEp + (double) this.Rarity) / 2.0);
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleResist
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.TITANIUM_HULL:
          case ArmorClass.COMPOSITE_HULL:
          case ArmorClass.CARBIDE_HULL:
            return (float) (((double) this._baseEp + (double) this.Rarity) / 2.0);
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalResist
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.EXOPRENE_SHIELD:
          case ArmorClass.CYTOPLAST_SHIELD:
          case ArmorClass.HOLOCRINE_SHIELD:
            return (float) (((double) this._baseEp + (double) this.Rarity) / 2.0);
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashResist
    {
      get
      {
        switch (this._class)
        {
          case ArmorClass.DIAMOND_ARMOR:
          case ArmorClass.THORIUM_ARMOR:
          case ArmorClass.OSMIUM_ARMOR:
            return (float) (((double) this._baseEp + (double) this.Rarity) / 2.0);
          default:
            return 0.0f;
        }
      }
    }

    public ArmorClass Class
    {
      get
      {
        return this._class;
      }
    }

    public sbyte BaseEP
    {
      get
      {
        return this._baseEp;
      }
    }

    protected internal ArmorItem(ArmorClass armorClass, sbyte baseEp, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(rarity, durability, noDrop, bindOnEquip)
    {
      this._class = armorClass;
      this._baseEp = baseEp;
    }

    public override void Write(IByteBuffer output)
    {
      base.Write(output);
      output.Write((sbyte) this._class);
      output.Write(this._baseEp);
    }

    public override void AppendName(StringBuilder sb)
    {
      sb.Append(this._class.Name());
      sb.Append(" ");
      sb.Append(Util.NUMERALS[(int) this._baseEp]);
    }

    public override EquipmentItem GetUpgrade(EngineeringType type)
    {
      return (EquipmentItem) ArmorCache.GetUpgrade(type, this);
    }

    public override bool CanUpgrade(EngineeringType type)
    {
      if (this.Rarity <= ItemRarity.COMMON)
        return false;
      switch (type)
      {
        case EngineeringType.DECONSTRUCT:
          return true;
        case EngineeringType.REPAIR:
          if ((int) this.Durability > 1)
            return (int) this.Durability < 100;
          return false;
        case EngineeringType.RANK_UP:
          return (int) this._baseEp < 30;
        case EngineeringType.RANK_DOWN:
          return this.EPCost > 1;
        case EngineeringType.RARITY:
          return this.Rarity < ItemRarity.ULTIMATE;
        case EngineeringType.UNBIND:
          return this.NoDrop;
        default:
          return false;
      }
    }

    public override void AppendSubTitle(StringBuilder sb)
    {
      sb.Append(this.Rarity.Name());
      sb.Append(" Armor");
    }

    public override void AppendDescription(StringBuilder sb)
    {
    }
  }
}
