﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.StorageCache
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Library.Items
{
  public static class StorageCache
  {
    private static readonly Dictionary<StorageClass, Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, StorageItem[]>>>> _cache = new Dictionary<StorageClass, Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, StorageItem[]>>>>();

    public static StorageItem Get(IByteBuffer input)
    {
      ItemRarity rarity = (ItemRarity) input.Get();
      sbyte durability = input.Get();
      bool noDrop = input.GetBool();
      bool bindOnEquip = !noDrop && input.GetBool();
      return StorageCache.Get((StorageClass) input.Get(), input.Get(), rarity, durability, noDrop, bindOnEquip);
    }

    public static StorageItem Get(StorageClass type, sbyte ep, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      lock (StorageCache._cache)
      {
        Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, StorageItem[]>>> dictionary1;
        if (!StorageCache._cache.TryGetValue(type, out dictionary1))
        {
          dictionary1 = new Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, StorageItem[]>>>();
          StorageCache._cache[type] = dictionary1;
        }
        Dictionary<ItemRarity, Dictionary<sbyte, StorageItem[]>> dictionary2;
        if (!dictionary1.TryGetValue(ep, out dictionary2))
        {
          dictionary2 = new Dictionary<ItemRarity, Dictionary<sbyte, StorageItem[]>>();
          dictionary1[ep] = dictionary2;
        }
        Dictionary<sbyte, StorageItem[]> dictionary3;
        if (!dictionary2.TryGetValue(rarity, out dictionary3))
        {
          dictionary3 = new Dictionary<sbyte, StorageItem[]>();
          dictionary2[rarity] = dictionary3;
        }
        StorageItem[] storageItemArray;
        if (!dictionary3.TryGetValue(durability, out storageItemArray))
        {
          storageItemArray = new StorageItem[4];
          dictionary3[durability] = storageItemArray;
        }
        int index = noDrop ? (bindOnEquip ? 0 : 1) : (bindOnEquip ? 2 : 3);
        StorageItem storageItem = storageItemArray[index];
        if (storageItem == null)
        {
          storageItem = new StorageItem(type, ep, rarity, durability, noDrop, bindOnEquip);
          storageItemArray[index] = storageItem;
        }
        return storageItem;
      }
    }

    public static StorageItem GetUpgrade(EngineeringType type, StorageItem item)
    {
      if (!item.CanUpgrade(type))
        return (StorageItem) null;
      switch (type)
      {
        case EngineeringType.REPAIR:
          return StorageCache.Get(item.Class, item.BaseEP, item.Rarity, (int) item.Durability + 10 >= 100 ? (sbyte) 100 : (sbyte) ((int) item.Durability + 10), item.NoDrop, item.BindOnEquip);
        case EngineeringType.RANK_UP:
          return StorageCache.Get(item.Class, (sbyte) ((int) item.BaseEP + 1), item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RANK_DOWN:
          return StorageCache.Get(item.Class, (sbyte) ((int) item.BaseEP - 1), item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RARITY:
          return StorageCache.Get(item.Class, item.BaseEP, item.Rarity + 1, item.Durability, item.NoDrop, item.BindOnEquip);
        default:
          return (StorageItem) null;
      }
    }
  }
}
