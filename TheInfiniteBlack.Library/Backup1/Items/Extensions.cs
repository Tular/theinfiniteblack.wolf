﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.Extensions
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Items
{
  public static class Extensions
  {
    public static string Name(this StorageClass type)
    {
      switch (type)
      {
        case StorageClass.BasicStorage:
          return "Storage";
        case StorageClass.HoldH1A:
          return "Hold-H1A";
        case StorageClass.HoldH2A:
          return "Hold-H2A";
        case StorageClass.HoldH1B:
          return "Hold-H1B";
        case StorageClass.HoldH2B:
          return "Hold-H2B";
        case StorageClass.HoldH1G:
          return "Hold-H1G";
        case StorageClass.HoldH2G:
          return "Hold-H2G";
        case StorageClass.HoldP1A:
          return "Hold-P1A";
        case StorageClass.HoldP2A:
          return "Hold-P2A";
        case StorageClass.HoldP1B:
          return "Hold-P1B";
        case StorageClass.HoldP2B:
          return "Hold-P2B";
        case StorageClass.HoldP1G:
          return "Hold-P1G";
        case StorageClass.HoldP2G:
          return "Hold-P2G";
        case StorageClass.HoldW1A:
          return "Hold-W1A";
        case StorageClass.HoldW2A:
          return "Hold-W2A";
        case StorageClass.HoldW1B:
          return "Hold-W1B";
        case StorageClass.HoldW2B:
          return "Hold-W2B";
        case StorageClass.HoldW1G:
          return "Hold-W1G";
        case StorageClass.HoldW2G:
          return "Hold-W2G";
        case StorageClass.HoldR1A:
          return "Hold-R1A";
        case StorageClass.HoldR2A:
          return "Hold-R2A";
        case StorageClass.HoldR1B:
          return "Hold-R1B";
        case StorageClass.HoldR2B:
          return "Hold-R2B";
        case StorageClass.HoldR1G:
          return "Hold-R1G";
        case StorageClass.HoldR2G:
          return "Hold-R2G";
        case StorageClass.HoldH102:
          return "Hold-H102";
        case StorageClass.HoldH103:
          return "Hold-H103";
        case StorageClass.HoldH104:
          return "Hold-H104";
        case StorageClass.HoldH105:
          return "Hold-H105";
        case StorageClass.HoldH106:
          return "Hold-H106";
        case StorageClass.HoldH107:
          return "Hold-H107";
        case StorageClass.HoldH108:
          return "Hold-H108";
        case StorageClass.HoldH109:
          return "Hold-H109";
        case StorageClass.HoldH110:
          return "Hold-H110";
        case StorageClass.HoldH111:
          return "Hold-H111";
        case StorageClass.HoldH112:
          return "Hold-H112";
        case StorageClass.HoldH113:
          return "Hold-H113";
        case StorageClass.HoldH114:
          return "Hold-H114";
        case StorageClass.HoldH115:
          return "Hold-H115";
        case StorageClass.HoldH116:
          return "Hold-H116";
        case StorageClass.HoldH117:
          return "Hold-H117";
        case StorageClass.HoldH118:
          return "Hold-H118";
        case StorageClass.HoldH119:
          return "Hold-H119";
        case StorageClass.HoldH120:
          return "Hold-H120";
        case StorageClass.HoldH121:
          return "Hold-H121";
        case StorageClass.HoldH122:
          return "Hold-H122";
        case StorageClass.HoldH123:
          return "Hold-H123";
        case StorageClass.HoldH124:
          return "Hold-H124";
        case StorageClass.HoldH125:
          return "Hold-H125";
        case StorageClass.HoldH202:
          return "Hold-H202";
        case StorageClass.HoldH203:
          return "Hold-H203";
        case StorageClass.HoldH204:
          return "Hold-H204";
        case StorageClass.HoldH205:
          return "Hold-H205";
        case StorageClass.HoldH206:
          return "Hold-H206";
        case StorageClass.HoldH207:
          return "Hold-H207";
        case StorageClass.HoldH208:
          return "Hold-H208";
        case StorageClass.HoldH209:
          return "Hold-H209";
        case StorageClass.HoldH210:
          return "Hold-H210";
        case StorageClass.HoldH211:
          return "Hold-H211";
        case StorageClass.HoldH212:
          return "Hold-H212";
        case StorageClass.HoldH213:
          return "Hold-H213";
        case StorageClass.HoldH214:
          return "Hold-H214";
        case StorageClass.HoldH215:
          return "Hold-H215";
        case StorageClass.HoldH216:
          return "Hold-H216";
        case StorageClass.HoldH217:
          return "Hold-H217";
        case StorageClass.HoldH218:
          return "Hold-H218";
        case StorageClass.HoldH219:
          return "Hold-H219";
        case StorageClass.HoldH220:
          return "Hold-H220";
        case StorageClass.HoldH221:
          return "Hold-H221";
        case StorageClass.HoldH222:
          return "Hold-H222";
        case StorageClass.HoldH223:
          return "Hold-H223";
        case StorageClass.HoldH224:
          return "Hold-H224";
        case StorageClass.HoldH225:
          return "Hold-H225";
        case StorageClass.HoldP102:
          return "Hold-P102";
        case StorageClass.HoldP103:
          return "Hold-P103";
        case StorageClass.HoldP104:
          return "Hold-P104";
        case StorageClass.HoldP105:
          return "Hold-P105";
        case StorageClass.HoldP106:
          return "Hold-P106";
        case StorageClass.HoldP107:
          return "Hold-P107";
        case StorageClass.HoldP108:
          return "Hold-P108";
        case StorageClass.HoldP109:
          return "Hold-P109";
        case StorageClass.HoldP110:
          return "Hold-P110";
        case StorageClass.HoldP111:
          return "Hold-P111";
        case StorageClass.HoldP112:
          return "Hold-P112";
        case StorageClass.HoldP113:
          return "Hold-P113";
        case StorageClass.HoldP114:
          return "Hold-P114";
        case StorageClass.HoldP115:
          return "Hold-P115";
        case StorageClass.HoldP116:
          return "Hold-P116";
        case StorageClass.HoldP117:
          return "Hold-P117";
        case StorageClass.HoldP118:
          return "Hold-P118";
        case StorageClass.HoldP119:
          return "Hold-P119";
        case StorageClass.HoldP120:
          return "Hold-P120";
        case StorageClass.HoldP121:
          return "Hold-P121";
        case StorageClass.HoldP122:
          return "Hold-P122";
        case StorageClass.HoldP123:
          return "Hold-P123";
        case StorageClass.HoldP124:
          return "Hold-P124";
        case StorageClass.HoldP125:
          return "Hold-P125";
        case StorageClass.HoldP202:
          return "Hold-P202";
        case StorageClass.HoldP203:
          return "Hold-P203";
        case StorageClass.HoldP204:
          return "Hold-P204";
        case StorageClass.HoldP205:
          return "Hold-P205";
        case StorageClass.HoldP206:
          return "Hold-P206";
        case StorageClass.HoldP207:
          return "Hold-P207";
        case StorageClass.HoldP208:
          return "Hold-P208";
        case StorageClass.HoldP209:
          return "Hold-P209";
        case StorageClass.HoldP210:
          return "Hold-P210";
        case StorageClass.HoldP211:
          return "Hold-P211";
        case StorageClass.HoldP212:
          return "Hold-P212";
        case StorageClass.HoldP213:
          return "Hold-P213";
        case StorageClass.HoldP214:
          return "Hold-P214";
        case StorageClass.HoldP215:
          return "Hold-P215";
        case StorageClass.HoldP216:
          return "Hold-P216";
        case StorageClass.HoldP217:
          return "Hold-P217";
        case StorageClass.HoldP218:
          return "Hold-P218";
        case StorageClass.HoldP219:
          return "Hold-P219";
        case StorageClass.HoldP220:
          return "Hold-P220";
        case StorageClass.HoldP221:
          return "Hold-P221";
        case StorageClass.HoldP222:
          return "Hold-P222";
        case StorageClass.HoldP223:
          return "Hold-P223";
        case StorageClass.HoldP224:
          return "Hold-P224";
        case StorageClass.HoldP225:
          return "Hold-P225";
        case StorageClass.HoldW102:
          return "Hold-W102";
        case StorageClass.HoldW103:
          return "Hold-W103";
        case StorageClass.HoldW104:
          return "Hold-W104";
        case StorageClass.HoldW105:
          return "Hold-W105";
        case StorageClass.HoldW106:
          return "Hold-W106";
        case StorageClass.HoldW107:
          return "Hold-W107";
        case StorageClass.HoldW108:
          return "Hold-W108";
        case StorageClass.HoldW109:
          return "Hold-W109";
        case StorageClass.HoldW110:
          return "Hold-W110";
        case StorageClass.HoldW111:
          return "Hold-W111";
        case StorageClass.HoldW112:
          return "Hold-W112";
        case StorageClass.HoldW113:
          return "Hold-W113";
        case StorageClass.HoldW114:
          return "Hold-W114";
        case StorageClass.HoldW115:
          return "Hold-W115";
        case StorageClass.HoldW116:
          return "Hold-W116";
        case StorageClass.HoldW117:
          return "Hold-W117";
        case StorageClass.HoldW118:
          return "Hold-W118";
        case StorageClass.HoldW119:
          return "Hold-W119";
        case StorageClass.HoldW120:
          return "Hold-W120";
        case StorageClass.HoldW121:
          return "Hold-W121";
        case StorageClass.HoldW122:
          return "Hold-W122";
        case StorageClass.HoldW123:
          return "Hold-W123";
        case StorageClass.HoldW124:
          return "Hold-W124";
        case StorageClass.HoldW125:
          return "Hold-W125";
        case StorageClass.HoldW202:
          return "Hold-W202";
        case StorageClass.HoldW203:
          return "Hold-W203";
        case StorageClass.HoldW204:
          return "Hold-W204";
        case StorageClass.HoldW205:
          return "Hold-W205";
        case StorageClass.HoldW206:
          return "Hold-W206";
        case StorageClass.HoldW207:
          return "Hold-W207";
        case StorageClass.HoldW208:
          return "Hold-W208";
        case StorageClass.HoldW209:
          return "Hold-W209";
        case StorageClass.HoldW210:
          return "Hold-W210";
        case StorageClass.HoldW211:
          return "Hold-W211";
        case StorageClass.HoldW212:
          return "Hold-W212";
        case StorageClass.HoldW213:
          return "Hold-W213";
        case StorageClass.HoldW214:
          return "Hold-W214";
        case StorageClass.HoldW215:
          return "Hold-W215";
        case StorageClass.HoldW216:
          return "Hold-W216";
        case StorageClass.HoldW217:
          return "Hold-W217";
        case StorageClass.HoldW218:
          return "Hold-W218";
        case StorageClass.HoldW219:
          return "Hold-W219";
        case StorageClass.HoldW220:
          return "Hold-W220";
        case StorageClass.HoldW221:
          return "Hold-W221";
        case StorageClass.HoldW222:
          return "Hold-W222";
        case StorageClass.HoldW223:
          return "Hold-W223";
        case StorageClass.HoldW224:
          return "Hold-W224";
        case StorageClass.HoldW225:
          return "Hold-W225";
        case StorageClass.HoldR102:
          return "Hold-R102";
        case StorageClass.HoldR103:
          return "Hold-R103";
        case StorageClass.HoldR104:
          return "Hold-R104";
        case StorageClass.HoldR105:
          return "Hold-R105";
        case StorageClass.HoldR106:
          return "Hold-R106";
        case StorageClass.HoldR107:
          return "Hold-R107";
        case StorageClass.HoldR108:
          return "Hold-R108";
        case StorageClass.HoldR109:
          return "Hold-R109";
        case StorageClass.HoldR110:
          return "Hold-R110";
        case StorageClass.HoldR111:
          return "Hold-R111";
        case StorageClass.HoldR112:
          return "Hold-R112";
        case StorageClass.HoldR113:
          return "Hold-R113";
        case StorageClass.HoldR114:
          return "Hold-R114";
        case StorageClass.HoldR115:
          return "Hold-R115";
        case StorageClass.HoldR116:
          return "Hold-R116";
        case StorageClass.HoldR117:
          return "Hold-R117";
        case StorageClass.HoldR118:
          return "Hold-R118";
        case StorageClass.HoldR119:
          return "Hold-R119";
        case StorageClass.HoldR120:
          return "Hold-R120";
        case StorageClass.HoldR121:
          return "Hold-R121";
        case StorageClass.HoldR122:
          return "Hold-R122";
        case StorageClass.HoldR123:
          return "Hold-R123";
        case StorageClass.HoldR124:
          return "Hold-R124";
        case StorageClass.HoldR125:
          return "Hold-R125";
        case StorageClass.HoldR202:
          return "Hold-R202";
        case StorageClass.HoldR203:
          return "Hold-R203";
        case StorageClass.HoldR204:
          return "Hold-R204";
        case StorageClass.HoldR205:
          return "Hold-R205";
        case StorageClass.HoldR206:
          return "Hold-R206";
        case StorageClass.HoldR207:
          return "Hold-R207";
        case StorageClass.HoldR208:
          return "Hold-R208";
        case StorageClass.HoldR209:
          return "Hold-R209";
        case StorageClass.HoldR210:
          return "Hold-R210";
        case StorageClass.HoldR211:
          return "Hold-R211";
        case StorageClass.HoldR212:
          return "Hold-R212";
        case StorageClass.HoldR213:
          return "Hold-R213";
        case StorageClass.HoldR214:
          return "Hold-R214";
        case StorageClass.HoldR215:
          return "Hold-R215";
        case StorageClass.HoldR216:
          return "Hold-R216";
        case StorageClass.HoldR217:
          return "Hold-R217";
        case StorageClass.HoldR218:
          return "Hold-R218";
        case StorageClass.HoldR219:
          return "Hold-R219";
        case StorageClass.HoldR220:
          return "Hold-R220";
        case StorageClass.HoldR221:
          return "Hold-R221";
        case StorageClass.HoldR222:
          return "Hold-R222";
        case StorageClass.HoldR223:
          return "Hold-R223";
        case StorageClass.HoldR224:
          return "Hold-R224";
        case StorageClass.HoldR225:
          return "Hold-R225";
        case StorageClass.BigCrate:
          return "Big Crate";
        case StorageClass.ReallyBigCrate:
          return "Really Big Crate";
        default:
          return "Unknown?";
      }
    }

    public static string IconTag(this StorageClass type, int baseEp)
    {
      switch (type)
      {
        case StorageClass.BasicStorage:
          return Util.NUMERALS[baseEp] + "\nSTOR";
        case StorageClass.HoldH1A:
          return Util.NUMERALS[baseEp] + "\nH1A";
        case StorageClass.HoldH2A:
          return Util.NUMERALS[baseEp] + "\nH2A";
        case StorageClass.HoldH1B:
          return Util.NUMERALS[baseEp] + "\nH1B";
        case StorageClass.HoldH2B:
          return Util.NUMERALS[baseEp] + "\nH2B";
        case StorageClass.HoldH1G:
          return Util.NUMERALS[baseEp] + "\nH1G";
        case StorageClass.HoldH2G:
          return Util.NUMERALS[baseEp] + "\nH2G";
        case StorageClass.HoldP1A:
          return Util.NUMERALS[baseEp] + "\nP1A";
        case StorageClass.HoldP2A:
          return Util.NUMERALS[baseEp] + "\nP2A";
        case StorageClass.HoldP1B:
          return Util.NUMERALS[baseEp] + "\nP1B";
        case StorageClass.HoldP2B:
          return Util.NUMERALS[baseEp] + "\nP2B";
        case StorageClass.HoldP1G:
          return Util.NUMERALS[baseEp] + "\nP1G";
        case StorageClass.HoldP2G:
          return Util.NUMERALS[baseEp] + "\nP2G";
        case StorageClass.HoldW1A:
          return Util.NUMERALS[baseEp] + "\nW1A";
        case StorageClass.HoldW2A:
          return Util.NUMERALS[baseEp] + "\nW2A";
        case StorageClass.HoldW1B:
          return Util.NUMERALS[baseEp] + "\nW1B";
        case StorageClass.HoldW2B:
          return Util.NUMERALS[baseEp] + "\nW2B";
        case StorageClass.HoldW1G:
          return Util.NUMERALS[baseEp] + "\nW1G";
        case StorageClass.HoldW2G:
          return Util.NUMERALS[baseEp] + "\nW2G";
        case StorageClass.HoldR1A:
          return Util.NUMERALS[baseEp] + "\nR1A";
        case StorageClass.HoldR2A:
          return Util.NUMERALS[baseEp] + "\nR2A";
        case StorageClass.HoldR1B:
          return Util.NUMERALS[baseEp] + "\nR1B";
        case StorageClass.HoldR2B:
          return Util.NUMERALS[baseEp] + "\nR2B";
        case StorageClass.HoldR1G:
          return Util.NUMERALS[baseEp] + "\nR1G";
        case StorageClass.HoldR2G:
          return Util.NUMERALS[baseEp] + "\nR2G";
        case StorageClass.HoldH102:
          return Util.NUMERALS[baseEp] + "\nH102";
        case StorageClass.HoldH103:
          return Util.NUMERALS[baseEp] + "\nH103";
        case StorageClass.HoldH104:
          return Util.NUMERALS[baseEp] + "\nH104";
        case StorageClass.HoldH105:
          return Util.NUMERALS[baseEp] + "\nH105";
        case StorageClass.HoldH106:
          return Util.NUMERALS[baseEp] + "\nH106";
        case StorageClass.HoldH107:
          return Util.NUMERALS[baseEp] + "\nH107";
        case StorageClass.HoldH108:
          return Util.NUMERALS[baseEp] + "\nH108";
        case StorageClass.HoldH109:
          return Util.NUMERALS[baseEp] + "\nH109";
        case StorageClass.HoldH110:
          return Util.NUMERALS[baseEp] + "\nH110";
        case StorageClass.HoldH111:
          return Util.NUMERALS[baseEp] + "\nH111";
        case StorageClass.HoldH112:
          return Util.NUMERALS[baseEp] + "\nH112";
        case StorageClass.HoldH113:
          return Util.NUMERALS[baseEp] + "\nH113";
        case StorageClass.HoldH114:
          return Util.NUMERALS[baseEp] + "\nH114";
        case StorageClass.HoldH115:
          return Util.NUMERALS[baseEp] + "\nH115";
        case StorageClass.HoldH116:
          return Util.NUMERALS[baseEp] + "\nH116";
        case StorageClass.HoldH117:
          return Util.NUMERALS[baseEp] + "\nH117";
        case StorageClass.HoldH118:
          return Util.NUMERALS[baseEp] + "\nH118";
        case StorageClass.HoldH119:
          return Util.NUMERALS[baseEp] + "\nH119";
        case StorageClass.HoldH120:
          return Util.NUMERALS[baseEp] + "\nH120";
        case StorageClass.HoldH121:
          return Util.NUMERALS[baseEp] + "\nH121";
        case StorageClass.HoldH122:
          return Util.NUMERALS[baseEp] + "\nH122";
        case StorageClass.HoldH123:
          return Util.NUMERALS[baseEp] + "\nH123";
        case StorageClass.HoldH124:
          return Util.NUMERALS[baseEp] + "\nH124";
        case StorageClass.HoldH125:
          return Util.NUMERALS[baseEp] + "\nH125";
        case StorageClass.HoldH202:
          return Util.NUMERALS[baseEp] + "\nH202";
        case StorageClass.HoldH203:
          return Util.NUMERALS[baseEp] + "\nH203";
        case StorageClass.HoldH204:
          return Util.NUMERALS[baseEp] + "\nH204";
        case StorageClass.HoldH205:
          return Util.NUMERALS[baseEp] + "\nH205";
        case StorageClass.HoldH206:
          return Util.NUMERALS[baseEp] + "\nH206";
        case StorageClass.HoldH207:
          return Util.NUMERALS[baseEp] + "\nH207";
        case StorageClass.HoldH208:
          return Util.NUMERALS[baseEp] + "\nH208";
        case StorageClass.HoldH209:
          return Util.NUMERALS[baseEp] + "\nH209";
        case StorageClass.HoldH210:
          return Util.NUMERALS[baseEp] + "\nH210";
        case StorageClass.HoldH211:
          return Util.NUMERALS[baseEp] + "\nH211";
        case StorageClass.HoldH212:
          return Util.NUMERALS[baseEp] + "\nH212";
        case StorageClass.HoldH213:
          return Util.NUMERALS[baseEp] + "\nH213";
        case StorageClass.HoldH214:
          return Util.NUMERALS[baseEp] + "\nH214";
        case StorageClass.HoldH215:
          return Util.NUMERALS[baseEp] + "\nH215";
        case StorageClass.HoldH216:
          return Util.NUMERALS[baseEp] + "\nH216";
        case StorageClass.HoldH217:
          return Util.NUMERALS[baseEp] + "\nH217";
        case StorageClass.HoldH218:
          return Util.NUMERALS[baseEp] + "\nH218";
        case StorageClass.HoldH219:
          return Util.NUMERALS[baseEp] + "\nH219";
        case StorageClass.HoldH220:
          return Util.NUMERALS[baseEp] + "\nH220";
        case StorageClass.HoldH221:
          return Util.NUMERALS[baseEp] + "\nH221";
        case StorageClass.HoldH222:
          return Util.NUMERALS[baseEp] + "\nH222";
        case StorageClass.HoldH223:
          return Util.NUMERALS[baseEp] + "\nH223";
        case StorageClass.HoldH224:
          return Util.NUMERALS[baseEp] + "\nH224";
        case StorageClass.HoldH225:
          return Util.NUMERALS[baseEp] + "\nH225";
        case StorageClass.HoldP102:
          return Util.NUMERALS[baseEp] + "\nP102";
        case StorageClass.HoldP103:
          return Util.NUMERALS[baseEp] + "\nP103";
        case StorageClass.HoldP104:
          return Util.NUMERALS[baseEp] + "\nP104";
        case StorageClass.HoldP105:
          return Util.NUMERALS[baseEp] + "\nP105";
        case StorageClass.HoldP106:
          return Util.NUMERALS[baseEp] + "\nP106";
        case StorageClass.HoldP107:
          return Util.NUMERALS[baseEp] + "\nP107";
        case StorageClass.HoldP108:
          return Util.NUMERALS[baseEp] + "\nP108";
        case StorageClass.HoldP109:
          return Util.NUMERALS[baseEp] + "\nP109";
        case StorageClass.HoldP110:
          return Util.NUMERALS[baseEp] + "\nP110";
        case StorageClass.HoldP111:
          return Util.NUMERALS[baseEp] + "\nP111";
        case StorageClass.HoldP112:
          return Util.NUMERALS[baseEp] + "\nP112";
        case StorageClass.HoldP113:
          return Util.NUMERALS[baseEp] + "\nP113";
        case StorageClass.HoldP114:
          return Util.NUMERALS[baseEp] + "\nP114";
        case StorageClass.HoldP115:
          return Util.NUMERALS[baseEp] + "\nP115";
        case StorageClass.HoldP116:
          return Util.NUMERALS[baseEp] + "\nP116";
        case StorageClass.HoldP117:
          return Util.NUMERALS[baseEp] + "\nP117";
        case StorageClass.HoldP118:
          return Util.NUMERALS[baseEp] + "\nP118";
        case StorageClass.HoldP119:
          return Util.NUMERALS[baseEp] + "\nP119";
        case StorageClass.HoldP120:
          return Util.NUMERALS[baseEp] + "\nP120";
        case StorageClass.HoldP121:
          return Util.NUMERALS[baseEp] + "\nP121";
        case StorageClass.HoldP122:
          return Util.NUMERALS[baseEp] + "\nP122";
        case StorageClass.HoldP123:
          return Util.NUMERALS[baseEp] + "\nP123";
        case StorageClass.HoldP124:
          return Util.NUMERALS[baseEp] + "\nP124";
        case StorageClass.HoldP125:
          return Util.NUMERALS[baseEp] + "\nP125";
        case StorageClass.HoldP202:
          return Util.NUMERALS[baseEp] + "\nP202";
        case StorageClass.HoldP203:
          return Util.NUMERALS[baseEp] + "\nP203";
        case StorageClass.HoldP204:
          return Util.NUMERALS[baseEp] + "\nP204";
        case StorageClass.HoldP205:
          return Util.NUMERALS[baseEp] + "\nP205";
        case StorageClass.HoldP206:
          return Util.NUMERALS[baseEp] + "\nP206";
        case StorageClass.HoldP207:
          return Util.NUMERALS[baseEp] + "\nP207";
        case StorageClass.HoldP208:
          return Util.NUMERALS[baseEp] + "\nP208";
        case StorageClass.HoldP209:
          return Util.NUMERALS[baseEp] + "\nP209";
        case StorageClass.HoldP210:
          return Util.NUMERALS[baseEp] + "\nP210";
        case StorageClass.HoldP211:
          return Util.NUMERALS[baseEp] + "\nP211";
        case StorageClass.HoldP212:
          return Util.NUMERALS[baseEp] + "\nP212";
        case StorageClass.HoldP213:
          return Util.NUMERALS[baseEp] + "\nP213";
        case StorageClass.HoldP214:
          return Util.NUMERALS[baseEp] + "\nP214";
        case StorageClass.HoldP215:
          return Util.NUMERALS[baseEp] + "\nP215";
        case StorageClass.HoldP216:
          return Util.NUMERALS[baseEp] + "\nP216";
        case StorageClass.HoldP217:
          return Util.NUMERALS[baseEp] + "\nP217";
        case StorageClass.HoldP218:
          return Util.NUMERALS[baseEp] + "\nP218";
        case StorageClass.HoldP219:
          return Util.NUMERALS[baseEp] + "\nP219";
        case StorageClass.HoldP220:
          return Util.NUMERALS[baseEp] + "\nP220";
        case StorageClass.HoldP221:
          return Util.NUMERALS[baseEp] + "\nP221";
        case StorageClass.HoldP222:
          return Util.NUMERALS[baseEp] + "\nP222";
        case StorageClass.HoldP223:
          return Util.NUMERALS[baseEp] + "\nP223";
        case StorageClass.HoldP224:
          return Util.NUMERALS[baseEp] + "\nP224";
        case StorageClass.HoldP225:
          return Util.NUMERALS[baseEp] + "\nP225";
        case StorageClass.HoldW102:
          return Util.NUMERALS[baseEp] + "\nW102";
        case StorageClass.HoldW103:
          return Util.NUMERALS[baseEp] + "\nW103";
        case StorageClass.HoldW104:
          return Util.NUMERALS[baseEp] + "\nW104";
        case StorageClass.HoldW105:
          return Util.NUMERALS[baseEp] + "\nW105";
        case StorageClass.HoldW106:
          return Util.NUMERALS[baseEp] + "\nW106";
        case StorageClass.HoldW107:
          return Util.NUMERALS[baseEp] + "\nW107";
        case StorageClass.HoldW108:
          return Util.NUMERALS[baseEp] + "\nW108";
        case StorageClass.HoldW109:
          return Util.NUMERALS[baseEp] + "\nW109";
        case StorageClass.HoldW110:
          return Util.NUMERALS[baseEp] + "\nW110";
        case StorageClass.HoldW111:
          return Util.NUMERALS[baseEp] + "\nW111";
        case StorageClass.HoldW112:
          return Util.NUMERALS[baseEp] + "\nW112";
        case StorageClass.HoldW113:
          return Util.NUMERALS[baseEp] + "\nW113";
        case StorageClass.HoldW114:
          return Util.NUMERALS[baseEp] + "\nW114";
        case StorageClass.HoldW115:
          return Util.NUMERALS[baseEp] + "\nW115";
        case StorageClass.HoldW116:
          return Util.NUMERALS[baseEp] + "\nW116";
        case StorageClass.HoldW117:
          return Util.NUMERALS[baseEp] + "\nW117";
        case StorageClass.HoldW118:
          return Util.NUMERALS[baseEp] + "\nW118";
        case StorageClass.HoldW119:
          return Util.NUMERALS[baseEp] + "\nW119";
        case StorageClass.HoldW120:
          return Util.NUMERALS[baseEp] + "\nW120";
        case StorageClass.HoldW121:
          return Util.NUMERALS[baseEp] + "\nW121";
        case StorageClass.HoldW122:
          return Util.NUMERALS[baseEp] + "\nW122";
        case StorageClass.HoldW123:
          return Util.NUMERALS[baseEp] + "\nW123";
        case StorageClass.HoldW124:
          return Util.NUMERALS[baseEp] + "\nW124";
        case StorageClass.HoldW125:
          return Util.NUMERALS[baseEp] + "\nW125";
        case StorageClass.HoldW202:
          return Util.NUMERALS[baseEp] + "\nW202";
        case StorageClass.HoldW203:
          return Util.NUMERALS[baseEp] + "\nW203";
        case StorageClass.HoldW204:
          return Util.NUMERALS[baseEp] + "\nW204";
        case StorageClass.HoldW205:
          return Util.NUMERALS[baseEp] + "\nW205";
        case StorageClass.HoldW206:
          return Util.NUMERALS[baseEp] + "\nW206";
        case StorageClass.HoldW207:
          return Util.NUMERALS[baseEp] + "\nW207";
        case StorageClass.HoldW208:
          return Util.NUMERALS[baseEp] + "\nW208";
        case StorageClass.HoldW209:
          return Util.NUMERALS[baseEp] + "\nW209";
        case StorageClass.HoldW210:
          return Util.NUMERALS[baseEp] + "\nW210";
        case StorageClass.HoldW211:
          return Util.NUMERALS[baseEp] + "\nW211";
        case StorageClass.HoldW212:
          return Util.NUMERALS[baseEp] + "\nW212";
        case StorageClass.HoldW213:
          return Util.NUMERALS[baseEp] + "\nW213";
        case StorageClass.HoldW214:
          return Util.NUMERALS[baseEp] + "\nW214";
        case StorageClass.HoldW215:
          return Util.NUMERALS[baseEp] + "\nW215";
        case StorageClass.HoldW216:
          return Util.NUMERALS[baseEp] + "\nW216";
        case StorageClass.HoldW217:
          return Util.NUMERALS[baseEp] + "\nW217";
        case StorageClass.HoldW218:
          return Util.NUMERALS[baseEp] + "\nW218";
        case StorageClass.HoldW219:
          return Util.NUMERALS[baseEp] + "\nW219";
        case StorageClass.HoldW220:
          return Util.NUMERALS[baseEp] + "\nW220";
        case StorageClass.HoldW221:
          return Util.NUMERALS[baseEp] + "\nW221";
        case StorageClass.HoldW222:
          return Util.NUMERALS[baseEp] + "\nW222";
        case StorageClass.HoldW223:
          return Util.NUMERALS[baseEp] + "\nW223";
        case StorageClass.HoldW224:
          return Util.NUMERALS[baseEp] + "\nW224";
        case StorageClass.HoldW225:
          return Util.NUMERALS[baseEp] + "\nW225";
        case StorageClass.HoldR102:
          return Util.NUMERALS[baseEp] + "\nR102";
        case StorageClass.HoldR103:
          return Util.NUMERALS[baseEp] + "\nR103";
        case StorageClass.HoldR104:
          return Util.NUMERALS[baseEp] + "\nR104";
        case StorageClass.HoldR105:
          return Util.NUMERALS[baseEp] + "\nR105";
        case StorageClass.HoldR106:
          return Util.NUMERALS[baseEp] + "\nR106";
        case StorageClass.HoldR107:
          return Util.NUMERALS[baseEp] + "\nR107";
        case StorageClass.HoldR108:
          return Util.NUMERALS[baseEp] + "\nR108";
        case StorageClass.HoldR109:
          return Util.NUMERALS[baseEp] + "\nR109";
        case StorageClass.HoldR110:
          return Util.NUMERALS[baseEp] + "\nR110";
        case StorageClass.HoldR111:
          return Util.NUMERALS[baseEp] + "\nR111";
        case StorageClass.HoldR112:
          return Util.NUMERALS[baseEp] + "\nR112";
        case StorageClass.HoldR113:
          return Util.NUMERALS[baseEp] + "\nR113";
        case StorageClass.HoldR114:
          return Util.NUMERALS[baseEp] + "\nR114";
        case StorageClass.HoldR115:
          return Util.NUMERALS[baseEp] + "\nR115";
        case StorageClass.HoldR116:
          return Util.NUMERALS[baseEp] + "\nR116";
        case StorageClass.HoldR117:
          return Util.NUMERALS[baseEp] + "\nR117";
        case StorageClass.HoldR118:
          return Util.NUMERALS[baseEp] + "\nR118";
        case StorageClass.HoldR119:
          return Util.NUMERALS[baseEp] + "\nR119";
        case StorageClass.HoldR120:
          return Util.NUMERALS[baseEp] + "\nR120";
        case StorageClass.HoldR121:
          return Util.NUMERALS[baseEp] + "\nR121";
        case StorageClass.HoldR122:
          return Util.NUMERALS[baseEp] + "\nR122";
        case StorageClass.HoldR123:
          return Util.NUMERALS[baseEp] + "\nR123";
        case StorageClass.HoldR124:
          return Util.NUMERALS[baseEp] + "\nR124";
        case StorageClass.HoldR125:
          return Util.NUMERALS[baseEp] + "\nR125";
        case StorageClass.HoldR202:
          return Util.NUMERALS[baseEp] + "\nR202";
        case StorageClass.HoldR203:
          return Util.NUMERALS[baseEp] + "\nR203";
        case StorageClass.HoldR204:
          return Util.NUMERALS[baseEp] + "\nR204";
        case StorageClass.HoldR205:
          return Util.NUMERALS[baseEp] + "\nR205";
        case StorageClass.HoldR206:
          return Util.NUMERALS[baseEp] + "\nR206";
        case StorageClass.HoldR207:
          return Util.NUMERALS[baseEp] + "\nR207";
        case StorageClass.HoldR208:
          return Util.NUMERALS[baseEp] + "\nR208";
        case StorageClass.HoldR209:
          return Util.NUMERALS[baseEp] + "\nR209";
        case StorageClass.HoldR210:
          return Util.NUMERALS[baseEp] + "\nR210";
        case StorageClass.HoldR211:
          return Util.NUMERALS[baseEp] + "\nR211";
        case StorageClass.HoldR212:
          return Util.NUMERALS[baseEp] + "\nR212";
        case StorageClass.HoldR213:
          return Util.NUMERALS[baseEp] + "\nR213";
        case StorageClass.HoldR214:
          return Util.NUMERALS[baseEp] + "\nR214";
        case StorageClass.HoldR215:
          return Util.NUMERALS[baseEp] + "\nR215";
        case StorageClass.HoldR216:
          return Util.NUMERALS[baseEp] + "\nR216";
        case StorageClass.HoldR217:
          return Util.NUMERALS[baseEp] + "\nR217";
        case StorageClass.HoldR218:
          return Util.NUMERALS[baseEp] + "\nR218";
        case StorageClass.HoldR219:
          return Util.NUMERALS[baseEp] + "\nR219";
        case StorageClass.HoldR220:
          return Util.NUMERALS[baseEp] + "\nR220";
        case StorageClass.HoldR221:
          return Util.NUMERALS[baseEp] + "\nR221";
        case StorageClass.HoldR222:
          return Util.NUMERALS[baseEp] + "\nR222";
        case StorageClass.HoldR223:
          return Util.NUMERALS[baseEp] + "\nR223";
        case StorageClass.HoldR224:
          return Util.NUMERALS[baseEp] + "\nR224";
        case StorageClass.HoldR225:
          return Util.NUMERALS[baseEp] + "\nR225";
        case StorageClass.BigCrate:
          return Util.NUMERALS[baseEp] + "\nBC";
        case StorageClass.ReallyBigCrate:
          return Util.NUMERALS[baseEp] + "\nRBC";
        default:
          return Util.NUMERALS[baseEp] + "\nERR";
      }
    }

    public static string Name(this ArmorClass type)
    {
      switch (type)
      {
        case ArmorClass.DIAMOND_ARMOR:
          return "Diamond Armor";
        case ArmorClass.THORIUM_ARMOR:
          return "Thorium Armor";
        case ArmorClass.OSMIUM_ARMOR:
          return "Osmium Armor";
        case ArmorClass.CITADEL_GAMBIT:
          return "Citadel Gambit";
        case ArmorClass.AJAX_GAMBIT:
          return "Ajax Gambit";
        case ArmorClass.AEGIS_GAMBIT:
          return "Aegis Gambit";
        case ArmorClass.KISMET_GAMBIT:
          return "Kismet Gambit";
        case ArmorClass.TITANIUM_HULL:
          return "Titanium Hull";
        case ArmorClass.COMPOSITE_HULL:
          return "Composite Hull";
        case ArmorClass.CARBIDE_HULL:
          return "Carbide Hull";
        case ArmorClass.EXOPRENE_SHIELD:
          return "Exoprene Shield";
        case ArmorClass.CYTOPLAST_SHIELD:
          return "Cytoplast Shield";
        case ArmorClass.HOLOCRINE_SHIELD:
          return "Holocrine Shield";
        default:
          return "Unknown?";
      }
    }

    public static string IconTag(this ArmorClass type, int baseEp)
    {
      switch (type)
      {
        case ArmorClass.DIAMOND_ARMOR:
          return Util.NUMERALS[baseEp] + "\nDIAM";
        case ArmorClass.THORIUM_ARMOR:
          return Util.NUMERALS[baseEp] + "\nTHOR";
        case ArmorClass.OSMIUM_ARMOR:
          return Util.NUMERALS[baseEp] + "\nOSMI";
        case ArmorClass.CITADEL_GAMBIT:
          return Util.NUMERALS[baseEp] + "\nCITA";
        case ArmorClass.AJAX_GAMBIT:
          return Util.NUMERALS[baseEp] + "\nAJAX";
        case ArmorClass.AEGIS_GAMBIT:
          return Util.NUMERALS[baseEp] + "\nAEGI";
        case ArmorClass.KISMET_GAMBIT:
          return Util.NUMERALS[baseEp] + "\nKISM";
        case ArmorClass.TITANIUM_HULL:
          return Util.NUMERALS[baseEp] + "\nTITA";
        case ArmorClass.COMPOSITE_HULL:
          return Util.NUMERALS[baseEp] + "\nCOMP";
        case ArmorClass.CARBIDE_HULL:
          return Util.NUMERALS[baseEp] + "\nCARB";
        case ArmorClass.EXOPRENE_SHIELD:
          return Util.NUMERALS[baseEp] + "\nEXO";
        case ArmorClass.CYTOPLAST_SHIELD:
          return Util.NUMERALS[baseEp] + "\nCYTO";
        case ArmorClass.HOLOCRINE_SHIELD:
          return Util.NUMERALS[baseEp] + "\nHOLO";
        default:
          return Util.NUMERALS[baseEp] + "\n?";
      }
    }

    public static NpcFaction Faction(this ArmorClass type)
    {
      switch (type)
      {
        case ArmorClass.DIAMOND_ARMOR:
        case ArmorClass.THORIUM_ARMOR:
        case ArmorClass.OSMIUM_ARMOR:
          return NpcFaction.WYRD;
        case ArmorClass.CITADEL_GAMBIT:
        case ArmorClass.AJAX_GAMBIT:
        case ArmorClass.AEGIS_GAMBIT:
        case ArmorClass.KISMET_GAMBIT:
          return NpcFaction.RIFT;
        case ArmorClass.TITANIUM_HULL:
        case ArmorClass.COMPOSITE_HULL:
        case ArmorClass.CARBIDE_HULL:
          return NpcFaction.PIRATE;
        case ArmorClass.EXOPRENE_SHIELD:
        case ArmorClass.CYTOPLAST_SHIELD:
        case ArmorClass.HOLOCRINE_SHIELD:
          return NpcFaction.HETEROCLITE;
        default:
          return NpcFaction.None;
      }
    }

    public static string Name(this ComputerClass type)
    {
      switch (type)
      {
        case ComputerClass.SAGE_MK_1:
          return "Sage Mk. I";
        case ComputerClass.SAGE_MK_2:
          return "Sage Mk. II";
        case ComputerClass.SAGE_MK_3:
          return "Sage Mk. III";
        case ComputerClass.SAGE_MK_4:
          return "Sage Mk. IV";
        case ComputerClass.TOIL_MK_1:
          return "Toil Mk. I";
        case ComputerClass.TOIL_MK_2:
          return "Toil Mk. II";
        case ComputerClass.TOIL_MK_3:
          return "Toil Mk. III";
        case ComputerClass.TOIL_MK_4:
          return "Toil Mk. IV";
        case ComputerClass.HAVOK_MK_1:
          return "Havok Mk. I";
        case ComputerClass.HAVOK_MK_2:
          return "Havok Mk. II";
        case ComputerClass.HAVOK_MK_3:
          return "Havok Mk. III";
        case ComputerClass.HAVOK_MK_4:
          return "Havok Mk. IV";
        case ComputerClass.CABAL_MK_1:
          return "Cabal Mk. I";
        case ComputerClass.CABAL_MK_2:
          return "Cabal Mk. II";
        case ComputerClass.CABAL_MK_3:
          return "Cabal Mk. III";
        case ComputerClass.CABAL_MK_4:
          return "Cabal Mk. IV";
        case ComputerClass.AGENT_MK_1:
          return "Agent Mk. I";
        case ComputerClass.AGENT_MK_2:
          return "Agent Mk. II";
        case ComputerClass.AGENT_MK_3:
          return "Agent Mk. III";
        case ComputerClass.AGENT_MK_4:
          return "Agent Mk. IV";
        case ComputerClass.DRONE_MK_1:
          return "Drone Mk. I";
        case ComputerClass.DRONE_MK_2:
          return "Drone Mk. II";
        case ComputerClass.DRONE_MK_3:
          return "Drone Mk. III";
        case ComputerClass.DRONE_MK_4:
          return "Drone Mk. IV";
        case ComputerClass.WARRIOR_MK_1:
          return "Warrior Mk. I";
        case ComputerClass.WARRIOR_MK_2:
          return "Warrior Mk. II";
        case ComputerClass.WARRIOR_MK_3:
          return "Warrior Mk. III";
        case ComputerClass.WARRIOR_MK_4:
          return "Warrior Mk. IV";
        case ComputerClass.A1_SAGE:
          return "Sage Tech I";
        case ComputerClass.A1_TOIL:
          return "Toil Tech I";
        case ComputerClass.A1_HAVOK:
          return "Havok Tech I";
        case ComputerClass.A1_CABAL:
          return "Cabal Tech I";
        case ComputerClass.A1_AGENT:
          return "Agent Tech I";
        case ComputerClass.A1_DRONE:
          return "Drone Tech I";
        case ComputerClass.A1_WARRIOR:
          return "Warrior Tech I";
        case ComputerClass.A2_SAGE:
          return "Sage Tech II";
        case ComputerClass.A2_TOIL:
          return "Toil Tech II";
        case ComputerClass.A2_HAVOK:
          return "Havok Tech II";
        case ComputerClass.A2_CABAL:
          return "Cabal Tech II";
        case ComputerClass.A2_AGENT:
          return "Agent Tech II";
        case ComputerClass.A2_DRONE:
          return "Drone Tech II";
        case ComputerClass.A2_WARRIOR:
          return "Warrior Tech II";
        case ComputerClass.A3_SAGE:
          return "Sage Tech III";
        case ComputerClass.A3_TOIL:
          return "Toil Tech III";
        case ComputerClass.A3_HAVOK:
          return "Havok Tech III";
        case ComputerClass.A3_CABAL:
          return "Cabal Tech III";
        case ComputerClass.A3_AGENT:
          return "Agent Tech III";
        case ComputerClass.A3_DRONE:
          return "Drone Tech III";
        case ComputerClass.A3_WARRIOR:
          return "Warrior Tech III";
        default:
          return "Unknown?";
      }
    }

    public static string IconTag(this ComputerClass type)
    {
      switch (type)
      {
        case ComputerClass.SAGE_MK_1:
          return "S-I";
        case ComputerClass.SAGE_MK_2:
          return "S-II";
        case ComputerClass.SAGE_MK_3:
          return "S-III";
        case ComputerClass.SAGE_MK_4:
          return "S-IV";
        case ComputerClass.TOIL_MK_1:
          return "T-I";
        case ComputerClass.TOIL_MK_2:
          return "T-II";
        case ComputerClass.TOIL_MK_3:
          return "T-III";
        case ComputerClass.TOIL_MK_4:
          return "T-IV";
        case ComputerClass.HAVOK_MK_1:
          return "H-I";
        case ComputerClass.HAVOK_MK_2:
          return "H-II";
        case ComputerClass.HAVOK_MK_3:
          return "H-III";
        case ComputerClass.HAVOK_MK_4:
          return "H-IV";
        case ComputerClass.CABAL_MK_1:
          return "C-I";
        case ComputerClass.CABAL_MK_2:
          return "C-II";
        case ComputerClass.CABAL_MK_3:
          return "C-III";
        case ComputerClass.CABAL_MK_4:
          return "C-IV";
        case ComputerClass.AGENT_MK_1:
          return "A-I";
        case ComputerClass.AGENT_MK_2:
          return "A-II";
        case ComputerClass.AGENT_MK_3:
          return "A-III";
        case ComputerClass.AGENT_MK_4:
          return "A-IV";
        case ComputerClass.DRONE_MK_1:
          return "D-I";
        case ComputerClass.DRONE_MK_2:
          return "D-II";
        case ComputerClass.DRONE_MK_3:
          return "D-III";
        case ComputerClass.DRONE_MK_4:
          return "D-IV";
        case ComputerClass.WARRIOR_MK_1:
          return "W-I";
        case ComputerClass.WARRIOR_MK_2:
          return "W-II";
        case ComputerClass.WARRIOR_MK_3:
          return "W-III";
        case ComputerClass.WARRIOR_MK_4:
          return "W-IV";
        case ComputerClass.A1_SAGE:
          return "S!T1";
        case ComputerClass.A1_TOIL:
          return "T!T1";
        case ComputerClass.A1_HAVOK:
          return "H!T1";
        case ComputerClass.A1_CABAL:
          return "C!T1";
        case ComputerClass.A1_AGENT:
          return "A!T1";
        case ComputerClass.A1_DRONE:
          return "D!T1";
        case ComputerClass.A1_WARRIOR:
          return "W!T1";
        case ComputerClass.A2_SAGE:
          return "S!T2";
        case ComputerClass.A2_TOIL:
          return "T!T2";
        case ComputerClass.A2_HAVOK:
          return "H!T2";
        case ComputerClass.A2_CABAL:
          return "C!T2";
        case ComputerClass.A2_AGENT:
          return "A!T2";
        case ComputerClass.A2_DRONE:
          return "D!T2";
        case ComputerClass.A2_WARRIOR:
          return "W!T2";
        case ComputerClass.A3_SAGE:
          return "S!T3";
        case ComputerClass.A3_TOIL:
          return "T!T3";
        case ComputerClass.A3_HAVOK:
          return "H!T3";
        case ComputerClass.A3_CABAL:
          return "C!T3";
        case ComputerClass.A3_AGENT:
          return "A!T3";
        case ComputerClass.A3_DRONE:
          return "D!T3";
        case ComputerClass.A3_WARRIOR:
          return "W!T3";
        default:
          return "NAME ERROR";
      }
    }

    public static string IconTag(this EngineClass type)
    {
      switch (type)
      {
        case EngineClass.GRAVITY:
          return "GRAV";
        case EngineClass.SKIP:
          return "SKIP";
        case EngineClass.FISSION:
          return "FISN";
        case EngineClass.IMPULSE:
          return "IMP";
        case EngineClass.FUSION:
          return "FUSN";
        case EngineClass.NEUTRON:
          return "NEUT";
        case EngineClass.STEALTH:
          return "STLT";
        default:
          return "UNK?";
      }
    }

    public static string Name(this EngineClass type)
    {
      switch (type)
      {
        case EngineClass.GRAVITY:
          return "Gravity Drive";
        case EngineClass.SKIP:
          return "Skip Drive";
        case EngineClass.FISSION:
          return "Fission Drive";
        case EngineClass.IMPULSE:
          return "Impulse Drive";
        case EngineClass.FUSION:
          return "Fusion Drive";
        case EngineClass.NEUTRON:
          return "Neutron Drive";
        case EngineClass.STEALTH:
          return "Stealth Drive";
        default:
          return "Unknown?";
      }
    }

    public static NpcFaction Faction(this HarvesterClass type)
    {
      switch (type)
      {
        case HarvesterClass.LCD_V1:
        case HarvesterClass.LCD_V2:
        case HarvesterClass.LCD_V3:
        case HarvesterClass.LCD_V4:
        case HarvesterClass.LCD_V5:
        case HarvesterClass.LCD_V6:
        case HarvesterClass.LCD_V7:
        case HarvesterClass.LCD_V8:
        case HarvesterClass.LCD_V9:
        case HarvesterClass.LCD_V10:
        case HarvesterClass.LCD_V11:
        case HarvesterClass.LCD_V12:
          return NpcFaction.PIRATE;
        case HarvesterClass.HX_V1:
        case HarvesterClass.HX_V2:
        case HarvesterClass.HX_V3:
        case HarvesterClass.HX_V4:
        case HarvesterClass.HX_V5:
        case HarvesterClass.HX_V6:
        case HarvesterClass.HX_V7:
        case HarvesterClass.HX_V8:
        case HarvesterClass.HX_V9:
        case HarvesterClass.HX_V10:
        case HarvesterClass.HX_V11:
        case HarvesterClass.HX_V12:
          return NpcFaction.HETEROCLITE;
        case HarvesterClass.WP_V1:
        case HarvesterClass.WP_V2:
        case HarvesterClass.WP_V3:
        case HarvesterClass.WP_V4:
        case HarvesterClass.WP_V5:
        case HarvesterClass.WP_V6:
        case HarvesterClass.WP_V7:
        case HarvesterClass.WP_V8:
        case HarvesterClass.WP_V9:
        case HarvesterClass.WP_V10:
        case HarvesterClass.WP_V11:
        case HarvesterClass.WP_V12:
          return NpcFaction.WYRD;
        case HarvesterClass.DCD_V1:
        case HarvesterClass.DCD_V2:
        case HarvesterClass.DCD_V3:
        case HarvesterClass.DCD_V4:
        case HarvesterClass.DCD_V5:
        case HarvesterClass.DCD_V6:
        case HarvesterClass.DCD_V7:
        case HarvesterClass.DCD_V8:
        case HarvesterClass.DCD_V9:
        case HarvesterClass.DCD_V10:
        case HarvesterClass.DCD_V11:
        case HarvesterClass.DCD_V12:
          return NpcFaction.RIFT;
        default:
          return NpcFaction.None;
      }
    }

    public static string IconTag(this HarvesterClass type)
    {
      switch (type)
      {
        case HarvesterClass.LCD_V1:
          return "LCD\nV1";
        case HarvesterClass.HX_V1:
          return "HX\nV1";
        case HarvesterClass.WP_V1:
          return "WP\nV1";
        case HarvesterClass.DCD_V1:
          return "DCD\nV1";
        case HarvesterClass.LCD_V2:
          return "LCD\nV2";
        case HarvesterClass.LCD_V3:
          return "LCD\nV3";
        case HarvesterClass.LCD_V4:
          return "LCD\nV4";
        case HarvesterClass.LCD_V5:
          return "LCD\nV5";
        case HarvesterClass.LCD_V6:
          return "LCD\nV6";
        case HarvesterClass.LCD_V7:
          return "LCD\nV7";
        case HarvesterClass.LCD_V8:
          return "LCD\nV8";
        case HarvesterClass.LCD_V9:
          return "LCD\nV9";
        case HarvesterClass.LCD_V10:
          return "LCD\nV10";
        case HarvesterClass.LCD_V11:
          return "LCD\nV11";
        case HarvesterClass.LCD_V12:
          return "LCD\nV12";
        case HarvesterClass.HX_V2:
          return "HX\nV2";
        case HarvesterClass.HX_V3:
          return "HX\nV3";
        case HarvesterClass.HX_V4:
          return "HX\nV4";
        case HarvesterClass.HX_V5:
          return "HX\nV5";
        case HarvesterClass.HX_V6:
          return "HX\nV6";
        case HarvesterClass.HX_V7:
          return "HX\nV7";
        case HarvesterClass.HX_V8:
          return "HX\nV8";
        case HarvesterClass.HX_V9:
          return "HX\nV9";
        case HarvesterClass.HX_V10:
          return "HX\nV10";
        case HarvesterClass.HX_V11:
          return "HX\nV11";
        case HarvesterClass.HX_V12:
          return "HX\nV12";
        case HarvesterClass.WP_V2:
          return "WP\nV2";
        case HarvesterClass.WP_V3:
          return "WP\nV3";
        case HarvesterClass.WP_V4:
          return "WP\nV4";
        case HarvesterClass.WP_V5:
          return "WP\nV5";
        case HarvesterClass.WP_V6:
          return "WP\nV6";
        case HarvesterClass.WP_V7:
          return "WP\nV7";
        case HarvesterClass.WP_V8:
          return "WP\nV8";
        case HarvesterClass.WP_V9:
          return "WP\nV9";
        case HarvesterClass.WP_V10:
          return "WP\nV10";
        case HarvesterClass.WP_V11:
          return "WP\nV11";
        case HarvesterClass.WP_V12:
          return "WP\nV12";
        case HarvesterClass.DCD_V2:
          return "DCD\nV2";
        case HarvesterClass.DCD_V3:
          return "DCD\nV3";
        case HarvesterClass.DCD_V4:
          return "DCD\nV4";
        case HarvesterClass.DCD_V5:
          return "DCD\nV5";
        case HarvesterClass.DCD_V6:
          return "DCD\nV6";
        case HarvesterClass.DCD_V7:
          return "DCD\nV7";
        case HarvesterClass.DCD_V8:
          return "DCD\nV8";
        case HarvesterClass.DCD_V9:
          return "DCD\nV9";
        case HarvesterClass.DCD_V10:
          return "DCD\nV10";
        case HarvesterClass.DCD_V11:
          return "DCD\nV11";
        case HarvesterClass.DCD_V12:
          return "DCD\nV12";
        default:
          return "?\nV?";
      }
    }

    public static string Name(this HarvesterClass type)
    {
      switch (type)
      {
        case HarvesterClass.LCD_V1:
          return "LCD-V1";
        case HarvesterClass.HX_V1:
          return "HX-V1";
        case HarvesterClass.WP_V1:
          return "WP-V1";
        case HarvesterClass.DCD_V1:
          return "DCD-V1";
        case HarvesterClass.LCD_V2:
          return "LCD-V2";
        case HarvesterClass.LCD_V3:
          return "LCD-V3";
        case HarvesterClass.LCD_V4:
          return "LCD-V4";
        case HarvesterClass.LCD_V5:
          return "LCD-V5";
        case HarvesterClass.LCD_V6:
          return "LCD-V6";
        case HarvesterClass.LCD_V7:
          return "LCD-V7";
        case HarvesterClass.LCD_V8:
          return "LCD-V8";
        case HarvesterClass.LCD_V9:
          return "LCD-V9";
        case HarvesterClass.LCD_V10:
          return "LCD-V10";
        case HarvesterClass.LCD_V11:
          return "LCD-V11";
        case HarvesterClass.LCD_V12:
          return "LCD-V12";
        case HarvesterClass.HX_V2:
          return "HX-V2";
        case HarvesterClass.HX_V3:
          return "HX-V3";
        case HarvesterClass.HX_V4:
          return "HX-V4";
        case HarvesterClass.HX_V5:
          return "HX-V5";
        case HarvesterClass.HX_V6:
          return "HX-V6";
        case HarvesterClass.HX_V7:
          return "HX-V7";
        case HarvesterClass.HX_V8:
          return "HX-V8";
        case HarvesterClass.HX_V9:
          return "HX-V9";
        case HarvesterClass.HX_V10:
          return "HX-V10";
        case HarvesterClass.HX_V11:
          return "HX-V11";
        case HarvesterClass.HX_V12:
          return "HX-V12";
        case HarvesterClass.WP_V2:
          return "WP-V2";
        case HarvesterClass.WP_V3:
          return "WP-V3";
        case HarvesterClass.WP_V4:
          return "WP-V4";
        case HarvesterClass.WP_V5:
          return "WP-V5";
        case HarvesterClass.WP_V6:
          return "WP-V6";
        case HarvesterClass.WP_V7:
          return "WP-V7";
        case HarvesterClass.WP_V8:
          return "WP-V8";
        case HarvesterClass.WP_V9:
          return "WP-V9";
        case HarvesterClass.WP_V10:
          return "WP-V10";
        case HarvesterClass.WP_V11:
          return "WP-V11";
        case HarvesterClass.WP_V12:
          return "WP-V12";
        case HarvesterClass.DCD_V2:
          return "DCD-V2";
        case HarvesterClass.DCD_V3:
          return "DCD-V3";
        case HarvesterClass.DCD_V4:
          return "DCD-V4";
        case HarvesterClass.DCD_V5:
          return "DCD-V5";
        case HarvesterClass.DCD_V6:
          return "DCD-V6";
        case HarvesterClass.DCD_V7:
          return "DCD-V7";
        case HarvesterClass.DCD_V8:
          return "DCD-V8";
        case HarvesterClass.DCD_V9:
          return "DCD-V9";
        case HarvesterClass.DCD_V10:
          return "DCD-V10";
        case HarvesterClass.DCD_V11:
          return "DCD-V11";
        case HarvesterClass.DCD_V12:
          return "DCD-V12";
        default:
          return "Unknown?";
      }
    }

    public static string Name(this ItemRarity rarity)
    {
      switch (rarity)
      {
        case ItemRarity.COMMON:
          return "Common";
        case ItemRarity.UNCOMMON:
          return "Uncommon";
        case ItemRarity.RARE:
          return "Rare";
        case ItemRarity.ULTRA_RARE:
          return "Ultra-Rare";
        case ItemRarity.LEGENDARY:
          return "Legendary";
        case ItemRarity.PRECURSOR:
          return "Precursor";
        case ItemRarity.ULTIMATE:
          return "Ultimate";
        default:
          return "Unknown";
      }
    }

    public static string GetMarkup(this ItemRarity rarity)
    {
      switch (rarity)
      {
        case ItemRarity.COMMON:
          return "[gr]";
        case ItemRarity.UNCOMMON:
          return "[g]";
        case ItemRarity.RARE:
          return "[b]";
        case ItemRarity.ULTRA_RARE:
          return "[v]";
        case ItemRarity.LEGENDARY:
          return "[o]";
        case ItemRarity.PRECURSOR:
          return "[r]";
        case ItemRarity.ULTIMATE:
          return "[w]";
        default:
          return "[?]";
      }
    }

    public static string Name(this ItemType type)
    {
      switch (type)
      {
        case ItemType.WEAPON:
          return "Weapon";
        case ItemType.ARMOR:
          return "Armor";
        case ItemType.STORAGE:
          return "Storage";
        case ItemType.HARVESTER:
          return "Harvester";
        case ItemType.ENGINE:
          return "Engine";
        case ItemType.COMPUTER:
          return "Computer";
        case ItemType.SPECIAL:
          return "Special";
        default:
          return "NULL";
      }
    }

    public static string Name(this SpecialClass type)
    {
      switch (type)
      {
        case SpecialClass.TECHNICIAN:
          return "Technician";
        case SpecialClass.PROSPECTOR:
          return "Prospector";
        case SpecialClass.TANK:
          return "Tank";
        case SpecialClass.SCOUT:
          return "Scout";
        case SpecialClass.TRACTOR:
          return "Tractor Beam";
        case SpecialClass.NOVA:
          return "Nova Device";
        case SpecialClass.BATTLE_RAM:
          return "Battle Ram";
        case SpecialClass.ALIEN_HUNTER:
          return "Alien Hunter";
        case SpecialClass.BOUNTY_HUNTER:
          return "Bounty Hunter";
        case SpecialClass.DEFLECTOR:
          return "Deflector";
        case SpecialClass.WEIRD_ALIEN_ARTIFACT:
          return "Weird Alien Artifact";
        case SpecialClass.GRAPPLING_HOOK:
          return "Grappling Hook";
        case SpecialClass.ADVANCED_CONSTRUCT:
          return "Advanced Construct";
        case SpecialClass.ADVANCED_SHIELDS:
          return "Advanced Shields";
        case SpecialClass.ADVANCED_ELECTRONICS:
          return "Advanced Electronics";
        case SpecialClass.ADVANCED_MUNITIONS:
          return "Advanced Munitions";
        case SpecialClass.ADVANCED_PROPULSION:
          return "Advanced Propulsion";
        default:
          return "Unknown?";
      }
    }

    public static string IconTag(this SpecialClass type)
    {
      switch (type)
      {
        case SpecialClass.TECHNICIAN:
          return "TECH";
        case SpecialClass.PROSPECTOR:
          return "MINE";
        case SpecialClass.TANK:
          return "TANK";
        case SpecialClass.SCOUT:
          return "SCOT";
        case SpecialClass.TRACTOR:
          return "TRAC";
        case SpecialClass.NOVA:
          return "NOVA";
        case SpecialClass.BATTLE_RAM:
          return "RAM";
        case SpecialClass.ALIEN_HUNTER:
          return "ALIN";
        case SpecialClass.BOUNTY_HUNTER:
          return "BNTY";
        case SpecialClass.DEFLECTOR:
          return "DEF";
        case SpecialClass.WEIRD_ALIEN_ARTIFACT:
          return "WAA!";
        case SpecialClass.GRAPPLING_HOOK:
          return "HOOK";
        case SpecialClass.ADVANCED_CONSTRUCT:
          return "CONS";
        case SpecialClass.ADVANCED_SHIELDS:
          return "SHLD";
        case SpecialClass.ADVANCED_ELECTRONICS:
          return "ELEC";
        case SpecialClass.ADVANCED_MUNITIONS:
          return "MUNI";
        case SpecialClass.ADVANCED_PROPULSION:
          return "PROP";
        default:
          return "ERR";
      }
    }

    public static string Name(this WeaponClass type)
    {
      switch (type)
      {
        case WeaponClass.AUTO_CANNON:
          return "Auto Cannon";
        case WeaponClass.MASS_DRIVER:
          return "Mass Driver";
        case WeaponClass.LEVIATHAN:
          return "Leviathan";
        case WeaponClass.PULVERIZER:
          return "Pulverizer";
        case WeaponClass.RIPPER:
          return "Ripper";
        case WeaponClass.SCREAMER:
          return "Screamer";
        case WeaponClass.HELLCANNON:
          return "Hellcannon";
        case WeaponClass.BURST_CANNON:
          return "Burst Cannon";
        case WeaponClass.PROTON_LAUNCHER:
          return "Proton Launcher";
        case WeaponClass.FUSION_BEAM:
          return "Fusion Beam";
        case WeaponClass.PHASER:
          return "Phaser";
        case WeaponClass.GAUSS_CANNON:
          return "Gauss Cannon";
        case WeaponClass.MESON_BLASTER:
          return "Meson Blaster";
        case WeaponClass.OMEGA_RIFLE:
          return "Omega Rifle";
        case WeaponClass.ACCELERATOR:
          return "Accelerator";
        case WeaponClass.RAIL_GUN:
          return "Rail Gun";
        case WeaponClass.DISRUPTOR:
          return "Disruptor";
        case WeaponClass.GRAVITY_SMASHER:
          return "Gravity Smasher";
        case WeaponClass.ION_CANNON:
          return "Ion Cannon";
        case WeaponClass.PLASMA_LANCE:
          return "Plasma Lance";
        case WeaponClass.MATTER_INVERTER:
          return "Matter Inverter";
        case WeaponClass.RAPTURE:
          return "Rapture";
        case WeaponClass.GLORY:
          return "Glory";
        case WeaponClass.OBLIVION:
          return "Oblivion";
        case WeaponClass.HORROR:
          return "Horror";
        case WeaponClass.RUIN:
          return "Ruin";
        case WeaponClass.CATACLYSM:
          return "Cataclysm";
        case WeaponClass.TORMENT:
          return "Torment";
        case WeaponClass.SMOLDER:
          return "Smolder";
        case WeaponClass.DESTRUCTION:
          return "Destruction";
        case WeaponClass.FRENZY:
          return "Frenzy";
        case WeaponClass.SILENCE:
          return "Silence";
        case WeaponClass.EXODUS:
          return "Exodus";
        case WeaponClass.DARKNESS:
          return "Darkness";
        case WeaponClass.AGONY:
          return "Agony";
        case WeaponClass.PROPHECY:
          return "Prophecy";
        case WeaponClass.RADIANCE:
          return "Radiance";
        case WeaponClass.ANIMUS:
          return "Animus";
        case WeaponClass.PAIN:
          return "Pain";
        case WeaponClass.SUCCUBUS:
          return "Succubus";
        case WeaponClass.BANSHEE:
          return "Banshee";
        case WeaponClass.BASILISK:
          return "Basilisk";
        case WeaponClass.HARPY:
          return "Harpy";
        case WeaponClass.WYVERN:
          return "Wyvern";
        case WeaponClass.VIPER:
          return "Viper";
        case WeaponClass.PENATRATOR:
          return "Penatrator";
        case WeaponClass.SERPENT:
          return "Serpent";
        case WeaponClass.HYDRA:
          return "Hydra";
        case WeaponClass.FIRECAT:
          return "Firecat";
        case WeaponClass.OPHIDIAN:
          return "Ophidian";
        case WeaponClass.BEHEMOTH:
          return "Behemoth";
        case WeaponClass.GARGOYLE:
          return "Gargoyle";
        case WeaponClass.KRAKEN:
          return "Kraken";
        case WeaponClass.DRAGON:
          return "Dragon";
        case WeaponClass.MUTILATOR:
          return "Mutilator";
        case WeaponClass.STARSHATTER:
          return "Starshatter";
        case WeaponClass.STRIKER:
          return "Striker";
        case WeaponClass.EXTERMINATOR:
          return "Exterminator";
        case WeaponClass.VOIDBLASTER:
          return "Voidblaster";
        case WeaponClass.RAVAGER:
          return "Ravager";
        case WeaponClass.BRUTALIZER:
          return "Brutalizer";
        case WeaponClass.VAPORIZER:
          return "Vaporizer";
        case WeaponClass.DESOLATOR:
          return "Desolator";
        case WeaponClass.ATOMIZER:
          return "Atomizer";
        case WeaponClass.CORRUPTOR:
          return "Corruptor";
        case WeaponClass.MINDSLAYER:
          return "Mindslayer";
        case WeaponClass.RIFTBREAKER:
          return "Riftbreaker";
        case WeaponClass.SOULTAKER:
          return "Soultaker";
        case WeaponClass.NULLCANNON:
          return "Nullcannon";
        case WeaponClass.DEMOLISHER:
          return "Demolisher";
        case WeaponClass.INCINERATOR:
          return "Incinerator";
        case WeaponClass.ERADICATOR:
          return "Eradicator";
        default:
          return "Unknown?";
      }
    }

    public static int AttackSpeedMS(this WeaponClass type)
    {
      switch (type)
      {
        case WeaponClass.AUTO_CANNON:
        case WeaponClass.RIPPER:
        case WeaponClass.OMEGA_RIFLE:
        case WeaponClass.OBLIVION:
        case WeaponClass.BANSHEE:
        case WeaponClass.STRIKER:
        case WeaponClass.VAPORIZER:
          return 5000;
        case WeaponClass.MASS_DRIVER:
        case WeaponClass.PROTON_LAUNCHER:
        case WeaponClass.GLORY:
        case WeaponClass.SUCCUBUS:
        case WeaponClass.WYVERN:
        case WeaponClass.STARSHATTER:
        case WeaponClass.ATOMIZER:
          return 4000;
        case WeaponClass.LEVIATHAN:
        case WeaponClass.PHASER:
        case WeaponClass.RUIN:
        case WeaponClass.HARPY:
        case WeaponClass.SERPENT:
        case WeaponClass.VOIDBLASTER:
        case WeaponClass.BRUTALIZER:
          return 7000;
        case WeaponClass.PULVERIZER:
        case WeaponClass.ACCELERATOR:
        case WeaponClass.PROPHECY:
        case WeaponClass.FIRECAT:
        case WeaponClass.BEHEMOTH:
        case WeaponClass.CORRUPTOR:
        case WeaponClass.MINDSLAYER:
          return 17000;
        case WeaponClass.SCREAMER:
        case WeaponClass.BURST_CANNON:
        case WeaponClass.GAUSS_CANNON:
        case WeaponClass.RAPTURE:
        case WeaponClass.VIPER:
        case WeaponClass.MUTILATOR:
        case WeaponClass.DESOLATOR:
          return 3000;
        case WeaponClass.HELLCANNON:
        case WeaponClass.DISRUPTOR:
        case WeaponClass.PLASMA_LANCE:
        case WeaponClass.ANIMUS:
        case WeaponClass.KRAKEN:
        case WeaponClass.SOULTAKER:
        case WeaponClass.INCINERATOR:
          return 19000;
        case WeaponClass.FUSION_BEAM:
        case WeaponClass.MESON_BLASTER:
        case WeaponClass.HORROR:
        case WeaponClass.BASILISK:
        case WeaponClass.PENATRATOR:
        case WeaponClass.EXTERMINATOR:
        case WeaponClass.RAVAGER:
          return 6000;
        case WeaponClass.RAIL_GUN:
        case WeaponClass.ION_CANNON:
        case WeaponClass.RADIANCE:
        case WeaponClass.HYDRA:
        case WeaponClass.GARGOYLE:
        case WeaponClass.NULLCANNON:
        case WeaponClass.DEMOLISHER:
          return 18000;
        case WeaponClass.GRAVITY_SMASHER:
        case WeaponClass.MATTER_INVERTER:
        case WeaponClass.PAIN:
        case WeaponClass.OPHIDIAN:
        case WeaponClass.DRAGON:
        case WeaponClass.RIFTBREAKER:
        case WeaponClass.ERADICATOR:
          return 20000;
        case WeaponClass.CATACLYSM:
          return 8000;
        case WeaponClass.TORMENT:
          return 9000;
        case WeaponClass.SMOLDER:
          return 10000;
        case WeaponClass.DESTRUCTION:
          return 11000;
        case WeaponClass.FRENZY:
          return 12000;
        case WeaponClass.SILENCE:
          return 13000;
        case WeaponClass.EXODUS:
          return 14000;
        case WeaponClass.DARKNESS:
          return 15000;
        case WeaponClass.AGONY:
          return 16000;
        default:
          return int.MaxValue;
      }
    }

    public static string IconTag(this WeaponClass type, int baseEp)
    {
      switch (type)
      {
        case WeaponClass.AUTO_CANNON:
          return Util.NUMERALS[baseEp] + "\nAUTO";
        case WeaponClass.MASS_DRIVER:
          return Util.NUMERALS[baseEp] + "\nMASS";
        case WeaponClass.LEVIATHAN:
          return Util.NUMERALS[baseEp] + "\nLVTH";
        case WeaponClass.PULVERIZER:
          return Util.NUMERALS[baseEp] + "\nPULV";
        case WeaponClass.RIPPER:
          return Util.NUMERALS[baseEp] + "\nRIPR";
        case WeaponClass.SCREAMER:
          return Util.NUMERALS[baseEp] + "\nSCRM";
        case WeaponClass.HELLCANNON:
          return Util.NUMERALS[baseEp] + "\nHELL";
        case WeaponClass.BURST_CANNON:
          return Util.NUMERALS[baseEp] + "\nBRST";
        case WeaponClass.PROTON_LAUNCHER:
          return Util.NUMERALS[baseEp] + "\nPRTO";
        case WeaponClass.FUSION_BEAM:
          return Util.NUMERALS[baseEp] + "\nFUSN";
        case WeaponClass.PHASER:
          return Util.NUMERALS[baseEp] + "\nPHSR";
        case WeaponClass.GAUSS_CANNON:
          return Util.NUMERALS[baseEp] + "\nGAUS";
        case WeaponClass.MESON_BLASTER:
          return Util.NUMERALS[baseEp] + "\nMESO";
        case WeaponClass.OMEGA_RIFLE:
          return Util.NUMERALS[baseEp] + "\nOMGA";
        case WeaponClass.ACCELERATOR:
          return Util.NUMERALS[baseEp] + "\nACCL";
        case WeaponClass.RAIL_GUN:
          return Util.NUMERALS[baseEp] + "\nRAIL";
        case WeaponClass.DISRUPTOR:
          return Util.NUMERALS[baseEp] + "\nDISR";
        case WeaponClass.GRAVITY_SMASHER:
          return Util.NUMERALS[baseEp] + "\nGRAV";
        case WeaponClass.ION_CANNON:
          return Util.NUMERALS[baseEp] + "\nIONC";
        case WeaponClass.PLASMA_LANCE:
          return Util.NUMERALS[baseEp] + "\nPLSM";
        case WeaponClass.MATTER_INVERTER:
          return Util.NUMERALS[baseEp] + "\nMATR";
        case WeaponClass.RAPTURE:
          return Util.NUMERALS[baseEp] + "\nRPTR";
        case WeaponClass.GLORY:
          return Util.NUMERALS[baseEp] + "\nGLRY";
        case WeaponClass.OBLIVION:
          return Util.NUMERALS[baseEp] + "\nOBLV";
        case WeaponClass.HORROR:
          return Util.NUMERALS[baseEp] + "\nHROR";
        case WeaponClass.RUIN:
          return Util.NUMERALS[baseEp] + "\nRUIN";
        case WeaponClass.CATACLYSM:
          return Util.NUMERALS[baseEp] + "\nCATA";
        case WeaponClass.TORMENT:
          return Util.NUMERALS[baseEp] + "\nTORM";
        case WeaponClass.SMOLDER:
          return Util.NUMERALS[baseEp] + "\nSMLD";
        case WeaponClass.DESTRUCTION:
          return Util.NUMERALS[baseEp] + "\nDSTR";
        case WeaponClass.FRENZY:
          return Util.NUMERALS[baseEp] + "\nFRZY";
        case WeaponClass.SILENCE:
          return Util.NUMERALS[baseEp] + "\nSLNC";
        case WeaponClass.EXODUS:
          return Util.NUMERALS[baseEp] + "\nXODS";
        case WeaponClass.DARKNESS:
          return Util.NUMERALS[baseEp] + "\nDARK";
        case WeaponClass.AGONY:
          return Util.NUMERALS[baseEp] + "\nAGNY";
        case WeaponClass.PROPHECY:
          return Util.NUMERALS[baseEp] + "\nPRPH";
        case WeaponClass.RADIANCE:
          return Util.NUMERALS[baseEp] + "\nRADI";
        case WeaponClass.ANIMUS:
          return Util.NUMERALS[baseEp] + "\nANIM";
        case WeaponClass.PAIN:
          return Util.NUMERALS[baseEp] + "\nPAIN";
        case WeaponClass.SUCCUBUS:
          return Util.NUMERALS[baseEp] + "\nSUCU";
        case WeaponClass.BANSHEE:
          return Util.NUMERALS[baseEp] + "\nBNSH";
        case WeaponClass.BASILISK:
          return Util.NUMERALS[baseEp] + "\nBSIL";
        case WeaponClass.HARPY:
          return Util.NUMERALS[baseEp] + "\nHRPY";
        case WeaponClass.WYVERN:
          return Util.NUMERALS[baseEp] + "\nWYRN";
        case WeaponClass.VIPER:
          return Util.NUMERALS[baseEp] + "\nVIPR";
        case WeaponClass.PENATRATOR:
          return Util.NUMERALS[baseEp] + "\nPENA";
        case WeaponClass.SERPENT:
          return Util.NUMERALS[baseEp] + "\nSERP";
        case WeaponClass.HYDRA:
          return Util.NUMERALS[baseEp] + "\nHYDR";
        case WeaponClass.FIRECAT:
          return Util.NUMERALS[baseEp] + "\nFCAT";
        case WeaponClass.OPHIDIAN:
          return Util.NUMERALS[baseEp] + "\nOPHD";
        case WeaponClass.BEHEMOTH:
          return Util.NUMERALS[baseEp] + "\nBEHE";
        case WeaponClass.GARGOYLE:
          return Util.NUMERALS[baseEp] + "\nGARG";
        case WeaponClass.KRAKEN:
          return Util.NUMERALS[baseEp] + "\nKRAK";
        case WeaponClass.DRAGON:
          return Util.NUMERALS[baseEp] + "\nDRGN";
        case WeaponClass.MUTILATOR:
          return Util.NUMERALS[baseEp] + "\nMUTI";
        case WeaponClass.STARSHATTER:
          return Util.NUMERALS[baseEp] + "\nSTAR";
        case WeaponClass.STRIKER:
          return Util.NUMERALS[baseEp] + "\nSTRK";
        case WeaponClass.EXTERMINATOR:
          return Util.NUMERALS[baseEp] + "\nXTRM";
        case WeaponClass.VOIDBLASTER:
          return Util.NUMERALS[baseEp] + "\nVOID";
        case WeaponClass.RAVAGER:
          return Util.NUMERALS[baseEp] + "\nRVGR";
        case WeaponClass.BRUTALIZER:
          return Util.NUMERALS[baseEp] + "\nBRTL";
        case WeaponClass.VAPORIZER:
          return Util.NUMERALS[baseEp] + "\nVPRZ";
        case WeaponClass.DESOLATOR:
          return Util.NUMERALS[baseEp] + "\nDESO";
        case WeaponClass.ATOMIZER:
          return Util.NUMERALS[baseEp] + "\nATOM";
        case WeaponClass.CORRUPTOR:
          return Util.NUMERALS[baseEp] + "\nCRPT";
        case WeaponClass.MINDSLAYER:
          return Util.NUMERALS[baseEp] + "\nMIND";
        case WeaponClass.RIFTBREAKER:
          return Util.NUMERALS[baseEp] + "\nRIFT";
        case WeaponClass.SOULTAKER:
          return Util.NUMERALS[baseEp] + "\nSOUL";
        case WeaponClass.NULLCANNON:
          return Util.NUMERALS[baseEp] + "\nNULL";
        case WeaponClass.DEMOLISHER:
          return Util.NUMERALS[baseEp] + "\nDEMO";
        case WeaponClass.INCINERATOR:
          return Util.NUMERALS[baseEp] + "\nINCN";
        case WeaponClass.ERADICATOR:
          return Util.NUMERALS[baseEp] + "\nERAD";
        default:
          return Util.NUMERALS[baseEp] + "\n?";
      }
    }

    public static NpcFaction Faction(this WeaponClass type)
    {
      switch (type)
      {
        case WeaponClass.AUTO_CANNON:
        case WeaponClass.MASS_DRIVER:
        case WeaponClass.LEVIATHAN:
        case WeaponClass.PULVERIZER:
        case WeaponClass.BURST_CANNON:
        case WeaponClass.PROTON_LAUNCHER:
        case WeaponClass.FUSION_BEAM:
        case WeaponClass.PHASER:
        case WeaponClass.GAUSS_CANNON:
        case WeaponClass.MESON_BLASTER:
        case WeaponClass.OMEGA_RIFLE:
        case WeaponClass.ACCELERATOR:
        case WeaponClass.RAIL_GUN:
        case WeaponClass.DISRUPTOR:
        case WeaponClass.GRAVITY_SMASHER:
        case WeaponClass.ION_CANNON:
        case WeaponClass.PLASMA_LANCE:
        case WeaponClass.MATTER_INVERTER:
          return NpcFaction.PIRATE;
        case WeaponClass.RIPPER:
        case WeaponClass.SCREAMER:
        case WeaponClass.HELLCANNON:
        case WeaponClass.SUCCUBUS:
        case WeaponClass.BANSHEE:
        case WeaponClass.BASILISK:
        case WeaponClass.HARPY:
        case WeaponClass.WYVERN:
        case WeaponClass.VIPER:
        case WeaponClass.PENATRATOR:
        case WeaponClass.SERPENT:
        case WeaponClass.HYDRA:
        case WeaponClass.FIRECAT:
        case WeaponClass.OPHIDIAN:
        case WeaponClass.BEHEMOTH:
        case WeaponClass.GARGOYLE:
        case WeaponClass.KRAKEN:
        case WeaponClass.DRAGON:
          return NpcFaction.WYRD;
        case WeaponClass.RAPTURE:
        case WeaponClass.GLORY:
        case WeaponClass.OBLIVION:
        case WeaponClass.HORROR:
        case WeaponClass.RUIN:
        case WeaponClass.CATACLYSM:
        case WeaponClass.TORMENT:
        case WeaponClass.SMOLDER:
        case WeaponClass.DESTRUCTION:
        case WeaponClass.FRENZY:
        case WeaponClass.SILENCE:
        case WeaponClass.EXODUS:
        case WeaponClass.DARKNESS:
        case WeaponClass.AGONY:
        case WeaponClass.PROPHECY:
        case WeaponClass.RADIANCE:
        case WeaponClass.ANIMUS:
        case WeaponClass.PAIN:
          return NpcFaction.HETEROCLITE;
        case WeaponClass.MUTILATOR:
        case WeaponClass.STARSHATTER:
        case WeaponClass.STRIKER:
        case WeaponClass.EXTERMINATOR:
        case WeaponClass.VOIDBLASTER:
        case WeaponClass.RAVAGER:
        case WeaponClass.BRUTALIZER:
        case WeaponClass.VAPORIZER:
        case WeaponClass.DESOLATOR:
        case WeaponClass.ATOMIZER:
        case WeaponClass.CORRUPTOR:
        case WeaponClass.MINDSLAYER:
        case WeaponClass.RIFTBREAKER:
        case WeaponClass.SOULTAKER:
        case WeaponClass.NULLCANNON:
        case WeaponClass.DEMOLISHER:
        case WeaponClass.INCINERATOR:
        case WeaponClass.ERADICATOR:
          return NpcFaction.RIFT;
        default:
          return NpcFaction.None;
      }
    }
  }
}
