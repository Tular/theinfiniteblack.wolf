﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.HarvesterClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public enum HarvesterClass
  {
    NULL = -128,
    LCD_V1 = 0,
    HX_V1 = 1,
    WP_V1 = 2,
    DCD_V1 = 3,
    LCD_V2 = 11,
    LCD_V3 = 12,
    LCD_V4 = 13,
    LCD_V5 = 14,
    LCD_V6 = 15,
    LCD_V7 = 16,
    LCD_V8 = 17,
    LCD_V9 = 18,
    LCD_V10 = 19,
    LCD_V11 = 20,
    LCD_V12 = 21,
    HX_V2 = 31,
    HX_V3 = 32,
    HX_V4 = 33,
    HX_V5 = 34,
    HX_V6 = 35,
    HX_V7 = 36,
    HX_V8 = 37,
    HX_V9 = 38,
    HX_V10 = 39,
    HX_V11 = 40,
    HX_V12 = 41,
    WP_V2 = 51,
    WP_V3 = 52,
    WP_V4 = 53,
    WP_V5 = 54,
    WP_V6 = 55,
    WP_V7 = 56,
    WP_V8 = 57,
    WP_V9 = 58,
    WP_V10 = 59,
    WP_V11 = 60,
    WP_V12 = 61,
    DCD_V2 = 71,
    DCD_V3 = 72,
    DCD_V4 = 73,
    DCD_V5 = 74,
    DCD_V6 = 75,
    DCD_V7 = 76,
    DCD_V8 = 77,
    DCD_V9 = 78,
    DCD_V10 = 79,
    DCD_V11 = 80,
    DCD_V12 = 81,
  }
}
