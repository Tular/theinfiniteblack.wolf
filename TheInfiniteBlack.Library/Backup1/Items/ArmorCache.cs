﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ArmorCache
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Library.Items
{
  public static class ArmorCache
  {
    private static readonly Dictionary<ArmorClass, Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, ArmorItem[]>>>> _cache = new Dictionary<ArmorClass, Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, ArmorItem[]>>>>();

    public static ArmorItem Get(IByteBuffer input)
    {
      ItemRarity rarity = (ItemRarity) input.Get();
      sbyte durability = input.Get();
      bool noDrop = input.GetBool();
      bool bindOnEquip = !noDrop && input.GetBool();
      return ArmorCache.Get((ArmorClass) input.Get(), input.Get(), rarity, durability, noDrop, bindOnEquip);
    }

    public static ArmorItem Get(ArmorClass type, sbyte ep, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      lock (ArmorCache._cache)
      {
        Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, ArmorItem[]>>> dictionary1;
        if (!ArmorCache._cache.TryGetValue(type, out dictionary1))
        {
          dictionary1 = new Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, ArmorItem[]>>>();
          ArmorCache._cache[type] = dictionary1;
        }
        Dictionary<ItemRarity, Dictionary<sbyte, ArmorItem[]>> dictionary2;
        if (!dictionary1.TryGetValue(ep, out dictionary2))
        {
          dictionary2 = new Dictionary<ItemRarity, Dictionary<sbyte, ArmorItem[]>>();
          dictionary1[ep] = dictionary2;
        }
        Dictionary<sbyte, ArmorItem[]> dictionary3;
        if (!dictionary2.TryGetValue(rarity, out dictionary3))
        {
          dictionary3 = new Dictionary<sbyte, ArmorItem[]>();
          dictionary2[rarity] = dictionary3;
        }
        ArmorItem[] armorItemArray;
        if (!dictionary3.TryGetValue(durability, out armorItemArray))
        {
          armorItemArray = new ArmorItem[4];
          dictionary3[durability] = armorItemArray;
        }
        int index = noDrop ? (bindOnEquip ? 0 : 1) : (bindOnEquip ? 2 : 3);
        ArmorItem armorItem = armorItemArray[index];
        if (armorItem == null)
        {
          armorItem = new ArmorItem(type, ep, rarity, durability, noDrop, bindOnEquip);
          armorItemArray[index] = armorItem;
        }
        return armorItem;
      }
    }

    public static ArmorItem GetUpgrade(EngineeringType type, ArmorItem item)
    {
      if (!item.CanUpgrade(type))
        return (ArmorItem) null;
      switch (type)
      {
        case EngineeringType.REPAIR:
          return ArmorCache.Get(item.Class, item.BaseEP, item.Rarity, (int) item.Durability + 10 >= 100 ? (sbyte) 100 : (sbyte) ((int) item.Durability + 10), item.NoDrop, item.BindOnEquip);
        case EngineeringType.RANK_UP:
          return ArmorCache.Get(item.Class, (sbyte) ((int) item.BaseEP + 1), item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RANK_DOWN:
          return ArmorCache.Get(item.Class, (sbyte) ((int) item.BaseEP - 1), item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RARITY:
          return ArmorCache.Get(item.Class, item.BaseEP, item.Rarity + 1, item.Durability, item.NoDrop, item.BindOnEquip);
        default:
          return (ArmorItem) null;
      }
    }
  }
}
