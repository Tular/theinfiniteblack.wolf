﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.SpecialCache
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Items
{
  public static class SpecialCache
  {
    private static readonly Dictionary<SpecialClass, Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, SpecialItem[]>>>> _cache = new Dictionary<SpecialClass, Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, SpecialItem[]>>>>();

    public static SpecialItem Get(IByteBuffer input)
    {
      ItemRarity rarity = (ItemRarity) input.Get();
      sbyte durability = input.Get();
      bool noDrop = input.GetBool();
      bool bindOnEquip = !noDrop && input.GetBool();
      return SpecialCache.Get((SpecialClass) input.Get(), (ShipClass) input.Get(), rarity, durability, noDrop, bindOnEquip);
    }

    public static SpecialItem Get(SpecialClass type, ShipClass ship, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      lock (SpecialCache._cache)
      {
        Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, SpecialItem[]>>> dictionary1;
        if (!SpecialCache._cache.TryGetValue(type, out dictionary1))
        {
          dictionary1 = new Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, SpecialItem[]>>>();
          SpecialCache._cache[type] = dictionary1;
        }
        Dictionary<ItemRarity, Dictionary<sbyte, SpecialItem[]>> dictionary2;
        if (!dictionary1.TryGetValue(ship, out dictionary2))
        {
          dictionary2 = new Dictionary<ItemRarity, Dictionary<sbyte, SpecialItem[]>>();
          dictionary1[ship] = dictionary2;
        }
        Dictionary<sbyte, SpecialItem[]> dictionary3;
        if (!dictionary2.TryGetValue(rarity, out dictionary3))
        {
          dictionary3 = new Dictionary<sbyte, SpecialItem[]>();
          dictionary2[rarity] = dictionary3;
        }
        SpecialItem[] specialItemArray;
        if (!dictionary3.TryGetValue(durability, out specialItemArray))
        {
          specialItemArray = new SpecialItem[4];
          dictionary3[durability] = specialItemArray;
        }
        int index = noDrop ? (bindOnEquip ? 0 : 1) : (bindOnEquip ? 2 : 3);
        SpecialItem specialItem = specialItemArray[index];
        if (specialItem == null)
        {
          specialItem = new SpecialItem(type, ship, rarity, durability, noDrop, bindOnEquip);
          specialItemArray[index] = specialItem;
        }
        return specialItem;
      }
    }

    public static SpecialItem GetUpgrade(EngineeringType type, SpecialItem item)
    {
      if (!item.CanUpgrade(type))
        return (SpecialItem) null;
      switch (type)
      {
        case EngineeringType.REPAIR:
          return SpecialCache.Get(item.Class, item.RequiredShip, item.Rarity, (int) item.Durability + 10 >= 100 ? (sbyte) 100 : (sbyte) ((int) item.Durability + 10), item.NoDrop, item.BindOnEquip);
        case EngineeringType.CLASS_UP:
          return SpecialCache.Get(item.Class, item.RequiredShip + 1, item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.CLASS_DOWN:
          return SpecialCache.Get(item.Class, item.RequiredShip - 1, item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RARITY:
          return SpecialCache.Get(item.Class, item.RequiredShip, item.Rarity + 1, item.Durability, item.NoDrop, item.BindOnEquip);
        default:
          return (SpecialItem) null;
      }
    }
  }
}
