﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.EngineCache
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Library.Items
{
  public static class EngineCache
  {
    private static readonly Dictionary<EngineClass, Dictionary<ItemRarity, Dictionary<sbyte, EngineItem[]>>> _cache = new Dictionary<EngineClass, Dictionary<ItemRarity, Dictionary<sbyte, EngineItem[]>>>();

    public static EngineItem Get(IByteBuffer input)
    {
      ItemRarity rarity = (ItemRarity) input.Get();
      sbyte durability = input.Get();
      bool noDrop = input.GetBool();
      bool bindOnEquip = !noDrop && input.GetBool();
      return EngineCache.Get((EngineClass) input.Get(), rarity, durability, noDrop, bindOnEquip);
    }

    public static EngineItem Get(EngineClass type, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      lock (EngineCache._cache)
      {
        Dictionary<ItemRarity, Dictionary<sbyte, EngineItem[]>> dictionary1;
        if (!EngineCache._cache.TryGetValue(type, out dictionary1))
        {
          dictionary1 = new Dictionary<ItemRarity, Dictionary<sbyte, EngineItem[]>>();
          EngineCache._cache[type] = dictionary1;
        }
        Dictionary<sbyte, EngineItem[]> dictionary2;
        if (!dictionary1.TryGetValue(rarity, out dictionary2))
        {
          dictionary2 = new Dictionary<sbyte, EngineItem[]>();
          dictionary1[rarity] = dictionary2;
        }
        EngineItem[] engineItemArray;
        if (!dictionary2.TryGetValue(durability, out engineItemArray))
        {
          engineItemArray = new EngineItem[4];
          dictionary2[durability] = engineItemArray;
        }
        int index = noDrop ? (bindOnEquip ? 0 : 1) : (bindOnEquip ? 2 : 3);
        EngineItem engineItem = engineItemArray[index];
        if (engineItem == null)
        {
          engineItem = new EngineItem(type, rarity, durability, noDrop, bindOnEquip);
          engineItemArray[index] = engineItem;
        }
        return engineItem;
      }
    }

    public static EngineItem GetUpgrade(EngineeringType type, EngineItem item)
    {
      if (!item.CanUpgrade(type))
        return (EngineItem) null;
      if (type == EngineeringType.REPAIR)
        return EngineCache.Get(item.Class, item.Rarity, (int) item.Durability + 10 >= 100 ? (sbyte) 100 : (sbyte) ((int) item.Durability + 10), item.NoDrop, item.BindOnEquip);
      if (type != EngineeringType.RARITY)
        return (EngineItem) null;
      return EngineCache.Get(item.Class, item.Rarity + 1, item.Durability, item.NoDrop, item.BindOnEquip);
    }
  }
}
