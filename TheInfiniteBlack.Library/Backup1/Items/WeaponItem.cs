﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.WeaponItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Items
{
  public class WeaponItem : EquipmentItem
  {
    private readonly WeaponClass _class;
    private readonly sbyte _baseEp;

    public override ItemType Type
    {
      get
      {
        return ItemType.WEAPON;
      }
    }

    public override int MaxCreditValue
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.MUTILATOR:
          case WeaponClass.STARSHATTER:
          case WeaponClass.STRIKER:
          case WeaponClass.EXTERMINATOR:
          case WeaponClass.VOIDBLASTER:
          case WeaponClass.RAVAGER:
          case WeaponClass.BRUTALIZER:
          case WeaponClass.VAPORIZER:
          case WeaponClass.DESOLATOR:
          case WeaponClass.ATOMIZER:
          case WeaponClass.CORRUPTOR:
          case WeaponClass.MINDSLAYER:
          case WeaponClass.RIFTBREAKER:
          case WeaponClass.SOULTAKER:
          case WeaponClass.NULLCANNON:
          case WeaponClass.DEMOLISHER:
          case WeaponClass.INCINERATOR:
          case WeaponClass.ERADICATOR:
            if (this.Rarity == ItemRarity.COMMON)
              return 2400 + (int) this._baseEp * 50;
            int rarity1 = (int) this.Rarity;
            return rarity1 * rarity1 * rarity1 * 4800 + (int) this._baseEp * 100 * rarity1;
          default:
            if (this.Rarity == ItemRarity.COMMON)
              return 1600 + (int) this._baseEp * 50;
            int rarity2 = (int) this.Rarity;
            return rarity2 * rarity2 * rarity2 * 3200 + (int) this._baseEp * 100 * rarity2;
        }
      }
    }

    public override int EPCost
    {
      get
      {
        int num = (int) ((int) this._baseEp - this.Rarity + 1);
        if (num > 0)
          return num;
        return 0;
      }
    }

    public override string IconTag
    {
      get
      {
        return this._class.IconTag((int) this._baseEp);
      }
    }

    public int AttackSpeedMS
    {
      get
      {
        return this._class.AttackSpeedMS();
      }
    }

    public override int EPBonus
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.PLASMA_LANCE:
          case WeaponClass.STARSHATTER:
          case WeaponClass.STRIKER:
          case WeaponClass.RAVAGER:
          case WeaponClass.BRUTALIZER:
          case WeaponClass.ATOMIZER:
          case WeaponClass.CORRUPTOR:
          case WeaponClass.SOULTAKER:
          case WeaponClass.NULLCANNON:
          case WeaponClass.ERADICATOR:
          case WeaponClass.PROTON_LAUNCHER:
          case WeaponClass.FUSION_BEAM:
          case WeaponClass.ACCELERATOR:
            return 1;
          default:
            return 0;
        }
      }
    }

    public override int ResourceCapacity
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.FUSION_BEAM:
            return (int) this.Rarity * 2;
          case WeaponClass.RAPTURE:
          case WeaponClass.OBLIVION:
          case WeaponClass.RUIN:
          case WeaponClass.TORMENT:
          case WeaponClass.DESTRUCTION:
          case WeaponClass.SILENCE:
          case WeaponClass.DARKNESS:
          case WeaponClass.PROPHECY:
          case WeaponClass.ANIMUS:
            return (int) this.Rarity * 3;
          case WeaponClass.SMOLDER:
            return (int) this.Rarity;
          default:
            return 0;
        }
      }
    }

    public override int MoveSpeedMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.ACCELERATOR:
            return (int) this.Rarity * -300;
          case WeaponClass.RAIL_GUN:
            return (int) this.Rarity * -100;
          case WeaponClass.EXODUS:
          case WeaponClass.MUTILATOR:
          case WeaponClass.STARSHATTER:
          case WeaponClass.STRIKER:
          case WeaponClass.EXTERMINATOR:
          case WeaponClass.VOIDBLASTER:
          case WeaponClass.RAVAGER:
          case WeaponClass.BRUTALIZER:
          case WeaponClass.VAPORIZER:
          case WeaponClass.DESOLATOR:
          case WeaponClass.ATOMIZER:
          case WeaponClass.CORRUPTOR:
          case WeaponClass.MINDSLAYER:
          case WeaponClass.RIFTBREAKER:
          case WeaponClass.SOULTAKER:
          case WeaponClass.NULLCANNON:
          case WeaponClass.DEMOLISHER:
          case WeaponClass.INCINERATOR:
          case WeaponClass.ERADICATOR:
            return (int) this.Rarity * -200;
          default:
            return 0;
        }
      }
    }

    public override float MetalRepairMod
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.ION_CANNON:
            return (float) this.Rarity * 0.01f;
          case WeaponClass.RAPTURE:
          case WeaponClass.GLORY:
          case WeaponClass.OBLIVION:
          case WeaponClass.HORROR:
          case WeaponClass.RUIN:
          case WeaponClass.CATACLYSM:
          case WeaponClass.TORMENT:
          case WeaponClass.SMOLDER:
          case WeaponClass.DESTRUCTION:
          case WeaponClass.FRENZY:
          case WeaponClass.SILENCE:
          case WeaponClass.DARKNESS:
            return (float) this.Rarity * 0.03f;
          case WeaponClass.EXODUS:
          case WeaponClass.AGONY:
          case WeaponClass.PROPHECY:
          case WeaponClass.RADIANCE:
          case WeaponClass.ANIMUS:
          case WeaponClass.PAIN:
            return (float) this.Rarity * 0.035f;
          default:
            return 0.0f;
        }
      }
    }

    public override float OutgoingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.AUTO_CANNON:
          case WeaponClass.MASS_DRIVER:
          case WeaponClass.LEVIATHAN:
          case WeaponClass.PULVERIZER:
          case WeaponClass.RIPPER:
          case WeaponClass.SCREAMER:
          case WeaponClass.BURST_CANNON:
          case WeaponClass.MESON_BLASTER:
          case WeaponClass.OMEGA_RIFLE:
          case WeaponClass.ACCELERATOR:
          case WeaponClass.RAIL_GUN:
          case WeaponClass.DISRUPTOR:
          case WeaponClass.GRAVITY_SMASHER:
          case WeaponClass.ION_CANNON:
          case WeaponClass.PLASMA_LANCE:
          case WeaponClass.MATTER_INVERTER:
          case WeaponClass.SUCCUBUS:
            float num = (float) (this.Rarity - 3) * 0.02f;
            return (double) num > 0.0 ? num : 0.0f;
          case WeaponClass.HELLCANNON:
          case WeaponClass.HYDRA:
          case WeaponClass.FIRECAT:
          case WeaponClass.OPHIDIAN:
          case WeaponClass.BEHEMOTH:
          case WeaponClass.GARGOYLE:
          case WeaponClass.KRAKEN:
          case WeaponClass.DRAGON:
          case WeaponClass.EXTERMINATOR:
          case WeaponClass.BRUTALIZER:
            return (float) this.Rarity * 0.01f;
          case WeaponClass.PHASER:
            return (float) this.Rarity * 0.015f;
          case WeaponClass.MUTILATOR:
          case WeaponClass.VOIDBLASTER:
          case WeaponClass.RAVAGER:
          case WeaponClass.VAPORIZER:
          case WeaponClass.ATOMIZER:
            return (float) this.Rarity * 0.005f;
          case WeaponClass.STARSHATTER:
          case WeaponClass.STRIKER:
          case WeaponClass.DESOLATOR:
            return (float) this.Rarity * (3f / 1000f);
          case WeaponClass.CORRUPTOR:
          case WeaponClass.MINDSLAYER:
          case WeaponClass.RIFTBREAKER:
          case WeaponClass.SOULTAKER:
          case WeaponClass.NULLCANNON:
          case WeaponClass.DEMOLISHER:
          case WeaponClass.INCINERATOR:
          case WeaponClass.ERADICATOR:
            return (float) this.Rarity * 0.012f;
          default:
            return 0.0f;
        }
      }
    }

    public override float XpMod
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.OBLIVION:
          case WeaponClass.HORROR:
          case WeaponClass.TORMENT:
          case WeaponClass.EXODUS:
          case WeaponClass.PROPHECY:
          case WeaponClass.RADIANCE:
            return (float) this.Rarity * 0.01f;
          case WeaponClass.SMOLDER:
          case WeaponClass.SILENCE:
            return (float) this.Rarity * 0.015f;
          default:
            return 0.0f;
        }
      }
    }

    public override int MaxHullBonus
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.MESON_BLASTER:
            return (int) this.Rarity * 50;
          case WeaponClass.RAPTURE:
          case WeaponClass.GLORY:
          case WeaponClass.RUIN:
          case WeaponClass.DESTRUCTION:
            return (int) (this.Rarity - 2) * 75;
          case WeaponClass.CATACLYSM:
          case WeaponClass.FRENZY:
          case WeaponClass.DARKNESS:
          case WeaponClass.AGONY:
          case WeaponClass.ANIMUS:
          case WeaponClass.PAIN:
            return (int) (this.Rarity - 2) * 100;
          default:
            return 0;
        }
      }
    }

    public override float CriticalDamageMod
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.MATTER_INVERTER:
          case WeaponClass.STARSHATTER:
          case WeaponClass.EXTERMINATOR:
          case WeaponClass.RAVAGER:
          case WeaponClass.VAPORIZER:
          case WeaponClass.ATOMIZER:
          case WeaponClass.BURST_CANNON:
            return (float) this.Rarity * 0.02f;
          case WeaponClass.SUCCUBUS:
          case WeaponClass.HARPY:
          case WeaponClass.SERPENT:
          case WeaponClass.HYDRA:
          case WeaponClass.OPHIDIAN:
          case WeaponClass.GARGOYLE:
          case WeaponClass.KRAKEN:
          case WeaponClass.CORRUPTOR:
          case WeaponClass.INCINERATOR:
          case WeaponClass.HELLCANNON:
            return (float) this.Rarity * 0.01f;
          case WeaponClass.BANSHEE:
          case WeaponClass.WYVERN:
          case WeaponClass.PENATRATOR:
          case WeaponClass.FIRECAT:
          case WeaponClass.BEHEMOTH:
          case WeaponClass.DRAGON:
          case WeaponClass.MINDSLAYER:
          case WeaponClass.DEMOLISHER:
          case WeaponClass.ERADICATOR:
            return (float) this.Rarity * 0.03f;
          case WeaponClass.SOULTAKER:
            return (float) this.Rarity * 0.025f;
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalChance
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.AUTO_CANNON:
          case WeaponClass.FUSION_BEAM:
          case WeaponClass.PHASER:
          case WeaponClass.MESON_BLASTER:
          case WeaponClass.RAIL_GUN:
          case WeaponClass.DISRUPTOR:
          case WeaponClass.GRAVITY_SMASHER:
          case WeaponClass.MATTER_INVERTER:
          case WeaponClass.MUTILATOR:
          case WeaponClass.STRIKER:
          case WeaponClass.VOIDBLASTER:
          case WeaponClass.BRUTALIZER:
          case WeaponClass.DESOLATOR:
          case WeaponClass.CORRUPTOR:
          case WeaponClass.INCINERATOR:
            return (float) this.Rarity * 2f;
          case WeaponClass.MASS_DRIVER:
          case WeaponClass.LEVIATHAN:
          case WeaponClass.RIPPER:
          case WeaponClass.SCREAMER:
          case WeaponClass.HELLCANNON:
          case WeaponClass.GAUSS_CANNON:
          case WeaponClass.SUCCUBUS:
          case WeaponClass.BANSHEE:
          case WeaponClass.BASILISK:
          case WeaponClass.HARPY:
          case WeaponClass.WYVERN:
          case WeaponClass.VIPER:
          case WeaponClass.PENATRATOR:
          case WeaponClass.SERPENT:
          case WeaponClass.HYDRA:
          case WeaponClass.FIRECAT:
          case WeaponClass.OPHIDIAN:
          case WeaponClass.BEHEMOTH:
          case WeaponClass.GARGOYLE:
          case WeaponClass.KRAKEN:
          case WeaponClass.DRAGON:
            return (float) this.Rarity;
          case WeaponClass.RIFTBREAKER:
            return (float) this.Rarity * 3f;
          case WeaponClass.SOULTAKER:
            return (float) this.Rarity * 0.5f;
          case WeaponClass.NULLCANNON:
            return (float) this.Rarity * 2.5f;
          default:
            return 0.0f;
        }
      }
    }

    public override float EvasionChance
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.RAIL_GUN:
          case WeaponClass.GRAVITY_SMASHER:
          case WeaponClass.PLASMA_LANCE:
          case WeaponClass.SMOLDER:
          case WeaponClass.FRENZY:
          case WeaponClass.AGONY:
          case WeaponClass.RADIANCE:
          case WeaponClass.FUSION_BEAM:
          case WeaponClass.GAUSS_CANNON:
          case WeaponClass.OMEGA_RIFLE:
            return (float) this.Rarity * 2f;
          case WeaponClass.GLORY:
          case WeaponClass.HORROR:
          case WeaponClass.PAIN:
          case WeaponClass.PROTON_LAUNCHER:
            return (float) this.Rarity * 1.5f;
          case WeaponClass.CATACLYSM:
          case WeaponClass.EXODUS:
            return (float) this.Rarity * 3f;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashChance
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.LEVIATHAN:
            return (float) this.Rarity;
          case WeaponClass.PULVERIZER:
          case WeaponClass.WYVERN:
          case WeaponClass.SERPENT:
          case WeaponClass.FIRECAT:
            return (float) this.Rarity * 2f;
          case WeaponClass.RIPPER:
            return (float) this.Rarity * 5f;
          case WeaponClass.SCREAMER:
          case WeaponClass.HELLCANNON:
          case WeaponClass.SUCCUBUS:
          case WeaponClass.BANSHEE:
          case WeaponClass.BASILISK:
          case WeaponClass.HARPY:
          case WeaponClass.PENATRATOR:
          case WeaponClass.HYDRA:
          case WeaponClass.OPHIDIAN:
          case WeaponClass.BEHEMOTH:
          case WeaponClass.GARGOYLE:
          case WeaponClass.KRAKEN:
          case WeaponClass.DRAGON:
            return (float) this.Rarity * 4f;
          case WeaponClass.VIPER:
            return (float) this.Rarity * 6f;
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleChance
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.PENATRATOR:
          case WeaponClass.ATOMIZER:
          case WeaponClass.BANSHEE:
          case WeaponClass.HARPY:
            return (float) this.Rarity;
          case WeaponClass.HYDRA:
          case WeaponClass.BEHEMOTH:
          case WeaponClass.KRAKEN:
            return (float) this.Rarity * 3f;
          case WeaponClass.GRAVITY_SMASHER:
            return (float) this.Rarity * 4f;
          default:
            return 0.0f;
        }
      }
    }

    public override float StunChance
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.SERPENT:
          case WeaponClass.GARGOYLE:
          case WeaponClass.DRAGON:
          case WeaponClass.MUTILATOR:
          case WeaponClass.EXTERMINATOR:
          case WeaponClass.VOIDBLASTER:
          case WeaponClass.VAPORIZER:
          case WeaponClass.DESOLATOR:
          case WeaponClass.MINDSLAYER:
          case WeaponClass.RIFTBREAKER:
          case WeaponClass.DEMOLISHER:
          case WeaponClass.INCINERATOR:
          case WeaponClass.BASILISK:
          case WeaponClass.WYVERN:
            return (float) this.Rarity;
          case WeaponClass.FIRECAT:
            return (float) this.Rarity * 2f;
          case WeaponClass.ION_CANNON:
            return (float) this.Rarity * 3f;
          default:
            return 0.0f;
        }
      }
    }

    public override int StunDurationMSAdjust
    {
      get
      {
        if (this._class != WeaponClass.OMEGA_RIFLE)
          return 0;
        return (int) this.Rarity * 1000;
      }
    }

    public override float HitChance
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.AUTO_CANNON:
          case WeaponClass.PROTON_LAUNCHER:
          case WeaponClass.FUSION_BEAM:
          case WeaponClass.PHASER:
          case WeaponClass.GAUSS_CANNON:
          case WeaponClass.VIPER:
          case WeaponClass.STARSHATTER:
          case WeaponClass.STRIKER:
          case WeaponClass.VOIDBLASTER:
          case WeaponClass.RAVAGER:
          case WeaponClass.VAPORIZER:
          case WeaponClass.DESOLATOR:
            return (float) this.Rarity * 2f;
          case WeaponClass.MASS_DRIVER:
          case WeaponClass.MESON_BLASTER:
          case WeaponClass.BASILISK:
          case WeaponClass.HARPY:
          case WeaponClass.SERPENT:
            return (float) this.Rarity * 1.5f;
          case WeaponClass.LEVIATHAN:
          case WeaponClass.OMEGA_RIFLE:
            return (float) this.Rarity * 2.5f;
          case WeaponClass.PULVERIZER:
          case WeaponClass.BURST_CANNON:
          case WeaponClass.RAIL_GUN:
          case WeaponClass.DISRUPTOR:
          case WeaponClass.GRAVITY_SMASHER:
          case WeaponClass.ION_CANNON:
          case WeaponClass.MATTER_INVERTER:
          case WeaponClass.OPHIDIAN:
          case WeaponClass.GARGOYLE:
            return (float) this.Rarity * 3.5f;
          case WeaponClass.RIPPER:
          case WeaponClass.SCREAMER:
          case WeaponClass.BRUTALIZER:
            return (float) this.Rarity;
          case WeaponClass.HELLCANNON:
          case WeaponClass.HYDRA:
          case WeaponClass.FIRECAT:
          case WeaponClass.BEHEMOTH:
          case WeaponClass.KRAKEN:
          case WeaponClass.DRAGON:
          case WeaponClass.MINDSLAYER:
            return (float) this.Rarity * 3f;
          case WeaponClass.ACCELERATOR:
          case WeaponClass.PLASMA_LANCE:
            return (float) this.Rarity * 5f;
          case WeaponClass.CORRUPTOR:
          case WeaponClass.RIFTBREAKER:
          case WeaponClass.SOULTAKER:
          case WeaponClass.NULLCANNON:
          case WeaponClass.DEMOLISHER:
          case WeaponClass.INCINERATOR:
          case WeaponClass.ERADICATOR:
            return (float) this.Rarity * 4f;
          default:
            return 0.0f;
        }
      }
    }

    public override float StunResist
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.OMEGA_RIFLE:
          case WeaponClass.ACCELERATOR:
          case WeaponClass.GLORY:
          case WeaponClass.RUIN:
          case WeaponClass.SILENCE:
          case WeaponClass.DARKNESS:
          case WeaponClass.RADIANCE:
          case WeaponClass.GARGOYLE:
            return (float) this.Rarity * 2f;
          case WeaponClass.ION_CANNON:
          case WeaponClass.SMOLDER:
          case WeaponClass.AGONY:
          case WeaponClass.ANIMUS:
          case WeaponClass.FIRECAT:
          case WeaponClass.OPHIDIAN:
            return (float) this.Rarity * 3f;
          case WeaponClass.RAPTURE:
          case WeaponClass.HORROR:
          case WeaponClass.TORMENT:
          case WeaponClass.FRENZY:
            return (float) this.Rarity;
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleResist
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.HELLCANNON:
          case WeaponClass.CATACLYSM:
          case WeaponClass.DESTRUCTION:
          case WeaponClass.PROPHECY:
            return (float) this.Rarity * 3f;
          case WeaponClass.RAPTURE:
          case WeaponClass.HORROR:
          case WeaponClass.TORMENT:
          case WeaponClass.FRENZY:
          case WeaponClass.DARKNESS:
            return (float) this.Rarity;
          case WeaponClass.OBLIVION:
          case WeaponClass.EXODUS:
          case WeaponClass.RADIANCE:
            return (float) this.Rarity * 2f;
          case WeaponClass.PAIN:
            return (float) this.Rarity * 4f;
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalResist
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.PHASER:
            return (float) this.Rarity * 4f;
          case WeaponClass.GAUSS_CANNON:
          case WeaponClass.FRENZY:
          case WeaponClass.RADIANCE:
            return (float) this.Rarity * 2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashResist
    {
      get
      {
        switch (this._class)
        {
          case WeaponClass.PROTON_LAUNCHER:
          case WeaponClass.DISRUPTOR:
            return (float) this.Rarity * 2f;
          default:
            return 0.0f;
        }
      }
    }

    public WeaponClass Class
    {
      get
      {
        return this._class;
      }
    }

    public sbyte BaseEP
    {
      get
      {
        return this._baseEp;
      }
    }

    protected internal WeaponItem(WeaponClass weaponClass, sbyte baseEp, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(rarity, durability, noDrop, bindOnEquip)
    {
      this._class = weaponClass;
      this._baseEp = baseEp;
    }

    public override void Write(IByteBuffer output)
    {
      base.Write(output);
      output.Write((sbyte) this._class);
      output.Write(this._baseEp);
    }

    public override void AppendName(StringBuilder sb)
    {
      sb.Append(this._class.Name());
      sb.Append(" ");
      sb.Append(Util.NUMERALS[(int) this._baseEp]);
    }

    public override EquipmentItem GetUpgrade(EngineeringType type)
    {
      return (EquipmentItem) WeaponCache.GetUpgrade(type, this);
    }

    public override bool CanUpgrade(EngineeringType type)
    {
      if (this.Rarity <= ItemRarity.COMMON)
        return false;
      switch (type)
      {
        case EngineeringType.DECONSTRUCT:
          return true;
        case EngineeringType.REPAIR:
          if ((int) this.Durability > 1)
            return (int) this.Durability < 100;
          return false;
        case EngineeringType.RANK_UP:
          return (int) this._baseEp < 30;
        case EngineeringType.RANK_DOWN:
          return this.EPCost > 1;
        case EngineeringType.RARITY:
          return this.Rarity < ItemRarity.ULTIMATE;
        case EngineeringType.UNBIND:
          return this.NoDrop;
        default:
          return false;
      }
    }

    public override void AppendSubTitle(StringBuilder sb)
    {
      sb.Append(this.Rarity.Name());
      sb.Append(" Weapon");
    }

    public override void AppendDescription(StringBuilder sb)
    {
      int attackSpeedMs = this.AttackSpeedMS;
      int playerMinDamage = CombatHelper.getPlayerMinDamage((int) this._baseEp, attackSpeedMs);
      sb.Append(playerMinDamage);
      sb.Append(" to ");
      sb.Append(playerMinDamage * 2);
      sb.Append(" Damage / ");
      sb.Append(attackSpeedMs / 1000);
      sb.Append(" Seconds");
      if (this._class.Faction() != NpcFaction.HETEROCLITE)
        return;
      sb.Append("\nRepairs ");
      sb.Append((int) this.Rarity);
      sb.Append("% of Max Hull / 30 Seconds");
    }
  }
}
