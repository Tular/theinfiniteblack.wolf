﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.WeaponCache
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Library.Items
{
  public static class WeaponCache
  {
    private static readonly Dictionary<WeaponClass, Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, WeaponItem[]>>>> _cache = new Dictionary<WeaponClass, Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, WeaponItem[]>>>>();

    public static WeaponItem Get(IByteBuffer input)
    {
      ItemRarity rarity = (ItemRarity) input.Get();
      sbyte durability = input.Get();
      bool noDrop = input.GetBool();
      bool bindOnEquip = !noDrop && input.GetBool();
      return WeaponCache.Get((WeaponClass) input.Get(), input.Get(), rarity, durability, noDrop, bindOnEquip);
    }

    public static WeaponItem Get(WeaponClass type, sbyte ep, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      lock (WeaponCache._cache)
      {
        Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, WeaponItem[]>>> dictionary1;
        if (!WeaponCache._cache.TryGetValue(type, out dictionary1))
        {
          dictionary1 = new Dictionary<sbyte, Dictionary<ItemRarity, Dictionary<sbyte, WeaponItem[]>>>();
          WeaponCache._cache[type] = dictionary1;
        }
        Dictionary<ItemRarity, Dictionary<sbyte, WeaponItem[]>> dictionary2;
        if (!dictionary1.TryGetValue(ep, out dictionary2))
        {
          dictionary2 = new Dictionary<ItemRarity, Dictionary<sbyte, WeaponItem[]>>();
          dictionary1[ep] = dictionary2;
        }
        Dictionary<sbyte, WeaponItem[]> dictionary3;
        if (!dictionary2.TryGetValue(rarity, out dictionary3))
        {
          dictionary3 = new Dictionary<sbyte, WeaponItem[]>();
          dictionary2[rarity] = dictionary3;
        }
        WeaponItem[] weaponItemArray;
        if (!dictionary3.TryGetValue(durability, out weaponItemArray))
        {
          weaponItemArray = new WeaponItem[4];
          dictionary3[durability] = weaponItemArray;
        }
        int index = noDrop ? (bindOnEquip ? 0 : 1) : (bindOnEquip ? 2 : 3);
        WeaponItem weaponItem = weaponItemArray[index];
        if (weaponItem == null)
        {
          weaponItem = new WeaponItem(type, ep, rarity, durability, noDrop, bindOnEquip);
          weaponItemArray[index] = weaponItem;
        }
        return weaponItem;
      }
    }

    public static WeaponItem GetUpgrade(EngineeringType type, WeaponItem item)
    {
      if (!item.CanUpgrade(type))
        return (WeaponItem) null;
      switch (type)
      {
        case EngineeringType.REPAIR:
          return WeaponCache.Get(item.Class, item.BaseEP, item.Rarity, (int) item.Durability + 10 >= 100 ? (sbyte) 100 : (sbyte) ((int) item.Durability + 10), item.NoDrop, item.BindOnEquip);
        case EngineeringType.RANK_UP:
          return WeaponCache.Get(item.Class, (sbyte) ((int) item.BaseEP + 1), item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RANK_DOWN:
          return WeaponCache.Get(item.Class, (sbyte) ((int) item.BaseEP - 1), item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RARITY:
          return WeaponCache.Get(item.Class, item.BaseEP, item.Rarity + 1, item.Durability, item.NoDrop, item.BindOnEquip);
        default:
          return (WeaponItem) null;
      }
    }
  }
}
