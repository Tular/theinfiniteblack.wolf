﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.ConsoleLogger
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;

namespace TheInfiniteBlack.Library
{
  public class ConsoleLogger : Logger
  {
    protected override void OnDebug(object sender, string callerName, string log, string line)
    {
      Console.WriteLine(line);
    }

    protected override void OnInformation(object sender, string callerName, string log, string line)
    {
      Console.WriteLine(line);
    }

    protected override void OnWarning(object sender, string callerName, string log, string line)
    {
      Console.WriteLine(line);
    }

    protected override void OnException(object sender, string callerName, Exception e, string line)
    {
      Console.WriteLine(line);
    }
  }
}
