﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.CorpRankType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum CorpRankType
  {
    NULL = -128,
    RECRUIT = 1,
    MEMBER = 2,
    OFFICER = 3,
    SUPEROFFICER = 4,
    LEADER = 5,
  }
}
