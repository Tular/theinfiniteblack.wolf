﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.GameSettings
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace TheInfiniteBlack.Library
{
  public static class GameSettings
  {
    public static readonly sbyte CompliantVersion = 86;
    public static sbyte ServerVersion = 86;
    public static readonly string[] ModeratorList = new string[5]
    {
      "cyric",
      "ozymandias",
      "mythra",
      "amcilla",
      "ascena"
    };
    public const int CorpNameMIN = 3;
    public const int CorpNameMAX = 18;
    public const int BlackDollarRUValue = 20;
    public const int EarthEntityID = 0;

    public static string GetClientVersion(DistributionType type, string baseVersion)
    {
      switch (type)
      {
        case DistributionType.Android_Open:
          return baseVersion + "-AO(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.Android_GooglePlay:
          return baseVersion + "-AGP(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.iOS:
          return baseVersion + "-IOS(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.MacOSX:
          return baseVersion + "-OSX(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.Linux:
          return baseVersion + "-L(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.BlackBerry:
          return baseVersion + "-BB(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.Windows:
          return baseVersion + "-W(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.WindowsWinForm:
          return baseVersion + "-WF(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.WindowsMobile:
          return baseVersion + "-WM(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.Browser:
          return baseVersion + "-Br(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.Steam_Windows:
          return baseVersion + "-SW(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.Steam_Linux:
          return baseVersion + "-SL(" + (object) GameSettings.CompliantVersion + ")";
        case DistributionType.Steam_OSX:
          return baseVersion + "-SOSX(" + (object) GameSettings.CompliantVersion + ")";
        default:
          return baseVersion + "-?(" + (object) GameSettings.CompliantVersion + ")";
      }
    }

    public static bool IsModerator(string name)
    {
      name = name.ToLower().Trim();
      return ((IEnumerable<string>) GameSettings.ModeratorList).Any<string>((Func<string, bool>) (t => t.Equals(name)));
    }
  }
}
