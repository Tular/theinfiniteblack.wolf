﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: ComVisible(false)]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyTrademark("")]
[assembly: Extension]
[assembly: AssemblyProduct("TheInfiniteBlack.Library")]
[assembly: Guid("e4eded2b-9033-497e-8024-db4556e4ff51")]
[assembly: AssemblyCopyright("Copyright ©  2013")]
[assembly: AssemblyTitle("TheInfiniteBlack.Library")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
