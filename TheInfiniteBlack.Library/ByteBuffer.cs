﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.ByteBuffer
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;

namespace TheInfiniteBlack.Library
{
  public class ByteBuffer : IByteBuffer
  {
    protected sbyte[] _data;
    protected int _index;

    public sbyte[] Data
    {
      get
      {
        return this._data;
      }
    }

    public int Position
    {
      get
      {
        return this._index;
      }
      set
      {
        this._index = value;
      }
    }

    public ByteBuffer()
    {
    }

    public ByteBuffer(sbyte[] data)
    {
      this._data = data;
    }

    public void Get(sbyte[] data)
    {
      Buffer.BlockCopy((Array) this._data, this._index, (Array) data, 0, data.Length);
      this._index = this._index + data.Length;
    }

    public sbyte Get()
    {
      sbyte[] data = this._data;
      int index1 = this._index;
      this._index = index1 + 1;
      int index2 = index1;
      return data[index2];
    }

    public short GetShort()
    {
      int num = (int) (byte) this._data[this._index + 1] | (int) (byte) this._data[this._index] << 8;
      this._index = this._index + 2;
      return (short) num;
    }

    public int GetInt()
    {
      int num = (int) (byte) this._data[this._index + 3] | (int) (byte) this._data[this._index + 2] << 8 | (int) (byte) this._data[this._index + 1] << 16 | (int) (byte) this._data[this._index] << 24;
      this._index = this._index + 4;
      return num;
    }

    public unsafe float GetFloat()
    {
      uint num = (uint) ((int) (byte) this._data[this._index + 3] | (int) (byte) this._data[this._index + 2] << 8 | (int) (byte) this._data[this._index + 1] << 16 | (int) (byte) this._data[this._index] << 24);
      this._index = this._index + 4;
      return *(float*) &num;
    }

    public bool GetBool()
    {
      sbyte[] data = this._data;
      int index1 = this._index;
      this._index = index1 + 1;
      int index2 = index1;
      return (int) data[index2] > (int) sbyte.MinValue;
    }

    public string GetString()
    {
      int length = (int) this._data[this._index];
      byte[] bytes = new byte[length];
      Buffer.BlockCopy((Array) this._data, this._index + 1, (Array) bytes, 0, bytes.Length);
      this._index = this._index + (length + 1);
      return Util.ASCII.GetString(bytes);
    }

    public void Write(sbyte value)
    {
      this._data[this._index] = value;
      this._index = this._index + 1;
    }

    public void Write(short value)
    {
      this._data[this._index + 1] = (sbyte) value;
      this._data[this._index] = (sbyte) ((int) value >> 8);
      this._index = this._index + 2;
    }

    public void Write(int value)
    {
      this._data[this._index + 3] = (sbyte) value;
      this._data[this._index + 2] = (sbyte) (value >> 8);
      this._data[this._index + 1] = (sbyte) (value >> 16);
      this._data[this._index] = (sbyte) (value >> 24);
      this._index = this._index + 4;
    }

    public unsafe void Write(float value)
    {
      uint num = *(uint*) &value;
      this._data[this._index + 3] = (sbyte) num;
      this._data[this._index + 2] = (sbyte) (num >> 8);
      this._data[this._index + 1] = (sbyte) (num >> 16);
      this._data[this._index] = (sbyte) (num >> 24);
      this._index = this._index + 4;
    }

    public void Write(bool value)
    {
      this._data[this._index] = value ? sbyte.MaxValue : sbyte.MinValue;
      this._index = this._index + 1;
    }

    public void Write(string value)
    {
      byte[] bytes = Util.ASCII.GetBytes(value);
      if (bytes.Length >= (int) sbyte.MaxValue)
        throw new NotSupportedException("length not supported");
      this._data[this._index] = (sbyte) bytes.Length;
      Buffer.BlockCopy((Array) bytes, 0, (Array) this._data, this._index + 1, bytes.Length);
      this._index = this._index + (bytes.Length + 1);
    }

    public void Write(sbyte[] data)
    {
      Buffer.BlockCopy((Array) data, 0, (Array) this._data, this._index, data.Length);
      this._index = this._index + data.Length;
    }

    public void Write(byte[] data)
    {
      Buffer.BlockCopy((Array) data, 0, (Array) this._data, this._index, data.Length);
      this._index = this._index + data.Length;
    }
  }
}
