﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.PlayerCollection
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client
{
  public class PlayerCollection
  {
    private Dictionary<int, ClientPlayer> _values = new Dictionary<int, ClientPlayer>();
    private readonly IGameState _state;

    public IEnumerable<ClientPlayer> Values
    {
      get
      {
        return (IEnumerable<ClientPlayer>) this._values.Values;
      }
    }

    public PlayerCollection(IGameState state)
    {
      this._state = state;
    }

    public IEnumerable<ClientPlayer> GetOnline()
    {
      return this._values.Values.Where<ClientPlayer>((Func<ClientPlayer, bool>) (p => p.Online));
    }

    public ClientPlayer Get(int id)
    {
      if (id == int.MinValue)
        return (ClientPlayer) null;
      ClientPlayer clientPlayer1;
      if (this._values.TryGetValue(id, out clientPlayer1))
        return clientPlayer1;
      ClientPlayer clientPlayer2 = new ClientPlayer(this._state, id);
      this._values = new Dictionary<int, ClientPlayer>((IDictionary<int, ClientPlayer>) this._values)
      {
        {
          id,
          clientPlayer2
        }
      };
      this._state.Net.Send(RequestPlayer.Create(id));
      return clientPlayer2;
    }

    public ClientPlayer Get(string name)
    {
      return this._values.Values.FirstOrDefault<ClientPlayer>((Func<ClientPlayer, bool>) (p => p.Name.Equals(name, StringComparison.OrdinalIgnoreCase)));
    }
  }
}
