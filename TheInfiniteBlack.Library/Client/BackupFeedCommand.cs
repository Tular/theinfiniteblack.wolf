﻿namespace TheInfiniteBlack.Library.Client
{
  internal class BackupFeedCommand : ITextCommands
  {
    public string Trigger()
    {
      return ":backupfeed";
    }

    public void Process(GameState gameState, string command)
    {
      if (command.Trim() == ":backupfeed")
      {
        gameState.BackupFeedTarget = "";
        gameState.BackupFeed = !gameState.BackupFeed;
      }
      else
      {
        gameState.BackupFeedTarget = command.Replace(":backupfeed", "").Trim();
        gameState.BackupFeed = true;
      }
    }
  }
}