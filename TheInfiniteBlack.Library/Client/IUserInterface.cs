﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.IUserInterface
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Library.Client
{
  public interface IUserInterface
  {
    void Show(DisconnectEventArgs e);

    void Show(LoginSuccessEventArgs e);

    void Show(AcceptDeclineWindowEventArgs e);

    void Show(PopupWindowEventArgs e);

    void Show(EntityArriveEventArgs e);

    void Show(EntityExitEventArgs e);

    void Show(EntityHullChangedEventArgs e);

    void Show(AttackEventArgs e);

    void Show(ChatEventArgs e);

    void Show(LocationChangedEventArgs e);

    void Show(PvPFlagChangeEventArgs e);

    void Show(GainXpEventArgs e);

    void Show(GainItemEventArgs e);

    void Show(MoneyChangeEventArgs e);

    void Show(InventoryChangedEventArgs e);

    void Show(ResourcesChangedEventArgs e);

    void Show(PlayerOnlineStatusChangedEventArgs e);

    void Show(HarvestEventArgs e);

    void Show(BankChangedEventArgs e);

    void Show(FollowTargetChangedEventArgs e);

    void Show(AttackTargetChangedEventArgs e);

    void Show(TradeEventArgs e);

    void Show(TradeFailedEventArgs e);

    void Show(CourseSetEventArgs e);

    void Show(MovementStatusChangedEventArgs e);

    void Show(AuctionUpdatedEventArgs e);

    void Show(AuctionRemovedEventArgs e);

    void Show(ShowEntityDetailEventArgs e);

    void Show(CombatFlashEventArgs e);

    void Show(SoundEventArgs e);

    void Show(ShowUrlEventArgs e);

    void Show(AchievementEventArgs e);
  }
}
