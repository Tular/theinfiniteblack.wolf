﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.LoginRequest
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Security.Cryptography;
using TheInfiniteBlack.Library.Network;

namespace TheInfiniteBlack.Library.Client
{
  public class LoginRequest
  {
    public string ServerIP { get; set; }

    public int ServerPort { get; set; }

    public DistributionType Distribution { get; set; }

    public string VersionIdentity { get; set; }

    public sbyte ServerID { get; set; }

    public string Name { get; set; }

    public string Password { get; set; }

    public string DeviceID { get; set; }

    public string DeviceOS { get; set; }

    public string DeviceModel { get; set; }

    public string DeviceName { get; set; }

    public bool IsNewAccount { get; set; }

    public bool IsHardcore { get; set; }

    public byte FaceByte { get; set; }

    public byte AttributeByte { get; set; }

    public byte HairByte { get; set; }

    public bool Success { get; protected internal set; }

    protected internal string GetDeviceType()
    {
      string str1 = Util.Regex(this.DeviceOS, true, true, true).Trim();
      while (str1.Contains("  "))
        str1 = str1.Replace("  ", " ");
      if (str1.Length > 35)
        str1 = str1.Substring(0, 35);
      string str2 = Util.Regex(this.DeviceModel, true, true, true).Trim();
      while (str2.Contains("  "))
        str2 = str2.Replace("  ", " ");
      if (str2.Length > 62)
        str2 = str2.Substring(0, 62);
      string str3 = Util.Regex(this.DeviceName, true, true, true).Trim();
      while (str3.Contains("  "))
        str3 = str3.Replace("  ", " ");
      if (str3.Length > 21)
        str3 = str3.Substring(0, 21);
      return (str1 + " " + str2 + " " + str3).ToLower();
    }

    protected internal string GetDeviceID()
    {
      string lower = Util.Regex(this.DeviceID, true, true, false).ToLower();
      if (lower.Length > 120)
        return lower.Substring(0, 120);
      return lower;
    }

    protected internal INetworkData Serialize(byte[] iv, sbyte[] skey, string deviceId, string deviceType)
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) -67, 1024);
      byte[] bytes1 = Util.ASCII.GetBytes(this.Name);
      byte[] bytes2 = Util.ASCII.GetBytes(this.Password);
      byte[] bytes3 = Util.ASCII.GetBytes(deviceId);
      byte[] bytes4 = Util.ASCII.GetBytes(deviceType);
      byte[] bytes5 = Util.ASCII.GetBytes(GameSettings.GetClientVersion(this.Distribution, this.VersionIdentity));
      byte[] numArray1 = new byte[16];
      Buffer.BlockCopy((Array) skey, 0, (Array) numArray1, 0, 16);
      RijndaelManaged rijndaelManaged = new RijndaelManaged();
      int num1 = 1;
      rijndaelManaged.Mode = (CipherMode) num1;
      int num2 = 2;
      rijndaelManaged.Padding = (PaddingMode) num2;
      byte[] numArray2 = numArray1;
      rijndaelManaged.Key = numArray2;
      byte[] numArray3 = iv;
      rijndaelManaged.IV = numArray3;
      byte[] data1 = rijndaelManaged.CreateEncryptor().TransformFinalBlock(bytes1, 0, bytes1.Length);
      byte[] data2 = rijndaelManaged.CreateEncryptor().TransformFinalBlock(bytes2, 0, bytes2.Length);
      byte[] data3 = rijndaelManaged.CreateEncryptor().TransformFinalBlock(bytes3, 0, bytes3.Length);
      byte[] data4 = rijndaelManaged.CreateEncryptor().TransformFinalBlock(bytes4, 0, bytes4.Length);
      byte[] data5 = rijndaelManaged.CreateEncryptor().TransformFinalBlock(bytes5, 0, bytes5.Length);
      networkData.Write((short) data1.Length);
      networkData.Write((short) data2.Length);
      networkData.Write((short) data3.Length);
      networkData.Write((short) data4.Length);
      networkData.Write((short) data5.Length);
      networkData.Write(data1);
      networkData.Write(data2);
      networkData.Write(data3);
      networkData.Write(data4);
      networkData.Write(data5);
      networkData.Write(this.IsNewAccount);
      networkData.Write(this.IsHardcore);
      networkData.Write(this.ServerID);
      if (this.IsNewAccount)
      {
        networkData.Write((short) this.FaceByte);
        networkData.Write((short) this.AttributeByte);
        networkData.Write((short) this.HairByte);
      }
      return networkData;
    }
  }
}
