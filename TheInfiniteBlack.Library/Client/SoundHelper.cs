﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.SoundHelper
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client
{
  public static class SoundHelper
  {
    public static EventSoundEffect GetDamagedSound(Entity entity)
    {
      if (entity.Type == EntityType.MINES || RNG.R.Next(5) != 0)
        return (EventSoundEffect) null;
      float delay = 0.3f + (float) (RNG.R.NextDouble() * 0.5);
      if (entity.Type != EntityType.NPC)
        return SoundHelper.GetSmallBoom(delay);
      return SoundHelper.GetDamagedSound((Npc) entity, delay);
    }

    private static EventSoundEffect GetDamagedSound(Npc npc, float delay)
    {
      switch (npc.Faction)
      {
        case NpcFaction.HETEROCLITE:
          float rate1 = SoundHelper.GetSoundRate(npc.Class) + (float) (RNG.R.NextDouble() * 0.04 - RNG.R.NextDouble() * -0.04);
          if (RNG.R.Next(2) != 1)
            return new EventSoundEffect(SoundEffectType.damaged_heteroclite_1, rate1, delay);
          return new EventSoundEffect(SoundEffectType.damaged_heteroclite_2, rate1, delay);
        case NpcFaction.WYRD:
          float rate2 = SoundHelper.GetSoundRate(npc.Class) + (float) (RNG.R.NextDouble() * 0.04 - RNG.R.NextDouble() * -0.04);
          switch (RNG.R.Next(4))
          {
            case 1:
              return new EventSoundEffect(SoundEffectType.damaged_wyrd_2, rate2, delay);
            case 2:
              return new EventSoundEffect(SoundEffectType.damaged_wyrd_3, rate2, delay);
            case 3:
              return new EventSoundEffect(SoundEffectType.damaged_wyrd_4, rate2, delay);
            default:
              return new EventSoundEffect(SoundEffectType.damaged_wyrd_1, rate2, delay);
          }
        default:
          return SoundHelper.GetSmallBoom(delay);
      }
    }

    public static EventSoundEffect GetSmallBoom(float delay)
    {
      float rate = 1f + (float) (RNG.R.NextDouble() * 0.08 - RNG.R.NextDouble() * -0.08);
      switch (RNG.R.Next(4))
      {
        case 1:
          return new EventSoundEffect(SoundEffectType.boom_1, rate, delay);
        case 2:
          return new EventSoundEffect(SoundEffectType.boom_2, rate, delay);
        case 3:
          return new EventSoundEffect(SoundEffectType.boom_7, rate, delay);
        default:
          return new EventSoundEffect(SoundEffectType.boom_0, rate, delay);
      }
    }

    public static EventSoundEffect GetBigBoom(float delay)
    {
      float rate = 1f + (float) (RNG.R.NextDouble() * 0.08 - RNG.R.NextDouble() * -0.08);
      switch (RNG.R.Next(4))
      {
        case 1:
          return new EventSoundEffect(SoundEffectType.boom_4, rate, delay);
        case 2:
          return new EventSoundEffect(SoundEffectType.boom_5, rate, delay);
        case 3:
          return new EventSoundEffect(SoundEffectType.boom_6, rate, delay);
        default:
          return new EventSoundEffect(SoundEffectType.boom_3, rate, delay);
      }
    }

    public static EventSoundEffect GetHarvestSound(Entity entity)
    {
      switch (entity.Type)
      {
        case EntityType.SHIP:
          return new EventSoundEffect(SoundEffectType.harvest, SoundHelper.GetSoundRate(((Ship) entity).Class));
        case EntityType.NPC:
          return new EventSoundEffect(SoundEffectType.harvest, SoundHelper.GetSoundRate(((Npc) entity).Class));
        default:
          return (EventSoundEffect) null;
      }
    }

    public static EventSoundEffect GetRepairSound(Entity entity)
    {
      switch (entity.Type)
      {
        case EntityType.SHIP:
          return new EventSoundEffect(SoundEffectType.repair, SoundHelper.GetSoundRate(((Ship) entity).Class));
        case EntityType.NPC:
          return new EventSoundEffect(SoundEffectType.repair, SoundHelper.GetSoundRate(((Npc) entity).Class));
        default:
          return (EventSoundEffect) null;
      }
    }

    public static EventSoundEffect GetAttackSound(Entity entity)
    {
      switch (entity.Type)
      {
        case EntityType.PLANET:
          return new EventSoundEffect(SoundEffectType.attack_planet);
        case EntityType.SHIP:
          return SoundHelper.GetAttack(((Ship) entity).Weapon);
        case EntityType.NPC:
          return SoundHelper.GetAttack(((Npc) entity).Class);
        case EntityType.FIGHTER:
          return new EventSoundEffect(SoundEffectType.weapon_laser_5, 1.3f - (float) (RNG.R.NextDouble() * 0.1));
        case EntityType.DEFENSE_PLATFORM:
          return new EventSoundEffect(SoundEffectType.attack_defense_platform);
        case EntityType.MINES:
          return new EventSoundEffect(SoundEffectType.boom_7);
        case EntityType.INTRADICTOR:
          return new EventSoundEffect(SoundEffectType.attack_intradictor);
        case EntityType.REPAIR_DRONE:
          return new EventSoundEffect(SoundEffectType.repair);
        case EntityType.GARRISON:
          return new EventSoundEffect(SoundEffectType.attack_garrison);
        default:
          return (EventSoundEffect) null;
      }
    }

    public static EventSoundEffect GetDeathSound(Entity entity)
    {
      switch (entity.Type)
      {
        case EntityType.PLANET:
          return new EventSoundEffect(SoundEffectType.boom_5, 0.5f);
        case EntityType.SHIP:
        case EntityType.DEFENSE_PLATFORM:
        case EntityType.INTRADICTOR:
        case EntityType.GARRISON:
          return SoundHelper.GetBigBoom(0.0f);
        case EntityType.NPC:
          return SoundHelper.GetDeath(((Npc) entity).Class);
        case EntityType.FIGHTER:
        case EntityType.REPAIR_DRONE:
          return SoundHelper.GetSmallBoom(0.0f);
        default:
          return (EventSoundEffect) null;
      }
    }

    public static EventSoundEffect GetMoveSound(Entity entity)
    {
      switch (entity.Type)
      {
        case EntityType.SHIP:
          return SoundHelper.GetMove((Ship) entity);
        case EntityType.NPC:
          return SoundHelper.GetMove((Npc) entity);
        default:
          return (EventSoundEffect) null;
      }
    }

    private static EventSoundEffect GetDeath(NpcClass npc)
    {
      float rate = SoundHelper.GetSoundRate(npc) + (float) (RNG.R.NextDouble() * 0.04 - RNG.R.NextDouble() * -0.04);
      switch (npc.Faction())
      {
        case NpcFaction.HETEROCLITE:
          return new EventSoundEffect(SoundEffectType.death_heteroclite_1, rate);
        case NpcFaction.WYRD:
          if (RNG.R.Next(2) != 1)
            return new EventSoundEffect(SoundEffectType.death_wyrd_1, rate);
          return new EventSoundEffect(SoundEffectType.death_wyrd_2, rate);
        default:
          return SoundHelper.GetBigBoom(0.0f);
      }
    }

    private static EventSoundEffect GetAttack(NpcClass npc)
    {
      float num = (float) (RNG.R.NextDouble() * 0.04 - RNG.R.NextDouble() * -0.04);
      switch (npc)
      {
        case NpcClass.Drone:
        case NpcClass.Warrior:
        case NpcClass.Defender:
        case NpcClass.Predator:
        case NpcClass.Queen:
        case NpcClass.King:
          switch (RNG.R.Next(3))
          {
            case 1:
              return new EventSoundEffect(SoundEffectType.attack_heteroclite_2, SoundHelper.GetSoundRate(npc) + num);
            case 2:
              return new EventSoundEffect(SoundEffectType.attack_heteroclite_3, SoundHelper.GetSoundRate(npc) + num);
            default:
              return new EventSoundEffect(SoundEffectType.attack_heteroclite_1, SoundHelper.GetSoundRate(npc) + num);
          }
        case NpcClass.Hive:
          return new EventSoundEffect(SoundEffectType.attack_hive);
        case NpcClass.Raider:
          return new EventSoundEffect(SoundEffectType.weapon_machine_1, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Bandit:
          return new EventSoundEffect(SoundEffectType.weapon_laser_4, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Outlaw:
          return new EventSoundEffect(SoundEffectType.weapon_machine_2, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Hijacker:
          return new EventSoundEffect(SoundEffectType.weapon_laser_2, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Brigand:
          return new EventSoundEffect(SoundEffectType.weapon_machine_3, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Prowler:
          return new EventSoundEffect(SoundEffectType.weapon_laser_3, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Marauder:
          return new EventSoundEffect(SoundEffectType.weapon_machine_4, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Corsair:
          return new EventSoundEffect(SoundEffectType.weapon_laser_7, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Enforcer:
          return new EventSoundEffect(SoundEffectType.weapon_machine_5, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Stronghold:
          return new EventSoundEffect(SoundEffectType.attack_stronghold);
        case NpcClass.Colonizer:
          return new EventSoundEffect(SoundEffectType.attack_wyrd_2, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Wayfarer:
          return new EventSoundEffect(SoundEffectType.attack_wyrd_3, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Invader:
          return new EventSoundEffect(SoundEffectType.attack_wyrd_1, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Assassin:
          return new EventSoundEffect(SoundEffectType.attack_wyrd_2, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.Reaper:
          return new EventSoundEffect(SoundEffectType.attack_wyrd_3, SoundHelper.GetSoundRate(npc) + num);
        case NpcClass.SeedWorld:
          return new EventSoundEffect(SoundEffectType.attack_seed_world);
        default:
          return (EventSoundEffect) null;
      }
    }

    private static EventSoundEffect GetMove(Npc npc)
    {
      return new EventSoundEffect(SoundHelper.GetMove(npc.Class), SoundHelper.GetSoundRate(npc.Class));
    }

    private static EventSoundEffect GetMove(Ship ship)
    {
      SoundEffectType move = SoundHelper.GetMove(ship.Engine);
      return new EventSoundEffect(move == SoundEffectType.NULL || ship.Class == ShipClass.WyrdAssassin ? SoundHelper.GetMoveSound(ship.Class) : move, SoundHelper.GetSoundRate(ship.Class));
    }

    private static SoundEffectType GetMove(EngineItem engine)
    {
      if (engine != null)
      {
        switch (engine.Class)
        {
          case EngineClass.GRAVITY:
            return SoundEffectType.engine_wyrd_1;
          case EngineClass.SKIP:
          case EngineClass.NEUTRON:
            return SoundEffectType.engine_skip;
          case EngineClass.FISSION:
          case EngineClass.FUSION:
            return SoundEffectType.engine_pirate_3;
          case EngineClass.IMPULSE:
            return SoundEffectType.engine_wyrd_2;
          case EngineClass.STEALTH:
            return SoundEffectType.engine_stealth;
        }
      }
      return SoundEffectType.NULL;
    }

    private static SoundEffectType GetMove(NpcClass npc)
    {
      switch (npc)
      {
        case NpcClass.Drone:
        case NpcClass.Warrior:
        case NpcClass.Defender:
        case NpcClass.Predator:
          return SoundEffectType.engine_heteroclite_1;
        case NpcClass.Queen:
        case NpcClass.King:
        case NpcClass.Hive:
          return SoundEffectType.engine_heteroclite_2;
        case NpcClass.Raider:
        case NpcClass.Bandit:
        case NpcClass.Brigand:
        case NpcClass.Marauder:
          return SoundEffectType.engine_pirate_3;
        case NpcClass.Outlaw:
        case NpcClass.Corsair:
        case NpcClass.Stronghold:
          return SoundEffectType.engine_pirate_1;
        case NpcClass.Hijacker:
        case NpcClass.Prowler:
        case NpcClass.Enforcer:
          return SoundEffectType.engine_skip;
        case NpcClass.Colonizer:
        case NpcClass.Invader:
          return SoundEffectType.engine_wyrd_1;
        case NpcClass.Wayfarer:
        case NpcClass.Reaper:
        case NpcClass.SeedWorld:
          return SoundEffectType.engine_wyrd_2;
        case NpcClass.Assassin:
          return SoundEffectType.engine_stealth;
        default:
          return SoundEffectType.engine_pirate_1;
      }
    }

    private static float GetSoundRate(ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Shuttle:
          return 1.25f;
        case ShipClass.Corvette:
          return 1.21f;
        case ShipClass.Frigate:
          return 1.17f;
        case ShipClass.Destroyer:
          return 1.13f;
        case ShipClass.Cruiser:
          return 1.09f;
        case ShipClass.Battleship:
          return 1.05f;
        case ShipClass.Dreadnought:
          return 1.01f;
        case ShipClass.Titan:
          return 0.97f;
        case ShipClass.Flagship:
        case ShipClass.Carrier:
        case ShipClass.WyrdInvader:
          return 0.93f;
        case ShipClass.Flayer:
        case ShipClass.WyrdAssassin:
          return 0.89f;
        case ShipClass.Executioner:
        case ShipClass.WyrdReaper:
          return 0.85f;
        case ShipClass.Devastator:
          return 0.81f;
        case ShipClass.Despoiler:
          return 0.77f;
        case ShipClass.Annihilator:
          return 0.73f;
        case ShipClass.WyrdTerminus:
          return 0.69f;
        default:
          return 1f;
      }
    }

    private static SoundEffectType GetMoveSound(ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Corvette:
        case ShipClass.Destroyer:
        case ShipClass.Battleship:
          return SoundEffectType.engine_pirate_3;
        case ShipClass.Titan:
        case ShipClass.Flayer:
        case ShipClass.Devastator:
        case ShipClass.Annihilator:
          return SoundEffectType.engine_pirate_2;
        case ShipClass.WyrdInvader:
        case ShipClass.WyrdReaper:
          return SoundEffectType.engine_wyrd_1;
        case ShipClass.WyrdAssassin:
          return SoundEffectType.engine_stealth;
        case ShipClass.WyrdTerminus:
          return SoundEffectType.engine_wyrd_2;
        default:
          return SoundEffectType.engine_pirate_1;
      }
    }

    private static float GetSoundRate(NpcClass npc)
    {
      switch (npc)
      {
        case NpcClass.Drone:
          return 1.35f;
        case NpcClass.Warrior:
          return 1.25f;
        case NpcClass.Defender:
          return 1.15f;
        case NpcClass.Predator:
          return 1.05f;
        case NpcClass.Queen:
          return 0.95f;
        case NpcClass.King:
          return 0.85f;
        case NpcClass.Hive:
          return 0.75f;
        case NpcClass.Raider:
          return 1.2f;
        case NpcClass.Bandit:
          return 1.15f;
        case NpcClass.Outlaw:
          return 1.1f;
        case NpcClass.Hijacker:
          return 1.05f;
        case NpcClass.Brigand:
          return 1f;
        case NpcClass.Prowler:
          return 0.95f;
        case NpcClass.Marauder:
          return 0.9f;
        case NpcClass.Corsair:
          return 0.85f;
        case NpcClass.Enforcer:
          return 0.8f;
        case NpcClass.Stronghold:
          return 0.75f;
        case NpcClass.Colonizer:
          return 1.25f;
        case NpcClass.Wayfarer:
          return 1.15f;
        case NpcClass.Invader:
          return 1.05f;
        case NpcClass.Assassin:
          return 0.95f;
        case NpcClass.Reaper:
          return 0.85f;
        case NpcClass.SeedWorld:
          return 0.75f;
        default:
          return 1f;
      }
    }

    private static EventSoundEffect GetAttack(WeaponItem weapon)
    {
      return SoundHelper.GetAttack(weapon == null ? WeaponClass.NULL : weapon.Class);
    }

    private static EventSoundEffect GetAttack(WeaponClass weapon)
    {
      float rate = (float) (1.25 - ((weapon == WeaponClass.NULL ? 5.0 : (double) weapon.AttackSpeedMS() / 1000.0) - 3.0) * 0.0299999993294477) + (float) (RNG.R.NextDouble() * 0.04 - RNG.R.NextDouble() * -0.04);
      return new EventSoundEffect(SoundHelper.GetSoundType(weapon), rate);
    }

    private static SoundEffectType GetSoundType(WeaponClass weapon)
    {
      switch (weapon)
      {
        case WeaponClass.AUTO_CANNON:
        case WeaponClass.BASILISK:
        case WeaponClass.MUTILATOR:
          return SoundEffectType.weapon_machine_2;
        case WeaponClass.MASS_DRIVER:
        case WeaponClass.RIPPER:
        case WeaponClass.GRAVITY_SMASHER:
        case WeaponClass.ION_CANNON:
        case WeaponClass.SUCCUBUS:
        case WeaponClass.HARPY:
        case WeaponClass.HYDRA:
        case WeaponClass.ERADICATOR:
          return SoundEffectType.weapon_laser_7;
        case WeaponClass.LEVIATHAN:
        case WeaponClass.PULVERIZER:
        case WeaponClass.PROTON_LAUNCHER:
        case WeaponClass.DESTRUCTION:
        case WeaponClass.EXODUS:
        case WeaponClass.PROPHECY:
        case WeaponClass.PAIN:
        case WeaponClass.ATOMIZER:
        case WeaponClass.RIFTBREAKER:
        case WeaponClass.NULLCANNON:
          return SoundEffectType.weapon_laser_6;
        case WeaponClass.SCREAMER:
        case WeaponClass.BURST_CANNON:
        case WeaponClass.SERPENT:
          return SoundEffectType.weapon_machine_1;
        case WeaponClass.HELLCANNON:
        case WeaponClass.OMEGA_RIFLE:
        case WeaponClass.DISRUPTOR:
        case WeaponClass.PENATRATOR:
        case WeaponClass.FIRECAT:
        case WeaponClass.BEHEMOTH:
        case WeaponClass.GARGOYLE:
        case WeaponClass.STARSHATTER:
        case WeaponClass.VOIDBLASTER:
          return SoundEffectType.weapon_laser_5;
        case WeaponClass.FUSION_BEAM:
        case WeaponClass.PHASER:
        case WeaponClass.MESON_BLASTER:
        case WeaponClass.SILENCE:
        case WeaponClass.OPHIDIAN:
          return SoundEffectType.weapon_laser_4;
        case WeaponClass.GAUSS_CANNON:
        case WeaponClass.RAPTURE:
        case WeaponClass.TORMENT:
        case WeaponClass.FRENZY:
        case WeaponClass.DEMOLISHER:
          return SoundEffectType.weapon_machine_4;
        case WeaponClass.ACCELERATOR:
        case WeaponClass.PLASMA_LANCE:
        case WeaponClass.KRAKEN:
        case WeaponClass.DESOLATOR:
        case WeaponClass.SOULTAKER:
          return SoundEffectType.weapon_laser_2;
        case WeaponClass.RAIL_GUN:
        case WeaponClass.DARKNESS:
        case WeaponClass.RADIANCE:
        case WeaponClass.BRUTALIZER:
        case WeaponClass.VAPORIZER:
        case WeaponClass.MINDSLAYER:
          return SoundEffectType.weapon_laser_3;
        case WeaponClass.GLORY:
        case WeaponClass.OBLIVION:
        case WeaponClass.RUIN:
        case WeaponClass.SMOLDER:
        case WeaponClass.AGONY:
        case WeaponClass.ANIMUS:
        case WeaponClass.RAVAGER:
          return SoundEffectType.weapon_machine_5;
        case WeaponClass.HORROR:
        case WeaponClass.BANSHEE:
        case WeaponClass.WYVERN:
        case WeaponClass.DRAGON:
        case WeaponClass.CORRUPTOR:
        case WeaponClass.INCINERATOR:
          return SoundEffectType.weapon_laser_1;
        case WeaponClass.CATACLYSM:
        case WeaponClass.VIPER:
        case WeaponClass.EXTERMINATOR:
          return SoundEffectType.weapon_machine_3;
        default:
          return SoundEffectType.weapon_machine_1;
      }
    }
  }
}
