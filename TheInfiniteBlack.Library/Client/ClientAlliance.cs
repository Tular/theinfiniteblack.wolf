﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientAlliance
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public class ClientAlliance : NamedClientObject
  {
    private HashSet<ClientCorporation> _members = new HashSet<ClientCorporation>();
    private readonly short _id;
    private ClientCorporation _leader;
    private Sector _location;
    private PlanetClass _planet;

    public short ID
    {
      get
      {
        return this._id;
      }
    }

    public ClientCorporation Leader
    {
      get
      {
        return this._leader;
      }
      set
      {
        if (this._leader == value)
          return;
        this._leader = value;
        this.OnChanged();
      }
    }

    public PlanetClass Planet
    {
      get
      {
        return this._planet;
      }
      set
      {
        if (this._planet == value)
          return;
        this._planet = value;
        this.OnChanged();
      }
    }

    public Sector Location
    {
      get
      {
        return this._location;
      }
    }

    public IEnumerable<ClientCorporation> Members
    {
      get
      {
        return (IEnumerable<ClientCorporation>) this._members;
      }
    }

    public ClientAlliance(IGameState state, short id)
      : base(state)
    {
      this._id = id;
    }

    public bool Contains(ClientCorporation corp)
    {
      return this._members.Contains(corp);
    }

    protected internal void Add(ClientCorporation corp)
    {
      if (this._members.Contains(corp))
        return;
      this._members = new HashSet<ClientCorporation>((IEnumerable<ClientCorporation>) this._members)
      {
        corp
      };
      this.OnChanged();
    }

    protected internal void Remove(ClientCorporation corp)
    {
      if (!this._members.Contains(corp))
        return;
      HashSet<ClientCorporation> clientCorporationSet = new HashSet<ClientCorporation>((IEnumerable<ClientCorporation>) this._members);
      clientCorporationSet.Remove(corp);
      this._members = clientCorporationSet;
      this.OnChanged();
    }

    protected internal void SetLocation(Sector location)
    {
      Sector location1 = this._location;
      this._location = (Sector) null;
      if (location1 != null && location1.Alliance == this)
        location1.Alliance = (ClientAlliance) null;
      this._location = location;
      if (this._location != null)
        this._location.Alliance = this;
      this.OnChanged();
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      sb.Append(this._leader == null ? "Unknown?" : this._leader.Name);
    }
  }
}
