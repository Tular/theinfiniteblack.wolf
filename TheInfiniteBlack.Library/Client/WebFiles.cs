﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.WebFiles
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Client
{
  public static class WebFiles
  {
    public const string BlackDollars = "https://spellbook.com/purchase/tib/blackdollars.php?playername={0}&mobile=1";
    public const string BlackDollarsNoPlayer = "https://spellbook.com/purchase/tib/blackdollars.php?mobile=1";
    public const string Forums = "https://www.spellbook.com/forum/?cat=2&mobile=1";
    public const string Wiki = "https://www.spellbook.com/tib/wiki/";
    public const string Facebook = "https://www.facebook.com/spellbooks";
    public const string Twitter = "https://twitter.com/Spellbookgames";
    public const string Tutorial = "http://www.spellbook.com/tib/tutorial.php?mobile=1";
    public const string EULA = "http://www.theinfiniteblack.com/tib/eula.html";
    public const string IP = "http://www.theinfiniteblack.com/tib/ip.txt";
    public const string IP_DEV = "http://www.theinfiniteblack.com/tib/ip_dev.txt";
    public const string ServerInfo = "http://www.theinfiniteblack.com/tib/server_info_unity.txt";
    public const string MOTD = "http://www.theinfiniteblack.com/tib/MOTD/unity_motd.txt";
    public const string Leaderboards = "http://www.TheInfiniteBlack.com/Community/TIB/Leaderboards/";
    private const string PlayerLeaderboardPage = "http://www.TheInfiniteBlack.com/Community/TIB/Leaderboards/player.aspx?playername={0}&server={1}";

    public static string GetPlayerLeaderboardUrl(ClientPlayer player, sbyte serverId)
    {
      return string.Format("http://www.TheInfiniteBlack.com/Community/TIB/Leaderboards/player.aspx?playername={0}&server={1}", (object) player.Name, (object) serverId);
    }

    public static string GetPlayerLeaderboardUrl(string playerName, sbyte serverId)
    {
      return string.Format("http://www.TheInfiniteBlack.com/Community/TIB/Leaderboards/player.aspx?playername={0}&server={1}", (object) playerName, (object) serverId);
    }
  }
}
