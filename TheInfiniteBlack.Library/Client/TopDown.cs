﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.TopDown
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  internal class TopDown : NpcAttackOrder
  {
    public override string Description
    {
      get
      {
        return "Top Down";
      }
    }

    public override Npc SelectTargetFrom(IEnumerable<Npc> npcs)
    {
      return npcs.First<Npc>();
    }

    public override NpcAttackOrder Next()
    {
      return (NpcAttackOrder) new DreadsFirst();
    }
  }
}
