﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ShipBank
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public class ShipBank
  {
    private bool[] _bank = new bool[19];

    public bool this[ShipClass type]
    {
      get
      {
        return this._bank[(int) type];
      }
    }

    protected internal void Set(bool[] bank)
    {
      this._bank = bank;
    }
  }
}
