﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.FollowCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Client
{
  internal class FollowCommand : ITextCommands
  {
    public string Trigger()
    {
      return ":follow ";
    }

    public void Process(GameState gameState, string command)
    {
      string target = command.Replace(":follow", "").Trim();
      gameState.Follow(target);
    }
  }
}
