﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.AllianceCollection
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Library.Client
{
  public class AllianceCollection
  {
    private readonly Dictionary<short, ClientAlliance> _values = new Dictionary<short, ClientAlliance>();
    private readonly IGameState _state;

    public IEnumerable<ClientAlliance> Values
    {
      get
      {
        return (IEnumerable<ClientAlliance>) this._values.Values;
      }
    }

    public AllianceCollection(IGameState state)
    {
      this._state = state;
    }

    public ClientAlliance Get(short id)
    {
      if ((int) id == (int) short.MinValue)
        return (ClientAlliance) null;
      ClientAlliance clientAlliance1;
      if (this._values.TryGetValue(id, out clientAlliance1))
        return clientAlliance1;
      ClientAlliance clientAlliance2 = new ClientAlliance(this._state, id);
      this._values[id] = clientAlliance2;
      return clientAlliance2;
    }
  }
}
