﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Message
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Client
{
  public class Message
  {
    public string Player { get; set; }

    public string Text { get; set; }

    public override bool Equals(object obj)
    {
      return this.Player == ((Message) obj).Player && this.Text == ((Message) obj).Text;
    }
  }
}
