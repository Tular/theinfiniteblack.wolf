﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.IGameState
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Library.Network;

namespace TheInfiniteBlack.Library.Client
{
  public interface IGameState
  {
    bool IsLoggedIn { get; }

    IUserInterface UI { get; }

    INetwork Net { get; }

    MusicMoodType Music { get; }

    AccountManager Accounts { get; }

    LoginRequest Login { get; }

    Account MyAccount { get; }

    AccountSettings MySettings { get; }

    ClientPlayer MyPlayer { get; }

    Ship MyShip { get; }

    Sector MyLoc { get; }

    CombatEntity MyAttackTarget { get; }

    CombatEntity MyFollowTarget { get; }

    LocalEntities Local { get; }

    ClientBank Bank { get; }

    ClientBuyables Buyables { get; }

    PlayerCollection Players { get; }

    CorporationCollection Corporations { get; }

    AllianceCollection Alliances { get; }

    AuctionCollection Auctions { get; }

    EntityCollection Entities { get; }

    ClientMap Map { get; }

    ShipBank Ships { get; }

    CourseController Course { get; }

    TimeComponent AttackCooldown { get; }

    TimeComponent MoveCooldown { get; }

    TimeComponent RepairCooldown { get; }

    TimeComponent HarvestCooldown { get; }

    TimeComponent EarthJumpTime { get; }

    CasinoManager Casino { get; }

    long WorldTimeMS { get; }

    string CourseStatusHeader { get; }

    string CourseStatus { get; }

    string FollowStatusHeader { get; }

    string FollowStatus { get; }

    string AttackStatusHeader { get; }

    string AttackStatus { get; }

    string HarvestStatus { get; }

    bool RecordKills { get; }

    string LastKillString { get; set; }

    int KillsFor { get; set; }

    int KillsAgainst { get; set; }

    bool TimeStamp { get; }

    bool Panic { get; set; }

    bool AutoSellNon { get; set; }

    bool AutoSellHalf { get; set; }

    bool AutoSellAll { get; set; }

    bool AutoGun { get; set; }

    bool AutoMoney { get; set; }

    int AutoRjPhase { get; set; }

    string FolTar { get; set; }

    string GetNextReply();

    void ShowPopup(string text, bool canClose);

    void ShowAlert(string text);

    void ShowEvent(string text);

    void ShowAcceptDecline(string title, string text, INetworkData accept, INetworkData decline);

    void Play(SoundEffectType sound);

    void Play(SoundEffectType sound, float rate);

    void Play(SoundEffectType sound, float rate, float delay);

    void OnUpdate();

    void Connect(sbyte[] key);

    void Disconnect(string reason);

    void DoChat(string text, ChatType mode);

    void DoPrivateChat(string text, string playerName);

    void DoPurchase(PurchaseItem item);

    void DoLoot(CargoEntity target);

    void DoHarvest(Asteroid target);

    void DoRepair(PlayerCombatEntity target);

    void DoRepairWith(StarPort starport);

    void DoAttack(CombatEntity target);

    void DoFollow(CombatEntity target);

    void DoDevelopment(PlayerCombatEntity target, ResourceType resource);

    void DoDevelopment(PlayerCombatEntity target, int blackDollars);

    void DoResourcesJettison(int organics, int gas, int metals, int radioactives, int darkMatter);

    void DoResourcesTransfer(Ship target, int organics, int gas, int metals, int radioactives, int darkMatter);

    void DoResourcesSellAll();

    void DoResourcesSellAllExceptMetals();

    void DoResourcesSell(int organics, int gas, int metals, int radioactives, int darkMatter);

    void DoResourcesBuy(int organics, int gas, int metals, int radioactives, int darkMatter);

    void DoItemJettison(EquipmentItem item);

    void DoItemSell(StarPort starport, EquipmentItem item);

    void DoItemBuy(StarPort starport, EquipmentItem item, CurrencyType currency);

    void DoItemToBank(EquipmentItem item);

    void DoItemFromBank(EquipmentItem item);

    void DoItemToGarrison(EquipmentItem item);

    void DoItemFromGarrison(EquipmentItem item);

    void DoItemEngineer(EngineeringType type, EquipmentItem item);

    void DoItemEquip(EquipmentItem item);

    void DoItemUnequip(EquipmentItem item);

    void DoAuctionRequestPage(ItemType type, ItemRarity rarity);

    void DoAuctionBid(ClientAuction auction);

    void DoAuctionBuyout(ClientAuction auction, CurrencyType currency);

    void DoAuctionCancel(ClientAuction auction);

    void DoAuctionPost(EquipmentItem item, int bid, int buyout);

    void DoSwapShips(ShipClass ship, sbyte shipSkin);

    bool CanBuyShip(ShipClass ship, CurrencyType currency);

    void DoBuyShip(ShipClass ship, CurrencyType currency);

    void DoResearch(TechnologyType type);

    void DoCorpCreate(string name);

    void DoCorpRename(string name);

    void DoCorpLeave();

    void DoCorpDeposit(CurrencyType currency, int amt);

    void DoCorpWithdraw(CurrencyType currency, int amt);

    void DoCorpInvite(string playerName);

    void DoCorpKick(ClientPlayer target);

    void DoCorpDemote(ClientPlayer target);

    void DoCorpPromote(ClientPlayer target);

    void DoCorpDeployGarrison();

    void DoCorpRecallGarrison();

    void DoCorpSetShipColor(byte value);

    void DoAllianceCreate();

    void DoAllianceLeave();

    void DoAllianceLeader(ClientCorporation corp);

    void DoAllianceInvite(ClientCorporation corp);

    void DoAllianceKick(ClientCorporation corp);

    void DoAllianceDeployPlanet();

    void DoAllianceRecallPlanet();

    void DoRequestInventory(Ship ship);

    void DoRequestInventory(StarPort starport);

    void DoRequestInventory(Garrison garrison);

    void DoRequestStats(ClientPlayer player);
  }
}
