﻿namespace TheInfiniteBlack.Library.Client
{
    internal class AutoResPlanetCommand : ITextCommands
    {
        public string Trigger()
        {
            return ":autoresplanet";
        }

        public void Process(GameState gameState, string command)
        {
            gameState.ResPlanetToggle(command.Replace(":autoresplanet", "").Trim());
        }
    }
}
