﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.GameState
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Client.Mods;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Library.Network;
using TheInfiniteBlack.Library.Network.Commands;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client
{
  public class GameState : IGameState
  {
    private static readonly List<string> _lastPrivates = new List<string>();
    private static int _lastPrivateIndex = -1;
    public static Message LastSentText = new Message();
    public static bool InvasionFreeRouting = false;
    public static bool ShowAsteroidChanges = false;
    public static List<string> NoBustList = new List<string>();
    public static bool NoBust = false;
    public static bool HitSMS = false;
    public static bool DoLog = false;
    public static string LastLog = "";
    private readonly bool[,] _auctionRequestCheck = new bool[8, 8];
    private readonly ClientBank _bank = new ClientBank();
    private readonly EntityCollection _entities = new EntityCollection();
    private readonly ClientMap _map = new ClientMap();
    private readonly ShipBank _ships = new ShipBank();
    private IList<Ship> DTeam = (IList<Ship>) new List<Ship>();
    private IMessageController _messageController = (IMessageController) new MessageController();
    private NpcAttackOrder _attackOrder = (NpcAttackOrder) new TopDown();
    private PlayerAttackOrder _playerAttackOrder = (PlayerAttackOrder) new LowestHull();
    private long _lastMindSurgePurchase = 0;
    private DateTime _markedInvasionsTimestamp = DateTime.MinValue;
    private DateTime _lastRepair = DateTime.MinValue;
    private DateTime _repTS = DateTime.MinValue;
    private DateTime _lastRes = DateTime.MinValue; // last timestamp of resource pick up.
    public DateTime JettiTrigger = DateTime.MinValue; // timestamp when Jettison was executed or triggered.
    public DateTime RjTrigger = DateTime.MinValue; // timestamp when Jettison was executed or triggered.
    public DateTime RjPurchase = DateTime.MinValue; // timestamp when RJ was purchased.
    public DateTime RjExecuted = DateTime.MinValue; // timestamp when the RJ was executed.
    private DateTime _lastFeed = DateTime.MinValue; // last timestamp of feeding.
    private DateTime _FeedDelayTmr = DateTime.MinValue; // last timestamp of feeding.
    private DateTime _lastSale = DateTime.MinValue; // timestamp of last sale to SP.
    private Dictionary<string, EquippedItems> _shipSetups = new Dictionary<string, EquippedItems>();
    private DateTime _lastStealAlertTime = DateTime.MinValue;
    private bool _autoFarm = false;
    private DateTime _lastMove = DateTime.MinValue;
    private bool _autoFarmBlackGun = false;
    private bool _autoFarmBlackDriver = false;
    private Queue<Message> _unsentMessages = new Queue<Message>();
    private DateTime _lastMessageSent = DateTime.MinValue;
    private int _shipsInFleet = 0;
    private DateTime _lastWAAAlertTime = DateTime.MinValue;
    private List<ITextCommands> _textCommands = new List<ITextCommands>()
    {
      (ITextCommands) new FollowCommand(),
      (ITextCommands) new AutoFeedCommand(),
      (ITextCommands) new BackupFeedCommand(),
      (ITextCommands) new SelfHealAmountCommand(),
      (ITextCommands) new FriendHealAmountCommand(),
      (ITextCommands) new InfoCommand(),
      (ITextCommands) new MinMetsCommand(),
      (ITextCommands) new AutoResearchCommand(),
      (ITextCommands) new AutoResPlanetCommand(),
      (ITextCommands) new AutoHealTimeCommand(),
      (ITextCommands) new MinResTimeCommand(),
      (ITextCommands) new DTeamMessageCommand(),
      (ITextCommands) new SortCommand()
    };
    private readonly List<string> _boolCommands = new List<string>()
    {
      nameof (AutoDecon),
      nameof (AutoDegrapple),
      nameof (AutoEngage),
      nameof (AutoFill),
      nameof (AutoFriendHeal),
      nameof (AutoGun),
      nameof (AutoHook),
      nameof (AutoMindSurge),
      nameof (AutoMoney),
      nameof (AutoRes),
      nameof (AutoHarvest),
      nameof (AutoScrap),
      nameof (AutoSelfHeal),
      nameof (AutoSell),
      nameof (AutoSellHalf),
      nameof (AutoSellAll),
      nameof (AutoSellNon),
      nameof (GrabIt),
      nameof (Invasions),
      nameof (Panic),
      nameof (PanicMode),
      nameof (Realism),
      nameof (Record),
      nameof (RoidChanges),
      nameof (RouteMode),
      nameof (SeedSave),
      nameof (ShotHold),
      nameof (ShouldLog),
      nameof (SMS),
      nameof (AutoJetti),
      nameof (FkeyLevel)
    };
    private bool _seedSave = false;
    private bool _panic = false;
    private bool _panicMode = false;
    private bool _autoDegrapple = false;
    private bool _autoFill = false;
    private bool _shotHold = false;
    private bool _autoSell = false;
    private bool _autoSellAll = false; /* Bool to trigger selling all mets also at first SP */
    private bool _autoSellHalf = false; /* Bool to trigger selling half mets also at first SP */
    private bool _autoSellNon = false; /* Bool to trigger selling all non mets at first SP */
    private int _autoSellPhase = 0; // INT to state which type of sell action is to be executed.
    private int _autoRjPhase = 0; // INT to state which phase of the RJ process you are in.

    Dictionary<int, string> sellDict = new Dictionary<int, string>()
                                                {
                                                    {1,"Off"},
                                                    {2,"Sell non"},
                                                    {3,"Sell all(Tech non)"},
                                                    {4,"Sell half(Tech non)"},
                                                    {5,"Sell all and Tech half"}
                                                };
    Dictionary<int, string> rjDict = new Dictionary<int, string>()
                                                {
                                                    {1,"Off"},
                                                    {2,"Calc RJ timers"}, 
                                                    {3,"Wait to align RJ across Stack"},
                                                    {4,"Buy RJ"},
                                                    {5,"Wait for RJ to happen"},
                                                    {6,"Refollow and turn off"}
                                                };
    private bool _autoScrap = true;
    private bool _autoDecon = false;
    private bool _autoMoney = false;
    private bool _autoRes = false;
    private bool _autoJetti = false;
    private bool _autoRJ = false;
    private bool _autoHarvest = false; // bool to trigger automatic roid munching
    private bool _invasions = false;
    private bool _autoMindSurge = false;
    private bool _autoHook = false;
    private bool _autoEngage = false;
    private bool _autoSelfHeal = false;
    private bool _autoFriendHeal = false;
    private bool _autoGun = false;
    private string _autofeedTarget = "";
    private string _backupfeedTarget = "";
    private string _reptarget = "";
    private bool _autofeed = false;
    private bool _backupfeed = false;
    private int _autoSelfHealAmount = 5500;
    private int _autoFriendHealAmount = 5150;
    private int _minimumMetals = 0;
    private int _minimumMetals_org = 0;
    private int _curRes = 0;
    private int _minimumRes = 100; // Int to delay autosell
    private int _minFeedMetals = 25;
    private int _autoHealTime = 375;
    private int _autoHealDelay = 1000;
//    private int _autoFeedTime = 250;
    private int _MinResTime = 500; // Minimum wait time for RES pick up after teching.
    private bool _grabit = false;
    // REALISM variables
    private bool _realism = false;
    Random _rand = new Random();
    private int _RealismMS;
    private int _RealismMS_SP = 1500;
    private int _RealismMS_RES = 750;
    private int _RealismMS_FEED = 750;
    private int _RealismMS_GUN = 750;
    private int _RealismMS_TECH = 750;
    private int _RealismMS_JETTI = 400;
    private int _RoidMS = 250;
    // end REALISM
    private int _MySpeed = 0;
    private int _StackSpeed = 0;
    private int _SpeedSelf = 0;
    private int _SpeedCrew = 0;
    private int _RJDelay = 0;
    private int _RJ_Pur = 0; // RIFT JUMP Purchase
    private int _RJ_Ref_Delay = 250; // Delay before setting refollow
    private Queue<string> unsentAlerts = new Queue<string>();
    private readonly Dictionary<string, Action<GameState>> _functionalCommands = new Dictionary<string, Action<GameState>>()
    {
      {
        "custom",
        (Action<GameState>) (x => x.ShowCustomCommands())
      },
      {
        "rescue",
        (Action<GameState>) (x => x.Rescue())
      },
      {
        "afdriver",
        (Action<GameState>) (x => x.AutoFarmBlackDriver())
      },
      {
        "afgun",
        (Action<GameState>) (x => x.AutoFarmBlackGun())
      },
      {
        "invydriver",
        (Action<GameState>) (x => x.InvyDriver())
      },
      {
        "invygun",
        (Action<GameState>) (x => x.InvyGun())
      },
      {
        "notfull",
        (Action<GameState>) (x => x.NotFull())
      },
      {
        "afgrey",
        (Action<GameState>) (x => x.AutoFarm())
      },
      {
        "emulate",
        (Action<GameState>) (x => x.Emulate())
      },
      {
        "stack",
        (Action<GameState>) (x => x.StackSummary())
      },
      {
        "nostealth",
        (Action<GameState>) (x => x.NoStealth())
      },
      {
        "setdteam",
        (Action<GameState>) (x => x.SetDTeam())
      },
      {
        "resetkills",
        (Action<GameState>) (x => x.ResetKills())
      },
      {
        "gunmode",
        (Action<GameState>) (x => x.GunMode())
      },
      {
        "playergunmode",
        (Action<GameState>) (x => x.PlayerGunMode())
      },
      {
        "autofarm off",
        (Action<GameState>) (x => x.AutoFarmOff())
      },
      {
        "status",
        (Action<GameState>) (x => x.Status())
      },
      {
        "listseeds",
        (Action<GameState>) (x => x.ListSeeds())
      },
      {
        "bfg",
        (Action<GameState>) (x => x.BFG())
      },
      {
        "hook",
        (Action<GameState>) (x => x.Hook())
      },
      {
        "beam",
        (Action<GameState>) (x => x.Beam())
      },
      {
        "test",
        (Action<GameState>) (x => x.Test())
      },
      {
        "eastgun",
        (Action<GameState>) (x => x.EastGun())
      },
      {
        "1.",
        (Action<GameState>) (x => x.EastGun())
      },
      {
        "eastdriver",
        (Action<GameState>) (x => x.EastDriver())
      },
      {
        "2.",
        (Action<GameState>) (x => x.EastDriver())
      },
      {
        "eurogun",
        (Action<GameState>) (x => x.EuroGun())
      },
      {
        "3.",
        (Action<GameState>) (x => x.EuroGun())
      },
      {
        "4.",
        (Action<GameState>) (x => x.ToggleGun())
      },
      {
        "5.",
        (Action<GameState>) (x => x.ToggleMoney())
      },
      {
        "6.",
        (Action<GameState>) (x => x.ToggleSell())
      },
      {
        "8.",
        (Action<GameState>) (x => x.ToggleHarvest())
      },
      {
        "m",
        (Action<GameState>) (x => x.ToggleMets())
      },
      {
        "clearjet",
        (Action<GameState>) (x => x.ClearJet())
      },
      {
        "9.",
        (Action<GameState>) (x => x.ClearJet())
      },
      {
        "setmule",
        (Action<GameState>) (x => x.SetMule())
      },
      {
        "gomule",
        (Action<GameState>) (x => x.GoMule())
      },
            {
        "jumpspeed",
        (Action<GameState>) (x => x.JumpSpeed())
      },
      {
        "findroid",
        (Action<GameState>) (x => x.FindRoid())
      }
    };
    private long _lastSellTime = 0;
    private long _lastTfTime = 0;
    private long _lastHeartBeat = 0;
    private long _lastMessageCheck = 0;
    private Dictionary<string, string> _savedSeeds = new Dictionary<string, string>();
    private long _lastMoneyPickup = 0;
    private readonly AccountManager _accounts;
    private readonly AllianceCollection _alliances;
    private readonly TimeComponent _attackCooldown;
    private readonly AuctionCollection _auctions;
    private readonly ClientBuyables _buyables;
    private readonly CasinoManager _casino;
    private readonly CorporationCollection _corporations;
    private readonly CourseController _course;
    private readonly TimeComponent _earthJumpTime;
    private readonly TimeComponent _harvestCooldown;
    private readonly LocalEntities _local;
    private readonly LoginRequest _login;
    private readonly TimeComponent _moveCooldown;
    private readonly INetwork _net;
    private readonly PlayerCollection _players;
    private readonly TimeComponent _repairCooldown;
    private readonly IUserInterface _ui;
    private long _lastAttackTimeMS;
    private sbyte[] _lastKey;
    private int _lastTicks;
    private Account _myAccount;
    private CombatEntity _myAttackTarget;
    private CombatEntity _myFollowTarget;
    private Sector _myLoc;
    private ClientPlayer _myPlayer;
    private Ship _myShip;
    private int _secondTimeMS;
    private long _worldTimeMS;
    private CombatEntity _driver;
    private Sector _previousSector;
    private bool _autoResearch;
    private bool _autoResPlanet;
    private string _researchTarget;
    private Sector _jettisonSector; // keep track of where you jettison so autores will not pick up from that sector.
    private Sector _muleSector; // keep track of where you mule to so autores will not pick up from that sector.
    private string _foltar = ""; // Use to refollow the new drivers based upon sector chat.
    private CombatEntity _NewFollowTarget;

    // EXPERIMENTAL
    private KeyboardInput keyboard;
    private int TIBInstanceID;
    private string TIBInstanceName;
    private string TibProcessName = "The Infinite Black";
    private int win_x = 0;
    private int win_y = 0;
    private int win_h = 0;
    private int win_w = 0;
    public static bool _FkeyLevel = false; // lvl to indicate which level of F-KEYS to use
    private int _FkeyDelay = 10000;
    private DateTime TabKeyPress = DateTime.MinValue;

    // END

    public static string SubtitleStat { get; private set; }

    public bool RecordKills { get; private set; }

    public string LastKillString { get; set; }

    public int KillsFor { get; set; }

    public int KillsAgainst { get; set; }

    public bool TimeStamp { get; private set; }

    public bool IsLoggedIn { get; private set; }

    public bool SeedSave
    {
      get
      {
        return this._seedSave;
      }
      set
      {
        this._seedSave = value;
        this.ShowAlert("Seed Saving: " + (object) this._seedSave);
      }
    }

    public bool Panic
    {
      get
      {
        return this._panic;
      }
      set
      {
        this._panic = value;
        this.ShowAlert("PANIC: " + (object) this._panic);
      }
    }

    public bool PanicMode
    {
      get
      {
        return this._panicMode;
      }
      set
      {
        this._panicMode = value;
        this.ShowAlert("Panic Mode " + (this._panicMode ? "Enabled" : "Disabled"));
      }
    }

    public bool AutoDegrapple
    {
      get
      {
        return this._autoDegrapple;
      }
      set
      {
        this._autoDegrapple = value;
        this.ShowAlert("Auto Degrapple " + (this._autoDegrapple ? "ON" : "OFF"));
      }
    }

    public bool AutoFill
    {
      get
      {
        return this._autoFill;
      }
      set
      {
        this._autoFill = value;
        this.ShowAlert("Auto Fill " + (this._autoFill ? "ON" : "OFF"));
      }
    }

    public bool ShotHold
    {
      get
      {
        return this._shotHold;
      }
      set
      {
        this._shotHold = value;
        this.ShowAlert("Shot Hold Toggle " + (this._shotHold ? "ON" : "OFF"));
      }
    }

    public bool AutoSell
    {
      get
      {
        return this._autoSell;
      }
      set
      {
        this._autoSell = value;
        this.ShowAlert("Auto Sell " + (this._autoSell ? "ON" : "OFF"));
        this.ShowAlert("Use :minmets <amount> to set how many metals to keep when auto selling");
      }
    }

    public bool AutoSellNon
    {
      get
      {
        return this._autoSellNon;
      }
      set
      {
        this._autoSellNon = value;
        this.ShowAlert("Auto Sell Non " + (this._autoSellNon ? "ON" : "OFF"));
        this.ShowAlert("This will sell all non mets!");
      }
    }

    public bool AutoSellHalf
    {
      get
      {
        return this._autoSellHalf;
      }
      set
      {
        if (this._myShip.Special != null && this._myShip.Special.Class == SpecialClass.TECHNICIAN)
        {
          this._autoSellHalf = false;
          this._autoSellNon = true;
        }
        else
        {
          this._autoSellHalf = value;
          this.ShowAlert("Auto Sell Half " + (this._autoSellHalf ? "ON" : "OFF"));
          this.ShowAlert("This will sell mets up to half of your ships mets capacity");
        }
      }
    }
    public bool AutoSellAll
    {
      get
      {
        return this._autoSellAll;
      }
      set
      {
        if (this._myShip.Special != null && this._myShip.Special.Class == SpecialClass.TECHNICIAN)
        {
          this._autoSellAll = false;
          this._autoSellNon = true;
        }
        else
        {
          this._autoSellAll = value;
          this.ShowAlert("Auto Sell All " + (this._autoSellAll ? "ON" : "OFF"));
          this.ShowAlert("This will sell all your mets");
        }
      }
    }
    /* AutoSell Phases:
    Stage 0 = Off
    Stage 1 = Sell non
    Stage 2 = Sell all(Tech non)
    Stage 3 = Sell half(Tech non)
    Stage 4 = Sell all and Tech half. */
    public int AutoSellPhase
    {
      get
      {
        return this._autoSellPhase;
      }
      set
      {
        if (this._autoSellPhase >= 0 && this._autoSellPhase < 4)
        {
          this._autoSellPhase++;
          if (this._autoSellPhase == 1) // Stage 1 = Sell non
          {
            this._autoSellNon = true;
          }
          else if (this._autoSellPhase == 2) // Stage 2 = Sell all(Tech non)
          {
            if (this._myShip.Special == null || this._myShip.Special.Class != SpecialClass.TECHNICIAN)   // NO SPECIAL or NON CARRIER get to SELL ALL
            {
              this._autoSellAll = true;
              this._autoSellNon = false;
            }
          }
          else if (this._autoSellPhase == 3) // Stage 3 = Sell half(Tech non)
          {
            if (this._myShip.Special == null || this._myShip.Special.Class != SpecialClass.TECHNICIAN)  // NO SPECIAL or NON CARRIER get to SELL HALF
            {
              this._autoSellAll = false;
              this._autoSellHalf = true;
            }
          }
          else if (this._autoSellPhase == 4) // Stage 4 = Sell all(Tech half)
          {
            if (this._myShip.Special != null && this._myShip.Special.Class == SpecialClass.TECHNICIAN)  // Carrier gets to sell half
            {
              this._autoSellHalf = true;
            }
            else
            {
              this._autoSellAll = true;
              this._autoSellHalf = false;
            }
          }
        }
        else
        {
          this._autoSellPhase = 0;
          this._autoSellAll = false;
          this._autoSellHalf = false;
          this._autoSellNon = false;
        }
        this.ShowAlert("AutoSellPhase set to: " + (this.AutoSellPhase) + ": " + sellDict[(this._autoSellPhase) + 1]);
      }
    }
    public bool AutoScrap
    {
      get
      {
        return this._autoScrap;
      }
      set
      {
        this._autoScrap = value;
        this.ShowAlert("Auto Scrap " + (this._autoScrap ? "ON" : "OFF"));
      }
    }

    public bool AutoDecon
    {
      get
      {
        return this._autoDecon;
      }
      set
      {
        this._autoDecon = value;
        this.ShowAlert("Auto Deconstruct " + (this._autoDecon ? "ON" : "OFF"));
      }
    }

    public bool AutoMoney
    {
      get
      {
        return this._autoMoney;
      }
      set
      {
        this._autoMoney = value;
        this.ShowAlert("Auto Money " + (this._autoMoney ? "ON" : "OFF"));
      }
    }

    public bool AutoRes
    {
      get
      {
        return this._autoRes;
      }
      set
      {
        this._autoRes = value;
        this.ShowAlert("Auto Resource " + (this._autoRes ? "ON" : "OFF"));
      }
    }

    public bool AutoJetti
    {
      get
      {
        return this._autoJetti;
      }
      set
      {
        this._autoJetti = value;
        this.ShowAlert("Auto Jettison " + (this._autoJetti ? "ON" : "OFF"));
      }
    }

    public int AutoRjPhase
    {
      get
      {
        return this._autoRjPhase;
      }
      set
      {
        if (this._autoRjPhase >= 0 && this._autoRjPhase < 5)
        {
          this._autoRjPhase++;

        }
        else
        {
          this._autoRjPhase = 0;
        }
        this.ShowAlert("AutoRjPhase set to: " + (this.AutoRjPhase) + ": " + rjDict[(this._autoRjPhase) + 1]);
      }
    }

    public int MySpeed
    {
      get
      {
        return this._MySpeed;
      }
      set
      {
        this._MySpeed = this._myShip.MoveSpeedMS;
      }
    }

    public bool AutoHarvest
    {
      get
      {
        return this._autoHarvest;
      }
      set
      {
        if (this._myShip.Special.Class != SpecialClass.PROSPECTOR)
        {
          this._autoHarvest = false;
          this.ShowAlert("Equip a PROSPECTOR! ");
        }
        else
        { 
          this._autoHarvest = value;
          this.AutoGun = true;
          this.AutoScrap = true;
          this.AutoSell = true;
          this.MinimumMetals = this._myShip.ResourceCapacity;
          this.AutoRes = true;
          this.AutoMoney = true;
          GameState.InvasionFreeRouting = true;
          this.AutoSelfHeal = true;
          this.SelfHealAmount = 5000;
          this.Invasions = true;
        }
        this.ShowAlert("Auto Harvest " + (this._autoHarvest ? "ON" : "OFF"));
      }
    }

    public bool Invasions
    {
      get
      {
        return this._invasions;
      }
      set
      {
        this._invasions = value;
        this.ShowAlert("Invasions " + (this._invasions ? "ON" : "OFF"));
      }
    }

    public bool AutoMindSurge
    {
      get
      {
        return this._autoMindSurge;
      }
      set
      {
        this._autoMindSurge = value;
        this.ShowAlert("Auto MindSurge " + (this._autoMindSurge ? "ON" : "OFF"));
      }
    }

    public bool AutoHook
    {
      get
      {
        return this._autoHook;
      }
      set
      {
        this._autoHook = value;
        this.ShowAlert("Auto Hook " + (this._autoHook ? "ON" : "OFF"));
      }
    }

    public bool AutoEngage
    {
      get
      {
        return this._autoEngage;
      }
      set
      {
        this._autoEngage = value;
        this.ShowAlert("Auto Engage " + (this._autoEngage ? "ON" : "OFF"));
      }
    }

    public bool SMS
    {
      get
      {
        return GameState.HitSMS;
      }
      set
      {
        GameState.HitSMS = value;
        this.ShowAlert("Hit SMS " + (GameState.HitSMS ? "ON" : "OFF"));
      }
    }

    public bool AutoSelfHeal
    {
      get
      {
        return this._autoSelfHeal;
      }
      set
      {
        this._autoSelfHeal = value;
        this.ShowAlert("Auto Self Heal " + (this._autoSelfHeal ? "ON" : "OFF"));
      }
    }

    public bool Realism
    {
      get
      {
        return this._realism;
      }
      set
      {
        this._realism = value;
        this.ShowAlert("Realism " + (this._realism ? "ON" : "OFF"));
      }
    }

    public bool AutoFriendHeal
    {
      get
      {
        return this._autoFriendHeal;
      }
      set
      {
        this._autoFriendHeal = value;
        this.ShowAlert("Auto Friend Heal " + (this._autoFriendHeal ? "ON" : "OFF"));
      }
    }

    public bool Record
    {
      get
      {
        return this.RecordKills;
      }
      set
      {
        this.RecordKills = value;
        this.ShowAlert("Recording Kills " + (this.RecordKills ? "ON" : "OFF"));
      }
    }

    public bool NBL
    {
      get
      {
        return GameState.NoBust;
      }
      set
      {
        GameState.NoBust = value;
        if (GameState.NoBust)
          GameState.NoBustList = NoBustRetrieval.GetNoBustList();
        this.ShowAlert("NoBust " + (GameState.NoBust ? "ON" : "OFF"));
      }
    }

    public bool RouteMode
    {
      get
      {
        return GameState.InvasionFreeRouting;
      }
      set
      {
        GameState.InvasionFreeRouting = value;
        this.ShowAlert(GameState.InvasionFreeRouting ? "Invasion Free Routing" : "Normal Routing");
      }
    }

    public bool AutoGun
    {
      get
      {
        return this._autoGun;
      }
      set
      {
        this._autoGun = value;
        this.ShowAlert("Auto Gun " + (this._autoGun ? "ON" : "OFF"));
      }
    }

    public bool RoidChanges
    {
      get
      {
        return GameState.ShowAsteroidChanges;
      }
      set
      {
        GameState.ShowAsteroidChanges = value;
        this.ShowAlert("Show Asteroid Changes " + (GameState.ShowAsteroidChanges ? "ON" : "OFF"));
      }
    }

    public bool ShouldLog
    {
      get
      {
        return this.Panic || GameState.DoLog;
      }
      set
      {
        GameState.DoLog = value;
        this.ShowAlert("Logging " + (GameState.DoLog ? "ON" : "OFF"));
      }
    }

    public string AutoFeedTarget
    {
      get
      {
        return this._autofeedTarget;
      }
      set
      {
        this._autofeedTarget = value;
        this.ShowAlert("Auto Feed Target: " + (string.IsNullOrEmpty(this._autofeedTarget) ? "Rescues" : this._autofeedTarget));
      }
    }

    public bool AutoFeed
    {
      get
      {
        return this._autofeed;
      }
      set
      {
        this._autofeed = value;
        this.ShowAlert("Auto Feed " + (this._autofeed ? "ON" : "OFF"));
      }
    }

    public string BackupFeedTarget
    {
      get
      {
        return this._backupfeedTarget;
      }
      set
      {
        this._backupfeedTarget = value;
        this.ShowAlert("Backup Feed Target: " + (string.IsNullOrEmpty(this._backupfeedTarget)));
      }
    }
    public bool BackupFeed
    {
      get
      {
        return this._backupfeed;
      }
      set
      {
        this._backupfeed = value;
        this.ShowAlert("Backup Feed " + (this._backupfeed ? "ON" : "OFF"));
      }
    }

    public int SelfHealAmount
    {
      get
      {
        return this._autoSelfHealAmount;
      }
      set
      {
        this._autoSelfHealAmount = value;
        this.ShowAlert("Auto Self Heal Below " + (object) this._autoSelfHealAmount);
      }
    }

    public string FolTar
    {
      get
      {
        return this._foltar;
      }
      set
      {
        this._foltar = value;
        //this.ShowAlert("New driver: " + this._foltar);
      }
    }

    public int FriendHealAmount
    {
      get
      {
        return this._autoFriendHealAmount;
      }
      set
      {
        this._autoFriendHealAmount = value;
        this.ShowAlert("Auto Friend Heal Below " + (object) this._autoFriendHealAmount);
      }
    }

    public int MinimumMetals
    {
      get
      {
        return this._minimumMetals;
      }
      set
      {
        this._minimumMetals = value;
        this.ShowAlert("Minimum Metals: " + (object) this._minimumMetals);
      }
    }

    public int AutoHealTime
    {
      get
      {
        return this._autoHealTime;
      }
      set
      {
        this._autoHealTime = value;
        this.ShowAlert("Auto Heal Time: " + (object) this._autoHealTime + "ms");
      }
    }

    public int MinResTime
    {
      get
      {
        return this._MinResTime;
      }
      set
      {
        this._MinResTime = value;
        this.ShowAlert("Min RES Time: " + (object)this._MinResTime + "ms");
      }
    }

    public bool GrabIt
    {
      get
      {
        return this._grabit;
      }
      set
      {
        this._grabit = value;
        this.ShowAlert("Grab It: " + (this._grabit ? "ON" : "OFF"));
      }
    }

    // FKEY Level Toggle
    public bool FkeyLevel
    {
      get
      {
        return GameState._FkeyLevel;
      }
      set
      {
        GameState._FkeyLevel = value;
//        this.ShowAlert("FKEY LEVEL = " + GameState._FkeyLevel);
        if (GameState._FkeyLevel == false)
        {
          this.ShowAlert("FKEY LEVEL = 1");
        }
        else
        {
          this.ShowAlert("FKEY LEVEL = 2");
        }
        this.ShowAlert("Time: " + DateTime.Now);
        
      }
    }


    private void ToggleBool(string propertyName)
    {
      PropertyInfo property = typeof (GameState).GetProperty(propertyName);
      bool flag = (bool) property.GetValue((object) this, (object[]) null);
      property.SetValue((object) this, (object) !flag, (object[]) null);
    }

    public void Follow(string target)
    {
      if (!this._local.Values.Any<Entity>((Func<Entity, bool>) (x => x.Title.ToLower() == target.ToLower())))
        return;
      this.DoFollow((CombatEntity) this._local.Values.First<Entity>((Func<Entity, bool>) (x => x.Title.ToLower() == target.ToLower())));
    }

    public void Info(string propertyName)
    {
      GameState.SubtitleStat = propertyName;
      if (!string.IsNullOrEmpty(propertyName))
        return;
      this.ShowAlert("[b]Available Stats: " + string.Join(",", ((IEnumerable<PropertyInfo>) typeof (Ship).GetProperties()).Select<PropertyInfo, string>((Func<PropertyInfo, string>) (x => x.Name)).ToArray<string>()));
    }

    public void ResearchToggle(string researchTarget)
    {
      this._autoResearch = !this._autoResearch;
      this.ShowAlert("Auto Research " + (this._autoResearch ? "ON" : "OFF"));
      this._researchTarget = researchTarget;
    }

    public void ResPlanetToggle(string researchTarget)
    {
      this._autoResPlanet = !this._autoResPlanet;
      this.ShowAlert("Auto Res Planet " + (this._autoResPlanet ? "ON" : "OFF"));
      this._researchTarget = researchTarget;
    }

    public void SendDTeamMessage(string message)
    {
      if (!this.DTeam.Any<Ship>())
        return;
      this._unsentMessages = new Queue<Message>(this.DTeam.Select<Ship, Message>((Func<Ship, Message>) (x => new Message()
      {
        Player = x.Player.Name,
        Text = message
      })));
    }

    public void SortCommand(string command)
    {
      string str1 = command.Replace(":sort", "").Trim();
      if (string.IsNullOrEmpty(str1))
      {
        this.ShowAlert("[b]Available Stats: " + string.Join(",", ((IEnumerable<PropertyInfo>) typeof (Ship).GetProperties()).Select<PropertyInfo, string>((Func<PropertyInfo, string>) (x => x.Name)).ToArray<string>()));
        this.ShowAlert("[b]Sort order: asc, desc (default desc)");
      }
      else
      {
        string[] strArray = str1.Split(' ');
        string str2 = "desc";
        if (((IEnumerable<string>) strArray).Count<string>() > 1)
          str2 = strArray[1];
        if (!((IEnumerable<string>) strArray).Any<string>())
          return;
        this._local.OrderStat = strArray[0];
        this._local.SortOrder = str2;
        this.ShowAlert("[g]<b>Sorted Column D by " + this._local.OrderStat + " " + (this._local.SortOrder == "desc" ? "Descending" : "Ascending"));
      }
    }

    public IUserInterface UI
    {
      get
      {
        return this._ui;
      }
    }

    public INetwork Net
    {
      get
      {
        return this._net;
      }
    }

    public MusicMoodType Music
    {
      get
      {
        if (!this.IsLoggedIn || this._myLoc == null || (this._myPlayer == null || this._myShip == null) || this._myLoc.Class == SectorClass.PROTECTED)
          return MusicMoodType.Login;
        switch (this._myLoc.Status)
        {
          case SectorStatus.PRE_ENGAGE:
          case SectorStatus.ENGAGED:
          case SectorStatus.PIRATE:
          case SectorStatus.WYRD:
          case SectorStatus.HETEROCLITE:
            return MusicMoodType.Invasion;
          default:
            return MusicMoodType.Explore;
        }
      }
    }

    public AccountManager Accounts
    {
      get
      {
        return this._accounts;
      }
    }

    public LoginRequest Login
    {
      get
      {
        return this._login;
      }
    }

    public Account MyAccount
    {
      get
      {
        return this._myAccount;
      }
    }

    public AccountSettings MySettings
    {
      get
      {
        if (this._myAccount != null)
          return this._myAccount.Settings;
        return (AccountSettings) null;
      }
    }

    public ClientPlayer MyPlayer
    {
      get
      {
        return this._myPlayer;
      }
    }

    public Ship MyShip
    {
      get
      {
        return this._myShip;
      }
    }

    public Sector MyLoc
    {
      get
      {
        return this._myLoc;
      }
    }

    public CombatEntity MyAttackTarget
    {
      get
      {
        return this._myAttackTarget;
      }
    }

    public CombatEntity MyFollowTarget
    {
      get
      {
        return this._myFollowTarget;
      }
    }

    public LocalEntities Local
    {
      get
      {
        return this._local;
      }
    }

    public ClientBank Bank
    {
      get
      {
        return this._bank;
      }
    }

    public ClientBuyables Buyables
    {
      get
      {
        return this._buyables;
      }
    }

    public PlayerCollection Players
    {
      get
      {
        return this._players;
      }
    }

    public CorporationCollection Corporations
    {
      get
      {
        return this._corporations;
      }
    }

    public AllianceCollection Alliances
    {
      get
      {
        return this._alliances;
      }
    }

    public AuctionCollection Auctions
    {
      get
      {
        return this._auctions;
      }
    }

    public EntityCollection Entities
    {
      get
      {
        return this._entities;
      }
    }

    public ClientMap Map
    {
      get
      {
        return this._map;
      }
    }

    public ShipBank Ships
    {
      get
      {
        return this._ships;
      }
    }

    public CourseController Course
    {
      get
      {
        return this._course;
      }
    }

    public TimeComponent AttackCooldown
    {
      get
      {
        return this._attackCooldown;
      }
    }

    public TimeComponent MoveCooldown
    {
      get
      {
        return this._moveCooldown;
      }
    }

    public TimeComponent RepairCooldown
    {
      get
      {
        return this._repairCooldown;
      }
    }

    public TimeComponent HarvestCooldown
    {
      get
      {
        return this._harvestCooldown;
      }
    }

    public TimeComponent EarthJumpTime
    {
      get
      {
        return this._earthJumpTime;
      }
    }

    public CasinoManager Casino
    {
      get
      {
        return this._casino;
      }
    }

    public long WorldTimeMS
    {
      get
      {
        return this._worldTimeMS;
      }
    }

    public string CourseStatusHeader
    {
      get
      {
        if (this._myShip.Grappled)
          return "!GRAPPLED!";
        if (this._myShip.Stunned)
          return "!! STUN !!";
        if (this._course.Count == 0)
          return string.Empty;
        return !this._course.Paused ? "| MOVING |" : "| PAUSED |";
      }
    }

    public string CourseStatus
    {
      get
      {
        if (this.SeedSave && this._savedSeeds.Count > 0)
          return string.Format("{0} seeds", (object) this._savedSeeds.Count);
        if (this._course.Count == 0)
          return string.Empty;
        StringBuilder sb = new StringBuilder(200);
        if (this._course.Count != 1)
        {
          sb.Append(this._course.Count);
          sb.Append(" Jumps to ");
        }
        else
          sb.Append("1 Jump to ");
        this._course.Destination.AppendName(sb);
        return sb.ToString();
      }
    }

    public string FollowStatusHeader
    {
      get
      {
        return string.Format("{0} DPS", (object) this._local.FriendlyShips.Sum<Ship>((Func<Ship, double>) (x => x.DPS)).ToString("0"));
      }
    }

    public string FollowStatus
    {
      get
      {
        return string.Format("{0}:{1} stack, {2}/{3} metals{4}", (object) this._local.FriendlyShipCount, (object) this._local.EnemyShipCount, (object) this._local.FriendlyShips.Sum<Ship>((Func<Ship, int>) (x => (int) x.Metals)), (object) this._local.FriendlyShips.Sum<Ship>((Func<Ship, int>) (x => x.ResourceCapacity)), this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>) (x => (x.Engine == null || x.Engine.Class != EngineClass.STEALTH) && x.Class != ShipClass.WyrdAssassin)) > 0 ? (object) " NOSTEALTH" : (object) "");
      }
    }

    public string AttackStatusHeader
    {
      get
      {
        if (this._myShip.Grappled)
          return "!GRAPPLED!";
        if (this._myShip.Stunned)
          return "!! STUN !!";
        if (this._myAttackTarget != null)
          return "| ATTACK |";
        return string.Empty;
      }
    }

    public string AttackStatus
    {
      get
      {
        if (!this._buyables.SuspendSuperCharge || !this._buyables.SuspendTactics)
          return string.Format("{0} {1} ON", this._buyables.SuspendSuperCharge ? (object) "" : (object) "SC", this._buyables.SuspendTactics ? (object) "" : (object) "Tactics");
        if (this._myAttackTarget == null)
        {
          if (!this._attackCooldown.IsFinished)
            return "No Target (" + this._attackCooldown.RemainingSeconds.ToString("0") + ")";
          return string.Empty;
        }
        StringBuilder sb = new StringBuilder(200);
        this._myAttackTarget.AppendName(sb, false);
        if (!this._attackCooldown.IsFinished)
        {
          sb.Append(" (");
          sb.Append(this._attackCooldown.RemainingSeconds.ToString("0"));
          sb.Append(")");
        }
        return sb.ToString();
      }
    }

    public string HarvestStatus
    {
      get
      {
        if (this._harvestCooldown.IsFinished)
          return (string) null;
        StringBuilder stringBuilder = new StringBuilder(200);
        string str1 = "Next Harvest in ";
        stringBuilder.Append(str1);
        string str2 = this._harvestCooldown.RemainingSeconds.ToString("0");
        stringBuilder.Append(str2);
        string str3 = "s";
        stringBuilder.Append(str3);
        return stringBuilder.ToString();
      }
    }

    public GameState(IUserInterface ui, INetwork net, AccountManager accounts, LoginRequest login)
      : this()
    {
      if (ui == null)
        throw new ArgumentNullException(nameof (ui));
      if (net == null)
        throw new ArgumentNullException(nameof (net));
      if (accounts == null)
        throw new ArgumentNullException(nameof (accounts));
      if (login == null)
        throw new ArgumentNullException(nameof (login));
      this._ui = ui;
      this._net = net;
      this._accounts = accounts;
      this._login = login;
    }

    public GameState(IUserInterface ui, AccountManager accounts, LoginRequest login)
      : this()
    {
      if (ui == null)
        throw new ArgumentNullException(nameof (ui));
      if (accounts == null)
        throw new ArgumentNullException(nameof (accounts));
      if (login == null)
        throw new ArgumentNullException(nameof (login));
      this._ui = ui;
      this._net = (INetwork) new ClientNet();
      this._accounts = accounts;
      this._login = login;
    }

    private GameState()
    {
      this._local = new LocalEntities(this);
      this._buyables = new ClientBuyables((IGameState) this);
      this._players = new PlayerCollection((IGameState) this);
      this._corporations = new CorporationCollection((IGameState) this);
      this._alliances = new AllianceCollection((IGameState) this);
      this._auctions = new AuctionCollection((IGameState) this);
      this._course = new CourseController((IGameState) this);
      this._casino = new CasinoManager((IGameState) this);
      this._attackCooldown = new TimeComponent((IGameState) this);
      this._moveCooldown = new TimeComponent((IGameState) this);
      this._repairCooldown = new TimeComponent((IGameState) this);
      this._harvestCooldown = new TimeComponent((IGameState) this);
      this._earthJumpTime = new TimeComponent((IGameState) this);
      this.KillsAgainst = 0;
      this.KillsFor = 0;
      this.TimeStamp = true;
    }

    public string GetNextReply()
    {
      if (GameState._lastPrivates.Count <= 0)
        return string.Empty;
      if (GameState._lastPrivateIndex >= GameState._lastPrivates.Count - 1)
        GameState._lastPrivateIndex = 0;
      else
        ++GameState._lastPrivateIndex;
      return "/" + GameState._lastPrivates[GameState._lastPrivateIndex] + " ";
    }

    public void ShowPopup(string text, bool canClose)
    {
      this._ui.Show(new PopupWindowEventArgs((IGameState) this, text, canClose));
      if (!canClose)
        return;
      this.ShowAlert("[y]" + text);
    }

    public void ShowAlert(string text)
    {
      this.unsentAlerts.Enqueue(text);
    }

    public void AlertsCheck()
    {
      if (!this.unsentAlerts.Any<string>())
        return;
      this._ui.Show(new ChatEventArgs((IGameState) this, this.unsentAlerts.Dequeue(), string.Empty, string.Empty, ChatType.ALERT));
    }

    public void ShowEvent(string text)
    {
      this._ui.Show(new ChatEventArgs((IGameState) this, text, string.Empty, string.Empty, ChatType.EVENT));
    }

    public void ShowAcceptDecline(string title, string text, INetworkData accept, INetworkData decline)
    {
      this._ui.Show(new AcceptDeclineWindowEventArgs((IGameState) this, title, text, accept, decline));
    }

    public void Play(SoundEffectType sound)
    {
      this.Play(sound, 1f, 0.0f);
    }

    public void Play(SoundEffectType sound, float rate)
    {
      this.Play(sound, rate, 0.0f);
    }

    public void Play(SoundEffectType sound, float rate, float delay)
    {
      this._ui.Show(new SoundEventArgs((IGameState) this, new EventSoundEffect(sound, rate, delay)));
    }

    public void OnUpdate()
    {
      if (this._net.Connected)
      {
        int tickCount = Environment.TickCount;
        int num = tickCount - this._lastTicks;
        this._lastTicks = tickCount;
        if (num > 0 && num <= 10000)
        {
          this._worldTimeMS = this._worldTimeMS + (long) num;
          this._secondTimeMS = this._secondTimeMS + num;
          if (this._secondTimeMS >= 1000)
          {
            this._secondTimeMS = this._secondTimeMS - 1000;
            this._map.OnSecond();
          }
        }
        for (Command nextCommand = this._net.GetNextCommand(); nextCommand != null; nextCommand = this._net.GetNextCommand())
        {
          if (Log.Debug)
            Log.D((object) nextCommand, "Execute", "");
          nextCommand.Execute(this);
        }
        this._net.TryDoAlive((IGameState) this);
        this.UpdateAttack();
        this._course.OnUpdate();
        this.DoCustomActions();
        this.AlertsCheck();
      }
      else
        this.Disconnect("Connection Lost");
    }

    private static bool NeedsFeeding(Ship x)
    {
      return (int) x.Metals != x.ResourceCapacity;
    }

    private static Func<EquipmentItem, bool> IsScrappable()
    {
      return (Func<EquipmentItem, bool>) (x => x.Rarity == ItemRarity.COMMON && (x.Type == ItemType.ARMOR || x.Type == ItemType.STORAGE || x.Type == ItemType.WEAPON || x.Type == ItemType.HARVESTER));
    }

    private static Func<EquipmentItem, bool> IsDecon()
    {
      return new Func<EquipmentItem, bool>(GameState.IsDecon);
    }

    private static bool IsDecon(EquipmentItem x)
    {
      return !x.BindOnEquip && x.Rarity == ItemRarity.UNCOMMON && (x.Type == ItemType.ARMOR || x.Type == ItemType.STORAGE || x.Type == ItemType.WEAPON || x.Type == ItemType.HARVESTER);
    }

    // Experimental

    [DllImport("user32.dll")]
    public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

    [DllImport("user32.dll")]
    private static extern IntPtr GetForegroundWindow();

    //PdW Get Window Text
    [DllImport("user32.dll")]
    private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
    //

    //PdW Get Window Coords
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
      public int Left;        // x position of upper-left corner  
      public int Top;         // y position of upper-left corner  
      public int Right;       // x position of lower-right corner  
      public int Bottom;      // y position of lower-right corner  
    }
    //

    //PdW Move Window
    [DllImport("User32.dll")]
    private static extern bool MoveWindow(IntPtr handle, int x, int y, int width, int height, bool redraw);

    [DllImport("User32.dll", EntryPoint = "SetWindowPos")]
    private static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);
    //

    void keyboard_KeyBoardKeyPressed(object sender, EventArgs e)
    {
      IntPtr hwnd = GetForegroundWindow();
      uint pid;
      GetWindowThreadProcessId(hwnd, out pid);
      GetActiveWindow();
      if (keyboard.keydown == true && TIBInstanceID == pid)  // THESE ARE LOCAL TO THE INSTANCE OF TIB RUNNING
      {
        switch (keyboard.keycode)
        {
          case 112: // F1
            // default for all TRUE
            this.AutoGun = true;
            this.AutoSell = true;
            this.AutoScrap = true;
            this.AutoRes = true;
            if (this._myShip.Special.Class == SpecialClass.TECHNICIAN)
            {
              this.ShowAlert("[g]East Driver");
              this.AutoDegrapple = true;
              this.AutoFriendHeal = true;
              this.FriendHealAmount = 6500;
              this.AutoSelfHeal = true;
              this.SelfHealAmount = 6500;
              this.AutoMoney = true;
              this.MinimumMetals = this._myShip.ResourceCapacity;
              this._shipsInFleet = this._local.FriendlyShipCount;
              this.Invasions = true;
//              this.AutoHealTime = 375;
            }
            else if (this._myShip.Special.Class != SpecialClass.PROSPECTOR)
            {
              if (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Special.Class == SpecialClass.TECHNICIAN)) > 0)  // tech in sector
              {
                this.ShowAlert("[g]East Gun");
                this._shipsInFleet = this._local.FriendlyShipCount;
                this.ShowAlert("Ships: " + this._shipsInFleet);
                this._driver = this.MyFollowTarget != null ? this.MyFollowTarget : (CombatEntity)this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => x.Special != null && x.Special.Class == SpecialClass.TECHNICIAN));
                if (this.MyFollowTarget == null)
                {
                  this.DoFollow(this._driver);
                }
                this.AutoFeedTarget = this._driver.Title;
                this.AutoFeed = true;
              }
              else
              {
                this.ShowAlert("[g]Solo Gun");
              }
              this._attackOrder = (NpcAttackOrder)new TopDown();
              this.MinimumMetals = this._myShip.ResourceCapacity;
              this.AutoSelfHeal = true;
              this.SelfHealAmount = 3250;
            }
            else
            {
              this.ShowAlert("[g]HARVEST MODE");
              this._autoHarvest = true;
              this.MinimumMetals = this._myShip.ResourceCapacity;
              this.AutoMoney = true;
              GameState.InvasionFreeRouting = true;
              this.AutoSelfHeal = true;
              this.SelfHealAmount = 5000;
              this.Invasions = true;
            }
            break;

          case 113: // F2
            if (GameState._FkeyLevel == false)
            {
              AutoMoney = !_autoMoney;
            }
            else
            {
              GrabIt = !_grabit;
            }
              
            break;

          case 114: // F3
            if (GameState._FkeyLevel == false)
            {
              AutoRes = !_autoRes;
            }
            else
            {
              AutoFarmBlackGun();
            }
            break;

          case 115: // F4
            if (GameState._FkeyLevel == false)
            {
              SeedSave = !_seedSave;
            }
            else
            {
              AutoFarmBlackDriver();
            }
            
            break;

          case 116: // F5
            if (GameState._FkeyLevel == false)
            {
              AutoGun = !_autoGun;
            }
            else
            {
              RouteMode = !RouteMode;
            }
            break;

            //case 123: // F12 under alt so out of stack ships can be fixed.
            if (GameState._FkeyLevel == true)
            {
              AutoSellPhase++;
            }
            break;
          //  AutoSellPhase++;
          //  break;

          case 96: // numpad zero for testing new options.
            this.ShowAlert("[g]FollowTarget: " + this.MyFollowTarget);
            this.ShowAlert("[g]FeedTarget: " + this.AutoFeedTarget);
            this.ShowAlert("[g]Driver: " + this._driver);
            //this.ShowAlert("[r]Process Name: " + TIBInstanceName);
            RECT t2;
            GetWindowRect(GetForegroundWindow(), out t2);
            this.ShowAlert("[r]Upper left corner x: " + t2.Left);
            this.ShowAlert("[r]Upper left corner y: " + t2.Top);
            this.ShowAlert("[r]Bottom right corner x: " + t2.Right);
            this.ShowAlert("[r]Bottom right corner y: " + t2.Bottom);
            this.ShowAlert("[r]Width: " + (t2.Right - t2.Left));
            this.ShowAlert("[r]Height: " + (t2.Bottom - t2.Top));

            break;

          case 192: // tilde / grave key
            this.ShowAlert("Function Key Commands");
            this.ShowAlert("F1  = Set tech/farm/roid mode (eastdriver/eastgun)");
            this.ShowAlert("F2  = Toggle AutoMoney");
            this.ShowAlert("F3  = Toggle AutoRes");
            this.ShowAlert("F4  = Toggle SeedSave");
            this.ShowAlert("F5  = Toggle AutoGun (single ship)");
            this.ShowAlert("F6  = ALL Players Cycle GUNMODE");
            this.ShowAlert("F7  = ALL Players Jettison Mets (Excl Tech)");
            this.ShowAlert("F8  = ALL Players Toggle AutoGun");
            this.ShowAlert("F9  = Toggle Routemode");
            this.ShowAlert("F10 = ALL Players RJ");
            this.ShowAlert("F11 = ALL Players PANIC");
            this.ShowAlert("F12 = ALL Players - Cycle Sell phases");
            this.ShowAlert("`/~ = FKey Help");
            this.ShowAlert("TAB = Toggle F-Key Level");
            this.ShowAlert("The following commands are on LEVEL 2"); 
            this.ShowAlert("To access lvl 2 commands you have to press the TAB key!");
            this.ShowAlert("Level 2 will remain accessible for 10 seconds after pressing TAB!");
            this.ShowAlert("Then it will revert back to LEVEL 1!");
            this.ShowAlert("ALT + F2  = Toggle GrabIt");
            this.ShowAlert("ALT + F3  = Toggle Autofarm GUN");
            this.ShowAlert("ALT + F4  = Toggle Autofarm DRIVER");
            this.ShowAlert("ALT + F5  = PVP mode");
            break;

          case 97: // numpad 1
            this.ShowAlert("Character Name: " + this._myPlayer.Name);
            //pid
            //            Rectangle rect1;
            //            GetWindowRect(GetForegroundWindow(), out rect1);
            switch (this._myPlayer.Name)
            {
              case "Etherealwolf":
                win_x = 581;
                win_y = 0;
                win_w = 604;
                win_h = 527;
                break;
              case "NeoWolf":
                win_x = -7;
                win_y = 0;
                win_w = 603;
                win_h = 528;
                break;
              case "XenoWolf":
                win_x = -7;
                win_y = 520;
                win_w = 603;
                win_h = 528;
                break;
              case "WolfAvenger":
                win_x = 581;
                win_y = 520;
                win_w = 604;
                win_h = 527;
                break;
              case "SavageWolf":
                win_x = 1170;
                win_y = 0;
                win_w = 757;
                win_h = 1047;
                break;
              default:
                break;
            }
            if (win_w != 0 && win_h != 0) // Check if Width and Height are not zero
            {
              //MoveWindow(hwnd, win_x, win_y, win_w, win_h, true);
              MoveActiveWindow();
            }
            break;

          case 9: // TAB Key
            this.ShowAlert("TAB KEY PRESSED");
            FkeyLevel = !FkeyLevel;  // TOGGLE FKEY level
            TabKeyPress = DateTime.Now;
            break;

          default:
            //                        this.ShowAlert("No Command");
            break;
        }
      }
      //if (keyboard.keydown == true)   // THESE ARE GLOBAL TO ALL OF THE INSTANCE OF TIB RUNNING
      if (keyboard.keydown == true && TIBInstanceName == "The Infinite Black")
      {
        switch (keyboard.keycode)
        {
          case 117: // F6
            GunMode(); // CYCLE GUN MODES
            break;

          case 118: // F7  JETTISON
            if (_myShip.Metals > 0 && _myShip.Special.Class != SpecialClass.TECHNICIAN)
            {
              JettiTrigger = DateTime.Now;
              _RealismMS_JETTI = _rand.Next(400, 2000);
              AutoJetti = true;
            }
            break;

          case 119: // F8  AUTOGUN - TOGGLE on / off
            AutoGun = !_autoGun;
            break;

          case 120: // F9
            AutoGun = false;
            AutoSelfHeal = true;
            AutoRes = true;
            AutoHook = true;
            break;

          case 121: // F10
            this.ShowAlert("RJ ALL PLAYERS");
            AutoRjPhase = 1;
            break;

          case 122: // F11
            this.ShowAlert("PANIC KEY PRESSED");
            Panic = true;
            break;

          case 123: // F12
            AutoSellPhase++;
            break;

          default:
            //                        this.ShowAlert("NOTHING PRESSED");
            break;
        }
      }

    }

    private void GetActiveWindow()
    {
        const int nChars = 256;
        IntPtr handle;
        StringBuilder Buff = new StringBuilder(nChars);
        handle = GetForegroundWindow();
        if (GetWindowText(handle, Buff, nChars) > 0)
        {
          TIBInstanceName = Buff.ToString();
        }
    }

    private void MoveActiveWindow()
    {
      IntPtr handle2;
      handle2 = GetForegroundWindow();
      MoveWindow(handle2, win_x, win_y, win_w, win_h, true);
      //RECT t3;
      //GetWindowRect(GetForegroundWindow(), out t3);

      //SetWindowPos(hwnd, 0, win_x, win_y, win_h, win_w, 0x0010);
    }
    public void Connect(sbyte[] key)
    {
      // Experimental
      keyboard = new KeyboardInput();
      keyboard.KeyBoardKeyPressed += keyboard_KeyBoardKeyPressed;
      TIBInstanceID = Process.GetCurrentProcess().Id;

      // END Experimental

      string deviceId = this._login.GetDeviceID();
      string deviceType = this._login.GetDeviceType();
      if (key == null)
        key = this._lastKey;
      else
        this._lastKey = key;
      this._login.Name = Util.Regex(this._login.Name, true, true, false);
      if (this._login.Name.Length <= 0 || this._login.Name.Length > 12)
        this.OnLoginFailed("Name must be between 1 and 12 letters or numbers!");
      else if (this._login.Password.Length <= 1 || this._login.Password.Length > 64)
        this.OnLoginFailed("Password must be between 2 and 64 characters!");
      else if (deviceId.Length <= 5)
        this.OnLoginFailed("Device ID Invalid!\nPlease report error to Contact@Spellbook.com");
      else if (deviceType.Length <= 5)
      {
        this.OnLoginFailed("Device Type Invalid!\nPlease report error to Contact@Spellbook.com");
      }
      else
      {
        if (!this._net.Connected)
        {
          this.ShowPopup("Connecting to server...", false);
          int num = 50;
          Socket socket = SocketHelper.Connect(this._login.ServerIP, this._login.ServerPort, 5000);
          if (socket == null)
            Thread.Sleep(100);
          while (socket == null && num > 0)
          {
            --num;
            socket = SocketHelper.Connect(this._login.ServerIP, this._login.ServerPort, 5000);
            if (socket == null)
              Thread.Sleep(100);
          }
          if (socket != null)
            this._net.Connect(socket);
        }
        if (this._net.Connected)
        {
          if (this._net.IV != null)
          {
            this.ShowPopup(this._login.IsNewAccount ? "Creating new account..." : "Logging in...", false);
            this._net.Send(this._login.Serialize(this._net.IV, key, deviceId, deviceType));
          }
          else
            this.ShowPopup("Waiting for handshake...", false);
        }
        else
          this.OnLoginFailed("Connection Failed - No Network Available");
      }
    }

    public void Disconnect(string reason)
    {
      this.IsLoggedIn = false;
      this.RefreshMyAccount();
      this._myAccount = (Account) null;
      this._myPlayer = (ClientPlayer) null;
      this._myShip = (Ship) null;
      this._myLoc = (Sector) null;
      this._net.Disconnect(reason);
      this._ui.Show(new DisconnectEventArgs((IGameState) this, reason));
    }

    public void DoChat(string text, ChatType mode)
    {
      if (this.CustomCommands(text) || !mode.CanClientSend())
        return;
      text = text.Trim();
      if (string.IsNullOrEmpty(text) || this.TryDoChatCommand(text) || this.TryDoPrivateMessage(text))
        return;
      this._net.Send(RequestChat.Create(text, mode));
    }

    public void DoPrivateChat(string text, string playerName)
    {
      text = text.Trim();
      if (string.IsNullOrEmpty(text))
        return;
      playerName = playerName.Trim();
      if (string.IsNullOrEmpty(playerName) || this.TryDoChatCommand(text) || this.TryDoPrivateMessage(text))
        return;
      this._net.Send(RequestChat.CreatePrivate(text, playerName));
    }

    public void DoPurchase(PurchaseItem item)
    {
      if (item.Cost <= this.Bank.BlackDollars)
      {
        if (item == PurchaseItem.EarthJump)
        {
          if (!this._myLoc.IsSol)
          {
            this.ShowAlert("[g]Requesting Earth Jump...");
            this._net.Send(RequestEarthJump.Create(false));
          }
          else
            this.ShowAlert("[y]Earth Jump Failed - You're already at Earth!");
        }
        else if (item == PurchaseItem.CorpJump)
        {
          Garrison garrison = this._local.Garrison;   // DO NOT JUMP WHEN ON OUR OWN CLUSTER
          if (garrison != null && garrison.Relation == RelationType.FRIEND)
            return;
          this.ShowAlert("[g]Requesting Corp Jump...");
          this._net.Send(RequestEarthJump.Create(true));
        }
        else if (item == PurchaseItem.FullRepair)
        {
          if (this._repairCooldown.IsFinished)
          {
            this._repairCooldown.Set(3000L);
            this.ShowAlert("[g]Requesting Full Repair...");
            this._net.Send(RequestRepairBlackDollar.Create());
          }
          else
            this.ShowAlert("[y]Repair Failed - Wait " + this._repairCooldown.RemainingSecondsString + " seconds!");
        }
        else if (item.RequireConfirm)
        {
          StringBuilder stringBuilder = new StringBuilder(200);
          stringBuilder.Append("Purchase '");
          stringBuilder.Append(item.Name);
          stringBuilder.Append("' for ");
          stringBuilder.Append(item.Cost.ToString("#,#"));
          stringBuilder.Append(" BlackDollars");
          this.ShowAcceptDecline("Purchase - " + item.Name, stringBuilder.ToString(), item.Data, (INetworkData) null);
        }
        else
        {
          this.ShowAlert("[g]Requesting '" + item.Name + "'...");
          this._net.Send(item.Data);
        }
      }
      else
        this.ShowAlert("[y]" + item.Name + " Failed - Requires " + item.Cost.ToString("#,#") + " BlackDollars!");
    }

    public void DoLoot(CargoEntity target)
    {
      if (target != null && target.CanLoot(this._myShip))
        this._net.Send(RequestLoot.Create(target));
      else
        this.ShowAlert("[y]Loot Failed - Storage is full!");
    }

    public void DoHarvest(Asteroid target)
    {
      if (target == null)
        return;
      if (!this._harvestCooldown.IsFinished)
        this.ShowAlert("[y]Harvest Failed - Wait " + this._harvestCooldown.RemainingSecondsString + " seconds!");
      else
        this._net.Send(RequestHarvest.Create(target));
    }

    public void DoRepair(PlayerCombatEntity target)
    {
      if (target != null && target.Type.CanBeRepaired())
      {
        if (!target.Grappled && (int) this._myShip.Metals <= 0)
          this.ShowAlert("[y]Repair Failed - Requires at least 1 Metal!");
        else if (!target.Grappled && target.Hull >= target.MaxHull)
          this.ShowAlert("[y]Repair Failed - Already fully repaired!");
        else
          this._net.Send(RequestRepairTarget.Create((CombatEntity) target));
      }
      else
        this.ShowAlert("[y]Repair Failed - Invalid target!");
    }

    public void DoRepairWith(StarPort starport)
    {
      if (starport != null)
      {
        if (this.Bank.Credits > 0)
          this._net.Send(RequestRepairStarPort.Create());
        else
          this.ShowAlert("[y]Repair Failed - Requires at least $1 Credit!");
      }
      else
        this.ShowAlert("[y]Repair Failed - There is no starport nearby!");
    }

    public void DoAttack(CombatEntity target)
    {
      if (target == this._myShip || target == this._myAttackTarget)
        target = (CombatEntity) null;
      if (target == null)
      {
        this._net.Send(RequestAttack.Create((CombatEntity) null));
        this.OnAttack((CombatEntity) null);
        this.Play(SoundEffectType.ui_off);
      }
      else if (target.ID == 0)
        this.ShowAlert("[y]Why would you want to destroy Earth?! You monster!");
      else if (!target.Type.CanBeAttacked())
        this.ShowAlert("[y]You can't attack that!");
      else if (target.Relation != RelationType.ENEMY)
        this.ShowAlert("[y]You can only attack hostile targets!");
      else if (this._myLoc.Status == SectorStatus.PRE_ENGAGE)
        this.ShowAlert("[y]Sector Enagement will begin soon!");
      else if (target.Type == EntityType.PLANET && this._local.Contains(EntityType.GARRISON))
        this.ShowAlert("[y]Planet is protected by a Garrison!");
      else if (target.Type == EntityType.PLANET && this._local.Contains(EntityType.DEFENSE_PLATFORM))
        this.ShowAlert("[y]Planet is protected by a Defense Platform!");
      else if (target.Type == EntityType.PLANET && this._local.Contains(EntityType.INTRADICTOR))
        this.ShowAlert("[y]Planet is protected by an Intradictor!");
      else if (target.Type == EntityType.GARRISON && this._local.Contains(EntityType.DEFENSE_PLATFORM))
        this.ShowAlert("[y]Garrison is protected by a Defense Platform!");
      else if (target.Type == EntityType.GARRISON && this._local.Contains(EntityType.INTRADICTOR))
        this.ShowAlert("[y]Garrison is protected by an Intradictor!");
      else if (target.Type == EntityType.INTRADICTOR && this._local.Contains(EntityType.DEFENSE_PLATFORM))
      {
        this.ShowAlert("[y]Intradictor is protected by a Defense Platform!");
      }
      else
      {
        this._net.Send(RequestAttack.Create(target));
        this.OnAttack(target);
        if (this._attackCooldown.IsFinished)
          return;
        this.Play(SoundEffectType.ui_on);
      }
    }

    public void DoFollow(CombatEntity target)
    {
      if (target == this._myShip || target == this._myFollowTarget)
        target = (CombatEntity) null;
      if (target == null)
      {
        this._net.Send(RequestFollow.Create((CombatEntity) null));
        this.OnFollow((CombatEntity) null);
        this.Play(SoundEffectType.ui_off);
      }
      else
      {
        if (!target.Type.CanBeFollowed())
          return;
        this._net.Send(RequestFollow.Create(target));
        this._course.Pause();
        this.OnFollow(target);
        this.Play(SoundEffectType.ui_on);
      }
    }

    public void DoDevelopment(PlayerCombatEntity target, ResourceType resource)
    {
      if (target == null || !target.Type.CanBeDeveloped() || this._myShip.GetResourceCount(resource) <= 0)
        return;
      this._myShip.SetResourceCount(resource, (sbyte) 0);
      this._net.Send(RequestDevelopResources.Create(target.ID, resource));
    }

    public void DoDevelopment(PlayerCombatEntity target, int blackDollars)
    {
      if (target == null || !target.Type.CanBeDeveloped() || (blackDollars <= 0 || blackDollars > this._bank.BlackDollars))
        return;
      this._net.Send(RequestDevelopBlackDollars.Create(target.ID, blackDollars));
    }

    public void DoResourcesJettison(int organics, int gas, int metals, int radioactives, int darkMatter)
    {
      organics = organics < 0 ? 0 : (organics > (int) this._myShip.Organics ? (int) this._myShip.Organics : organics);
      gas = gas < 0 ? 0 : (gas > (int) this._myShip.Gas ? (int) this._myShip.Gas : gas);
      metals = metals < 0 ? 0 : (metals > (int) this._myShip.Metals ? (int) this._myShip.Metals : metals);
      radioactives = radioactives < 0 ? 0 : (radioactives > (int) this._myShip.Radioactives ? (int) this._myShip.Radioactives : radioactives);
      darkMatter = darkMatter < 0 ? 0 : (darkMatter > (int) this._myShip.DarkMatter ? (int) this._myShip.DarkMatter : darkMatter);
      if (organics <= 0 && gas <= 0 && (metals <= 0 && radioactives <= 0) && darkMatter <= 0)
        return;
      this._net.Send(RequestJettison.Create((sbyte) organics, (sbyte) gas, (sbyte) metals, (sbyte) radioactives, (sbyte) darkMatter));
      this._jettisonSector = this._myLoc;
    }

    public void DoResourcesTransfer(Ship target, int organics, int gas, int metals, int radioactives, int darkMatter)
    {
      if (target == null)
        return;
      organics = organics < 0 ? 0 : (organics > (int) this._myShip.Organics ? (int) this._myShip.Organics : organics);
      gas = gas < 0 ? 0 : (gas > (int) this._myShip.Gas ? (int) this._myShip.Gas : gas);
      metals = metals < 0 ? 0 : (metals > (int) this._myShip.Metals ? (int) this._myShip.Metals : metals);
      radioactives = radioactives < 0 ? 0 : (radioactives > (int) this._myShip.Radioactives ? (int) this._myShip.Radioactives : radioactives);
      darkMatter = darkMatter < 0 ? 0 : (darkMatter > (int) this._myShip.DarkMatter ? (int) this._myShip.DarkMatter : darkMatter);
      if (organics <= 0 && gas <= 0 && (metals <= 0 && radioactives <= 0) && darkMatter <= 0)
        return;
      this._net.Send(RequestTransfer.Create((sbyte) organics, (sbyte) gas, (sbyte) metals, (sbyte) radioactives, (sbyte) darkMatter, target));
    }

    public void DoResourcesSellAll()
    {
      this.DoResourcesSell((int) this._myShip.Organics, (int) this._myShip.Gas, (int) this._myShip.Metals, (int) this._myShip.Radioactives, (int) this._myShip.DarkMatter);
      this._lastRes = DateTime.Now;
    }

    public void DoResourcesSellAllExceptMetals()
    {
      this.DoResourcesSell((int) this._myShip.Organics, (int) this._myShip.Gas, 0, (int) this._myShip.Radioactives, (int) this._myShip.DarkMatter);
      this._lastRes = DateTime.Now; // IF ANYTHING IS BEING SOLD THEN DELAY FEED
    }

    public void DoResourcesSell(int organics, int gas, int metals, int radioactives, int darkMatter)
    {
      if (this._local.StarPort == null)
        return;
      organics = organics < 0 ? 0 : (organics > (int) this._myShip.Organics ? (int) this._myShip.Organics : organics);
      gas = gas < 0 ? 0 : (gas > (int) this._myShip.Gas ? (int) this._myShip.Gas : gas);
      metals = metals < 0 ? 0 : (metals > (int) this._myShip.Metals ? (int) this._myShip.Metals : metals);
      radioactives = radioactives < 0 ? 0 : (radioactives > (int) this._myShip.Radioactives ? (int) this._myShip.Radioactives : radioactives);
      darkMatter = darkMatter < 0 ? 0 : (darkMatter > (int) this._myShip.DarkMatter ? (int) this._myShip.DarkMatter : darkMatter);
      if (organics > 0 && gas > 0 && radioactives > 0 && darkMatter > 0)
      { // IF ANYTHING IS BEING SOLD THEN DELAY RES
        this._lastRes = DateTime.Now;
      }
      if (organics > 0)
        this._net.Send(RequestResourceSell.Create(ResourceType.ORGANIC, (sbyte) organics));
      if (gas > 0)
        this._net.Send(RequestResourceSell.Create(ResourceType.GAS, (sbyte) gas));
      if (metals > 0)
        this._net.Send(RequestResourceSell.Create(ResourceType.METAL, (sbyte) metals));
      if (radioactives > 0)
        this._net.Send(RequestResourceSell.Create(ResourceType.RADIOACTIVE, (sbyte) radioactives));
      if (darkMatter <= 0)
        return;
      this._net.Send(RequestResourceSell.Create(ResourceType.DARKMATTER, (sbyte) darkMatter));
    }

    public void DoResourcesBuy(int organics, int gas, int metals, int radioactives, int darkMatter)
    {
      if (this._local.StarPort == null)
        return;
      int resourceCapacity = this._myShip.ResourceCapacity;
      organics = organics < 0 ? 0 : (organics > resourceCapacity - (int) this._myShip.Organics ? resourceCapacity - (int) this._myShip.Organics : organics);
      gas = gas < 0 ? 0 : (gas > resourceCapacity - (int) this._myShip.Gas ? resourceCapacity - (int) this._myShip.Gas : gas);
      metals = metals < 0 ? 0 : (metals > resourceCapacity - (int) this._myShip.Metals ? resourceCapacity - (int) this._myShip.Metals : metals);
      radioactives = radioactives < 0 ? 0 : (radioactives > resourceCapacity - (int) this._myShip.Radioactives ? resourceCapacity - (int) this._myShip.Radioactives : radioactives);
      darkMatter = darkMatter < 0 ? 0 : (darkMatter > resourceCapacity - (int) this._myShip.DarkMatter ? resourceCapacity - (int) this._myShip.DarkMatter : darkMatter);
      if (organics > 0)
        this._net.Send(RequestResourceBuy.Create(ResourceType.ORGANIC, (sbyte) organics));
      if (gas > 0)
        this._net.Send(RequestResourceBuy.Create(ResourceType.GAS, (sbyte) gas));
      if (metals > 0)
        this._net.Send(RequestResourceBuy.Create(ResourceType.METAL, (sbyte) metals));
      if (radioactives > 0)
        this._net.Send(RequestResourceBuy.Create(ResourceType.RADIOACTIVE, (sbyte) radioactives));
      if (darkMatter <= 0)
        return;
      this._net.Send(RequestResourceBuy.Create(ResourceType.DARKMATTER, (sbyte) darkMatter));
    }

    public void DoItemJettison(EquipmentItem item)
    {
      if (item == null || !this._myShip.Inventory.Contains(item))
        return;
      if (!item.NoDrop)
      {
        INetworkData networkData = RequestItemJettison.Create(item);
        if (item.IsOfNote)
        {
          StringBuilder sb = new StringBuilder(200);
          sb.Append("Jettison ");
          item.AppendLongName(sb, false, true);
          this.ShowAcceptDecline("Item - Jettison", sb.ToString(), networkData, (INetworkData) null);
        }
        else
          this._net.Send(networkData);
      }
      else
        this.ShowAlert("Request Failed - Item is NO DROP");
    }

    public void DoItemSell(StarPort starport, EquipmentItem item)
    {
      if (item == null || !this._myShip.Inventory.Contains(item) && !this._bank.Items.Contains<EquipmentItem>(item))
        return;
      INetworkData networkData = RequestItemSell.Create(item, starport);
      if (item.IsOfNote)
      {
        StringBuilder sb = new StringBuilder(200);
        item.AppendLongName(sb, false, true);
        sb.Append("\nScrap for $");
        sb.Append(item.StarPortSellValue.ToString("#,0"));
        sb.Append(" Credits");
        this.ShowAcceptDecline("Item - StarPort - Sell", sb.ToString(), networkData, (INetworkData) null);
      }
      else
        this._net.Send(networkData);
    }

    public void DoItemBuy(StarPort starport, EquipmentItem item, CurrencyType currency)
    {
      if (item == null || starport == null || !starport.Inventory.Contains(item))
        return;
      StringBuilder sb = new StringBuilder(200);
      item.AppendLongName(sb, false, true);
      INetworkData accept;
      if (currency != CurrencyType.Credits)
      {
        if (currency != CurrencyType.BlackDollars)
          return;
        sb.Append("\nBuy for ");
        sb.Append(item.ActualBlackDollarValue.ToString("#,0"));
        sb.Append(" BlackDollars");
        accept = RequestItemBuy.Create(item, starport, true);
      }
      else
      {
        sb.Append("\nBuy for $");
        sb.Append(item.ActualCreditValue.ToString("#,0"));
        sb.Append(" Credits");
        accept = RequestItemBuy.Create(item, starport, false);
      }
      this.ShowAcceptDecline("Item - StarPort - Buy", sb.ToString(), accept, (INetworkData) null);
    }

    public void DoItemToBank(EquipmentItem item)
    {
      if (item == null || !this._myShip.Inventory.Contains(item))
        return;
      if (this._bank.ItemCount < this._buyables.BankCap)
        this._net.Send(RequestItemMoveToBank.Create(item));
      else
        this.ShowAlert("[y]Request Failed - Bank is full!");
    }

    public void DoItemFromBank(EquipmentItem item)
    {
      if (item == null || !this._bank.Items.Contains<EquipmentItem>(item))
        return;
      if (this._myShip.Inventory.Count < this._buyables.InventoryCap)
        this._net.Send(RequestItemRemoveFromBank.Create(item));
      else
        this.ShowAlert("[y]Request Failed - Inventory is full!");
    }

    public void DoItemToGarrison(EquipmentItem item)
    {
      if (item == null || !this._myShip.Inventory.Contains(item))
        return;
      if (!item.NoDrop)
      {
        Garrison garrison = this._local.Garrison;
        if (garrison == null || garrison.Relation != RelationType.FRIEND)
          return;
        this._net.Send(RequestItemMoveToGarrison.Create(item));
      }
      else
        this.ShowAlert("Request Failed - Item is NO DROP");
    }

    public void DoItemFromGarrison(EquipmentItem item)
    {
      if (item == null)
        return;
      Garrison garrison = this._local.Garrison;
      if (garrison == null || garrison.Relation != RelationType.FRIEND || !garrison.Inventory.Contains(item))
        return;
      if (this._myShip.Inventory.Count < this._buyables.InventoryCap)
        this._net.Send(RequestItemRemoveFromGarrison.Create(item));
      else
        this.ShowAlert("[y]Request Failed - Inventory is full!");
    }

    public void DoItemEngineer(EngineeringType type, EquipmentItem item)
    {
      if (item == null || !item.CanUpgrade(type) || !this.Bank.Items.Contains<EquipmentItem>(item) && !this._myShip.Inventory.Contains(item))
        return;
      StringBuilder sb = new StringBuilder(200);
      item.AppendLongName(sb, false, true);
      sb.Append("\n");
      sb.Append(type.Name());
      sb.Append(" for ");
      if (type == EngineeringType.DECONSTRUCT)
      {
        sb.Append(item.DeconstructRewardPointValue.ToString("#,#.#####"));
        sb.Append(" :REWARD points");
      }
      else
      {
        sb.Append(item.GetEngineeringCost(type).ToString("#,#"));
        sb.Append(" BlackDollars");
      }
      INetworkData networkData = RequestItemEngineer.Create(item, type);
      if (GameState.IsDecon(item) && type == EngineeringType.DECONSTRUCT)
        this._net.Send(networkData);
      else
        this.ShowAcceptDecline("Item - Engineering - " + type.Name(), sb.ToString(), networkData, (INetworkData) null);
    }

    private void ForceItemEquip(EquipmentItem item)
    {
      if (item == null || !this._myShip.Inventory.Contains(item))
        return;
      EquipmentItem equippedItem = this._myShip.GetEquippedItem(item.Type);
      if (equippedItem != null)
        this.DoItemUnequip(equippedItem);
      this._net.Send(RequestItemEquip.Create(item));
    }

    public void DoItemEquip(EquipmentItem item)
    {
      if (item == null || !this._myShip.Inventory.Contains(item))
        return;
      EquipmentItem equippedItem = this._myShip.GetEquippedItem(item.Type);
      if (equippedItem != null)
        this.DoItemUnequip(equippedItem);
      INetworkData networkData = RequestItemEquip.Create(item);
      StringBuilder sb = new StringBuilder(200);
      if (item.BindOnEquip && !item.NoDrop)
      {
        sb.Append("[");
        item.AppendName(sb);
        sb.Append("] will Bind-On-Equip\nYou can never DROP, TRADE or SELL bound items!\nReally bind this item to your account?");
        this.ShowAcceptDecline("WARNING - Bind On Equip", sb.ToString(), networkData, (INetworkData) null);
      }
      else
      {
        sb.Append("[g]");
        sb.Append("Equipping ");
        item.AppendLongName(sb, true, true);
        sb.Append("...");
        this.ShowAlert(sb.ToString());
        this._net.Send(networkData);
      }
    }

    public void DoItemUnequip(EquipmentItem item)
    {
      if (item == null || this._myShip.GetEquippedItem(item.Type) != item)
        return;
      StringBuilder sb = new StringBuilder(200);
      sb.Append("[g]");
      sb.Append("Un-Equipping ");
      item.AppendLongName(sb, true, true);
      sb.Append("...");
      this.ShowAlert(sb.ToString());
      this._net.Send(RequestItemUnequip.Create(item));
    }

    public void DoAuctionRequestPage(ItemType type, ItemRarity rarity)
    {
      if (type == ItemType.NULL || rarity == ItemRarity.NULL || this._auctionRequestCheck[(int) type, (int) rarity])
        return;
      this._auctionRequestCheck[(int) type, (int) rarity] = true;
      if (Log.Debug)
      {
        StringBuilder stringBuilder = new StringBuilder(200);
        stringBuilder.Append("Requesting all [");
        stringBuilder.Append(rarity.Name());
        stringBuilder.Append(" ");
        stringBuilder.Append(type.Name());
        stringBuilder.Append("] auctions...");
        Log.D((object) this, nameof (DoAuctionRequestPage), stringBuilder.ToString());
      }
      this._net.Send(RequestAuctionPage.Create(type, rarity));
    }

    public void DoAuctionBid(ClientAuction auction)
    {
      if (auction == null || !auction.CanBid)
        return;
      StringBuilder sb = new StringBuilder(200);
      auction.AppendName(sb, false);
      sb.Append("\nBid $");
      sb.Append(auction.NextBid.ToString("#,#"));
      sb.Append(" Credits");
      INetworkData accept = RequestAuctionBid.Create(auction);
      this.ShowAcceptDecline("Item - Auction - Bid", sb.ToString(), accept, (INetworkData) null);
    }

    public void DoAuctionBuyout(ClientAuction auction, CurrencyType currency)
    {
      if (auction == null || !auction.CanBuyout(currency))
        return;
      StringBuilder sb = new StringBuilder(200);
      auction.AppendName(sb, false);
      INetworkData accept;
      if (currency != CurrencyType.Credits)
      {
        if (currency != CurrencyType.BlackDollars)
          return;
        sb.Append("\nBuyout for ");
        sb.Append(auction.BuyoutBlackDollars.ToString("#,#"));
        sb.Append(" BlackDollars");
        accept = RequestAuctionBuyout.Create(auction, true);
      }
      else
      {
        sb.Append("\nBuyout for $");
        sb.Append(auction.BuyoutCredits.ToString("#,#"));
        sb.Append(" Credits");
        accept = RequestAuctionBuyout.Create(auction, false);
      }
      this.ShowAcceptDecline("Item - Auction - Buyout", sb.ToString(), accept, (INetworkData) null);
    }

    public void DoAuctionCancel(ClientAuction auction)
    {
      if (auction == null || !auction.CanCancel)
        return;
      StringBuilder sb = new StringBuilder(200);
      auction.AppendName(sb, false);
      sb.Append("\nCancel Auction (You will not recover posting fee)");
      INetworkData accept = RequestAuctionCancel.Create(auction);
      this.ShowAcceptDecline("Item - Auction - Cancel", sb.ToString(), accept, (INetworkData) null);
    }

    public void DoAuctionPost(EquipmentItem item, int bid, int buyout)
    {
      if (item == null || !item.CanAuction || !this._myShip.Inventory.Contains(item) && !this.Bank.Items.Contains<EquipmentItem>(item))
        return;
      if (!item.NoDrop)
      {
        int auctionPostCost = item.AuctionPostCost;
        if (auctionPostCost <= this.Bank.Credits)
        {
          if (buyout == 0 || bid <= buyout)
          {
            StringBuilder sb = new StringBuilder(200);
            item.AppendLongName(sb, false, true);
            sb.Append("\nBid: $");
            sb.Append(bid.ToString("#,0"));
            if (buyout > 0)
            {
              sb.Append(" - Buyout: $");
              sb.Append(buyout.ToString("#,0"));
            }
            else
              sb.Append(" - Buyout: NONE");
            INetworkData accept = RequestAuctionSell.Create(item, bid, buyout);
            this.ShowAcceptDecline("Item - Auction - Sell", sb.ToString(), accept, (INetworkData) null);
          }
          else
            this.ShowAlert("[y]Request Failed - Minimum bid may not be higher than the buyout!");
        }
        else
          this.ShowAlert("[y]Request Failed - Auction posting fee is $" + auctionPostCost.ToString("#,#") + " Credits!");
      }
      else
        this.ShowAlert("Request Failed - Item is NO DROP");
    }

    public void DoSwapShips(ShipClass ship, sbyte shipSkin)
    {
      if (ship != this._myShip.Class && this._ships[ship])
      {
        Planet planet = this._local.Planet;
        if (planet != null && planet.Relation == RelationType.FRIEND)
        {
          StringBuilder stringBuilder = new StringBuilder(200);
          stringBuilder.Append("[g]");
          stringBuilder.Append("Changing to the ");
          stringBuilder.Append(ship.Name());
          stringBuilder.Append("...");
          this.ShowAlert(stringBuilder.ToString());
          this._net.Send(RequestSwapShip.Create(ship));
        }
      }
      if ((int) this._myPlayer.ShipSkin == (int) shipSkin)
        return;
      this._myPlayer.ShipSkin = shipSkin;
      this._net.Send(RequestSetAccountVariables.Create(shipSkin, (sbyte) 0, (sbyte) 0, (sbyte) 0, (sbyte) 0, (sbyte) 0, (sbyte) 0, (sbyte) 0));
    }

    public bool CanBuyShip(ShipClass ship, CurrencyType currency)
    {
      if (ship == this._myShip.Class || this._ships[ship] || !ship.CanBuy(currency))
        return false;
      if (ship.BuyRequiresPlanet(currency))
      {
        Planet planet = this._local.Planet;
        if (planet == null || planet.Relation != RelationType.FRIEND || (currency == CurrencyType.Credits || currency == CurrencyType.BlackDollars) && !planet.Class.CanBuildShip(ship))
          return false;
      }
      switch (currency)
      {
        case CurrencyType.Credits:
          return ship.CreditCost() <= this._bank.Credits;
        case CurrencyType.BlackDollars:
          return ship.BlackDollarCost() <= this._bank.BlackDollars;
        case CurrencyType.RewardPoints:
          return (double) ship.RewardPointCost() <= (double) this._bank.RewardPoints;
        case CurrencyType.CombatPoints:
          return ship.CombatPointCost() <= this._bank.CombatPoints;
        default:
          return false;
      }
    }

    public void DoBuyShip(ShipClass ship, CurrencyType currency)
    {
      if (ship == this._myShip.Class || this._ships[ship])
        return;
      string str = ship.Name();
      if (!ship.CanBuy(currency))
      {
        string text = "The " + str + " can not be purchased with " + currency.Name() + "!";
        if (currency != CurrencyType.RewardPoints)
        {
          if (currency == CurrencyType.CombatPoints)
            text += "\nType :COMBAT in chat for details!";
        }
        else
          text += "\nType :REWARD in chat for details!";
        this.ShowPopup(text, true);
      }
      else
      {
        if (ship.BuyRequiresPlanet(currency))
        {
          Planet planet = this._local.Planet;
          if (planet == null || planet.Relation != RelationType.FRIEND)
          {
            this.ShowPopup("You may only do that at a friendly planet!", true);
            return;
          }
          if ((currency == CurrencyType.Credits || currency == CurrencyType.BlackDollars) && !planet.Class.CanBuildShip(ship))
          {
            this.ShowPopup("This planet is not advanced enough to build that!", true);
            return;
          }
        }
        int cost = ship.GetCost(currency);
        float num = this._bank[currency];
        if ((double) cost > (double) num)
        {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.Append("The ");
          stringBuilder.Append(str);
          stringBuilder.Append(" costs ");
          if (currency == CurrencyType.Credits)
            stringBuilder.Append("$");
          stringBuilder.Append(cost.ToString("#,0"));
          stringBuilder.Append(" ");
          stringBuilder.Append(currency.Name());
          if ((double) num <= 0.0)
          {
            stringBuilder.Append("!\nYou don't have any!");
          }
          else
          {
            stringBuilder.Append("!\nYou only have ");
            if (currency == CurrencyType.Credits)
              stringBuilder.Append("$");
            stringBuilder.Append(num.ToString("#,0.##"));
            stringBuilder.Append("...");
          }
          switch (currency)
          {
            case CurrencyType.BlackDollars:
              stringBuilder.Append("\nYou may buy more with the BlackDollar button!");
              break;
            case CurrencyType.RewardPoints:
              stringBuilder.Append("\nType :REWARD in chat for details!");
              break;
            case CurrencyType.CombatPoints:
              stringBuilder.Append("\nType :COMBAT in chat for details!");
              break;
          }
          this.ShowPopup(stringBuilder.ToString(), true);
        }
        else
        {
          StringBuilder stringBuilder = new StringBuilder(200);
          INetworkData data;
          switch (currency)
          {
            case CurrencyType.Credits:
              data = RequestBuyShip.Create(ship, false);
              stringBuilder.Append("Unlock the ");
              stringBuilder.Append(str);
              stringBuilder.Append(" for $");
              stringBuilder.Append(ship.CreditCost().ToString("#,0"));
              stringBuilder.Append(" Credits");
              break;
            case CurrencyType.BlackDollars:
              switch (ship)
              {
                case ShipClass.WyrdInvader:
                  data = PurchaseItem.WyrdInvader.Data;
                  break;
                case ShipClass.WyrdAssassin:
                  data = PurchaseItem.WyrdAssassin.Data;
                  break;
                case ShipClass.WyrdReaper:
                  data = PurchaseItem.WyrdReaper.Data;
                  break;
                default:
                  data = RequestBuyShip.Create(ship, true);
                  break;
              }
              stringBuilder.Append("Unlock the ");
              stringBuilder.Append(str);
              stringBuilder.Append(" for ");
              stringBuilder.Append(ship.BlackDollarCost().ToString("#,0"));
              stringBuilder.Append(" BlackDollars");
              break;
            case CurrencyType.RewardPoints:
              data = RequestChat.Create(":UNLOCK " + this._myPlayer.Name + " " + str.Replace(" ", ""), ChatType.SECTOR);
              stringBuilder.Append("Unlock the ");
              stringBuilder.Append(str);
              stringBuilder.Append(" for ");
              stringBuilder.Append(ship.RewardPointCost().ToString("#,0.0"));
              stringBuilder.Append(" :REWARD Points");
              break;
            case CurrencyType.CombatPoints:
              data = RequestChat.Create(":UNLOCK " + this._myPlayer.Name + " " + str.Replace(" ", ""), ChatType.SECTOR);
              stringBuilder.Append("Unlock the ");
              stringBuilder.Append(str);
              stringBuilder.Append(" for ");
              stringBuilder.Append(ship.CombatPointCost().ToString("#,0"));
              stringBuilder.Append(" :COMBAT Points");
              break;
            default:
              return;
          }
          this.ShowAcceptDecline("Ship - Unlock - " + str, stringBuilder.ToString(), data, (INetworkData) null);
        }
      }
    }

    public void DoResearch(TechnologyType type)
    {
      Garrison garrison = this._local.Garrison;
      if (garrison == null || garrison.Tech[type])
        return;
      if (this._myPlayer.IsCorpLeader && garrison.Corporation == this._myPlayer.MyCorporation)
      {
        Technology tech = new Technology();
        tech.Set(garrison.Tech);
        tech[type] = true;
        if (tech.PointsUsed <= garrison.Level)
        {
          INetworkData accept = RequestSetTechnology.Create(tech);
          this.ShowAcceptDecline("Corporation - Garrison - Technology", "Research Technology: " + type.Name(), accept, (INetworkData) null);
        }
        else
          this.ShowPopup("Increase Garrison rank to support more technology!", true);
      }
      else
        this.ShowPopup("Only the corporate leader can research new technology!", true);
    }

    public void DoCorpCreate(string name)
    {
      if (this._myPlayer.MyCorporation == null)
      {
        if (this._bank.Credits >= 1000000)
        {
          while (name.Contains("  "))
            name = name.Replace("  ", " ");
          name = Util.Regex(name.Trim(), true, true, true);
          if (name.Length > 18)
            name = name.Substring(0, 18);
          if (name.Length >= 3)
          {
            if (this.Corporations.Get(name) == null)
            {
              INetworkData accept = RequestChat.Create(":CREATE " + name, ChatType.SECTOR);
              this.ShowAcceptDecline("Corporation - Create", "Create '" + name + "' for $1,000,000 Credits", accept, (INetworkData) null);
            }
            else
              this.ShowPopup("'" + name + "' is already taken!", true);
          }
          else
            //this.ShowPopup("'" + name + "' is too short! (" + (string) (ValueType) 3 + " letters minimum)", true);
            this.ShowPopup("'" + name + "' is too short! (3 letters minimum)", true);
                }
                else
          this.ShowPopup("Corporations cost $1,000,000 Credits to create!", true);
      }
      else
        this.ShowPopup("You are already in a Corporation!", true);
    }

    public void DoCorpRename(string name)
    {
      if (this._myPlayer.IsCorpLeader)
      {
        while (name.Contains("  "))
          name = name.Replace("  ", " ");
        name = Util.Regex(name.Trim(), true, true, true);
        if (name.Length > 18)
          name = name.Substring(0, 18);
        if (name.Length >= 3)
        {
          if (this.Corporations.Get(name) == null)
          {
            INetworkData accept = RequestChat.Create(":RENAME " + name, ChatType.SECTOR);
            this.ShowAcceptDecline("Corporation - Rename", "Rename Corporation to '" + name + "'\nCost: 1,000 BlackDollars (1st rename free)", accept, (INetworkData) null);
          }
          else
            this.ShowPopup("'" + name + "' is already taken!", true);
        }
        else
          //this.ShowPopup("'" + name + "' is too short! (" + (string) (ValueType) 3 + " letters minimum)", true);
          this.ShowPopup("'" + name + "' is too short! (3 letters minimum)", true);
            }
      else
        this.ShowPopup("You are not the leader of a Corporation!", true);
    }

    public void DoCorpLeave()
    {
      if (this._myPlayer.MyCorporation != null)
        this.ShowAcceptDecline("Corporation - Leave", "Abandon '" + this._myPlayer.MyCorporation.Name + "' Corporation", RequestChat.Create(":LEAVE CONFIRM", ChatType.SECTOR), (INetworkData) null);
      else
        this.ShowPopup("You are not in a Corporation!", true);
    }

    public void DoCorpDeposit(CurrencyType currency, int amt)
    {
      if (amt <= 0)
        return;
      if (this._myPlayer.MyCorporation != null)
      {
        if (currency != CurrencyType.Credits)
        {
          if (currency != CurrencyType.BlackDollars)
            return;
          if (this._bank.BlackDollars >= amt)
          {
            INetworkData accept = RequestChat.Create(":DEPOSITBD " + (object) amt, ChatType.SECTOR);
            this.ShowAcceptDecline("Corporation - Bank - Deposit", "Deposit " + amt.ToString("#,0") + " BlackDollars", accept, (INetworkData) null);
          }
          else
            this.ShowPopup("Deposit Failed - You do not have " + amt.ToString("#,0") + " BlackDollars!", true);
        }
        else if (this._bank.Credits >= amt)
        {
          INetworkData accept = RequestChat.Create(":DEPOSITCREDIT " + (object) amt, ChatType.SECTOR);
          this.ShowAcceptDecline("Corporation - Bank - Deposit", "Deposit $" + amt.ToString("#,0") + " Credits", accept, (INetworkData) null);
        }
        else
          this.ShowPopup("Deposit Failed - You do not have $" + amt.ToString("#,0") + " Credits!", true);
      }
      else
        this.ShowPopup("You are not in a Corporation!", true);
    }

    public void DoCorpWithdraw(CurrencyType currency, int amt)
    {
      if (amt <= 0)
        return;
      if (this._myPlayer.MyCorporation != null)
      {
        if (currency != CurrencyType.Credits)
        {
          if (currency != CurrencyType.BlackDollars)
            return;
          INetworkData accept = RequestChat.Create(":WITHDRAWBD " + (object) amt, ChatType.SECTOR);
          this.ShowAcceptDecline("Corporation - Bank - Withdraw", "Withdraw " + amt.ToString("#,0") + " BlackDollars", accept, (INetworkData) null);
        }
        else
        {
          INetworkData accept = RequestChat.Create(":WITHDRAWCREDIT " + (object) amt, ChatType.SECTOR);
          this.ShowAcceptDecline("Corporation - Bank - Withdraw", "Withdraw $" + amt.ToString("#,0") + " Credits", accept, (INetworkData) null);
        }
      }
      else
        this.ShowPopup("You are not in a Corporation!", true);
    }

    public void DoCorpInvite(string playerName)
    {
      playerName = Util.Regex(playerName.Trim(), true, true, true);
      if (this._myPlayer.MyCorporation != null)
      {
        if (this._myPlayer.Rank.CanInvite())
        {
          ClientPlayer clientPlayer = this._players.Get(playerName);
          if (clientPlayer == null || clientPlayer.MyCorporation == null)
          {
            INetworkData accept = RequestChat.Create(":INVITE " + playerName, ChatType.SECTOR);
            this.ShowAcceptDecline("Corporation - Member - Invite", "Invite " + (clientPlayer == null ? playerName : clientPlayer.Name) + " to " + this._myPlayer.MyCorporation.Name, accept, (INetworkData) null);
          }
          else
            this.ShowPopup(clientPlayer.Name + " is already in a Corporation!", true);
        }
        else
          this.ShowPopup("You are not high enough rank to send invites!", true);
      }
      else
        this.ShowPopup("You are not in a Corporation!", true);
    }

    public void DoCorpKick(ClientPlayer target)
    {
      if (target == null)
        return;
      if (this._myPlayer.MyCorporation != null && target.MyCorporation == this._myPlayer.MyCorporation)
      {
        if (this._myPlayer.Rank.CanKick() && (this._myPlayer == target || target.Rank < this._myPlayer.Rank))
        {
          INetworkData accept = RequestChat.Create(":KICK " + target.Name, ChatType.SECTOR);
          this.ShowAcceptDecline("Corporation - Member - Kick", "Kick " + target.Name, accept, (INetworkData) null);
        }
        else
          this.ShowPopup("You are not high enough rank to kick " + target.Name + "!", true);
      }
      else
        this.ShowPopup("You are not in " + target.Name + "'s Corporation!", true);
    }

    public void DoCorpDemote(ClientPlayer target)
    {
      if (target == null)
        return;
      if (this._myPlayer.MyCorporation != null && target.MyCorporation == this._myPlayer.MyCorporation)
      {
        if (target.Rank > CorpRankType.RECRUIT)
        {
          if (this._myPlayer.Rank.CanDemote() && (this._myPlayer == target || target.Rank < this._myPlayer.Rank))
          {
            INetworkData accept = RequestChat.Create(":DEMOTE " + target.Name, ChatType.SECTOR);
            this.ShowAcceptDecline("Corporation - Member - Demote", "Demote " + target.Name + " to " + (target.Rank - 1).Name(), accept, (INetworkData) null);
          }
          else
            this.ShowPopup("You are not high enough rank to demote " + target.Name + "!", true);
        }
        else
          this.ShowPopup(target.Name + " cannot be demoted further!", true);
      }
      else
        this.ShowPopup("You are not in " + target.Name + "'s Corporation!", true);
    }

    public void DoCorpPromote(ClientPlayer target)
    {
      if (target == null)
        return;
      if (this._myPlayer.MyCorporation != null && target.MyCorporation == this._myPlayer.MyCorporation)
      {
        if (target.Rank < CorpRankType.LEADER)
        {
          if (this._myPlayer.Rank >= CorpRankType.LEADER || this._myPlayer.Rank.CanPromote() && target.Rank < this._myPlayer.Rank)
          {
            INetworkData accept = RequestChat.Create(":PROMOTE " + target.Name, ChatType.SECTOR);
            this.ShowAcceptDecline("Corporation - Member - Promote", "Promote " + target.Name + " to " + (target.Rank + 1).Name(), accept, (INetworkData) null);
          }
          else
            this.ShowPopup("You are not high enough rank to promote " + target.Name + "!", true);
        }
        else
          this.ShowPopup(target.Name + " cannot be promoted further!", true);
      }
      else
        this.ShowPopup("You are not in " + target.Name + "'s Corporation!", true);
    }

    public void DoCorpDeployGarrison()
    {
      if (this._myPlayer.IsCorpLeader)
      {
        if (this._myPlayer.MyCorporation.Location == null)
        {
          int num = 25000 + 25000 * (int) this._myPlayer.MyCorporation.GarrisonRank;
          if (num <= this._bank.Credits)
          {
            INetworkData accept = RequestChat.Create(":DEPLOYGARRISON CONFIRM", ChatType.SECTOR);
            this.ShowAcceptDecline("Corporation - Garrison - Deploy", "Deploy Garrison to " + (object) this._myLoc + " for $" + num.ToString("#,0") + " Credits", accept, (INetworkData) null);
          }
          else
            this.ShowPopup("Deploy Garrison requires $25,000 Credits per rank! ($" + num.ToString("#,0") + ")", true);
        }
        else
          this.ShowPopup("Garrison already deployed in " + (object) this._myPlayer.MyCorporation.Location + "!", true);
      }
      else
        this.ShowPopup("You are not the leader of a Corporation!", true);
    }

    public void DoCorpRecallGarrison()
    {
      if (this._myPlayer.IsCorpLeader)
      {
        if (this._myPlayer.MyCorporation.Location != null)
          this.ShowAcceptDecline("Corporation - Garrison - Recall", "Recall Garrison from " + (object) this._myPlayer.MyCorporation.Location + "\n(You will not recover deployment costs)", RequestChat.Create(":RECALLGARRISON CONFIRM", ChatType.SECTOR), (INetworkData) null);
        else
          this.ShowPopup("Garrison is not delpoyed!", true);
      }
      else
        this.ShowPopup("You are not the leader of a Corporation!", true);
    }

    public void DoCorpSetShipColor(byte value)
    {
      if (!this._myPlayer.IsCorpLeader)
      {
        this.ShowPopup("You are not the leader of a Corporation!", true);
      }
      else
      {
        if ((int) this._myPlayer.MyCorporation.ShipColor == (int) value)
          return;
        INetworkData accept = RequestSetCorpShipColor.Create((sbyte) value);
        if (this._bank.BlackDollars >= 1000)
          this.ShowAcceptDecline("Corporation - Ship Color", "Set Ship Color (#" + (object) value + ")\nCost: 1,000 BlackDollars", accept, (INetworkData) null);
        else
          this.ShowPopup("Ship color change requires 1,000 BlackDollars", true);
      }
    }

    public void DoAllianceCreate()
    {
      if (this._myPlayer.IsCorpLeader)
      {
        if (this._myPlayer.MyCorporation.Alliance == null)
        {
          if (this._bank.Credits >= 10000000)
            this.ShowAcceptDecline("Alliance - Create", "Create Alliance for $10,000,000 Credits", RequestChat.Create(":ALLIANCECREATE", ChatType.SECTOR), (INetworkData) null);
          else
            this.ShowPopup("Alliances cost $10,000,000 Credits to create!", true);
        }
        else
          this.ShowPopup("Your Corporation is already in an Alliance!", true);
      }
      else
        this.ShowPopup("You are not the leader of a Corporation!", true);
    }

    public void DoAllianceLeave()
    {
      if (this._myPlayer.IsCorpLeader)
      {
        if (this._myPlayer.MyCorporation.Alliance != null)
          this.ShowAcceptDecline("Alliance - Leave", "Abandon your Corporation's Alliance", RequestChat.Create(":ALLIANCELEAVE CONFIRM", ChatType.SECTOR), (INetworkData) null);
        else
          this.ShowPopup("Your Corporation is not in an Alliance!", true);
      }
      else
        this.ShowPopup("You are not the leader of a Corporation!", true);
    }

    public void DoAllianceLeader(ClientCorporation corp)
    {
      if (corp == null)
        return;
      if (this._myPlayer.IsAllianceLeader)
      {
        if (corp.Alliance == this._myPlayer.MyAlliance)
        {
          INetworkData accept = RequestChat.Create(":ALLIANCELEADER " + corp.Name, ChatType.SECTOR);
          this.ShowAcceptDecline("Alliance - Member - Leadership", "Give up control of your Alliance to " + corp.Name, accept, (INetworkData) null);
        }
        else
          this.ShowPopup(corp.Name + " is not in your Alliance!", true);
      }
      else
        this.ShowPopup("You are not the leader of an Alliance!", true);
    }

    public void DoAllianceInvite(ClientCorporation corp)
    {
      if (corp == null)
        return;
      if (this._myPlayer.IsAllianceLeader)
      {
        if (corp.Alliance == null)
        {
          INetworkData accept = RequestChat.Create(":ALLIANCEINVITE " + corp.Name, ChatType.SECTOR);
          this.ShowAcceptDecline("Alliance - Member - Invite", "Invite " + corp.Name + " to your Alliance", accept, (INetworkData) null);
        }
        else
          this.ShowPopup(corp.Name + " is already in another Alliance!", true);
      }
      else
        this.ShowPopup("You are not the leader of an Alliance!", true);
    }

    public void DoAllianceKick(ClientCorporation corp)
    {
      if (corp == null)
        return;
      if (this._myPlayer.IsAllianceLeader)
      {
        if (corp.Alliance == this._myPlayer.MyAlliance)
        {
          INetworkData accept = RequestChat.Create(":ALLIANCEKICK " + corp.Name, ChatType.SECTOR);
          this.ShowAcceptDecline("Alliance - Member - Kick", "Kick " + corp.Name + " from your Alliance", accept, (INetworkData) null);
        }
        else
          this.ShowPopup(corp.Name + " is not in your Alliance!", true);
      }
      else
        this.ShowPopup("You are not the leader of an Alliance!", true);
    }

    public void DoAllianceDeployPlanet()
    {
      if (this._myPlayer.IsAllianceLeader)
      {
        if (this._myPlayer.MyAlliance.Location == null)
        {
          int num = 50000 + 50000 * (int) this._myPlayer.MyAlliance.Planet;
          if (num <= this._bank.Credits)
          {
            INetworkData accept = RequestChat.Create(":DEPLOYPLANET CONFIRM", ChatType.SECTOR);
            this.ShowAcceptDecline("Alliance - Planet - Deploy", "Deploy Planet to " + (object) this._myLoc + " for $" + num.ToString("#,0") + " Credits", accept, (INetworkData) null);
          }
          else
            this.ShowPopup("Deploy Planet requires $50,000 Credits per class! ($" + num.ToString("#,0") + ")", true);
        }
        else
          this.ShowPopup("Planet already deployed in " + (object) this._myPlayer.MyAlliance.Location + "!", true);
      }
      else
        this.ShowPopup("You are not the leader of an Alliance!", true);
    }

    public void DoAllianceRecallPlanet()
    {
      if (this._myPlayer.IsAllianceLeader)
      {
        if (this._myPlayer.MyAlliance.Location != null)
          this.ShowAcceptDecline("Alliance - Planet - Recall", "Recall Planet from " + (object) this._myPlayer.MyCorporation.Location + "\n(You will not recover deployment costs)", RequestChat.Create(":RECALLPLANET CONFIRM", ChatType.SECTOR), (INetworkData) null);
        else
          this.ShowPopup("Planet is not deployed!", true);
      }
      else
        this.ShowPopup("You are not the leader of an Alliance!", true);
    }

    public void DoRequestInventory(Ship ship)
    {
      if (ship == null)
        return;
      this._net.Send(RequestInventory.Create(ship.ID));
    }

    public void DoRequestInventory(StarPort starport)
    {
      if (starport == null)
        return;
      this._net.Send(RequestInventory.Create(starport.ID));
    }

    public void DoRequestInventory(Garrison garrison)
    {
      if (garrison == null)
        return;
      this._net.Send(RequestInventory.Create(garrison.ID));
    }

    public void DoRequestStats(ClientPlayer player)
    {
      if (player == null)
        return;
      this._net.Send(RequestPlayerStats.Create(player.ID));
    }

    protected internal void AddReply(string name)
    {
      name = name.Trim();
      if (string.IsNullOrEmpty(name) || this._myPlayer == null || this._myPlayer.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
        return;
      GameState._lastPrivateIndex = -1;
      GameState._lastPrivates.RemoveAll((Predicate<string>) (n => n.Equals(name, StringComparison.OrdinalIgnoreCase)));
      GameState._lastPrivates.Insert(0, name);
      if (GameState._lastPrivates.Count <= 10)
        return;
      GameState._lastPrivates.RemoveAt(GameState._lastPrivates.Count - 1);
    }

    private void UpdateAttack()
    {
      if (this._myAttackTarget == null || this._worldTimeMS - this._lastAttackTimeMS < 30000L)
        return;
      this.OnAttack((CombatEntity) null);
    }

    private bool TryDoChatCommand(string text)
    {
      if (!text.StartsWith(":"))
        return false;
      AccountSettings mySettings = this.MySettings;
      if (mySettings != null)
      {
        if (text.ToLower().StartsWith(":friend"))
        {
          int num = text.IndexOf(" ", StringComparison.OrdinalIgnoreCase);
          string name = text.Substring(num + 1).Trim();
          if (!string.IsNullOrEmpty(name))
          {
            if (mySettings.Social.IsFriend(name))
            {
              mySettings.Social.RemoveFriend(name);
              this.ShowAlert("[b]No longer friending '" + name + "'");
            }
            else
            {
              mySettings.Social.AddFriend(name);
              this.ShowAlert("[b]Friending '" + name + "'");
            }
          }
        }
        else if (text.ToLower().StartsWith(":ignore"))
        {
          int num = text.IndexOf(" ", StringComparison.OrdinalIgnoreCase);
          string name = text.Substring(num + 1).Trim();
          if (!string.IsNullOrEmpty(name))
          {
            if (mySettings.Social.IsIgnore(name))
            {
              mySettings.Social.RemoveIgnore(name);
              this.ShowAlert("[r]No longer ignoring '" + name + "'");
            }
            else
            {
              mySettings.Social.AddIgnore(name);
              this.ShowAlert("[r]Ignoring '" + name + "'");
            }
          }
        }
        else
          this._net.Send(RequestChat.Create(text, ChatType.SECTOR));
      }
      return true;
    }

    private bool TryDoPrivateMessage(string text)
    {
      if (!text.StartsWith("/"))
        return false;
      int num = text.IndexOf(" ", StringComparison.OrdinalIgnoreCase);
      if (num >= 1)
      {
        string message = text.Substring(num + 1).Trim();
        string reciever = text.Substring(1, num - 1).Trim();
        if (message.Length > 0 && reciever.Length > 0)
          this._net.Send(RequestChat.CreatePrivate(message, reciever));
      }
      return true;
    }

    protected internal void OnLoginFailed(string text)
    {
      this.Disconnect(text);
      this.ShowPopup(text, true);
    }

    protected internal void OnLogin(ClientPlayer myPlayer, Ship myShip)
    {
      this._myPlayer = myPlayer;
      this._myShip = myShip;
      this._login.Success = true;
      this.RefreshMyAccount();
      this.IsLoggedIn = true;
      this._ui.Show(new LoginSuccessEventArgs((IGameState) this, this._myPlayer, this._myShip));
      GameState.IsOnDiscord = DiscordBot.WebHooks.ContainsKey(myPlayer.Name);
      this.TakeOffSkipDrive();
      if (!this._accounts.System.ShowTutorial)
        return;
      this._accounts.System.ShowTutorial = false;
      this._ui.Show(new ShowUrlEventArgs((IGameState) this, "http://www.spellbook.com/tib/tutorial.php?mobile=1", false));
    }

    public static bool IsOnDiscord { get; set; }

    private void TakeOffSkipDrive()
    {
      if (this._myShip.Engine == null || this._myShip.Engine.Class != EngineClass.SKIP)
        return;
      this.DoItemUnequip((EquipmentItem) this._myShip.Engine);
    }

    protected internal void OnMove(Sector myLoc, List<Entity> entities)
    {
      foreach (CombatEntity combatEntity in entities.OfType<CombatEntity>())
        combatEntity.Grappled = false;
      this._myShip.Grappled = false;
      if (this._myLoc != null)
        this._myLoc.OnChanged();
      myLoc.OnChanged();
      this._myLoc = myLoc;
      this._local.Set(entities);
      this._ui.Show(new LocationChangedEventArgs((IGameState) this, myLoc, entities));
    }

    protected internal void OnAttack(CombatEntity target)
    {
      this.SetLastAttackTime();
      this._myAttackTarget = target;
      this._ui.Show(new AttackTargetChangedEventArgs((IGameState) this, target));
    }

    protected internal void OnFollow(CombatEntity target)
    {
      this._myFollowTarget = target;
      this._ui.Show(new FollowTargetChangedEventArgs((IGameState) this, target));
    }

    protected internal void SetLastAttackTime()
    {
      this._lastAttackTimeMS = this._worldTimeMS;
    }

    private void RefreshMyAccount()
    {
      if (!this._login.Success)
        return;
      this._myAccount = this._accounts.Refresh(this._myPlayer, this._myShip, this._accounts.System.RememberPassword ? this._login.Password : string.Empty, this._login.ServerID);
    }

    public static void LogError(Exception e)
    {
      using (StreamWriter streamWriter = File.AppendText("C:\\logs\\errors.txt"))
        streamWriter.WriteLine((object) e);
    }

    private void StackInfo(IEnumerable<Ship> stack)
    {
      this.ShowAlert("[g]Speed Range: " + (object) stack.Min<Ship>((Func<Ship, int>) (x => x.MoveSpeedMS)) + " - " + (object) stack.Max<Ship>((Func<Ship, int>) (x => x.MoveSpeedMS)));
      if (stack.Count<Ship>((Func<Ship, bool>) (x => x.Armor != null && x.Armor.Class == ArmorClass.KISMET_GAMBIT)) > 0)
        this.ShowAlert("[r]Kismets: " + (object) stack.Count<Ship>((Func<Ship, bool>) (x => x.Armor != null && x.Armor.Class == ArmorClass.KISMET_GAMBIT)));
      this.ShowAlert("[g]Rescues: " + (object) stack.Count<Ship>(GameState.IsRescue()));
      foreach (IGrouping<SpecialClass, Ship> source in stack.Where<Ship>((Func<Ship, bool>) (x =>
      {
        int num1;
        if (x.Special != null)
        {
          int num2 = (int) x.Special.Class;
          num1 = 1;
        }
        else
          num1 = 0;
        return num1 != 0;
      })).GroupBy<Ship, SpecialClass>((Func<Ship, SpecialClass>) (x => x.Special.Class)))
        this.ShowAlert(string.Format("[g]{0}: {1}", (object) source.Key, (object) source.Count<Ship>()));
    }

    private static Func<Ship, bool> IsRescue()
    {
      return (Func<Ship, bool>) (x => x.Special != null && x.Special.Class == SpecialClass.TECHNICIAN && (x.Engine != null && x.Engine.Class == EngineClass.SKIP) && x.Class == ShipClass.Carrier);
    }

    private static bool IsRescue(Ship x)
    {
      return x.Special != null && x.Special.Class == SpecialClass.TECHNICIAN && (x.Engine != null && x.Engine.Class == EngineClass.SKIP) && x.Class == ShipClass.Carrier;
    }

    private Sector NearestSP()
    {
      int num1 = this._myLoc.X - 9 <= 0 ? 0 : this._myLoc.X - 9;
      int num2 = this._myLoc.Y - 9 <= 0 ? 0 : this._myLoc.Y - 9;
      int num3 = this._myLoc.X + 11 >= 79 ? 79 : this._myLoc.X + 11;
      int num4 = this._myLoc.Y + 11 >= 79 ? 79 : this._myLoc.Y + 11;
      List<Sector> source = new List<Sector>();
      for (int x = num1; x <= num3; ++x)
      {
        for (int y = num2; y <= num4; ++y)
        {
          Sector sector = this.Map.Get(this._myLoc.Rift, x, y);
          if (sector != null)
            source.Add(sector);
          else
            this.ShowAlert("X:" + (object) x + ",Y:" + (object) y + " is null");
        }
      }
      return source.Where<Sector>((Func<Sector, bool>) (s => s.StarPort)).Where<Sector>((Func<Sector, bool>) (x => this.Map.GetPath(this._myLoc, x).Count != 0)).OrderBy<Sector, int>((Func<Sector, int>) (x => this.Map.GetPath(this._myLoc, x).Count)).First<Sector>();
    }
    // find nearest asteroid

    private Sector NearestRoid()
    {
      int num1 = 0;
      int num2 = 0;
      int num3 = 79;
      int num4 = 79;
      List<Sector> source = new List<Sector>();
      for (int x = num1; x <= num3; ++x)
      {
        for (int y = num2; y <= num4; ++y)
        {
          Sector sector = this.Map.Get(this._myLoc.Rift, x, y);
          if (sector != null)
            source.Add(sector);
          else
            this.ShowAlert("X:" + (object)x + ",Y:" + (object)y + " is null");
        }
      }
      return source.Where<Sector>((Func<Sector, bool>)(s => s.Roid)).Where<Sector>((Func<Sector, bool>)(x => this.Map.GetPath(this._myLoc, x).Count != 0)).OrderBy<Sector, int>((Func<Sector, int>)(x => this.Map.GetPath(this._myLoc, x).Count)).First<Sector>();

    }
    private void DoCustomActions()
    {
      if (this.SeedSave && (this._local.Contains(EntityType.NPC) && ((Npc) this._local.Values.First<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC))).IsSeed()))
      {
        Npc npc = (Npc) this._local.Values.First<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC));
        if (!this._savedSeeds.ContainsKey(this._myLoc.ToString()))
          this._savedSeeds.Add(this._myLoc.ToString(), npc.Title);
        if (!object.Equals((object) this._savedSeeds[this._myLoc.ToString()], (object) npc.Title))
          this._savedSeeds[this._myLoc.ToString()] = npc.Title;
      }
      if (this.IsLoggedIn && this.WorldTimeMS - this._lastHeartBeat > 60000L)
      {
        this._lastHeartBeat = this.WorldTimeMS;
        this._messageController.SendHeartBeat(this._myPlayer.Name);
      }
      if (this.WorldTimeMS - this._lastMessageCheck > 10000L)
      {
        this._lastMessageCheck = this.WorldTimeMS;
        this._messageController.GetMessage(this._myPlayer.Name, (IGameState) this);
      }
      // FKEY SWITCH
      if (this.FkeyLevel == true && this.TabKeyPress != null)
      {
        int numf1;
        TimeSpan timeSpan_f;
        timeSpan_f = DateTime.Now - this.TabKeyPress;
        numf1 = timeSpan_f.TotalMilliseconds > (double)this._FkeyDelay ? 0 : 1;
        if (numf1 == 0)
        {
          FkeyLevel = !FkeyLevel; // TOGGLE FKEY LEVEL
        }
      }
      // AUTOHOOK
      if (this.AutoHook && !(this.MyLoc.IsGreyShallows()))
      {
        if (this.AutoHook && (this._local.EnemyShips.Any<Ship>((Func<Ship, bool>) (x => x.Relation == RelationType.ENEMY && (x.Special == null || (x.Special.Class != SpecialClass.NOVA && x.Special.Class != SpecialClass.TECHNICIAN && x.Special.Class != SpecialClass.TANK && x.Special.Class != SpecialClass.DEFLECTOR)))) && this.AttackCooldown.IsFinished && this._myAttackTarget == null && (this.MoveCooldown.RemainingMS < 200L || this.MoveCooldown.IsFinished)) && this._worldTimeMS - this._lastAttackTimeMS > 1000L)
          this.DoAttack(!this._local.EnemyShips.Any<Ship>((Func<Ship, bool>) (x => x.Class != ShipClass.Carrier && x.Relation == RelationType.ENEMY && (x.Special == null || x.Special.Class != SpecialClass.NOVA))) ? (CombatEntity) this._playerAttackOrder.SelectTarget(this._local.EnemyShips.Where<Ship>((Func<Ship, bool>) (x => x.Relation == RelationType.ENEMY && (x.Special == null || x.Special.Class != SpecialClass.NOVA)))) : (CombatEntity) this._playerAttackOrder.SelectTarget(this._local.EnemyShips.Where<Ship>((Func<Ship, bool>) (x => x.Class != ShipClass.Carrier && x.Relation == RelationType.ENEMY && (x.Special == null || x.Special.Class != SpecialClass.NOVA)))));
        else if (this.AutoHook && this._myAttackTarget != null && this._worldTimeMS - this._lastAttackTimeMS > 1000L)
          this.DoAttack((CombatEntity) null);
      }
      // BUYABLES
      if (this.AutoMindSurge && this._buyables.MindSurge < 20 && this._worldTimeMS - this._lastMindSurgePurchase > 1000L)
      {
        this._lastMindSurgePurchase = this._worldTimeMS;
        this.DoPurchase(PurchaseItem.MindSurge);
      }
      if (this.ShotHold && this._local.EnemyShips.Any<Ship>((Func<Ship, bool>) (x => x.Relation == RelationType.ENEMY)) && this.AttackCooldown.IsFinished)
      {
        Ship ship = this._local.EnemyShips.Where<Ship>((Func<Ship, bool>) (x => x.Relation == RelationType.ENEMY)).OrderBy<Ship, int>((Func<Ship, int>) (x => x.Hull)).First<Ship>();
        if (ship != null && ship.Hull < this.MyShip.MinimumDamage * 2 && (ship.Special == null || ship.Special.Class != SpecialClass.NOVA) && (this._myAttackTarget == null || this._myAttackTarget.Name != ship.Name))
          this.DoAttack((CombatEntity) ship);
      }
      else if (this.ShotHold && this._myAttackTarget != null)
        this.DoAttack((CombatEntity) null);
      // HEALING SECTION
      //      this.ShowAlert("[g]START of the HEALING section!");
      TimeSpan timeSpan;
      //int num1;
      //timeSpan = DateTime.Now - this._lastRepair;  // moved to before healing statements. commented for selfheal, degrapple, friendheal.

      ////num1 = timeSpan.TotalMilliseconds > (double)this._autoHealTime ? 0 : 1;
      //num1 = timeSpan.TotalMilliseconds > this._autoHealTime ? 0 : 1;
      //if (num1 != 0)
      //{
      //  //this.ShowAlert("[b]Timespan too short!");
      //  goto label_99;
      //}
      //else
      //{
      //  // if there has not been any recent TECH activities wait for a short time to avoid hitting the tech slowdown.
      //  int num2;
      //  num2 = timeSpan.TotalMilliseconds > this._autoHealDelay ? 0 : 1;
      //  this.ShowAlert("[r]CHECK if Teching done recently!");
      //  //num2 = timeSpan.TotalMilliseconds > (double)this._autoHealDelay ? 0 : 1;
      //  if (num2 != 0)
      //  {
      //    this.ShowAlert("[r]Delaying teching to prevent insta tech nerf!");
      //    _repTS = DateTime.Now;
      //    this._lastRepair = _repTS.AddMilliseconds(this._autoHealTime - 150);
      //    goto label_99;
      //  }
      //}
      if (this.AutoSelfHeal && this._myShip.Hull < this.SelfHealAmount && ((int) this._myShip.Metals > 0 && this._myShip.Hull < this._myShip.MaxHull))
      {
        int num3;
        timeSpan = DateTime.Now - this._lastRepair;
        num3 = timeSpan.TotalMilliseconds > (double)this._autoHealTime ? 0 : 1;
        //this.ShowAlert("[b]SELFHEAL section!");
        //        num1 = timeSpan.TotalMilliseconds > this._autoHealTime ? 0 : 1;
        if (num3 != 0)
        {
          //this.ShowAlert("[g]Selfheal - Timespan too short!");
          goto label_99;
        }
        if (this._repairCooldown.IsFinished && this._myLoc.Status == SectorStatus.ENGAGED)
        {
          this._repairCooldown.Set(3000L);
          this.ShowAlert("[g]Requesting Full Repair...");
          this._net.Send(RequestRepairBlackDollar.Create());
        }
        else
        {
          // suppress on friendly garrison
          Garrison garrison = this._local.Garrison;
          //if (!(garrison != null && garrison.Relation == RelationType.FRIEND))
          if ((garrison != null && garrison.Relation == RelationType.FRIEND))
          {
            this.ShowAlert("[r]SELFHEAL - SKIP ON Friendly Garrison");
            goto label_98;
          }
          if(this._myShip.Special.Class != SpecialClass.TECHNICIAN)
          { // check on special equipped and if it's a TECHNICIAN
            //this.ShowAlert("[r]SELFHEAL - I am not a TECH");
            IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>)(x => x.Special != null && x.Special.Class == SpecialClass.TECHNICIAN));
            if ((source.Any<Ship>()))
            {
              //this.ShowAlert("[r]SELFHEAL - TECH in SECTOR");
              goto label_98;
            }
          }
          //this.ShowAlert("[r]SELFHEAL");
          this._lastRepair = DateTime.Now;
          this.DoRepair((PlayerCombatEntity)this._myShip);
        }
      }
      label_98:  // TECHNICIAN SECTION
      //      this.ShowAlert("[g]Check on TECH Special!");
      if (this._myShip.Special.Class != SpecialClass.TECHNICIAN)
      {
        goto label_99; // No TECHNICIAN special equipped so jumping past healing section.
      }
      // NEW SECTION TO DETERMINE HEALING DELAY
//      TimeSpan timeSpan;
      int num1;
      timeSpan = DateTime.Now - this._lastRepair;
      num1 = timeSpan.TotalMilliseconds > (double)this._autoHealTime ? 0 : 1;   // IF last repair within X milliseconds then skip healing
      if (num1 != 0)
      {
        //this.ShowAlert("[b]Timespan: " + timeSpan);
        //this.ShowAlert("[g]num1" + num1);
        //this.ShowAlert("[r]last repair: " + this._lastRepair);
        //this.ShowAlert("[r]DateTime now: " + DateTime.Now);
        //this.ShowAlert("[g]Timespan too short!");
        goto label_99;
      }
      //this.ShowAlert("[r]CHECK if Teching done recently!");
      if ((int)this._myShip.Metals > 0 && (this.AutoDegrapple || this.AutoFriendHeal))
      {
        // check if anything needs to be degrappled or healed
        IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>)(x => x.ShouldBeDegrappled(this._worldTimeMS)));
        if ((source.Any<Ship>()) || (this._local.FriendlyShips.Any<Ship>((Func<Ship, bool>)(x => (double)x.EffectiveHull < this.FriendHealAmount && x.EffectiveMaxHull - x.EffectiveHull > 2))))
        {
          //this.ShowAlert("[g]SOMETHING IS IN NEED OF ATTENTION!");
          if (source.Any<Ship>())
          {
            //this.ShowAlert("[g]executing DeGrapple - NEW");
            this.DoRepair((PlayerCombatEntity)source.First<Ship>());
            //          goto label_53;   // LOOP removed to prevent hitting the nerf.
            this._lastRepair = DateTime.Now;
            goto label_99; // EXIT Healing.
          }
          if (this._local.FriendlyShips.Any<Ship>((Func<Ship, bool>)(x => (double)x.EffectiveHull < this.FriendHealAmount && x.EffectiveMaxHull - x.EffectiveHull > 2)))
          {
            this.DoRepair((PlayerCombatEntity)this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => (double)x.EffectiveHull < this.FriendHealAmount && x.EffectiveMaxHull - x.EffectiveHull > 2)));
            this._lastRepair = DateTime.Now;
            //this.ShowAlert("[r]Executed repair! - NEW");
          }
        }
      }
      //int num2;
      //num2 = timeSpan.TotalMilliseconds > this._autoHealDelay ? 0 : 1;  // If last repair longer than 1000 ms ago then wait for a bit to start healing. 
      //if (num2 != 0)
      //{
      //  //this.ShowAlert("[r]Delaying teching to prevent insta tech nerf!");
      //  _repTS = DateTime.Now;
      //  this._lastRepair = _repTS.AddMilliseconds(this._autoHealTime - 150);
      //  goto label_99;
      //}
      ////
      //// DEGRAPPLE SECTION
      //if (this.AutoDegrapple && (int)this._myShip.Metals > 0)
      //{
      //  //this.ShowAlert("[g]DEGRAPPLE Section!");
      //  IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>)(x => x.ShouldBeDegrappled(this._worldTimeMS)));
      //  if (source.Any<Ship>())
      //  {
      //    this.ShowAlert("[g]executing DeGrapple");
      //    this.DoRepair((PlayerCombatEntity)source.First<Ship>());
      //    //          goto label_53;   // LOOP removed to prevent hitting the nerf.
      //    this._lastRepair = DateTime.Now;
      //    goto label_99; // EXIT Healing.
      //  }
      //}
      //if (this.AutoFriendHeal && (int)this._myShip.Metals > 0)
      //{
        //IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>)(x => x.Hull < this.FriendHealAmount && x.MaxHull - x.Hull > 2)); // > 0 is original value. changed to > 2 to avoid 1hp nerf.
        //if (source.Any<Ship>())
        //{
        //  this.ShowAlert("[g]FRIENDheal Section!");
        //  // suppress on cluster
        //  Garrison garrison = this._local.Garrison;
        //  if (garrison != null && garrison.Relation == RelationType.FRIEND)
        //  {
        //    return;
        //  }
        //  else
        //  {
        //    //_reptarget = this.DoRepair((PlayerCombatEntity)source.OrderByDescending<Ship, int>((Func<Ship, int>)(x => x.MaxHull - x.Hull)).First<Ship>());
        //    this.DoRepair((PlayerCombatEntity)source.OrderByDescending<Ship, int>((Func<Ship, int>)(x => x.MaxHull - x.Hull)).First<Ship>());
        //    this._lastRepair = DateTime.Now;
        //    this.ShowAlert("[b]Executing repair!");
        //  }
        //}
        //IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>)(x => x.EffectiveHull < this.FriendHealAmount && x.EffectiveMaxHull - x.EffectiveHull > 2));
        //if (source.Any<Ship>())
        //{
        //  this.ShowAlert("[b]Name: " + source.First<Ship>().Name);
        //  this.ShowAlert("[b]Effective HULL: " + source.First<Ship>().EffectiveHull);
        //  this.ShowAlert("[b]Effective Max Hull: " + source.First<Ship>().EffectiveMaxHull);
        //}
        // WORKING VERSION below!!
        //if (this._local.FriendlyShips.Any<Ship>((Func<Ship, bool>)(x => (double)x.EffectiveHull < this.FriendHealAmount && x.EffectiveMaxHull - x.EffectiveHull > 2)))
        //{
        //  this.DoRepair((PlayerCombatEntity)this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => (double)x.EffectiveHull < this.FriendHealAmount && x.EffectiveMaxHull - x.EffectiveHull > 2)));
        //  this._lastRepair = DateTime.Now;
        //  this.ShowAlert("[r]Executed repair!");
        //}
        // WORKING VERSION HERE!!
        //if (this._local.FriendlyShips.Any<Ship>((Func<Ship, bool>)(x => (double)x.Hull < (double)x.MaxHull * 0.8)))
        //{
        //  this.DoRepair((PlayerCombatEntity)this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => (double)x.Hull < (double)x.MaxHull * 0.8)));
        //  this._lastRepair = DateTime.Now;
        //  this.ShowAlert("[r]Executed repair!");
        //}

      //} // END OF HEALING SECTION
      label_99: // exit when 1 of the healing items has been executed. No need to check the others.
//      this.ShowAlert("[g]END of HEALING Section!");
      // AUTOFILL
      if (this.AutoFill && this._local.Contains(EntityType.STARPORT) && (int) this._myShip.Metals < this._myShip.ResourceCapacity)
        this.DoResourcesBuy(0, 0, this._myShip.ResourceCapacity - (int) this._myShip.Metals, 0, 0);
      // AUTORESEARCH - Work in Progress
      if (this._autoResearch)
      {
        if (this._local.Contains(EntityType.STARPORT) && (int) this._myShip.Metals + (int) this._myShip.Gas + (int) this._myShip.DarkMatter + (int) this._myShip.Radioactives + (int) this._myShip.Organics < this._myShip.ResourceCapacity * 5)
          this.DoResourcesBuy(this._myShip.ResourceCapacity - (int) this._myShip.Organics, this._myShip.ResourceCapacity - (int) this._myShip.Gas, this._myShip.ResourceCapacity - (int) this._myShip.Metals, this._myShip.ResourceCapacity - (int) this._myShip.Radioactives, this._myShip.ResourceCapacity - (int) this._myShip.DarkMatter);
        if (this._local.Contains(EntityType.GARRISON) && !string.IsNullOrEmpty(this._researchTarget) && this._myLoc.Corporation.Name == this._researchTarget)
        {
          if ((int) this._myShip.DarkMatter > 0)
            this.DoDevelopment((PlayerCombatEntity) this._local.Garrison, ResourceType.DARKMATTER);
          if ((int) this._myShip.Metals > 0)
            this.DoDevelopment((PlayerCombatEntity) this._local.Garrison, ResourceType.METAL);
          if ((int) this._myShip.Gas > 0)
            this.DoDevelopment((PlayerCombatEntity) this._local.Garrison, ResourceType.GAS);
          if ((int) this._myShip.Organics > 0)
            this.DoDevelopment((PlayerCombatEntity) this._local.Garrison, ResourceType.ORGANIC);
          if ((int) this._myShip.Radioactives > 0)
            this.DoDevelopment((PlayerCombatEntity) this._local.Garrison, ResourceType.RADIOACTIVE);
        }
      }
      // REFOLLOW
      if (this._foltar != "")
      {
        //this.ShowAlert("HAIL the NEW Slave: " + this._foltar);
        //this._NewFollowTarget = (CombatEntity)this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => x.Title.ToLower() == this._foltar));
        this._NewFollowTarget = (CombatEntity)(this._local.FriendlyShips.First((Func<Ship, bool>)(x => x.Title.Equals(this._foltar))));
        this.DoFollow(this._NewFollowTarget);
        this.ShowAlert("HAIL the NEW Driver: " + this._NewFollowTarget);
        this._foltar = "";
      }

      if (this._autoResPlanet)
      {
        if (this._local.Contains(EntityType.STARPORT) && (int) this._myShip.Metals + (int) this._myShip.Gas + (int) this._myShip.DarkMatter + (int) this._myShip.Radioactives + (int) this._myShip.Organics < this._myShip.ResourceCapacity * 5)
        {
          this.DoResourcesBuy(this._myShip.ResourceCapacity - (int) this._myShip.Organics, this._myShip.ResourceCapacity - (int) this._myShip.Gas, this._myShip.ResourceCapacity - (int) this._myShip.Metals, this._myShip.ResourceCapacity - (int) this._myShip.Radioactives, this._myShip.ResourceCapacity - (int) this._myShip.DarkMatter);
        }
        Planet planet = this._local.Planet;
        if (planet != null && planet.Relation == RelationType.FRIEND)// && !string.IsNullOrEmpty(this._researchTarget) //&& this._worldTimeMS - this._lastTfTime > 100L)
        {
          if ((int)this._myShip.DarkMatter > 0)
            this.DoDevelopment((PlayerCombatEntity)planet, ResourceType.DARKMATTER);
          if ((int)this._myShip.Metals > 0)
            this.DoDevelopment((PlayerCombatEntity)planet, ResourceType.METAL);
          if ((int)this._myShip.Gas > 0)
            this.DoDevelopment((PlayerCombatEntity)planet, ResourceType.GAS);
          if ((int)this._myShip.Organics > 0)
            this.DoDevelopment((PlayerCombatEntity)planet, ResourceType.ORGANIC);
          if ((int)this._myShip.Radioactives > 0)
            this.DoDevelopment((PlayerCombatEntity)planet, ResourceType.RADIOACTIVE);
          this._lastTfTime = this._worldTimeMS;                    
        }
      }
      //AUTOSELL
      int num7T;
      int num7M;
      timeSpan = DateTime.Now - this._lastSale;
      num7T = timeSpan.TotalMilliseconds > _RealismMS_SP ? 1 : 0;
      num7M = this.MoveCooldown.RemainingMS <= _RealismMS_SP ? 1 : 0;
      if ((this.AutoSell && this._local.Contains(EntityType.STARPORT) && (((int) this._myShip.Gas + (int) this._myShip.Organics + (int) this._myShip.DarkMatter + (int) this._myShip.Radioactives +((int)this._myShip.Metals - this.MinimumMetals) > 100) || (this.AutoSellAll) || (this.AutoSellHalf) || (this.AutoSellNon))))// && (int) this._myShip.Metals > 0) // && this._worldTimeMS - this._lastSellTime > 300L
      {
        if (/*_realism == false || this.MoveCooldown.RemainingMS <= _RealismMS_SP */ num7M == 1 && num7T == 1)
        {
          if ((this._myShip.Special != null && this._myShip.Special.Class == SpecialClass.TECHNICIAN) || ((!AutoSellAll) && (!AutoSellHalf)))
          {
            if ((this._myShip.Special != null && this._myShip.Special.Class == SpecialClass.TECHNICIAN) && AutoSellHalf)
            {
              this.DoResourcesSell((int)this._myShip.Organics, (int)this._myShip.Gas, (int)this._myShip.Metals / 2, (int)this._myShip.Radioactives, (int)this._myShip.DarkMatter);
            }
            else
            {
              this.DoResourcesSell((int)this._myShip.Organics, (int)this._myShip.Gas, (int)this._myShip.Metals > this.MinimumMetals ? (int)this._myShip.Metals - this.MinimumMetals : 0, (int)this._myShip.Radioactives, (int)this._myShip.DarkMatter);
            }
          }
          else if (AutoSellAll)
          {
            this.DoResourcesSell((int)this._myShip.Organics, (int)this._myShip.Gas, (int)this._myShip.Metals , (int)this._myShip.Radioactives, (int)this._myShip.DarkMatter);
          }
          else if (AutoSellHalf)
          {
             this.DoResourcesSell((int)this._myShip.Organics, (int)this._myShip.Gas, (int)this._myShip.Metals / 2, (int)this._myShip.Radioactives, (int)this._myShip.DarkMatter);
          }
          else if (AutoSellNon)
          {
            this.DoResourcesSell((int)this._myShip.Organics, (int)this._myShip.Gas, 0, (int)this._myShip.Radioactives, (int)this._myShip.DarkMatter);
          }
            //  this._lastSellTime = this._worldTimeMS;
          this._lastSale = DateTime.Now;
          this._autoSellNon = false;
          this._autoSellHalf = false;
          this._autoSellAll = false;
          _RealismMS_SP = _rand.Next(400, 1900); // REALISM - WAIT MS for NEXT action
          this._autoSellPhase = 0; // reset autosell phase
        }
      }
      if (this.AutoMoney && this._local.Values.Count<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.CARGO_MONEY)) > 0 && this._worldTimeMS - this._lastMoneyPickup > 100L)
      {
        this._lastMoneyPickup = this._worldTimeMS;
        this.DoLoot((CargoEntity) this._local.Values.First<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.CARGO_MONEY)));
      }
      if (this.GrabIt && this._local.Values.Count<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.CARGO_ITEM)) > 0)
      {
        foreach (CargoEntity target in this._local.Values.Where<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.CARGO_ITEM)).Select<Entity, CargoEntity>((Func<Entity, CargoEntity>) (x => (CargoEntity) x)))
          this.DoLoot(target);
      }

      // first attempt on AUTOHARVEST
      if (this.AutoHarvest && /*this._local.Values.Count<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.ASTEROID)) > 0 */ this._local.Contains(EntityType.ASTEROID) && this._harvestCooldown.IsFinished)
      {
        // DO NOT HARVEST WHEN FRIENDLIES OR REDS ARE IN SECTOR
        if (this._local.EnemyShipCount >= 1)// || (this._local.FriendlyShipCount > 1))// || (this._local.FriendlyShipCount == 1 && (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Player.Name == this.AutoFeedTarget && GameState.NeedsFeeding(x))) > 0)
        {
          this.ShowAlert("SKIP Autoharvest RED in SECTOR");
          goto skiproid;
        }
        //else if (this._local.FriendlyShipCount == 1 && (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Player.Name == this.AutoFeedTarget && GameState.NeedsFeeding(x))) > 0))
        else if (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Special.Class == SpecialClass.PROSPECTOR )) > 1)
        {
          this.ShowAlert("SKIP Autoharvest - Multiple Prospectors in Sector");
          goto skiproid;
        }
        else
        {
          //this.ShowAlert("Testing Autoharvest");
          Asteroid target = (Asteroid)this._local.Values.First<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.ASTEROID));
          this._net.Send(RequestHarvest.Create(target));
          //this.AutoHarvest = false;
        }
        // do not roid
      }
      //
      skiproid:

      if (this.AutoRes && this._local.Values.Count<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.CARGO_RESOURCE)) > 0)
      {
        // SUPPRESS ON jettison sector + REALISM
        int num9;
        timeSpan = DateTime.Now - _lastRes;
        num9 = timeSpan.TotalMilliseconds > _RealismMS_RES ? 1 : 0;
        // realism changed to true, || before remainingMS changed to &&
        if ((this._jettisonSector == null || this._jettisonSector != this._myLoc) && (/*_realism == true && */this.MoveCooldown.RemainingMS <= _RealismMS_RES) && (num9 == 1))
        //        IEnumerable<Npc> npcs = this._local.Values.Where<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.NPC)).Select<Entity, Npc>((Func<Entity, Npc>)(x => (Npc)x));
        {
          foreach (CargoResource cargoResource in this._local.Values.Where<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.CARGO_RESOURCE)).Select<Entity, CargoResource>((Func<Entity, CargoResource>)(x => (CargoResource)x)))
          {
            //          if (this._myShip.GetResourceCount(cargoResource.Resource) < this._myShip.ResourceCapacity && !npcs.Any<Npc>((Func<Npc, bool>)(x => x.IsSeed())))
            if (this._myShip.GetResourceCount(cargoResource.Resource) < this._myShip.ResourceCapacity)
            { 
              this.DoLoot((CargoEntity)cargoResource);
              goto realism_loot;
            }
          }
          realism_loot:
          this._lastRes = DateTime.Now;
          _RealismMS_RES = _rand.Next(250, 400); // REALISM
        }
      }
      // JETTISON

      if (this.AutoJetti)
      {
        int num_Jetti;
        timeSpan = DateTime.Now - JettiTrigger;
        num_Jetti = timeSpan.TotalMilliseconds > _RealismMS_JETTI ? 1 : 0;
        if (num_Jetti == 1)
        {
          DoResourcesJettison(0, 0, (int)this._myShip.Metals, 0, 0);
          _RealismMS_JETTI = _rand.Next(400, 2000);
          AutoJetti = false;
        }
      }
      // END JETTISON

      // RIFT JUMP
      // autoRjPhases:
      // 0 - OFF
      // 1 - Calculate ShipJump, StackJump and Delay(F10 forces phase 1)
      // 2 - Wait for delay to pass
      // 3 - Buy RJ
      // 4 - Wait for RJ to happen(check text or something ?) Scan for Text: JUMP FAILED
      // 5 - Reset follow - turn off
      if (this.AutoRjPhase > 0 && AutoRjPhase <= 5)
      {
//        this.ShowAlert("AutoRJ Phase section");
        switch (this.AutoRjPhase)
        {
          case 1:
            this.ShowAlert("AutoRJ Initiated");
            RjTrigger = DateTime.Now;
            JumpSpeed();
            this.AutoRjPhase++;
            break;
          case 2:
            //this.ShowAlert("AutoRJ Phase 2");
            int num_RJ;
            timeSpan = DateTime.Now - RjTrigger;
            num_RJ = timeSpan.TotalMilliseconds > this._RJDelay ? 1 : 0;
            if (num_RJ == 1)
            {
              //this.ShowAlert("AutoRJ Phase 2 - Delay passed");
              this.AutoRjPhase++; // TEMP to test Phase
            }
            //else
            //{
            //  this.ShowAlert("AutoRJ Phase 2 - Waiting to finish Delay");
            //}
            break;
          case 3:
            //this.ShowAlert("AutoRJ Phase 3 - Buy RJ");
            this.DoPurchase(PurchaseItem.RiftJump);
            RjPurchase = DateTime.Now;
            this.AutoRjPhase++;
            break;
          case 4:
            //this.ShowAlert("AutoRJ Phase 4 - Wait for RJ to finish");
            if ((!this._myLoc.Rift && this._myLoc.IsSol) || (this._myLoc.Rift && this._myLoc.Corporation != null) && (this._myLoc.Corporation.Name == this._myPlayer.MyCorporation.Name)) // at earth of corp
            {
              this.ShowAlert("AutoRJ Phase X - At EARTH or GARRI - STOP RJ");
              this.AutoRjPhase = 0;
            }
            _RJ_Pur = timeSpan.TotalMilliseconds > this._SpeedSelf ? 1 : 0;
            if (_RJ_Pur == 1)
            {
              //this.ShowAlert("AutoRJ Phase 4 - RJ Cooldown finished");
              this.AutoRjPhase++;
            }
            break;
          case 5:
            //this.ShowAlert("AutoRJ Phase 5");
            int num_RJE;
            timeSpan = DateTime.Now - RjPurchase;
            num_RJE = timeSpan.TotalMilliseconds > _RJ_Ref_Delay ? 1 : 0;
            if (num_RJE == 1)
            {
              if (this.MyFollowTarget != this._driver && this._local.Contains((Entity)this._driver))
              {
                this.ShowAlert("AutoRJ Done - Refollow Driver");
                this.DoFollow(this._driver);
                this.AutoRjPhase = 0;
              }
            }        
            break;
          default:
            this.AutoRjPhase = 0;
            break;
        }
      }
      // END RIFT JUMP

      timeSpan = DateTime.Now - this._lastWAAAlertTime;
      if (timeSpan.TotalSeconds > 30.0 && this._local.EnemyShips.Count<Ship>((Func<Ship, bool>) (x => x.Inventory != null && x.Inventory.Count != 0 && x.Inventory.Count<EquipmentItem>((Func<EquipmentItem, bool>) (y => y.LongName.IndexOf("Weird") > -1)) > 0)) > 0)
      {
        this.ShowAlert("<b>[r]ENEMY WAA IN SECTOR");
        this._lastWAAAlertTime = DateTime.Now;
      }
      timeSpan = DateTime.Now - this._lastStealAlertTime;
      if (timeSpan.TotalSeconds > 30.0 && this._myShip.AllEquippedItems.Count<EquipmentItem>((Func<EquipmentItem, bool>) (x => (int) x.Durability <= 25)) > 0)
      {
        this.ShowAlert("<b>[r]YOU ARE WEARING STEALABLE GEAR");
        this._lastStealAlertTime = DateTime.Now;
      }
      if (this.AutoScrap && this._myShip.Inventory.Count<EquipmentItem>(GameState.IsScrappable()) > 0)
        this.DoItemSell((StarPort) null, this._myShip.Inventory.First<EquipmentItem>(GameState.IsScrappable()));
      if (this.AutoDecon && this._myShip.Inventory.Count<EquipmentItem>(GameState.IsDecon()) > 0)
        this.DoItemEngineer(EngineeringType.DECONSTRUCT, this._myShip.Inventory.First<EquipmentItem>(GameState.IsDecon()));
      // AUTOFEED
      if (this.AutoFeed && (int) this._myShip.Metals > 0)
      {
        string target = "";
        if (string.IsNullOrEmpty(this.AutoFeedTarget))
        {
          if (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => GameState.IsRescue(x) && GameState.NeedsFeeding(x))) > 0)
            target = this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => GameState.IsRescue(x) && GameState.NeedsFeeding(x))).Player.Name;
        }
        /*        else if (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Player.Name == this.AutoFeedTarget && GameState.NeedsFeeding(x))) > 0)
                {
                  target = this.AutoFeedTarget;
                }*/
        else
        {
          if (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Player.Name == this.AutoFeedTarget && GameState.NeedsFeeding(x))) > 0)
          {
            target = this.AutoFeedTarget;
          }
          else if (this.BackupFeed && (int)this._myShip.Metals > 0)
          {
            if (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Player.Name == this.BackupFeedTarget && GameState.NeedsFeeding(x))) > 0)
            {
              target = this.BackupFeedTarget; // BACKUP FEED TARGET
            }
          }
          else
          {
            _FeedDelayTmr = DateTime.Now;
          }
        }
        if (!string.IsNullOrEmpty(target))
        {
          int num8;
          timeSpan = DateTime.Now - this._lastFeed;
          num8 = timeSpan.TotalMilliseconds > _RealismMS_FEED ? 1 : 0;
          int num10;
          timeSpan = DateTime.Now - this._FeedDelayTmr;
          num10 = timeSpan.TotalMilliseconds > _RealismMS_FEED ? 1 : 0;
          Ship target1 = this._local.FriendlyShips.First<Ship>((Func<Ship, bool>) (x => x.Player.Name == target && GameState.NeedsFeeding(x)));
          if (target1 != null && (int) target1.Metals < target1.ResourceCapacity && num8 == 1 && num10 == 1)
          {
            int metals = target1.ResourceCapacity - (int)target1.Metals > (int)this._myShip.Metals ? (int)this._myShip.Metals : target1.ResourceCapacity - (int)target1.Metals;
            if (metals > 25)
            { // only feed when more than 25 metals 
              this.DoResourcesTransfer(target1, 0, 0, metals, 0, 0);
              this._lastFeed = DateTime.Now;  // Timestamp of last FEED
              _RealismMS_FEED = _rand.Next(750, 2500); // REALISM
            }  // feed to oom the feeding ship
            else if (metals == (int)this._myShip.Metals)
            {
              this.DoResourcesTransfer(target1, 0, 0, metals, 0, 0);
              this._lastFeed = DateTime.Now;  // Timestamp of last FEED
              _RealismMS_FEED = _rand.Next(750, 2500); // REALISM
            }
          }
        }
      }
      if (this._myLoc.Marked && this._local.Values.Count<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.CARGO_MONEY)) == 0 && !this._myLoc.IsInvasion())
        this._myLoc.UnMark();
      int num4;
      if (this.Invasions)
      {
        timeSpan = DateTime.Now - this._markedInvasionsTimestamp;
        num4 = timeSpan.TotalSeconds <= 60.0 ? 1 : 0;
      }
      else
        num4 = 1;
      if (num4 == 0)
      {
        Sector[,] riftSpace = this._map.RiftSpace;
        int upperBound1 = riftSpace.GetUpperBound(0);
        int upperBound2 = riftSpace.GetUpperBound(1);
        for (int lowerBound1 = riftSpace.GetLowerBound(0); lowerBound1 <= upperBound1; ++lowerBound1)
        {
          for (int lowerBound2 = riftSpace.GetLowerBound(1); lowerBound2 <= upperBound2; ++lowerBound2)
          {
            Sector sector = riftSpace[lowerBound1, lowerBound2];
            sector.Mark();
            if (!sector.Alerted && sector.Marked && !sector.IsInvasion())
            {
              sector.Alerted = true;
              this.ShowAlert("[r]Sector update: " + sector.ToString());
            }
          }
        }
        Sector[,] normalSpace = this._map.NormalSpace;
        int upperBound3 = normalSpace.GetUpperBound(0);
        int upperBound4 = normalSpace.GetUpperBound(1);
        for (int lowerBound1 = normalSpace.GetLowerBound(0); lowerBound1 <= upperBound3; ++lowerBound1)
        {
          for (int lowerBound2 = normalSpace.GetLowerBound(1); lowerBound2 <= upperBound4; ++lowerBound2)
          {
            Sector sector = normalSpace[lowerBound1, lowerBound2];
            sector.Mark();
            if (!sector.Alerted && sector.Marked && !sector.IsInvasion())
            {
              sector.Alerted = true;
              this.ShowAlert("[r]Sector update: " + sector.ToString());
            }
          }
        }
        this.ShowAlert("Invasions Marked");
        this._markedInvasionsTimestamp = DateTime.Now;
      }
      if (this._autoFarm && this._myAttackTarget == null)
      {
        if ((int) this._myShip.Metals == this._myShip.ResourceCapacity)
        {
          this.Course.Set(40, 40);
          this.Course.Resume();
        }
        else if (this._local.Values.Any<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC)))
        {
          CombatEntity target = (CombatEntity) this._local.Values.First<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC));
          this.DoAttack(target);
          this.DoFollow(target);
        }
        else
        {
          int num5;
          if (this.MoveCooldown.IsFinished)
          {
            timeSpan = DateTime.Now - this._lastMove;
            num5 = timeSpan.TotalMilliseconds <= 1000.0 ? 1 : 0;
          }
          else
            num5 = 1;
          if (num5 == 0)
          {
            Direction dir = (Direction) RNG.RandomDirection();
            bool flag = false;
            while (!flag)
            {
              Sector neighbor = this._myLoc.GetNeighbor(dir);
              if (neighbor != null && neighbor.Class != SectorClass.CONTESTED && (this._myLoc.IsSol || neighbor.Class != SectorClass.PROTECTED || this._myLoc.Connections().All<Sector>((Func<Sector, bool>) (x => x.Class == SectorClass.PROTECTED))) && (this._previousSector == null || this._myLoc.Connections().Count<Sector>((Func<Sector, bool>) (x => x.Class != SectorClass.CONTESTED)) == 1 || (this._myLoc.Connections().Count<Sector>((Func<Sector, bool>) (x => x.Class != SectorClass.PROTECTED)) == 1 || neighbor.ToString() != this._previousSector.ToString())) && (neighbor.Class != SectorClass.PROTECTED || this._myLoc.Connections().All<Sector>((Func<Sector, bool>) (x => x.Class == SectorClass.PROTECTED))))
              {
                this._lastMove = DateTime.Now;
                this._previousSector = this._myLoc;
                this._net.Send(RequestMove.Create(dir));
                flag = true;
              }
              else
                dir = (Direction) RNG.RandomDirection();
            }
          }
        }
      }
      if (this._autoFarmBlackDriver)
      {
        if (this.Panic)
          this._autoFarmBlackDriver = false;
        else if (this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>)(x => x.Stunned)) > 0)
        {
          if (!this.Course.Paused)
            this.Course.Pause();
          if (this._myFollowTarget != null)
            this.DoFollow((CombatEntity)null);
        }
        // hold to pick up credits/bds
        else if (this._local.Values.Count<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.CARGO_MONEY)) > 0)
        {
          if (!this.Course.Paused)
            this.Course.Pause();
        }
        else if (this.Course.Count <= 0 || this.Course.Paused)
        {
          if (this._local.FriendlyShips.Sum<Ship>((Func<Ship, int>)(x => (int)x.Metals)) == this._local.FriendlyShips.Sum<Ship>((Func<Ship, int>)(x => x.ResourceCapacity)))
          {
            if (this._local.FriendlyShips.Any<Ship>((Func<Ship, bool>)(x => (double)x.Hull < (double)x.MaxHull * 0.8)))
            {
              this.DoRepair((PlayerCombatEntity)this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => (double)x.Hull < (double)x.MaxHull * 0.8)));
            }
            else
            {
              int num5;
              if (this.Course.Paused && this._local.FriendlyShipCount >= this._shipsInFleet && this.MoveCooldown.IsFinished)
              {
                timeSpan = DateTime.Now - this._lastMove;
                num5 = timeSpan.TotalMilliseconds <= 1000.0 ? 1 : 0;
              }
              else
                num5 = 1;
              if (num5 == 0)
              {
                Sector target = this.NearestSP();
                this.ShowAlert(target.ToString());
                this.Course.Set(target);
                this.Course.Resume();
              }
            }
          }
          else if (this._local.Values.Any<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.NPC && ((CombatEntity)x).Hull <= 12000)) && this._local.Values.Count<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.NPC && ((CombatEntity)x).Hull > 12000)) == 0)
          {
            if (this._myAttackTarget == null)
            {
              CombatEntity npc = (CombatEntity)this._local.Values.First<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.NPC && ((CombatEntity)x).Hull <= 12000));
              this.DoAttack(npc);
              if (npc.Type == EntityType.NPC && this._myLoc.Connections().Count<Sector>((Func<Sector, bool>)(s => this.NpcMatchesInvasion(s.Status, ((Npc)npc).Faction))) == 0)
                this.DoFollow(npc);
            }
            else if (this._myFollowTarget == null && this._myAttackTarget.Type == EntityType.NPC && this._myLoc.Connections().Count<Sector>((Func<Sector, bool>)(s => this.NpcMatchesInvasion(s.Status, ((Npc)this._myAttackTarget).Faction))) == 0)
              this.DoFollow(this._myAttackTarget);
          }
          else if (this._myLoc.IsGreyShallows() || this._myLoc.Connections().All<Sector>((Func<Sector, bool>)(x => x.IsGreyShallows() || x.IsInvasion())))
          {
            Sector sectorToMoveTo = this.CalculateSectorToMoveTo();
            if (this.Course.Paused)
            {
              this.Course.Set(sectorToMoveTo);
              this.Course.Resume();
            }
          }
          else
          {
            int num5;
            if (this.MoveCooldown.IsFinished)
            {
              timeSpan = DateTime.Now - this._lastMove;
              if (timeSpan.TotalMilliseconds > 1000.0)
              {
                num5 = this._local.Contains(EntityType.CARGO_RESOURCE) ? 1 : 0;
                goto label_175;
              }
            }
            num5 = 1;
            label_175:
            if (num5 == 0)
            {
              Direction dir = (Direction)RNG.RandomDirection();
              bool flag = false;
              while (!flag)
              {
                Sector neighbor = this._myLoc.GetNeighbor(dir);
                if (neighbor != null && (!neighbor.IsInvasion() || this._myLoc.InvasionFreeConnections() == 0) && !neighbor.IsGreyShallows() && (this._previousSector == null || this._myLoc.Connections().Count<Sector>((Func<Sector, bool>)(x => !x.IsGreyShallows() && !x.IsInvasion())) == 1 || neighbor.ToString() != this._previousSector.ToString()))
                {
                  this._lastMove = DateTime.Now;
                  this._previousSector = this._myLoc;
                  this._net.Send(RequestMove.Create(dir));
                  flag = true;
                }
                else
                  dir = (Direction)RNG.RandomDirection();
              }
            }
          }
        }
      }
      if (this._autoFarmBlackGun)
      {
        if (this.MyFollowTarget != this._driver && this._local.Contains((Entity) this._driver))
          this.DoFollow(this._driver);
        if (this._local.FriendlyShips.Sum<Ship>((Func<Ship, int>) (x => (int) x.Metals)) != this._local.FriendlyShips.Sum<Ship>((Func<Ship, int>) (x => x.ResourceCapacity)) && !this.Panic && !this._myShip.Stunned && (this._local.Values.Any<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC)) && !this.Panic) && (this._myAttackTarget != null && !this._local.Contains((Entity) this._myAttackTarget) && this._local.Contains((Entity) this.MyFollowTarget) || this._myAttackTarget == null))
          this.DoAttack((CombatEntity) this._local.Values.First<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC)));
      }
      // STOP FIRING WHEN JUMPED INSIDE AN INVY/RIFTY
      if (this.AutoGun && this._local.EnemyShips.Any<Ship>() && this._myLoc.IsInvasion())
      {
        this.AutoGun = false;
        this.ShowAlert("REDS! - HOLD GUN");
      }

      if (this.AutoGun && this._local.Values.Any<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC)))
      {
        IEnumerable<Npc> npcs = this._local.Values.Where<Entity>((Func<Entity, bool>) (x => x.Type == EntityType.NPC)).Select<Entity, Npc>((Func<Entity, Npc>) (x => (Npc) x));
        if (npcs.Count<Npc>() == 1 && (npcs.All<Npc>((Func<Npc, bool>) (x => x.IsSeed())) && (double) npcs.First<Npc>().Hull < (double) this._myShip.MaxCrit * 2.5) && this.SomeoneHasABetterAlienHunterInStack())
        {
          if (this._myAttackTarget != null)
          {
            this.DoAttack((CombatEntity) null);
          }
        }
        else if (this._myAttackTarget == null || this._myAttackTarget != null && !this._local.Contains((Entity) this._myAttackTarget) && (this.MyFollowTarget == null || this._local.Contains((Entity) this.MyFollowTarget)))
        {
          Npc npc = !npcs.Any<Npc>((Func<Npc, bool>) (x => x.IsSeed())) || npcs.Count<Npc>() <= 1 ? this._attackOrder.SelectTargetFrom(npcs) : this._attackOrder.SelectTargetFrom(npcs.Where<Npc>((Func<Npc, bool>) (x => !x.IsSeed())));
          if (npc != this._myAttackTarget)
            this.DoAttack((CombatEntity) npc);
        }
      }
      if (this.PanicMode && (this._myLoc.IsInvasion() || this._local.EnemyShips.Count<Ship>((Func<Ship, bool>) (x => x.PvPFlag)) > 3) && !this.Panic && this._myShip.PvPFlag)
      {
        this.Panic = true;
        if (DiscordBot.WebHooks.ContainsKey(this._myPlayer.Name))
          DiscordBot.SendPM(new DiscordMessage()
          {
            icon_url = "",
            text = "Panic ON",
            username = this._myPlayer.Name,
            webook = DiscordBot.WebHooks[this._myPlayer.Name]
          });
      }
      if (this.Panic && (!this._myLoc.Rift && this._myLoc.IsSol || this._myLoc.Rift && this._myLoc.Corporation != null && this._myLoc.Corporation.Name == this._myPlayer.MyCorporation.Name))
        this.Panic = false;
      else if (this.Panic)
      {
        if (this._earthJumpTime.IsFinished)
        {
          int num5;
          if (this._myShip.Special != null && this._myShip.Special.Class == SpecialClass.TECHNICIAN)
          {
            if (this._local.FriendlyShipCount < 2 && this.MoveCooldown.IsFinished)
            {
              timeSpan = DateTime.Now - this._lastMove;
              num5 = timeSpan.TotalMilliseconds <= 1000.0 ? 1 : 0;
            }
            else
              num5 = 1;
          }
          else
            num5 = 0;
          if (num5 == 0)
          {
            this.DoFollow((CombatEntity) null);
            this._net.Send(RequestEarthJump.Create(this._myLoc.Rift));
//            this.ShowAlert("[lb]:!SHOWN!");
//            this.ShowAlert("[r]:!SHOWN!");
//            this.Panic = false;
          }
        }
        if (this._myShip.Special != null && this._myShip.Special.Class == SpecialClass.TRACTOR && this.AttackCooldown.IsFinished)
        {
          CombatEntity target = (CombatEntity) null;
          if (this.AttackCooldown.IsFinished && this._local.EnemyShips.Count<Ship>((Func<Ship, bool>) (x => (x.Special == null || x.Special.Class != SpecialClass.DEFLECTOR) && x.Class != ShipClass.Carrier)) > 0)
            target = (CombatEntity) this._local.EnemyShips.Where<Ship>((Func<Ship, bool>) (x => (x.Special == null || x.Special.Class != SpecialClass.DEFLECTOR) && x.Class != ShipClass.Carrier)).OrderBy<Ship, bool>((Func<Ship, bool>) (x => x.Stunned)).ThenBy<Ship, float>((Func<Ship, float>) (x => x.StunResist)).First<Ship>();
          if (this.MyAttackTarget != target)
            this.DoAttack(target);
        }
      }
      if (this.AutoEngage && (this._myLoc.Status == SectorStatus.ENGAGED && this.MyAttackTarget == null))
      {
        if (this._local.Contains(EntityType.DEFENSE_PLATFORM))
          this.DoAttack((CombatEntity)this._local.Values.First<Entity>((Func<Entity, bool>)(x => x.Type == EntityType.DEFENSE_PLATFORM)));
        else if (this._local.Contains(EntityType.INTRADICTOR))
          this.DoAttack((CombatEntity)this._local.Intradictor);
        else if (this._local.Contains(EntityType.GARRISON))
          this.DoAttack((CombatEntity)this._local.Garrison);
        else if (this._local.Contains(EntityType.PLANET))
          this.DoAttack((CombatEntity)this._local.Planet);
      }
      int num6;
      if (this._unsentMessages.Any<Message>())
      {
        timeSpan = DateTime.Now - this._lastMessageSent;
        num6 = timeSpan.TotalMilliseconds <= 300.0 ? 1 : 0;
      }
      else
        num6 = 1;
      if (num6 != 0)
        return;
      Message message = this._unsentMessages.Dequeue();
      this.DoPrivateChat(message.Text, message.Player);
      this._lastMessageSent = DateTime.Now;
    }


    // determine slowest ship in stack
    //private int SlowestShipSpeed()
    //{
    //  IEnumerable<Ship> shiplist = this._local.FriendlyShips
    //  .Max<Ship>((Func<Ship, int>)(x => x.MoveSpeedMS))); //(Max<Ship>((Func<Ship, int>) (x => x.MoveSpeedMS));
    //  if (!shiplist.Any<Ship>())
    //    return 20000;
    //  return shiplist.OrderByDescending<Ship>, (<Func<Ship, int>) x.MoveSpeedMS);
    //  //OrderByDescending<Ship, MoveSpeedMS>((Func<Ship, MoveSpeedMS>)(x => x.MoveSpeedMS)).First<Ship>().MoveSpeedMS;
    //}
    private void JumpSpeed()
    {
      try
      {
        this.ShowAlert("[r]STACK SPEED: " + (object)this._local.FriendlyShips.Count<Ship>());
        this.StackMS(this._local.FriendlyShips);
        this.ShowAlert("[r]MyMS!");
        this.MyMS();

        //        _SpeedDriver = new MySpeed;

        this.ShowAlert("[r]CrewSpeed calculated: " + this._SpeedCrew);
        this.ShowAlert("[r]MySpeed calculated: "+ this._SpeedSelf);
        this._RJDelay = this._SpeedCrew - this._SpeedSelf;
        this.ShowAlert("[r]RJ Delay: " + this._RJDelay);
      }
      catch (Exception ex)
      {
        using (StreamWriter streamWriter = File.AppendText("C:\\logs\\errors.txt"))
          streamWriter.WriteLine((object)ex);
      }
    }
    private void StackMS(IEnumerable<Ship> stack2)
    {
      this.ShowAlert("[g]Speed: " + (int)((object)stack2.Max<Ship>((Func<Ship, int>)(x => x.MoveSpeedMS)))*1.5);
      this.ShowAlert("[r]DBL to INT - 1!");
      double dbl1;
      this.ShowAlert("[r]DBL to INT - 2!");
      //dbl1 = (double)((object)stack2.Max<Ship>((Func<Ship, int>)(x => x.MoveSpeedMS))) * 1.5;
      this.ShowAlert("[r]DBL to INT!");
     // _SpeedCrew = Convert.ToInt32(dbl1);
     _SpeedCrew = Convert.ToInt32((int)((object)stack2.Max<Ship>((Func<Ship, int>)(x => x.MoveSpeedMS))) * 1.5);
    }

    private void MyMS()
    {
      this.ShowAlert("My Speed: " + this._myShip.MoveSpeedMS);
      _SpeedSelf = (int)(this._myShip.MoveSpeedMS*1.5);
    }
//
    private bool SomeoneHasABetterAlienHunterInStack()
    {
      //        return this._myShip.Special == null || this._myShip.Special.Class != SpecialClass.ALIEN_HUNTER || this._myShip.Special.Rarity < this.HighestRarityAlienHunterInStack();
      if (this.HighestRarityAlienHunterInStack() != ItemRarity.NULL) // AH in stack
      {
        if (this._myShip.Special.Class != SpecialClass.ALIEN_HUNTER) /* No AH equipped -> AH in stack to kill */
        {
          return true;
        }
        else if ((this._myShip.Special.Class == SpecialClass.ALIEN_HUNTER) && (this._myShip.Special.Rarity < this.HighestRarityAlienHunterInStack()))  // AH but not the best in stack
          return true;
        else
        {
          return false;
        }
      }
      else
        return false;
    }

    private ItemRarity HighestRarityAlienHunterInStack()
    {
      IEnumerable<Ship> source = this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>) (x => x.Special != null && x.Special.Class == SpecialClass.ALIEN_HUNTER));
      if (!source.Any<Ship>())
        return ItemRarity.NULL;
      return source.OrderByDescending<Ship, ItemRarity>((Func<Ship, ItemRarity>) (x => x.Special.Rarity)).First<Ship>().Special.Rarity;
    }

    private bool NpcMatchesInvasion(SectorStatus status, NpcFaction faction)
    {
      return status == SectorStatus.HETEROCLITE && faction == NpcFaction.HETEROCLITE || status == SectorStatus.PIRATE && faction == NpcFaction.PIRATE || status == SectorStatus.WYRD && faction == NpcFaction.WYRD;
    }

    private Sector CalculateSectorToMoveTo()
    {
      int num1 = this._myLoc.X;
      int num2 = this._myLoc.Y;
      while (num1 > 0 && num1 <= 80 && num2 > 0 && num2 <= 80)
      {
        if (num1 >= 19 && num1 <= 60)
          num1 = this.AwayFrom39(num1, 1);
        if (num2 >= 19 && num2 <= 60)
          num2 = this.AwayFrom39(num2, 1);
        Sector sector = this._map.Get(this._myLoc.Rift, num1, num2);
        if (!sector.IsInvasion())
          return sector;
      }
      return this._map.Get(this._myLoc.Rift, 40, 40);
    }

    private int AwayFrom39(int i, int increment)
    {
      return i < 39 ? i - increment : i + increment;
    }

    private bool CustomCommands(string text)
    {
      try
      {
        if (this._textCommands.Any<ITextCommands>((Func<ITextCommands, bool>) (x => text.IndexOf(x.Trigger()) > -1)))
        {
          this._textCommands.First<ITextCommands>((Func<ITextCommands, bool>) (x => text.IndexOf(x.Trigger()) > -1)).Process(this, text);
          return true;
        }
        if (this._boolCommands.Any<string>((Func<string, bool>) (x => x.ToLower() == text.Replace(":", "").ToLower().Trim())))
        {
          this.ToggleBool(this._boolCommands.First<string>((Func<string, bool>) (x => x.ToLower() == text.Replace(":", "").ToLower().Trim())));
          return true;
        }
        if (this._functionalCommands.ContainsKey(text.Replace(":", "").Trim()))
        {
          this._functionalCommands[text.Replace(":", "").Trim()](this);
          return true;
        }
      }
      catch (Exception ex)
      {
        GameState.LogError(ex);
      }
      return false;
    }

    private void ShowCustomCommands()
    {
      this.ShowAlert("[r]Custom Commands [g](cAsE sEnSiTiVe)");
      this.ShowAlert("[g]Driving");
      this.ShowAlert("[lb]:info <statname> - shows a ship stat in SubTitle of ship (leave blank to get options)");
      this.ShowAlert("[lb]:stack - shows summary of friendly and enemy stacks");
      this.ShowAlert("[lb]:nostealth - shows friendly ships who arent stealthed");
      this.ShowAlert("[lb]:notfull - shows friendly ships who arent full");
      this.ShowAlert("[g]DTEAM");
      this.ShowAlert("[lb]:setdteam - saves alli members in current sector as dteam");
      this.ShowAlert("[lb]/dteam <message> - sends the message to the saved dteam");
      this.ShowAlert("[lb]:emulate - emulates dteam against enemies in current engagement");
      this.ShowAlert("[g]Healing");
      this.ShowAlert("[lb]:autodegrapple - automatically degrapple friends");
      this.ShowAlert("[lb]:autoselfheal - toggles Auto Self Heal");
      this.ShowAlert("[lb]:selfhealamount <number> - set HP to heal below (default 3000)");
      this.ShowAlert("[lb]replace self with friend to do the same for friends");
      this.ShowAlert("[lb]:autohealtime <number> - set minimum time in ms to wait between heals");
      this.ShowAlert("[g]Resources/Items");
      this.ShowAlert("[lb]:autosell - toggles automatically selling non-metals at starports");
      this.ShowAlert("[lb]:minmets <amount> - sets amount of metals to keep");
      this.ShowAlert("[lb]:autofill - toggles automatically filling at starports");
      this.ShowAlert("[lb]:autofeed <target> - automatically feeds target. Leave blank for rescues or to turn off");
      this.ShowAlert("[lb]:autoscrap - toggles automatically scrapping common weapons/armour/storage/harvesters (default ON)");
      this.ShowAlert("[lb]:autodecon - toggles automatically deconstructing uncommon weapons/armour/storage/harvesters (default OFF)");
      this.ShowAlert("[lb]:automoney - toggles automatically picking up credits/bds");
      this.ShowAlert("[lb]:autores - toggles automatically picking up resources");
      this.ShowAlert("[lb]:autoharvest - toggles automatically munching on roids");
      this.ShowAlert("[g]Utility");
      this.ShowAlert("[lb]:record - toggles kill count recording");
      this.ShowAlert("[lb]:resetkills - resets kill counts to 0");
      this.ShowAlert("[lb]:nbl - puts a 1 next to nobust garrisons (toggles off with :nbl)");
      this.ShowAlert("[lb]:invasions - toggles automatically marking invasions");
      this.ShowAlert("[lb]:routemode - toggles invasion free routing");
      this.ShowAlert("[lb]:sort <stat> asc/desc - sorts Column D ships by a specified stat");
      this.ShowAlert("[lb]:shouldlog - Logs combat and chat to file");
      this.ShowAlert("[lb]:roidchanges - highlights sectors with roid changes within the past 2 minutes in blue");
      this.ShowAlert("[lb]:seedsave - saves all strongholds, seed worlds and hives you scout");
      this.ShowAlert("[lb]:listseeds - lists anything found while :seedsave on");
      this.ShowAlert("[g]Other");
      this.ShowAlert("[lb]:gunmode - rotates through possible gun modes");
      this.ShowAlert("[lb]:gunmode - rotates through possible player gun modes");
      this.ShowAlert("[g]Wolf");
      this.ShowAlert("[lb]:eastdriver (2) - driver default");
      this.ShowAlert("[lb]:eastgun (1) - gun default");
      this.ShowAlert("[lb]:eurogun (3) - toggle res/sell/gun");
      this.ShowAlert("[lb]:eurogun (4) - toggle gun");
//      this.ShowAlert("[lb]:autoresplanet - auto terraform planet");
      this.ShowAlert("[lb]:automoney (5) - toggle on/off");
      this.ShowAlert("[lb]:autosell (6) - toggle on/off");
      this.ShowAlert("[lb]:autoharvest (8) - toggle on/off");
      this.ShowAlert("[lb]:clearjet (9) - clear jettison sector");
      this.ShowAlert("[lb]:setmule - set mule sector - (when in sector)");
      this.ShowAlert("[lb]:gomule - go to mule sector");
      //this.ShowAlert("[lb]:findroid - find nearest asteroid");
      this.ShowAlert("[g]SECTOR Commands");
      this.ShowAlert("[lb]Turn on AutoGun: wake up everyone / stop sleeping");
      this.ShowAlert("[lb]Turn off AutoMoney: stop scooping the bds / no more vacuuming / turn off the hoover");
    }

    private void Rescue()
    {
      this.AutoDegrapple = true;
      this.AutoRes = true;
      this.AutoFill = true;
      this.AutoFriendHeal = true;
      this.FriendHealAmount = 3000;
      if (this._myShip.Engine == null || this._myShip.Engine.Class != EngineClass.SKIP || this._myShip.Special == null || this._myShip.Special.Class != SpecialClass.TECHNICIAN)
        this.ShowPopup("You don't have either a skip or technician equipped", true);
      this.ShowAlert("Rescue Mode ON (degrapple, resource, fill, friend heal)");
    }

    private void AutoFarmBlackDriver()
    {
      if (this._bank.BlackDollars < 2)
      {
        this.ShowPopup("You have insufficient EJ BDs", true);
      }
      else
      {
        this._autoFarmBlackDriver = true;
        this.AutoDegrapple = true;
        if (this._local.FriendlyShipCount > 1)
        {
          this.AutoFriendHeal = true;
          this.FriendHealAmount = this._myLoc.Rift ? 6500 : 4500;
        }
        else
        {
          this.AutoSelfHeal = true;
          this.SelfHealAmount = this._myLoc.Rift ? 6500 : 4500;
        }
        this.AutoSell = true;
        this.AutoScrap = true;
        this.AutoMoney = true;
        this.AutoRes = true;
        GameState.InvasionFreeRouting = true;
        this.MinimumMetals = this._local.FriendlyShipCount > 1 ? this._myShip.ResourceCapacity : this._myShip.ResourceCapacity / 2;
//        this.PanicMode = true;
        this._shipsInFleet = this._local.FriendlyShipCount;
        this.Realism = true; // REALISM
        this.ShowAlert("Auto Farm Black Driver");
      }
    }

    private void AutoFarmBlackGun()
    {
      if (this._bank.BlackDollars < 2)
      {
        this.ShowPopup("You have insufficient EJ BDs", true);
      }
      else
      {
        this._driver = this.MyFollowTarget != null ? this.MyFollowTarget : (CombatEntity) this._local.FriendlyShips.First<Ship>((Func<Ship, bool>) (x => x.Special != null && x.Special.Class == SpecialClass.TECHNICIAN));
        this.DoFollow(this._driver);
        this._autoFarmBlackGun = true;
        this.AutoFeed = true;
        this.AutoFeedTarget = this._driver.Title;
        this.AutoSell = true;
        this.AutoScrap = true;
        this.AutoRes = true;
        this.AutoMoney = true;
//        this.AutoMindSurge = true;
//        this.PanicMode = true;
        this.AutoSelfHeal = true;
        this.SelfHealAmount = this._myLoc.Rift ? 4000 : 2000;
        this.Realism = true;
        this.ShowAlert("Auto Farm Black Gun");
      }
    }

    private void InvyDriver()
    {
      this.AutoDegrapple = true;
      this.AutoFriendHeal = true;
      this.FriendHealAmount = this._myLoc.Rift ? 6500 : 5000;
      this.AutoSell = true;
      this.AutoScrap = true;
      this.AutoMoney = true;
      this.AutoRes = true;
      this.MinimumMetals = this._local.FriendlyShipCount > 1 ? this._myShip.ResourceCapacity : this._myShip.ResourceCapacity / 2;
      this._shipsInFleet = this._local.FriendlyShipCount;
      this.Invasions = true;
      this.ShowAlert("Invasion Driver");
    }

    private void InvyGun()
    {
      this._driver = this.MyFollowTarget != null ? this.MyFollowTarget : (CombatEntity) this._local.FriendlyShips.First<Ship>((Func<Ship, bool>) (x => x.Special != null && x.Special.Class == SpecialClass.TECHNICIAN));
      this.DoFollow(this._driver);
      this.AutoGun = true;
      this._attackOrder = (NpcAttackOrder) new TopDown();
      this.AutoFeed = true;
      this.AutoFeedTarget = this._driver.Title;
      this.AutoSell = true;
      this.AutoScrap = true;
      this.AutoRes = true;
      this.MinimumMetals = this._myShip.ResourceCapacity;
      this.AutoSelfHeal = true;
      this.SelfHealAmount = this._myLoc.Rift ? 4000 : 2000;
      this.ShowAlert("Invasion Gun");
    }
    private void EastDriver()
    {
      this.AutoDegrapple = true;
      this.AutoFriendHeal = true;
      this.FriendHealAmount = 6500;
      this.AutoSelfHeal = true;
      this.SelfHealAmount = 6500;
      this.AutoGun = true;
      this.AutoSell = true;
      this.AutoScrap = true;
      this.AutoMoney = true;
      this.AutoRes = true;
      this.MinimumMetals = this._myShip.ResourceCapacity;
      this._shipsInFleet = this._local.FriendlyShipCount;
      this.Invasions = true;
      this.AutoHealTime = 400;
      this.Realism = true; // REALISM
      this.ShowAlert("East Driver");
    }
    private void EastGun()
    {
        this._driver = this.MyFollowTarget != null ? this.MyFollowTarget : (CombatEntity)this._local.FriendlyShips.First<Ship>((Func<Ship, bool>)(x => x.Special != null && x.Special.Class == SpecialClass.TECHNICIAN));
        if (this.MyFollowTarget == null)
        {
          this.DoFollow(this._driver);
        }       
        this.AutoGun = true;
        this._attackOrder = (NpcAttackOrder)new TopDown();
        this.AutoFeed = true;
        this.AutoFeedTarget = this._driver.Title;
        this.AutoSell = true;
        this.AutoScrap = true;
        this.AutoRes = true;
        this.MinimumMetals = this._myShip.ResourceCapacity;
        this.AutoSelfHeal = true;
        this.SelfHealAmount = 3250;
        this.Realism = true; // REALISM
        this.ShowAlert("East Gun");
    }
    private void EuroGun()
    {
        if (this.AutoRes != false || this.AutoSell != false || this.AutoGun != false)
        {
          this.AutoRes = false;
          this.AutoSell = false;
          this.AutoGun = false;
          this.ShowAlert("Euro GUN");
        }
        else
        {
          this.AutoRes = true;
          this.AutoSell = true;
          this.AutoGun = true;
          this.ShowAlert("Auto GUN");
      }

    }

    private void ToggleGun()
    {
      this.AutoGun = !this.AutoGun;
      //this.ShowAlert("Auto Gun " + (this.AutoGun ? "ON" : "OFF"));
    }

    private void ToggleMoney()
    {
      this.AutoMoney = !this.AutoMoney;
      //this.ShowAlert("Auto Gun " + (this.AutoGun ? "ON" : "OFF"));
    }

    private void ToggleHarvest()
    {
      this._autoHarvest = !this._autoHarvest;
    }

    private void ToggleSell()
    {
      this.AutoSell = !this.AutoSell;
    }
    private void ToggleMets()
    {
      if (this.MinimumMetals == 0)
      {
        this.MinimumMetals = this._myShip.ResourceCapacity;
      }
      else
      {
        this.MinimumMetals = 0;
      }
    }
    private void ClearJet()
    {
      this._jettisonSector = null;
      this.ShowAlert("Mule sector cleared");
    }
    private void SetMule()
    {
      this._muleSector = this._myLoc;
      this.ShowAlert("Mule set to: " +_muleSector.ToString());
    }

    private void GoMule()
    {
      Sector target = this._muleSector;
      this.ShowAlert("Going to: " + target.ToString());
      this.Course.Set(target);
      this.Course.Resume();
    }

    private void FindRoid()
    {
      Sector target = this.NearestRoid();
      this.ShowAlert(target.ToString());
      this.Course.Set(target);
    }

    private void NotFull()
    {
      this.ShowAlert("[r]NOT FULL:");
      foreach (Ship ship in this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>) (x => x.ResourceCapacity - (int) x.Metals > 0)))
        this.ShowAlert(string.Format("{0}: {1} Metals ({2} Capacity)", (object) ship.Player.Name, (object) ship.Metals, (object) ship.ResourceCapacity));
    }

    private void AutoFarm()
    {
      this._autoFarm = !this._autoFarm;
      this.AutoRes = true;
      this.AutoSell = true;
      this.AutoSelfHeal = true;
      this.SelfHealAmount = 600;
      this.AutoScrap = true;
      this.MinimumMetals = 10;
      this.ShowAlert("Auto Farm " + (this._autoFarm ? "ON" : "OFF"));
    }

    private void Emulate()
    {
      if (this.DTeam.Count <= 0 || !this._local.EnemyShips.Any<Ship>())
        return;
      this.ShowAlert("[b]Emulating... ");
      try
      {
        foreach (VolleyResult volleyResult in (IEnumerable<VolleyResult>) Emulator.Emulate(this.DTeam, this._local.EnemyShips).OrderByDescending<VolleyResult, double>((Func<VolleyResult, double>) (x => x.SuccessChance)))
          this.ShowAlert(string.Format("[o]Player: {0}, Success: {1}", (object) volleyResult.PlayerName, (object) volleyResult.SuccessChance));
      }
      catch (Exception ex)
      {
        GameState.LogError(ex);
      }
    }

    private void StackSummary()
    {
      try
      {
        this.ShowAlert("[r]YOUR STACK: " + (object) this._local.FriendlyShips.Count<Ship>());
        this.StackInfo(this._local.FriendlyShips);
        this.ShowAlert("[o]:nostealth " + (object) this._local.FriendlyShips.Count<Ship>((Func<Ship, bool>) (x => (x.Engine == null || x.Engine.Class != EngineClass.STEALTH) && x.Class != ShipClass.WyrdAssassin)));
        this.ShowAlert("[r]ENEMY STACK: " + (object) this._local.EnemyShips.Count<Ship>());
        this.StackInfo(this._local.EnemyShips);
      }
      catch (Exception ex)
      {
        using (StreamWriter streamWriter = File.AppendText("C:\\logs\\errors.txt"))
          streamWriter.WriteLine((object) ex);
      }
    }

    private void NoStealth()
    {
      this.ShowAlert("[r]NO STEALTH:");
      foreach (PlayerCombatEntity playerCombatEntity in this._local.FriendlyShips.Where<Ship>((Func<Ship, bool>) (x => (x.Engine == null || x.Engine.Class != EngineClass.STEALTH) && x.Class != ShipClass.WyrdAssassin)))
        this.ShowAlert("[r]" + playerCombatEntity.Player.Name);
    }

    private void SetDTeam()
    {
      if (this._local.FriendlyShips.Any<Ship>())
        this.DTeam = (IList<Ship>) this._local.FriendlyShips.ToList<Ship>();
      this.ShowAlert(string.Format("[g]DTEAM ({0}):", (object) this._local.FriendlyShips.Count<Ship>()));
      foreach (Ship ship in (IEnumerable<Ship>) this.DTeam)
        this.ShowAlert(string.Format("[g]{0}: {1}[w]H [g]{2}[w]C [g]{3}[w]MD", (object) ship.Player.Name, (object) (int) ship.HitChance, (object) (int) ship.CriticalChance, (object) ship.MinimumDamage));
    }

    private void ResetKills()
    {
      this.KillsAgainst = 0;
      this.KillsFor = 0;
      this.ShowAlert("Reset Kill Counters to 0");
    }

    private void GunMode()
    {
      this._attackOrder = this._attackOrder.Next();
      this.ShowAlert("Gun Mode: " + this._attackOrder.Description);
    }

    private void PlayerGunMode()
    {
      this._playerAttackOrder = this._playerAttackOrder.Next();
      this.ShowAlert("Player Attack Mode: " + this._playerAttackOrder.Description);
    }

    private void AutoFarmOff()
    {
      this._autoFarm = false;
      this._autoFarmBlackDriver = false;
      this._autoFarmBlackGun = false;
      this.AutoMindSurge = false;
      this.ShowAlert("AutoFarm Off");
      this.Status();
    }

    public void BFG()
    {
      this.AutoFill = true;
      this.AutoSell = true;
      this.MinimumMetals = this._myShip.ResourceCapacity;
      this.AutoFeed = true;
      this.AutoRes = true;
      this.AutoSelfHeal = true;
      this.AutoHealTime = 100;
      this.AutoMoney = true;
      this.ShotHold = true;
    }

    public void Beam()
    {
      this.AutoFill = true;
      this.AutoSell = true;
      this.MinimumMetals = this._myShip.ResourceCapacity;
      this.AutoFeed = true;
      this.AutoRes = true;
      this.AutoSelfHeal = true;
      this.AutoHealTime = 100;
      this.AutoMoney = true;
      this.AutoHook = true;
      this._playerAttackOrder = (PlayerAttackOrder) new BiggestTwat();
    }

    public void Hook()
    {
      this.AutoFill = true;
      this.AutoSell = true;
      this.MinimumMetals = this._myShip.ResourceCapacity;
      this.AutoFeed = true;
      this.AutoRes = true;
      this.AutoSelfHeal = true;
      this.AutoHealTime = 100;
      this.AutoMoney = true;
      this.AutoHook = true;
      this._playerAttackOrder = (PlayerAttackOrder) new BiggestTwat();
    }

    private void Test()
    {
      foreach (string text in this.Accounts.Accounts.Select<Account, string>((Func<Account, string>) (x => x.Name)))
        this.ShowAlert(text);
    }

    private void Status()
    {
      this.ShowAlert("DTeam Count: " + (object) this.DTeam.Count);
      this.AutoFeed = this.AutoFeed;
      this.AutoFeedTarget = this.AutoFeedTarget;
      this.AutoSelfHeal = this.AutoSelfHeal;
      this.SelfHealAmount = this.SelfHealAmount;
      this.AutoFriendHeal = this.AutoFriendHeal;
      this.FriendHealAmount = this.FriendHealAmount;
      this.AutoRes = this.AutoRes;
      this.AutoHealTime = this.AutoHealTime;
      this.AutoDegrapple = this.AutoDegrapple;
      this.AutoEngage = this.AutoEngage;
      this.AutoFill = this.AutoFill;
      this.AutoGun = this.AutoGun;
      this.AutoHook = this.AutoHook;
      this.AutoMindSurge = this.AutoMindSurge;
      this.AutoMoney = this.AutoMoney;
      this.AutoScrap = this.AutoScrap;
      this.AutoSell = this.AutoSell;
      this.AutoSellHalf = this.AutoSellHalf;
      this.AutoSellAll = this.AutoSellAll;
      this.AutoSellNon = this.AutoSellNon;
      this.AutoSellPhase = this.AutoSellPhase;
      this.AutoHarvest = this.AutoHarvest;
      this.MinimumMetals = this.MinimumMetals;
      this.Invasions = this.Invasions;
      this.Panic = this.Panic;
      this.PanicMode = this.PanicMode;
      this.Realism = this.Realism; // REALISM
      this.Record = this.Record;
      this.RoidChanges = this.RoidChanges;
      this.RouteMode = this.RouteMode;
      this.ShouldLog = this.ShouldLog;
      this.AutoJetti = this.AutoJetti;
      this.FkeyLevel = this.FkeyLevel;
    }

    private void ListSeeds()
    {
      foreach (KeyValuePair<string, string> savedSeed in this._savedSeeds)
        this.ShowAlert(string.Format("{0} in {1}", (object) savedSeed.Value, (object) savedSeed.Key));
    }
  }
}
