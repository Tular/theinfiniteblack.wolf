﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.TimeComponent
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library.Client
{
  public class TimeComponent
  {
    private readonly IGameState _state;
    private long _startMS;
    private long _stopMS;

    public bool IsFinished
    {
      get
      {
        return this._state.WorldTimeMS >= this._stopMS;
      }
    }

    public long DurationMS
    {
      get
      {
        if (this._stopMS <= this._startMS)
          return 0;
        return this._stopMS - this._startMS;
      }
    }

    public string DurationString
    {
      get
      {
        return ((float) this.DurationMS / 1000f).ToString("#,##0.0");
      }
    }

    public long RemainingMS
    {
      get
      {
        if (this._stopMS <= this._state.WorldTimeMS)
          return 0;
        return this._stopMS - this._state.WorldTimeMS;
      }
    }

    public float RemainingSeconds
    {
      get
      {
        return (float) this.RemainingMS / 1000f;
      }
    }

    public string RemainingSecondsString
    {
      get
      {
        return ((float) this.RemainingMS / 1000f).ToString("#,##0.0");
      }
    }

    public float PercentRemaining
    {
      get
      {
        long num1 = this._stopMS - this._startMS;
        if (num1 <= 0L)
          return 0.0f;
        long num2 = this._stopMS - this._state.WorldTimeMS;
        if (num2 <= 0L)
          return 0.0f;
        return (float) num2 / (float) num1;
      }
    }

    public float PercentComplete
    {
      get
      {
        long num1 = this._stopMS - this._startMS;
        if (num1 <= 0L)
          return 0.0f;
        long num2 = this._stopMS - this._state.WorldTimeMS;
        if (num2 <= 0L)
          return 1f;
        return (float) (1.0 - (double) num2 / (double) num1);
      }
    }

    public long StartMS
    {
      get
      {
        return this._startMS;
      }
    }

    public long StopMS
    {
      get
      {
        return this._stopMS;
      }
    }

    public TimeComponent(IGameState state)
    {
      this._state = state;
      this.Reset();
    }

    public void Set(long startMS, long stopMS)
    {
      this._startMS = startMS;
      this._stopMS = stopMS;
    }

    public void Set(long timeFromNowMS)
    {
      this._startMS = this._state.WorldTimeMS;
      this._stopMS = this._startMS + timeFromNowMS;
    }

    public void Reset()
    {
      this._startMS = this._state.WorldTimeMS;
      this._stopMS = this._startMS;
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder(300);
      string str1 = ((float) this.RemainingMS / 1000f).ToString("#,##0.0");
      stringBuilder.Append(str1);
      string str2 = " Seconds";
      stringBuilder.Append(str2);
      return stringBuilder.ToString();
    }
  }
}
