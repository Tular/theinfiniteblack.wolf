﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.CasinoManager
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Client
{
  public class CasinoManager
  {
    private readonly IGameState _state;

    public CasinoManager(IGameState state)
    {
      this._state = state;
    }

    public void BuyLottery(int tickets)
    {
    }

    public void SpaceballBetRed(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBetBlack(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBetOdd(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBetEven(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBetLo(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBetHi(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet1D(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet2D(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet3D(CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet(int num1, CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet(int num1, int num2, CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet(int num1, int num2, int num3, CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet(int num1, int num2, int num3, int num4, CurrencyType currency, int amount, bool standing)
    {
    }

    public void SpaceballBet(int num1, int num2, int num3, int num4, int num5, int num6, CurrencyType currency, int amount, bool standing)
    {
    }
  }
}
