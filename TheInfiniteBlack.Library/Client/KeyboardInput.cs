﻿using System;
using System.Runtime.InteropServices;

namespace TheInfiniteBlack.Library.Client
{
  public class KeyboardInput : IDisposable
  {
    public event EventHandler<EventArgs> KeyBoardKeyPressed;
    public bool keydown;
    public int keycode;
    private WindowsHookHelper.HookDelegate keyBoardDelegate;
    private IntPtr keyBoardHandle;
    private const Int32 WH_KEYBOARD_LL = 13;
    private bool disposed;

    public KeyboardInput()
    {
      keyBoardDelegate = KeyboardHookDelegate;
      keyBoardHandle = WindowsHookHelper.SetWindowsHookEx(WH_KEYBOARD_LL, keyBoardDelegate, IntPtr.Zero, 0);
    }

    private IntPtr KeyboardHookDelegate(Int32 Code, IntPtr wParam, IntPtr lParam)
    {
      if (Code < 0)
      {
        return WindowsHookHelper.CallNextHookEx(keyBoardHandle, Code, wParam, lParam);
      }

      keydown = false;

      if (wParam == (IntPtr)0x0100 || wParam == (IntPtr)0x0104) //KeyDown
      {
        int vkCode = Marshal.ReadInt32(lParam);
        keydown = true;
        keycode = vkCode;
      }

      if (KeyBoardKeyPressed != null)
        KeyBoardKeyPressed(this, new EventArgs());

      return WindowsHookHelper.CallNextHookEx(keyBoardHandle, Code, wParam, lParam);
    }

    public void Dispose()
    {
      Dispose(true);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        if (keyBoardHandle != IntPtr.Zero)
        {
          WindowsHookHelper.UnhookWindowsHookEx(keyBoardHandle);
        }

        disposed = true;
      }
    }

    ~KeyboardInput()
    {
      Dispose(false);
    }
  }
}