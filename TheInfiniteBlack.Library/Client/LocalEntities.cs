﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.LocalEntities
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client
{
  public class LocalEntities
  {
    private List<Entity> _values = new List<Entity>();
    public string OrderStat = "";
    public string SortOrder = "";
    private readonly GameState _state;
    private int _lastChangeFlagId;
    
    public int LastChangeFlagID
    {
      get
      {
        return this._lastChangeFlagId;
      }
    }

    public int Count
    {
      get
      {
        return this._values.Count;
      }
    }

    public IEnumerable<Ship> FriendlyShips
    {
      get
      {
        return this._values.Where<Entity>((Func<Entity, bool>) (e => e.Type == EntityType.SHIP && (e.Relation == RelationType.FRIEND || e.Relation == RelationType.SELF))).Select<Entity, Ship>((Func<Entity, Ship>) (x => (Ship) x));
      }
    }

    public IEnumerable<Ship> EnemyShips
    {
      get
      {
        return this._values.Where<Entity>((Func<Entity, bool>) (e => e.Type == EntityType.SHIP && (e.Relation == RelationType.ENEMY || e.Relation == RelationType.NEUTRAL))).Select<Entity, Ship>((Func<Entity, Ship>) (x => (Ship) x));
      }
    }

    public int FriendlyShipCount
    {
      get
      {
        return this.FriendlyShips.Count<Ship>();
      }
    }

    public int EnemyShipCount
    {
      get
      {
        return this._values.Count<Entity>((Func<Entity, bool>) (e => e.Type == EntityType.SHIP && (e.Relation == RelationType.ENEMY || e.Relation == RelationType.NEUTRAL)));
      }
    }

    public IEnumerable<Entity> Values
    {
      get
      {
        return (IEnumerable<Entity>) this._values;
      }
    }

    public Planet Planet
    {
      get
      {
        return this._values.FirstOrDefault<Entity>((Func<Entity, bool>) (e => e.Type == EntityType.PLANET)) as Planet;
      }
    }

    public Garrison Garrison
    {
      get
      {
        return this._values.FirstOrDefault<Entity>((Func<Entity, bool>) (e => e.Type == EntityType.GARRISON)) as Garrison;
      }
    }

    public Intradictor Intradictor
    {
      get
      {
        return this._values.FirstOrDefault<Entity>((Func<Entity, bool>) (e => e.Type == EntityType.INTRADICTOR)) as Intradictor;
      }
    }

    public StarPort StarPort
    {
      get
      {
        return this._values.FirstOrDefault<Entity>((Func<Entity, bool>) (e => e.Type == EntityType.STARPORT)) as StarPort;
      }
    }
    public IEnumerable<Entity> ColumnA
    {
      get
      {
        AccountSettings mySettings = this._state.MySettings;
        if (mySettings != null)
          return mySettings.EntityList.ColumnA.Apply((IEnumerable<Entity>) this._values);
        return (IEnumerable<Entity>) this._values;
      }
    }

    public IEnumerable<Entity> ColumnB
    {
      get
      {
        AccountSettings mySettings = this._state.MySettings;
        if (mySettings != null)
          return mySettings.EntityList.ColumnB.Apply((IEnumerable<Entity>) this._values);
        return (IEnumerable<Entity>) this._values;
      }
    }

    public IEnumerable<Entity> ColumnC
    {
      get
      {
        AccountSettings mySettings = this._state.MySettings;
        if (mySettings != null)
          return mySettings.EntityList.ColumnC.Apply((IEnumerable<Entity>) this._values);
        return (IEnumerable<Entity>) this._values;
      }
    }

    public IEnumerable<Entity> ColumnD
    {
      get
      {
        IEnumerable<Entity> source = this._values.AsEnumerable<Entity>();
        AccountSettings mySettings = this._state.MySettings;
        if (mySettings != null)
          source = mySettings.EntityList.ColumnD.Apply((IEnumerable<Entity>) this._values);
        IEnumerable<Entity> entities1;
        if (string.IsNullOrEmpty(this.OrderStat) || string.IsNullOrEmpty(this.SortOrder))
          entities1 = source.Where<Entity>((Func<Entity, bool>) (e => e.GetType() == typeof (Ship)));
        else
          entities1 = (IEnumerable<Entity>) source.Where<Entity>((Func<Entity, bool>) (e => e.GetType() == typeof (Ship))).Select<Entity, Ship>((Func<Entity, Ship>) (x => (Ship) x)).AsQueryable<Ship>().OrderBy<Ship>(this.OrderStat, this.SortOrder == "desc").Select<Ship, Entity>((Expression<Func<Ship, Entity>>) (x => (Entity) x));
        IEnumerable<Entity> entities2 = entities1;
        return entities2.Any<Entity>() ? entities2.Union<Entity>(source.Where<Entity>((Func<Entity, bool>) (e => e.GetType() != typeof (Ship)))) : source;
      }
    }

    public LocalEntities(GameState state)
    {
      this._state = state;
    }

    public bool Contains(Entity value)
    {
      return this._values.Contains(value);
    }

    public bool Contains(EntityType type)
    {
      return this._values.FirstOrDefault<Entity>((Func<Entity, bool>) (e => e.Type == type)) != null;
    }

    protected internal void Add(Entity value)
    {
      if (value == null || this._values.Contains(value))
        return;
      CombatEntity combatEntity = value as CombatEntity;
      if (combatEntity != null)
        combatEntity.Grappled = false;
      List<Entity> values = new List<Entity>((IEnumerable<Entity>) this._values);
      Ship ship1 = value as Ship;
      if (ship1 != null && ship1.Class == ShipClass.Carrier)
      {
        int index = 0;
        foreach (Entity entity in values)
        {
          Ship ship2 = entity as Ship;
          if (ship2 != null && ship2.Class == ShipClass.Carrier)
            ++index;
        }
        values.Insert(index, value);
      }
      else
        values.Add(value);
      this.Set(values);
      this._state.UI.Show(new EntityArriveEventArgs((IGameState) this._state, value));
    }

    protected internal void Remove(Entity value, EntityExitType type, Direction dir)
    {
      if (value == null || !this._values.Contains(value))
        return;
      switch (type)
      {
        case EntityExitType.SILENT:
        case EntityExitType.DESTROYED:
        case EntityExitType.EARTH_JUMP:
        case EntityExitType.CORP_JUMP:
        case EntityExitType.RIFT_JUMP:
        case EntityExitType.LOGOUT:
          if (this._state.MyAttackTarget == value)
            this._state.OnAttack((CombatEntity) null);
          if (this._state.MyFollowTarget == value)
          {
            this._state.OnFollow((CombatEntity) null);
            break;
          }
          break;
      }
      List<Entity> values = new List<Entity>((IEnumerable<Entity>) this._values);
      values.Remove(value);
      this.Set(values);
      this._state.UI.Show(new EntityExitEventArgs((IGameState) this._state, value, type, dir));
    }

    protected internal void Set(List<Entity> values)
    {
      this._values = values;
      this._lastChangeFlagId = this._lastChangeFlagId + 1;
    }
  }
}
