﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.AttackTargetChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class AttackTargetChangedEventArgs : GameEventArgs
  {
    private readonly CombatEntity _target;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.AttackTargetChanged;
      }
    }

    public CombatEntity Target
    {
      get
      {
        return this._target;
      }
    }

    public AttackTargetChangedEventArgs(IGameState state, CombatEntity target)
      : base(state)
    {
      this._target = target;
    }

    public override void AppendText(StringBuilder sb)
    {
      if (this._target != null)
      {
        sb.Append("Attacking ");
        this._target.AppendName(sb, true);
        sb.Append("!");
      }
      else
        sb.Append("Stopped attacking.");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.AttackStatus;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
