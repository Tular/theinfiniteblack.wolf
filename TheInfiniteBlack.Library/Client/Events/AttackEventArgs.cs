﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.AttackEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class AttackEventArgs : GameEventArgs
  {
    private readonly Entity _source;
    private readonly Entity _target;
    private readonly int _damage;
    private readonly AttackEventType _attackType;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.Attack;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public Entity Target
    {
      get
      {
        return this._target;
      }
    }

    public int Damage
    {
      get
      {
        return this._damage;
      }
    }

    public AttackEventType AttackType
    {
      get
      {
        return this._attackType;
      }
    }

    public AttackEventArgs(IGameState state, Entity source, Entity target, int damage, AttackEventType attackType)
      : base(state)
    {
      this._source = source;
      this._target = target;
      this._damage = damage;
      this._attackType = attackType;
    }

    public override void AppendText(StringBuilder sb)
    {
      switch (this._attackType)
      {
        case AttackEventType.MISS:
          this._source.AppendName(sb, true);
          sb.Append(" MISSES ");
          this._target.AppendName(sb, true);
          sb.Append("!");
          if (this._target == this.State.MyShip && this.State.Buyables.Tactics > 0 && !this.State.Buyables.SuspendTactics)
          {
            sb.Append(" (");
            sb.Append(this.State.Buyables.Tactics.ToString("#,0"));
            sb.Append(" Tactics remaining)");
            break;
          }
          break;
        case AttackEventType.GRAZE:
          this._source.AppendName(sb, true);
          sb.Append(" GRAZES ");
          this._target.AppendName(sb, true);
          sb.Append(" for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" damage!");
          break;
        case AttackEventType.HIT:
          this._source.AppendName(sb, true);
          sb.Append(" attacks ");
          this._target.AppendName(sb, true);
          sb.Append(" for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" damage!");
          break;
        case AttackEventType.CRITICAL:
          this._source.AppendName(sb, true);
          sb.Append(" CRITICALS ");
          this._target.AppendName(sb, true);
          sb.Append(" for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" damage!");
          if (this._source == this.State.MyShip && this.State.Buyables.SuperCharge > 0 && !this.State.Buyables.SuspendSuperCharge)
          {
            sb.Append(" (");
            sb.Append(this.State.Buyables.SuperCharge.ToString("#,0"));
            sb.Append(" Super Charge remaining)");
            break;
          }
          break;
        case AttackEventType.SPLASH:
          this._source.AppendName(sb, true);
          sb.Append(" SPLASHES ");
          this._target.AppendName(sb, true);
          sb.Append(" for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" damage!");
          break;
        case AttackEventType.REPAIR:
          this._source.AppendName(sb, true);
          sb.Append(" REPAIRS ");
          this._target.AppendName(sb, true);
          sb.Append(" for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append("!");
          break;
        case AttackEventType.STUN:
          this._target.AppendName(sb, true);
          sb.Append(" was STUNNED by ");
          this._source.AppendName(sb, true);
          sb.Append(" for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" seconds!");
          break;
        case AttackEventType.COLLIDE:
          this._source.AppendName(sb, true);
          sb.Append(" and ");
          this._target.AppendName(sb, true);
          sb.Append(" COLLIDE for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" damage!");
          break;
        case AttackEventType.DEFLECT:
          this._source.AppendName(sb, true);
          sb.Append(" DEFLECTS ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" damage to ");
          this._target.AppendName(sb, true);
          sb.Append("!");
          break;
        case AttackEventType.RAM:
          this._source.AppendName(sb, true);
          sb.Append(" RAMS ");
          this._target.AppendName(sb, true);
          sb.Append(" for ");
          sb.Append(this._damage.ToString("#,#"));
          sb.Append(" damage!");
          break;
        case AttackEventType.GRAPPLE:
          this._source.AppendName(sb, true);
          sb.Append(" GRAPPLES ");
          this._target.AppendName(sb, true);
          sb.Append("!");
          break;
        case AttackEventType.DEGRAPPLE:
          this._source.AppendName(sb, true);
          sb.Append(" DE-GRAPPLES ");
          this._target.AppendName(sb, true);
          sb.Append("!");
          break;
      }
      if (!GameState.DoLog || !(GameState.LastLog != sb.ToString()))
        return;
      this.Log(sb);
      GameState.LastLog = sb.ToString();
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      if (!filter.Combat.Units.IsVisible(this._source) && !filter.Combat.Units.IsVisible(this._target))
        return false;
      RelationFilter attack = filter.Combat.Attacks[this.AttackType];
      if (!attack[this._source.Relation])
        return attack[this._target.Relation];
      return true;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if ((double) systemSettings.CombatSound > 1.0000000116861E-07)
      {
        switch (this._attackType)
        {
          case AttackEventType.MISS:
          case AttackEventType.GRAZE:
          case AttackEventType.HIT:
          case AttackEventType.CRITICAL:
            return SoundHelper.GetAttackSound(this._source);
          case AttackEventType.REPAIR:
            return SoundHelper.GetRepairSound(this._source);
          case AttackEventType.STUN:
            return new EventSoundEffect(SoundEffectType.stun, 1f);
          case AttackEventType.GRAPPLE:
            return new EventSoundEffect(SoundEffectType.grapple);
          case AttackEventType.DEGRAPPLE:
            return new EventSoundEffect(SoundEffectType.degrapple, 1f);
        }
      }
      return (EventSoundEffect) null;
    }
  }
}
