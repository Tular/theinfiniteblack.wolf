﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.ShowEntityDetailEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class ShowEntityDetailEventArgs : GameEventArgs
  {
    private readonly Entity _source;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.ShowEntityDetail;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public ShowEntityDetailEventArgs(IGameState state, Entity source)
      : base(state)
    {
      this._source = source;
    }

    public override void AppendText(StringBuilder sb)
    {
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return false;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
