﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.PlayerOnlineStatusChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class PlayerOnlineStatusChangedEventArgs : GameEventArgs
  {
    private readonly ClientPlayer _source;
    private readonly bool _isOnline;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.PlayerOnlineStatus;
      }
    }

    public ClientPlayer Source
    {
      get
      {
        return this._source;
      }
    }

    public bool IsOnline
    {
      get
      {
        return this._isOnline;
      }
    }

    public PlayerOnlineStatusChangedEventArgs(IGameState state, ClientPlayer source, bool isOnline)
      : base(state)
    {
      this._source = source;
      this._isOnline = isOnline;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      sb.Append(this._isOnline ? " logged in." : " logged off.");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      if (filter.Misc.FriendOnlineStatus && this.State.MySettings != null)
        return this.State.MySettings.Social.IsFriend(this._source.Name);
      return false;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
