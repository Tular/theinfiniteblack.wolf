﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.CourseSetEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class CourseSetEventArgs : GameEventArgs
  {
    private readonly List<Sector> _course;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.CourseSet;
      }
    }

    public IEnumerable<Sector> Course
    {
      get
      {
        return (IEnumerable<Sector>) this._course;
      }
    }

    public CourseSetEventArgs(IGameState state, List<Sector> course)
      : base(state)
    {
      this._course = course;
    }

    public override void AppendText(StringBuilder sb)
    {
      Sector sector = this._course.LastOrDefault<Sector>();
      if (sector != null)
      {
        sb.Append("New Course Set: ");
        sector.AppendName(sb);
        if (this._course.Count != 1)
        {
          sb.Append(" (");
          sb.Append(this._course.Count);
          sb.Append(" Jumps)");
        }
        else
          sb.Append(" (1 Jump)");
      }
      else
        sb.Append("Course Cleared");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.CourseStatus;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
