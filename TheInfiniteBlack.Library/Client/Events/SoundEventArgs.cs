﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.SoundEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class SoundEventArgs : GameEventArgs
  {
    private readonly EventSoundEffect _sound;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.Sound;
      }
    }

    public EventSoundEffect Sound
    {
      get
      {
        return this._sound;
      }
    }

    public SoundEventArgs(IGameState state, EventSoundEffect sound)
      : base(state)
    {
      this._sound = sound;
    }

    public override void AppendText(StringBuilder sb)
    {
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return false;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return this._sound;
    }
  }
}
