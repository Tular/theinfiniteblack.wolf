﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.TradeEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Network;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class TradeEventArgs : GameEventArgs
  {
    private readonly TradeComponent _a = new TradeComponent();
    private readonly TradeComponent _b = new TradeComponent();

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.Trade;
      }
    }

    public TradeComponent A
    {
      get
      {
        return this._a;
      }
    }

    public TradeComponent B
    {
      get
      {
        return this._b;
      }
    }

    public TradeComponent MyOffer
    {
      get
      {
        if (this._a.Player == this.State.MyPlayer)
          return this._a;
        if (this._b.Player != this.State.MyPlayer)
          return (TradeComponent) null;
        return this._b;
      }
    }

    public TradeComponent TheirOffer
    {
      get
      {
        if (this._a.Player != this.State.MyPlayer)
          return this._a;
        if (this._b.Player == this.State.MyPlayer)
          return (TradeComponent) null;
        return this._b;
      }
    }

    public bool IsFinal { get; set; }

    public TradeEventArgs(IGameState state)
      : base(state)
    {
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append("...Modify the trade deal between ");
      this._a.Player.AppendName(sb, true);
      sb.Append(" and ");
      this._b.Player.AppendName(sb, true);
      sb.Append(this.IsFinal ? " (FINAL)." : ".");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.NonCombat[RelationType.SELF];
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }

    public void Cancel()
    {
      this.State.Net.Send(RequestCancelTrade.Create());
    }

    public INetworkData Serialize()
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) -40, 64);
      networkData.Write(this._a.Player.ID);
      networkData.Write(this._b.Player.ID);
      networkData.Write(this._a.Credits);
      networkData.Write(this._a.BlackDollars);
      if (this._a.Item == null)
        networkData.Write(sbyte.MinValue);
      else
        this._a.Item.Write((IByteBuffer) networkData);
      networkData.Write(this._b.Credits);
      networkData.Write(this._b.BlackDollars);
      if (this._b.Item == null)
        networkData.Write(sbyte.MinValue);
      else
        this._b.Item.Write((IByteBuffer) networkData);
      networkData.Write(this.IsFinal);
      return networkData;
    }
  }
}
