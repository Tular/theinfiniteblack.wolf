﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.InventoryChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class InventoryChangedEventArgs : GameEventArgs
  {
    private readonly Entity _source;
    private readonly List<EquipmentItem> _items;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.InventoryChanged;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public List<EquipmentItem> Items
    {
      get
      {
        return this._items;
      }
    }

    public InventoryChangedEventArgs(IGameState state, Entity source, List<EquipmentItem> items)
      : base(state)
    {
      this._source = source;
      this._items = items;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      sb.Append("'s Inventory Changed (");
      sb.Append(this._items.Count);
      sb.Append(" Items)");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.Debug;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
