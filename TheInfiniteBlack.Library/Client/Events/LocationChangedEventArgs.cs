﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.LocationChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class LocationChangedEventArgs : GameEventArgs
  {
    private readonly Sector _location;
    private readonly List<Entity> _entites;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.LocationChanged;
      }
    }

    public Sector Location
    {
      get
      {
        return this._location;
      }
    }

    public List<Entity> Entites
    {
      get
      {
        return this._entites;
      }
    }

    public LocationChangedEventArgs(IGameState state, Sector location, List<Entity> entites)
      : base(state)
    {
      this._location = location;
      this._entites = entites;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append("You arrived in ");
      this._location.AppendName(sb);
      sb.Append(".");
      if (this.State.Buyables.HyperDrive <= 0 || this.State.Buyables.SuspendHyperDrive)
        return;
      sb.Append(" (");
      sb.Append(this.State.Buyables.HyperDrive.ToString("#,0"));
      sb.Append(" Hyper Drive remaining)");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.LocationStatus;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if ((double) systemSettings.UiSound > 1.0000000116861E-07)
        return SoundHelper.GetMoveSound((Entity) this.State.MyShip);
      return (EventSoundEffect) null;
    }
  }
}
