﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.ResourcesChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class ResourcesChangedEventArgs : GameEventArgs
  {
    private readonly Ship _source;
    private readonly int _organics;
    private readonly int _gas;
    private readonly int _metals;
    private readonly int _radioactives;
    private readonly int _darkMatter;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.ResourcesChanged;
      }
    }

    public Ship Source
    {
      get
      {
        return this._source;
      }
    }

    public int Organics
    {
      get
      {
        return this._organics;
      }
    }

    public int Gas
    {
      get
      {
        return this._gas;
      }
    }

    public int Metals
    {
      get
      {
        return this._metals;
      }
    }

    public int Radioactives
    {
      get
      {
        return this._radioactives;
      }
    }

    public int DarkMatter
    {
      get
      {
        return this._darkMatter;
      }
    }

    public ResourcesChangedEventArgs(IGameState state, Ship source, int organics, int gas, int metals, int radioactives, int darkMatter)
      : base(state)
    {
      this._source = source;
      this._organics = organics;
      this._gas = gas;
      this._metals = metals;
      this._radioactives = radioactives;
      this._darkMatter = darkMatter;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      if (this._darkMatter != 0)
      {
        sb.Append(this._darkMatter >= 0 ? " +" : " ");
        sb.Append(this._darkMatter);
        sb.Append(" DarkMatter");
        if (this._radioactives != 0 || this._metals != 0 || (this._gas != 0 || this._organics != 0))
          sb.Append(" |");
      }
      if (this._radioactives != 0)
      {
        sb.Append(this._radioactives >= 0 ? " +" : " ");
        sb.Append(this._radioactives);
        sb.Append(" Radioactives");
        if (this._metals != 0 || this._gas != 0 || this._organics != 0)
          sb.Append(" |");
      }
      if (this._metals != 0)
      {
        sb.Append(this._metals >= 0 ? " +" : " ");
        sb.Append(this._metals);
        sb.Append(" Metals");
        if (this._gas != 0 || this._organics != 0)
          sb.Append(" |");
      }
      if (this._gas != 0)
      {
        sb.Append(this._gas >= 0 ? " +" : " ");
        sb.Append(this._gas);
        sb.Append(" Gas");
        if (this._organics != 0)
          sb.Append(" |");
      }
      if (this._organics == 0)
        return;
      sb.Append(this._organics >= 0 ? " +" : " ");
      sb.Append(this._organics);
      sb.Append(" Organics");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.Gains[this._source.Relation];
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
