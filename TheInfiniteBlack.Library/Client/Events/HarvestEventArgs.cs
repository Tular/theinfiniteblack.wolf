﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.HarvestEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class HarvestEventArgs : GameEventArgs
  {
    private readonly Entity _source;
    private readonly Entity _target;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.Harvest;
      }
    }

    public Entity Source
    {
      get
      {
        return this._source;
      }
    }

    public Entity Target
    {
      get
      {
        return this._target;
      }
    }

    public HarvestEventArgs(IGameState state, Entity source, Entity target)
      : base(state)
    {
      this._source = source;
      this._target = target;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      sb.Append(" harvests ");
      this._target.AppendName(sb, true);
      sb.Append("...");
      if (this._source != this.State.MyShip || this.State.Buyables.Accelerate <= 0)
        return;
      sb.Append(" (");
      sb.Append(this.State.Buyables.Accelerate.ToString("#,0"));
      sb.Append(" Accelerate remaining)");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      if (!filter.Misc.NonCombat[this._source.Relation])
        return filter.Misc.NonCombat[this._target.Relation];
      return true;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if ((double) systemSettings.CombatSound > 1.0000000116861E-07)
        return SoundHelper.GetHarvestSound(this._source);
      return (EventSoundEffect) null;
    }
  }
}
