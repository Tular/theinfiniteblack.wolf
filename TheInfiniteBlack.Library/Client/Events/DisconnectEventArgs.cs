﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.DisconnectEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class DisconnectEventArgs : GameEventArgs
  {
    private readonly string _reason;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.Disconnect;
      }
    }

    public string Reason
    {
      get
      {
        return this._reason;
      }
    }

    public DisconnectEventArgs(IGameState state, string reason)
      : base(state)
    {
      this._reason = reason;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append("Disconnected: ");
      sb.Append(this._reason);
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return true;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
