﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.AchievementEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class AchievementEventArgs : GameEventArgs
  {
    public readonly string Achievement;
    public readonly string Text;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.Chat;
      }
    }

    public AchievementEventArgs(IGameState state, string achievement, string text)
      : base(state)
    {
      this.Achievement = achievement;
      this.Text = text;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append(this.Text);
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return true;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if (systemSettings == null || (double) systemSettings.UiSound <= 1.0000000116861E-07)
        return (EventSoundEffect) null;
      return new EventSoundEffect(SoundEffectType.ui_chat, 0.86f);
    }
  }
}
