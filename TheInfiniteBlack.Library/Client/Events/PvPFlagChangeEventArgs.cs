﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.PvPFlagChangeEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class PvPFlagChangeEventArgs : GameEventArgs
  {
    private readonly bool _isFlagged;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.PvPFlagChange;
      }
    }

    public bool IsFlagged
    {
      get
      {
        return this._isFlagged;
      }
    }

    public PvPFlagChangeEventArgs(IGameState state, bool isFlagged)
      : base(state)
    {
      this._isFlagged = isFlagged;
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append(this._isFlagged ? "[r]You are PvP flagged!" : "[g]You are no longer PvP flagged.");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.PvPStatus;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if ((double) systemSettings.UiSound > 1.0000000116861E-07)
        return new EventSoundEffect(SoundEffectType.ui_info);
      return (EventSoundEffect) null;
    }
  }
}
