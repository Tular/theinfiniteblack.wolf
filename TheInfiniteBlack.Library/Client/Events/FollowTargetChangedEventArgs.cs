﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.FollowTargetChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class FollowTargetChangedEventArgs : GameEventArgs
  {
    private readonly CombatEntity _target;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.FollowTargetChanged;
      }
    }

    public CombatEntity Target
    {
      get
      {
        return this._target;
      }
    }

    public FollowTargetChangedEventArgs(IGameState state, CombatEntity target)
      : base(state)
    {
      this._target = target;
    }

    public override void AppendText(StringBuilder sb)
    {
      if (this._target != null)
      {
        sb.Append("Following ");
        this._target.AppendName(sb, true);
        sb.Append(".");
      }
      else
        sb.Append("Stopped following.");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.FollowStatus;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
