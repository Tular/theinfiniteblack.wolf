﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.TradeFailedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class TradeFailedEventArgs : GameEventArgs
  {
    public override GameEventType EventType
    {
      get
      {
        return GameEventType.TradeCancelled;
      }
    }

    public TradeFailedEventArgs(IGameState state)
      : base(state)
    {
    }

    public override void AppendText(StringBuilder sb)
    {
      sb.Append("[y]Trade failed or was interrupted!");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.NonCombat[RelationType.SELF];
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      if ((double) systemSettings.UiSound > 1.0000000116861E-07)
        return new EventSoundEffect(SoundEffectType.ui_info);
      return (EventSoundEffect) null;
    }
  }
}
