﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.GameEventType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Client.Events
{
  public enum GameEventType
  {
    Attack,
    AttackTargetChanged,
    AuctionUpdated,
    AuctionRemoved,
    BankChanged,
    Chat,
    CourseSet,
    Disconnect,
    EntityEnter,
    EntityExit,
    EntityHullChanged,
    FollowTargetChanged,
    GainItem,
    GainXp,
    Harvest,
    InventoryChanged,
    LocationChanged,
    LoginSuccess,
    MoneyChanged,
    MovementChanged,
    PlayerOnlineStatus,
    AcceptDeclineWindow,
    PopupWindow,
    ShowUrl,
    PvPFlagChange,
    ResourcesChanged,
    Trade,
    TradeCancelled,
    ShowEntityDetail,
    CombatFlash,
    Sound,
  }
}
