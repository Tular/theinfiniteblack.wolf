﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.ShowUrlEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class ShowUrlEventArgs : GameEventArgs
  {
    private readonly string _url;
    private readonly bool _closeApp;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.ShowUrl;
      }
    }

    public string Url
    {
      get
      {
        return this._url;
      }
    }

    public bool CloseApp
    {
      get
      {
        return this._closeApp;
      }
    }

    public ShowUrlEventArgs(IGameState state, string url, bool closeApp)
      : base(state)
    {
      this._url = url;
      this._closeApp = closeApp;
    }

    public override void AppendText(StringBuilder sb)
    {
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return true;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
