﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Events.EntityHullChangedEventArgs
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Events
{
  public class EntityHullChangedEventArgs : GameEventArgs
  {
    private readonly CombatEntity _source;
    private readonly int _previous;
    private readonly int _current;

    public override GameEventType EventType
    {
      get
      {
        return GameEventType.EntityHullChanged;
      }
    }

    public CombatEntity Source
    {
      get
      {
        return this._source;
      }
    }

    public int Previous
    {
      get
      {
        return this._previous;
      }
    }

    public int Current
    {
      get
      {
        return this._current;
      }
    }

    public int Delta
    {
      get
      {
        return this._current - this._previous;
      }
    }

    public EntityHullChangedEventArgs(IGameState state, CombatEntity source, int previous, int current)
      : base(state)
    {
      this._source = source;
      this._previous = previous;
      this._current = current;
    }

    public override void AppendText(StringBuilder sb)
    {
      this._source.AppendName(sb, true);
      sb.Append(" hull changed. (");
      sb.Append(this.Delta.ToString("#,#"));
      sb.Append(")");
    }

    public override bool IsVisible(GameEventsFilter filter)
    {
      return filter.Misc.Debug;
    }

    public override EventSoundEffect GetSound(SystemSettings systemSettings, AccountSettings accountSettings)
    {
      return (EventSoundEffect) null;
    }
  }
}
