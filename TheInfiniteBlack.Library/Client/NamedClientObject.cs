﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.NamedClientObject
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library.Client
{
  public abstract class NamedClientObject
  {
    private readonly IGameState _state;
    private int _lastChangeFlagId;

    public virtual string Name
    {
      get
      {
        StringBuilder sb = new StringBuilder(200);
        this.AppendName(sb, false);
        return sb.ToString();
      }
    }

    public int LastChangeFlagID
    {
      get
      {
        return this._lastChangeFlagId;
      }
    }

    public IGameState State
    {
      get
      {
        return this._state;
      }
    }

    protected NamedClientObject(IGameState state)
    {
      this._state = state;
    }

    protected void OnChanged()
    {
      this._lastChangeFlagId = this._lastChangeFlagId + 1;
    }

    public abstract void AppendName(StringBuilder sb, bool markup);

    public override string ToString()
    {
      return this.Name;
    }
  }
}
