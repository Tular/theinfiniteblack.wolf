﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.CorporationCollection
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace TheInfiniteBlack.Library.Client
{
  public class CorporationCollection
  {
    private readonly Dictionary<short, ClientCorporation> _values = new Dictionary<short, ClientCorporation>();
    private readonly IGameState _state;

    public IEnumerable<ClientCorporation> Values
    {
      get
      {
        return (IEnumerable<ClientCorporation>) this._values.Values;
      }
    }

    public CorporationCollection(IGameState state)
    {
      this._state = state;
    }

    public ClientCorporation Get(short id)
    {
      if ((int) id == (int) short.MinValue)
        return (ClientCorporation) null;
      ClientCorporation clientCorporation1;
      if (this._values.TryGetValue(id, out clientCorporation1))
        return clientCorporation1;
      ClientCorporation clientCorporation2 = new ClientCorporation(this._state, id);
      this._values[id] = clientCorporation2;
      return clientCorporation2;
    }

    public ClientCorporation Get(string name)
    {
      return this._values.Values.FirstOrDefault<ClientCorporation>((Func<ClientCorporation, bool>) (p => p.Name.Equals(name, StringComparison.OrdinalIgnoreCase)));
    }
  }
}
