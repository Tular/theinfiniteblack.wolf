﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.AuctionCollection
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client
{
  public class AuctionCollection
  {
    private Dictionary<int, ClientAuction> _values = new Dictionary<int, ClientAuction>();
    private HashSet<int> _requested = new HashSet<int>();
    private readonly IGameState _state;

    public IEnumerable<ClientAuction> Values
    {
      get
      {
        return (IEnumerable<ClientAuction>) this._values.Values;
      }
    }

    public AuctionCollection(IGameState state)
    {
      this._state = state;
    }

    public ClientAuction Get(int id)
    {
      if (id == int.MinValue)
        return (ClientAuction) null;
      ClientAuction clientAuction;
      if (!this._values.TryGetValue(id, out clientAuction))
      {
        if (this._requested.Contains(id))
          return (ClientAuction) null;
        this._requested = new HashSet<int>((IEnumerable<int>) this._requested)
        {
          id
        };
        clientAuction = new ClientAuction(this._state, id);
        this._values = new Dictionary<int, ClientAuction>((IDictionary<int, ClientAuction>) this._values)
        {
          {
            id,
            clientAuction
          }
        };
        this._state.Net.Send(RequestAuctionItem.Create(id));
      }
      return clientAuction;
    }

    public ClientAuction Remove(int id)
    {
      this._requested = new HashSet<int>((IEnumerable<int>) this._requested)
      {
        id
      };
      ClientAuction clientAuction;
      if (!this._values.TryGetValue(id, out clientAuction))
        return (ClientAuction) null;
      Dictionary<int, ClientAuction> dictionary = new Dictionary<int, ClientAuction>((IDictionary<int, ClientAuction>) this._values);
      dictionary.Remove(id);
      this._values = dictionary;
      return clientAuction;
    }
  }
}
