﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.ClientBuyables
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Client
{
  public class ClientBuyables
  {
    private readonly IGameState _state;
    private int _hyperDrive;
    private int _superCharge;
    private int _tactics;
    private int _mindSurge;
    private int _accelerate;
    private int _bankCap;
    private int _inventoryCap;
    private bool _suspendHyperDrive;
    private bool _suspendSuperCharge;
    private bool _suspendTactics;
    private bool _suspendMindSurge;
    private int _lastChangeFlagId;

    public int LastChangeFlagID
    {
      get
      {
        return this._lastChangeFlagId;
      }
    }

    public int HyperDrive
    {
      get
      {
        return this._hyperDrive;
      }
      set
      {
        if (this._hyperDrive == value)
          return;
        this._hyperDrive = value;
        if (this._hyperDrive <= 0)
          this._state.ShowAlert("[r]Hyper Drive ran out! Get more from the BlackDollar menu!");
        this.OnChanged();
      }
    }

    public int SuperCharge
    {
      get
      {
        return this._superCharge;
      }
      set
      {
        if (this._superCharge == value)
          return;
        this._superCharge = value;
        if (this._superCharge <= 0)
          this._state.ShowAlert("[r]Super Charge ran out! Get more from the BlackDollar menu!");
        this.OnChanged();
      }
    }

    public int Tactics
    {
      get
      {
        return this._tactics;
      }
      set
      {
        if (this._tactics == value)
          return;
        this._tactics = value;
        if (this._tactics <= 0)
          this._state.ShowAlert("[r]Tactics ran out! Get more from the BlackDollar menu!");
        this.OnChanged();
      }
    }

    public int MindSurge
    {
      get
      {
        return this._mindSurge;
      }
      set
      {
        if (this._mindSurge == value)
          return;
        this._mindSurge = value;
        if (this._mindSurge <= 0)
          this._state.ShowAlert("[r]Mind Surge ran out! Get more from the BlackDollar menu!");
        this.OnChanged();
      }
    }

    public int Accelerate
    {
      get
      {
        return this._accelerate;
      }
      set
      {
        if (this._accelerate == value)
          return;
        this._accelerate = value;
        if (this._accelerate <= 0)
          this._state.ShowAlert("[r]Accelerate ran out! Get more from the BlackDollar menu!");
        this.OnChanged();
      }
    }

    public int BankCap
    {
      get
      {
        return this._bankCap;
      }
      set
      {
        if (this._bankCap == value)
          return;
        int num = this._bankCap == 0 ? 1 : 0;
        this._bankCap = value;
        if (num == 0)
          this._state.ShowAlert("[g]Bank Inventory Cap increased to " + (object) this._bankCap + "!");
        this.OnChanged();
      }
    }

    public int InventoryCap
    {
      get
      {
        return this._inventoryCap;
      }
      set
      {
        if (this._inventoryCap == value)
          return;
        int num = this._inventoryCap == 0 ? 1 : 0;
        this._inventoryCap = value;
        if (num == 0)
          this._state.ShowAlert("[g]Inventory Cap increased to " + (object) this._inventoryCap + "!");
        this.OnChanged();
      }
    }

    public bool SuspendHyperDrive
    {
      get
      {
        return this._suspendHyperDrive;
      }
    }

    public bool SuspendSuperCharge
    {
      get
      {
        return this._suspendSuperCharge;
      }
    }

    public bool SuspendTactics
    {
      get
      {
        return this._suspendTactics;
      }
    }

    public bool SuspendMindSurge
    {
      get
      {
        return this._suspendMindSurge;
      }
    }

    public ClientBuyables(IGameState state)
    {
      this._state = state;
    }

    public void ToggleHyperDrive()
    {
      this._state.Net.Send(RequestChat.Create(":hyperdrive", ChatType.SECTOR));
      this.SetHyperDrive(!this._suspendHyperDrive);
    }

    public void ToggleSuperCharge()
    {
      this.SetSuperCharge(!this._suspendSuperCharge);
      this._state.Net.Send(RequestChat.Create(":supercharge", ChatType.SECTOR));
    }

    public void ToggleTactics()
    {
      this.SetTactics(!this._suspendTactics);
      this._state.Net.Send(RequestChat.Create(":tactics", ChatType.SECTOR));
    }

    public void ToggleMindSurge()
    {
      this.SetMindSurge(!this._suspendMindSurge);
      this._state.Net.Send(RequestChat.Create(":mindsurge", ChatType.SECTOR));
    }

    protected internal void SetHyperDrive(bool value)
    {
      if (this._suspendHyperDrive == value)
        return;
      this._suspendHyperDrive = value;
      if (this._hyperDrive > 0)
        this._state.ShowAlert("[y]" + (this._suspendHyperDrive ? "Hyper Drive is OFF!" : "Hyper Drive is ON!"));
      this.OnChanged();
    }

    protected internal void SetSuperCharge(bool value)
    {
      if (this._suspendSuperCharge == value)
        return;
      this._suspendSuperCharge = value;
      if (this._superCharge > 0)
        this._state.ShowAlert("[y]" + (this._suspendSuperCharge ? "Super Charge is OFF!" : "Super Charge is ON!"));
      this.OnChanged();
    }

    protected internal void SetTactics(bool value)
    {
      if (this._suspendTactics == value)
        return;
      this._suspendTactics = value;
      if (this._tactics > 0)
        this._state.ShowAlert("[y]" + (this._suspendTactics ? "Tactics is OFF!" : "Tactics is ON!"));
      this.OnChanged();
    }

    protected internal void SetMindSurge(bool value)
    {
      if (this._suspendMindSurge == value)
        return;
      this._suspendMindSurge = value;
      if (this._mindSurge > 0)
        this._state.ShowAlert("[y]" + (this._suspendMindSurge ? "Mind Surge is OFF!" : "Mind Surge is ON!"));
      this.OnChanged();
    }

    private void OnChanged()
    {
      this._lastChangeFlagId = this._lastChangeFlagId + 1;
    }
  }
}
