﻿namespace TheInfiniteBlack.Library.Client
{
  internal class MinResTimeCommand : ITextCommands
  {
    public string Trigger()
    {
      return ":minrestime ";
    }

    public void Process(GameState gameState, string command)
    {
      gameState.AutoHealTime = int.Parse(command.Replace(":minrestime ", ""));
    }
  }
}