﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.Character
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.ComponentModel;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class Character
  {
    public sbyte ServerID { get; set; }

    public string Corporation { get; set; }

    public float Level { get; set; }

    public ShipClass Ship { get; set; }

    public DateTime LastActivity { get; set; }

    protected internal void Refresh(ClientPlayer player, TheInfiniteBlack.Library.Entities.Ship ship, sbyte serverId)
    {
      if (player == null || ship == null)
        return;
      this.ServerID = serverId;
      this.Corporation = player.MyCorporation == null ? "<Privateer>" : player.MyCorporation.Name;
      this.Level = ship.Level;
      this.Ship = ship.Class;
      this.LastActivity = DateTime.Now;
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
