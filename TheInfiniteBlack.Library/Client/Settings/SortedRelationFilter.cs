﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.SortedRelationFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class SortedRelationFilter : RelationFilter, ISortedFilter
  {
    public int SortOrder { get; set; }

    public override string ToString()
    {
      return "#" + (object) this.SortOrder + " - " + base.ToString();
    }
  }
}
