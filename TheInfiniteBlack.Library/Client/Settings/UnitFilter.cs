﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.UnitFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class UnitFilter
  {
    public RelationFilter Ship { get; set; }

    public RelationFilter Fighter { get; set; }

    public RelationFilter RepairDrone { get; set; }

    public RelationFilter Planet { get; set; }

    public RelationFilter Garrison { get; set; }

    public RelationFilter Intradictor { get; set; }

    public RelationFilter DefensePlatform { get; set; }

    public RelationFilter Mines { get; set; }

    public bool NPC { get; set; }

    public bool StarPort { get; set; }

    public bool CapturePoint { get; set; }

    public bool Cargo { get; set; }

    public bool Asteroid { get; set; }

    public UnitFilter()
    {
      this.Mines = new RelationFilter();
      this.DefensePlatform = new RelationFilter();
      this.Intradictor = new RelationFilter();
      this.Garrison = new RelationFilter();
      this.Planet = new RelationFilter();
      this.RepairDrone = new RelationFilter();
      this.Fighter = new RelationFilter();
      this.Ship = new RelationFilter();
    }

    public void SetAll(bool value)
    {
      this.Ship.SetAll(value);
      this.Fighter.SetAll(value);
      this.RepairDrone.SetAll(value);
      this.Planet.SetAll(value);
      this.Garrison.SetAll(value);
      this.Intradictor.SetAll(value);
      this.DefensePlatform.SetAll(value);
      this.Mines.SetAll(value);
      this.NPC = value;
      this.StarPort = value;
      this.CapturePoint = value;
      this.Cargo = value;
      this.Asteroid = value;
    }

    public void SetCombatUnits(RelationType relation, bool value)
    {
      this.Ship[relation] = value;
      this.Fighter[relation] = value;
      this.RepairDrone[relation] = value;
      this.Planet[relation] = value;
      this.Garrison[relation] = value;
      this.Intradictor[relation] = value;
      this.DefensePlatform[relation] = value;
      this.Mines[relation] = value;
    }

    public void SetNonCombatUnits(bool value)
    {
      this.NPC = value;
      this.StarPort = value;
      this.CapturePoint = value;
      this.Cargo = value;
      this.Asteroid = value;
    }

    public void Set(EntityType type, bool value)
    {
      switch (type)
      {
        case EntityType.PLANET:
          this.Planet.SetAll(value);
          break;
        case EntityType.ASTEROID:
          this.Asteroid = value;
          break;
        case EntityType.STARPORT:
          this.StarPort = value;
          break;
        case EntityType.SHIP:
          this.Ship.SetAll(value);
          break;
        case EntityType.NPC:
          this.NPC = value;
          break;
        case EntityType.FIGHTER:
          this.Fighter.SetAll(value);
          break;
        case EntityType.DEFENSE_PLATFORM:
          this.DefensePlatform.SetAll(value);
          break;
        case EntityType.MINES:
          this.Mines.SetAll(value);
          break;
        case EntityType.INTRADICTOR:
          this.Intradictor.SetAll(value);
          break;
        case EntityType.REPAIR_DRONE:
          this.RepairDrone.SetAll(value);
          break;
        case EntityType.GARRISON:
          this.Garrison.SetAll(value);
          break;
        case EntityType.CARGO_MONEY:
        case EntityType.CARGO_ITEM:
        case EntityType.CARGO_RESOURCE:
          this.Cargo = value;
          break;
        case EntityType.CAPTURE_POINT:
          this.CapturePoint = value;
          break;
      }
    }

    public bool IsVisible(Entity value)
    {
      if (value != null)
      {
        switch (value.Type)
        {
          case EntityType.PLANET:
            return this.Planet[value.Relation];
          case EntityType.ASTEROID:
            return this.Asteroid;
          case EntityType.STARPORT:
            return this.StarPort;
          case EntityType.SHIP:
            return this.Ship[value.Relation];
          case EntityType.NPC:
            return this.NPC;
          case EntityType.FIGHTER:
            return this.Fighter[value.Relation];
          case EntityType.DEFENSE_PLATFORM:
            return this.DefensePlatform[value.Relation];
          case EntityType.MINES:
            return this.Mines[value.Relation];
          case EntityType.INTRADICTOR:
            return this.Intradictor[value.Relation];
          case EntityType.REPAIR_DRONE:
            return this.RepairDrone[value.Relation];
          case EntityType.GARRISON:
            return this.Garrison[value.Relation];
          case EntityType.CARGO_MONEY:
          case EntityType.CARGO_ITEM:
          case EntityType.CARGO_RESOURCE:
            return this.Cargo;
          case EntityType.CAPTURE_POINT:
            return this.CapturePoint;
        }
      }
      return false;
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
