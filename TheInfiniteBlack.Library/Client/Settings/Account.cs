﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.Account
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class Account
  {
    public string Name { get; set; }

    public string Password { get; set; }

    public bool Hardcore { get; set; }

    public List<Character> Characters { get; set; }

    [XmlIgnore]
    public AccountSettings Settings { get; set; }

    public Account()
    {
      this.Characters = new List<Character>();
      this.Settings = new AccountSettings();
      this.Name = "Default";
      this.Password = string.Empty;
    }

    public Character Get(sbyte serverId)
    {
      Character character = this.Characters.FirstOrDefault<Character>((Func<Character, bool>) (c => (int) c.ServerID == (int) serverId));
      if (character == null)
      {
        character = new Character() { ServerID = serverId };
        this.Characters = new List<Character>((IEnumerable<Character>) this.Characters)
        {
          character
        };
      }
      return character;
    }

    protected internal void Refresh(ClientPlayer player, Ship ship, string password, sbyte serverId)
    {
      if (player == null || ship == null)
        return;
      this.Name = player.Name;
      this.Password = password;
      this.Hardcore = player.Hardcore;
      for (sbyte serverId1 = 0; (int) serverId1 <= 10; ++serverId1)
      {
        Character character = this.Get(serverId1);
        if ((int) serverId1 == (int) serverId)
          character.Refresh(player, ship, serverId);
      }
    }

    public override string ToString()
    {
      return this.Name;
    }
  }
}
