﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.SocialSettings
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class SocialSettings
  {
    public GameEventsFilter UniverseChat { get; set; }

    public GameEventsFilter ServerChat { get; set; }

    public GameEventsFilter SectorChat { get; set; }

    public GameEventsFilter CorpChat { get; set; }

    public GameEventsFilter AllianceChat { get; set; }

    public GameEventsFilter MarketChat { get; set; }

    public GameEventsFilter EventsChat { get; set; }

    public List<StringFilter> FriendList { get; set; }

    public List<StringFilter> IgnoreList { get; set; }

    public bool FilterProfanity { get; set; }

    public bool RejectInvites { get; set; }

    public bool RejectTrades { get; set; }

    public int chatFontSize { get; set; }

    public int eventLogFontSize { get; set; }

    public SocialSettings()
    {
      this.UniverseChat = new GameEventsFilter();
      this.ServerChat = new GameEventsFilter();
      this.SectorChat = new GameEventsFilter();
      this.CorpChat = new GameEventsFilter();
      this.AllianceChat = new GameEventsFilter();
      this.MarketChat = new GameEventsFilter();
      this.EventsChat = new GameEventsFilter();
      this.FriendList = new List<StringFilter>();
      this.IgnoreList = new List<StringFilter>();
      this.Default();
    }

    public void Default()
    {
      this.UniverseChat.Default(ChatType.UNIVERSE);
      this.ServerChat.Default(ChatType.SERVER);
      this.SectorChat.Default(ChatType.SECTOR);
      this.CorpChat.Default(ChatType.CORP);
      this.AllianceChat.Default(ChatType.ALLIANCE);
      this.MarketChat.Default(ChatType.MARKET);
      this.EventsChat.Default(ChatType.EVENT);
      if (this.FriendList == null)
        this.FriendList = new List<StringFilter>();
      if (this.IgnoreList == null)
        this.IgnoreList = new List<StringFilter>();
      this.FilterProfanity = true;
      this.RejectInvites = false;
      this.RejectTrades = false;
      this.chatFontSize = 24;
      this.eventLogFontSize = 18;
      this.PurgeDuplicates();
    }

    public void AddFriend(string name)
    {
      this.RemoveFriend(name);
      this.FriendList.Add(new StringFilter()
      {
        Value = name
      });
      this.PurgeDuplicates();
    }

    public void AddIgnore(string name)
    {
      this.RemoveIgnore(name);
      this.IgnoreList.Add(new StringFilter()
      {
        Value = name
      });
      this.PurgeDuplicates();
    }

    public void RemoveFriend(string name)
    {
      this.FriendList.RemoveAll((Predicate<StringFilter>) (o => string.Equals(o.Value, name, StringComparison.OrdinalIgnoreCase)));
    }

    public void RemoveIgnore(string name)
    {
      this.IgnoreList.RemoveAll((Predicate<StringFilter>) (o => string.Equals(o.Value, name, StringComparison.OrdinalIgnoreCase)));
    }

    public bool IsFriend(string name)
    {
      return this.FriendList.Any<StringFilter>((Func<StringFilter, bool>) (o => string.Equals(o.Value, name, StringComparison.OrdinalIgnoreCase)));
    }

    public bool IsIgnore(string name)
    {
      return this.IgnoreList.Any<StringFilter>((Func<StringFilter, bool>) (o => string.Equals(o.Value, name, StringComparison.OrdinalIgnoreCase)));
    }

    public void PurgeDuplicates()
    {
      HashSet<string> friends = new HashSet<string>();
      this.FriendList.ForEach((Action<StringFilter>) (v => friends.Add(v.Value)));
      this.FriendList = new List<StringFilter>(friends.Select<string, StringFilter>((Func<string, StringFilter>) (v => new StringFilter()
      {
        Value = v
      })));
      HashSet<string> ignores = new HashSet<string>();
      this.IgnoreList.ForEach((Action<StringFilter>) (v => ignores.Add(v.Value)));
      this.IgnoreList = new List<StringFilter>(ignores.Select<string, StringFilter>((Func<string, StringFilter>) (v => new StringFilter()
      {
        Value = v
      })));
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
