﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.AuctionWatchList
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class AuctionWatchList
  {
    public List<AuctionWatcher> Values { get; set; }

    public AuctionWatchList()
    {
      this.Clear();
    }

    public void Clear()
    {
      this.Values = new List<AuctionWatcher>();
    }

    public bool Contains(ClientAuction auction)
    {
      if (auction != null)
        return this.Values.Any<AuctionWatcher>((Func<AuctionWatcher, bool>) (aw =>
        {
          if (aw.AuctionID == auction.ID)
            return (int) aw.ServerID == (int) auction.ServerID;
          return false;
        }));
      return false;
    }

    public void Add(ClientAuction auction)
    {
      if (auction == null || this.Contains(auction))
        return;
      this.Values.Add(new AuctionWatcher()
      {
        AuctionID = auction.ID,
        ServerID = auction.ServerID
      });
    }

    public void Remove(ClientAuction auction)
    {
      if (auction == null)
        return;
      this.Remove(auction.ID, auction.ServerID);
    }

    public void Remove(int auctionId, sbyte serverId)
    {
      this.Values.RemoveAll((Predicate<AuctionWatcher>) (aw =>
      {
        if (aw.AuctionID == auctionId)
          return (int) aw.ServerID == (int) serverId;
        return false;
      }));
    }

    public override string ToString()
    {
      return "Watch List - " + (object) this.Values.Count + " Auctions";
    }
  }
}
