﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.SortedUnitFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class SortedUnitFilter
  {
    public bool Enabled { get; set; }

    public SortedToggleFilter ShipSelf { get; set; }

    public SortedToggleFilter ShipFriend { get; set; }

    public SortedToggleFilter ShipEnemy { get; set; }

    public SortedToggleFilter ShipNeutral { get; set; }

    public SortedToggleFilter FighterSelf { get; set; }

    public SortedToggleFilter FighterFriend { get; set; }

    public SortedToggleFilter FighterEnemy { get; set; }

    public SortedToggleFilter FighterNeutral { get; set; }

    public SortedToggleFilter RepairDroneSelf { get; set; }

    public SortedToggleFilter RepairDroneFriend { get; set; }

    public SortedToggleFilter RepairDroneEnemy { get; set; }

    public SortedToggleFilter RepairDroneNeutral { get; set; }

    public SortedToggleFilter Planet { get; set; }

    public SortedToggleFilter Garrison { get; set; }

    public SortedToggleFilter Intradictor { get; set; }

    public SortedToggleFilter DefensePlatform { get; set; }

    public SortedToggleFilter Mines { get; set; }

    public SortedToggleFilter NPC { get; set; }

    public SortedToggleFilter StarPort { get; set; }

    public SortedToggleFilter CapturePoint { get; set; }

    public SortedToggleFilter Cargo { get; set; }

    public SortedToggleFilter Asteroid { get; set; }

    public SortedUnitFilter()
    {
      this.ShipSelf = new SortedToggleFilter();
      this.ShipFriend = new SortedToggleFilter();
      this.ShipEnemy = new SortedToggleFilter();
      this.ShipNeutral = new SortedToggleFilter();
      this.FighterSelf = new SortedToggleFilter();
      this.FighterFriend = new SortedToggleFilter();
      this.FighterEnemy = new SortedToggleFilter();
      this.FighterNeutral = new SortedToggleFilter();
      this.RepairDroneSelf = new SortedToggleFilter();
      this.RepairDroneFriend = new SortedToggleFilter();
      this.RepairDroneEnemy = new SortedToggleFilter();
      this.RepairDroneNeutral = new SortedToggleFilter();
      this.Planet = new SortedToggleFilter();
      this.Garrison = new SortedToggleFilter();
      this.Intradictor = new SortedToggleFilter();
      this.DefensePlatform = new SortedToggleFilter();
      this.Mines = new SortedToggleFilter();
      this.NPC = new SortedToggleFilter();
      this.StarPort = new SortedToggleFilter();
      this.CapturePoint = new SortedToggleFilter();
      this.Cargo = new SortedToggleFilter();
      this.Asteroid = new SortedToggleFilter();
    }

    public IEnumerable<Entity> Apply(IEnumerable<Entity> values)
    {
      return (IEnumerable<Entity>) values.Where<Entity>(new Func<Entity, bool>(this.IsVisible)).OrderBy<Entity, int>(new Func<Entity, int>(this.GetOrder));
    }

    public void SetAll(bool value)
    {
      this.ShipSelf.Value = value;
      this.ShipFriend.Value = value;
      this.ShipEnemy.Value = value;
      this.ShipNeutral.Value = value;
      this.FighterSelf.Value = value;
      this.FighterFriend.Value = value;
      this.FighterEnemy.Value = value;
      this.FighterNeutral.Value = value;
      this.RepairDroneSelf.Value = value;
      this.RepairDroneFriend.Value = value;
      this.RepairDroneEnemy.Value = value;
      this.RepairDroneNeutral.Value = value;
      this.Planet.Value = value;
      this.Garrison.Value = value;
      this.Intradictor.Value = value;
      this.DefensePlatform.Value = value;
      this.Mines.Value = value;
      this.NPC.Value = value;
      this.StarPort.Value = value;
      this.CapturePoint.Value = value;
      this.Cargo.Value = value;
      this.Asteroid.Value = value;
    }

    public void DefaultSortOrder()
    {
      this.Planet.SortOrder = 0;
      this.Garrison.SortOrder = 1;
      this.StarPort.SortOrder = 2;
      this.CapturePoint.SortOrder = 3;
      this.Intradictor.SortOrder = 4;
      this.DefensePlatform.SortOrder = 5;
      this.Mines.SortOrder = 6;
      this.NPC.SortOrder = 7;
      this.ShipSelf.SortOrder = 8;
      this.ShipFriend.SortOrder = 9;
      this.ShipEnemy.SortOrder = 10;
      this.ShipNeutral.SortOrder = 11;
      this.FighterSelf.SortOrder = 12;
      this.FighterFriend.SortOrder = 13;
      this.FighterEnemy.SortOrder = 14;
      this.FighterNeutral.SortOrder = 15;
      this.RepairDroneSelf.SortOrder = 16;
      this.RepairDroneFriend.SortOrder = 17;
      this.RepairDroneEnemy.SortOrder = 18;
      this.RepairDroneNeutral.SortOrder = 19;
      this.Asteroid.SortOrder = 20;
      this.Cargo.SortOrder = 21;
    }

    public bool IsVisible(Entity entity)
    {
      if (entity != null)
      {
        switch (entity.Type)
        {
          case EntityType.PLANET:
            return this.Planet.Value;
          case EntityType.ASTEROID:
            return this.Asteroid.Value;
          case EntityType.STARPORT:
            return this.StarPort.Value;
          case EntityType.SHIP:
            switch (entity.Relation)
            {
              case RelationType.NEUTRAL:
                return this.ShipNeutral.Value;
              case RelationType.SELF:
                return this.ShipSelf.Value;
              case RelationType.FRIEND:
                return this.ShipFriend.Value;
              case RelationType.ENEMY:
                return this.ShipEnemy.Value;
            }
                break;
          case EntityType.NPC:
            return this.NPC.Value;
          case EntityType.FIGHTER:
            switch (entity.Relation)
            {
              case RelationType.NEUTRAL:
                return this.FighterNeutral.Value;
              case RelationType.SELF:
                return this.FighterSelf.Value;
              case RelationType.FRIEND:
                return this.FighterFriend.Value;
              case RelationType.ENEMY:
                return this.FighterEnemy.Value;
            }
                break;
          case EntityType.DEFENSE_PLATFORM:
            return this.DefensePlatform.Value;
          case EntityType.MINES:
            return this.Mines.Value;
          case EntityType.INTRADICTOR:
            return this.Intradictor.Value;
          case EntityType.REPAIR_DRONE:
            switch (entity.Relation)
            {
              case RelationType.NEUTRAL:
                return this.RepairDroneNeutral.Value;
              case RelationType.SELF:
                return this.RepairDroneSelf.Value;
              case RelationType.FRIEND:
                return this.RepairDroneFriend.Value;
              case RelationType.ENEMY:
                return this.RepairDroneEnemy.Value;
            }
                break;
          case EntityType.GARRISON:
            return this.Garrison.Value;
          case EntityType.CARGO_MONEY:
          case EntityType.CARGO_ITEM:
          case EntityType.CARGO_RESOURCE:
            return this.Cargo.Value;
          case EntityType.CAPTURE_POINT:
            return this.CapturePoint.Value;
        }
      }
      return false;
    }

    public int GetOrder(Entity entity)
    {
      if (entity != null)
      {
        switch (entity.Type)
        {
          case EntityType.PLANET:
            return this.Planet.SortOrder;
          case EntityType.ASTEROID:
            return this.Asteroid.SortOrder;
          case EntityType.STARPORT:
            return this.StarPort.SortOrder;
          case EntityType.SHIP:
            switch (entity.Relation)
            {
              case RelationType.NEUTRAL:
                return this.ShipNeutral.SortOrder;
              case RelationType.SELF:
                return this.ShipSelf.SortOrder;
              case RelationType.FRIEND:
                return this.ShipFriend.SortOrder;
              case RelationType.ENEMY:
                return this.ShipEnemy.SortOrder;
            }
                break;
          case EntityType.NPC:
            return this.NPC.SortOrder;
          case EntityType.FIGHTER:
            switch (entity.Relation)
            {
              case RelationType.NEUTRAL:
                return this.FighterNeutral.SortOrder;
              case RelationType.SELF:
                return this.FighterSelf.SortOrder;
              case RelationType.FRIEND:
                return this.FighterFriend.SortOrder;
              case RelationType.ENEMY:
                return this.FighterEnemy.SortOrder;
            }
                break;
          case EntityType.DEFENSE_PLATFORM:
            return this.DefensePlatform.SortOrder;
          case EntityType.MINES:
            return this.Mines.SortOrder;
          case EntityType.INTRADICTOR:
            return this.Intradictor.SortOrder;
          case EntityType.REPAIR_DRONE:
            switch (entity.Relation)
            {
              case RelationType.NEUTRAL:
                return this.RepairDroneNeutral.SortOrder;
              case RelationType.SELF:
                return this.RepairDroneSelf.SortOrder;
              case RelationType.FRIEND:
                return this.RepairDroneFriend.SortOrder;
              case RelationType.ENEMY:
                return this.RepairDroneEnemy.SortOrder;
            }
                break;
          case EntityType.GARRISON:
            return this.Garrison.SortOrder;
          case EntityType.CARGO_MONEY:
          case EntityType.CARGO_ITEM:
          case EntityType.CARGO_RESOURCE:
            return this.Cargo.SortOrder;
          case EntityType.CAPTURE_POINT:
            return this.CapturePoint.SortOrder;
        }
      }
      return int.MaxValue;
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
