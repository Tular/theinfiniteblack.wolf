﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.EntityListSettings
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class EntityListSettings
  {
    public SortedUnitFilter ColumnA { get; set; }

    public SortedUnitFilter ColumnB { get; set; }

    public SortedUnitFilter ColumnC { get; set; }

    public SortedUnitFilter ColumnD { get; set; }

    public bool TapToRepair { get; set; }

    public bool TapToAttack { get; set; }

    public bool HideOnMapScroll { get; set; }

    public float Scale { get; set; }

    public EntityListSettings()
    {
      this.ColumnA = new SortedUnitFilter();
      this.ColumnB = new SortedUnitFilter();
      this.ColumnC = new SortedUnitFilter();
      this.ColumnD = new SortedUnitFilter();
    }

    public void Default()
    {
      this.TapToRepair = false;
      this.TapToAttack = false;
      this.HideOnMapScroll = false;
      this.Scale = 1.5f;
      this.ColumnA.SetAll(false);
      this.ColumnB.SetAll(false);
      this.ColumnC.SetAll(false);
      this.ColumnD.SetAll(false);
      this.ColumnA.Enabled = false;
      this.ColumnB.Enabled = true;
      this.ColumnC.Enabled = true;
      this.ColumnD.Enabled = true;
      this.ColumnA.DefaultSortOrder();
      this.ColumnB.DefaultSortOrder();
      this.ColumnC.DefaultSortOrder();
      this.ColumnD.DefaultSortOrder();
      this.ColumnB.ShipSelf.Value = true;
      this.ColumnB.RepairDroneSelf.Value = true;
      this.ColumnB.FighterSelf.Value = true;
      this.ColumnB.Asteroid.Value = true;
      this.ColumnB.Cargo.Value = true;
      this.ColumnC.ShipFriend.Value = true;
      this.ColumnC.Planet.Value = true;
      this.ColumnC.Garrison.Value = true;
      this.ColumnC.StarPort.Value = true;
      this.ColumnC.CapturePoint.Value = true;
      this.ColumnD.ShipEnemy.Value = true;
      this.ColumnD.ShipNeutral.Value = true;
      this.ColumnD.Intradictor.Value = true;
      this.ColumnD.DefensePlatform.Value = true;
      this.ColumnD.Mines.Value = true;
      this.ColumnD.NPC.Value = true;
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
