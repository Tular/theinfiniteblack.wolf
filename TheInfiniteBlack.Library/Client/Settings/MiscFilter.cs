﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.MiscFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class MiscFilter
  {
    public RelationFilter Arrivals { get; set; }

    public RelationFilter Departures { get; set; }

    public RelationFilter Gains { get; set; }

    public RelationFilter NonCombat { get; set; }

    public bool FriendOnlineStatus { get; set; }

    public bool CourseStatus { get; set; }

    public bool MovementStatus { get; set; }

    public bool LocationStatus { get; set; }

    public bool FollowStatus { get; set; }

    public bool AttackStatus { get; set; }

    public bool PvPStatus { get; set; }

    public bool MyAuctions { get; set; }

    public bool AllAuctions { get; set; }

    public bool Debug { get; set; }

    public MiscFilter()
    {
      this.NonCombat = new RelationFilter();
      this.Gains = new RelationFilter();
      this.Departures = new RelationFilter();
      this.Arrivals = new RelationFilter();
    }

    public void SetAll(bool value)
    {
      this.Arrivals.SetAll(value);
      this.Departures.SetAll(value);
      this.Gains.SetAll(value);
      this.NonCombat.SetAll(value);
      this.FriendOnlineStatus = value;
      this.CourseStatus = value;
      this.MovementStatus = value;
      this.LocationStatus = value;
      this.FollowStatus = value;
      this.AttackStatus = value;
      this.PvPStatus = value;
      this.MyAuctions = value;
      this.AllAuctions = value;
    }

    public void Default(ChatType type)
    {
      switch (type)
      {
        case ChatType.CORP:
        case ChatType.ALLIANCE:
          this.SetAll(false);
          break;
        case ChatType.EVENT:
          this.SetAll(true);
          this.AllAuctions = false;
          this.CourseStatus = false;
          this.MovementStatus = false;
          break;
        case ChatType.MARKET:
        case ChatType.MARKET_EVENT:
          this.SetAll(false);
          this.MyAuctions = true;
          break;
        default:
          this.SetAll(false);
          break;
      }
    }

    public override string ToString()
    {
      return this.GetType().Name;
    }
  }
}
