﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.AuctionWatcher
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.ComponentModel;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class AuctionWatcher
  {
    private int _auctionID = int.MinValue;

    public sbyte ServerID { get; set; }

    public int AuctionID
    {
      get
      {
        return this._auctionID;
      }
      set
      {
        this._auctionID = value;
      }
    }

    public override string ToString()
    {
      return "Auction #" + (object) this._auctionID + "  (Server " + (string) (object) this.ServerID + ")";
    }
  }
}
