﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Client.Settings.RelationFilter
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.ComponentModel;
using System.Text;

namespace TheInfiniteBlack.Library.Client.Settings
{
  [TypeConverter(typeof (ExpandableObjectConverter))]
  public class RelationFilter
  {
    public bool this[RelationType type]
    {
      get
      {
        switch (type)
        {
          case RelationType.NEUTRAL:
            return this.Neutral;
          case RelationType.SELF:
            return this.Self;
          case RelationType.FRIEND:
            return this.Friend;
          case RelationType.ENEMY:
            return this.Enemy;
          default:
            return false;
        }
      }
      set
      {
        switch (type)
        {
          case RelationType.NEUTRAL:
            this.Neutral = value;
            break;
          case RelationType.SELF:
            this.Self = value;
            break;
          case RelationType.FRIEND:
            this.Friend = value;
            break;
          case RelationType.ENEMY:
            this.Enemy = value;
            break;
        }
      }
    }

    protected internal bool Always
    {
      get
      {
        if (this.Self && this.Friend && this.Enemy)
          return this.Neutral;
        return false;
      }
    }

    protected internal bool Never
    {
      get
      {
        if (!this.Self && !this.Friend && !this.Enemy)
          return !this.Neutral;
        return false;
      }
    }

    public bool Self { get; set; }

    public bool Friend { get; set; }

    public bool Enemy { get; set; }

    public bool Neutral { get; set; }

    public void SetAll(bool value)
    {
      this.Neutral = value;
      this.Enemy = value;
      this.Friend = value;
      this.Self = value;
    }

    public override string ToString()
    {
      if (this.Always)
        return "ALWAYS";
      if (this.Never)
        return "NEVER";
      StringBuilder stringBuilder = new StringBuilder(100);
      if (this.Self)
        stringBuilder.Append("SELF ");
      if (this.Friend)
        stringBuilder.Append("FRIEND ");
      if (this.Enemy)
        stringBuilder.Append("ENEMY ");
      if (this.Neutral)
        stringBuilder.Append("NEUTRAL");
      return stringBuilder.ToString().Trim();
    }
  }
}
