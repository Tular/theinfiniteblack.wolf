﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.PurchaseItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Network;

namespace TheInfiniteBlack.Library
{
  public class PurchaseItem
  {
    private static readonly PurchaseItem[] _values = new PurchaseItem[30];
    public static readonly PurchaseItem EarthJump = new PurchaseItem((sbyte) 25, "Earth Jump", "Open a wormhole passage directly to Earth!", 2, false);
    public static readonly PurchaseItem CorpJump = new PurchaseItem((sbyte) 26, "Corp Jump", "Open a wormhole passage directly to your Corporation Headquarters!", 2, false);
    public static readonly PurchaseItem FullRepair = new PurchaseItem((sbyte) 26, "Full Repair", "Instantly repair all damage to your Ship, Repair Drone, and Fighter!", 2, false);
    public static readonly PurchaseItem RiftJump = new PurchaseItem((sbyte) 0, "Rift Jump", "Jump between The Rift and Normal Space (High Difficulty!)", 2, false);
    public static readonly PurchaseItem HyperDrive = new PurchaseItem((sbyte) 1, "Hyper Drive", "-50% Movement Speed! (For 300 Jumps)", 8, false);
    public static readonly PurchaseItem SuperCharge = new PurchaseItem((sbyte) 2, "Super Charge", "+5 Critical Chance! (For 5 Successful Criticals)", 5, false);
    public static readonly PurchaseItem Tactics = new PurchaseItem((sbyte) 3, nameof (Tactics), "+5 Evasion Chance! (For 10 Successful Misses VS You)", 5, false);
    public static readonly PurchaseItem MindSurge = new PurchaseItem((sbyte) 4, "Mind Surge", "Double all XP earned from kills! (For 20 Kills)", 5, false);
    public static readonly PurchaseItem Accelerate = new PurchaseItem((sbyte) 5, nameof (Accelerate), "-50% Asteroid Harvest Speed! (For 20 Harvests)", 5, false);
    public static readonly PurchaseItem CreditExchange = new PurchaseItem((sbyte) 6, "Credit Exchange", "Sell one BlackDollar for $1,000 Credits!", 1, false);
    public static readonly PurchaseItem RaiseBankCap = new PurchaseItem((sbyte) 7, "Increase Bank Capacity", "Increase your Bank Capacity by +2 Items", 3, false);
    public static readonly PurchaseItem RaiseInventoryCap = new PurchaseItem((sbyte) 8, "Increase Inventory Capacity", "Increase your Ship's Inventory Capacity by +2 Items", 3, false);
    public static readonly PurchaseItem Fighter = new PurchaseItem((sbyte) 10, nameof (Fighter), "Deploy a Fighter that attacks hostile units!", 10, false);
    public static readonly PurchaseItem RepairDrone = new PurchaseItem((sbyte) 11, "Repair Drone", "Deploy a Repair Drone that repairs friendly units!", 10, false);
    public static readonly PurchaseItem Mines = new PurchaseItem((sbyte) 12, "Mine Cluster", "Deploy a deadly cluster of Mines!", 100, true);
    public static readonly PurchaseItem WyrdInvader = new PurchaseItem((sbyte) 13, "Wyrd Invader", "Flagship-Class alien ship with a Splash Damage bonus!", 500, true);
    public static readonly PurchaseItem WyrdAssassin = new PurchaseItem((sbyte) 14, "Wyrd Assassin", "Flayer-Class alien ship with a built-in Stealth Drive!", 1000, true);
    public static readonly PurchaseItem WyrdReaper = new PurchaseItem((sbyte) 15, "Wyrd Reaper", "Executioner-Class alien ship with a built-in Tractor Beam!", 2000, true);
    public static readonly PurchaseItem AllianceHyperDrive = new PurchaseItem((sbyte) 16, "Alliance Hyper Drive", "Purchase Hyper Drive for everyone in your Alliance!", 400, true);
    public static readonly PurchaseItem AllianceSuperCharge = new PurchaseItem((sbyte) 17, "Alliance Super Charge", "Purchase Super Charge for everyone in your Alliance!", 250, true);
    public static readonly PurchaseItem AllianceTactics = new PurchaseItem((sbyte) 18, "Alliance Tactics", "Purchase Tactics for everyone in your Alliance!", 250, true);
    public static readonly PurchaseItem AllianceMindSurge = new PurchaseItem((sbyte) 19, "Alliance Mind Surge", "Purchase Mind Surge for everyone in your Alliance!", 250, true);
    public static readonly PurchaseItem AllianceAccelerate = new PurchaseItem((sbyte) 20, "Alliance Accelerate", "Purchase Accelerate for everyone in your Alliance!", 250, true);
    public readonly sbyte ID;
    public readonly string Name;
    public readonly string Description;
    public readonly int Cost;
    public readonly bool RequireConfirm;
    private INetworkData _data;

    public static IEnumerable<PurchaseItem> Values
    {
      get
      {
        yield return PurchaseItem.FullRepair;
        yield return PurchaseItem.EarthJump;
        yield return PurchaseItem.CorpJump;
        yield return PurchaseItem.RiftJump;
        yield return PurchaseItem.HyperDrive;
        yield return PurchaseItem.SuperCharge;
        yield return PurchaseItem.Tactics;
        yield return PurchaseItem.MindSurge;
        yield return PurchaseItem.Accelerate;
        yield return PurchaseItem.CreditExchange;
        yield return PurchaseItem.RaiseBankCap;
        yield return PurchaseItem.RaiseInventoryCap;
        yield return PurchaseItem.Fighter;
        yield return PurchaseItem.RepairDrone;
        yield return PurchaseItem.Mines;
        yield return PurchaseItem.WyrdInvader;
        yield return PurchaseItem.WyrdAssassin;
        yield return PurchaseItem.WyrdReaper;
        yield return PurchaseItem.AllianceHyperDrive;
        yield return PurchaseItem.AllianceSuperCharge;
        yield return PurchaseItem.AllianceTactics;
        yield return PurchaseItem.AllianceMindSurge;
        yield return PurchaseItem.AllianceAccelerate;
      }
    }

    public INetworkData Data
    {
      get
      {
        if (this._data == null)
        {
          this._data = (INetworkData) new RequestBuffer((sbyte) -44, 1);
          this._data.Write(this.ID);
        }
        return this._data;
      }
    }

    private PurchaseItem(sbyte id, string name, string description, int cost, bool requireConfirm)
    {
      this.ID = id;
      this.Name = name;
      this.Description = description;
      this.Cost = cost;
      this.RequireConfirm = requireConfirm;
      PurchaseItem._values[(int) id] = this;
    }

    public override string ToString()
    {
      return this.Name + " (" + this.Cost.ToString("#,#") + " BlackDollars)";
    }

    public static PurchaseItem Get(sbyte id)
    {
      if ((int) id >= 0 && (int) id < PurchaseItem._values.Length)
        return PurchaseItem._values[(int) id];
      return (PurchaseItem) null;
    }
  }
}
