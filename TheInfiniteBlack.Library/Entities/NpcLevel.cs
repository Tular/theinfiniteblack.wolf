﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.NpcLevel
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Entities
{
  public enum NpcLevel
  {
    NULL = -128,
    WEAK = 0,
    NORMAL = 1,
    ELITE = 2,
    BOSS = 3,
    DREAD = 4,
  }
}
