﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.AsteroidClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Entities
{
  public enum AsteroidClass
  {
    NULL = -128,
    SMALL_ROCK = 0,
    MEDIUM_ROCK = 1,
    LARGE_ROCK = 2,
    VERYLARGE_ROCK = 3,
    SMALL_METAL = 4,
    MEDIUM_METAL = 5,
    LARGE_METAL = 6,
    VERYLARGE_METAL = 7,
    SMALL_RADIOACTIVE = 8,
    MEDIUM_RADIOACTIVE = 9,
    LARGE_RADIOACTIVE = 10,
    VERYLARGE_RADIOACTIVE = 11,
    SMALL_ICE = 12,
    MEDIUM_ICE = 13,
    LARGE_ICE = 14,
    VERYLARGE_ICE = 15,
    SMALL_DARKMATTER = 16,
    MEDIUM_DARKMATTER = 17,
    LARGE_DARKMATTER = 18,
    VERYLARGE_DARKMATTER = 19,
  }
}
