﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.StarPort
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Entities
{
  public class StarPort : NonCombatEntity
  {
    private List<EquipmentItem> _inventory;

    public override EntityType Type
    {
      get
      {
        return EntityType.STARPORT;
      }
    }

    public override string Title
    {
      get
      {
        return nameof (StarPort);
      }
    }

    public override string SubTitle
    {
      get
      {
        return string.Empty;
      }
    }

    public List<EquipmentItem> Inventory
    {
      get
      {
        return this._inventory;
      }
      set
      {
        this._inventory = value;
        this.OnChanged();
      }
    }

    public StarPort(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      this.State.UI.Show(new ShowEntityDetailEventArgs(this.State, (Entity) this));
    }

    public override void OnLongPress()
    {
      this.State.DoRepairWith(this);
    }

    protected internal override void Set(IByteBuffer input)
    {
      int capacity = (int) input.Get();
      List<EquipmentItem> equipmentItemList = new List<EquipmentItem>(capacity);
      for (int index = 0; index < capacity; ++index)
      {
        EquipmentItem equipmentItem = EquipmentItem.Execute(input);
        if (equipmentItem != null)
          equipmentItemList.Add(equipmentItem);
      }
      this._inventory = equipmentItemList;
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      sb.Append(nameof (StarPort));
    }
  }
}
