﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.PlanetClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Entities
{
  public enum PlanetClass
  {
    NULL = -128,
    None = -1,
    Dead = 0,
    Airless = 1,
    Metallic = 2,
    Ice = 3,
    Barren = 4,
    Desert = 5,
    Arid = 6,
    Primordial = 7,
    Terrestrial = 8,
    Fertile = 9,
    Gaia_I = 10,
    Gaia_II = 11,
    Gaia_III = 12,
    Gaia_IV = 13,
    Gaia_V = 14,
  }
}
