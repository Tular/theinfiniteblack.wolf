﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.CombatEntity
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Library.Entities
{
  public abstract class CombatEntity : Entity
  {
    protected int _hull;
    private long _stunEndTime;

    public abstract int MaxHull { get; }

    public int Hull
    {
      get
      {
        return this._hull;
      }
      set
      {
        if (this._hull == value)
          return;
        int hull = this._hull;
        this._hull = value;
        this.OnChanged();
        this.State.UI.Show(new EntityHullChangedEventArgs(this.State, this, hull, this._hull));
      }
    }

    public bool Grappled { get; set; }

    public bool Stunned
    {
      get
      {
        return this._stunEndTime > this.State.WorldTimeMS;
      }
    }

    protected internal CombatEntity(IGameState state, int id)
      : base(state, id)
    {
    }

    protected internal override void Set(IByteBuffer input)
    {
      this.Hull = (int) input.GetShort();
    }

    protected internal void SetStunned(int seconds)
    {
      this._stunEndTime = this.State.WorldTimeMS + (long) seconds * 1000L;
    }
  }
}
