﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Npc
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class Npc : CombatEntity
  {
    private NpcClass _class;
    private NpcLevel _level;

    public override EntityType Type
    {
      get
      {
        return EntityType.NPC;
      }
    }

    public override string Title
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder(50);
        if (this._level != NpcLevel.NORMAL)
        {
          stringBuilder.Append(this._level.Name());
          stringBuilder.Append(" ");
        }
        stringBuilder.Append(this._class.Name());
        return stringBuilder.ToString();
      }
    }

    public override string SubTitle
    {
      get
      {
        switch (this._class.Faction())
        {
          case NpcFaction.HETEROCLITE:
            return "<Het>";
          case NpcFaction.PIRATE:
            return "<Pir>";
          case NpcFaction.WYRD:
            return "<Wyrd>";
          case NpcFaction.RIFT:
            return "<The Rift>";
          default:
            return "<Unknown>";
        }
      }
    }

    public NpcFaction Faction
    {
      get
      {
        return this._class.Faction();
      }
    }

    public bool Heteroclite
    {
      get
      {
        return this.Faction == NpcFaction.HETEROCLITE;
      }
    }

    public bool Pirate
    {
      get
      {
        return this.Faction == NpcFaction.PIRATE;
      }
    }

    public bool Wyrd
    {
      get
      {
        return this.Faction == NpcFaction.WYRD;
      }
    }

    public bool Immobile
    {
      get
      {
        switch (this._class)
        {
          case NpcClass.Hive:
          case NpcClass.Stronghold:
          case NpcClass.SeedWorld:
            return true;
          default:
            return false;
        }
      }
    }

    public override int MaxHull
    {
      get
      {
        int num1 = this._class.HullBase(this._level);
        int num2;
        switch (this._level)
        {
          case NpcLevel.ELITE:
            num2 = this.Immobile ? 30000 : num1 * 2;
            break;
          case NpcLevel.BOSS:
            num2 = this.Immobile ? 31000 : num1 * 4;
            break;
          case NpcLevel.DREAD:
            num2 = this.Immobile ? 32000 : num1 * 6;
            break;
          default:
            num2 = this.Immobile ? 29000 : num1;
            break;
        }
        if (num2 <= (int) short.MaxValue)
          return num2;
        return (int) short.MaxValue;
      }
    }

    public NpcClass Class
    {
      get
      {
        return this._class;
      }
    }

    public NpcLevel Level
    {
      get
      {
        return this._level;
      }
    }

    public Npc(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
      if (this.State.MyAttackTarget == this)
      {
        this.State.DoAttack((CombatEntity) null);
        if (this.State.MyFollowTarget != this)
          return;
        this.State.DoFollow((CombatEntity) null);
      }
      else
        this.State.DoAttack((CombatEntity) this);
    }

    public override void OnLongPress()
    {
      if (this.State.MyAttackTarget != this)
        this.State.DoAttack((CombatEntity) this);
      if (this.State.MyFollowTarget == this || this.State.MyFollowTarget != null && this.State.MyFollowTarget.Relation == RelationType.FRIEND)
        return;
      this.State.DoFollow((CombatEntity) this);
    }

    protected internal override void Set(IByteBuffer input)
    {
      base.Set(input);
      this._class = (NpcClass) input.Get();
      this._level = (NpcLevel) input.Get();
    }

    protected internal void Set(NpcClass npcClass, NpcLevel npcLevel)
    {
      this._class = npcClass;
      this._level = npcLevel;
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append("[r]");
      if (this._level != NpcLevel.NORMAL)
      {
        sb.Append(this._level.Name());
        sb.Append(" ");
      }
      sb.Append(this._class.Faction().Name());
      sb.Append(" ");
      sb.Append(this._class.Name());
      if (!markup)
        return;
      sb.Append("[-]");
    }

    public int MinimumDamage
    {
      get
      {
        return CombatHelper.getNpcMinDamage(this.Class.EP(this.Level), this.WeaponSpeedMS);
      }
    }

    public int MaxCrit
    {
      get
      {
        return (int) ((double) (this.MinimumDamage * 2) * (2.0 + (double) this.CriticalDamageMod));
      }
    }

    private float CriticalDamageMod
    {
      get
      {
        return (float) ((int) (this.Level + 1) / 10);
      }
    }

    private int WeaponSpeedMS
    {
      get
      {
        switch (this.Class)
        {
          case NpcClass.Drone:
          case NpcClass.Brigand:
          case NpcClass.Wayfarer:
          case NpcClass.SeedWorld:
            return 8000;
          case NpcClass.Warrior:
          case NpcClass.Prowler:
          case NpcClass.Invader:
            return 9000;
          case NpcClass.Defender:
          case NpcClass.Marauder:
          case NpcClass.Assassin:
            return 10000;
          case NpcClass.Predator:
          case NpcClass.Corsair:
          case NpcClass.Reaper:
            return 11000;
          case NpcClass.Queen:
          case NpcClass.Enforcer:
            return 12000;
          case NpcClass.King:
            return 13000;
          case NpcClass.Hive:
          case NpcClass.Outlaw:
            return 6000;
          case NpcClass.Raider:
            return 4000;
          case NpcClass.Bandit:
            return 5000;
          case NpcClass.Hijacker:
          case NpcClass.Stronghold:
          case NpcClass.Colonizer:
            return 7000;
          default:
            return 0;
        }
      }
    }

    public bool IsSeed()
    {
      return this.Class == NpcClass.SeedWorld || this.Class == NpcClass.Stronghold || this.Class == NpcClass.Hive;
    }
  }
}
