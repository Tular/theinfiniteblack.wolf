﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.NpcClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Entities
{
  public enum NpcClass
  {
    NULL = -128,
    Drone = 0,
    Warrior = 1,
    Defender = 2,
    Predator = 3,
    Queen = 4,
    King = 5,
    Hive = 6,
    Raider = 10,
    Bandit = 11,
    Outlaw = 12,
    Hijacker = 13,
    Brigand = 14,
    Prowler = 15,
    Marauder = 16,
    Corsair = 17,
    Enforcer = 18,
    Stronghold = 19,
    Colonizer = 30,
    Wayfarer = 31,
    Invader = 32,
    Assassin = 33,
    Reaper = 34,
    SeedWorld = 35,
  }
}
