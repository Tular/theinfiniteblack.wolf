﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.CapturePoint
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public class CapturePoint : NonCombatEntity
  {
    private ClientAlliance _alliance;

    public override EntityType Type
    {
      get
      {
        return EntityType.CAPTURE_POINT;
      }
    }

    public override string Title
    {
      get
      {
        return "Capture Point";
      }
    }

    public override string SubTitle
    {
      get
      {
        if (this.Alliance == null)
          return "FREE";
        StringBuilder sb = new StringBuilder(100);
        sb.Append("<");
        this.Alliance.AppendName(sb, false);
        sb.Append(">");
        return sb.ToString();
      }
    }

    public ClientAlliance Alliance
    {
      get
      {
        return this._alliance;
      }
    }

    public CapturePoint(IGameState state, int id)
      : base(state, id)
    {
    }

    public override void OnClick()
    {
    }

    public override void OnLongPress()
    {
      this.OnClick();
    }

    protected internal override void Set(IByteBuffer input)
    {
      this._alliance = this.State.Alliances.Get(input.GetShort());
    }

    public override void AppendName(StringBuilder sb, bool markup)
    {
      if (markup)
        sb.Append(this.Relation.GetMarkup());
      if (this._alliance != null)
      {
        this._alliance.AppendName(sb, markup);
        sb.Append("'s Capture Point");
      }
      else
        sb.Append("Capture Point");
      if (!markup)
        return;
      sb.Append("[-]");
    }
  }
}
