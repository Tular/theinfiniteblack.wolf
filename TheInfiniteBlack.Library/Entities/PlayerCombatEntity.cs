﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.PlayerCombatEntity
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public abstract class PlayerCombatEntity : CombatEntity
  {
    private ClientPlayer _player;
    private ClientCorporation _corporation;
    private ClientAlliance _alliance;
    private Technology _tech;

    public ClientPlayer Player
    {
      get
      {
        return this._player;
      }
    }

    public ClientCorporation Corporation
    {
      get
      {
        return this._corporation;
      }
    }

    public ClientAlliance Alliance
    {
      get
      {
        return this._alliance;
      }
    }

    public Technology Tech
    {
      get
      {
        return this._tech;
      }
    }

    protected internal PlayerCombatEntity(IGameState state, int id)
      : base(state, id)
    {
    }

    protected internal override void Set(IByteBuffer input)
    {
      base.Set(input);
      this._player = this.State.Players.Get(input.GetInt());
      this._corporation = this.State.Corporations.Get(input.GetShort());
      this._alliance = this.State.Alliances.Get(input.GetShort());
      this._tech = this._corporation == null ? (Technology) null : this._corporation.Tech;
    }
  }
}
