﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.ShipClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Entities
{
  public enum ShipClass
  {
    NULL = -128,
    None = -1,
    Shuttle = 0,
    Corvette = 1,
    Frigate = 2,
    Destroyer = 3,
    Cruiser = 4,
    Battleship = 5,
    Dreadnought = 6,
    Titan = 7,
    Flagship = 8,
    Carrier = 9,
    Flayer = 10,
    Executioner = 11,
    Devastator = 12,
    Despoiler = 13,
    Annihilator = 14,
    WyrdInvader = 15,
    WyrdAssassin = 16,
    WyrdReaper = 17,
    WyrdTerminus = 18,
    LAST = 19,
  }
}
