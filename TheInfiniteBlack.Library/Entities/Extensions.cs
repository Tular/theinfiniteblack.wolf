﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.Extensions
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Entities
{
  public static class Extensions
  {
    public static Entity Create(this EntityType type, IGameState state, int id)
    {
      switch (type)
      {
        case EntityType.PLANET:
          return (Entity) new Planet(state, id);
        case EntityType.ASTEROID:
          return (Entity) new Asteroid(state, id);
        case EntityType.STARPORT:
          return (Entity) new StarPort(state, id);
        case EntityType.SHIP:
          return (Entity) new Ship(state, id);
        case EntityType.NPC:
          return (Entity) new Npc(state, id);
        case EntityType.FIGHTER:
          return (Entity) new Fighter(state, id);
        case EntityType.DEFENSE_PLATFORM:
          return (Entity) new DefensePlatform(state, id);
        case EntityType.MINES:
          return (Entity) new Mines(state, id);
        case EntityType.INTRADICTOR:
          return (Entity) new Intradictor(state, id);
        case EntityType.REPAIR_DRONE:
          return (Entity) new RepairDrone(state, id);
        case EntityType.GARRISON:
          return (Entity) new Garrison(state, id);
        case EntityType.CARGO_MONEY:
          return (Entity) new CargoMoney(state, id);
        case EntityType.CARGO_ITEM:
          return (Entity) new CargoItem(state, id);
        case EntityType.CARGO_RESOURCE:
          return (Entity) new CargoResource(state, id);
        case EntityType.CAPTURE_POINT:
          return (Entity) new CapturePoint(state, id);
        default:
          return (Entity) null;
      }
    }

    public static bool CanBeAttacked(this EntityType type)
    {
      switch (type)
      {
        case EntityType.PLANET:
        case EntityType.SHIP:
        case EntityType.NPC:
        case EntityType.DEFENSE_PLATFORM:
        case EntityType.INTRADICTOR:
        case EntityType.GARRISON:
          return true;
        default:
          return false;
      }
    }

    public static bool CanBeFollowed(this EntityType type)
    {
      return type == EntityType.SHIP || type == EntityType.NPC;
    }

    public static bool CanBeRepaired(this EntityType type)
    {
      return type == EntityType.SHIP || type == EntityType.FIGHTER || type == EntityType.REPAIR_DRONE;
    }

    public static bool CanBeDeveloped(this EntityType type)
    {
      return type == EntityType.PLANET || type == EntityType.GARRISON;
    }

    public static string Name(this NpcClass npc)
    {
      switch (npc)
      {
        case NpcClass.Drone:
          return "Drone";
        case NpcClass.Warrior:
          return "Warrior";
        case NpcClass.Defender:
          return "Defender";
        case NpcClass.Predator:
          return "Predator";
        case NpcClass.Queen:
          return "Queen";
        case NpcClass.King:
          return "King";
        case NpcClass.Hive:
          return "Hive";
        case NpcClass.Raider:
          return "Raider";
        case NpcClass.Bandit:
          return "Bandit";
        case NpcClass.Outlaw:
          return "Outlaw";
        case NpcClass.Hijacker:
          return "Hijacker";
        case NpcClass.Brigand:
          return "Brigand";
        case NpcClass.Prowler:
          return "Prowler";
        case NpcClass.Marauder:
          return "Marauder";
        case NpcClass.Corsair:
          return "Corsair";
        case NpcClass.Enforcer:
          return "Enforcer";
        case NpcClass.Stronghold:
          return "Stronghold";
        case NpcClass.Colonizer:
          return "Colonizer";
        case NpcClass.Wayfarer:
          return "Wayfarer";
        case NpcClass.Invader:
          return "Invader";
        case NpcClass.Assassin:
          return "Assassin";
        case NpcClass.Reaper:
          return "Reaper";
        case NpcClass.SeedWorld:
          return "Seed World";
        default:
          return "Unknown";
      }
    }

    public static NpcFaction Faction(this NpcClass npc)
    {
      switch (npc)
      {
        case NpcClass.Drone:
        case NpcClass.Warrior:
        case NpcClass.Defender:
        case NpcClass.Predator:
        case NpcClass.Queen:
        case NpcClass.King:
        case NpcClass.Hive:
          return NpcFaction.HETEROCLITE;
        case NpcClass.Raider:
        case NpcClass.Bandit:
        case NpcClass.Outlaw:
        case NpcClass.Hijacker:
        case NpcClass.Brigand:
        case NpcClass.Prowler:
        case NpcClass.Marauder:
        case NpcClass.Corsair:
        case NpcClass.Enforcer:
        case NpcClass.Stronghold:
          return NpcFaction.PIRATE;
        case NpcClass.Colonizer:
        case NpcClass.Wayfarer:
        case NpcClass.Invader:
        case NpcClass.Assassin:
        case NpcClass.Reaper:
        case NpcClass.SeedWorld:
          return NpcFaction.WYRD;
        default:
          return NpcFaction.None;
      }
    }

    public static int EP(this NpcClass npc, NpcLevel level)
    {
      switch (npc)
      {
        case NpcClass.Drone:
          return 1 + (int) level * 2;
        case NpcClass.Warrior:
          return 3 + (int) level * 2;
        case NpcClass.Defender:
          return 5 + (int) level * 2;
        case NpcClass.Predator:
          return 9 + (int) level * 2;
        case NpcClass.Queen:
          return 12 + (int) level * 2;
        case NpcClass.King:
          return 15 + (int) level * 2;
        case NpcClass.Hive:
          return 20 + (int) level * 3;
        case NpcClass.Raider:
          return 1 + (int) level * 2;
        case NpcClass.Bandit:
          return 2 + (int) level * 2;
        case NpcClass.Outlaw:
          return 4 + (int) level * 2;
        case NpcClass.Hijacker:
          return 6 + (int) level * 2;
        case NpcClass.Brigand:
          return 8 + (int) level * 2;
        case NpcClass.Prowler:
          return 10 + (int) level * 2;
        case NpcClass.Marauder:
          return 14 + (int) level * 2;
        case NpcClass.Corsair:
          return 16 + (int) level * 2;
        case NpcClass.Enforcer:
          return 18 + (int) level * 2;
        case NpcClass.Stronghold:
          return 23 + (int) level * 3;
        case NpcClass.Colonizer:
          return 5 + (int) level * 2;
        case NpcClass.Wayfarer:
          return 9 + (int) level * 2;
        case NpcClass.Invader:
          return 13 + (int) level * 2;
        case NpcClass.Assassin:
          return 17 + (int) level * 2;
        case NpcClass.Reaper:
          return 21 + (int) level * 2;
        case NpcClass.SeedWorld:
          return 26 + (int) level * 3;
        default:
          return 0;
      }
    }

    public static int HullBase(this NpcClass npc, NpcLevel level)
    {
      int num = npc.EP(level);
      switch (npc)
      {
        case NpcClass.Drone:
        case NpcClass.Warrior:
        case NpcClass.Defender:
        case NpcClass.Predator:
        case NpcClass.Queen:
        case NpcClass.King:
        case NpcClass.Hive:
          return num * 180;
        case NpcClass.Raider:
        case NpcClass.Bandit:
        case NpcClass.Outlaw:
        case NpcClass.Hijacker:
        case NpcClass.Brigand:
        case NpcClass.Prowler:
        case NpcClass.Marauder:
        case NpcClass.Corsair:
        case NpcClass.Enforcer:
        case NpcClass.Stronghold:
          return num * 190;
        case NpcClass.Colonizer:
        case NpcClass.Wayfarer:
        case NpcClass.Invader:
        case NpcClass.Assassin:
        case NpcClass.Reaper:
        case NpcClass.SeedWorld:
          return num * 200;
        default:
          return 0;
      }
    }

    public static string Name(this NpcFaction faction)
    {
      switch (faction)
      {
        case NpcFaction.HETEROCLITE:
          return "Heteroclite";
        case NpcFaction.PIRATE:
          return "Pirate";
        case NpcFaction.WYRD:
          return "Wyrd";
        case NpcFaction.RIFT:
          return "Rift";
        default:
          return "Unknown";
      }
    }

    public static string Name(this NpcLevel level)
    {
      switch (level)
      {
        case NpcLevel.WEAK:
          return "Weak";
        case NpcLevel.NORMAL:
          return "Normal";
        case NpcLevel.ELITE:
          return "Elite";
        case NpcLevel.BOSS:
          return "Boss";
        case NpcLevel.DREAD:
          return "Dread";
        default:
          return "Unknown";
      }
    }

    public static string Name(this PlanetClass planet)
    {
      switch (planet)
      {
        case PlanetClass.Dead:
          return "Dead World";
        case PlanetClass.Airless:
          return "Airless World";
        case PlanetClass.Metallic:
          return "Metallic World";
        case PlanetClass.Ice:
          return "Ice World";
        case PlanetClass.Barren:
          return "Barren World";
        case PlanetClass.Desert:
          return "Desert World";
        case PlanetClass.Arid:
          return "Arid World";
        case PlanetClass.Primordial:
          return "Primordial World";
        case PlanetClass.Terrestrial:
          return "Terrestrial World";
        case PlanetClass.Fertile:
          return "Fertile World";
        case PlanetClass.Gaia_I:
          return "Gaia I World";
        case PlanetClass.Gaia_II:
          return "Gaia II World";
        case PlanetClass.Gaia_III:
          return "Gaia III World";
        case PlanetClass.Gaia_IV:
          return "Gaia IV World";
        case PlanetClass.Gaia_V:
          return "Gaia V World";
        default:
          return "ERR";
      }
    }

    public static float ResearchUnitProgressValue(this PlanetClass planet)
    {
      switch (planet)
      {
        case PlanetClass.Dead:
          return 0.1f;
        case PlanetClass.Airless:
          return 0.08f;
        case PlanetClass.Metallic:
          return 0.06f;
        case PlanetClass.Ice:
          return 0.04f;
        case PlanetClass.Barren:
          return 0.02f;
        case PlanetClass.Desert:
          return 0.01f;
        case PlanetClass.Arid:
          return 0.005f;
        case PlanetClass.Primordial:
          return 1f / 500f;
        case PlanetClass.Terrestrial:
          return 1f / 1000f;
        case PlanetClass.Fertile:
          return 0.0005f;
        case PlanetClass.Gaia_I:
          return 0.0001f;
        case PlanetClass.Gaia_II:
          return 7E-05f;
        case PlanetClass.Gaia_III:
          return 5E-05f;
        case PlanetClass.Gaia_IV:
          return 4E-05f;
        case PlanetClass.Gaia_V:
          return 3E-05f;
        default:
          return 3E-05f;
      }
    }

    public static bool CanBuildShip(this PlanetClass planet, ShipClass ship)
    {
      switch (planet)
      {
        case PlanetClass.Airless:
          return ship <= ShipClass.Shuttle;
        case PlanetClass.Metallic:
          return ship <= ShipClass.Corvette;
        case PlanetClass.Ice:
          return ship <= ShipClass.Frigate;
        case PlanetClass.Barren:
          return ship <= ShipClass.Destroyer;
        case PlanetClass.Desert:
          return ship <= ShipClass.Cruiser;
        case PlanetClass.Arid:
          return ship <= ShipClass.Battleship;
        case PlanetClass.Primordial:
          return ship <= ShipClass.Dreadnought;
        case PlanetClass.Terrestrial:
          return ship <= ShipClass.Titan;
        case PlanetClass.Fertile:
          return ship <= ShipClass.Flagship;
        case PlanetClass.Gaia_I:
          return ship <= ShipClass.Flayer;
        case PlanetClass.Gaia_II:
          return ship <= ShipClass.Executioner;
        case PlanetClass.Gaia_III:
          return ship <= ShipClass.Devastator;
        case PlanetClass.Gaia_IV:
          return ship <= ShipClass.Despoiler;
        case PlanetClass.Gaia_V:
          return ship <= ShipClass.Annihilator;
        default:
          return false;
      }
    }

    public static string Name(this AsteroidClass type)
    {
      switch (type)
      {
        case AsteroidClass.SMALL_ROCK:
        case AsteroidClass.MEDIUM_ROCK:
        case AsteroidClass.LARGE_ROCK:
        case AsteroidClass.VERYLARGE_ROCK:
          return "Rock Asteroid";
        case AsteroidClass.SMALL_METAL:
        case AsteroidClass.MEDIUM_METAL:
        case AsteroidClass.LARGE_METAL:
        case AsteroidClass.VERYLARGE_METAL:
          return "Metal Asteroid";
        case AsteroidClass.SMALL_RADIOACTIVE:
        case AsteroidClass.MEDIUM_RADIOACTIVE:
        case AsteroidClass.LARGE_RADIOACTIVE:
        case AsteroidClass.VERYLARGE_RADIOACTIVE:
          return "Radioactive Asteroid";
        case AsteroidClass.SMALL_ICE:
        case AsteroidClass.MEDIUM_ICE:
        case AsteroidClass.LARGE_ICE:
        case AsteroidClass.VERYLARGE_ICE:
          return "Ice Asteroid";
        case AsteroidClass.SMALL_DARKMATTER:
        case AsteroidClass.MEDIUM_DARKMATTER:
        case AsteroidClass.LARGE_DARKMATTER:
        case AsteroidClass.VERYLARGE_DARKMATTER:
          return "DarkMatter Asteroid";
        default:
          return "Unknown";
      }
    }

    public static string Size(this AsteroidClass type)
    {
      switch (type)
      {
        case AsteroidClass.SMALL_ROCK:
        case AsteroidClass.SMALL_METAL:
        case AsteroidClass.SMALL_RADIOACTIVE:
        case AsteroidClass.SMALL_ICE:
        case AsteroidClass.SMALL_DARKMATTER:
          return "Small";
        case AsteroidClass.MEDIUM_ROCK:
        case AsteroidClass.MEDIUM_METAL:
        case AsteroidClass.MEDIUM_RADIOACTIVE:
        case AsteroidClass.MEDIUM_ICE:
        case AsteroidClass.MEDIUM_DARKMATTER:
          return "Medium";
        case AsteroidClass.LARGE_ROCK:
        case AsteroidClass.LARGE_METAL:
        case AsteroidClass.LARGE_RADIOACTIVE:
        case AsteroidClass.LARGE_ICE:
        case AsteroidClass.LARGE_DARKMATTER:
          return "Large";
        case AsteroidClass.VERYLARGE_ROCK:
        case AsteroidClass.VERYLARGE_METAL:
        case AsteroidClass.VERYLARGE_RADIOACTIVE:
        case AsteroidClass.VERYLARGE_ICE:
        case AsteroidClass.VERYLARGE_DARKMATTER:
          return "Very Large";
        default:
          return "Unknown";
      }
    }

    public static string Name(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Shuttle:
          return "Shuttle";
        case ShipClass.Corvette:
          return "Corvette";
        case ShipClass.Frigate:
          return "Frigate";
        case ShipClass.Destroyer:
          return "Destroyer";
        case ShipClass.Cruiser:
          return "Cruiser";
        case ShipClass.Battleship:
          return "Battleship";
        case ShipClass.Dreadnought:
          return "Dreadnought";
        case ShipClass.Titan:
          return "Titan";
        case ShipClass.Flagship:
          return "Flagship";
        case ShipClass.Carrier:
          return "Carrier";
        case ShipClass.Flayer:
          return "Flayer";
        case ShipClass.Executioner:
          return "Executioner";
        case ShipClass.Devastator:
          return "Devastator";
        case ShipClass.Despoiler:
          return "Despoiler";
        case ShipClass.Annihilator:
          return "Annihilator";
        case ShipClass.WyrdInvader:
          return "Wyrd Invader";
        case ShipClass.WyrdAssassin:
          return "Wyrd Assassin";
        case ShipClass.WyrdReaper:
          return "Wyrd Reaper";
        case ShipClass.WyrdTerminus:
          return "Wyrd Terminus";
        default:
          return "Unknown?";
      }
    }

    public static bool BuyRequiresPlanet(this ShipClass ship, CurrencyType currency)
    {
      switch (ship)
      {
        case ShipClass.WyrdInvader:
        case ShipClass.WyrdAssassin:
        case ShipClass.WyrdReaper:
        case ShipClass.WyrdTerminus:
          return false;
        default:
          if (currency != CurrencyType.BlackDollars)
            return currency == CurrencyType.Credits;
          return true;
      }
    }

    public static bool CanBuy(this ShipClass ship, CurrencyType currency)
    {
      switch (currency)
      {
        case CurrencyType.Credits:
          switch (ship)
          {
            case ShipClass.Shuttle:
            case ShipClass.WyrdInvader:
            case ShipClass.WyrdAssassin:
            case ShipClass.WyrdReaper:
            case ShipClass.WyrdTerminus:
              return false;
            default:
              return true;
          }
        case CurrencyType.BlackDollars:
          if (ship > ShipClass.Shuttle)
            return ship < ShipClass.WyrdTerminus;
          return false;
        case CurrencyType.RewardPoints:
          switch (ship)
          {
            case ShipClass.Flayer:
            case ShipClass.Executioner:
            case ShipClass.Devastator:
            case ShipClass.Despoiler:
            case ShipClass.Annihilator:
              return true;
            default:
              return false;
          }
        case CurrencyType.CombatPoints:
          return ship == ShipClass.WyrdTerminus;
        default:
          return false;
      }
    }

    public static int GetCost(this ShipClass ship, CurrencyType currency)
    {
      switch (currency)
      {
        case CurrencyType.Credits:
          return ship.CreditCost();
        case CurrencyType.BlackDollars:
          return ship.BlackDollarCost();
        case CurrencyType.RewardPoints:
          return ship.RewardPointCost();
        case CurrencyType.CombatPoints:
          return ship.CombatPointCost();
        default:
          return int.MaxValue;
      }
    }

    public static int BlackDollarCost(this ShipClass ship)
    {
      if (!ship.CanBuy(CurrencyType.BlackDollars))
        return int.MaxValue;
      switch (ship)
      {
        case ShipClass.WyrdInvader:
          return PurchaseItem.WyrdInvader.Cost;
        case ShipClass.WyrdAssassin:
          return PurchaseItem.WyrdAssassin.Cost;
        case ShipClass.WyrdReaper:
          return PurchaseItem.WyrdReaper.Cost;
        default:
          return EquipmentItem.GetBlackDollarValue((float) ship.CreditCost());
      }
    }

    public static int CreditCost(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Corvette:
          return 3900;
        case ShipClass.Frigate:
          return 7800;
        case ShipClass.Destroyer:
          return 15500;
        case ShipClass.Cruiser:
          return 31000;
        case ShipClass.Battleship:
          return 62500;
        case ShipClass.Dreadnought:
          return 125000;
        case ShipClass.Titan:
          return 250000;
        case ShipClass.Flagship:
          return 500000;
        case ShipClass.Carrier:
          return 25000000;
        case ShipClass.Flayer:
          return 1000000;
        case ShipClass.Executioner:
          return 2000000;
        case ShipClass.Devastator:
          return 4000000;
        case ShipClass.Despoiler:
          return 8000000;
        case ShipClass.Annihilator:
          return 16000000;
        default:
          return int.MaxValue;
      }
    }

    public static int CombatPointCost(this ShipClass ship)
    {
      return ship != ShipClass.WyrdTerminus ? int.MaxValue : 30000;
    }

    public static int RewardPointCost(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Flayer:
          return 10;
        case ShipClass.Executioner:
          return 20;
        case ShipClass.Devastator:
          return 30;
        case ShipClass.Despoiler:
          return 50;
        case ShipClass.Annihilator:
          return 125;
        default:
          return int.MaxValue;
      }
    }

    public static int MoveSpeedPenalty(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Shuttle:
          return 0;
        case ShipClass.Corvette:
          return 0;
        case ShipClass.Frigate:
          return 200;
        case ShipClass.Destroyer:
          return 400;
        case ShipClass.Cruiser:
          return 600;
        case ShipClass.Battleship:
          return 800;
        case ShipClass.Dreadnought:
          return 1000;
        case ShipClass.Titan:
          return 1200;
        case ShipClass.Flagship:
        case ShipClass.Carrier:
        case ShipClass.WyrdInvader:
          return 1500;
        case ShipClass.Flayer:
        case ShipClass.WyrdAssassin:
          return 2000;
        case ShipClass.Executioner:
        case ShipClass.WyrdReaper:
          return 2500;
        case ShipClass.Devastator:
          return 3000;
        case ShipClass.Despoiler:
          return 3500;
        case ShipClass.Annihilator:
          return 4000;
        case ShipClass.WyrdTerminus:
          return 3000;
        default:
          return int.MaxValue;
      }
    }

    public static float EvasionBonus(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Shuttle:
          return 22f;
        case ShipClass.Corvette:
          return 20f;
        case ShipClass.Frigate:
          return 18f;
        case ShipClass.Destroyer:
          return 16f;
        case ShipClass.Cruiser:
          return 14f;
        case ShipClass.Battleship:
          return 12f;
        case ShipClass.Dreadnought:
          return 10f;
        case ShipClass.Titan:
          return 8f;
        case ShipClass.Flagship:
        case ShipClass.Carrier:
        case ShipClass.WyrdInvader:
          return 6f;
        case ShipClass.Flayer:
        case ShipClass.WyrdAssassin:
          return 4f;
        case ShipClass.Executioner:
        case ShipClass.WyrdReaper:
          return 2f;
        case ShipClass.Devastator:
          return 1f;
        case ShipClass.Despoiler:
          return 0.5f;
        case ShipClass.Annihilator:
        case ShipClass.WyrdTerminus:
          return 0.0f;
        default:
          return float.MinValue;
      }
    }

    public static int BaseEquipPoints(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Shuttle:
          return 3;
        case ShipClass.Corvette:
          return 4;
        case ShipClass.Frigate:
          return 5;
        case ShipClass.Destroyer:
          return 6;
        case ShipClass.Cruiser:
          return 7;
        case ShipClass.Battleship:
          return 8;
        case ShipClass.Dreadnought:
          return 9;
        case ShipClass.Titan:
          return 10;
        case ShipClass.Flagship:
        case ShipClass.Carrier:
        case ShipClass.WyrdInvader:
          return 11;
        case ShipClass.Flayer:
        case ShipClass.WyrdAssassin:
          return 12;
        case ShipClass.Executioner:
        case ShipClass.WyrdReaper:
          return 13;
        case ShipClass.Devastator:
          return 14;
        case ShipClass.Despoiler:
          return 16;
        case ShipClass.Annihilator:
          return 18;
        case ShipClass.WyrdTerminus:
          return 19;
        default:
          return 0;
      }
    }

    public static PlanetClass RequiredPlanetClass(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Shuttle:
          return PlanetClass.Airless;
        case ShipClass.Corvette:
          return PlanetClass.Metallic;
        case ShipClass.Frigate:
          return PlanetClass.Ice;
        case ShipClass.Destroyer:
          return PlanetClass.Barren;
        case ShipClass.Cruiser:
          return PlanetClass.Desert;
        case ShipClass.Battleship:
          return PlanetClass.Arid;
        case ShipClass.Dreadnought:
          return PlanetClass.Primordial;
        case ShipClass.Titan:
          return PlanetClass.Terrestrial;
        case ShipClass.Flagship:
          return PlanetClass.Fertile;
        case ShipClass.Carrier:
        case ShipClass.Annihilator:
          return PlanetClass.Gaia_V;
        case ShipClass.Flayer:
          return PlanetClass.Gaia_I;
        case ShipClass.Executioner:
          return PlanetClass.Gaia_II;
        case ShipClass.Devastator:
          return PlanetClass.Gaia_III;
        case ShipClass.Despoiler:
          return PlanetClass.Gaia_IV;
        default:
          return PlanetClass.None;
      }
    }

    public static bool CanUseEquipment(this ShipClass ship, ShipClass test)
    {
      if (ship == test)
        return true;
      switch (ship)
      {
        case ShipClass.Flagship:
          return test == ShipClass.WyrdInvader;
        case ShipClass.Flayer:
          return test == ShipClass.WyrdAssassin;
        case ShipClass.Executioner:
          return test == ShipClass.WyrdReaper;
        case ShipClass.Annihilator:
          return test == ShipClass.WyrdTerminus;
        case ShipClass.WyrdInvader:
          return test == ShipClass.Flagship;
        case ShipClass.WyrdAssassin:
          return test == ShipClass.Flayer;
        case ShipClass.WyrdReaper:
          return test == ShipClass.Executioner;
        case ShipClass.WyrdTerminus:
          return test == ShipClass.Annihilator;
        default:
          return false;
      }
    }

    public static ShipClass GetWyrdEquiv(this ShipClass ship)
    {
      switch (ship)
      {
        case ShipClass.Flagship:
          return ShipClass.WyrdInvader;
        case ShipClass.Flayer:
          return ShipClass.WyrdAssassin;
        case ShipClass.Executioner:
          return ShipClass.WyrdReaper;
        case ShipClass.Annihilator:
          return ShipClass.WyrdTerminus;
        default:
          return ShipClass.None;
      }
    }
  }
}
