﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Entities.DefenseEntity
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Entities
{
  public abstract class DefenseEntity : PlayerCombatEntity
  {
    private int _level;

    public override string SubTitle
    {
      get
      {
        StringBuilder sb = new StringBuilder(50);
        sb.Append("<");
        if (this.Corporation == null)
        {
          if (this.Alliance != null)
            this.Alliance.AppendName(sb, false);
          else if (this.Player == null)
            sb.Append("?Unknown");
          else
            this.Player.AppendName(sb, false);
        }
        else
          this.Corporation.AppendName(sb, false);
        sb.Append(">");
        return sb.ToString();
      }
    }

    public int Level
    {
      get
      {
        return this._level;
      }
      set
      {
        if (this._level == value)
          return;
        this._level = value;
        this.OnChanged();
      }
    }

    protected DefenseEntity(IGameState state, int id)
      : base(state, id)
    {
    }
  }
}
