﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Technology
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public class Technology
  {
    public bool this[TechnologyType type]
    {
      get
      {
        switch (type)
        {
          case TechnologyType.AdvancedDefense:
            return this.AdvancedDefense;
          case TechnologyType.Scanners:
            return this.Scanners;
          case TechnologyType.DefenseTactics:
            return this.DefenseTactics;
          case TechnologyType.Marauder:
            return this.Marauder;
          case TechnologyType.Defender:
            return this.Defender;
          case TechnologyType.Scavenger:
            return this.Scavenger;
          case TechnologyType.Analysis:
            return this.Analysis;
          case TechnologyType.Critical:
            return this.Critical;
          case TechnologyType.Splash:
            return this.Splash;
          case TechnologyType.Evasion:
            return this.Evasion;
          case TechnologyType.ToHit:
            return this.ToHit;
          case TechnologyType.AdvancedWeapons:
            return this.AdvancedWeapons;
          case TechnologyType.AdvancedHulls:
            return this.AdvancedHulls;
          case TechnologyType.AdvancedStorage:
            return this.AdvancedStorage;
          case TechnologyType.AdvancedEngines:
            return this.AdvancedEngines;
          case TechnologyType.AdvancedDrones:
            return this.AdvancedDrones;
          case TechnologyType.Nanorobotics:
            return this.Nanorobotics;
          default:
            return false;
        }
      }
      set
      {
        switch (type)
        {
          case TechnologyType.AdvancedDefense:
            this.AdvancedDefense = value;
            break;
          case TechnologyType.Scanners:
            this.Scanners = value;
            break;
          case TechnologyType.DefenseTactics:
            this.DefenseTactics = value;
            break;
          case TechnologyType.Marauder:
            this.Marauder = value;
            break;
          case TechnologyType.Defender:
            this.Defender = value;
            break;
          case TechnologyType.Scavenger:
            this.Scavenger = value;
            break;
          case TechnologyType.Analysis:
            this.Analysis = value;
            break;
          case TechnologyType.Critical:
            this.Critical = value;
            break;
          case TechnologyType.Splash:
            this.Splash = value;
            break;
          case TechnologyType.Evasion:
            this.Evasion = value;
            break;
          case TechnologyType.ToHit:
            this.ToHit = value;
            break;
          case TechnologyType.AdvancedWeapons:
            this.AdvancedWeapons = value;
            break;
          case TechnologyType.AdvancedHulls:
            this.AdvancedHulls = value;
            break;
          case TechnologyType.AdvancedStorage:
            this.AdvancedStorage = value;
            break;
          case TechnologyType.AdvancedEngines:
            this.AdvancedEngines = value;
            break;
          case TechnologyType.AdvancedDrones:
            this.AdvancedDrones = value;
            break;
          case TechnologyType.Nanorobotics:
            this.Nanorobotics = value;
            break;
        }
      }
    }

    public int PointsUsed
    {
      get
      {
        int num = 0;
        if (this.AdvancedDefense)
          ++num;
        if (this.Scanners)
          ++num;
        if (this.DefenseTactics)
          ++num;
        if (this.Marauder)
          ++num;
        if (this.Defender)
          ++num;
        if (this.Scavenger)
          ++num;
        if (this.Analysis)
          ++num;
        if (this.Critical)
          ++num;
        if (this.Splash)
          ++num;
        if (this.Evasion)
          ++num;
        if (this.ToHit)
          ++num;
        if (this.AdvancedWeapons)
          ++num;
        if (this.AdvancedHulls)
          ++num;
        if (this.AdvancedStorage)
          ++num;
        if (this.AdvancedEngines)
          ++num;
        if (this.AdvancedDrones)
          ++num;
        if (this.Nanorobotics)
          ++num;
        return num;
      }
    }

    public bool AdvancedDefense { get; set; }

    public bool Scanners { get; set; }

    public bool DefenseTactics { get; set; }

    public bool Marauder { get; set; }

    public bool Defender { get; set; }

    public bool Scavenger { get; set; }

    public bool Analysis { get; set; }

    public bool Critical { get; set; }

    public bool Splash { get; set; }

    public bool Evasion { get; set; }

    public bool ToHit { get; set; }

    public bool AdvancedWeapons { get; set; }

    public bool AdvancedHulls { get; set; }

    public bool AdvancedStorage { get; set; }

    public bool AdvancedEngines { get; set; }

    public bool AdvancedDrones { get; set; }

    public bool Nanorobotics { get; set; }

    public Technology(IByteBuffer input)
    {
      this.AdvancedDefense = input.GetBool();
      this.Scanners = input.GetBool();
      this.DefenseTactics = input.GetBool();
      this.Marauder = input.GetBool();
      this.Defender = input.GetBool();
      this.Scavenger = input.GetBool();
      this.Analysis = input.GetBool();
      this.Critical = input.GetBool();
      this.Splash = input.GetBool();
      this.Evasion = input.GetBool();
      this.ToHit = input.GetBool();
      this.AdvancedWeapons = input.GetBool();
      this.AdvancedHulls = input.GetBool();
      this.AdvancedStorage = input.GetBool();
      this.AdvancedEngines = input.GetBool();
      this.AdvancedDrones = input.GetBool();
      this.Nanorobotics = input.GetBool();
    }

    public Technology()
    {
    }

    public void Write(IByteBuffer output)
    {
      output.Write(this.AdvancedDefense);
      output.Write(this.Scanners);
      output.Write(this.DefenseTactics);
      output.Write(this.Marauder);
      output.Write(this.Defender);
      output.Write(this.Scavenger);
      output.Write(this.Analysis);
      output.Write(this.Critical);
      output.Write(this.Splash);
      output.Write(this.Evasion);
      output.Write(this.ToHit);
      output.Write(this.AdvancedWeapons);
      output.Write(this.AdvancedHulls);
      output.Write(this.AdvancedStorage);
      output.Write(this.AdvancedEngines);
      output.Write(this.AdvancedDrones);
      output.Write(this.Nanorobotics);
    }

    public void Clear()
    {
      this.AdvancedDefense = false;
      this.Scanners = false;
      this.DefenseTactics = false;
      this.Marauder = false;
      this.Defender = false;
      this.Scavenger = false;
      this.Analysis = false;
      this.Critical = false;
      this.Splash = false;
      this.Evasion = false;
      this.ToHit = false;
      this.AdvancedWeapons = false;
      this.AdvancedHulls = false;
      this.AdvancedStorage = false;
      this.AdvancedEngines = false;
      this.AdvancedDrones = false;
      this.Nanorobotics = false;
    }

    public void Set(Technology tech)
    {
      this.AdvancedDefense = tech.AdvancedDefense;
      this.Scanners = tech.Scanners;
      this.DefenseTactics = tech.DefenseTactics;
      this.Marauder = tech.Marauder;
      this.Defender = tech.Defender;
      this.Scavenger = tech.Scavenger;
      this.Analysis = tech.Analysis;
      this.Critical = tech.Critical;
      this.Splash = tech.Splash;
      this.Evasion = tech.Evasion;
      this.ToHit = tech.ToHit;
      this.AdvancedWeapons = tech.AdvancedWeapons;
      this.AdvancedHulls = tech.AdvancedHulls;
      this.AdvancedStorage = tech.AdvancedStorage;
      this.AdvancedEngines = tech.AdvancedEngines;
      this.AdvancedDrones = tech.AdvancedDrones;
      this.Nanorobotics = tech.Nanorobotics;
    }
  }
}
