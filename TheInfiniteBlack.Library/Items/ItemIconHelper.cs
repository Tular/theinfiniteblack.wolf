﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ItemIconHelper
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public static class ItemIconHelper
  {
    public static ItemIcon GetIcon(EquipmentItem item)
    {
      switch (item.Type)
      {
        case ItemType.WEAPON:
          return ItemIconHelper.GetIcon((WeaponItem) item);
        case ItemType.ARMOR:
          return ItemIconHelper.GetIcon((ArmorItem) item);
        case ItemType.STORAGE:
          return ItemIconHelper.GetIcon((StorageItem) item);
        case ItemType.HARVESTER:
          return ItemIconHelper.GetIcon((HarvesterItem) item);
        case ItemType.ENGINE:
          return ItemIconHelper.GetIcon((EngineItem) item);
        case ItemType.COMPUTER:
          return ItemIconHelper.GetIcon((ComputerItem) item);
        case ItemType.SPECIAL:
          return ItemIconHelper.GetIcon((SpecialItem) item);
        default:
          return ItemIcon.NULL;
      }
    }

    public static ItemIcon GetIcon(WeaponItem item)
    {
      switch (item.Class)
      {
        case WeaponClass.AUTO_CANNON:
        case WeaponClass.BURST_CANNON:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_09_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_09_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_09_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_09_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_09_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_09_7;
            default:
              return ItemIcon.item_06_09_1;
          }
        case WeaponClass.MASS_DRIVER:
        case WeaponClass.RAIL_GUN:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_05_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_05_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_05_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_05_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_05_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_05_7;
            default:
              return ItemIcon.item_06_05_1;
          }
        case WeaponClass.LEVIATHAN:
        case WeaponClass.PLASMA_LANCE:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_14_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_14_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_14_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_14_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_14_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_14_7;
            default:
              return ItemIcon.item_06_14_1;
          }
        case WeaponClass.PULVERIZER:
        case WeaponClass.MATTER_INVERTER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_16_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_16_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_16_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_16_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_16_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_16_7;
            default:
              return ItemIcon.item_06_16_1;
          }
        case WeaponClass.RIPPER:
        case WeaponClass.HARPY:
        case WeaponClass.WYVERN:
        case WeaponClass.PENATRATOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_22_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_22_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_22_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_22_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_22_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_22_07;
            default:
              return ItemIcon.item_06_22_01;
          }
        case WeaponClass.SCREAMER:
        case WeaponClass.BANSHEE:
        case WeaponClass.BASILISK:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_20_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_20_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_20_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_20_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_20_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_20_07;
            default:
              return ItemIcon.item_06_20_01;
          }
        case WeaponClass.HELLCANNON:
        case WeaponClass.SERPENT:
        case WeaponClass.HYDRA:
        case WeaponClass.FIRECAT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_18_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_18_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_18_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_18_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_18_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_18_07;
            default:
              return ItemIcon.item_06_18_01;
          }
        case WeaponClass.PROTON_LAUNCHER:
        case WeaponClass.DEMOLISHER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_06_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_06_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_06_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_06_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_06_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_06_7;
            default:
              return ItemIcon.item_06_06_1;
          }
        case WeaponClass.FUSION_BEAM:
        case WeaponClass.DISRUPTOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_08_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_08_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_08_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_08_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_08_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_08_7;
            default:
              return ItemIcon.item_06_08_1;
          }
        case WeaponClass.PHASER:
        case WeaponClass.MESON_BLASTER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_03_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_03_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_03_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_03_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_03_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_03_7;
            default:
              return ItemIcon.item_06_03_1;
          }
        case WeaponClass.GAUSS_CANNON:
        case WeaponClass.OMEGA_RIFLE:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_04_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_04_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_04_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_04_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_04_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_04_7;
            default:
              return ItemIcon.item_06_04_1;
          }
        case WeaponClass.ACCELERATOR:
        case WeaponClass.ION_CANNON:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_11_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_11_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_11_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_11_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_11_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_11_7;
            default:
              return ItemIcon.item_06_11_1;
          }
        case WeaponClass.GRAVITY_SMASHER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_02_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_02_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_02_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_02_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_02_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_02_7;
            default:
              return ItemIcon.item_06_02_1;
          }
        case WeaponClass.RAPTURE:
        case WeaponClass.GLORY:
        case WeaponClass.HORROR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_28_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_28_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_28_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_28_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_28_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_28_07;
            default:
              return ItemIcon.item_06_28_01;
          }
        case WeaponClass.OBLIVION:
        case WeaponClass.SMOLDER:
        case WeaponClass.RADIANCE:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_29_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_29_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_29_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_29_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_29_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_29_07;
            default:
              return ItemIcon.item_06_29_01;
          }
        case WeaponClass.RUIN:
        case WeaponClass.CATACLYSM:
        case WeaponClass.TORMENT:
        case WeaponClass.DESTRUCTION:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_25_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_25_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_25_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_25_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_25_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_25_07;
            default:
              return ItemIcon.item_06_25_01;
          }
        case WeaponClass.FRENZY:
        case WeaponClass.SILENCE:
        case WeaponClass.EXODUS:
        case WeaponClass.DARKNESS:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_27_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_27_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_27_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_27_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_27_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_27_07;
            default:
              return ItemIcon.item_06_27_01;
          }
        case WeaponClass.AGONY:
        case WeaponClass.PROPHECY:
        case WeaponClass.ANIMUS:
        case WeaponClass.PAIN:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_26_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_26_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_26_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_26_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_26_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_26_07;
            default:
              return ItemIcon.item_06_26_01;
          }
        case WeaponClass.SUCCUBUS:
        case WeaponClass.VIPER:
        case WeaponClass.GARGOYLE:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_17_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_17_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_17_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_17_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_17_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_17_07;
            default:
              return ItemIcon.item_06_17_01;
          }
        case WeaponClass.OPHIDIAN:
        case WeaponClass.BEHEMOTH:
        case WeaponClass.KRAKEN:
        case WeaponClass.DRAGON:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_21_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_21_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_21_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_21_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_21_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_21_07;
            default:
              return ItemIcon.item_06_21_01;
          }
        case WeaponClass.MUTILATOR:
        case WeaponClass.EXTERMINATOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_13_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_13_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_13_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_13_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_13_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_13_7;
            default:
              return ItemIcon.item_06_13_1;
          }
        case WeaponClass.STARSHATTER:
        case WeaponClass.VOIDBLASTER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_23_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_23_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_23_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_23_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_23_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_23_07;
            default:
              return ItemIcon.item_06_23_01;
          }
        case WeaponClass.STRIKER:
        case WeaponClass.DESOLATOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_10_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_10_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_10_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_10_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_10_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_10_7;
            default:
              return ItemIcon.item_06_10_1;
          }
        case WeaponClass.RAVAGER:
        case WeaponClass.BRUTALIZER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_15_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_15_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_15_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_15_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_15_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_15_7;
            default:
              return ItemIcon.item_06_15_1;
          }
        case WeaponClass.VAPORIZER:
        case WeaponClass.ATOMIZER:
        case WeaponClass.INCINERATOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_01_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_01_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_01_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_01_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_01_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_01_7;
            default:
              return ItemIcon.item_06_01_1;
          }
        case WeaponClass.CORRUPTOR:
        case WeaponClass.SOULTAKER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_19_02;
            case ItemRarity.RARE:
              return ItemIcon.item_06_19_03;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_19_04;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_19_05;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_19_06;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_19_07;
            default:
              return ItemIcon.item_06_19_01;
          }
        case WeaponClass.MINDSLAYER:
        case WeaponClass.RIFTBREAKER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_07_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_07_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_07_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_07_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_07_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_07_7;
            default:
              return ItemIcon.item_06_07_1;
          }
        case WeaponClass.NULLCANNON:
        case WeaponClass.ERADICATOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_06_12_2;
            case ItemRarity.RARE:
              return ItemIcon.item_06_12_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_06_12_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_06_12_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_06_12_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_06_12_7;
            default:
              return ItemIcon.item_06_12_1;
          }
        default:
          return ItemIcon.NULL;
      }
    }

    public static ItemIcon GetIcon(ArmorItem item)
    {
      switch (item.Class)
      {
        case ArmorClass.DIAMOND_ARMOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_06_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_06_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_06_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_06_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_06_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_06_7;
            default:
              return ItemIcon.item_01_06_1;
          }
        case ArmorClass.THORIUM_ARMOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_07_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_07_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_07_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_07_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_07_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_07_7;
            default:
              return ItemIcon.item_01_07_1;
          }
        case ArmorClass.OSMIUM_ARMOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_04_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_04_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_04_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_04_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_04_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_04_7;
            default:
              return ItemIcon.item_01_04_1;
          }
        case ArmorClass.CITADEL_GAMBIT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_10_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_10_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_10_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_10_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_10_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_10_7;
            default:
              return ItemIcon.item_01_10_1;
          }
        case ArmorClass.AJAX_GAMBIT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_12_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_12_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_12_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_12_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_12_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_12_7;
            default:
              return ItemIcon.item_01_12_1;
          }
        case ArmorClass.AEGIS_GAMBIT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_11_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_11_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_11_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_11_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_11_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_11_7;
            default:
              return ItemIcon.item_01_11_1;
          }
        case ArmorClass.KISMET_GAMBIT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_09_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_09_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_09_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_09_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_09_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_09_7;
            default:
              return ItemIcon.item_01_09_1;
          }
        case ArmorClass.TITANIUM_HULL:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_01_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_01_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_01_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_01_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_01_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_01_7;
            default:
              return ItemIcon.item_01_01_1;
          }
        case ArmorClass.COMPOSITE_HULL:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_02_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_02_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_02_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_02_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_02_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_02_7;
            default:
              return ItemIcon.item_01_02_1;
          }
        case ArmorClass.CARBIDE_HULL:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_08_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_08_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_08_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_08_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_08_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_08_7;
            default:
              return ItemIcon.item_01_08_1;
          }
        case ArmorClass.EXOPRENE_SHIELD:
        case ArmorClass.CYTOPLAST_SHIELD:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_05_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_05_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_05_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_05_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_05_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_05_7;
            default:
              return ItemIcon.item_01_05_1;
          }
        case ArmorClass.HOLOCRINE_SHIELD:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_01_03_2;
            case ItemRarity.RARE:
              return ItemIcon.item_01_03_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_01_03_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_01_03_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_01_03_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_01_03_7;
            default:
              return ItemIcon.item_01_03_1;
          }
        default:
          return ItemIcon.NULL;
      }
    }

    public static ItemIcon GetIcon(StorageItem item)
    {
      switch (item.Class)
      {
        case StorageClass.BasicStorage:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.icon_item_storage_2;
            case ItemRarity.RARE:
              return ItemIcon.icon_item_storage_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.icon_item_storage_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.icon_item_storage_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.icon_item_storage_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.icon_item_storage_7;
            default:
              return ItemIcon.icon_item_storage_1;
          }
        case StorageClass.HoldH1A:
        case StorageClass.HoldH1B:
        case StorageClass.HoldH1G:
        case StorageClass.HoldH102:
        case StorageClass.HoldH103:
        case StorageClass.HoldH104:
        case StorageClass.HoldH105:
        case StorageClass.HoldH106:
        case StorageClass.HoldH107:
        case StorageClass.HoldH108:
        case StorageClass.HoldH109:
        case StorageClass.HoldH110:
        case StorageClass.HoldH111:
        case StorageClass.HoldH112:
        case StorageClass.HoldH113:
        case StorageClass.HoldH114:
        case StorageClass.HoldH115:
        case StorageClass.HoldH116:
        case StorageClass.HoldH117:
        case StorageClass.HoldH118:
        case StorageClass.HoldH119:
        case StorageClass.HoldH120:
        case StorageClass.HoldH121:
        case StorageClass.HoldH122:
        case StorageClass.HoldH123:
        case StorageClass.HoldH124:
        case StorageClass.HoldH125:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_01_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_01_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_01_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_01_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_01_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_01_7;
            default:
              return ItemIcon.item_05_01_1;
          }
        case StorageClass.HoldH2A:
        case StorageClass.HoldH2B:
        case StorageClass.HoldH2G:
        case StorageClass.HoldH202:
        case StorageClass.HoldH203:
        case StorageClass.HoldH204:
        case StorageClass.HoldH205:
        case StorageClass.HoldH206:
        case StorageClass.HoldH207:
        case StorageClass.HoldH208:
        case StorageClass.HoldH209:
        case StorageClass.HoldH210:
        case StorageClass.HoldH211:
        case StorageClass.HoldH212:
        case StorageClass.HoldH213:
        case StorageClass.HoldH214:
        case StorageClass.HoldH215:
        case StorageClass.HoldH216:
        case StorageClass.HoldH217:
        case StorageClass.HoldH218:
        case StorageClass.HoldH219:
        case StorageClass.HoldH220:
        case StorageClass.HoldH221:
        case StorageClass.HoldH222:
        case StorageClass.HoldH223:
        case StorageClass.HoldH224:
        case StorageClass.HoldH225:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_02_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_02_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_02_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_02_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_02_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_02_7;
            default:
              return ItemIcon.item_05_02_1;
          }
        case StorageClass.HoldP1A:
        case StorageClass.HoldP1B:
        case StorageClass.HoldP1G:
        case StorageClass.HoldP102:
        case StorageClass.HoldP103:
        case StorageClass.HoldP104:
        case StorageClass.HoldP105:
        case StorageClass.HoldP106:
        case StorageClass.HoldP107:
        case StorageClass.HoldP108:
        case StorageClass.HoldP109:
        case StorageClass.HoldP110:
        case StorageClass.HoldP111:
        case StorageClass.HoldP112:
        case StorageClass.HoldP113:
        case StorageClass.HoldP114:
        case StorageClass.HoldP115:
        case StorageClass.HoldP116:
        case StorageClass.HoldP117:
        case StorageClass.HoldP118:
        case StorageClass.HoldP119:
        case StorageClass.HoldP120:
        case StorageClass.HoldP121:
        case StorageClass.HoldP122:
        case StorageClass.HoldP123:
        case StorageClass.HoldP124:
        case StorageClass.HoldP125:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_03_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_03_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_03_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_03_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_03_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_03_7;
            default:
              return ItemIcon.item_05_03_1;
          }
        case StorageClass.HoldP2A:
        case StorageClass.HoldP2B:
        case StorageClass.HoldP2G:
        case StorageClass.HoldP202:
        case StorageClass.HoldP203:
        case StorageClass.HoldP204:
        case StorageClass.HoldP205:
        case StorageClass.HoldP206:
        case StorageClass.HoldP207:
        case StorageClass.HoldP208:
        case StorageClass.HoldP209:
        case StorageClass.HoldP210:
        case StorageClass.HoldP211:
        case StorageClass.HoldP212:
        case StorageClass.HoldP213:
        case StorageClass.HoldP214:
        case StorageClass.HoldP215:
        case StorageClass.HoldP216:
        case StorageClass.HoldP217:
        case StorageClass.HoldP218:
        case StorageClass.HoldP219:
        case StorageClass.HoldP220:
        case StorageClass.HoldP221:
        case StorageClass.HoldP222:
        case StorageClass.HoldP223:
        case StorageClass.HoldP224:
        case StorageClass.HoldP225:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_04_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_04_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_04_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_04_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_04_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_04_7;
            default:
              return ItemIcon.item_05_04_1;
          }
        case StorageClass.HoldW1A:
        case StorageClass.HoldW1B:
        case StorageClass.HoldW1G:
        case StorageClass.HoldW102:
        case StorageClass.HoldW103:
        case StorageClass.HoldW104:
        case StorageClass.HoldW105:
        case StorageClass.HoldW106:
        case StorageClass.HoldW107:
        case StorageClass.HoldW108:
        case StorageClass.HoldW109:
        case StorageClass.HoldW110:
        case StorageClass.HoldW111:
        case StorageClass.HoldW112:
        case StorageClass.HoldW113:
        case StorageClass.HoldW114:
        case StorageClass.HoldW115:
        case StorageClass.HoldW116:
        case StorageClass.HoldW117:
        case StorageClass.HoldW118:
        case StorageClass.HoldW119:
        case StorageClass.HoldW120:
        case StorageClass.HoldW121:
        case StorageClass.HoldW122:
        case StorageClass.HoldW123:
        case StorageClass.HoldW124:
        case StorageClass.HoldW125:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_05_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_05_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_05_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_05_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_05_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_05_7;
            default:
              return ItemIcon.item_05_05_1;
          }
        case StorageClass.HoldW2A:
        case StorageClass.HoldW2B:
        case StorageClass.HoldW2G:
        case StorageClass.HoldW202:
        case StorageClass.HoldW203:
        case StorageClass.HoldW204:
        case StorageClass.HoldW205:
        case StorageClass.HoldW206:
        case StorageClass.HoldW207:
        case StorageClass.HoldW208:
        case StorageClass.HoldW209:
        case StorageClass.HoldW210:
        case StorageClass.HoldW211:
        case StorageClass.HoldW212:
        case StorageClass.HoldW213:
        case StorageClass.HoldW214:
        case StorageClass.HoldW215:
        case StorageClass.HoldW216:
        case StorageClass.HoldW217:
        case StorageClass.HoldW218:
        case StorageClass.HoldW219:
        case StorageClass.HoldW220:
        case StorageClass.HoldW221:
        case StorageClass.HoldW222:
        case StorageClass.HoldW223:
        case StorageClass.HoldW224:
        case StorageClass.HoldW225:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_06_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_06_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_06_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_06_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_06_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_06_7;
            default:
              return ItemIcon.item_05_06_1;
          }
        case StorageClass.HoldR1A:
        case StorageClass.HoldR1B:
        case StorageClass.HoldR1G:
        case StorageClass.HoldR102:
        case StorageClass.HoldR103:
        case StorageClass.HoldR104:
        case StorageClass.HoldR105:
        case StorageClass.HoldR106:
        case StorageClass.HoldR107:
        case StorageClass.HoldR108:
        case StorageClass.HoldR109:
        case StorageClass.HoldR110:
        case StorageClass.HoldR111:
        case StorageClass.HoldR112:
        case StorageClass.HoldR113:
        case StorageClass.HoldR114:
        case StorageClass.HoldR115:
        case StorageClass.HoldR116:
        case StorageClass.HoldR117:
        case StorageClass.HoldR118:
        case StorageClass.HoldR119:
        case StorageClass.HoldR120:
        case StorageClass.HoldR121:
        case StorageClass.HoldR122:
        case StorageClass.HoldR123:
        case StorageClass.HoldR124:
        case StorageClass.HoldR125:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_07_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_07_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_07_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_07_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_07_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_07_7;
            default:
              return ItemIcon.item_05_07_1;
          }
        case StorageClass.HoldR2A:
        case StorageClass.HoldR2B:
        case StorageClass.HoldR2G:
        case StorageClass.HoldR202:
        case StorageClass.HoldR203:
        case StorageClass.HoldR204:
        case StorageClass.HoldR205:
        case StorageClass.HoldR206:
        case StorageClass.HoldR207:
        case StorageClass.HoldR208:
        case StorageClass.HoldR209:
        case StorageClass.HoldR210:
        case StorageClass.HoldR211:
        case StorageClass.HoldR212:
        case StorageClass.HoldR213:
        case StorageClass.HoldR214:
        case StorageClass.HoldR215:
        case StorageClass.HoldR216:
        case StorageClass.HoldR217:
        case StorageClass.HoldR218:
        case StorageClass.HoldR219:
        case StorageClass.HoldR220:
        case StorageClass.HoldR221:
        case StorageClass.HoldR222:
        case StorageClass.HoldR223:
        case StorageClass.HoldR224:
        case StorageClass.HoldR225:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_08_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_08_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_08_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_08_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_08_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_08_7;
            default:
              return ItemIcon.item_05_08_1;
          }
        case StorageClass.BigCrate:
        case StorageClass.ReallyBigCrate:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_05_00_2;
            case ItemRarity.RARE:
              return ItemIcon.item_05_00_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_05_00_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_05_00_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_05_00_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_05_00_7;
            default:
              return ItemIcon.item_05_00_1;
          }
        default:
          return ItemIcon.NULL;
      }
    }

    public static ItemIcon GetIcon(HarvesterItem item)
    {
      switch (item.Class)
      {
        case HarvesterClass.LCD_V1:
        case HarvesterClass.LCD_V2:
        case HarvesterClass.LCD_V3:
        case HarvesterClass.LCD_V4:
        case HarvesterClass.LCD_V5:
        case HarvesterClass.LCD_V6:
        case HarvesterClass.LCD_V7:
        case HarvesterClass.LCD_V8:
        case HarvesterClass.LCD_V9:
        case HarvesterClass.LCD_V10:
        case HarvesterClass.LCD_V11:
        case HarvesterClass.LCD_V12:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_04_02_2;
            case ItemRarity.RARE:
              return ItemIcon.item_04_02_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_04_02_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_04_02_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_04_02_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_04_02_7;
            default:
              return ItemIcon.item_04_02_1;
          }
        case HarvesterClass.HX_V1:
        case HarvesterClass.HX_V2:
        case HarvesterClass.HX_V3:
        case HarvesterClass.HX_V4:
        case HarvesterClass.HX_V5:
        case HarvesterClass.HX_V6:
        case HarvesterClass.HX_V7:
        case HarvesterClass.HX_V8:
        case HarvesterClass.HX_V9:
        case HarvesterClass.HX_V10:
        case HarvesterClass.HX_V11:
        case HarvesterClass.HX_V12:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_04_01_2;
            case ItemRarity.RARE:
              return ItemIcon.item_04_01_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_04_01_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_04_01_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_04_01_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_04_01_7;
            default:
              return ItemIcon.item_04_01_1;
          }
        case HarvesterClass.WP_V1:
        case HarvesterClass.WP_V2:
        case HarvesterClass.WP_V3:
        case HarvesterClass.WP_V4:
        case HarvesterClass.WP_V5:
        case HarvesterClass.WP_V6:
        case HarvesterClass.WP_V7:
        case HarvesterClass.WP_V8:
        case HarvesterClass.WP_V9:
        case HarvesterClass.WP_V10:
        case HarvesterClass.WP_V11:
        case HarvesterClass.WP_V12:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_04_06_2;
            case ItemRarity.RARE:
              return ItemIcon.item_04_06_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_04_06_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_04_06_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_04_06_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_04_06_7;
            default:
              return ItemIcon.item_04_06_1;
          }
        case HarvesterClass.DCD_V1:
        case HarvesterClass.DCD_V2:
        case HarvesterClass.DCD_V3:
        case HarvesterClass.DCD_V4:
        case HarvesterClass.DCD_V5:
        case HarvesterClass.DCD_V6:
        case HarvesterClass.DCD_V7:
        case HarvesterClass.DCD_V8:
        case HarvesterClass.DCD_V9:
        case HarvesterClass.DCD_V10:
        case HarvesterClass.DCD_V11:
        case HarvesterClass.DCD_V12:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_04_08_2;
            case ItemRarity.RARE:
              return ItemIcon.item_04_08_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_04_08_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_04_08_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_04_08_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_04_08_7;
            default:
              return ItemIcon.item_04_08_1;
          }
        default:
          return ItemIcon.NULL;
      }
    }

    public static ItemIcon GetIcon(EngineItem item)
    {
      switch (item.Class)
      {
        case EngineClass.GRAVITY:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_02_06_2;
            case ItemRarity.RARE:
              return ItemIcon.item_02_06_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_02_06_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_02_06_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_02_06_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_02_06_7;
            default:
              return ItemIcon.item_02_06_1;
          }
        case EngineClass.SKIP:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_02_04_2;
            case ItemRarity.RARE:
              return ItemIcon.item_02_04_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_02_04_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_02_04_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_02_04_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_02_04_7;
            default:
              return ItemIcon.item_02_04_1;
          }
        case EngineClass.FUSION:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_02_03_2;
            case ItemRarity.RARE:
              return ItemIcon.item_02_03_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_02_03_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_02_03_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_02_03_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_02_03_7;
            default:
              return ItemIcon.item_02_03_1;
          }
        case EngineClass.NEUTRON:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_02_02_2;
            case ItemRarity.RARE:
              return ItemIcon.item_02_02_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_02_02_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_02_02_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_02_02_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_02_02_7;
            default:
              return ItemIcon.item_02_02_1;
          }
        case EngineClass.STEALTH:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_02_05_2;
            case ItemRarity.RARE:
              return ItemIcon.item_02_05_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_02_05_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_02_05_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_02_05_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_02_05_7;
            default:
              return ItemIcon.item_02_05_1;
          }
        default:
          return ItemIcon.NULL;
      }
    }

    public static ItemIcon GetIcon(ComputerItem item)
    {
      switch (item.Class)
      {
        case ComputerClass.SAGE_MK_1:
        case ComputerClass.SAGE_MK_2:
        case ComputerClass.SAGE_MK_3:
        case ComputerClass.SAGE_MK_4:
        case ComputerClass.A1_SAGE:
        case ComputerClass.A2_SAGE:
        case ComputerClass.A3_SAGE:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_03_01_2;
            case ItemRarity.RARE:
              return ItemIcon.item_03_01_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_03_01_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_03_01_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_03_01_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_03_01_7;
            default:
              return ItemIcon.item_03_01_1;
          }
        case ComputerClass.TOIL_MK_1:
        case ComputerClass.TOIL_MK_2:
        case ComputerClass.TOIL_MK_3:
        case ComputerClass.TOIL_MK_4:
        case ComputerClass.A1_TOIL:
        case ComputerClass.A2_TOIL:
        case ComputerClass.A3_TOIL:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_03_02_2;
            case ItemRarity.RARE:
              return ItemIcon.item_03_02_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_03_02_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_03_02_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_03_02_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_03_02_7;
            default:
              return ItemIcon.item_03_02_1;
          }
        case ComputerClass.HAVOK_MK_1:
        case ComputerClass.HAVOK_MK_2:
        case ComputerClass.HAVOK_MK_3:
        case ComputerClass.HAVOK_MK_4:
        case ComputerClass.A1_HAVOK:
        case ComputerClass.A2_HAVOK:
        case ComputerClass.A3_HAVOK:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_03_03_2;
            case ItemRarity.RARE:
              return ItemIcon.item_03_03_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_03_03_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_03_03_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_03_03_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_03_03_7;
            default:
              return ItemIcon.item_03_03_1;
          }
        case ComputerClass.CABAL_MK_1:
        case ComputerClass.CABAL_MK_2:
        case ComputerClass.CABAL_MK_3:
        case ComputerClass.CABAL_MK_4:
        case ComputerClass.A1_CABAL:
        case ComputerClass.A2_CABAL:
        case ComputerClass.A3_CABAL:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_03_04_2;
            case ItemRarity.RARE:
              return ItemIcon.item_03_04_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_03_04_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_03_04_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_03_04_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_03_04_7;
            default:
              return ItemIcon.item_03_04_1;
          }
        case ComputerClass.AGENT_MK_1:
        case ComputerClass.AGENT_MK_2:
        case ComputerClass.AGENT_MK_3:
        case ComputerClass.AGENT_MK_4:
        case ComputerClass.A1_AGENT:
        case ComputerClass.A2_AGENT:
        case ComputerClass.A3_AGENT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_03_05_2;
            case ItemRarity.RARE:
              return ItemIcon.item_03_05_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_03_05_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_03_05_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_03_05_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_03_05_7;
            default:
              return ItemIcon.item_03_05_1;
          }
        case ComputerClass.DRONE_MK_1:
        case ComputerClass.DRONE_MK_2:
        case ComputerClass.DRONE_MK_3:
        case ComputerClass.DRONE_MK_4:
        case ComputerClass.A1_DRONE:
        case ComputerClass.A2_DRONE:
        case ComputerClass.A3_DRONE:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_03_06_2;
            case ItemRarity.RARE:
              return ItemIcon.item_03_06_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_03_06_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_03_06_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_03_06_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_03_06_7;
            default:
              return ItemIcon.item_03_06_1;
          }
        case ComputerClass.WARRIOR_MK_1:
        case ComputerClass.WARRIOR_MK_2:
        case ComputerClass.WARRIOR_MK_3:
        case ComputerClass.WARRIOR_MK_4:
        case ComputerClass.A1_WARRIOR:
        case ComputerClass.A2_WARRIOR:
        case ComputerClass.A3_WARRIOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_03_08_2;
            case ItemRarity.RARE:
              return ItemIcon.item_03_08_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_03_08_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_03_08_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_03_08_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_03_08_7;
            default:
              return ItemIcon.item_03_08_1;
          }
        default:
          return ItemIcon.NULL;
      }
    }

    public static ItemIcon GetIcon(SpecialItem item)
    {
      switch (item.Class)
      {
        case SpecialClass.TECHNICIAN:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_00_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_00_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_00_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_00_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_00_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_00_7;
            default:
              return ItemIcon.item_07_00_1;
          }
        case SpecialClass.PROSPECTOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_01_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_01_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_01_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_01_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_01_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_01_7;
            default:
              return ItemIcon.item_07_01_1;
          }
        case SpecialClass.TANK:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_02_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_02_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_02_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_02_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_02_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_02_7;
            default:
              return ItemIcon.item_07_02_1;
          }
        case SpecialClass.SCOUT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_03_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_03_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_03_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_03_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_03_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_03_7;
            default:
              return ItemIcon.item_07_03_1;
          }
        case SpecialClass.TRACTOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_04_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_04_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_04_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_04_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_04_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_04_7;
            default:
              return ItemIcon.item_07_04_1;
          }
        case SpecialClass.NOVA:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_05_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_05_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_05_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_05_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_05_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_05_7;
            default:
              return ItemIcon.item_07_05_1;
          }
        case SpecialClass.BATTLE_RAM:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_06_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_06_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_06_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_06_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_06_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_06_7;
            default:
              return ItemIcon.item_07_06_1;
          }
        case SpecialClass.ALIEN_HUNTER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_07_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_07_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_07_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_07_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_07_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_07_7;
            default:
              return ItemIcon.item_07_07_1;
          }
        case SpecialClass.BOUNTY_HUNTER:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_08_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_08_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_08_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_08_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_08_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_08_7;
            default:
              return ItemIcon.item_07_08_1;
          }
        case SpecialClass.DEFLECTOR:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_09_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_09_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_09_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_09_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_09_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_09_7;
            default:
              return ItemIcon.item_07_09_1;
          }
        case SpecialClass.WEIRD_ALIEN_ARTIFACT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_10_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_10_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_10_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_10_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_10_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_10_7;
            default:
              return ItemIcon.item_07_10_1;
          }
        case SpecialClass.GRAPPLING_HOOK:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_11_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_11_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_11_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_11_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_11_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_11_7;
            default:
              return ItemIcon.item_07_11_1;
          }
        case SpecialClass.ADVANCED_CONSTRUCT:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_12_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_12_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_12_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_12_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_12_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_12_7;
            default:
              return ItemIcon.item_07_12_1;
          }
        case SpecialClass.ADVANCED_SHIELDS:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_13_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_13_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_13_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_13_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_13_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_13_7;
            default:
              return ItemIcon.item_07_13_1;
          }
        case SpecialClass.ADVANCED_ELECTRONICS:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_14_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_14_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_14_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_14_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_14_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_14_7;
            default:
              return ItemIcon.item_07_14_1;
          }
        case SpecialClass.ADVANCED_MUNITIONS:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_15_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_15_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_15_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_15_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_15_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_15_7;
            default:
              return ItemIcon.item_07_15_1;
          }
        case SpecialClass.ADVANCED_PROPULSION:
          switch (item.Rarity)
          {
            case ItemRarity.UNCOMMON:
              return ItemIcon.item_07_16_2;
            case ItemRarity.RARE:
              return ItemIcon.item_07_16_3;
            case ItemRarity.ULTRA_RARE:
              return ItemIcon.item_07_16_4;
            case ItemRarity.LEGENDARY:
              return ItemIcon.item_07_16_5;
            case ItemRarity.PRECURSOR:
              return ItemIcon.item_07_16_6;
            case ItemRarity.ULTIMATE:
              return ItemIcon.item_07_16_7;
            default:
              return ItemIcon.item_07_16_1;
          }
        default:
          return ItemIcon.NULL;
      }
    }
  }
}
