﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ComputerCache
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Items
{
  public static class ComputerCache
  {
    private static readonly Dictionary<ComputerClass, Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, ComputerItem[]>>>> _cache = new Dictionary<ComputerClass, Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, ComputerItem[]>>>>();

    public static ComputerItem Get(IByteBuffer input)
    {
      ItemRarity rarity = (ItemRarity) input.Get();
      sbyte durability = input.Get();
      bool noDrop = input.GetBool();
      bool bindOnEquip = !noDrop && input.GetBool();
      return ComputerCache.Get((ComputerClass) input.Get(), (ShipClass) input.Get(), rarity, durability, noDrop, bindOnEquip);
    }

    public static ComputerItem Get(ComputerClass type, ShipClass ship, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      lock (ComputerCache._cache)
      {
        Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, ComputerItem[]>>> dictionary1;
        if (!ComputerCache._cache.TryGetValue(type, out dictionary1))
        {
          dictionary1 = new Dictionary<ShipClass, Dictionary<ItemRarity, Dictionary<sbyte, ComputerItem[]>>>();
          ComputerCache._cache[type] = dictionary1;
        }
        Dictionary<ItemRarity, Dictionary<sbyte, ComputerItem[]>> dictionary2;
        if (!dictionary1.TryGetValue(ship, out dictionary2))
        {
          dictionary2 = new Dictionary<ItemRarity, Dictionary<sbyte, ComputerItem[]>>();
          dictionary1[ship] = dictionary2;
        }
        Dictionary<sbyte, ComputerItem[]> dictionary3;
        if (!dictionary2.TryGetValue(rarity, out dictionary3))
        {
          dictionary3 = new Dictionary<sbyte, ComputerItem[]>();
          dictionary2[rarity] = dictionary3;
        }
        ComputerItem[] computerItemArray;
        if (!dictionary3.TryGetValue(durability, out computerItemArray))
        {
          computerItemArray = new ComputerItem[4];
          dictionary3[durability] = computerItemArray;
        }
        int index = noDrop ? (bindOnEquip ? 0 : 1) : (bindOnEquip ? 2 : 3);
        ComputerItem computerItem = computerItemArray[index];
        if (computerItem == null)
        {
          computerItem = new ComputerItem(type, ship, rarity, durability, noDrop, bindOnEquip);
          computerItemArray[index] = computerItem;
        }
        return computerItem;
      }
    }

    public static ComputerItem GetUpgrade(EngineeringType type, ComputerItem item)
    {
      if (!item.CanUpgrade(type))
        return (ComputerItem) null;
      switch (type)
      {
        case EngineeringType.REPAIR:
          return ComputerCache.Get(item.Class, item.RequiredShip, item.Rarity, (int) item.Durability + 10 >= 100 ? (sbyte) 100 : (sbyte) ((int) item.Durability + 10), item.NoDrop, item.BindOnEquip);
        case EngineeringType.CLASS_UP:
          return ComputerCache.Get(item.Class, item.RequiredShip + 1, item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.CLASS_DOWN:
          return ComputerCache.Get(item.Class, item.RequiredShip - 1, item.Rarity, item.Durability, item.NoDrop, item.BindOnEquip);
        case EngineeringType.RARITY:
          return ComputerCache.Get(item.Class, item.RequiredShip, item.Rarity + 1, item.Durability, item.NoDrop, item.BindOnEquip);
        default:
          return (ComputerItem) null;
      }
    }
  }
}
