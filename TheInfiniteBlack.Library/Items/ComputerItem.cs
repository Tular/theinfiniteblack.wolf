﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ComputerItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Items
{
  public class ComputerItem : EquipmentItem
  {
    private readonly ComputerClass _class;
    private readonly ShipClass _requiredShip;

    public override ItemType Type
    {
      get
      {
        return ItemType.COMPUTER;
      }
    }

    public override int EPCost
    {
      get
      {
        return 0;
      }
    }

    public override int MaxCreditValue
    {
      get
      {
        int num = this._class <= ComputerClass.WARRIOR_MK_4 ? 3600 : (this._class <= ComputerClass.A1_WARRIOR ? 4800 : (this._class <= ComputerClass.A2_WARRIOR ? 6000 : 7200));
        if (this.Rarity == ItemRarity.COMMON)
          return num / 2;
        int rarity = (int) this.Rarity;
        return rarity * rarity * rarity * num;
      }
    }

    public override string IconTag
    {
      get
      {
        return this._class.IconTag();
      }
    }

    public override float XpMod
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.SAGE_MK_1:
          case ComputerClass.SAGE_MK_2:
          case ComputerClass.SAGE_MK_3:
          case ComputerClass.SAGE_MK_4:
            return (float) (0.0599999986588955 + (double) this.Rarity * 0.00999999977648258);
          case ComputerClass.TOIL_MK_1:
          case ComputerClass.HAVOK_MK_1:
          case ComputerClass.CABAL_MK_1:
          case ComputerClass.CABAL_MK_3:
          case ComputerClass.AGENT_MK_1:
          case ComputerClass.DRONE_MK_1:
          case ComputerClass.WARRIOR_MK_1:
            return (float) ((double) this.Rarity * 0.00999999977648258 - 0.0700000002980232);
          case ComputerClass.A1_SAGE:
            return (float) (0.0599999986588955 + (double) this.Rarity * 0.0149999996647239);
          case ComputerClass.A1_HAVOK:
          case ComputerClass.A1_CABAL:
          case ComputerClass.A1_WARRIOR:
          case ComputerClass.A2_HAVOK:
          case ComputerClass.A2_CABAL:
          case ComputerClass.A2_WARRIOR:
          case ComputerClass.A3_HAVOK:
          case ComputerClass.A3_CABAL:
          case ComputerClass.A3_WARRIOR:
            return (float) ((double) this.Rarity * 0.0500000007450581 - 0.349999994039536);
          case ComputerClass.A2_SAGE:
            return (float) (0.0649999976158142 + (double) this.Rarity * 0.0199999995529652);
          case ComputerClass.A3_SAGE:
            return (float) (0.0700000002980232 + (double) this.Rarity * 0.025000000372529);
          default:
            return 0.0f;
        }
      }
    }

    public override int EPBonus
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.TOIL_MK_1:
          case ComputerClass.TOIL_MK_2:
          case ComputerClass.TOIL_MK_3:
          case ComputerClass.TOIL_MK_4:
            return this.Rarity < ItemRarity.LEGENDARY ? 1 : 2;
          case ComputerClass.A1_SAGE:
          case ComputerClass.A1_HAVOK:
          case ComputerClass.A1_CABAL:
          case ComputerClass.A1_AGENT:
          case ComputerClass.A1_DRONE:
          case ComputerClass.A1_WARRIOR:
            return this.Rarity < ItemRarity.ULTIMATE ? 1 : 2;
          case ComputerClass.A1_TOIL:
            return this.Rarity < ItemRarity.ULTIMATE ? 2 : 3;
          case ComputerClass.A2_SAGE:
          case ComputerClass.A2_HAVOK:
          case ComputerClass.A2_CABAL:
          case ComputerClass.A2_AGENT:
          case ComputerClass.A2_DRONE:
          case ComputerClass.A2_WARRIOR:
            return this.Rarity < ItemRarity.ULTIMATE ? 2 : 3;
          case ComputerClass.A2_TOIL:
            return this.Rarity < ItemRarity.ULTIMATE ? 3 : 4;
          case ComputerClass.A3_SAGE:
          case ComputerClass.A3_HAVOK:
          case ComputerClass.A3_CABAL:
          case ComputerClass.A3_AGENT:
          case ComputerClass.A3_DRONE:
          case ComputerClass.A3_WARRIOR:
            return this.Rarity < ItemRarity.ULTIMATE ? 3 : 4;
          case ComputerClass.A3_TOIL:
            return this.Rarity < ItemRarity.ULTIMATE ? 4 : 5;
          default:
            return 0;
        }
      }
    }

    public override float HitChance
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A1_HAVOK:
            return (float) (8.0 + (double) this.Rarity * 1.75);
          case ComputerClass.A2_HAVOK:
            return (float) (10.0 + (double) this.Rarity * 2.0);
          case ComputerClass.A3_HAVOK:
            return (float) (12.0 + (double) this.Rarity * 2.25);
          case ComputerClass.SAGE_MK_4:
          case ComputerClass.TOIL_MK_4:
          case ComputerClass.CABAL_MK_4:
          case ComputerClass.AGENT_MK_4:
          case ComputerClass.DRONE_MK_4:
            return (float) this.Rarity - 10f;
          case ComputerClass.HAVOK_MK_1:
          case ComputerClass.HAVOK_MK_2:
          case ComputerClass.HAVOK_MK_3:
          case ComputerClass.HAVOK_MK_4:
            return (float) (8.0 + (double) this.Rarity * 1.5);
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalChance
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A1_TOIL:
          case ComputerClass.A1_AGENT:
          case ComputerClass.A2_TOIL:
          case ComputerClass.A2_AGENT:
          case ComputerClass.A3_TOIL:
          case ComputerClass.A3_AGENT:
            return (float) ((double) this.Rarity * 3.0 - 21.0);
          case ComputerClass.A1_CABAL:
            return (float) (6.0 + (double) this.Rarity * 2.0);
          case ComputerClass.A2_CABAL:
            return (float) (6.0 + (double) this.Rarity * 2.5);
          case ComputerClass.A3_CABAL:
            return (float) (6.0 + (double) this.Rarity * 3.0);
          case ComputerClass.CABAL_MK_1:
          case ComputerClass.CABAL_MK_2:
          case ComputerClass.CABAL_MK_3:
          case ComputerClass.CABAL_MK_4:
            return 6f + (float) this.Rarity;
          case ComputerClass.AGENT_MK_4:
          case ComputerClass.DRONE_MK_4:
            return (float) ((double) this.Rarity * 0.5 - 3.5);
          default:
            return 0.0f;
        }
      }
    }

    public override float EvasionChance
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.SAGE_MK_3:
          case ComputerClass.TOIL_MK_3:
          case ComputerClass.HAVOK_MK_3:
          case ComputerClass.CABAL_MK_3:
          case ComputerClass.DRONE_MK_3:
          case ComputerClass.WARRIOR_MK_3:
            return (float) this.Rarity - 10f;
          case ComputerClass.TOIL_MK_1:
          case ComputerClass.CABAL_MK_4:
          case ComputerClass.WARRIOR_MK_4:
            return (float) this.Rarity - 7f;
          case ComputerClass.AGENT_MK_1:
          case ComputerClass.AGENT_MK_2:
          case ComputerClass.AGENT_MK_4:
            return 6f + (float) this.Rarity;
          case ComputerClass.AGENT_MK_3:
            return (float) (6.0 + (double) this.Rarity * 1.5);
          case ComputerClass.A1_SAGE:
          case ComputerClass.A1_DRONE:
          case ComputerClass.A2_SAGE:
          case ComputerClass.A2_DRONE:
          case ComputerClass.A3_SAGE:
          case ComputerClass.A3_DRONE:
            return (float) ((double) this.Rarity * 3.0 - 21.0);
          case ComputerClass.A1_AGENT:
            return (float) (6.0 + (double) this.Rarity * 1.25);
          case ComputerClass.A2_AGENT:
            return (float) (8.0 + (double) this.Rarity * 2.0);
          case ComputerClass.A3_AGENT:
            return (float) (10.0 + (double) this.Rarity * 3.0);
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashChance
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A1_DRONE:
            return (float) (6.0 + (double) this.Rarity * 2.5);
          case ComputerClass.A2_DRONE:
            return (float) (8.0 + (double) this.Rarity * 3.0);
          case ComputerClass.A3_DRONE:
            return (float) (10.0 + (double) this.Rarity * 3.5);
          case ComputerClass.SAGE_MK_1:
          case ComputerClass.HAVOK_MK_4:
          case ComputerClass.CABAL_MK_1:
            return (float) this.Rarity - 7f;
          case ComputerClass.DRONE_MK_1:
          case ComputerClass.DRONE_MK_2:
          case ComputerClass.DRONE_MK_3:
          case ComputerClass.DRONE_MK_4:
            return (float) (4.0 + (double) this.Rarity * 2.0);
          default:
            return 0.0f;
        }
      }
    }

    public override float MetalRepairMod
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A2_DRONE:
            return (float) (0.0199999995529652 + (double) this.Rarity * 0.0140000004321337);
          case ComputerClass.A3_SAGE:
          case ComputerClass.A3_HAVOK:
          case ComputerClass.A1_SAGE:
          case ComputerClass.A1_HAVOK:
          case ComputerClass.A2_SAGE:
          case ComputerClass.A2_HAVOK:
            return (float) ((double) this.Rarity * 0.0299999993294477 - 0.209999993443489);
          case ComputerClass.A3_DRONE:
            return (float) (0.0199999995529652 + (double) this.Rarity * 0.0260000005364418);
          case ComputerClass.DRONE_MK_3:
          case ComputerClass.WARRIOR_MK_2:
          case ComputerClass.SAGE_MK_4:
          case ComputerClass.TOIL_MK_4:
          case ComputerClass.HAVOK_MK_1:
          case ComputerClass.CABAL_MK_2:
            return (float) ((double) this.Rarity * 0.00999999977648258 - 0.0700000002980232);
          case ComputerClass.WARRIOR_MK_4:
          case ComputerClass.SAGE_MK_1:
          case ComputerClass.HAVOK_MK_4:
            return (float) ((double) this.Rarity * 0.00999999977648258 - 0.100000001490116);
          case ComputerClass.A1_DRONE:
            return (float) (0.00999999977648258 + (double) this.Rarity * 0.00999999977648258);
          default:
            return 0.0f;
        }
      }
    }

    public override float OutgoingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A2_WARRIOR:
            return (float) this.Rarity * 0.019f;
          case ComputerClass.A3_WARRIOR:
            return (float) this.Rarity * 0.024f;
          case ComputerClass.WARRIOR_MK_1:
          case ComputerClass.WARRIOR_MK_2:
          case ComputerClass.WARRIOR_MK_3:
          case ComputerClass.WARRIOR_MK_4:
            return (float) (0.0199999995529652 + (double) this.Rarity * 0.00999999977648258);
          case ComputerClass.A1_WARRIOR:
            return (float) this.Rarity * 0.016f;
          default:
            return 0.0f;
        }
      }
    }

    public override float IncomingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A2_TOIL:
            return (float) (this.Rarity + 1) * -0.007f;
          case ComputerClass.A3_TOIL:
            return (float) (this.Rarity + 1) * (-3f / 500f);
          case ComputerClass.TOIL_MK_2:
            return (float) ((double) this.Rarity * 0.001 - 0.007);
          case ComputerClass.A1_TOIL:
            return (float) (this.Rarity + 1) * -0.005f;
          default:
            return 0.0f;
        }
      }
    }

    public override int ResourceCapacity
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.DRONE_MK_2:
          case ComputerClass.WARRIOR_MK_3:
          case ComputerClass.TOIL_MK_3:
          case ComputerClass.HAVOK_MK_3:
          case ComputerClass.AGENT_MK_1:
          case ComputerClass.AGENT_MK_3:
            return (int) (this.Rarity - 7);
          case ComputerClass.A1_CABAL:
          case ComputerClass.A1_AGENT:
          case ComputerClass.A2_CABAL:
          case ComputerClass.A2_AGENT:
          case ComputerClass.A3_CABAL:
          case ComputerClass.A3_AGENT:
            return 4 * (int) this.Rarity - 28;
          case ComputerClass.A1_DRONE:
          case ComputerClass.A2_DRONE:
          case ComputerClass.A3_DRONE:
            return (int) this.Rarity * 2;
          default:
            return 0;
        }
      }
    }

    public override int MoveSpeedMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.SAGE_MK_2:
          case ComputerClass.SAGE_MK_3:
          case ComputerClass.TOIL_MK_2:
          case ComputerClass.CABAL_MK_2:
          case ComputerClass.AGENT_MK_2:
          case ComputerClass.AGENT_MK_3:
          case ComputerClass.DRONE_MK_2:
          case ComputerClass.WARRIOR_MK_2:
            return 2100 - 300 * (int) this.Rarity;
          case ComputerClass.HAVOK_MK_1:
          case ComputerClass.HAVOK_MK_2:
          case ComputerClass.HAVOK_MK_3:
          case ComputerClass.HAVOK_MK_4:
            return -100 * (int) this.Rarity;
          case ComputerClass.A1_SAGE:
            return -50 * (int) this.Rarity;
          case ComputerClass.A1_TOIL:
          case ComputerClass.A2_TOIL:
          case ComputerClass.A3_TOIL:
            return 1400 - 200 * (int) this.Rarity;
          case ComputerClass.A1_HAVOK:
            return -125 * (int) this.Rarity;
          case ComputerClass.A2_SAGE:
            return -75 * (int) this.Rarity;
          case ComputerClass.A2_HAVOK:
            return -150 * (int) this.Rarity;
          case ComputerClass.A3_SAGE:
            return -100 * (int) this.Rarity;
          case ComputerClass.A3_HAVOK:
            return -175 * (int) this.Rarity;
          default:
            return 0;
        }
      }
    }

    public override float StunResist
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A2_AGENT:
          case ComputerClass.A2_TOIL:
            return (float) this.Rarity * 1.5f;
          case ComputerClass.A3_TOIL:
            return (float) this.Rarity * 2f;
          case ComputerClass.A3_AGENT:
            return (float) this.Rarity * 2.5f;
          case ComputerClass.A1_TOIL:
          case ComputerClass.A1_AGENT:
            return (float) this.Rarity * 1f;
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleResist
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A2_DRONE:
          case ComputerClass.A2_TOIL:
            return (float) this.Rarity * 1.5f;
          case ComputerClass.A3_TOIL:
            return (float) this.Rarity * 2f;
          case ComputerClass.A3_DRONE:
            return (float) this.Rarity * 2.5f;
          case ComputerClass.A1_TOIL:
          case ComputerClass.A1_DRONE:
            return (float) this.Rarity * 1f;
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalResist
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A2_WARRIOR:
          case ComputerClass.A2_TOIL:
            return (float) this.Rarity * 1.5f;
          case ComputerClass.A3_TOIL:
            return (float) this.Rarity * 2f;
          case ComputerClass.A3_WARRIOR:
            return (float) this.Rarity * 2.5f;
          case ComputerClass.A1_TOIL:
          case ComputerClass.A1_WARRIOR:
            return (float) this.Rarity * 1f;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashResist
    {
      get
      {
        switch (this._class)
        {
          case ComputerClass.A1_TOIL:
            return (float) this.Rarity * 1f;
          case ComputerClass.A2_TOIL:
            return (float) this.Rarity * 1.5f;
          case ComputerClass.A3_TOIL:
            return (float) this.Rarity * 2f;
          default:
            return 0.0f;
        }
      }
    }

    public ComputerClass Class
    {
      get
      {
        return this._class;
      }
    }

    public ShipClass RequiredShip
    {
      get
      {
        return this._requiredShip;
      }
    }

    protected internal ComputerItem(ComputerClass computerItemClass, ShipClass requiredShipClass, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(rarity, durability, noDrop, bindOnEquip)
    {
      this._class = computerItemClass;
      this._requiredShip = requiredShipClass;
    }

    public bool WorksInShip(ShipClass shipClass)
    {
      return this._requiredShip.CanUseEquipment(shipClass);
    }

    public override void Write(IByteBuffer output)
    {
      base.Write(output);
      output.Write((sbyte) this._class);
      output.Write((sbyte) this._requiredShip);
    }

    public override void AppendName(StringBuilder sb)
    {
      sb.Append(this._class.Name());
    }

    public override EquipmentItem GetUpgrade(EngineeringType type)
    {
      return (EquipmentItem) ComputerCache.GetUpgrade(type, this);
    }

    public override bool CanUpgrade(EngineeringType type)
    {
      if (this.Rarity <= ItemRarity.COMMON)
        return false;
      switch (type)
      {
        case EngineeringType.DECONSTRUCT:
          return true;
        case EngineeringType.REPAIR:
          if ((int) this.Durability > 1)
            return (int) this.Durability < 100;
          return false;
        case EngineeringType.CLASS_UP:
          return this._requiredShip < ShipClass.Annihilator;
        case EngineeringType.CLASS_DOWN:
          return this._requiredShip > ShipClass.Shuttle;
        case EngineeringType.RARITY:
          return this.Rarity < ItemRarity.ULTIMATE;
        case EngineeringType.UNBIND:
          return this.NoDrop;
        default:
          return false;
      }
    }

    public override void AppendSubTitle(StringBuilder sb)
    {
      sb.Append(this.Rarity.Name());
      if (this._class <= ComputerClass.WARRIOR_MK_4)
        sb.Append(" Computer");
      else if (this._class <= ComputerClass.A1_WARRIOR)
        sb.Append(" Tech I Computer");
      else if (this._class <= ComputerClass.A2_WARRIOR)
        sb.Append(" Tech II Computer");
      else
        sb.Append(" Tech III Computer");
    }

    public override void AppendDescription(StringBuilder sb)
    {
      sb.Append(this._requiredShip.Name());
      sb.Append("-Class Ships");
      ShipClass wyrdEquiv = this._requiredShip.GetWyrdEquiv();
      if (wyrdEquiv == ShipClass.None)
        return;
      sb.Append("\n");
      sb.Append(wyrdEquiv.Name());
      sb.Append(" Ships");
    }
  }
}
