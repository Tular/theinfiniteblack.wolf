﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ComputerClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public enum ComputerClass
  {
    NULL = -128,
    SAGE_MK_1 = 1,
    SAGE_MK_2 = 2,
    SAGE_MK_3 = 3,
    SAGE_MK_4 = 4,
    TOIL_MK_1 = 5,
    TOIL_MK_2 = 6,
    TOIL_MK_3 = 7,
    TOIL_MK_4 = 8,
    HAVOK_MK_1 = 9,
    HAVOK_MK_2 = 10,
    HAVOK_MK_3 = 11,
    HAVOK_MK_4 = 12,
    CABAL_MK_1 = 13,
    CABAL_MK_2 = 14,
    CABAL_MK_3 = 15,
    CABAL_MK_4 = 16,
    AGENT_MK_1 = 17,
    AGENT_MK_2 = 18,
    AGENT_MK_3 = 19,
    AGENT_MK_4 = 20,
    DRONE_MK_1 = 21,
    DRONE_MK_2 = 22,
    DRONE_MK_3 = 23,
    DRONE_MK_4 = 24,
    WARRIOR_MK_1 = 25,
    WARRIOR_MK_2 = 26,
    WARRIOR_MK_3 = 27,
    WARRIOR_MK_4 = 28,
    A1_SAGE = 29,
    A1_TOIL = 30,
    A1_HAVOK = 31,
    A1_CABAL = 32,
    A1_AGENT = 33,
    A1_DRONE = 34,
    A1_WARRIOR = 35,
    A2_SAGE = 36,
    A2_TOIL = 37,
    A2_HAVOK = 38,
    A2_CABAL = 39,
    A2_AGENT = 40,
    A2_DRONE = 41,
    A2_WARRIOR = 42,
    A3_SAGE = 43,
    A3_TOIL = 44,
    A3_HAVOK = 45,
    A3_CABAL = 46,
    A3_AGENT = 47,
    A3_DRONE = 48,
    A3_WARRIOR = 49,
  }
}
