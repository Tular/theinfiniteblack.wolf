﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ItemRarity
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public enum ItemRarity
  {
    NULL = -128,
    COMMON = 1,
    UNCOMMON = 2,
    RARE = 3,
    ULTRA_RARE = 4,
    LEGENDARY = 5,
    PRECURSOR = 6,
    ULTIMATE = 7,
  }
}
