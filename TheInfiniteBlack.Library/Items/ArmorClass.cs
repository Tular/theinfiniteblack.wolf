﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ArmorClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public enum ArmorClass
  {
    NULL = -128,
    TITANIUM_HULL = 0,
    COMPOSITE_HULL = 1,
    CARBIDE_HULL = 2,
    EXOPRENE_SHIELD = 10,
    CYTOPLAST_SHIELD = 11,
    HOLOCRINE_SHIELD = 12,
    DIAMOND_ARMOR = 20,
    THORIUM_ARMOR = 21,
    OSMIUM_ARMOR = 22,
    CITADEL_GAMBIT = 30,
    AJAX_GAMBIT = 31,
    AEGIS_GAMBIT = 32,
    KISMET_GAMBIT = 33,
  }
}
