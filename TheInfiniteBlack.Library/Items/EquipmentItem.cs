﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.EquipmentItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Text;

namespace TheInfiniteBlack.Library.Items
{
  public abstract class EquipmentItem
  {
    private readonly bool _bindOnEquip;
    private readonly sbyte _durability;
    private readonly bool _noDrop;
    private readonly ItemRarity _rarity;

    public abstract ItemType Type { get; }

    public string LongName
    {
      get
      {
        StringBuilder sb = new StringBuilder(200);
        this.AppendName(sb);
        return sb.ToString();
      }
    }

    public abstract int EPCost { get; }

    public bool IsOfNote
    {
      get
      {
        switch (this.Type)
        {
          case ItemType.WEAPON:
          case ItemType.ARMOR:
          case ItemType.STORAGE:
          case ItemType.HARVESTER:
            return this._rarity >= ItemRarity.UNCOMMON;
          default:
            return true;
        }
      }
    }

    public float DeconstructRewardPointValue
    {
      get
      {
        return (float) ((double) this.ActualCreditValue / 2000.0 * 0.00999999977648258);
      }
    }

    public abstract int MaxCreditValue { get; }

    public bool CanAuction
    {
      get
      {
        if (this._rarity > ItemRarity.COMMON && (int) this._durability > 15)
          return !this._noDrop;
        return false;
      }
    }

    public int AuctionPostCost
    {
      get
      {
        int num = this.ActualCreditValue * 3 / 100;
        if (num <= 15000)
          return num;
        return 15000;
      }
    }

    public int ActualCreditValue
    {
      get
      {
        return this.MaxCreditValue * (int) this._durability / 100;
      }
    }

    public int ActualBlackDollarValue
    {
      get
      {
        return EquipmentItem.GetBlackDollarValue((float) this.ActualCreditValue);
      }
    }

    public int StarPortSellValue
    {
      get
      {
        return this.ActualCreditValue / 10;
      }
    }

    public int RequiredLevel
    {
      get
      {
        return (int) (this._rarity - 1) * 5;
      }
    }

    public virtual int EPBonus
    {
      get
      {
        return 0;
      }
    }

    public virtual int ResourceCapacity
    {
      get
      {
        return 0;
      }
    }

    public virtual int MoveSpeedMSAdjust
    {
      get
      {
        return 0;
      }
    }

    public virtual float MetalRepairMod
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float OutgoingDamageMod
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float IncomingDamageMod
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float XpMod
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual int MaxHullBonus
    {
      get
      {
        return 0;
      }
    }

    public virtual float CriticalDamageMod
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float CriticalChance
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float EvasionChance
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float SplashChance
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float GrappleChance
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float StunChance
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual int StunDurationMSAdjust
    {
      get
      {
        return 0;
      }
    }

    public virtual float HitChance
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float StunResist
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float GrappleResist
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float CriticalResist
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual float SplashResist
    {
      get
      {
        return 0.0f;
      }
    }

    public virtual bool CanBeRepaired
    {
      get
      {
        if ((int) this._durability > 1 && (int) this._durability < 100)
          return this._rarity > ItemRarity.COMMON;
        return false;
      }
    }

    public string SubTitle
    {
      get
      {
        StringBuilder sb = new StringBuilder(25);
        this.AppendSubTitle(sb);
        return sb.ToString();
      }
    }

    public string Description
    {
      get
      {
        StringBuilder sb = new StringBuilder(30);
        this.AppendDescription(sb);
        return sb.ToString();
      }
    }

    public string Pros
    {
      get
      {
        StringBuilder pros = new StringBuilder(100);
        StringBuilder cons = new StringBuilder(100);
        this.AppendProsAndCons(pros, cons);
        return pros.ToString();
      }
    }

    public string Cons
    {
      get
      {
        StringBuilder pros = new StringBuilder(100);
        StringBuilder cons = new StringBuilder(100);
        this.AppendProsAndCons(pros, cons);
        return cons.ToString();
      }
    }

    public ItemIcon Icon
    {
      get
      {
        return ItemIconHelper.GetIcon(this);
      }
    }

    public abstract string IconTag { get; }

    public ItemRarity Rarity
    {
      get
      {
        return this._rarity;
      }
    }

    public sbyte Durability
    {
      get
      {
        return this._durability;
      }
    }

    public bool NoDrop
    {
      get
      {
        return this._noDrop;
      }
    }

    public bool BindOnEquip
    {
      get
      {
        return this._bindOnEquip;
      }
    }

    protected internal EquipmentItem(ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
    {
      this._rarity = rarity;
      this._durability = durability;
      this._noDrop = noDrop;
      this._bindOnEquip = bindOnEquip;
    }

    public virtual void Write(IByteBuffer output)
    {
      output.Write((sbyte) this.Type);
      output.Write((sbyte) this._rarity);
      output.Write(this._durability);
      output.Write(this._noDrop);
      if (this._noDrop)
        return;
      output.Write(this._bindOnEquip ? sbyte.MaxValue : sbyte.MinValue);
    }

    public void AppendProsAndCons(StringBuilder pros, StringBuilder cons)
    {
      int epBonus = this.EPBonus;
      int resourceCapacity = this.ResourceCapacity;
      int maxHullBonus = this.MaxHullBonus;
      float moveSpeedMsAdjust = (float) this.MoveSpeedMSAdjust;
      float metalRepairMod = this.MetalRepairMod;
      float criticalChance = this.CriticalChance;
      float criticalDamageMod = this.CriticalDamageMod;
      float evasionChance = this.EvasionChance;
      float hitChance = this.HitChance;
      float splashChance = this.SplashChance;
      float outgoingDamageMod = this.OutgoingDamageMod;
      float incomingDamageMod = this.IncomingDamageMod;
      float xpMod = this.XpMod;
      int requiredLevel = this.RequiredLevel;
      float stunChance = this.StunChance;
      float durationMsAdjust = (float) this.StunDurationMSAdjust;
      float grappleChance = this.GrappleChance;
      float stunResist = this.StunResist;
      float grappleResist = this.GrappleResist;
      float criticalResist = this.CriticalResist;
      float splashResist = this.SplashResist;
      if (epBonus > 0)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(epBonus);
        pros.Append(" EquipPoints");
      }
      else if (epBonus < 0)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(epBonus);
        cons.Append(" EquipPoints");
      }
      if (resourceCapacity > 0)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(resourceCapacity);
        pros.Append(" Resource Capacity");
      }
      else if (resourceCapacity < 0)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(resourceCapacity);
        cons.Append(" Resource Capacity");
      }
      if (maxHullBonus > 0)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(maxHullBonus);
        pros.Append(" Hull Capacity");
      }
      else if (maxHullBonus < 0)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(maxHullBonus);
        cons.Append(" Hull Capacity");
      }
      float num;
      if ((double) moveSpeedMsAdjust < -1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        StringBuilder stringBuilder = pros;
        num = moveSpeedMsAdjust / 1000f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        pros.Append("s Movement Cooldown");
      }
      else if ((double) moveSpeedMsAdjust > 1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append("+");
        cons.Append((moveSpeedMsAdjust / 1000f).ToString("#,0.##"));
        cons.Append("s Movement Cooldown");
      }
      if ((double) metalRepairMod > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        StringBuilder stringBuilder = pros;
        num = metalRepairMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        pros.Append("% Metal Hull Repair");
      }
      else if ((double) metalRepairMod < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        StringBuilder stringBuilder = cons;
        num = metalRepairMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        cons.Append("% Metal Hull Repair");
      }
      if ((double) criticalChance > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(criticalChance.ToString("#,0.##"));
        pros.Append(" Critical Chance");
      }
      else if ((double) criticalChance < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(criticalChance.ToString("#,0.##"));
        cons.Append(" Critical Chance");
      }
      if ((double) criticalDamageMod > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        StringBuilder stringBuilder = pros;
        num = criticalDamageMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        pros.Append("% Critical Damage");
      }
      else if ((double) criticalDamageMod < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        StringBuilder stringBuilder = cons;
        num = criticalDamageMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        cons.Append("% Critical Damage");
      }
      if ((double) evasionChance > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(evasionChance.ToString("#,0.##"));
        pros.Append(" Evasion");
      }
      else if ((double) evasionChance < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(evasionChance.ToString("#,0.##"));
        cons.Append(" Evasion");
      }
      if ((double) hitChance > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(hitChance.ToString("#,0.##"));
        pros.Append(" Hit Chance");
      }
      else if ((double) hitChance < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(hitChance.ToString("#,0.##"));
        cons.Append(" Hit Chance");
      }
      if ((double) splashChance > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(splashChance.ToString("#,0.##"));
        pros.Append(" Splash Chance");
      }
      else if ((double) splashChance < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(splashChance.ToString("#,0.##"));
        cons.Append(" Splash Chance");
      }
      if ((double) outgoingDamageMod > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        StringBuilder stringBuilder = pros;
        num = outgoingDamageMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        pros.Append("% Damage");
      }
      else if ((double) outgoingDamageMod < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        StringBuilder stringBuilder = cons;
        num = outgoingDamageMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        cons.Append("% Damage");
      }
      if ((double) incomingDamageMod < -1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        StringBuilder stringBuilder = pros;
        num = incomingDamageMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        pros.Append("% Damage Reduction");
      }
      else if ((double) incomingDamageMod > 1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append("+");
        StringBuilder stringBuilder = cons;
        num = incomingDamageMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        cons.Append("% Damage Vulnerability");
      }
      if ((double) xpMod > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        StringBuilder stringBuilder = pros;
        num = xpMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        pros.Append("% XP Gain");
      }
      else if ((double) xpMod < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        StringBuilder stringBuilder = cons;
        num = xpMod * 100f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        cons.Append("% XP Gain");
      }
      if ((double) stunChance > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(stunChance.ToString("#,0.##"));
        pros.Append(" Stun Chance");
      }
      else if ((double) stunChance < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(stunChance.ToString("#,0.##"));
        cons.Append(" Stun Chance");
      }
      if ((double) durationMsAdjust > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        StringBuilder stringBuilder = pros;
        num = durationMsAdjust / 1000f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        pros.Append("s Stun Duration");
      }
      else if ((double) durationMsAdjust < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        StringBuilder stringBuilder = cons;
        num = durationMsAdjust / 1000f;
        string str = num.ToString("#,0.##");
        stringBuilder.Append(str);
        cons.Append("s Stun Duration");
      }
      if ((double) grappleChance > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(grappleChance.ToString("#,0.##"));
        pros.Append(" Grapple Chance");
      }
      else if ((double) grappleChance < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(grappleChance.ToString("#,0.##"));
        cons.Append(" Grapple Chance");
      }
      if ((double) stunResist > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(stunResist.ToString("#,0.##"));
        pros.Append(" Stun Resist");
      }
      else if ((double) stunResist < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(stunResist.ToString("#,0.##"));
        cons.Append(" Stun Resist");
      }
      if ((double) grappleResist > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(grappleResist.ToString("#,0.##"));
        pros.Append(" Grapple Resist");
      }
      else if ((double) grappleResist < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(grappleResist.ToString("#,0.##"));
        cons.Append(" Grapple Resist");
      }
      if ((double) criticalResist > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(criticalResist.ToString("#,0.##"));
        pros.Append(" Critical Resist");
      }
      else if ((double) criticalResist < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(criticalResist.ToString("#,0.##"));
        cons.Append(" Critical Resist");
      }
      if ((double) splashResist > 1.0000000116861E-07)
      {
        if (pros.Length > 0)
          pros.AppendLine();
        pros.Append("+");
        pros.Append(splashResist.ToString("#,0.##"));
        pros.Append(" Splash Resist");
      }
      else if ((double) splashResist < -1.0000000116861E-07)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append(splashResist.ToString("#,0.##"));
        cons.Append(" Splash Resist");
      }
      if (requiredLevel > 0)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append("Requires Level ");
        cons.Append(requiredLevel);
      }
      if (!this.CanBeRepaired && (int) this._durability < 100 && this._rarity > ItemRarity.COMMON)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append("NO REPAIR");
      }
      if ((int) this._durability <= 25)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append("CAN BE STOLEN");
        cons.AppendLine();
        cons.Append("(LOW DURABILITY)");
      }
      if (this._noDrop)
      {
        if (cons.Length > 0)
          cons.AppendLine();
        cons.Append("NO DROP");
      }
      if (!this._bindOnEquip)
        return;
      if (cons.Length > 0)
        cons.AppendLine();
      cons.Append("BIND ON EQUIP");
    }

    public abstract void AppendSubTitle(StringBuilder sb);

    public abstract void AppendDescription(StringBuilder sb);

    public abstract void AppendName(StringBuilder sb);

    public void AppendLongName(StringBuilder sb, bool markup, bool durability)
    {
      if (!markup)
      {
        sb.Append(this._rarity.Name());
        sb.Append(" ");
      }
      else
        sb.Append(this.Rarity.GetMarkup());
      sb.Append("[");
      this.AppendName(sb);
      sb.Append("]");
      if (markup)
        sb.Append("[-]");
      if (!durability)
        return;
      sb.Append(" ");
      sb.Append(this._durability);
      sb.Append("%");
    }

    public abstract EquipmentItem GetUpgrade(EngineeringType type);

    public abstract bool CanUpgrade(EngineeringType type);

    public int GetEngineeringCost(EngineeringType type)
    {
      int cost = this.GetCost(type);
      if (type == EngineeringType.REPAIR || type == EngineeringType.UNBIND || !this._noDrop && !this._bindOnEquip)
        return cost;
      return cost / 2;
    }

    private int GetCost(EngineeringType type)
    {
      switch (type)
      {
        case EngineeringType.REPAIR:
          switch (this.Type)
          {
            case ItemType.WEAPON:
              return (int) this._rarity * 9;
            case ItemType.ARMOR:
              return (int) this._rarity * 8;
            case ItemType.STORAGE:
              return (int) this._rarity * 7;
            case ItemType.HARVESTER:
              return (int) this._rarity * 16;
            case ItemType.ENGINE:
              return (int) this._rarity * 24;
            case ItemType.COMPUTER:
              return (int) this._rarity * 14;
            case ItemType.SPECIAL:
              return (int) this._rarity * 20;
            default:
              return int.MaxValue;
          }
        case EngineeringType.RANK_UP:
        case EngineeringType.RANK_DOWN:
          switch (this.Type)
          {
            case ItemType.WEAPON:
              return (int) this._rarity * 22;
            case ItemType.ARMOR:
              return (int) this._rarity * 18;
            case ItemType.STORAGE:
              return (int) this._rarity * 14;
            default:
              return int.MaxValue;
          }
        case EngineeringType.CLASS_UP:
        case EngineeringType.CLASS_DOWN:
          switch (this.Type)
          {
            case ItemType.COMPUTER:
              return 4 * (int) ((ComputerItem) this).RequiredShip * (int) this._rarity;
            case ItemType.SPECIAL:
              return 6 * (int) ((SpecialItem) this).RequiredShip * (int) this._rarity;
            default:
              return int.MaxValue;
          }
        case EngineeringType.RARITY:
          int rarity = (int) this._rarity;
          int num = rarity * rarity * rarity * rarity;
          switch (this.Type)
          {
            case ItemType.WEAPON:
              return num * 6 / 2;
            case ItemType.ARMOR:
              return num * 5 / 2;
            case ItemType.STORAGE:
              return num * 4 / 2;
            case ItemType.HARVESTER:
              return num * 8 / 2;
            case ItemType.ENGINE:
              return num * 10 / 2;
            case ItemType.COMPUTER:
              return num * 6 / 2;
            case ItemType.SPECIAL:
              return num * 10 / 2;
            default:
              return int.MaxValue;
          }
        case EngineeringType.UNBIND:
          return (int) this._rarity * 100;
        default:
          return 0;
      }
    }

    public override string ToString()
    {
      return this.LongName;
    }

    public static int GetBlackDollarValue(float creditValue)
    {
      return (double) creditValue <= 1000.0 ? 1 : (int) (1.0 + Math.Round((double) creditValue / 1000.0));
    }

    public static EquipmentItem Execute(IByteBuffer input)
    {
      switch (input.Get())
      {
        case 1:
          return (EquipmentItem) WeaponCache.Get(input);
        case 2:
          return (EquipmentItem) ArmorCache.Get(input);
        case 3:
          return (EquipmentItem) StorageCache.Get(input);
        case 4:
          return (EquipmentItem) HarvesterCache.Get(input);
        case 5:
          return (EquipmentItem) EngineCache.Get(input);
        case 6:
          return (EquipmentItem) ComputerCache.Get(input);
        case 7:
          return (EquipmentItem) SpecialCache.Get(input);
        default:
          return (EquipmentItem) null;
      }
    }
  }
}
