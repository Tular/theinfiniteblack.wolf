﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.EngineItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library.Items
{
  public class EngineItem : EquipmentItem
  {
    private readonly EngineClass _class;

    public override ItemType Type
    {
      get
      {
        return ItemType.ENGINE;
      }
    }

    public override int EPCost
    {
      get
      {
        switch (this._class)
        {
          case EngineClass.GRAVITY:
            return 0;
          case EngineClass.SKIP:
            return 0;
          case EngineClass.FISSION:
            return 0;
          case EngineClass.IMPULSE:
            return 0;
          case EngineClass.FUSION:
            return 0;
          case EngineClass.NEUTRON:
            return 0;
          case EngineClass.STEALTH:
            return 0;
          default:
            return 0;
        }
      }
    }

    public override int MaxCreditValue
    {
      get
      {
        if (this.Rarity == ItemRarity.COMMON)
          return 2700;
        int rarity = (int) this.Rarity;
        return rarity * rarity * rarity * 5400;
      }
    }

    public override string IconTag
    {
      get
      {
        return this._class.IconTag();
      }
    }

    public override int MoveSpeedMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case EngineClass.GRAVITY:
            return 0;
          case EngineClass.SKIP:
            return 0;
          case EngineClass.FISSION:
            return 0;
          case EngineClass.IMPULSE:
            return 0;
          case EngineClass.FUSION:
            return 0;
          case EngineClass.NEUTRON:
            return 0;
          case EngineClass.STEALTH:
            return 0;
          default:
            return 0;
        }
      }
    }

    public override int EPBonus
    {
      get
      {
        return this._class == EngineClass.GRAVITY ? 1 : 0;
      }
    }

    public override float EvasionChance
    {
      get
      {
        switch (this._class)
        {
          case EngineClass.GRAVITY:
          case EngineClass.SKIP:
          case EngineClass.STEALTH:
            return (float) this.Rarity;
          default:
            return 0.0f;
        }
      }
    }

    public override float HitChance
    {
      get
      {
        switch (this._class)
        {
          case EngineClass.GRAVITY:
          case EngineClass.SKIP:
          case EngineClass.STEALTH:
            return (float) this.Rarity * 1.5f;
          default:
            return 0.0f;
        }
      }
    }

    public override int ResourceCapacity
    {
      get
      {
        if (this._class == EngineClass.FISSION)
          return (int) this.Rarity;
        return 0;
      }
    }

    public EngineClass Class
    {
      get
      {
        return this._class;
      }
    }

    protected internal EngineItem(EngineClass engineItemClass, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(rarity, durability, noDrop, bindOnEquip)
    {
      this._class = engineItemClass;
    }

    public override void Write(IByteBuffer output)
    {
      base.Write(output);
      output.Write((sbyte) this._class);
    }

    public override void AppendName(StringBuilder sb)
    {
      sb.Append(this._class.Name());
    }

    public override EquipmentItem GetUpgrade(EngineeringType type)
    {
      return (EquipmentItem) EngineCache.GetUpgrade(type, this);
    }

    public override bool CanUpgrade(EngineeringType type)
    {
      if (this.Rarity <= ItemRarity.COMMON)
        return false;
      if (type <= EngineeringType.REPAIR)
      {
        if (type == EngineeringType.DECONSTRUCT)
          return true;
        if (type == EngineeringType.REPAIR && (int) this.Durability > 1)
          return (int) this.Durability < 100;
      }
      else
      {
        if (type == EngineeringType.RARITY)
          return this.Rarity < ItemRarity.ULTIMATE;
        if (type == EngineeringType.UNBIND)
          return this.NoDrop;
      }
      return false;
    }

    public override void AppendSubTitle(StringBuilder sb)
    {
      sb.Append(this.Rarity.Name());
      sb.Append(" Engine");
    }

    public override void AppendDescription(StringBuilder sb)
    {
      switch (this._class)
      {
        case EngineClass.GRAVITY:
          sb.Append(10 + (int) this.Rarity * 2);
          sb.Append("% Chance of Instant Jump");
          break;
        case EngineClass.SKIP:
          sb.Append("Very Fast Movement\n");
          sb.Append(27 - (int) this.Rarity * 2);
          sb.Append("% Chance of Durability Loss");
          break;
        case EngineClass.NEUTRON:
          sb.Append("Very Fast Movement\nBurns 1 DarkMatter Every " + (object) (3 + this.Rarity) + " Jumps");
          break;
        case EngineClass.STEALTH:
          sb.Append("Silent Movement\nInvisible to Scanning");
          break;
      }
    }
  }
}
