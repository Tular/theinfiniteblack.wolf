﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.HarvesterItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;

namespace TheInfiniteBlack.Library.Items
{
  public class HarvesterItem : EquipmentItem
  {
    private readonly HarvesterClass _class;

    public override ItemType Type
    {
      get
      {
        return ItemType.HARVESTER;
      }
    }

    public override int EPCost
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.LCD_V1:
          case HarvesterClass.LCD_V2:
          case HarvesterClass.LCD_V3:
          case HarvesterClass.LCD_V4:
          case HarvesterClass.LCD_V5:
          case HarvesterClass.LCD_V6:
          case HarvesterClass.LCD_V7:
          case HarvesterClass.LCD_V8:
          case HarvesterClass.LCD_V9:
          case HarvesterClass.LCD_V10:
          case HarvesterClass.LCD_V11:
          case HarvesterClass.LCD_V12:
            switch (this.Rarity)
            {
              case ItemRarity.RARE:
              case ItemRarity.ULTRA_RARE:
                return 1;
              case ItemRarity.LEGENDARY:
              case ItemRarity.PRECURSOR:
              case ItemRarity.ULTIMATE:
                return 0;
              default:
                return 2;
            }
          case HarvesterClass.HX_V1:
          case HarvesterClass.HX_V2:
          case HarvesterClass.HX_V3:
          case HarvesterClass.HX_V4:
          case HarvesterClass.HX_V5:
          case HarvesterClass.HX_V6:
          case HarvesterClass.HX_V7:
          case HarvesterClass.HX_V8:
          case HarvesterClass.HX_V9:
          case HarvesterClass.HX_V10:
          case HarvesterClass.HX_V11:
          case HarvesterClass.HX_V12:
            switch (this.Rarity)
            {
              case ItemRarity.RARE:
              case ItemRarity.ULTRA_RARE:
                return 2;
              case ItemRarity.LEGENDARY:
              case ItemRarity.PRECURSOR:
                return 1;
              case ItemRarity.ULTIMATE:
                return 0;
              default:
                return 3;
            }
          case HarvesterClass.WP_V1:
          case HarvesterClass.WP_V2:
          case HarvesterClass.WP_V3:
          case HarvesterClass.WP_V4:
          case HarvesterClass.WP_V5:
          case HarvesterClass.WP_V6:
          case HarvesterClass.WP_V7:
          case HarvesterClass.WP_V8:
          case HarvesterClass.WP_V9:
          case HarvesterClass.WP_V10:
          case HarvesterClass.WP_V11:
          case HarvesterClass.WP_V12:
            switch (this.Rarity)
            {
              case ItemRarity.RARE:
              case ItemRarity.ULTRA_RARE:
                return 3;
              case ItemRarity.LEGENDARY:
              case ItemRarity.PRECURSOR:
                return 2;
              case ItemRarity.ULTIMATE:
                return 1;
              default:
                return 4;
            }
          case HarvesterClass.DCD_V1:
          case HarvesterClass.DCD_V2:
          case HarvesterClass.DCD_V3:
          case HarvesterClass.DCD_V4:
          case HarvesterClass.DCD_V5:
          case HarvesterClass.DCD_V6:
          case HarvesterClass.DCD_V7:
          case HarvesterClass.DCD_V8:
          case HarvesterClass.DCD_V9:
          case HarvesterClass.DCD_V10:
          case HarvesterClass.DCD_V11:
          case HarvesterClass.DCD_V12:
            switch (this.Rarity)
            {
              case ItemRarity.RARE:
              case ItemRarity.ULTRA_RARE:
                return 4;
              case ItemRarity.LEGENDARY:
              case ItemRarity.PRECURSOR:
                return 3;
              case ItemRarity.ULTIMATE:
                return 2;
              default:
                return 5;
            }
          default:
            return 0;
        }
      }
    }

    public override int MaxCreditValue
    {
      get
      {
        int rarity = (int) this.Rarity;
        switch (this._class)
        {
          case HarvesterClass.LCD_V1:
          case HarvesterClass.LCD_V2:
          case HarvesterClass.LCD_V3:
          case HarvesterClass.LCD_V4:
          case HarvesterClass.LCD_V5:
          case HarvesterClass.LCD_V6:
          case HarvesterClass.LCD_V7:
          case HarvesterClass.LCD_V8:
          case HarvesterClass.LCD_V9:
          case HarvesterClass.LCD_V10:
          case HarvesterClass.LCD_V11:
          case HarvesterClass.LCD_V12:
            if (this.Rarity == ItemRarity.COMMON)
              return 1800;
            return rarity * rarity * rarity * 3600;
          case HarvesterClass.HX_V1:
          case HarvesterClass.HX_V2:
          case HarvesterClass.HX_V3:
          case HarvesterClass.HX_V4:
          case HarvesterClass.HX_V5:
          case HarvesterClass.HX_V6:
          case HarvesterClass.HX_V7:
          case HarvesterClass.HX_V8:
          case HarvesterClass.HX_V9:
          case HarvesterClass.HX_V10:
          case HarvesterClass.HX_V11:
          case HarvesterClass.HX_V12:
            if (this.Rarity == ItemRarity.COMMON)
              return 1900;
            return rarity * rarity * rarity * 3800;
          case HarvesterClass.WP_V1:
          case HarvesterClass.WP_V2:
          case HarvesterClass.WP_V3:
          case HarvesterClass.WP_V4:
          case HarvesterClass.WP_V5:
          case HarvesterClass.WP_V6:
          case HarvesterClass.WP_V7:
          case HarvesterClass.WP_V8:
          case HarvesterClass.WP_V9:
          case HarvesterClass.WP_V10:
          case HarvesterClass.WP_V11:
          case HarvesterClass.WP_V12:
            if (this.Rarity == ItemRarity.COMMON)
              return 2000;
            return rarity * rarity * rarity * 4000;
          case HarvesterClass.DCD_V1:
          case HarvesterClass.DCD_V2:
          case HarvesterClass.DCD_V3:
          case HarvesterClass.DCD_V4:
          case HarvesterClass.DCD_V5:
          case HarvesterClass.DCD_V6:
          case HarvesterClass.DCD_V7:
          case HarvesterClass.DCD_V8:
          case HarvesterClass.DCD_V9:
          case HarvesterClass.DCD_V10:
          case HarvesterClass.DCD_V11:
          case HarvesterClass.DCD_V12:
            if (this.Rarity == ItemRarity.COMMON)
              return 2200;
            return rarity * rarity * rarity * 4400;
          default:
            return int.MinValue;
        }
      }
    }

    public override string IconTag
    {
      get
      {
        return this._class.IconTag();
      }
    }

    public int HarvestSpeedMS
    {
      get
      {
        return 85000 - (int) this.Rarity * 5000;
      }
    }

    public int HarvestCapacity
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.LCD_V1:
          case HarvesterClass.LCD_V2:
          case HarvesterClass.LCD_V3:
          case HarvesterClass.LCD_V4:
          case HarvesterClass.LCD_V5:
          case HarvesterClass.LCD_V6:
          case HarvesterClass.LCD_V7:
          case HarvesterClass.LCD_V8:
          case HarvesterClass.LCD_V9:
          case HarvesterClass.LCD_V10:
          case HarvesterClass.LCD_V11:
          case HarvesterClass.LCD_V12:
            return 2;
          case HarvesterClass.HX_V1:
          case HarvesterClass.HX_V2:
          case HarvesterClass.HX_V3:
          case HarvesterClass.HX_V4:
          case HarvesterClass.HX_V5:
          case HarvesterClass.HX_V6:
          case HarvesterClass.HX_V7:
          case HarvesterClass.HX_V8:
          case HarvesterClass.HX_V9:
          case HarvesterClass.HX_V10:
          case HarvesterClass.HX_V11:
          case HarvesterClass.HX_V12:
            return 3;
          case HarvesterClass.WP_V1:
          case HarvesterClass.WP_V2:
          case HarvesterClass.WP_V3:
          case HarvesterClass.WP_V4:
          case HarvesterClass.WP_V5:
          case HarvesterClass.WP_V6:
          case HarvesterClass.WP_V7:
          case HarvesterClass.WP_V8:
          case HarvesterClass.WP_V9:
          case HarvesterClass.WP_V10:
          case HarvesterClass.WP_V11:
          case HarvesterClass.WP_V12:
            return 4;
          case HarvesterClass.DCD_V1:
          case HarvesterClass.DCD_V2:
          case HarvesterClass.DCD_V3:
          case HarvesterClass.DCD_V4:
          case HarvesterClass.DCD_V5:
          case HarvesterClass.DCD_V6:
          case HarvesterClass.DCD_V7:
          case HarvesterClass.DCD_V8:
          case HarvesterClass.DCD_V9:
          case HarvesterClass.DCD_V10:
          case HarvesterClass.DCD_V11:
          case HarvesterClass.DCD_V12:
            return 5;
          default:
            return 1;
        }
      }
    }

    public override int ResourceCapacity
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V3:
            return (int) this.Rarity * 2;
          case HarvesterClass.DCD_V3:
            return (int) ((double) this.Rarity * 2.5 + 0.5);
          case HarvesterClass.LCD_V3:
            return (int) this.Rarity;
          case HarvesterClass.HX_V3:
            return (int) ((double) this.Rarity * 1.5 + 0.5);
          default:
            return 0;
        }
      }
    }

    public override int MoveSpeedMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.DCD_V5:
            return (int) this.Rarity * -300;
          case HarvesterClass.DCD_V9:
            return (int) this.Rarity * -145;
          case HarvesterClass.WP_V5:
            return (int) this.Rarity * -250;
          case HarvesterClass.WP_V9:
            return (int) this.Rarity * -115;
          case HarvesterClass.HX_V5:
            return (int) this.Rarity * -200;
          case HarvesterClass.HX_V9:
            return (int) this.Rarity * -75;
          case HarvesterClass.LCD_V5:
            return (int) this.Rarity * -150;
          case HarvesterClass.LCD_V9:
            return (int) this.Rarity * -50;
          default:
            return 0;
        }
      }
    }

    public override float MetalRepairMod
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.DCD_V3:
            return (float) this.Rarity * 0.013f;
          case HarvesterClass.DCD_V6:
            return (float) this.Rarity * 0.0286f;
          case HarvesterClass.WP_V3:
            return (float) this.Rarity * 0.01f;
          case HarvesterClass.WP_V6:
            return (float) this.Rarity * 0.02f;
          case HarvesterClass.HX_V3:
            return (float) this.Rarity * (3f / 500f);
          case HarvesterClass.HX_V6:
            return (float) this.Rarity * 0.015f;
          case HarvesterClass.LCD_V3:
            return (float) this.Rarity * 0.004f;
          case HarvesterClass.LCD_V6:
            return (float) this.Rarity * 0.01f;
          default:
            return 0.0f;
        }
      }
    }

    public override float OutgoingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V10:
            return (float) this.Rarity / 93f;
          case HarvesterClass.DCD_V10:
            return (float) this.Rarity / 76f;
          case HarvesterClass.LCD_V10:
            return (float) this.Rarity / 200f;
          case HarvesterClass.HX_V10:
            return (float) this.Rarity / 130f;
          default:
            return 0.0f;
        }
      }
    }

    public override float XpMod
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.LCD_V1:
            return (float) (0.00999999977648258 + (double) this.Rarity * 0.00999999977648258);
          case HarvesterClass.HX_V1:
            return (float) (0.0199999995529652 + (double) this.Rarity * 0.00999999977648258);
          case HarvesterClass.WP_V1:
            return (float) (0.0299999993294477 + (double) this.Rarity * 0.00999999977648258);
          case HarvesterClass.DCD_V1:
            return (float) (0.0399999991059303 + (double) this.Rarity * 0.00999999977648258);
          default:
            return 0.0f;
        }
      }
    }

    public override int MaxHullBonus
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V4:
            return (int) this.Rarity * 100;
          case HarvesterClass.DCD_V4:
            return (int) this.Rarity * 143;
          case HarvesterClass.LCD_V4:
            return (int) this.Rarity * 75;
          case HarvesterClass.HX_V4:
            return (int) this.Rarity * 85;
          default:
            return 0;
        }
      }
    }

    public override float CriticalDamageMod
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V7:
            return (float) this.Rarity * 0.025f;
          case HarvesterClass.DCD_V7:
            return (float) this.Rarity * 0.035f;
          case HarvesterClass.LCD_V7:
            return (float) this.Rarity * 0.015f;
          case HarvesterClass.HX_V7:
            return (float) this.Rarity * 0.02f;
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalChance
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V2:
            return (float) this.Rarity * 2f;
          case HarvesterClass.DCD_V2:
            return (float) this.Rarity * 2.5f;
          case HarvesterClass.LCD_V2:
            return (float) this.Rarity;
          case HarvesterClass.HX_V2:
            return (float) this.Rarity * 1.5f;
          default:
            return 0.0f;
        }
      }
    }

    public override float EvasionChance
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.HX_V9:
            return (float) this.Rarity / 1.6f;
          case HarvesterClass.WP_V9:
            return (float) this.Rarity / 1.2f;
          case HarvesterClass.DCD_V9:
            return (float) this.Rarity;
          case HarvesterClass.LCD_V1:
            return (float) this.Rarity;
          case HarvesterClass.HX_V1:
            return (float) this.Rarity * 1.5f;
          case HarvesterClass.WP_V1:
            return (float) this.Rarity * 2f;
          case HarvesterClass.DCD_V1:
            return (float) this.Rarity * 2.5f;
          case HarvesterClass.LCD_V9:
            return (float) this.Rarity / 2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashChance
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V9:
            return (float) this.Rarity * 2.5f;
          case HarvesterClass.DCD_V9:
            return (float) this.Rarity * 3f;
          case HarvesterClass.LCD_V9:
            return (float) this.Rarity * 1.5f;
          case HarvesterClass.HX_V9:
            return (float) this.Rarity * 2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleChance
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V12:
            return (float) this.Rarity * 2f;
          case HarvesterClass.DCD_V12:
            return (float) this.Rarity * 2.75f;
          case HarvesterClass.LCD_V12:
            return (float) this.Rarity;
          case HarvesterClass.HX_V12:
            return (float) this.Rarity * 1.5f;
          default:
            return 0.0f;
        }
      }
    }

    public override float StunChance
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V11:
            return (float) this.Rarity / 1.1f;
          case HarvesterClass.DCD_V11:
            return (float) this.Rarity * 1.2f;
          case HarvesterClass.LCD_V11:
            return (float) this.Rarity / 1.3f;
          case HarvesterClass.HX_V11:
            return (float) this.Rarity / 1.2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float HitChance
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V8:
            return (float) this.Rarity * 2.6f;
          case HarvesterClass.DCD_V8:
            return (float) this.Rarity * 3.3f;
          case HarvesterClass.LCD_V8:
            return (float) this.Rarity * 1.5f;
          case HarvesterClass.HX_V8:
            return (float) this.Rarity * 2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float StunResist
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.DCD_V2:
            return (float) (11.0 + (double) this.Rarity * 1.5);
          case HarvesterClass.DCD_V6:
          case HarvesterClass.DCD_V10:
            return 11f + (float) this.Rarity;
          case HarvesterClass.HX_V12:
          case HarvesterClass.HX_V8:
            return 6f + (float) this.Rarity;
          case HarvesterClass.WP_V5:
          case HarvesterClass.WP_V9:
            return 8f + (float) this.Rarity;
          case HarvesterClass.LCD_V11:
          case HarvesterClass.LCD_V3:
          case HarvesterClass.LCD_V7:
            return 5f + (float) this.Rarity;
          case HarvesterClass.HX_V4:
            return (float) (6.0 + (double) this.Rarity * 1.5);
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleResist
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.DCD_V3:
          case HarvesterClass.DCD_V7:
          case HarvesterClass.DCD_V11:
            return 11f + (float) this.Rarity;
          case HarvesterClass.WP_V2:
          case HarvesterClass.WP_V6:
          case HarvesterClass.WP_V10:
            return 8f + (float) this.Rarity;
          case HarvesterClass.LCD_V12:
          case HarvesterClass.LCD_V8:
            return 5f + (float) this.Rarity;
          case HarvesterClass.HX_V5:
          case HarvesterClass.HX_V9:
            return 6f + (float) this.Rarity;
          case HarvesterClass.LCD_V4:
            return (float) (5.0 + (double) this.Rarity * 1.5);
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalResist
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.DCD_V4:
            return (float) (11.0 + (double) this.Rarity * 2.0);
          case HarvesterClass.DCD_V8:
          case HarvesterClass.DCD_V12:
            return 11f + (float) this.Rarity;
          case HarvesterClass.WP_V3:
          case HarvesterClass.WP_V7:
          case HarvesterClass.WP_V11:
            return 8f + (float) this.Rarity;
          case HarvesterClass.HX_V2:
          case HarvesterClass.HX_V6:
          case HarvesterClass.HX_V10:
            return 6f + (float) this.Rarity;
          case HarvesterClass.LCD_V5:
          case HarvesterClass.LCD_V9:
            return 5f + (float) this.Rarity;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashResist
    {
      get
      {
        switch (this._class)
        {
          case HarvesterClass.WP_V12:
          case HarvesterClass.WP_V8:
            return 8f + (float) this.Rarity;
          case HarvesterClass.DCD_V5:
            return (float) (11.0 + (double) this.Rarity * 2.0);
          case HarvesterClass.DCD_V9:
            return 11f + (float) this.Rarity;
          case HarvesterClass.HX_V11:
          case HarvesterClass.HX_V3:
          case HarvesterClass.HX_V7:
            return 6f + (float) this.Rarity;
          case HarvesterClass.WP_V4:
            return (float) (8.0 + (double) this.Rarity * 2.0);
          case HarvesterClass.LCD_V10:
          case HarvesterClass.LCD_V2:
          case HarvesterClass.LCD_V6:
            return 5f + (float) this.Rarity;
          default:
            return 0.0f;
        }
      }
    }

    public HarvesterClass Class
    {
      get
      {
        return this._class;
      }
    }

    protected internal HarvesterItem(HarvesterClass harvesterClass, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(rarity, durability, noDrop, bindOnEquip)
    {
      this._class = harvesterClass;
    }

    public override void Write(IByteBuffer output)
    {
      base.Write(output);
      output.Write((sbyte) this._class);
    }

    public override void AppendName(StringBuilder sb)
    {
      sb.Append(this._class.Name());
    }

    public override EquipmentItem GetUpgrade(EngineeringType type)
    {
      return (EquipmentItem) HarvesterCache.GetUpgrade(type, this);
    }

    public override bool CanUpgrade(EngineeringType type)
    {
      if (this.Rarity <= ItemRarity.COMMON)
        return false;
      if (type <= EngineeringType.REPAIR)
      {
        if (type == EngineeringType.DECONSTRUCT)
          return true;
        if (type == EngineeringType.REPAIR && (int) this.Durability > 1)
          return (int) this.Durability < 100;
      }
      else
      {
        if (type == EngineeringType.RARITY)
          return this.Rarity < ItemRarity.ULTIMATE;
        if (type == EngineeringType.UNBIND)
          return this.NoDrop;
      }
      return false;
    }

    public override void AppendSubTitle(StringBuilder sb)
    {
      sb.Append(this.Rarity.Name());
      sb.Append(" Harvester");
    }

    public override void AppendDescription(StringBuilder sb)
    {
      sb.Append(this.HarvestCapacity);
      sb.Append(" Harvest Capacity\n");
      sb.Append(this.HarvestSpeedMS / 1000);
      sb.Append("s Harvest Speed");
    }
  }
}
