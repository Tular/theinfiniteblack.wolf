﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.EngineClass
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public enum EngineClass
  {
    NULL = -128,
    GRAVITY = 0,
    SKIP = 1,
    FISSION = 2,
    IMPULSE = 3,
    FUSION = 4,
    NEUTRON = 5,
    STEALTH = 6,
  }
}
