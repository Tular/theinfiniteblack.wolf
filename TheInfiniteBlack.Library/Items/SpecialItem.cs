﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.SpecialItem
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Items
{
  public class SpecialItem : EquipmentItem
  {
    private readonly SpecialClass _class;
    private readonly ShipClass _requiredShip;

    public override ItemType Type
    {
      get
      {
        return ItemType.SPECIAL;
      }
    }

    public override int MaxCreditValue
    {
      get
      {
        if (this.Rarity == ItemRarity.COMMON)
          return 2900;
        int rarity = (int) this.Rarity;
        return rarity * rarity * rarity * 5800;
      }
    }

    public override int EPCost
    {
      get
      {
        return 0;
      }
    }

    public override string IconTag
    {
      get
      {
        return this._class.IconTag();
      }
    }

    public override float MetalRepairMod
    {
      get
      {
        if (this._class != SpecialClass.TECHNICIAN)
          return 0.0f;
        return (float) (0.800000011920929 + ((double) this.Rarity - 1.0) * 0.100000001490116);
      }
    }

    public override float OutgoingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.TECHNICIAN:
            return (float) (((double) this.Rarity - 1.0) * 0.0500000007450581 - 0.800000011920929);
          case SpecialClass.TANK:
            return (float) (((double) this.Rarity - 1.0) * 0.0500000007450581 - 0.800000011920929);
          case SpecialClass.NOVA:
            return (float) this.Rarity * 0.02f;
          case SpecialClass.ADVANCED_MUNITIONS:
            return (float) this.Rarity * 0.017f;
          default:
            return 0.0f;
        }
      }
    }

    public override float IncomingDamageMod
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.TANK:
            return (float) (-0.5 - ((double) this.Rarity - 1.0) * 0.0199999995529652);
          case SpecialClass.ADVANCED_SHIELDS:
            return (float) this.Rarity * -0.015f;
          default:
            return 0.0f;
        }
      }
    }

    public override int MoveSpeedMSAdjust
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.SCOUT:
            return (int) this.Rarity * -250;
          case SpecialClass.ADVANCED_PROPULSION:
            return (int) this.Rarity * -143;
          default:
            return 0;
        }
      }
    }

    public override int EPBonus
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.TANK:
            return this.Rarity != ItemRarity.ULTIMATE ? 0 : 1;
          case SpecialClass.BATTLE_RAM:
            if (this.Rarity <= ItemRarity.ULTRA_RARE)
              return 0;
            if (this.Rarity <= ItemRarity.LEGENDARY)
              return 1;
            return this.Rarity > ItemRarity.PRECURSOR ? 3 : 2;
          case SpecialClass.ALIEN_HUNTER:
          case SpecialClass.BOUNTY_HUNTER:
          case SpecialClass.DEFLECTOR:
            if (this.Rarity <= ItemRarity.LEGENDARY)
              return 0;
            return this.Rarity > ItemRarity.PRECURSOR ? 2 : 1;
          case SpecialClass.ADVANCED_CONSTRUCT:
            if (this.Rarity <= ItemRarity.LEGENDARY)
              return 2;
            return this.Rarity > ItemRarity.PRECURSOR ? 4 : 3;
          case SpecialClass.ADVANCED_SHIELDS:
          case SpecialClass.ADVANCED_PROPULSION:
            if (this.Rarity <= ItemRarity.RARE)
              return 0;
            return this.Rarity > ItemRarity.PRECURSOR ? 2 : 1;
          case SpecialClass.ADVANCED_ELECTRONICS:
            if (this.Rarity <= ItemRarity.ULTRA_RARE)
              return 0;
            return this.Rarity > ItemRarity.PRECURSOR ? 2 : 1;
          case SpecialClass.ADVANCED_MUNITIONS:
            if (this.Rarity <= ItemRarity.ULTRA_RARE)
              return 0;
            return this.Rarity > ItemRarity.PRECURSOR ? 2 : 1;
          default:
            return 0;
        }
      }
    }

    public override int ResourceCapacity
    {
      get
      {
        if (this._class != SpecialClass.ADVANCED_CONSTRUCT)
          return 0;
        return (int) this.Rarity * 3;
      }
    }

    public override float CriticalChance
    {
      get
      {
        if (this._class != SpecialClass.ADVANCED_ELECTRONICS)
          return 0.0f;
        return (float) this.Rarity * 1.5f;
      }
    }

    public override float EvasionChance
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.SCOUT:
            return (float) this.Rarity * 2f;
          case SpecialClass.ADVANCED_SHIELDS:
          case SpecialClass.ADVANCED_PROPULSION:
            return (float) this.Rarity;
          default:
            return 0.0f;
        }
      }
    }

    public override float HitChance
    {
      get
      {
        if (this._class != SpecialClass.ADVANCED_ELECTRONICS)
          return 0.0f;
        return (float) this.Rarity * 1.5f;
      }
    }

    public override int MaxHullBonus
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.ADVANCED_CONSTRUCT:
            return 143 * (int) this.Rarity;
          case SpecialClass.ADVANCED_SHIELDS:
            return 50 * (int) this.Rarity;
          default:
            return 0;
        }
      }
    }

    public override float SplashChance
    {
      get
      {
        if (this._class != SpecialClass.ADVANCED_MUNITIONS)
          return 0.0f;
        return (float) this.Rarity * 2.5f;
      }
    }

    public override float StunChance
    {
      get
      {
        if (this._class != SpecialClass.TRACTOR)
          return 0.0f;
        return (float) (2.0 + (double) this.Rarity * 3.0);
      }
    }

    public override int StunDurationMSAdjust
    {
      get
      {
        if (this._class != SpecialClass.TRACTOR)
          return 0;
        return 1000 + (int) this.Rarity * 750;
      }
    }

    public override float GrappleChance
    {
      get
      {
        if (this._class != SpecialClass.GRAPPLING_HOOK)
          return 0.0f;
        return (float) this.Rarity * 7f;
      }
    }

    public override float XpMod
    {
      get
      {
        if (this._class != SpecialClass.ADVANCED_CONSTRUCT)
          return 0.0f;
        return (float) this.Rarity * 0.01f;
      }
    }

    public override float CriticalDamageMod
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.ADVANCED_ELECTRONICS:
            return (float) this.Rarity * 0.03f;
          case SpecialClass.ADVANCED_MUNITIONS:
            return (float) this.Rarity * 0.06f;
          default:
            return 0.0f;
        }
      }
    }

    public override float StunResist
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.SCOUT:
            return (float) this.Rarity * 2f;
          case SpecialClass.ADVANCED_SHIELDS:
            return (float) this.Rarity * 2.5f;
          case SpecialClass.ADVANCED_PROPULSION:
            return (float) this.Rarity * 7.2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float GrappleResist
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.TANK:
            if (this.Rarity <= ItemRarity.LEGENDARY)
              return 0.0f;
            return this.Rarity > ItemRarity.PRECURSOR ? 30f : 15f;
          case SpecialClass.DEFLECTOR:
            return (float) this.Rarity;
          case SpecialClass.GRAPPLING_HOOK:
            return (float) this.Rarity * 2f;
          case SpecialClass.ADVANCED_SHIELDS:
            return (float) this.Rarity * 2.5f;
          case SpecialClass.ADVANCED_PROPULSION:
            return (float) this.Rarity * 7.2f;
          default:
            return 0.0f;
        }
      }
    }

    public override float CriticalResist
    {
      get
      {
        switch (this._class)
        {
          case SpecialClass.ADVANCED_SHIELDS:
            return (float) this.Rarity * 2.5f;
          case SpecialClass.ADVANCED_ELECTRONICS:
            return (float) this.Rarity * 3f;
          default:
            return 0.0f;
        }
      }
    }

    public override float SplashResist
    {
      get
      {
        if (this._class != SpecialClass.ADVANCED_SHIELDS)
          return 0.0f;
        return (float) this.Rarity * 2.5f;
      }
    }

    public override bool CanBeRepaired
    {
      get
      {
        if (base.CanBeRepaired)
          return this._class != SpecialClass.WEIRD_ALIEN_ARTIFACT;
        return false;
      }
    }

    public SpecialClass Class
    {
      get
      {
        return this._class;
      }
    }

    public ShipClass RequiredShip
    {
      get
      {
        return this._requiredShip;
      }
    }

    protected internal SpecialItem(SpecialClass specialClass, ShipClass requiredShipClass, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(rarity, durability, noDrop, bindOnEquip)
    {
      this._class = specialClass;
      this._requiredShip = requiredShipClass;
    }

    public override void Write(IByteBuffer output)
    {
      base.Write(output);
      output.Write((sbyte) this._class);
      output.Write((sbyte) this._requiredShip);
    }

    public bool WorksInShip(ShipClass shipClass)
    {
      if (this._class != SpecialClass.WEIRD_ALIEN_ARTIFACT)
        return this._requiredShip.CanUseEquipment(shipClass);
      return true;
    }

    public override void AppendName(StringBuilder sb)
    {
      sb.Append(this._class.Name());
    }

    public override EquipmentItem GetUpgrade(EngineeringType type)
    {
      return (EquipmentItem) SpecialCache.GetUpgrade(type, this);
    }

    public override bool CanUpgrade(EngineeringType type)
    {
      if (this._class == SpecialClass.WEIRD_ALIEN_ARTIFACT || this.Rarity <= ItemRarity.COMMON)
        return false;
      switch (type)
      {
        case EngineeringType.DECONSTRUCT:
          return true;
        case EngineeringType.REPAIR:
          if ((int) this.Durability > 1)
            return (int) this.Durability < 100;
          return false;
        case EngineeringType.CLASS_UP:
          return this._requiredShip < ShipClass.Annihilator;
        case EngineeringType.CLASS_DOWN:
          return this._requiredShip > ShipClass.Shuttle;
        case EngineeringType.RARITY:
          return this.Rarity < ItemRarity.ULTIMATE;
        case EngineeringType.UNBIND:
          return this.NoDrop;
        default:
          return false;
      }
    }

    public override void AppendSubTitle(StringBuilder sb)
    {
      sb.Append(this.Rarity.Name());
      sb.Append(" Special");
    }

    public override void AppendDescription(StringBuilder sb)
    {
      if (this._class != SpecialClass.WEIRD_ALIEN_ARTIFACT)
      {
        sb.Append(this._requiredShip.Name());
        sb.Append("-Class Ships");
        ShipClass wyrdEquiv = this._requiredShip.GetWyrdEquiv();
        if (wyrdEquiv != ShipClass.None)
        {
          sb.Append("\n");
          sb.Append(wyrdEquiv.Name());
          sb.Append(" Ships");
        }
      }
      switch (this._class)
      {
        case SpecialClass.TECHNICIAN:
          sb.Append("\nFree GRAPPLED ships on repair");
          break;
        case SpecialClass.PROSPECTOR:
          sb.Append("\n");
          sb.Append(2 + (int) this.Rarity * 4);
          sb.Append("% chance to not deplete asteroids\n");
          sb.Append(5 * (int) this.Rarity);
          sb.Append("% chance of resource bonus from kills\n");
          sb.Append((int) this.Rarity);
          sb.Append("x experience bonus from harvesting");
          break;
        case SpecialClass.SCOUT:
          sb.Append("\nDetects moving ships nearby\nReveals stealth movement direction");
          break;
        case SpecialClass.NOVA:
          sb.Append("\nDamage killers for ");
          sb.Append(40 + (int) this.Rarity * 5);
          sb.Append("% of their max hull\nImmune to Nova Device damage");
          break;
        case SpecialClass.BATTLE_RAM:
          sb.Append("\n");
          sb.Append((int) this.Rarity * 5);
          sb.Append("% chance to RAM hostiles when moving");
          break;
        case SpecialClass.ALIEN_HUNTER:
          switch (this.Rarity)
          {
            case ItemRarity.RARE:
              sb.Append("\nResource and loot drop bonus\n+");
              break;
            case ItemRarity.ULTRA_RARE:
              sb.Append("\nEnhanced resource and loot drop bonus\n+");
              break;
            case ItemRarity.LEGENDARY:
              sb.Append("\nMajor resource and loot drop bonus\n+");
              break;
            case ItemRarity.PRECURSOR:
              sb.Append("\nSuperior resource and loot drop bonus\n+");
              break;
            case ItemRarity.ULTIMATE:
              sb.Append("\nUltimate resource and loot drop bonus\n+");
              break;
            default:
              sb.Append("\nMinor Resource and loot drop bonus\n+");
              break;
          }
          sb.Append((int) this.Rarity * 2);
          sb.Append("% damage versus aliens");
          break;
        case SpecialClass.BOUNTY_HUNTER:
          sb.Append("\n+");
          sb.Append((int) this.Rarity * 2);
          sb.Append("% damage versus Players and Pirates\n+XP for killing players\nRepairs your items on player kill");
          break;
        case SpecialClass.DEFLECTOR:
          sb.Append("\nSTUN deflected back at attackers\n");
          sb.Append((int) this.Rarity * 3);
          sb.Append("% damage deflected back at attackers");
          break;
        case SpecialClass.WEIRD_ALIEN_ARTIFACT:
          sb.Append("\nIt emits a deep hum...\n...Curious.");
          break;
      }
    }
  }
}
