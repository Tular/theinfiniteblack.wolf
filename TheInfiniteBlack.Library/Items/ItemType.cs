﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Items.ItemType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Items
{
  public enum ItemType
  {
    NULL = -128,
    WEAPON = 1,
    ARMOR = 2,
    STORAGE = 3,
    HARVESTER = 4,
    ENGINE = 5,
    COMPUTER = 6,
    SPECIAL = 7,
  }
}
