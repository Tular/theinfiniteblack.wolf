﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.ResourceType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum ResourceType
  {
    NULL = -128,
    ORGANIC = 0,
    GAS = 1,
    METAL = 2,
    RADIOACTIVE = 3,
    DARKMATTER = 4,
  }
}
