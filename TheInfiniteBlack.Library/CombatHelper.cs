﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.CombatHelper
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public static class CombatHelper
  {
    public static int getPlayerDPS(int weaponEP)
    {
      return 10 + weaponEP;
    }

    public static int getPlayerMinDamage(int weaponEP, int weaponSpeedMS)
    {
      int num1 = CombatHelper.getPlayerDPS(weaponEP) * weaponSpeedMS;
      int num2 = 3;
      int num3 = num1 / num2;
      return (num1 - num3) / 1000;
    }

    public static int getNpcDPS(int npcEP)
    {
      return 3 + npcEP;
    }

    public static int getNpcMinDamage(int npcEP, int weaponSpeedMS)
    {
      int num1 = CombatHelper.getNpcDPS(npcEP) * weaponSpeedMS;
      int num2 = 3;
      int num3 = num1 / num2;
      return (num1 - num3) / 1000;
    }

    public static double CalculateDPS(int minDmg, int speed, double hit, double critDmg, double crit, double eva, double critRes)
    {
      double num1 = (double) minDmg * 1.5;
      double num2 = (100.0 - hit) / 100.0 * (double) speed;
      if (num2 < 1.0)
        num2 = 1.0;
      double num3 = num1 * (1.0 + critDmg / 100.0);
      double num4 = (hit + 100.0) / (100.0 + hit + eva);
      double num5 = 1.0 - num4;
      double num6 = num4 * (crit / (100.0 + crit + critRes));
      double num7 = num4 - num6;
      double num8 = num7 * ((100.0 + hit) / (100.0 + hit + eva));
      double num9 = (num7 - num8) * 0.5 * num1 + num8 * num1 + num6 * num3;
      double num10 = num9;
      double num11 = num2;
      double num12 = num5;
      while (num11 < (double) speed)
      {
        double num13 = num12 * num4;
        num9 += num13 * (((double) speed - num11) / (double) speed) * num10;
        num12 -= num13;
        num11 += num2;
      }
      return num9 / (double) speed;
    }
  }
}
