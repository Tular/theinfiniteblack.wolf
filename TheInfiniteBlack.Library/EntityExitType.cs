﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.EntityExitType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum EntityExitType
  {
    NULL = -128,
    SILENT = 0,
    DESTROYED = 1,
    MOVED = 2,
    EARTH_JUMP = 3,
    CORP_JUMP = 4,
    RIFT_JUMP = 5,
    LOGOUT = 6,
    DRAGGED = 7,
  }
}
