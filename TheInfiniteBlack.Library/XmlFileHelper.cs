﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.XmlFileHelper
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library
{
  public static class XmlFileHelper
  {
    private static readonly XmlWriterSettings _settings = new XmlWriterSettings()
    {
      OmitXmlDeclaration = true,
      CloseOutput = true,
      Indent = true,
      NewLineOnAttributes = true,
      IndentChars = " "
    };

    public static void Save<T>(IFileManager files, string fileName, T obj) where T : class
    {
      try
      {
        string xml = XmlFileHelper.GetXml<T>(obj);
        using (FileStream fileStream = files.Get(fileName, FileMode.Create))
        {
          using (StreamWriter streamWriter = new StreamWriter((Stream) fileStream))
            streamWriter.Write(xml);
        }
      }
      catch (Exception ex)
      {
        Log.E((object) files, "Save<T>", ex);
      }
    }

    public static string GetXml<T>(T obj) where T : class
    {
      StringBuilder output = new StringBuilder(10000);
      using (XmlWriter xmlWriter1 = XmlWriter.Create(output, XmlFileHelper._settings))
      {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof (T));
        XmlSerializerNamespaces serializerNamespaces = new XmlSerializerNamespaces();
        serializerNamespaces.Add(string.Empty, string.Empty);
        XmlWriter xmlWriter2 = xmlWriter1;
        T obj1 = obj;
        XmlSerializerNamespaces namespaces = serializerNamespaces;
        xmlSerializer.Serialize(xmlWriter2, (object) obj1, namespaces);
      }
      return output.ToString();
    }

    public static T Load<T>(string xml) where T : class
    {
      using (TextReader textReader = (TextReader) new StringReader(xml))
        return (T) new XmlSerializer(typeof (T)).Deserialize(textReader);
    }

    public static T Load<T>(IFileManager files, string fileName) where T : class
    {
      try
      {
        using (FileStream fileStream = files.Get(fileName, FileMode.Open))
        {
          using (StreamReader streamReader = new StreamReader((Stream) fileStream))
            return XmlFileHelper.Load<T>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
        Log.E((object) files, "Load<T>", ex);
      }
      return default (T);
    }
  }
}
