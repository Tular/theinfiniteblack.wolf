﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.TcpClientWithTimeout
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Net.Sockets;
using System.Threading;

namespace TheInfiniteBlack.Library.Network
{
  public class TcpClientWithTimeout
  {
    private readonly string _hostname;
    private readonly int _port;
    private readonly int _timeoutMS;
    private TcpClient _connection;
    private bool _connected;
    private Exception _exception;

    public TcpClient Connection
    {
      get
      {
        return this._connection;
      }
    }

    public bool Connected
    {
      get
      {
        return this._connected;
      }
    }

    public Exception Exception
    {
      get
      {
        return this._exception;
      }
    }

    public TcpClientWithTimeout(string hostname, int port, int timeoutMS = 5000)
    {
      this._hostname = hostname;
      this._port = port;
      this._timeoutMS = timeoutMS;
    }

    public TcpClient Connect()
    {
      this._connected = false;
      this._exception = (Exception) null;
      Thread thread = new Thread(new ThreadStart(this.BeginConnect))
      {
        IsBackground = true
      };
      thread.Start();
      thread.Join(this._timeoutMS);
      if (this._connected)
      {
        thread.Abort();
        return this._connection;
      }
      if (this._exception != null)
      {
        thread.Abort();
        throw this._exception;
      }
      thread.Abort();
      throw new TimeoutException(string.Format("TcpClient connection to {0}:{1} timed out", (object) this._hostname, (object) this._port));
    }

    protected void BeginConnect()
    {
      try
      {
        this._connection = new TcpClient(this._hostname, this._port);
        this._connected = true;
      }
      catch (Exception ex)
      {
        this._exception = ex;
      }
    }
  }
}
