﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.Command
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public abstract class Command
  {
    private static readonly AliveCommand _alive = new AliveCommand();
    public const sbyte ALIVE = -122;
    public const sbyte CHAT_MESSAGE = -66;
    public const sbyte HANDSHAKE = -124;
    public const sbyte NOTICE = -120;
    public const sbyte REQUEST_RESPONSE = -119;
    public const sbyte UPDATE_PLAYER = -118;
    public const sbyte UPDATE_CORPORATION = -117;
    public const sbyte UPDATE_CURRENT_SECTOR = -113;
    public const sbyte UPDATE_PLAYER_STATS = -109;
    public const sbyte UPDATE_PLAYER_LOAD = -108;
    public const sbyte UPDATE_PLAYER_COOLDOWNS = -107;
    public const sbyte UPDATE_PLAYER_BUYABLES = -106;
    public const sbyte UPDATE_PLAYER_CREDITS = -105;
    public const sbyte UPDATE_PLAYER_LOCATION = -104;
    public const sbyte UPDATE_PLAYER_STATUS = -103;
    public const sbyte UPDATE_PLAYER_BANK = -102;
    public const sbyte UPDATE_PLAYER_SHIPS = -101;
    public const sbyte UPDATE_ENTITY_INVENTORY = -100;
    public const sbyte UPDATE_SHIP_RESOURCES = -99;
    public const sbyte UPDATE_SECTOR = -98;
    public const sbyte UPDATE_ALLIANCE_STATUS = -97;
    public const sbyte UPDATE_CORP_STATUS = -96;
    public const sbyte EVENT_CORP_INVITE = -88;
    public const sbyte EVENT_ALLIANCE_INVITE = -87;
    public const sbyte REMOVE_ENTITY = -86;
    public const sbyte UPDATE_ENTITY = -85;
    public const sbyte HARVEST_EVENT = -84;
    public const sbyte ATTACK_EVENT = -83;
    public const sbyte SET_FOLLOW = -82;
    public const sbyte GAIN_XP_EVENT = -78;
    public const sbyte CHANGE_MONEY_EVENT = -77;
    public const sbyte CHANGE_PVP_FLAG_EVENT = -76;
    public const sbyte GAIN_ITEM_EVENT = -75;
    public const sbyte AH_ITEM = 42;
    public const sbyte AH_ITEM_REMOVE = 43;
    public const sbyte REQUEST_TRADE = -40;

    public abstract void Execute(GameState state);

    public static Command Create(IByteBuffer input)
    {
      switch (input.Get())
      {
        case -124:
          return (Command) new HandshakeCommand(input);
        case -122:
          return (Command) Command._alive;
        case -120:
          return (Command) new ShowNoticeCommand(input);
        case -119:
          return (Command) new ShowRequestResponse(input);
        case -118:
          return (Command) new UpdatePlayer(input);
        case -117:
          return (Command) new UpdateCorporation(input);
        case -113:
          return (Command) new UpdateCurrentSector(input);
        case -109:
          return (Command) new UpdatePlayerStats(input);
        case -108:
          return (Command) new UpdatePlayerLoad(input);
        case -107:
          return (Command) new UpdatePlayerCooldowns(input);
        case -106:
          return (Command) new UpdatePlayerBuyables(input);
        case -105:
          return (Command) new UpdatePlayerCredits(input);
        case -104:
          return (Command) new UpdatePlayerLocation(input);
        case -103:
          return (Command) new UpdatePlayerStatus(input);
        case -102:
          return (Command) new UpdatePlayerBank(input);
        case -101:
          return (Command) new UpdatePlayerShips(input);
        case -100:
          return (Command) new UpdateEntityInventory(input);
        case -99:
          return (Command) new SetResourcesCommand(input);
        case -98:
          return (Command) new UpdateSector(input);
        case -97:
          return (Command) new UpdateAllianceStatus(input);
        case -96:
          return (Command) new UpdateCorpStatus(input);
        case -88:
          return (Command) new ShowCorpInviteCommand(input);
        case -87:
          return (Command) new ShowAllianceInviteCommand(input);
        case -86:
          return (Command) new RemoveEntityCommand(input);
        case -85:
          return (Command) new UpdateEntity(input);
        case -84:
          return (Command) new ShowHarvestCommand(input);
        case -83:
          return (Command) new ShowAttackCommand(input);
        case -82:
          return (Command) new SetFollowCommand(input);
        case -78:
          return (Command) new ShowGainXpCommand(input);
        case -77:
          return (Command) new ShowMoneyEventCommand(input);
        case -76:
          return (Command) new SetPvPFlagCommand(input);
        case -75:
          return (Command) new ShowGainItemCommand(input);
        case -66:
          return (Command) new ShowChatMessageCommand(input);
        case -40:
          return (Command) new ShowTradeCommand(input);
        case 42:
          return (Command) new CreateAuctionCommand(input);
        case 43:
          return (Command) new RemoveAuctionCommand(input);
        default:
          return (Command) null;
      }
    }
  }
}
