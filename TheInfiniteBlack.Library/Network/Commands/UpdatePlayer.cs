﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayer
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayer : Command
  {
    private readonly string _name;
    private readonly int _id;
    private readonly bool _hardcore;
    private readonly sbyte _shipSkin;

    protected internal UpdatePlayer(IByteBuffer input)
    {
      this._name = input.GetString();
      this._id = input.GetInt();
      this._hardcore = input.GetBool();
      this._shipSkin = input.Get();
      int num1 = (int) input.Get();
      int num2 = (int) input.Get();
      int num3 = (int) input.Get();
      int num4 = (int) input.Get();
      int num5 = (int) input.Get();
      int num6 = (int) input.Get();
      int num7 = (int) input.Get();
    }

    public override void Execute(GameState state)
    {
      ClientPlayer clientPlayer = state.Players.Get(this._id);
      string name = this._name;
      clientPlayer.SetName(name);
      int num = this._hardcore ? 1 : 0;
      clientPlayer.Hardcore = num != 0;
      int shipSkin = (int) this._shipSkin;
      clientPlayer.ShipSkin = (sbyte) shipSkin;
    }
  }
}
