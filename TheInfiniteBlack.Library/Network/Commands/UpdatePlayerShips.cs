﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerShips
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerShips : Command
  {
    private readonly bool[] _bank = new bool[19];

    protected internal UpdatePlayerShips(IByteBuffer input)
    {
      for (int index = 0; index < this._bank.Length; ++index)
        this._bank[index] = (int) input.Get() == 0;
    }

    public override void Execute(GameState state)
    {
      state.Ships.Set(this._bank);
    }
  }
}
