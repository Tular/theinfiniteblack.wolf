﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdateEntity
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdateEntity : Command
  {
    private readonly IByteBuffer _input;

    protected internal UpdateEntity(IByteBuffer input)
    {
      this._input = input;
    }

    public override void Execute(GameState state)
    {
      Entity entity = Entity.Execute((IGameState) state, this._input);
      state.Local.Add(entity);
      switch (entity.Type)
      {
        case EntityType.DEFENSE_PLATFORM:
        case EntityType.INTRADICTOR:
          Garrison garrison1 = state.Local.Garrison;
          if (garrison1 == null)
            break;
          ((DefenseEntity) entity).Level = garrison1.Level;
          break;
        case EntityType.GARRISON:
          Garrison garrison2 = (Garrison) entity;
          using (IEnumerator<Entity> enumerator = state.Local.Values.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              Entity current = enumerator.Current;
              switch (current.Type)
              {
                case EntityType.DEFENSE_PLATFORM:
                case EntityType.INTRADICTOR:
                  ((DefenseEntity) current).Level = garrison2.Level;
                  continue;
                default:
                  continue;
              }
            }
            break;
          }
      }
    }
  }
}
