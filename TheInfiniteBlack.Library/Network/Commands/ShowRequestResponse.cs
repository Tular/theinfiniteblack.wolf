﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowRequestResponse
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowRequestResponse : Command
  {
    private readonly string _text;
    private readonly string _tag;
    private readonly sbyte _requestId;

    public ShowRequestResponse(IByteBuffer input)
    {
      this._text = input.GetString();
      this._tag = input.GetString();
      this._requestId = input.Get();
    }

    public override void Execute(GameState state)
    {
      if ((int) this._requestId != -66)
        state.ShowPopup(this._text, true);
      else
        state.ShowAlert("[y]" + this._text);
    }
  }
}
