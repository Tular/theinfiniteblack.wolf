﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdateCorpStatus
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdateCorpStatus : Command
  {
    private readonly short _id;
    private readonly short _allianceId;
    private readonly bool _rift;
    private readonly sbyte _x;
    private readonly sbyte _y;
    private readonly sbyte _level;
    private readonly byte _shipColor;
    private readonly Technology _tech;

    protected internal UpdateCorpStatus(IByteBuffer input)
    {
      this._id = input.GetShort();
      this._allianceId = input.GetShort();
      this._rift = (uint) input.Get() > 0U;
      this._x = input.Get();
      this._y = input.Get();
      this._level = input.Get();
      this._shipColor = (byte) input.Get();
      this._tech = new Technology(input);
    }

    public override void Execute(GameState state)
    {
      ClientCorporation clientCorporation = state.Corporations.Get(this._id);
      ClientAlliance clientAlliance = state.Alliances.Get(this._allianceId);
      clientCorporation.Alliance = clientAlliance;
      int level = (int) this._level;
      clientCorporation.GarrisonRank = (sbyte) level;
      int shipColor = (int) this._shipColor;
      clientCorporation.ShipColor = (byte) shipColor;
      clientCorporation.Tech.Set(this._tech);
      Sector location = state.Map.Get(this._rift, (int) this._x, (int) this._y);
      clientCorporation.SetLocation(location);
    }
  }
}
