﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerStatus
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerStatus : Command
  {
    private readonly int _id;
    private readonly short _corpId;
    private readonly bool _online;
    private readonly CorpRankType _rank;

    protected internal UpdatePlayerStatus(IByteBuffer input)
    {
      this._id = input.GetInt();
      this._corpId = input.GetShort();
      this._online = input.GetBool();
      this._rank = (CorpRankType) input.Get();
    }

    public override void Execute(GameState state)
    {
      ClientPlayer source = state.Players.Get(this._id);
      bool online = source.Online;
      source.MyCorporation = state.Corporations.Get(this._corpId);
      source.Online = this._online;
      source.Rank = this._rank;
      if ((!online || this._online) && (online || !this._online))
        return;
      state.UI.Show(new PlayerOnlineStatusChangedEventArgs((IGameState) state, source, this._online));
    }
  }
}
