﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowNoticeCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowNoticeCommand : Command
  {
    private readonly NoticeType _type;

    protected internal ShowNoticeCommand(IByteBuffer input)
    {
      this._type = (NoticeType) input.Get();
    }

    public override void Execute(GameState state)
    {
      switch (this._type)
      {
        case NoticeType.TradeFailCancel:
          state.UI.Show(new TradeFailedEventArgs((IGameState) state));
          break;
        case NoticeType.AccountCreateSuccess:
          state.Login.IsNewAccount = false;
          state.Connect((sbyte[]) null);
          break;
        case NoticeType.LoginInvalid:
          state.OnLoginFailed("Login Invalid! Is your name and password correct?");
          break;
        case NoticeType.NameUnavailable:
          state.OnLoginFailed("Name unavailable! Pick a different one!");
          break;
        case NoticeType.ExploreBonus:
          state.ShowEvent("New Sector Explored!");
          break;
        default:
          state.ShowPopup(this._type.Description(), true);
          break;
      }
    }
  }
}
