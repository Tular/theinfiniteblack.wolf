﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowMoneyEventCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowMoneyEventCommand : Command
  {
    private readonly int _sourceId;
    private readonly int _credits;
    private readonly int _blackDollars;

    protected internal ShowMoneyEventCommand(IByteBuffer input)
    {
      this._sourceId = input.GetInt();
      this._credits = input.GetInt();
      this._blackDollars = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      Entity source = state.Entities.Get<Entity>(this._sourceId);
      state.UI.Show(new MoneyChangeEventArgs((IGameState) state, source, this._credits, this._blackDollars));
    }
  }
}
