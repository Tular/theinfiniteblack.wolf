﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowCorpInviteCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Network.Requests;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowCorpInviteCommand : Command
  {
    private readonly short _corpId;

    protected internal ShowCorpInviteCommand(IByteBuffer input)
    {
      this._corpId = input.GetShort();
    }

    public override void Execute(GameState state)
    {
      INetworkData networkData = RequestCorpRejectInvite.Create();
      if (state.MySettings != null && state.MySettings.Social.RejectInvites)
      {
        state.Net.Send(networkData);
        state.ShowEvent("[y]Corporation Invite Auto-Rejected");
      }
      else
      {
        ClientCorporation clientCorporation = state.Corporations.Get(this._corpId);
        INetworkData accept = RequestCorpAcceptInvite.Create();
        StringBuilder stringBuilder = new StringBuilder(100);
        StringBuilder sb = stringBuilder;
        int num = 0;
        clientCorporation.AppendName(sb, num != 0);
        stringBuilder.Append(" invites you to their Corporation!");
        state.ShowAcceptDecline("Corporation - Invite", stringBuilder.ToString(), accept, networkData);
      }
    }
  }
}
