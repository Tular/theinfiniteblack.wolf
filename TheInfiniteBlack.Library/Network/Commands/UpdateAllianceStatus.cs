﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdateAllianceStatus
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdateAllianceStatus : Command
  {
    private readonly short _id;
    private readonly short _leaderId;
    private readonly bool _rift;
    private readonly sbyte _x;
    private readonly sbyte _y;
    private readonly PlanetClass _planet;

    protected internal UpdateAllianceStatus(IByteBuffer input)
    {
      this._id = input.GetShort();
      this._leaderId = input.GetShort();
      this._rift = (uint) input.Get() > 0U;
      this._x = input.Get();
      this._y = input.Get();
      this._planet = (PlanetClass) input.Get();
    }

    public override void Execute(GameState state)
    {
      ClientAlliance clientAlliance = state.Alliances.Get(this._id);
      ClientCorporation clientCorporation = state.Corporations.Get(this._leaderId);
      clientAlliance.Leader = clientCorporation;
      int planet = (int) this._planet;
      clientAlliance.Planet = (PlanetClass) planet;
      Sector location = state.Map.Get(this._rift, (int) this._x, (int) this._y);
      clientAlliance.SetLocation(location);
    }
  }
}
