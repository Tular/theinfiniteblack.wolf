﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowChatMessageCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowChatMessageCommand : Command
  {
    private readonly string _text;
    private readonly string _sender;
    private readonly string _receiver;
    private readonly ChatType _type;

    public ShowChatMessageCommand(IByteBuffer input)
    {
      this._text = input.GetString();
      this._sender = input.GetString();
      this._receiver = input.GetString();
      this._type = (ChatType) input.Get();
    }

    public override void Execute(GameState state)
    {
      if (this._type == ChatType.ALERT && this._text.Contains("Achievement Earned!"))
      {
        string achievement = this._text.Replace("<b>[lb]* Achievement Earned! -=[ ", "").Replace(" ]=-", "");
        state.UI.Show(new AchievementEventArgs((IGameState) state, achievement, this._text));
      }
      else
      {
        state.UI.Show(new ChatEventArgs((IGameState) state, this._text, this._receiver, this._sender, this._type));
        if (this._type != ChatType.PRIVATE)
          return;
        state.AddReply(this._sender);
        state.AddReply(this._receiver);
      }
    }
  }
}
