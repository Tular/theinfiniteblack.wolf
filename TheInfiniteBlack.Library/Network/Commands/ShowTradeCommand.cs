﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowTradeCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowTradeCommand : Command
  {
    private readonly int _playerIdA;
    private readonly int _playerIdB;
    private readonly int _playerACredits;
    private readonly int _playerABlackDollars;
    private readonly EquipmentItem _playerAItem;
    private readonly int _playerBCredits;
    private readonly int _playerBBlackDollars;
    private readonly EquipmentItem _playerBItem;
    private readonly bool _final;

    public ShowTradeCommand(IByteBuffer input)
    {
      this._playerIdA = input.GetInt();
      this._playerIdB = input.GetInt();
      this._playerACredits = input.GetInt();
      this._playerABlackDollars = input.GetInt();
      this._playerAItem = EquipmentItem.Execute(input);
      this._playerBCredits = input.GetInt();
      this._playerBBlackDollars = input.GetInt();
      this._playerBItem = EquipmentItem.Execute(input);
      this._final = input.GetBool();
    }

    public override void Execute(GameState state)
    {
      TradeEventArgs tradeEventArgs = new TradeEventArgs((IGameState) state);
      int num = this._final ? 1 : 0;
      tradeEventArgs.IsFinal = num != 0;
      TradeEventArgs e = tradeEventArgs;
      TradeComponent a = e.A;
      TradeComponent b = e.B;
      ClientPlayer clientPlayer = state.Players.Get(this._playerIdA);
      a.Player = clientPlayer;
      int playerAblackDollars = this._playerABlackDollars;
      a.BlackDollars = playerAblackDollars;
      int playerAcredits = this._playerACredits;
      a.Credits = playerAcredits;
      EquipmentItem playerAitem = this._playerAItem;
      a.Item = playerAitem;
      b.Player = state.Players.Get(this._playerIdB);
      b.BlackDollars = this._playerBBlackDollars;
      b.Credits = this._playerBCredits;
      b.Item = this._playerBItem;
      if (state.MySettings != null && state.MySettings.Social.RejectTrades || state.MyShip.PvPFlag && (b.Player.Relation == RelationType.ENEMY || b.Player.Relation == RelationType.NEUTRAL))
      {
        e.Cancel();
        state.ShowEvent("[y]Trade Request Auto-Rejected");
      }
      else
        state.UI.Show(e);
    }
  }
}
