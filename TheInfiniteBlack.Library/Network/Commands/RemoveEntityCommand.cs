﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.RemoveEntityCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class RemoveEntityCommand : Command
  {
    private readonly int _id;
    private readonly EntityExitType _type;
    private readonly Direction _dir;

    protected internal RemoveEntityCommand(IByteBuffer input)
    {
      this._id = input.GetInt();
      this._type = (EntityExitType) input.Get();
      switch (this._type)
      {
        case EntityExitType.MOVED:
        case EntityExitType.DRAGGED:
          this._dir = (Direction) input.Get();
          break;
        default:
          this._dir = Direction.None;
          break;
      }
    }

    public override void Execute(GameState state)
    {
      Entity entity = state.Entities.Get<Entity>(this._id);
      state.Local.Remove(entity, this._type, this._dir);
    }
  }
}
