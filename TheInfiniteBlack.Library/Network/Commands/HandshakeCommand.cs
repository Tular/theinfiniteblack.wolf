﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.HandshakeCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class HandshakeCommand : Command
  {
    private readonly byte[] _iv;
    private readonly sbyte _version;

    protected internal HandshakeCommand(IByteBuffer input)
    {
      sbyte[] data = new sbyte[(int) input.Get()];
      input.Get(data);
      this._version = input.Get();
      this._iv = new byte[data.Length];
      Buffer.BlockCopy((Array) data, 0, (Array) this._iv, 0, data.Length);
    }

    public override void Execute(GameState state)
    {
      Log.I((object) this, nameof (Execute), "GOT HANDSHAKE: IV=" + (object) this._iv.Length + " BYTES -- VERSION " + (object) this._version);
      if ((int) GameSettings.CompliantVersion >= (int) this._version && (int) this._version != (int) sbyte.MinValue)
      {
        GameSettings.ServerVersion = this._version;
        state.Net.IV = this._iv;
        state.Connect((sbyte[]) null);
      }
      else
        state.OnLoginFailed("Wrong Client Version - Update Required");
    }
  }
}
