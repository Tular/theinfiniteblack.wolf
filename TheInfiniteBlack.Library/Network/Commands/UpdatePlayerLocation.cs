﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerLocation
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerLocation : Command
  {
    private readonly int _id;
    private readonly sbyte _x;
    private readonly sbyte _y;
    private readonly bool _rift;
    private readonly bool _hidden;

    protected internal UpdatePlayerLocation(IByteBuffer input)
    {
      this._id = input.GetInt();
      this._x = input.Get();
      if ((int) this._x >= 0)
      {
        this._y = input.Get();
        this._rift = (uint) input.Get() > 0U;
        this._hidden = false;
      }
      else
      {
        this._y = sbyte.MinValue;
        this._rift = false;
        this._hidden = true;
      }
    }

    public override void Execute(GameState state)
    {
      state.Players.Get(this._id).Location = this._hidden ? (Sector) null : state.Map.Get(this._rift, (int) this._x, (int) this._y);
    }
  }
}
