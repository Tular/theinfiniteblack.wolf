﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowGainItemCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowGainItemCommand : Command
  {
    private readonly int _sourceId;
    private readonly EquipmentItem _item;

    protected internal ShowGainItemCommand(IByteBuffer input)
    {
      this._sourceId = input.GetInt();
      this._item = EquipmentItem.Execute(input);
    }

    public override void Execute(GameState state)
    {
      Entity source = state.Entities.Get<Entity>(this._sourceId);
      state.UI.Show(new GainItemEventArgs((IGameState) state, source, this._item));
    }
  }
}
