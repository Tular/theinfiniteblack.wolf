﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdateEntityInventory
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdateEntityInventory : Command
  {
    private readonly int _sourceId;
    private readonly List<EquipmentItem> _items;

    protected internal UpdateEntityInventory(IByteBuffer input)
    {
      this._sourceId = input.GetInt();
      int capacity = (int) input.Get();
      this._items = new List<EquipmentItem>(capacity);
      for (int index = 0; index < capacity; ++index)
      {
        EquipmentItem equipmentItem = EquipmentItem.Execute(input);
        if (equipmentItem != null)
          this._items.Add(equipmentItem);
      }
    }

    public override void Execute(GameState state)
    {
      Entity source = state.Entities.Get<Entity>(this._sourceId);
      switch (source.Type)
      {
        case EntityType.STARPORT:
          ((StarPort) source).Inventory = this._items;
          break;
        case EntityType.SHIP:
          ((Ship) source).Inventory = this._items;
          break;
        case EntityType.GARRISON:
          ((Garrison) source).Inventory = this._items;
          break;
      }
      state.UI.Show(new InventoryChangedEventArgs((IGameState) state, source, this._items));
    }
  }
}
