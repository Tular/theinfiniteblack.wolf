﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerBank
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerBank : Command
  {
    private readonly List<EquipmentItem> _items;

    protected internal UpdatePlayerBank(IByteBuffer input)
    {
      int capacity = (int) input.Get();
      this._items = new List<EquipmentItem>(capacity);
      for (int index = 0; index < capacity; ++index)
      {
        EquipmentItem equipmentItem = EquipmentItem.Execute(input);
        if (equipmentItem != null)
          this._items.Add(equipmentItem);
      }
    }

    public override void Execute(GameState state)
    {
      state.Bank.Set(this._items);
      state.UI.Show(new BankChangedEventArgs((IGameState) state, this._items));
    }
  }
}
