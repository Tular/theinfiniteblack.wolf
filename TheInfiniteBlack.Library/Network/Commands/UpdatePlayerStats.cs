﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerStats
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerStats : Command
  {
    private readonly int _id;
    private readonly short _kills;
    private readonly short _deaths;
    private readonly short _garrisonKills;
    private readonly short _garrisonDefends;
    private readonly short _pvpRating;
    private readonly int _bounty;

    protected internal UpdatePlayerStats(IByteBuffer input)
    {
      this._id = input.GetInt();
      this._kills = input.GetShort();
      this._deaths = input.GetShort();
      this._garrisonKills = input.GetShort();
      this._garrisonDefends = input.GetShort();
      this._pvpRating = input.GetShort();
      this._bounty = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      ClientPlayer clientPlayer = state.Players.Get(this._id);
      int kills = (int) this._kills;
      clientPlayer.Kills = (short) kills;
      int deaths = (int) this._deaths;
      clientPlayer.Deaths = (short) deaths;
      int garrisonKills = (int) this._garrisonKills;
      clientPlayer.GarrisonKills = (short) garrisonKills;
      int garrisonDefends = (int) this._garrisonDefends;
      clientPlayer.GarrisonDefends = (short) garrisonDefends;
      int pvpRating = (int) this._pvpRating;
      clientPlayer.PvpRating = (short) pvpRating;
      int bounty = this._bounty;
      clientPlayer.Bounty = bounty;
    }
  }
}
