﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.SetFollowCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class SetFollowCommand : Command
  {
    private readonly int _targetId;

    protected internal SetFollowCommand(IByteBuffer input)
    {
      this._targetId = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      CombatEntity target = state.Entities.Get<CombatEntity>(this._targetId);
      state.OnFollow(target);
    }
  }
}
