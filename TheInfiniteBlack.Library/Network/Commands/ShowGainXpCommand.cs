﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.ShowGainXpCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class ShowGainXpCommand : Command
  {
    private readonly int _shipId;
    private readonly int _xp;

    protected internal ShowGainXpCommand(IByteBuffer input)
    {
      this._shipId = input.GetInt();
      this._xp = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      Ship source = state.Entities.Get<Ship>(this._shipId);
      source.XP += this._xp;
      state.UI.Show(new GainXpEventArgs((IGameState) state, source, this._xp));
    }
  }
}
