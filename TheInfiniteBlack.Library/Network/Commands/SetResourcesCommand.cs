﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.SetResourcesCommand
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class SetResourcesCommand : Command
  {
    private readonly int _shipId;
    private readonly sbyte _organics;
    private readonly sbyte _gas;
    private readonly sbyte _metals;
    private readonly sbyte _radioactives;
    private readonly sbyte _darkMatter;

    protected internal SetResourcesCommand(IByteBuffer input)
    {
      this._shipId = input.GetInt();
      this._darkMatter = input.Get();
      this._radioactives = input.Get();
      this._metals = input.Get();
      this._gas = input.Get();
      this._organics = input.Get();
    }

    public override void Execute(GameState state)
    {
      Ship source = state.Entities.Get<Ship>(this._shipId);
      int organics = (int) this._organics - (int) source.Organics;
      int gas = (int) this._gas - (int) source.Gas;
      int metals = (int) this._metals - (int) source.Metals;
      int radioactives = (int) this._radioactives - (int) source.Radioactives;
      int darkMatter = (int) this._darkMatter - (int) source.DarkMatter;
      source.Organics = this._organics;
      source.Gas = this._gas;
      source.Metals = this._metals;
      source.Radioactives = this._radioactives;
      source.DarkMatter = this._darkMatter;
      if (organics == 0 && gas == 0 && (metals == 0 && radioactives == 0) && darkMatter == 0)
        return;
      state.UI.Show(new ResourcesChangedEventArgs((IGameState) state, source, organics, gas, metals, radioactives, darkMatter));
    }
  }
}
