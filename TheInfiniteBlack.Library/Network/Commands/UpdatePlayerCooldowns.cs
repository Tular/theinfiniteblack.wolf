﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Commands.UpdatePlayerCooldowns
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Commands
{
  public class UpdatePlayerCooldowns : Command
  {
    private readonly int _attackCooldownMS;
    private readonly int _moveCooldownMS;
    private readonly int _harvestCooldownMS;
    private readonly int _repairCooldownMS;
    private readonly int _earthJumpTimeMS;

    protected internal UpdatePlayerCooldowns(IByteBuffer input)
    {
      this._attackCooldownMS = input.GetInt();
      this._moveCooldownMS = input.GetInt();
      this._harvestCooldownMS = input.GetInt();
      this._repairCooldownMS = input.GetInt();
      this._earthJumpTimeMS = input.GetInt();
    }

    public override void Execute(GameState state)
    {
      state.AttackCooldown.Set((long) this._attackCooldownMS);
      state.MoveCooldown.Set((long) this._moveCooldownMS);
      state.HarvestCooldown.Set((long) this._harvestCooldownMS);
      state.RepairCooldown.Set((long) this._repairCooldownMS);
      state.EarthJumpTime.Set((long) this._earthJumpTimeMS);
    }
  }
}
