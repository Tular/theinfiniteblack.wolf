﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.DataReceiver
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Net.Sockets;

namespace TheInfiniteBlack.Library.Network
{
  public abstract class DataReceiver
  {
    private byte[] _recBuffer = new byte[10240];
    private const int DefaultBufferSize = 10240;
    private int _readIndex;
    private int _writeIndex;
    private Socket _socket;

    public bool Connected
    {
      get
      {
        if (this._socket != null)
          return this._socket.Connected;
        return false;
      }
    }

    public virtual void Connect(Socket socket)
    {
      if (this.Connected)
        this.Disconnect("Re-Connecting");
      this._socket = socket;
      SocketAsyncEventArgs socketAsyncEventArgs = new SocketAsyncEventArgs();
      socketAsyncEventArgs.SetBuffer(new byte[10240], 0, 10240);
      socketAsyncEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(this.OnReceive);
      if (this._socket.ReceiveAsync(socketAsyncEventArgs))
        return;
      this.OnReceive((object) null, socketAsyncEventArgs);
    }

    public virtual void Disconnect(string reason)
    {
      if (this._socket == null)
        return;
      SocketHelper.Close(this._socket);
      this._socket = (Socket) null;
      Log.I((object) this, "Close", "Disconnected (" + reason + ")");
    }

    protected abstract void OnData(sbyte[] data);

    private void OnReceive(object sender, SocketAsyncEventArgs args)
    {
      try
      {
        if (Log.Debug)
          Log.D((object) this, nameof (OnReceive), ((int) args.SocketError).ToString() + " (" + (object) args.BytesTransferred + " bytes)");
        int bytesTransferred = args.BytesTransferred;
        if (bytesTransferred > 0 && this._socket.Connected && args.SocketError == SocketError.Success)
        {
          if (bytesTransferred + this._writeIndex >= this._recBuffer.Length)
          {
            int length = this._recBuffer.Length * 2 + bytesTransferred + 1024;
            if (Log.Debug)
              Log.D((object) this, nameof (OnReceive), "Resizing read buffer from " + (object) this._recBuffer.Length + " to " + (object) length);
            byte[] numArray = new byte[length];
            Buffer.BlockCopy((Array) this._recBuffer, 0, (Array) numArray, 0, this._writeIndex);
            this._recBuffer = numArray;
          }
          Buffer.BlockCopy((Array) args.Buffer, 0, (Array) this._recBuffer, this._writeIndex, bytesTransferred);
          this._writeIndex = this._writeIndex + bytesTransferred;
          int count;
          for (count = this._writeIndex - this._readIndex; count >= 3; count = this._writeIndex - this._readIndex)
          {
            int length = (int) this._recBuffer[this._readIndex + 1] | (int) this._recBuffer[this._readIndex] << 8;
            if (length + 2 <= count)
            {
              sbyte[] data = new sbyte[length];
              Buffer.BlockCopy((Array) this._recBuffer, this._readIndex + 2, (Array) data, 0, data.Length);
              this.OnData(data);
              this._readIndex = this._readIndex + (length + 2);
            }
            else
              break;
          }
          if (count <= 0)
          {
            this._readIndex = 0;
            this._writeIndex = 0;
          }
          else if (this._readIndex > 0)
          {
            Buffer.BlockCopy((Array) this._recBuffer, this._readIndex, (Array) this._recBuffer, 0, count);
            this._writeIndex = this._writeIndex - this._readIndex;
            this._readIndex = 0;
          }
          if (this._socket.ReceiveAsync(args))
            return;
          this.OnReceive((object) null, args);
        }
        else
        {
          this.Disconnect("Connection Lost");
          args.Dispose();
        }
      }
      catch (ObjectDisposedException ex)
      {
      }
      catch (Exception ex)
      {
        Log.E((object) this, nameof (OnReceive), ex);
        this.Disconnect("Error: " + (object) ex);
        args.Dispose();
      }
    }
  }
}
