﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestChat
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestChat
  {
    public static INetworkData Create(string message, ChatType type)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -66, message.Length + 8);
      string str = message;
      requestBuffer.Write(str);
      string empty1 = string.Empty;
      requestBuffer.Write(empty1);
      string empty2 = string.Empty;
      requestBuffer.Write(empty2);
      int num = (int) (sbyte) type;
      requestBuffer.Write((sbyte) num);
      return (INetworkData) requestBuffer;
    }

    public static INetworkData CreatePrivate(string message, string reciever)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -66, message.Length + reciever.Length + 8);
      string str1 = message;
      requestBuffer.Write(str1);
      string empty = string.Empty;
      requestBuffer.Write(empty);
      string str2 = reciever;
      requestBuffer.Write(str2);
      int num = 3;
      requestBuffer.Write((sbyte) num);
      return (INetworkData) requestBuffer;
    }
  }
}
