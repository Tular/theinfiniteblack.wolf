﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestAuctionCancel
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestAuctionCancel
  {
    public static INetworkData Create(ClientAuction auction)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 48, 4);
      int id = auction.ID;
      requestBuffer.Write(id);
      return (INetworkData) requestBuffer;
    }
  }
}
