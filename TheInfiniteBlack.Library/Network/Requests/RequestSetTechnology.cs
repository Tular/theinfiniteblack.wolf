﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestSetTechnology
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestSetTechnology
  {
    public static INetworkData Create(Technology tech)
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) -34, 64);
      tech.Write((IByteBuffer) networkData);
      return networkData;
    }
  }
}
