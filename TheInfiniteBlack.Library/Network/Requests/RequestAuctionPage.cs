﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestAuctionPage
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestAuctionPage
  {
    public static INetworkData Create(ItemType type, ItemRarity rarity)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 44, 2);
      int num1 = (int) (sbyte) type;
      requestBuffer.Write((sbyte) num1);
      int num2 = (int) (sbyte) rarity;
      requestBuffer.Write((sbyte) num2);
      return (INetworkData) requestBuffer;
    }
  }
}
