﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestCorpRejectInvite
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestCorpRejectInvite
  {
    private static INetworkData _cache;

    public static INetworkData Create()
    {
      return RequestCorpRejectInvite._cache ?? (RequestCorpRejectInvite._cache = (INetworkData) new RequestBuffer((sbyte) -37, 0));
    }
  }
}
