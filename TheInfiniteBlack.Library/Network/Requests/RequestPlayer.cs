﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestPlayer
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestPlayer
  {
    public static INetworkData Create(int id)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -65, 4);
      int num = id;
      requestBuffer.Write(num);
      return (INetworkData) requestBuffer;
    }
  }
}
