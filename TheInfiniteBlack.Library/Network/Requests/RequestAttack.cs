﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestAttack
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestAttack
  {
    private static INetworkData _clearCache;

    public static INetworkData Create(CombatEntity target)
    {
      if (target != null)
      {
        RequestBuffer requestBuffer = new RequestBuffer((sbyte) -52, 4);
        int id = target.ID;
        requestBuffer.Write(id);
        return (INetworkData) requestBuffer;
      }
      if (RequestAttack._clearCache == null)
      {
        RequestAttack._clearCache = (INetworkData) new RequestBuffer((sbyte) -52, 4);
        RequestAttack._clearCache.Write(int.MinValue);
      }
      return RequestAttack._clearCache;
    }
  }
}
