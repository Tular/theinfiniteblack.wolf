﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestJettison
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestJettison
  {
    public static INetworkData Create(sbyte organics, sbyte gas, sbyte metals, sbyte radioactives, sbyte darkMatter)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 12, 5);
      int num1 = (int) organics;
      requestBuffer.Write((sbyte) num1);
      int num2 = (int) gas;
      requestBuffer.Write((sbyte) num2);
      int num3 = (int) metals;
      requestBuffer.Write((sbyte) num3);
      int num4 = (int) radioactives;
      requestBuffer.Write((sbyte) num4);
      int num5 = (int) darkMatter;
      requestBuffer.Write((sbyte) num5);
      return (INetworkData) requestBuffer;
    }
  }
}
