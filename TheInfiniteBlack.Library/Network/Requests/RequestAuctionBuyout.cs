﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestAuctionBuyout
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestAuctionBuyout
  {
    public static INetworkData Create(ClientAuction auction, bool buyWithBlackDollars)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 45, 5);
      int id = auction.ID;
      requestBuffer.Write(id);
      int num = buyWithBlackDollars ? 1 : 0;
      requestBuffer.Write(num != 0);
      return (INetworkData) requestBuffer;
    }
  }
}
