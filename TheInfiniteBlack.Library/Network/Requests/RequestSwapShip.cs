﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestSwapShip
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestSwapShip
  {
    public static INetworkData Create(ShipClass type)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -45, 1);
      int num = (int) (sbyte) type;
      requestBuffer.Write((sbyte) num);
      return (INetworkData) requestBuffer;
    }
  }
}
