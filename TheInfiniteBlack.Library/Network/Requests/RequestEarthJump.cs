﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestEarthJump
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestEarthJump
  {
    private static INetworkData _ejCache;
    private static INetworkData _cjCache;

    public static INetworkData Create(bool corpJump)
    {
      if (corpJump)
      {
        if (RequestEarthJump._cjCache == null)
        {
          RequestEarthJump._cjCache = (INetworkData) new RequestBuffer((sbyte) -48, 1);
          RequestEarthJump._cjCache.Write(true);
        }
        return RequestEarthJump._cjCache;
      }
      if (RequestEarthJump._ejCache == null)
      {
        RequestEarthJump._ejCache = (INetworkData) new RequestBuffer((sbyte) -48, 1);
        RequestEarthJump._ejCache.Write(false);
      }
      return RequestEarthJump._ejCache;
    }
  }
}
