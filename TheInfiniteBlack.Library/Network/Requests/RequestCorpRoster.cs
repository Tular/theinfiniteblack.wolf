﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestCorpRoster
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestCorpRoster
  {
    public static INetworkData Create(short id, CorpRankType rank)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) -63, 3);
      int num1 = (int) id;
      requestBuffer.Write((short) num1);
      int num2 = (int) (sbyte) rank;
      requestBuffer.Write((sbyte) num2);
      return (INetworkData) requestBuffer;
    }
  }
}
