﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestFollow
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestFollow
  {
    private static INetworkData _nullCache;

    public static INetworkData Create(CombatEntity target)
    {
      if (target != null)
      {
        RequestBuffer requestBuffer = new RequestBuffer((sbyte) -53, 4);
        int id = target.ID;
        requestBuffer.Write(id);
        return (INetworkData) requestBuffer;
      }
      if (RequestFollow._nullCache == null)
      {
        RequestFollow._nullCache = (INetworkData) new RequestBuffer((sbyte) -53, 4);
        RequestFollow._nullCache.Write(int.MinValue);
      }
      return RequestFollow._nullCache;
    }
  }
}
