﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestDevelopBlackDollars
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestDevelopBlackDollars
  {
    public static INetworkData Create(int targetId, int blackDollars)
    {
      RequestBuffer requestBuffer = new RequestBuffer((sbyte) 2, 8);
      int num1 = targetId;
      requestBuffer.Write(num1);
      int num2 = blackDollars;
      requestBuffer.Write(num2);
      return (INetworkData) requestBuffer;
    }
  }
}
