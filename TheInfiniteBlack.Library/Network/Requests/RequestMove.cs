﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestMove
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public class RequestMove
  {
    private static readonly Dictionary<Direction, INetworkData> _cache = new Dictionary<Direction, INetworkData>();

    public static INetworkData Create(Direction dir)
    {
      INetworkData networkData1;
      if (RequestMove._cache.TryGetValue(dir, out networkData1))
        return networkData1;
      INetworkData networkData2 = (INetworkData) new RequestBuffer((sbyte) -58, 1);
      networkData2.Write((sbyte) dir);
      RequestMove._cache[dir] = networkData2;
      return networkData2;
    }
  }
}
