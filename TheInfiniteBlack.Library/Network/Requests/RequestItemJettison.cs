﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestItemJettison
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestItemJettison
  {
    public static INetworkData Create(EquipmentItem item)
    {
      INetworkData networkData = (INetworkData) new RequestBuffer((sbyte) 22, 20);
      item.Write((IByteBuffer) networkData);
      return networkData;
    }
  }
}
