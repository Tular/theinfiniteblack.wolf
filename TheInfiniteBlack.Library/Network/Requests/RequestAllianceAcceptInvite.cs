﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Network.Requests.RequestAllianceAcceptInvite
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library.Network.Requests
{
  public static class RequestAllianceAcceptInvite
  {
    private static INetworkData _cache;

    public static INetworkData Create()
    {
      return RequestAllianceAcceptInvite._cache ?? (RequestAllianceAcceptInvite._cache = (INetworkData) new RequestBuffer((sbyte) -36, 0));
    }
  }
}
