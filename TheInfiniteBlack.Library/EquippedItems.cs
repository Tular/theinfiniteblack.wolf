﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.EquippedItems
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Library
{
  public class EquippedItems
  {
    public ShipClass ShipClass { get; set; }

    public sbyte ShipSkin { get; set; }

    public int WeaponRank { get; set; }

    public WeaponClass WeaponClass { get; set; }

    public ItemRarity WeaponRarity { get; set; }

    public int ArmorRank { get; set; }

    public ArmorClass ArmorClass { get; set; }

    public ItemRarity ArmorRarity { get; set; }

    public int StorageRank { get; set; }

    public StorageClass StorageClass { get; set; }

    public ItemRarity StorageRarity { get; set; }

    public EngineClass EngineClass { get; set; }

    public ItemRarity EngineRarity { get; set; }

    public ComputerClass ComputerClass { get; set; }

    public ItemRarity ComputeRarity { get; set; }

    public HarvesterClass HarvesterClass { get; set; }

    public ItemRarity HarvesteRarity { get; set; }

    public SpecialClass SpecialClass { get; set; }

    public ItemRarity SpecialRarity { get; set; }

    public static EquippedItems FromShip(Ship ship, sbyte shipSkin)
    {
      return new EquippedItems()
      {
        ShipClass = ship.Class,
        ShipSkin = shipSkin,
        WeaponRank = (int) ship.Weapon.BaseEP,
        WeaponClass = ship.Weapon.Class,
        WeaponRarity = ship.Weapon.Rarity,
        ArmorRank = (int) ship.Armor.BaseEP,
        ArmorRarity = ship.Armor.Rarity,
        ArmorClass = ship.Armor.Class,
        StorageClass = ship.Storage.Class,
        StorageRarity = ship.Storage.Rarity,
        StorageRank = (int) ship.Storage.BaseEP,
        EngineClass = ship.Engine.Class,
        EngineRarity = ship.Engine.Rarity,
        ComputerClass = ship.Computer.Class,
        ComputeRarity = ship.Computer.Rarity,
        HarvesterClass = ship.Harvester.Class,
        HarvesteRarity = ship.Harvester.Rarity,
        SpecialClass = ship.Special.Class,
        SpecialRarity = ship.Special.Rarity
      };
    }
  }
}
