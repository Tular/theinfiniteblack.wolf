﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Logger
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Text;

namespace TheInfiniteBlack.Library
{
  public abstract class Logger : ILogger
  {
    public bool Debug { get; set; }

    protected abstract void OnDebug(object sender, string callerName, string log, string line);

    protected abstract void OnInformation(object sender, string callerName, string log, string line);

    protected abstract void OnWarning(object sender, string callerName, string log, string line);

    protected abstract void OnException(object sender, string callerName, Exception e, string line);

    public void I(object sender, string callerName, string log)
    {
      StringBuilder stringBuilder = new StringBuilder(Util.Now);
      stringBuilder.Append(" I-- ");
      stringBuilder.Append(sender.GetType().Name);
      stringBuilder.Append(".");
      stringBuilder.Append(callerName);
      stringBuilder.Append(": ");
      stringBuilder.Append(log);
      this.OnInformation(sender, callerName, log, stringBuilder.ToString());
    }

    public void D(object sender, string callerName, string log)
    {
      StringBuilder stringBuilder = new StringBuilder(Util.Now);
      stringBuilder.Append(" D-- ");
      stringBuilder.Append(sender.GetType().Name);
      stringBuilder.Append(".");
      stringBuilder.Append(callerName);
      stringBuilder.Append(": ");
      stringBuilder.Append(log);
      this.OnDebug(sender, callerName, log, stringBuilder.ToString());
    }

    public void W(object sender, string callerName, string log)
    {
      StringBuilder stringBuilder = new StringBuilder(Util.Now);
      stringBuilder.Append(" W-- ");
      stringBuilder.Append(sender.GetType().Name);
      stringBuilder.Append(".");
      stringBuilder.Append(callerName);
      stringBuilder.Append(": ");
      stringBuilder.Append(log);
      this.OnWarning(sender, callerName, log, stringBuilder.ToString());
    }

    public void E(object sender, string callerName, Exception e)
    {
      StringBuilder stringBuilder = new StringBuilder(Util.Now);
      stringBuilder.Append(" E-- ");
      stringBuilder.Append(sender.GetType().Name);
      stringBuilder.Append(".");
      stringBuilder.Append(callerName);
      stringBuilder.Append(": ");
      stringBuilder.Append((object) e);
      this.OnException(sender, callerName, e, stringBuilder.ToString());
    }
  }
}
