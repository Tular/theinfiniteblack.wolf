﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.ILogger
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;

namespace TheInfiniteBlack.Library
{
  public interface ILogger
  {
    bool Debug { get; set; }

    void I(object sender, string callerName, string msg);

    void D(object sender, string callerName, string msg);

    void W(object sender, string callerName, string msg);

    void E(object sender, string callerName, Exception e);
  }
}
