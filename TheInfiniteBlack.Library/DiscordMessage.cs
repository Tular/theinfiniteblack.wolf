﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.DiscordMessage
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public class DiscordMessage
  {
    public string webook { get; set; }

    public string username { get; set; }

    public string icon_url { get; set; }

    public string text { get; set; }

    public override bool Equals(object obj)
    {
      DiscordMessage discordMessage = (DiscordMessage) obj;
      return this.username == discordMessage.username && this.icon_url == discordMessage.icon_url && this.text == discordMessage.text;
    }
  }
}
