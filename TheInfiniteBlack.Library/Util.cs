﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.Util
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System;
using System.Linq;
using System.Text;

namespace TheInfiniteBlack.Library
{
  public static class Util
  {
    public static readonly Encoding ASCII = Encoding.ASCII;
    public static string[] NUMERALS = new string[31]
    {
      "0",
      "I",
      "II",
      "III",
      "IV",
      "V",
      "VI",
      "VII",
      "VIII",
      "IX",
      "X",
      "XI",
      "XII",
      "XIII",
      "XIV",
      "XV",
      "XVI",
      "XVII",
      "XVIII",
      "XIX",
      "XX",
      "XXI",
      "XXII",
      "XXIII",
      "XXIV",
      "XXV",
      "XXVI",
      "XXVII",
      "XXVIII",
      "XXIX",
      "XXX"
    };
    public const string TimeFormat = "MM/dd/yy H:mm:ss.fff";

    public static string Now
    {
      get
      {
        return DateTime.Now.ToString("MM/dd/yy H:mm:ss.fff");
      }
    }

    public static string Regex(string str, bool numbers, bool letters, bool space)
    {
      if (numbers)
      {
        if (letters)
        {
          if (space)
            return new string(str.Where<char>((Func<char, bool>) (c =>
            {
              if (!char.IsLetterOrDigit(c))
                return (int) c == 32;
              return true;
            })).ToArray<char>());
          return new string(str.Where<char>(new Func<char, bool>(char.IsLetterOrDigit)).ToArray<char>());
        }
        if (space)
          return new string(str.Where<char>((Func<char, bool>) (c =>
          {
            if (!char.IsDigit(c))
              return (int) c == 32;
            return true;
          })).ToArray<char>());
        return new string(str.Where<char>(new Func<char, bool>(char.IsDigit)).ToArray<char>());
      }
      if (!letters)
        return string.Empty;
      if (space)
        return new string(str.Where<char>((Func<char, bool>) (c =>
        {
          if (!char.IsLetter(c))
            return (int) c == 32;
          return true;
        })).ToArray<char>());
      return new string(str.Where<char>(new Func<char, bool>(char.IsLetter)).ToArray<char>());
    }
  }
}
