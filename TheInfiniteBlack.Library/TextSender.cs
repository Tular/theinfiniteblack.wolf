﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.TextSender
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

using System.Net;
using System.Net.Security;

namespace TheInfiniteBlack.Library
{
  public static class TextSender
  {
    private static string _lastMessage = "";

    public static void SendText(string message)
    {
      if (message == TextSender._lastMessage)
        return;
      ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(Certification.MyRemoteCertificateValidationCallback);
      (WebRequest.Create("https://api.clockworksms.com/http/send.aspx?key=dfd91e050b87ffec6607cbc4e9960be8fb285744&to=447583675957&content=" + message) as HttpWebRequest).GetResponse();
      TextSender._lastMessage = message;
    }
  }
}
