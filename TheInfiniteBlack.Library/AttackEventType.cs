﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Library.AttackEventType
// Assembly: TheInfiniteBlack.Library, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9B6DF1F4-53EE-47B5-BC4A-A5FBDE1E480C
// Assembly location: C:\Users\Pascal\Documents\The Infinite Black\Test\TheInfiniteBlack.Library.dll

namespace TheInfiniteBlack.Library
{
  public enum AttackEventType
  {
    NULL = -128,
    MISS = 0,
    GRAZE = 1,
    HIT = 2,
    CRITICAL = 3,
    SPLASH = 4,
    REPAIR = 5,
    STUN = 6,
    COLLIDE = 7,
    DEFLECT = 8,
    RAM = 9,
    GRAPPLE = 10,
    DEGRAPPLE = 11,
  }
}
