﻿// Decompiled with JetBrains decompiler
// Type: ItemSelectPopupSelectedItemDetailsCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class ItemSelectPopupSelectedItemDetailsCard : MonoBehaviour
{
  public string resourcePath = "items";
  public UITexture icon;
  public UILabel notSelectedMessage;
  public GameObject itemDetailsRoot;
  public UILabel title;
  public UILabel subTitle;
  public UILabel description;
  public UILabel equipPoints;
  public UILabel durability;
  public UILabel creditValue;
  public UITextList detailsTextList;
  private ItemSelectPopupContext _itemSelectPopupContext;
  private bool _isInitialized;
  private bool _isStarted;
  private EquipmentItem _backingItem;
  private bool _isUpdating;

  private void Show(EquipmentItem item)
  {
    this._backingItem = item;
    if (item == null)
    {
      if (!this.notSelectedMessage.gameObject.activeSelf)
        this.notSelectedMessage.gameObject.SetActive(true);
      if (this.itemDetailsRoot.gameObject.activeSelf)
        this.itemDetailsRoot.gameObject.SetActive(false);
      this.detailsTextList.scrollValue = 0.0f;
    }
    else
    {
      if (this.notSelectedMessage.gameObject.activeSelf)
        this.notSelectedMessage.gameObject.SetActive(false);
      if (!this.itemDetailsRoot.gameObject.activeSelf)
        this.itemDetailsRoot.gameObject.SetActive(true);
      this.UpdateVisuals(item);
    }
  }

  private void UpdateVisuals(EquipmentItem item)
  {
    if (item == null)
    {
      NGUITools.SetActiveSelf(this.gameObject, false);
    }
    else
    {
      if (!this.gameObject.activeSelf)
        this.gameObject.SetActive(true);
      this.LoadItemTexture(item.Icon);
      StringBuilder stringBuilder1 = new StringBuilder(100);
      StringBuilder stringBuilder2 = new StringBuilder(100);
      StringBuilder stringBuilder3 = new StringBuilder(500);
      item.AppendName(stringBuilder1);
      Markup.GetNGUI(stringBuilder1);
      this.title.text = stringBuilder1.ToString();
      stringBuilder2.Append(item.Rarity.GetMarkup());
      item.AppendSubTitle(stringBuilder2);
      Markup.GetNGUI(stringBuilder2);
      this.subTitle.text = stringBuilder2.ToString();
      item.AppendDescription(stringBuilder3);
      Markup.GetNGUI(stringBuilder3);
      this.description.text = stringBuilder3.ToString();
      this.equipPoints.text = string.Format("{0}", (object) item.EPCost);
      this.durability.text = string.Format("{0}%", (object) item.Durability);
      this.creditValue.text = string.Format("{0:$###,###,###}", (object) item.ActualCreditValue);
      StringBuilder stringBuilder4 = new StringBuilder();
      this.detailsTextList.Clear();
      stringBuilder4.AppendFormat("{0}", (object) "[g]");
      string newValue1 = "\n[g]";
      stringBuilder4.AppendLine(item.Pros.Replace("\n", newValue1));
      stringBuilder4.AppendFormat("{0}", (object) "[r]");
      string newValue2 = "\n[r]";
      stringBuilder4.AppendLine(item.Cons.Replace("\n", newValue2));
      Markup.GetNGUI(stringBuilder4);
      this.detailsTextList.Add(stringBuilder4.ToString());
      this.detailsTextList.scrollValue = 0.0f;
      if ((double) ((UIScrollBar) this.detailsTextList.scrollBar).barSize < 1.0)
        NGUITools.SetActiveSelf(this.detailsTextList.scrollBar.gameObject, true);
      else
        NGUITools.SetActiveSelf(this.detailsTextList.scrollBar.gameObject, false);
    }
  }

  private void LoadItemTexture(ItemIcon itemIcon)
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) itemIcon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.icon.mainTexture = (Texture) texture2D;
  }

  private void TryInitialize()
  {
    ItemSelectPopup inParents = NGUITools.FindInParents<ItemSelectPopup>(this.gameObject);
    if ((Object) inParents == (Object) null)
      return;
    this._itemSelectPopupContext = inParents.context;
    if (this._itemSelectPopupContext == null)
      return;
    this._itemSelectPopupContext = inParents.context;
    this._isInitialized = true;
  }

  [DebuggerHidden]
  private IEnumerator Start()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ItemSelectPopupSelectedItemDetailsCard.\u003CStart\u003Ec__Iterator1B()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnEnable()
  {
    if (!this._isStarted)
      return;
    if (!this._isInitialized)
      this.TryInitialize();
    if (!this._isInitialized)
      return;
    this.Show((EquipmentItem) null);
    this.StartCoroutine(this.UpdateCardCoroutine());
  }

  private void OnDisable()
  {
    this._isUpdating = false;
  }

  [DebuggerHidden]
  private IEnumerator UpdateCardCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ItemSelectPopupSelectedItemDetailsCard.\u003CUpdateCardCoroutine\u003Ec__Iterator1C()
    {
      \u003C\u003Ef__this = this
    };
  }
}
