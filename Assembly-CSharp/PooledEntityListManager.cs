﻿// Decompiled with JetBrains decompiler
// Type: PooledEntityListManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.Map;
using TheInfiniteBlack.Unity.SettingsUI;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class PooledEntityListManager : MonoBehaviour
{
  public static bool isScrolling;
  public bool listIsScrolling;
  private static PooledEntityListManager _instance;
  public Transform prefAsteroidCard;
  public Transform prefCapturePointCard;
  public Transform prefCargoCard;
  public Transform prefDefensePlatformCard;
  public Transform prefFighterCard;
  public Transform prefGarrisonCard;
  public Transform prefIntradictorCard;
  public Transform prefMinesCard;
  public Transform prefNpcCard;
  public Transform prefPlanetCard;
  public Transform prefRepairDroneCard;
  public Transform prefShipCard;
  public Transform prefStarPortCard;
  public UnitCardCellPooledScrollView listA;
  public UnitCardCellPooledScrollView listB;
  public UnitCardCellPooledScrollView listC;
  public UnitCardCellPooledScrollView listD;
  public EntityListScaler entityListScaler;
  private bool _isActiveListA;
  private bool _isActiveListB;
  private bool _isActiveListC;
  private bool _isActiveListD;
  private UnitCardCellInfoCollection _listA_Data;
  private UnitCardCellInfoCollection _listB_Data;
  private UnitCardCellInfoCollection _listC_Data;
  private UnitCardCellInfoCollection _listD_Data;
  private bool _isInitialized;
  private bool _resetScrollPosition;
  private int _nextUpdateFrame;

  public PooledEntityListManager Instance
  {
    get
    {
      return PooledEntityListManager._instance;
    }
    private set
    {
      PooledEntityListManager._instance = value;
    }
  }

  public bool isActiveListA
  {
    get
    {
      return this._isActiveListA;
    }
    set
    {
      this._isActiveListA = value;
      if (this.listA.gameObject.activeSelf == this._isActiveListA)
        return;
      this.listA.gameObject.SetActive(this._isActiveListA);
    }
  }

  public bool isActiveListB
  {
    get
    {
      return this._isActiveListB;
    }
    set
    {
      this._isActiveListB = value;
      if (this.listB.gameObject.activeSelf == this._isActiveListB)
        return;
      this.listB.gameObject.SetActive(this._isActiveListB);
    }
  }

  public bool isActiveListC
  {
    get
    {
      return this._isActiveListC;
    }
    set
    {
      this._isActiveListC = value;
      if (this.listC.gameObject.activeSelf == this._isActiveListC)
        return;
      this.listC.gameObject.SetActive(this._isActiveListC);
    }
  }

  public bool isActiveListD
  {
    get
    {
      return this._isActiveListD;
    }
    set
    {
      this._isActiveListD = value;
      if (this.listD.gameObject.activeSelf == this._isActiveListD)
        return;
      this.listD.gameObject.SetActive(this._isActiveListD);
    }
  }

  private void BindList(UnitCardCellPooledScrollView view, out UnitCardCellInfoCollection data)
  {
    data = new UnitCardCellInfoCollection()
    {
      prefAsteroidCard = this.prefAsteroidCard,
      prefCapturePointCard = this.prefCapturePointCard,
      prefCargoCard = this.prefCargoCard,
      prefDefensePlatformCard = this.prefDefensePlatformCard,
      prefFighterCard = this.prefFighterCard,
      prefGarrisonCard = this.prefGarrisonCard,
      prefIntradictorCard = this.prefIntradictorCard,
      prefMinesCard = this.prefMinesCard,
      prefNpcCard = this.prefNpcCard,
      prefPlanetCard = this.prefPlanetCard,
      prefRepairDroneCard = this.prefRepairDroneCard,
      prefShipCard = this.prefShipCard,
      prefStarPortCard = this.prefStarPortCard
    };
    view.data = data;
  }

  private void TryInitialize()
  {
    if (this._isInitialized || TibProxy.Instance == null || (TibProxy.gameState == null || TibProxy.mySettings == null))
      return;
    this.BindList(this.listA, out this._listA_Data);
    this.BindList(this.listB, out this._listB_Data);
    this.BindList(this.listC, out this._listC_Data);
    this.BindList(this.listD, out this._listD_Data);
    this.RefreshActiveLists(TibProxy.gameState, true);
    this._isInitialized = true;
  }

  private void RegisterEventHandlers()
  {
    if ((UnityEngine.Object) this.entityListScaler != (UnityEngine.Object) null)
    {
      this.entityListScaler.onUpdateScale -= new EventHandler(this.EntityListScaler_OnScaleChanged);
      this.entityListScaler.onUpdateScale += new EventHandler(this.EntityListScaler_OnScaleChanged);
    }
    TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_OnLoginSuccessEventHandler);
    TibProxy.Instance.onLoginSuccessEvent += new EventHandler<LoginSuccessEventArgs>(this.TibProxy_OnLoginSuccessEventHandler);
    TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_OnLocationChangedEventHandler);
    TibProxy.Instance.onLocationChangedEvent += new EventHandler<LocationChangedEventArgs>(this.TibProxy_OnLocationChangedEventHandler);
    TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.TibProxy_OnDisconnectEventHandler);
    TibProxy.Instance.onDisconnectEvent += new EventHandler<DisconnectEventArgs>(this.TibProxy_OnDisconnectEventHandler);
    PreInstantiatedSingleton<SectorMap>.Instance.controller.onScrollBegin -= new EventHandler(this.SectorMap_OnScrollBeginEventHandler);
    PreInstantiatedSingleton<SectorMap>.Instance.controller.onScrollBegin += new EventHandler(this.SectorMap_OnScrollBeginEventHandler);
    PreInstantiatedSingleton<SectorMap>.Instance.controller.onScrollEnd -= new EventHandler(this.SectorMap_OnScrollEndEventHandler);
    PreInstantiatedSingleton<SectorMap>.Instance.controller.onScrollEnd += new EventHandler(this.SectorMap_OnScrollEndEventHandler);
  }

  private void OnDestroy()
  {
    if ((UnityEngine.Object) this.entityListScaler != (UnityEngine.Object) null)
      this.entityListScaler.onUpdateScale -= new EventHandler(this.EntityListScaler_OnScaleChanged);
    TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_OnLoginSuccessEventHandler);
    TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_OnLocationChangedEventHandler);
    TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.TibProxy_OnDisconnectEventHandler);
    PreInstantiatedSingleton<SectorMap>.Instance.controller.onScrollBegin -= new EventHandler(this.SectorMap_OnScrollBeginEventHandler);
    PreInstantiatedSingleton<SectorMap>.Instance.controller.onScrollEnd -= new EventHandler(this.SectorMap_OnScrollEndEventHandler);
  }

  private void TibProxy_OnLoginSuccessEventHandler(object sender, LoginSuccessEventArgs e)
  {
    this._resetScrollPosition = true;
  }

  private void TibProxy_OnLocationChangedEventHandler(object sender, LocationChangedEventArgs e)
  {
    this._resetScrollPosition = true;
  }

  private void TibProxy_OnEntityArriveEventHandler(object sender, EntityArriveEventArgs e)
  {
  }

  private void TibProxy_OnEntityExitEventHandler(object sender, EntityExitEventArgs e)
  {
  }

  private void TibProxy_OnDisconnectEventHandler(object sender, DisconnectEventArgs e)
  {
  }

  private void SectorMap_OnScrollBeginEventHandler(object sender, EventArgs e)
  {
  }

  private void SectorMap_OnScrollEndEventHandler(object sender, EventArgs e)
  {
  }

  private void EntityListScaler_OnScaleChanged(object sender, EventArgs e)
  {
  }

  private void RefreshActiveLists(IGameState gameState, bool resetPosition)
  {
    if (this.isActiveListA)
    {
      this._listA_Data.RefreshList(gameState.Local.get_ColumnA());
      this.listA.resetPosition = resetPosition;
    }
    if (this.isActiveListB)
    {
      this._listB_Data.RefreshList(gameState.Local.get_ColumnB());
      this.listB.resetPosition = resetPosition;
    }
    if (this.isActiveListC)
    {
      this._listC_Data.RefreshList(gameState.Local.get_ColumnC());
      this.listC.resetPosition = resetPosition;
    }
    if (!this.isActiveListD)
      return;
    this._listD_Data.RefreshList(gameState.Local.get_ColumnD());
    this.listD.resetPosition = resetPosition;
  }

  private void Awake()
  {
    this.Instance = this;
  }

  private void Start()
  {
    if (this._isInitialized)
      return;
    this.TryInitialize();
  }

  private void OnEnable()
  {
    this.RegisterEventHandlers();
  }

  private void Update()
  {
    if (TibProxy.Instance == null || TibProxy.gameState == null)
      return;
    if (!this._isInitialized)
    {
      this.TryInitialize();
    }
    else
    {
      this.isActiveListA = TibProxy.mySettings.EntityList.ColumnA.Enabled;
      this.isActiveListB = TibProxy.mySettings.EntityList.ColumnB.Enabled;
      this.isActiveListC = TibProxy.mySettings.EntityList.ColumnC.Enabled;
      this.isActiveListD = TibProxy.mySettings.EntityList.ColumnD.Enabled;
      PooledEntityListManager.isScrolling = this.isActiveListA && this.listA.isScrolling || this.isActiveListB && this.listB.isScrolling || this.isActiveListC && this.listC.isScrolling || this.isActiveListD && this.listD.isScrolling;
      this.listIsScrolling = PooledEntityListManager.isScrolling;
      if (this._nextUpdateFrame > Time.frameCount)
        return;
      this.RefreshActiveLists(TibProxy.gameState, this._resetScrollPosition);
      this._resetScrollPosition = false;
      this._nextUpdateFrame = Time.frameCount + 2;
    }
  }
}
