﻿// Decompiled with JetBrains decompiler
// Type: TestReporter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestReporter : MonoBehaviour
{
  public int logTestCount = 100;
  public int threadLogTestCount = 100;
  public bool logEverySecond = true;
  private int currentLogTestCount;
  private Reporter reporter;
  private GUIStyle style;
  private Rect rect1;
  private Rect rect2;
  private Rect rect3;
  private Rect rect4;
  private Rect rect5;
  private Rect rect6;
  private Thread thread;
  private float elapsed;

  private void Start()
  {
    Application.runInBackground = true;
    this.reporter = Object.FindObjectOfType(typeof (Reporter)) as Reporter;
    Debug.Log((object) "test long text sdf asdfg asdfg sdfgsdfg sdfg sfgsdfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfg sdfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfg sdfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfg sdfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfg sdfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfg ssssssssssssssssssssssasdf asdf asdf asdf adsf \n dfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfasdf asdf asdf asdf adsf \n dfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfasdf asdf asdf asdf adsf \n dfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfasdf asdf asdf asdf adsf \n dfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdfasdf asdf asdf asdf adsf \n dfgsdfg sdfg sdf gsdfg sfdg sf gsdfg sdfg asdf");
    this.style = new GUIStyle();
    this.style.alignment = TextAnchor.MiddleCenter;
    this.style.normal.textColor = Color.white;
    this.style.wordWrap = true;
    for (int index = 0; index < 10; ++index)
    {
      Debug.Log((object) "Test Collapsed log");
      Debug.LogWarning((object) "Test Collapsed Warning");
      Debug.LogError((object) "Test Collapsed Error");
    }
    for (int index = 0; index < 10; ++index)
    {
      Debug.Log((object) "Test Collapsed log");
      Debug.LogWarning((object) "Test Collapsed Warning");
      Debug.LogError((object) "Test Collapsed Error");
    }
    this.rect1 = new Rect((float) (Screen.width / 2 - 120), (float) (Screen.height / 2 - 225), 240f, 50f);
    this.rect2 = new Rect((float) (Screen.width / 2 - 120), (float) (Screen.height / 2 - 175), 240f, 100f);
    this.rect3 = new Rect((float) (Screen.width / 2 - 120), (float) (Screen.height / 2 - 50), 240f, 50f);
    this.rect4 = new Rect((float) (Screen.width / 2 - 120), (float) (Screen.height / 2), 240f, 50f);
    this.rect5 = new Rect((float) (Screen.width / 2 - 120), (float) (Screen.height / 2 + 50), 240f, 50f);
    this.rect6 = new Rect((float) (Screen.width / 2 - 120), (float) (Screen.height / 2 + 100), 240f, 50f);
    this.thread = new Thread(new ThreadStart(this.threadLogTest));
    this.thread.Start();
  }

  private void OnDestroy()
  {
    this.thread.Abort();
  }

  private void threadLogTest()
  {
    for (int index = 0; index < this.threadLogTestCount; ++index)
    {
      Debug.Log((object) "Test Log from Thread");
      Debug.LogWarning((object) "Test Warning from Thread");
      Debug.LogError((object) "Test Error from Thread");
    }
  }

  private void Update()
  {
    for (int index = 0; this.currentLogTestCount < this.logTestCount && index < 10; ++this.currentLogTestCount)
    {
      Debug.Log((object) ("Test Log " + (object) this.currentLogTestCount));
      Debug.LogError((object) ("Test LogError " + (object) this.currentLogTestCount));
      Debug.LogWarning((object) ("Test LogWarning " + (object) this.currentLogTestCount));
      ++index;
    }
    this.elapsed += Time.deltaTime;
    if ((double) this.elapsed < 1.0)
      return;
    this.elapsed = 0.0f;
    Debug.Log((object) "One Second Passed");
  }

  private void OnGUI()
  {
    if (!(bool) ((Object) this.reporter) || this.reporter.show)
      return;
    GUI.Label(this.rect1, "Draw circle on screen to show logs", this.style);
    GUI.Label(this.rect2, "To use Reporter just create reporter from reporter menu at first scene your game start", this.style);
    if (GUI.Button(this.rect3, "Load ReporterScene"))
      SceneManager.LoadScene("ReporterScene");
    if (GUI.Button(this.rect4, "Load test1"))
      SceneManager.LoadScene("test1");
    if (GUI.Button(this.rect5, "Load test2"))
      SceneManager.LoadScene("test2");
    GUI.Label(this.rect6, "fps : " + this.reporter.fps.ToString("0.0"), this.style);
  }
}
