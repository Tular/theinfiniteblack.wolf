﻿// Decompiled with JetBrains decompiler
// Type: UIMessageBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIMessageBox : MonoBehaviour
{
  public UIPanel panel;
  public UILabel title;
  public UILabel content;
  private static UIMessageBox mInst;

  private void Awake()
  {
    UIMessageBox.mInst = this;
  }

  private void OnDestroy()
  {
    if (!((Object) UIMessageBox.mInst == (Object) this))
      return;
    UIMessageBox.mInst = (UIMessageBox) null;
  }

  public static void Show(string title, string content)
  {
    if (!((Object) UIMessageBox.mInst != (Object) null))
      return;
    UIMessageBox.mInst.title.text = title;
    UIMessageBox.mInst.content.text = content;
    UIWindow.Show(UIMessageBox.mInst.panel);
  }
}
