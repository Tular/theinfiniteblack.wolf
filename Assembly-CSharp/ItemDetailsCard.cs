﻿// Decompiled with JetBrains decompiler
// Type: ItemDetailsCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class ItemDetailsCard : MonoBehaviour
{
  public string resourcePath = "items";
  public UITexture icon;
  public UILabel title;
  public UILabel subTitle;
  public UILabel description;
  public UILabel equipPoints;
  public UILabel durability;
  public UILabel creditValue;
  public UITextList detailsTextList;
  private int _lastUpdateFrame;

  public ItemDetailsContext dialogContext { get; set; }

  private void UpdateVisuals()
  {
    if (this.dialogContext == null || this.dialogContext.engineeredItem == null)
      return;
    EquipmentItem engineeredItem = this.dialogContext.engineeredItem;
    this.LoadItemTexture(engineeredItem.Icon);
    StringBuilder stringBuilder1 = new StringBuilder(100);
    StringBuilder stringBuilder2 = new StringBuilder(100);
    StringBuilder stringBuilder3 = new StringBuilder(500);
    engineeredItem.AppendName(stringBuilder1);
    Markup.GetNGUI(stringBuilder1);
    this.title.text = stringBuilder1.ToString();
    stringBuilder2.Append(engineeredItem.Rarity.GetMarkup());
    engineeredItem.AppendSubTitle(stringBuilder2);
    Markup.GetNGUI(stringBuilder2);
    this.subTitle.text = stringBuilder2.ToString();
    engineeredItem.AppendDescription(stringBuilder3);
    Markup.GetNGUI(stringBuilder3);
    this.description.text = stringBuilder3.ToString();
    this.equipPoints.text = string.Format("{0}", (object) engineeredItem.EPCost);
    this.durability.text = string.Format("{0}%", (object) engineeredItem.Durability);
    this.creditValue.text = string.Format("{0:$###,###,###}", (object) engineeredItem.ActualCreditValue);
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.AppendFormat("{0}", (object) "[g]");
    string newValue1 = "\n[g]";
    stringBuilder4.AppendLine(engineeredItem.Pros.Replace("\n", newValue1));
    stringBuilder4.AppendFormat("{0}", (object) "[r]");
    string newValue2 = "\n[r]";
    stringBuilder4.AppendLine(engineeredItem.Cons.Replace("\n", newValue2));
    Markup.GetNGUI(stringBuilder4);
    if ((double) ((UIScrollBar) this.detailsTextList.scrollBar).barSize < 1.0)
      NGUITools.SetActiveSelf(this.detailsTextList.scrollBar.gameObject, true);
    else
      NGUITools.SetActiveSelf(this.detailsTextList.scrollBar.gameObject, false);
    this.detailsTextList.Clear();
    this.detailsTextList.Add(stringBuilder4.ToString());
    this.detailsTextList.scrollValue = 0.0f;
  }

  private void ResetVisuals()
  {
    if ((bool) ((Object) this.detailsTextList))
      this.detailsTextList.Clear();
    this.title.text = string.Empty;
    this.subTitle.text = string.Empty;
    this.description.text = string.Empty;
    this.equipPoints.text = string.Empty;
    this.durability.text = string.Empty;
    this.creditValue.text = string.Empty;
    this.icon.mainTexture = (Texture) null;
  }

  private void LoadItemTexture(ItemIcon itemIcon)
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) itemIcon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.icon.mainTexture = (Texture) texture2D;
  }

  private void OnEnable()
  {
    this.ResetVisuals();
  }

  private void Update()
  {
    if (this.dialogContext == null || this._lastUpdateFrame >= this.dialogContext.lastUpdateFrame)
      return;
    this.UpdateVisuals();
    this._lastUpdateFrame = Time.frameCount;
  }

  private void OnDisable()
  {
    this.ResetVisuals();
  }
}
