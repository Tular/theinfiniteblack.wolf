﻿// Decompiled with JetBrains decompiler
// Type: Swipe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class Swipe : MonoBehaviour
{
  public GameObject trail;
  public Text swipeText;

  private void OnEnable()
  {
    EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.On_SwipeStart);
    EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.On_SwipeStart);
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
  }

  private void On_SwipeStart(Gesture gesture)
  {
    this.swipeText.text = "You start a swipe";
  }

  private void On_Swipe(Gesture gesture)
  {
    this.trail.transform.position = gesture.GetTouchToWorldPoint(5f);
  }

  private void On_SwipeEnd(Gesture gesture)
  {
    float swipeOrDragAngle = gesture.GetSwipeOrDragAngle();
    this.swipeText.text = "Last swipe : " + gesture.swipe.ToString() + " /  vector : " + (object) gesture.swipeVector.normalized + " / angle : " + swipeOrDragAngle.ToString("f2");
  }
}
