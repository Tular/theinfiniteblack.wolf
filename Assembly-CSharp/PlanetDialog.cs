﻿// Decompiled with JetBrains decompiler
// Type: PlanetDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;

public class PlanetDialog : DialogBase
{
  public UILabel title;
  public UILabel subtitle;
  public PlanetTerraformingView terraformingView;
  public PlanetDialogShipFactoryView shipFactoryView;
  public PlanetShipListingView shipListingView;
  public ShipProfileTexture shipProfile;
  private PlanetDialogContext _dialogContext;

  public void ShowFactoryView()
  {
    this.terraformingView.Hide();
    this.shipListingView.Show();
    this.shipFactoryView.Show();
  }

  public void ShowTerraformingView()
  {
    this.shipFactoryView.Hide();
    this.shipListingView.Hide();
    this.terraformingView.Show();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this._dialogContext = new PlanetDialogContext();
    this.shipFactoryView.dialogContext = this._dialogContext;
    this.shipListingView.dialogContext = this._dialogContext;
    this.terraformingView.dialogContext = this._dialogContext;
    NGUITools.SetActiveSelf(this.shipFactoryView.gameObject, false);
    NGUITools.SetActiveSelf(this.shipListingView.gameObject, false);
    NGUITools.SetActiveSelf(this.terraformingView.gameObject, false);
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
    TibProxy.Instance.onShowEntityDetailEvent += new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  protected override void OnShow()
  {
    base.OnShow();
    this.ShowFactoryView();
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.terraformingView.Hide();
    this.shipFactoryView.Hide();
    this.shipListingView.Hide();
    this.shipProfile.Unload();
    this._dialogContext.planet = (Planet) null;
  }

  private void TibProxy_onShowEntityDetailEvent(object sender, ShowEntityDetailEventArgs e)
  {
    if (e.Source.Type != EntityType.PLANET)
      return;
    this._dialogContext.planet = (Planet) e.Source;
    this.Show();
  }

  private void Update()
  {
    if (TibProxy.gameState == null || TibProxy.gameState.Local.Planet == null || TibProxy.gameState.Local.Planet != this._dialogContext.planet)
      this.Hide();
    else
      this.subtitle.text = this._dialogContext.dialogSubtitleText;
  }
}
