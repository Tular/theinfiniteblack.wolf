﻿// Decompiled with JetBrains decompiler
// Type: UILimitedSavedOption
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIPopupList))]
public class UILimitedSavedOption : MonoBehaviour
{
  public string keyName = "Key Name";
  public string[] validChoices;
  private UIPopupList mList;

  private bool IsValid(string choice)
  {
    for (int index = 0; index < this.validChoices.Length; ++index)
    {
      if (this.validChoices[index] == choice)
        return true;
    }
    return false;
  }

  private void Awake()
  {
    this.mList = this.GetComponent<UIPopupList>();
    EventDelegate.Add(this.mList.onChange, new EventDelegate.Callback(this.OnSelection));
  }

  private void OnEnable()
  {
    string str = PlayerPrefs.GetString(this.keyName);
    if (string.IsNullOrEmpty(str))
      return;
    this.mList.value = str;
  }

  private void OnSelection()
  {
    if (PlayerProfile.fullAccess || this.IsValid(UIPopupList.current.value))
    {
      PlayerPrefs.SetString(this.keyName, UIPopupList.current.value);
    }
    else
    {
      this.mList.value = this.validChoices[this.validChoices.Length - 1];
      UIUpgradeWindow.Show();
    }
  }
}
