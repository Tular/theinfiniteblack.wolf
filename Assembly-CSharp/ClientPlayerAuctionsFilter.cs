﻿// Decompiled with JetBrains decompiler
// Type: ClientPlayerAuctionsFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;

public class ClientPlayerAuctionsFilter : IAuctionFilter
{
  public ClientPlayerAuctionsFilter(ClientPlayer clientPlayer, ClientPlayerAuctionsFilter.AuctionRole role)
  {
    this.player = clientPlayer;
    this.playerRole = role;
  }

  public ClientPlayerAuctionsFilter.AuctionRole playerRole { get; private set; }

  public ClientPlayer player { get; private set; }

  public string formattedName
  {
    get
    {
      if (this.player == null)
        return "Player is null";
      if (this.player == TibProxy.gameState.MyPlayer)
        return "MY POSTED AUCTIONS";
      return string.Format("{0}'s POSTED AUCTIONS", (object) this.player.Name);
    }
  }

  public IEnumerable<ClientAuction> GetFilteredAuctions(IEnumerable<ClientAuction> auctions)
  {
    if (this.player == null)
      return (IEnumerable<ClientAuction>) null;
    if (this.playerRole == ClientPlayerAuctionsFilter.AuctionRole.Owner)
      return (IEnumerable<ClientAuction>) auctions.Where<ClientAuction>((Func<ClientAuction, bool>) (i => i.Owner == this.player)).OrderBy<ClientAuction, long>((Func<ClientAuction, long>) (i => i.Time.RemainingMS));
    return (IEnumerable<ClientAuction>) auctions.Where<ClientAuction>((Func<ClientAuction, bool>) (i => i.Bidder == this.player)).OrderBy<ClientAuction, long>((Func<ClientAuction, long>) (i => i.Time.RemainingMS));
  }

  public void ResetFilter()
  {
    this.player = (ClientPlayer) null;
  }

  public enum AuctionRole
  {
    Bidder,
    Owner,
  }
}
