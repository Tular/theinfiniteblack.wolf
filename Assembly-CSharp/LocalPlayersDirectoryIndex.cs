﻿// Decompiled with JetBrains decompiler
// Type: LocalPlayersDirectoryIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LocalPlayersDirectoryIndex : PlayerDirectoryIndex
{
  private readonly PlayerDirectoryCellInfo _sectorHeadingInfo;

  public LocalPlayersDirectoryIndex(Transform headingPrefab)
    : base(headingPrefab)
  {
    this._sectorHeadingInfo = new PlayerDirectoryCellInfo(this.mHeadingPrefab, "- SECTOR -");
  }

  [DebuggerHidden]
  protected override IEnumerable<PlayerDirectoryCellInfo> FilteredValues()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    LocalPlayersDirectoryIndex.\u003CFilteredValues\u003Ec__Iterator13 valuesCIterator13 = new LocalPlayersDirectoryIndex.\u003CFilteredValues\u003Ec__Iterator13()
    {
      \u003C\u003Ef__this = this
    };
    // ISSUE: reference to a compiler-generated field
    valuesCIterator13.\u0024PC = -2;
    return (IEnumerable<PlayerDirectoryCellInfo>) valuesCIterator13;
  }

  protected override void OnClear()
  {
  }
}
