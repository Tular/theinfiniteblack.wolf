﻿// Decompiled with JetBrains decompiler
// Type: DialogTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class DialogTester : MonoBehaviour
{
  public DialogBase testDialog;

  [ContextMenu("Show Dialog")]
  public void ShowDialog()
  {
    ((object) this.testDialog).GetType();
  }

  [ContextMenu("Show Dialog As Popup")]
  public void ShowDialogAsPopup()
  {
    ((object) this.testDialog).GetType();
  }
}
