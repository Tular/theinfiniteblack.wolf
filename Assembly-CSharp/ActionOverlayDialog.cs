﻿// Decompiled with JetBrains decompiler
// Type: ActionOverlayDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[RequireComponent(typeof (UIWidgetDepthGroupController))]
public class ActionOverlayDialog : DialogBase
{
  public int portraitSize = 128;
  public string shipSpritePrefix = "entity_ship_";
  public int baseToggleGroup = 100;
  public UITexture portrait;
  public UILabel title;
  public UILabel subtitle;
  public UILabel shipClass;
  public UILabel level;
  public UILabel levelProgress;
  public UILabel totalXp;
  public UISprite shipIcon;
  public GameObject resourcesRoot;
  public UILabel metals;
  public UILabel organics;
  public UILabel gas;
  public UILabel radioactives;
  public UILabel darkmatter;
  public ActionOverlayDialogButton button_1_1;
  public ActionOverlayDialogButton button_1_2;
  public ActionOverlayDialogButton button_1_3;
  public ActionOverlayDialogButton button_2_1;
  public ActionOverlayDialogButton button_2_2;
  public ActionOverlayDialogButton button_2_3;
  public UIGrid buttonGrid1;
  public UIGrid buttonGrid2;
  private UIWidgetDepthGroupController _depthController;

  public void ShowLeaderboardPage()
  {
    InlineWebBrowser.Show(WebFiles.GetPlayerLeaderboardUrl(this.title.text, TibProxy.Instance.serverId));
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  protected override void OnAwake()
  {
    this._depthController = this.GetComponent<UIWidgetDepthGroupController>();
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
    TibProxy.Instance.onShowEntityDetailEvent += new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
    this.title.text = string.Empty;
    this.subtitle.text = string.Empty;
    this.metals.text = string.Empty;
    this.organics.text = string.Empty;
    this.gas.text = string.Empty;
    this.radioactives.text = string.Empty;
    this.darkmatter.text = string.Empty;
    this.shipIcon.spriteName = string.Empty;
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.title.text = string.Empty;
    this.subtitle.text = string.Empty;
    this.level.text = string.Empty;
    this.metals.text = string.Empty;
    this.organics.text = string.Empty;
    this.gas.text = string.Empty;
    this.radioactives.text = string.Empty;
    this.darkmatter.text = string.Empty;
    this.shipIcon.spriteName = string.Empty;
    this.UnloadPortrait();
    this.button_1_1.Hide();
    this.button_1_2.Hide();
    this.button_1_3.Hide();
    this.button_2_1.Hide();
    this.button_2_2.Hide();
    this.button_2_3.Hide();
  }

  private void Start()
  {
    this._depthController.SetToBase();
  }

  private void TibProxy_onShowEntityDetailEvent(object sender, ShowEntityDetailEventArgs e)
  {
    if (e.Source.Type != EntityType.SHIP)
      return;
    Ship source = (Ship) e.Source;
    this.Show();
    this.SetContext(source);
    this.StartCoroutine(this.LoadPortrait(source.Player.Name));
  }

  private void SetContext(Ship ship)
  {
    this.title.text = string.Format("{0}", (object) ship.Title);
    this.subtitle.text = ship.SubTitle;
    this.shipClass.text = ((Enum) ship.Class).ToString();
    if ((double) ship.Level <= 400.0)
    {
      int index = (int) ship.Level + 1;
      int num = LevelHelper.LevelXP[index] - ship.XP;
      this.level.text = string.Format("LVL {0:F2}", (object) ship.Level);
      this.levelProgress.text = string.Format("{0:###,###} XP to Level {1}", (object) num, (object) index);
    }
    else
    {
      this.level.text = string.Format("LVL {0:F2} (MAX)", (object) ship.Level);
      this.levelProgress.text = string.Empty;
    }
    this.totalXp.text = string.Format("{0:###,###} Total Xp", (object) ship.XP);
    this.metals.text = string.Format("{0}", (object) ship.Metals);
    this.organics.text = string.Format("{0}", (object) ship.Organics);
    this.gas.text = string.Format("{0}", (object) ship.Gas);
    this.radioactives.text = string.Format("{0}", (object) ship.Radioactives);
    this.darkmatter.text = string.Format("{0}", (object) ship.DarkMatter);
    this.shipIcon.spriteName = string.Format("{0}{1}", (object) this.shipSpritePrefix, (object) ship.Class);
    Color color1 = new Color(0.8627451f, 0.1764706f, 0.2745098f, 1f);
    Color color2 = new Color(0.4784314f, 0.2431373f, 0.4941176f, 1f);
    switch (ship.Relation)
    {
      case RelationType.NEUTRAL:
        this.button_1_1.Show("ATTACK", "gen_attack", true).SetIconColors(color1, color1, color1, Color.clear).InitializeToggleState(0, TibProxy.gameState.MyAttackTarget == ship, (EventDelegate.Callback) (() => TibProxy.gameState.DoAttack((CombatEntity) ship)), (EventDelegate.Callback) (() => TibProxy.gameState.DoAttack((CombatEntity) null))).SetHandleToggleChange((System.Action<ActionOverlayDialogButton>) (btn =>
        {
          bool selected = TibProxy.gameState.MyAttackTarget == ship;
          btn.SetToggleState(selected);
        }));
        this.button_1_2.Show("FOLLOW", "gen_follow", true).SetIconColors(color2, color2, color2, Color.clear).InitializeToggleState(0, TibProxy.gameState.MyFollowTarget == ship, (EventDelegate.Callback) (() => TibProxy.gameState.DoFollow((CombatEntity) ship)), (EventDelegate.Callback) (() => TibProxy.gameState.DoFollow((CombatEntity) null))).SetHandleToggleChange((System.Action<ActionOverlayDialogButton>) (btn =>
        {
          bool selected = TibProxy.gameState.MyFollowTarget == ship;
          btn.SetToggleState(selected);
        }));
        this.button_1_3.Show("REPAIR", "gen_repair").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => TibProxy.gameState.DoRepair((PlayerCombatEntity) ship)));
        this.button_2_1.Show("INFO", "gen_scan").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<ShipDetailsDialog>(new object[1]
        {
          (object) ship
        })));
        this.button_2_2.Show("TRANSFER", "gen_trade").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<TransferResourcesDialog>(new object[1]
        {
          (object) ship
        })));
        this.button_2_3.Show("TRADE", "gen_shopping").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() =>
        {
          DialogManager.CloseAllDialogs();
          DialogManager.ShowDialog<TradeDialog>(new object[1]
          {
            (object) ship.Player
          });
        }));
        break;
      case RelationType.SELF:
        this.button_1_1.Show("REPAIR", "gen_repair").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => TibProxy.gameState.DoRepair((PlayerCombatEntity) TibProxy.gameState.MyShip)));
        this.button_1_2.Show("INVENTORY", "gen_gear").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<ItemManagementDialog>((object) TibProxy.gameState.MyShip, (object) ItemManagementDialog.DisplayMode.Inventory)));
        this.button_1_3.Hide();
        this.button_2_1.Show("MY INFO", "gen_scan").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<ShipDetailsDialog>(new object[1]
        {
          (object) ship
        })));
        this.button_2_2.Show("AUCTION", "gen_auction").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<AuctionHouseDialog>(false)));
        this.button_2_3.Show("JETTISON", "gen_jettison").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<JettisonResourcesDialog>(false)));
        break;
      case RelationType.FRIEND:
        this.button_1_1.Show("REPAIR", "gen_repair").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => TibProxy.gameState.DoRepair((PlayerCombatEntity) ship)));
        this.button_1_2.Show("FOLLOW", "gen_follow", true).SetIconColors(color2, color2, color2, Color.clear).SetIconColors(Color.white, Color.black, Color.white, Color.clear).InitializeToggleState(0, TibProxy.gameState.MyFollowTarget == ship, (EventDelegate.Callback) (() => TibProxy.gameState.DoFollow((CombatEntity) ship)), (EventDelegate.Callback) (() => TibProxy.gameState.DoFollow((CombatEntity) null))).SetHandleToggleChange((System.Action<ActionOverlayDialogButton>) (btn =>
        {
          bool selected = TibProxy.gameState.MyFollowTarget == ship;
          btn.SetToggleState(selected);
        }));
        this.button_1_3.Hide();
        this.button_2_1.Show("INFO", "gen_scan").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<ShipDetailsDialog>(new object[1]
        {
          (object) ship
        })));
        this.button_2_2.Show("TRANSFER", "gen_trade").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<TransferResourcesDialog>(new object[1]
        {
          (object) ship
        })));
        this.button_2_3.Show("TRADE", "gen_shopping").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() =>
        {
          DialogManager.CloseAllDialogs();
          DialogManager.ShowDialog<TradeDialog>(new object[1]
          {
            (object) ship.Player
          });
        }));
        break;
      case RelationType.ENEMY:
        this.button_1_1.Show("ATTACK", "gen_attack", true).SetIconColors(color1, Color.black, color1, Color.clear).InitializeToggleState(0, TibProxy.gameState.MyAttackTarget == ship, (EventDelegate.Callback) (() => TibProxy.gameState.DoAttack((CombatEntity) ship)), (EventDelegate.Callback) (() => TibProxy.gameState.DoAttack((CombatEntity) null))).SetHandleToggleChange((System.Action<ActionOverlayDialogButton>) (btn =>
        {
          bool selected = TibProxy.gameState.MyAttackTarget == ship;
          btn.SetToggleState(selected);
        })).SetState(TibProxy.gameState.MyAttackTarget != ship ? SBUIButtonComponent.State.Normal : SBUIButtonComponent.State.Pressed);
        this.button_1_2.Show("FOLLOW", "gen_follow", true).SetIconColors(color2, color2, color2, Color.clear).InitializeToggleState(0, TibProxy.gameState.MyFollowTarget == ship, (EventDelegate.Callback) (() => TibProxy.gameState.DoFollow((CombatEntity) ship)), (EventDelegate.Callback) (() => TibProxy.gameState.DoFollow((CombatEntity) null))).SetHandleToggleChange((System.Action<ActionOverlayDialogButton>) (btn =>
        {
          bool selected = TibProxy.gameState.MyFollowTarget == ship;
          btn.SetToggleState(selected);
        }));
        this.button_1_3.Hide();
        this.button_2_1.Show("INFO", "gen_scan").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<ShipDetailsDialog>(new object[1]
        {
          (object) ship
        })));
        this.button_2_2.Show("TRANSFER", "gen_trade").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() => DialogManager.ShowDialog<TransferResourcesDialog>(new object[1]
        {
          (object) ship
        })));
        this.button_2_3.Show("TRADE", "gen_shopping").SetIconColors(Color.white, Color.black, Color.white, Color.clear).AddOnClickDelegate((EventDelegate.Callback) (() =>
        {
          DialogManager.CloseAllDialogs();
          DialogManager.ShowDialog<TradeDialog>(new object[1]
          {
            (object) ship.Player
          });
        }));
        break;
    }
    this.buttonGrid1.Reposition();
    this.buttonGrid2.Reposition();
  }

  [DebuggerHidden]
  private IEnumerator LoadPortrait(string playerName)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ActionOverlayDialog.\u003CLoadPortrait\u003Ec__IteratorC()
    {
      playerName = playerName,
      \u003C\u0024\u003EplayerName = playerName,
      \u003C\u003Ef__this = this
    };
  }

  private void UnloadPortrait()
  {
    if ((UnityEngine.Object) this.portrait.mainTexture == (UnityEngine.Object) null)
      ;
  }
}
