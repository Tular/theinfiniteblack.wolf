﻿// Decompiled with JetBrains decompiler
// Type: ChatEventListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class ChatEventListener : MonoBehaviour
{
  public UITextList chatOutput;
  public UITextList debugOutput;

  protected void Start()
  {
    TheInfiniteBlack.Library.Log.I((object) this, nameof (Start), string.Empty);
  }

  protected void Update()
  {
  }

  protected void OnEnable()
  {
    TheInfiniteBlack.Library.Log.I((object) this, nameof (OnEnable), string.Empty);
    TibProxy.Instance.onChatEvent += new EventHandler<ChatEventArgs>(this.OnChatEvent);
  }

  protected void OnDisable()
  {
    TheInfiniteBlack.Library.Log.I((object) this, nameof (OnDisable), string.Empty);
    TibProxy.Instance.onChatEvent -= new EventHandler<ChatEventArgs>(this.OnChatEvent);
  }

  public void OnChatEvent(object sender, ChatEventArgs e)
  {
    StringBuilder message = new StringBuilder();
    e.AppendText(message);
    this.chatOutput.Add(ChatEventListener.ReFormatMarkup(message));
  }

  protected static string ReFormatMarkup(StringBuilder message)
  {
    message.Append("[-]");
    return message.ToString();
  }
}
