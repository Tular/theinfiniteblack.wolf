﻿// Decompiled with JetBrains decompiler
// Type: ETCInput
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ETCInput : MonoBehaviour
{
  private Dictionary<string, ETCAxis> axes = new Dictionary<string, ETCAxis>();
  private Dictionary<string, ETCBase> controls = new Dictionary<string, ETCBase>();
  public static ETCInput _instance;
  private static ETCBase control;
  private static ETCAxis axis;

  public static ETCInput instance
  {
    get
    {
      if (!(bool) ((Object) ETCInput._instance))
      {
        ETCInput._instance = Object.FindObjectOfType(typeof (ETCInput)) as ETCInput;
        if (!(bool) ((Object) ETCInput._instance))
          ETCInput._instance = new GameObject("InputManager").AddComponent<ETCInput>();
      }
      return ETCInput._instance;
    }
  }

  public void RegisterControl(ETCBase ctrl)
  {
    if (this.controls.ContainsKey(ctrl.name))
    {
      Debug.LogWarning((object) ("ETCInput control : " + ctrl.name + " already exists"));
    }
    else
    {
      this.controls.Add(ctrl.name, ctrl);
      if (((object) ctrl).GetType() == typeof (ETCJoystick))
      {
        this.RegisterAxis((ctrl as ETCJoystick).axisX);
        this.RegisterAxis((ctrl as ETCJoystick).axisY);
      }
      else if (((object) ctrl).GetType() == typeof (ETCTouchPad))
      {
        this.RegisterAxis((ctrl as ETCTouchPad).axisX);
        this.RegisterAxis((ctrl as ETCTouchPad).axisY);
      }
      else if (((object) ctrl).GetType() == typeof (ETCDPad))
      {
        this.RegisterAxis((ctrl as ETCDPad).axisX);
        this.RegisterAxis((ctrl as ETCDPad).axisY);
      }
      else
      {
        if (((object) ctrl).GetType() != typeof (ETCButton))
          return;
        this.RegisterAxis((ctrl as ETCButton).axis);
      }
    }
  }

  public void UnRegisterControl(ETCBase ctrl)
  {
    if (!this.controls.ContainsKey(ctrl.name) || !ctrl.enabled)
      return;
    this.controls.Remove(ctrl.name);
    if (((object) ctrl).GetType() == typeof (ETCJoystick))
    {
      this.UnRegisterAxis((ctrl as ETCJoystick).axisX);
      this.UnRegisterAxis((ctrl as ETCJoystick).axisY);
    }
    else if (((object) ctrl).GetType() == typeof (ETCTouchPad))
    {
      this.UnRegisterAxis((ctrl as ETCTouchPad).axisX);
      this.UnRegisterAxis((ctrl as ETCTouchPad).axisY);
    }
    else if (((object) ctrl).GetType() == typeof (ETCDPad))
    {
      this.UnRegisterAxis((ctrl as ETCDPad).axisX);
      this.UnRegisterAxis((ctrl as ETCDPad).axisY);
    }
    else
    {
      if (((object) ctrl).GetType() != typeof (ETCButton))
        return;
      this.UnRegisterAxis((ctrl as ETCButton).axis);
    }
  }

  public void Create()
  {
  }

  public static void Register(ETCBase ctrl)
  {
    ETCInput.instance.RegisterControl(ctrl);
  }

  public static void UnRegister(ETCBase ctrl)
  {
    ETCInput.instance.UnRegisterControl(ctrl);
  }

  public static void SetControlVisible(string ctrlName, bool value)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      ETCInput.control.visible = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
  }

  public static bool GetControlVisible(string ctrlName)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      return ETCInput.control.visible;
    Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
    return false;
  }

  public static void SetControlActivated(string ctrlName, bool value)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      ETCInput.control.activated = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
  }

  public static bool GetControlActivated(string ctrlName)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      return ETCInput.control.activated;
    Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
    return false;
  }

  public static void SetControlSwipeIn(string ctrlName, bool value)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      ETCInput.control.isSwipeIn = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
  }

  public static bool GetControlSwipeIn(string ctrlName)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      return ETCInput.control.isSwipeIn;
    Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
    return false;
  }

  public static void SetControlSwipeOut(string ctrlName, bool value)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      ETCInput.control.isSwipeOut = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
  }

  public static bool GetControlSwipeOut(string ctrlName, bool value)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      return ETCInput.control.isSwipeOut;
    Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
    return false;
  }

  public static void SetDPadAxesCount(string ctrlName, ETCBase.DPadAxis value)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      ETCInput.control.dPadAxisCount = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
  }

  public static ETCBase.DPadAxis GetDPadAxesCount(string ctrlName)
  {
    if (ETCInput.instance.controls.TryGetValue(ctrlName, out ETCInput.control))
      return ETCInput.control.dPadAxisCount;
    Debug.LogWarning((object) ("ETCInput : " + ctrlName + " doesn't exist"));
    return ETCBase.DPadAxis.Two_Axis;
  }

  public static void ResetAxis(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
    {
      ETCInput.axis.axisValue = 0.0f;
      ETCInput.axis.axisSpeedValue = 0.0f;
    }
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static void SetAxisEnabled(string axisName, bool value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.enable = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static bool GetAxisEnabled(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.enable;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return false;
  }

  public static void SetAxisInverted(string axisName, bool value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.invertedAxis = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static bool GetAxisInverted(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.invertedAxis;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return false;
  }

  public static void SetAxisDeadValue(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.deadValue = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisDeadValue(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.deadValue;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisSensitivity(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.speed = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisSensitivity(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.speed;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisThreshold(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.axisThreshold = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisThreshold(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisThreshold;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisInertia(string axisName, bool value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.isEnertia = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static bool GetAxisInertia(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.isEnertia;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return false;
  }

  public static void SetAxisInertiaSpeed(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.inertia = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisInertiaSpeed(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.inertia;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisInertiaThreshold(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.inertiaThreshold = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisInertiaThreshold(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.inertiaThreshold;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisAutoStabilization(string axisName, bool value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.isAutoStab = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static bool GetAxisAutoStabilization(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.isAutoStab;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return false;
  }

  public static void SetAxisAutoStabilizationSpeed(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.autoStabSpeed = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisAutoStabilizationSpeed(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.autoStabSpeed;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisAutoStabilizationThreshold(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.autoStabThreshold = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisAutoStabilizationThreshold(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.autoStabThreshold;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisClampRotation(string axisName, bool value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.isClampRotation = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static bool GetAxisClampRotation(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.isClampRotation;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return false;
  }

  public static void SetAxisClampRotationValue(string axisName, float min, float max)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
    {
      ETCInput.axis.minAngle = min;
      ETCInput.axis.maxAngle = max;
    }
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static void SetAxisClampRotationMinValue(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.minAngle = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static void SetAxisClampRotationMaxValue(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.maxAngle = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisClampRotationMinValue(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.minAngle;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static float GetAxisClampRotationMaxValue(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.maxAngle;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisDirecTransform(string axisName, Transform value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.directTransform = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static Transform GetAxisDirectTransform(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.directTransform;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return (Transform) null;
  }

  public static void SetAxisDirectAction(string axisName, ETCAxis.DirectAction value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.directAction = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static ETCAxis.DirectAction GetAxisDirectAction(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.directAction;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return ETCAxis.DirectAction.Rotate;
  }

  public static void SetAxisAffectedAxis(string axisName, ETCAxis.AxisInfluenced value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.axisInfluenced = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static ETCAxis.AxisInfluenced GetAxisAffectedAxis(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisInfluenced;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return ETCAxis.AxisInfluenced.X;
  }

  public static void SetAxisOverTime(string axisName, bool value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.isValueOverTime = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static bool GetAxisOverTime(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.isValueOverTime;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return false;
  }

  public static void SetAxisOverTimeStep(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.overTimeStep = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisOverTimeStep(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.overTimeStep;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static void SetAxisOverTimeMaxValue(string axisName, float value)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      ETCInput.axis.maxOverTimeValue = value;
    else
      Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
  }

  public static float GetAxisOverTimeMaxValue(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.maxOverTimeValue;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return -1f;
  }

  public static float GetAxis(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisValue;
    Debug.LogWarning((object) ("ETCInput : " + axisName + " doesn't exist"));
    return 0.0f;
  }

  public static float GetAxisSpeed(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisSpeedValue;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return 0.0f;
  }

  public static bool GetAxisDownUp(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.DownUp;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetAxisDownDown(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.DownDown;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetAxisDownRight(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.DownRight;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetAxisDownLeft(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.DownLeft;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetAxisPressedUp(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.PressUp;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetAxisPressedDown(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.PressDown;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetAxisPressedRight(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.PressRight;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetAxisPressedLeft(string axisName)
  {
    if (ETCInput.instance.axes.TryGetValue(axisName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.PressLeft;
    Debug.LogWarning((object) (axisName + " doesn't exist"));
    return false;
  }

  public static bool GetButtonDown(string buttonName)
  {
    if (ETCInput.instance.axes.TryGetValue(buttonName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.Down;
    Debug.LogWarning((object) (buttonName + " doesn't exist"));
    return false;
  }

  public static bool GetButton(string buttonName)
  {
    if (ETCInput.instance.axes.TryGetValue(buttonName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.Down || ETCInput.axis.axisState == ETCAxis.AxisState.Press;
    Debug.LogWarning((object) (buttonName + " doesn't exist"));
    return false;
  }

  public static bool GetButtonUp(string buttonName)
  {
    if (ETCInput.instance.axes.TryGetValue(buttonName, out ETCInput.axis))
      return ETCInput.axis.axisState == ETCAxis.AxisState.Up;
    Debug.LogWarning((object) (buttonName + " doesn't exist"));
    return false;
  }

  public static float GetButtonValue(string buttonName)
  {
    if (ETCInput.instance.axes.TryGetValue(buttonName, out ETCInput.axis))
      return ETCInput.axis.axisValue;
    Debug.LogWarning((object) (buttonName + " doesn't exist"));
    return -1f;
  }

  private void RegisterAxis(ETCAxis axis)
  {
    if (ETCInput.instance.axes.ContainsKey(axis.name))
      Debug.LogWarning((object) ("ETCInput axis : " + axis.name + " already exists"));
    else
      this.axes.Add(axis.name, axis);
  }

  private void UnRegisterAxis(ETCAxis axis)
  {
    if (!ETCInput.instance.axes.ContainsKey(axis.name))
      return;
    this.axes.Remove(axis.name);
  }
}
