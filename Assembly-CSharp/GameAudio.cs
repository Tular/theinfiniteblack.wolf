﻿// Decompiled with JetBrains decompiler
// Type: GameAudio
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (AudioListener))]
public class GameAudio : MonoBehaviour
{
  private static GameAudio mInst;
  private Transform mTrans;
  private Transform mTarget;

  private void Awake()
  {
    if ((Object) GameAudio.mInst == (Object) null)
    {
      GameAudio.mInst = this;
      this.mTrans = this.transform;
      Object.DontDestroyOnLoad((Object) this.gameObject);
    }
    else
      Object.Destroy((Object) this.gameObject);
  }

  private void LateUpdate()
  {
    if ((Object) this.mTarget == (Object) null)
    {
      Camera main = Camera.main;
      if ((Object) main != (Object) null)
        this.mTarget = main.transform;
    }
    if (!((Object) this.mTarget != (Object) null) || !(this.mTrans.position != this.mTarget.position))
      return;
    this.mTrans.position = this.mTarget.position;
  }
}
