﻿// Decompiled with JetBrains decompiler
// Type: UnitCardCellInfoBase`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public abstract class UnitCardCellInfoBase<TData, TCell> : UnitCardCellInfoBase where TData : Entity where TCell : UnitCardCellBase, IScrollViewCell
{
  protected TData mCellData;
  protected TCell mCellInstance;
  private readonly TCell _cellPrefabComponent;

  protected UnitCardCellInfoBase(Transform prefab, TData data)
    : base(prefab)
  {
    this.mCellData = data;
    this._cellPrefabComponent = this.cellPrefab.GetComponent<TCell>();
  }

  public override Transform cellTransform
  {
    get
    {
      if ((UnityEngine.Object) this.mCellInstance != (UnityEngine.Object) null)
        return this.mCellInstance.transform;
      return (Transform) null;
    }
  }

  public int lastUpdateFrameId { get; private set; }

  public override void UpdateCellVisuals()
  {
    if (!this.isBound)
      return;
    this.OnUpdateCellVisuals();
  }

  public override int entityId
  {
    get
    {
      if ((object) this.mCellData == null)
        return 0;
      return this.mCellData.ID;
    }
  }

  public override object cellData
  {
    get
    {
      return (object) this.mCellData;
    }
  }

  public override System.Type cellType
  {
    get
    {
      return typeof (TCell);
    }
  }

  public override Vector3 extents
  {
    get
    {
      if (this.isBound)
        return this.mCellInstance.extents;
      return this._cellPrefabComponent.extents;
    }
  }

  protected override void OnBind(Transform cellTrans)
  {
    this.mCellInstance = cellTrans.GetComponent<TCell>();
    EventDelegate.Add(this.mCellInstance.onClick, new EventDelegate.Callback(this.Entity_OnClick));
    EventDelegate.Add(this.mCellInstance.onLongPress, new EventDelegate.Callback(this.Entity_OnLongPress));
    this.UpdateCellVisuals();
  }

  protected override void OnUnbind()
  {
    EventDelegate.Remove(this.mCellInstance.onClick, new EventDelegate.Callback(this.Entity_OnClick));
    EventDelegate.Remove(this.mCellInstance.onLongPress, new EventDelegate.Callback(this.Entity_OnLongPress));
    if ((UnityEngine.Object) this.mCellInstance == (UnityEngine.Object) null)
      return;
    this.mCellInstance.ResetVisuals();
    this.mCellInstance = (TCell) null;
  }

  protected virtual void OnUpdateCellVisuals()
  {
    this.mCellInstance.titleText = this.mCellData.Title;
    this.mCellInstance.subTitleText = this.mCellData.SubTitle;
  }

  private void Entity_OnClick()
  {
    this.mCellData.OnClick();
  }

  private void Entity_OnLongPress()
  {
    this.mCellData.OnLongPress();
  }
}
