﻿// Decompiled with JetBrains decompiler
// Type: ValueWrapper`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

public class ValueWrapper<T> : ValueWrapper where T : struct
{
  private T _t;

  public ValueWrapper()
  {
    this.HasValueChangedSinceLastToString = true;
  }

  public ValueWrapper(T t)
  {
    this.Value = t;
  }

  public T Value
  {
    get
    {
      return this._t;
    }
    set
    {
      this._t = value;
      this.HasValueChangedSinceLastToString = true;
    }
  }

  public override string ToString()
  {
    this.HasValueChangedSinceLastToString = false;
    return this._t.ToString();
  }

  public override string ToString(string format)
  {
    this.HasValueChangedSinceLastToString = false;
    return string.Format(format, (object) this._t);
  }

  public static implicit operator T(ValueWrapper<T> w)
  {
    return w.Value;
  }
}
