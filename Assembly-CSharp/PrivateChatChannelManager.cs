﻿// Decompiled with JetBrains decompiler
// Type: PrivateChatChannelManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Client.Settings;
using UnityEngine;

public class PrivateChatChannelManager : MonoBehaviour
{
  private readonly PrivateChatChannelCellInfoCollection data = new PrivateChatChannelCellInfoCollection();
  public ChatDialog chatDialog;
  public PrivateChatChannelScrollView view;
  public UIScrollView chatEntryView;
  public Transform prefParagraph;
  public Transform prefPrivateChannelTab;

  public void ProcessPrivateChatEvent(ChatEventArgs e)
  {
    if (e.State.MyPlayer == null)
      return;
    string channelKey = e.Sender.StartsWith("-") || e.Receiver.StartsWith("-") ? (!e.Sender.StartsWith("-") ? e.Receiver : e.Sender) : (!e.Receiver.Equals(e.State.MyPlayer.Name) ? e.Receiver : e.Sender);
    if (string.IsNullOrEmpty(channelKey))
      return;
    PrivateChatChannelCellInfo cellInfo;
    if (!this.data.TryGetChannel(channelKey, out cellInfo))
    {
      cellInfo = new PrivateChatChannelCellInfo(this.prefPrivateChannelTab, new ChatChannel(ChatType.PRIVATE, channelKey, 0, this.prefParagraph, (GameEventsFilter) null, this.chatEntryView))
      {
        onCloseClickedAction = new System.Action<PrivateChatChannelCellInfo>(this.OnPrivateChannelCloseClicked),
        onCellClickedAction = new System.Action<PrivateChatChannelCellInfo>(this.OnChannelCellClicked)
      };
      this.data.Add(cellInfo);
    }
    cellInfo.chatChannel.AddParagraph(e);
  }

  private void Awake()
  {
    this.view.data = (IPooledScrollViewCellInifoCollection<PrivateChatChannelCellInfo>) this.data;
    if ((bool) ((UnityEngine.Object) this.chatDialog))
      return;
    this.chatDialog = NGUITools.FindInParents<ChatDialog>(this.gameObject);
  }

  private void OnPrivateChannelCloseClicked(PrivateChatChannelCellInfo info)
  {
    this.data.Remove(info);
    info.onCellClickedAction = (System.Action<PrivateChatChannelCellInfo>) null;
    if (this.chatDialog.currentChannel != info.chatChannel)
      return;
    this.chatDialog.ShowUniverseChat();
  }

  private void OnChannelCellClicked(PrivateChatChannelCellInfo info)
  {
    if (this.chatDialog.currentChannel != info.chatChannel)
      this.chatDialog.currentChannel = info.chatChannel;
    else
      this.chatDialog.BeginMessageInput();
  }
}
