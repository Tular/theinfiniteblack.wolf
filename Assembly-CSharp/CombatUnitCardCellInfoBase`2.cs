﻿// Decompiled with JetBrains decompiler
// Type: CombatUnitCardCellInfoBase`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using UnityEngine;

public abstract class CombatUnitCardCellInfoBase<TData, TCell> : UnitCardCellInfoBase<TData, TCell> where TData : CombatEntity where TCell : CombatUnitCardCell
{
  protected CombatUnitCardCellInfoBase(Transform prefab, TData data)
    : base(prefab, data)
  {
  }

  protected override void OnUpdateCellVisuals()
  {
    base.OnUpdateCellVisuals();
    bool flag1 = TibProxy.gameState != null && TibProxy.gameState.MyAttackTarget != null && TibProxy.gameState.MyAttackTarget.ID == this.mCellData.ID;
    bool flag2 = TibProxy.gameState != null && TibProxy.gameState.MyFollowTarget != null && TibProxy.gameState.MyFollowTarget.ID == this.mCellData.ID;
    this.mCellInstance.isAttackTarget = flag1;
    this.mCellInstance.isFollowTarget = flag2;
    this.mCellInstance.isGrappled = this.mCellData.Grappled;
    this.mCellInstance.isStunned = this.mCellData.Stunned;
    this.mCellInstance.relationType = this.mCellData.Relation;
    this.mCellInstance.hull = (float) this.mCellData.Hull;
    this.mCellInstance.maxHull = (float) this.mCellData.MaxHull;
    if (!flag1)
      return;
    this.mCellInstance.cooldownRemaining = TibProxy.gameState == null ? 0.0f : TibProxy.gameState.AttackCooldown.RemainingSeconds;
  }
}
