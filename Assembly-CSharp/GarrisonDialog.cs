﻿// Decompiled with JetBrains decompiler
// Type: GarrisonDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;

public class GarrisonDialog : DialogBase
{
  public GarrisonTechView technologyView;
  public GarrisonBankView itemBankView;
  private GarrisonDialogContext _dialogContext;

  public void ShowTechnologyView()
  {
    NGUITools.SetActiveSelf(this.itemBankView.gameObject, false);
    NGUITools.SetActiveSelf(this.technologyView.gameObject, true);
  }

  public void ShowItemBankView()
  {
    NGUITools.SetActiveSelf(this.technologyView.gameObject, false);
    NGUITools.SetActiveSelf(this.itemBankView.gameObject, true);
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this._dialogContext = new GarrisonDialogContext();
    this.technologyView.dialogContext = this._dialogContext;
    this.itemBankView.dialogContext = this._dialogContext;
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
    TibProxy.Instance.onShowEntityDetailEvent += new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  private void TibProxy_onShowEntityDetailEvent(object sender, ShowEntityDetailEventArgs e)
  {
    if (e.Source.Type != EntityType.GARRISON)
      return;
    this.Show();
  }

  protected override void OnShow()
  {
    base.OnShow();
    this._dialogContext.garrison = TibProxy.gameState.Local.Garrison;
    this.ShowTechnologyView();
  }

  protected override void OnHide()
  {
    base.OnHide();
    this._dialogContext.garrison = (Garrison) null;
  }

  protected override void OnCleanup()
  {
    base.OnCleanup();
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  private void Update()
  {
    if (TibProxy.gameState != null && TibProxy.gameState.Local.Garrison != null && TibProxy.gameState.Local.Garrison == this._dialogContext.garrison)
      return;
    this.Hide();
  }
}
