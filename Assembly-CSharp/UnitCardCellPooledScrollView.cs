﻿// Decompiled with JetBrains decompiler
// Type: UnitCardCellPooledScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using PathologicalGames;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class UnitCardCellPooledScrollView : MonoBehaviour
{
  public UnitCardCellPooledScrollView.DragEffect dragEffect = UnitCardCellPooledScrollView.DragEffect.MomentumAndSpring;
  public bool restrictWithinPanel = true;
  public float scrollWheelFactor = 0.25f;
  public float dragSpeed = 10f;
  public float momentumAmount = 35f;
  public float springStrength = 13f;
  public float dampenStrength = 9f;
  public Vector2 customMovement = new Vector2(1f, 0.0f);
  protected Vector3 mMomentum = Vector3.zero;
  protected int mDragID = -10;
  protected Vector2 mDragStartOffset = Vector2.zero;
  private HashSet<UnitCardCellInfoBase> _prevData = new HashSet<UnitCardCellInfoBase>();
  private HashSet<UnitCardCellInfoBase> _currentData = new HashSet<UnitCardCellInfoBase>();
  public UnitCardCellPooledScrollView.Movement movement;
  public bool disableDragIfFits;
  public UIWidget.Pivot contentPivot;
  public UnitCardCellPooledScrollView.OnDragFinished onDragFinished;
  protected Transform mTrans;
  protected UIPanel mPanel;
  protected Plane mPlane;
  protected Vector3 mLastPos;
  protected bool mPressed;
  protected float mScroll;
  public Bounds mBounds;
  protected bool mCalculatedBounds;
  protected bool mShouldMove;
  protected bool mIgnoreCallbacks;
  protected bool mDragStarted;
  private bool _isPressed;
  private Vector3 _targetPos;
  private Vector3 offset;
  public SpawnPool cellPool;
  public Vector2 cellPadding;
  public int topBuffer;
  public int bottomBuffer;
  public int leftBuffer;
  public int rightBuffer;
  private int _lastMouseScrollFrame;
  public float momentumMag;
  private UnitCardCellInfoCollection _data;
  private bool _isInitialized;
  private bool _boundsDidFit;
  private int _nextRepositionFrame;

  public UIPanel panel
  {
    get
    {
      return this.mPanel;
    }
  }

  public bool isDragging
  {
    get
    {
      if (this.mPressed)
        return this.mDragStarted;
      return false;
    }
  }

  public bool canMoveHorizontally
  {
    get
    {
      if (this.movement == UnitCardCellPooledScrollView.Movement.Horizontal || this.movement == UnitCardCellPooledScrollView.Movement.Unrestricted)
        return true;
      if (this.movement == UnitCardCellPooledScrollView.Movement.Custom)
        return (double) this.customMovement.x != 0.0;
      return false;
    }
  }

  public bool canMoveVertically
  {
    get
    {
      if (this.movement == UnitCardCellPooledScrollView.Movement.Vertical || this.movement == UnitCardCellPooledScrollView.Movement.Unrestricted)
        return true;
      if (this.movement == UnitCardCellPooledScrollView.Movement.Custom)
        return (double) this.customMovement.y != 0.0;
      return false;
    }
  }

  public virtual bool shouldMoveHorizontally
  {
    get
    {
      float x = this.bounds.size.x;
      if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
        x += this.mPanel.clipSoftness.x * 2f;
      return Mathf.RoundToInt(x - this.mPanel.width) > 0;
    }
  }

  public virtual bool shouldMoveVertically
  {
    get
    {
      float y = this.bounds.size.y;
      if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
        y += this.mPanel.clipSoftness.y * 2f;
      return Mathf.RoundToInt(y - this.mPanel.height) > 0;
    }
  }

  protected virtual bool shouldMove
  {
    get
    {
      if (!this.disableDragIfFits)
        return true;
      if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
        this.mPanel = this.GetComponent<UIPanel>();
      Vector4 finalClipRegion = this.mPanel.finalClipRegion;
      Bounds bounds = this.bounds;
      float num1 = (double) finalClipRegion.z != 0.0 ? finalClipRegion.z * 0.5f : (float) Screen.width;
      float num2 = (double) finalClipRegion.w != 0.0 ? finalClipRegion.w * 0.5f : (float) Screen.height;
      return this.canMoveHorizontally && ((double) bounds.min.x < (double) finalClipRegion.x - (double) num1 || (double) bounds.max.x > (double) finalClipRegion.x + (double) num1) || this.canMoveVertically && ((double) bounds.min.y < (double) finalClipRegion.y - (double) num2 || (double) bounds.max.y > (double) finalClipRegion.y + (double) num2);
    }
  }

  public Vector3 currentMomentum
  {
    get
    {
      return this.mMomentum;
    }
    set
    {
      this.mMomentum = value;
      this.mShouldMove = true;
    }
  }

  private void Awake()
  {
    this.mTrans = this.transform;
    this.mPanel = this.GetComponent<UIPanel>();
    if (this.mPanel.clipping != UIDrawCall.Clipping.None)
      return;
    this.mPanel.clipping = UIDrawCall.Clipping.ConstrainButDontClip;
  }

  [ContextMenu("RestrictWithinBounds")]
  public void RestrictWithinBoundsTest()
  {
    this.RestrictWithinBounds(true);
  }

  public bool RestrictWithinBounds(bool instant)
  {
    return this.RestrictWithinBounds(instant, true, true);
  }

  public bool RestrictWithinBounds(bool instant, bool horizontal, bool vertical)
  {
    Bounds bounds = this.bounds;
    Vector2 vector2_1 = new Vector2(this.bounds.min.x + this.panel.clipSoftness.x, this.bounds.min.y + this.panel.clipSoftness.y);
    Vector2 vector2_2 = new Vector2(this.bounds.max.x - this.panel.clipSoftness.x, this.bounds.max.y - this.panel.clipSoftness.y);
    Vector3 constrainOffset = this.mPanel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!horizontal)
      constrainOffset.x = 0.0f;
    if (!vertical)
      constrainOffset.y = 0.0f;
    if ((double) constrainOffset.sqrMagnitude <= 0.100000001490116)
      return false;
    if (!instant && this.dragEffect == UnitCardCellPooledScrollView.DragEffect.MomentumAndSpring)
    {
      Vector3 pos = this.mTrans.localPosition + constrainOffset;
      pos.x = Mathf.Round(pos.x);
      pos.y = Mathf.Round(pos.y);
      SpringPanel.Begin(this.mPanel.gameObject, pos, this.springStrength);
    }
    else
    {
      this.MoveRelative(constrainOffset);
      this.mMomentum = Vector3.zero;
      this.mScroll = 0.0f;
    }
    return true;
  }

  public void DisableSpring()
  {
    SpringPanel component = this.GetComponent<SpringPanel>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.enabled = false;
  }

  public virtual void SetDragAmount(float x, float y, bool updateScrollbars)
  {
    if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
      this.mPanel = this.GetComponent<UIPanel>();
    this.DisableSpring();
    Bounds bounds = this.bounds;
    if ((double) bounds.min.x == (double) bounds.max.x || (double) bounds.min.y == (double) bounds.max.y)
      return;
    Vector4 finalClipRegion = this.mPanel.finalClipRegion;
    float num1 = finalClipRegion.z * 0.5f;
    float num2 = finalClipRegion.w * 0.5f;
    float a1 = bounds.min.x + num1;
    float b1 = bounds.max.x - num1;
    float b2 = bounds.min.y + num2;
    float a2 = bounds.max.y - num2;
    if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
    {
      a1 -= this.mPanel.clipSoftness.x;
      b1 += this.mPanel.clipSoftness.x;
      b2 -= this.mPanel.clipSoftness.y;
      a2 += this.mPanel.clipSoftness.y;
    }
    float num3 = Mathf.Lerp(a1, b1, x);
    float num4 = Mathf.Lerp(a2, b2, y);
    if (!updateScrollbars)
    {
      Vector3 localPosition = this.mTrans.localPosition;
      if (this.canMoveHorizontally)
        localPosition.x += finalClipRegion.x - num3;
      if (this.canMoveVertically)
        localPosition.y += finalClipRegion.y - num4;
      this.mTrans.localPosition = localPosition;
    }
    if (this.canMoveHorizontally)
      finalClipRegion.x = num3;
    if (this.canMoveVertically)
      finalClipRegion.y = num4;
    Vector4 baseClipRegion = this.mPanel.baseClipRegion;
    this.mPanel.clipOffset = new Vector2(finalClipRegion.x - baseClipRegion.x, finalClipRegion.y - baseClipRegion.y);
  }

  [ContextMenu("Reset Clipping Position")]
  public void ResetPosition()
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    this.mCalculatedBounds = false;
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.contentPivot);
    this.SetDragAmount(pivotOffset.x, 1f - pivotOffset.y, false);
    this.SetDragAmount(pivotOffset.x, 1f - pivotOffset.y, false);
  }

  public void InvalidateBounds()
  {
    this.mCalculatedBounds = false;
  }

  [ContextMenu("Update Position")]
  public void UpdatePosition()
  {
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.contentPivot);
    this.SetDragAmount(pivotOffset.x, 1f - pivotOffset.y, false);
  }

  public virtual void MoveRelative(Vector3 relative)
  {
    this.mTrans.localPosition += relative;
    Vector2 clipOffset = this.mPanel.clipOffset;
    clipOffset.x -= relative.x;
    clipOffset.y -= relative.y;
    this.mPanel.clipOffset = clipOffset;
  }

  public void MoveAbsolute(Vector3 absolute)
  {
    this.MoveRelative(this.mTrans.InverseTransformPoint(absolute) - this.mTrans.InverseTransformPoint(Vector3.zero));
  }

  public void Press(bool pressed)
  {
    this._isPressed = pressed;
    if (pressed)
    {
      this.mDragStarted = false;
      this.mDragStartOffset = Vector2.zero;
    }
    if (!this.enabled || !NGUITools.GetActive(this.gameObject))
      return;
    if (!pressed && this.mDragID == UICamera.currentTouchID)
      this.mDragID = -10;
    this.mCalculatedBounds = false;
    this.mShouldMove = this.shouldMove;
    if (!this.mShouldMove)
      return;
    if (pressed)
    {
      this.mMomentum = Vector3.zero;
      this.mScroll = 0.0f;
      this.DisableSpring();
      this.mLastPos = UICamera.lastHit.point;
      this.mPlane = new Plane(this.mTrans.rotation * Vector3.back, this.mLastPos);
      Vector2 clipOffset = this.mPanel.clipOffset;
      clipOffset.x = Mathf.Round(clipOffset.x);
      clipOffset.y = Mathf.Round(clipOffset.y);
      this.mPanel.clipOffset = clipOffset;
      Vector3 localPosition = this.mTrans.localPosition;
      localPosition.x = Mathf.Round(localPosition.x);
      localPosition.y = Mathf.Round(localPosition.y);
      this.mTrans.localPosition = localPosition;
      this._targetPos = this.mTrans.position;
    }
    else
    {
      if (this.mPressed && this.restrictWithinPanel && (this.mPanel.clipping != UIDrawCall.Clipping.None && this.dragEffect == UnitCardCellPooledScrollView.DragEffect.MomentumAndSpring))
        this.RestrictWithinBounds(false, this.canMoveHorizontally, this.canMoveVertically);
      if (this.onDragFinished != null)
        this.onDragFinished();
    }
    this.mPressed = pressed;
  }

  public void Drag()
  {
    if (!this.enabled || !NGUITools.GetActive(this.gameObject) || !this.mShouldMove)
      return;
    if (this.mDragID == -10)
      this.mDragID = UICamera.currentTouchID;
    UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;
    if (!this.mDragStarted)
    {
      this.mDragStarted = true;
      this.mDragStartOffset = UICamera.currentTouch.totalDelta;
    }
    Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos);
    float enter = 0.0f;
    if (!this.mPlane.Raycast(ray, out enter))
      return;
    Vector3 point = ray.GetPoint(enter);
    this.offset = point - this.mLastPos;
    this.mLastPos = point;
    if ((double) this.offset.x != 0.0 || (double) this.offset.y != 0.0 || (double) this.offset.z != 0.0)
    {
      this.offset = this.mTrans.InverseTransformDirection(this.offset);
      if (this.movement == UnitCardCellPooledScrollView.Movement.Horizontal)
      {
        this.offset.y = 0.0f;
        this.offset.z = 0.0f;
      }
      else if (this.movement == UnitCardCellPooledScrollView.Movement.Vertical)
      {
        this.offset.x = 0.0f;
        this.offset.z = 0.0f;
      }
      else if (this.movement == UnitCardCellPooledScrollView.Movement.Unrestricted)
        this.offset.z = 0.0f;
      else
        this.offset.Scale((Vector3) this.customMovement);
      this.offset = this.mTrans.TransformDirection(this.offset);
    }
    this._targetPos = this._targetPos + this.offset;
    this.mMomentum = Vector3.Lerp(this.mMomentum, this.mMomentum + this.offset * (0.01f * this.momentumAmount), 0.67f);
  }

  public void Scroll(float delta)
  {
    if (!this.enabled || !NGUITools.GetActive(this.gameObject) || (double) this.scrollWheelFactor == 0.0)
      return;
    this._lastMouseScrollFrame = Time.frameCount;
    if (this._isPressed)
      this.Press(false);
    this.DisableSpring();
    this.mShouldMove = this.shouldMove;
    if ((double) Mathf.Sign(this.mScroll) != (double) Mathf.Sign(delta))
      this.mScroll = 0.0f;
    this.mScroll += delta * this.scrollWheelFactor;
  }

  public void DoLateUpdate()
  {
    if (!Application.isPlaying)
      return;
    if (this.mMomentum == Vector3.zero && (double) Math.Abs(this.mScroll) <= 0.0)
    {
      this.isMoving = false;
    }
    else
    {
      float deltaTime = RealTime.deltaTime;
      if (this.mPressed)
        this.MoveRelative(NGUIMath.SpringLerp(this.mTrans.InverseTransformPoint(this.transform.position), this.mTrans.InverseTransformPoint(this._targetPos), this.dragSpeed, RealTime.deltaTime));
      else if (this.mShouldMove && !this.mPressed)
      {
        if (this.movement == UnitCardCellPooledScrollView.Movement.Horizontal)
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3(this.mScroll * 0.05f, 0.0f, 0.0f));
        else if (this.movement == UnitCardCellPooledScrollView.Movement.Vertical)
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3(0.0f, this.mScroll * 0.05f, 0.0f));
        else if (this.movement == UnitCardCellPooledScrollView.Movement.Unrestricted)
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3(this.mScroll * 0.05f, this.mScroll * 0.05f, 0.0f));
        else
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3((float) ((double) this.mScroll * (double) this.customMovement.x * 0.0500000007450581), (float) ((double) this.mScroll * (double) this.customMovement.y * 0.0500000007450581), 0.0f));
        if ((double) this.mMomentum.magnitude > 9.99999974737875E-05)
        {
          this.isMoving = true;
          this.mScroll = NGUIMath.SpringLerp(this.mScroll, 0.0f, 20f, deltaTime);
          this.MoveAbsolute(NGUIMath.SpringDampen(ref this.mMomentum, this.dampenStrength, deltaTime));
          if (this.restrictWithinPanel && this.mPanel.clipping != UIDrawCall.Clipping.None)
            this.RestrictWithinBounds(false, this.canMoveHorizontally, this.canMoveVertically);
          if ((double) this.mMomentum.magnitude >= 9.99999974737875E-05 || this.onDragFinished == null)
            return;
          this.onDragFinished();
          return;
        }
        this.mScroll = 0.0f;
        this.mMomentum = Vector3.zero;
      }
      else
      {
        this.mScroll = 0.0f;
        this.isMoving = false;
      }
      NGUIMath.SpringDampen(ref this.mMomentum, this.dampenStrength, deltaTime);
    }
  }

  public bool isMoving { get; private set; }

  protected bool mBackingDataChanged { get; private set; }

  public UnitCardCellInfoCollection data
  {
    get
    {
      return this._data;
    }
    set
    {
      if (value == this._data)
        return;
      this._data = value;
      this.mBackingDataChanged = true;
    }
  }

  public Bounds bounds
  {
    get
    {
      if (!this.mCalculatedBounds)
      {
        this.mCalculatedBounds = true;
        this.mTrans = this.transform;
        this.mBounds = this.CalculateBounds();
      }
      return this.mBounds;
    }
  }

  public Bounds bufferBounds
  {
    get
    {
      Vector2 viewSize = this.panel.GetViewSize();
      bool flag = this.panel.clipping != UIDrawCall.Clipping.None;
      Vector4 finalClipRegion = this.panel.finalClipRegion;
      Vector3 center = new Vector3(finalClipRegion.x, finalClipRegion.y);
      if (!flag)
        return new Bounds(center, new Vector3(viewSize.x, viewSize.y));
      float num1 = finalClipRegion.w / 2f;
      float num2 = finalClipRegion.z / 2f;
      Vector3 max = new Vector3(finalClipRegion.x + num2 + (float) this.rightBuffer, finalClipRegion.y + num1 + (float) this.topBuffer);
      Vector3 min = new Vector3(finalClipRegion.x - num2 - (float) this.leftBuffer, finalClipRegion.y - num1 - (float) this.bottomBuffer);
      Bounds bounds = new Bounds();
      bounds.SetMinMax(min, max);
      return bounds;
    }
  }

  protected virtual void UpdateCellPositions()
  {
    if (this.data == null)
      return;
    Vector3 zero = Vector3.zero;
    foreach (UnitCardCellInfoBase cardCellInfoBase in this.data.values)
    {
      switch (this.contentPivot)
      {
        case UIWidget.Pivot.Top:
          cardCellInfoBase.localPosition = zero - new Vector3(-this.cellPadding.x, cardCellInfoBase.extents.y);
          zero -= new Vector3(0.0f, 2f * cardCellInfoBase.extents.y + this.cellPadding.y);
          continue;
        case UIWidget.Pivot.Bottom:
          cardCellInfoBase.localPosition = zero + new Vector3(this.cellPadding.x, cardCellInfoBase.extents.y);
          zero += new Vector3(0.0f, 2f * cardCellInfoBase.extents.y + this.cellPadding.y);
          continue;
        default:
          continue;
      }
    }
  }

  private void Init()
  {
    if (this._isInitialized)
      return;
    this.OnInit();
    this._isInitialized = true;
  }

  protected virtual void OnInit()
  {
  }

  protected void UpdateVisibleCellData()
  {
    if (!Application.isPlaying || this.data == null)
      return;
    this._currentData.Clear();
    this._currentData.UnionWith(this.data.values);
    if (this._prevData.Count != this._currentData.Count)
      this.InvalidateBounds();
    this._prevData.ExceptWith((IEnumerable<UnitCardCellInfoBase>) this._currentData);
    using (HashSet<UnitCardCellInfoBase>.Enumerator enumerator = this._prevData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnitCardCellInfoBase current = enumerator.Current;
        if (current.isBound)
        {
          Transform cellTransform = current.cellTransform;
          current.Unbind();
          this.cellPool.Despawn(cellTransform);
        }
      }
    }
    Bounds bufferBounds = this.bufferBounds;
    float a1 = this.bounds.max.y;
    float a2 = this.bounds.min.y;
    int num = 0;
    using (HashSet<UnitCardCellInfoBase>.Enumerator enumerator = this._currentData.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UnitCardCellInfoBase current = enumerator.Current;
        bool flag = current.IsContainedIn(bufferBounds);
        if (!flag && current.isBound)
        {
          Transform cellTransform = current.cellTransform;
          current.Unbind();
          this.cellPool.Despawn(cellTransform);
        }
        else if (flag && !current.isBound)
        {
          Transform cellTrans = this.cellPool.Spawn(current.cellPrefab, this.transform);
          current.BindCell(cellTrans);
        }
        else if (flag && current.isBound)
          current.UpdateCellVisuals();
        if (flag)
        {
          ++num;
          a1 = Mathf.Min(a1, current.localPosition.y - current.extents.y);
          a2 = Mathf.Max(a2, current.localPosition.y + current.extents.y);
        }
      }
    }
    if (num == 0)
      this.RestrictWithinBounds(true);
    else if (!this.isMoving && !this.isDragging)
    {
      Vector4 finalClipRegion = this.panel.finalClipRegion;
      float f1 = finalClipRegion.y - finalClipRegion.w * 0.5f;
      float f2 = finalClipRegion.y + finalClipRegion.w * 0.5f;
      if ((double) a2 < (double) Mathf.Floor(f2))
        this.SetDragAmount(0.5f, 0.0f, false);
      if ((double) a1 > (double) Mathf.Ceil(f2) || (double) a1 > (double) Mathf.Ceil(f1) && (double) a2 > (double) Mathf.Ceil(f2))
        this.SetDragAmount(0.5f, 1f, false);
    }
    HashSet<UnitCardCellInfoBase> prevData = this._prevData;
    this._prevData = this._currentData;
    this._currentData = prevData;
    this._currentData.Clear();
  }

  private Bounds CalculateBounds()
  {
    Vector4 finalClipRegion = this.panel.finalClipRegion;
    if (!Application.isPlaying && (this.data == null || this.data.Count < 1))
      return new Bounds(Vector3.zero, new Vector3(finalClipRegion.z, 20f));
    if (this.data == null || this.data.Count < 1)
      return new Bounds(new Vector3(finalClipRegion.x, finalClipRegion.y), Vector3.zero);
    UnitCardCellInfoBase first = this.data.first;
    UnitCardCellInfoBase last = this.data.last;
    Bounds bounds = new Bounds();
    switch (this.contentPivot)
    {
      case UIWidget.Pivot.TopLeft:
      case UIWidget.Pivot.Top:
      case UIWidget.Pivot.TopRight:
        Vector3 max1 = first.localPosition + first.extents + new Vector3(this.panel.clipSoftness.x, this.panel.clipSoftness.y);
        Vector3 min1 = last.localPosition - last.extents - new Vector3(this.panel.clipSoftness.x, this.panel.clipSoftness.y);
        bounds.SetMinMax(min1, max1);
        break;
      case UIWidget.Pivot.BottomLeft:
      case UIWidget.Pivot.Bottom:
      case UIWidget.Pivot.BottomRight:
        Vector3 max2 = last.localPosition + last.extents;
        Vector3 min2 = first.localPosition - first.extents;
        bounds.SetMinMax(min2, max2);
        break;
    }
    return bounds;
  }

  private void OnDisable()
  {
    if (this.data != null)
      this.data.Clear();
    this.cellPool.DespawnAll();
    this._currentData.Clear();
    this._prevData.Clear();
  }

  private void Start()
  {
    this.Init();
    this.ResetPosition();
  }

  private void OnEnable()
  {
    if (!Application.isPlaying)
      return;
    this.Init();
    this.StartCoroutine(this.RepositionCoroutine());
    this.resetPosition = true;
  }

  [DebuggerHidden]
  private IEnumerator RepositionCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UnitCardCellPooledScrollView.\u003CRepositionCoroutine\u003Ec__Iterator35()
    {
      \u003C\u003Ef__this = this
    };
  }

  [ContextMenu("Dump Bounds Stuff")]
  public void DumpBoundsStuff()
  {
    UnityEngine.Debug.Log((object) string.Format("bounds.size: {0}", (object) this.bounds.size.y));
    UnityEngine.Debug.Log((object) string.Format("panel.finalClipRegion.w: {0}", (object) this.panel.finalClipRegion.w));
  }

  public bool resetPosition
  {
    get
    {
      if (this._nextRepositionFrame > Time.frameCount)
        return false;
      this._nextRepositionFrame = int.MaxValue;
      return true;
    }
    set
    {
      if (!value)
        return;
      this._nextRepositionFrame = Time.frameCount + 2;
    }
  }

  public bool isScrolling
  {
    get
    {
      if (this._lastMouseScrollFrame != Time.frameCount)
        return this.isDragging;
      return true;
    }
  }

  private void Update()
  {
    if (this.data == null)
      return;
    this.momentumMag = this.currentMomentum.magnitude;
    if (!this.mBackingDataChanged && !this.resetPosition)
      return;
    this.ResetPosition();
    this.mBackingDataChanged = false;
  }

  private void LateUpdate()
  {
    this.DoLateUpdate();
  }

  private void OnDestroy()
  {
    if (this.data != null)
      this.data.Clear();
    this.cellPool.DespawnAll();
  }

  public enum Movement
  {
    Horizontal,
    Vertical,
    Unrestricted,
    Custom,
  }

  public enum DragEffect
  {
    None,
    Momentum,
    MomentumAndSpring,
  }

  public delegate void OnDragFinished();
}
