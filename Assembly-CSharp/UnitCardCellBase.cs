﻿// Decompiled with JetBrains decompiler
// Type: UnitCardCellBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[RequireComponent(typeof (UIWidget))]
[RequireComponent(typeof (UISpriteColorGroupController))]
[RequireComponent(typeof (UIWidgetDepthGroupController))]
public class UnitCardCellBase : MonoBehaviour, IScrollViewCell
{
  public float scrollStopThreshold = 0.01f;
  private Color _borderColor = Color.black;
  private Color _backgroundColor = Color.black;
  private const float _longPressDelay = 0.5f;
  public UIWidget widget;
  public UILabel titleLabel;
  public UILabel subTitleLabel;
  public int borderColorGroup;
  public int backgroundColorGroup;
  public List<EventDelegate> onClick;
  public List<EventDelegate> onLongPress;
  protected int mLastChangedFrameId;
  private bool _cancelPress;
  protected UISpriteColorGroupController mSpriteColorGroupController;
  protected UIWidgetDepthGroupController mDepthGroupController;
  private bool _isPressed;
  private float _pressStart;
  private float _pressDelta;
  private Vector2 _dragDistance;
  private int _spawnFrame;
  private UnitCardCellPooledScrollView _scrollView;

  public string titleText
  {
    get
    {
      if ((Object) this.titleLabel != (Object) null)
        return this.titleLabel.text;
      return string.Empty;
    }
    set
    {
      if (!(bool) ((Object) this.titleLabel))
        return;
      this.titleLabel.text = value;
    }
  }

  public string subTitleText
  {
    get
    {
      if ((Object) this.subTitleLabel != (Object) null)
        return this.subTitleLabel.text;
      return string.Empty;
    }
    set
    {
      if (!(bool) ((Object) this.subTitleLabel))
        return;
      this.subTitleLabel.text = value;
    }
  }

  public Vector3 extents
  {
    get
    {
      return new Vector3((float) this.widget.width / 2f, (float) this.widget.height / 2f, 0.0f);
    }
  }

  public Color borderColor
  {
    set
    {
      if (this._borderColor == value)
        return;
      this._borderColor = value;
      this.mSpriteColorGroupController.SetColor(this.borderColorGroup, this._borderColor);
    }
  }

  public Color backgroundColor
  {
    set
    {
      if (this._backgroundColor == value)
        return;
      this._backgroundColor = value;
      this.mSpriteColorGroupController.SetColor(this.backgroundColorGroup, this._backgroundColor);
    }
  }

  public int orderId { get; set; }

  public int width
  {
    get
    {
      return this.widget.width;
    }
    set
    {
      this.widget.width = value;
    }
  }

  public int height
  {
    get
    {
      return this.widget.height;
    }
    set
    {
      this.widget.height = value;
    }
  }

  public virtual void ResetVisuals()
  {
    this.titleLabel.text = string.Empty;
    this.subTitleLabel.text = string.Empty;
  }

  public virtual void UpdateVisuals()
  {
  }

  protected virtual void Awake()
  {
    this.mSpriteColorGroupController = this.GetComponent<UISpriteColorGroupController>();
    this.mDepthGroupController = this.GetComponent<UIWidgetDepthGroupController>();
  }

  private void Update()
  {
    if (Time.frameCount <= this._spawnFrame + 1)
      this.widget.alpha = 1f;
    this.UpdateVisuals();
    if (!this._isPressed)
      return;
    this._pressDelta = RealTime.time - this._pressStart;
    if ((double) this._pressDelta <= 0.5)
      return;
    this.OnPress(false);
  }

  private void OnPress(bool isDown)
  {
    if (UICamera.currentTouch != null && (Object) UICamera.currentTouch.pressed != (Object) null)
      this.LogD("Current Touch Pressed: " + UICamera.currentTouch.pressed.name);
    this.LogD("UnitCardCellBase.OnPress - " + this.gameObject.name + " isDown: " + (object) isDown);
    if (isDown && !this._cancelPress && (!this._scrollView.isDragging && !this._scrollView.isScrolling) && (double) this._scrollView.currentMomentum.magnitude >= (double) this.scrollStopThreshold)
    {
      this._scrollView.currentMomentum = Vector3.zero;
      this._cancelPress = true;
    }
    if (this._cancelPress)
    {
      this._isPressed = false;
      this._cancelPress = false;
      this._pressStart = float.MinValue;
      UICamera.selectedObject = (GameObject) null;
    }
    else if (isDown)
    {
      this._isPressed = true;
      this._pressStart = RealTime.time;
    }
    else
    {
      if (!this._isPressed)
        return;
      if ((double) this._pressDelta < 0.5)
      {
        if (EasyTouch.GetTouchCount() <= 1)
          EventDelegate.Execute(this.onClick);
        UICamera.selectedObject = (GameObject) null;
      }
      else
      {
        if (EasyTouch.GetTouchCount() <= 1)
          EventDelegate.Execute(this.onLongPress);
        UICamera.selectedObject = (GameObject) null;
      }
      this._isPressed = false;
      this._pressStart = 0.0f;
    }
  }

  protected virtual void OnDragStart()
  {
    this._cancelPress = true;
  }

  public virtual void OnSpawned()
  {
    this._scrollView = NGUITools.FindInParents<UnitCardCellPooledScrollView>(this.transform);
    if (!(bool) ((Object) this._scrollView))
      this._scrollView = NGUITools.FindInParents<UnitCardCellPooledScrollView>(this.transform);
    this._spawnFrame = Time.frameCount;
    this.widget.alpha = 0.0f;
    this.ResetVisuals();
  }

  public virtual void OnDespawned()
  {
    this.transform.localPosition = Vector3.zero;
    this.widget.alpha = 0.0f;
    this.onClick.Clear();
    this.onLongPress.Clear();
    this._pressStart = float.MinValue;
    this._pressDelta = float.MinValue;
    this._isPressed = false;
    this._cancelPress = false;
  }
}
