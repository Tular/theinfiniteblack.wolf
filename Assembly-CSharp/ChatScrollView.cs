﻿// Decompiled with JetBrains decompiler
// Type: ChatScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using UnityEngine;

public class ChatScrollView : PooledScrollViewBase<ChatParagraphCellInfo>
{
  public UIToggle scrollLockToggle;
  private int _paragraphFontSize;
  private ChatParagraphCell _prefParagraph;

  public int paragraphFontSize
  {
    get
    {
      return this._paragraphFontSize;
    }
    set
    {
      if (value == this._paragraphFontSize)
        return;
      this._paragraphFontSize = value;
      this.UpdateFontSize();
    }
  }

  [ContextMenu("UpdateFontSize")]
  public void UpdateFontSize()
  {
    this._prefParagraph.fontSize = this._paragraphFontSize;
    foreach (ChatParagraphCell componentsInChild in this.GetComponentsInChildren<ChatParagraphCell>(true))
      componentsInChild.fontSize = this._paragraphFontSize;
    if (this.data == null)
      return;
    foreach (ChatParagraphCellInfo paragraphCellInfo in this.data.values)
      paragraphCellInfo.UpdateDimensions();
    this.OnResetCellPositions();
  }

  protected override void OnUpdateCellPositions()
  {
    if (this.isMoving || this.mPressed)
    {
      base.OnUpdateCellPositions();
      this.UpdatePosition();
    }
    else
    {
      this.ResetPosition();
      this.UpdateCellPositions();
      this.InvalidateBounds();
      this.UpdateVisibleCellData();
      this.ResetPosition();
    }
  }

  protected override void UpdateCellPositions()
  {
    Vector3 zero = Vector3.zero;
    Vector3[] localCorners = this.panel.localCorners;
    foreach (ChatParagraphCellInfo paragraphCellInfo in this.data.values)
    {
      paragraphCellInfo.UpdateAvaliableWidth(Mathf.RoundToInt(this.panel.width));
      switch (this.contentPivot)
      {
        case UIWidget.Pivot.BottomLeft:
          zero.x = localCorners[0].x + paragraphCellInfo.extents.x;
          paragraphCellInfo.localPosition = zero + new Vector3(0.0f, paragraphCellInfo.extents.y);
          zero += new Vector3(0.0f, 2f * paragraphCellInfo.extents.y + this.cellPadding.y);
          continue;
        case UIWidget.Pivot.Bottom:
          paragraphCellInfo.localPosition = zero + new Vector3(0.0f, paragraphCellInfo.extents.y);
          zero += new Vector3(0.0f, 2f * paragraphCellInfo.extents.y + this.cellPadding.y);
          continue;
        case UIWidget.Pivot.Top:
          paragraphCellInfo.localPosition = zero - new Vector3(0.0f, paragraphCellInfo.extents.y);
          zero -= new Vector3(0.0f, 2f * paragraphCellInfo.extents.y + this.cellPadding.y);
          continue;
        default:
          continue;
      }
    }
  }

  protected override void OnInit()
  {
    this._prefParagraph = NGUITools.FindInParents<ChatDialog>(this.gameObject).prefParagraph.GetComponent<ChatParagraphCell>();
  }

  protected override void Start()
  {
    base.Start();
    if (TibProxy.mySettings == null)
      return;
    this.paragraphFontSize = TibProxy.mySettings.Social.chatFontSize;
    this.UpdateFontSize();
  }
}
