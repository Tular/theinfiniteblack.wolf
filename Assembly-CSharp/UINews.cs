﻿// Decompiled with JetBrains decompiler
// Type: UINews
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UILabel))]
public class UINews : MonoBehaviour
{
  public string url = "http://misc.tasharen.com/news.txt";
  private UILabel mLabel;
  private string mData;

  private void Awake()
  {
    this.mLabel = this.GetComponent<UILabel>();
    this.mLabel.text = Localization.Get("Loading News");
  }

  private void OnEnable()
  {
    if (!string.IsNullOrEmpty(this.mData))
      return;
    if (PlayerProfile.allowedToAccessInternet)
      GameWebRequest.Create(this.url, new GameWebRequest.OnFinished(this.OnFinished));
    else
      this.mLabel.text = Localization.Get("Wifi Required");
  }

  private void OnFinished(bool success, object obj, string text)
  {
    if (success)
    {
      this.mData = text;
      this.mLabel.text = text;
    }
    else
      this.mLabel.text = Localization.Get("News Failed");
    Object.Destroy((Object) this);
  }
}
