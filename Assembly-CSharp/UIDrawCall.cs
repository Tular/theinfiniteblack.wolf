﻿// Decompiled with JetBrains decompiler
// Type: UIDrawCall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/Internal/Draw Call")]
public class UIDrawCall : MonoBehaviour
{
  private static BetterList<UIDrawCall> mActiveList = new BetterList<UIDrawCall>();
  private static BetterList<UIDrawCall> mInactiveList = new BetterList<UIDrawCall>();
  private static ColorSpace mColorSpace = ColorSpace.Uninitialized;
  private static List<int[]> mCache = new List<int[]>(10);
  private static int[] ClipRange = (int[]) null;
  private static int[] ClipArgs = (int[]) null;
  private static int dx9BugWorkaround = -1;
  [HideInInspector]
  [NonSerialized]
  public int depthStart = int.MaxValue;
  [HideInInspector]
  [NonSerialized]
  public int depthEnd = int.MinValue;
  [HideInInspector]
  [NonSerialized]
  public BetterList<Vector3> verts = new BetterList<Vector3>();
  [HideInInspector]
  [NonSerialized]
  public BetterList<Vector3> norms = new BetterList<Vector3>();
  [HideInInspector]
  [NonSerialized]
  public BetterList<Vector4> tans = new BetterList<Vector4>();
  [HideInInspector]
  [NonSerialized]
  public BetterList<Vector2> uvs = new BetterList<Vector2>();
  [HideInInspector]
  [NonSerialized]
  public BetterList<Color> cols = new BetterList<Color>();
  private bool mRebuildMat = true;
  private int mRenderQueue = 3000;
  private const int maxIndexBufferCache = 10;
  [HideInInspector]
  [NonSerialized]
  public int widgetCount;
  [HideInInspector]
  [NonSerialized]
  public UIPanel manager;
  [HideInInspector]
  [NonSerialized]
  public UIPanel panel;
  [HideInInspector]
  [NonSerialized]
  public Texture2D clipTexture;
  [HideInInspector]
  [NonSerialized]
  public bool alwaysOnScreen;
  private Material mMaterial;
  private Texture mTexture;
  private Shader mShader;
  private int mClipCount;
  private Transform mTrans;
  private Mesh mMesh;
  private MeshFilter mFilter;
  private MeshRenderer mRenderer;
  private Material mDynamicMat;
  private int[] mIndices;
  private bool mLegacyShader;
  private int mTriangles;
  [NonSerialized]
  public bool isDirty;
  [NonSerialized]
  private bool mTextureClip;
  public UIDrawCall.OnRenderCallback onRender;
  [NonSerialized]
  private string mSortingLayerName;

  [Obsolete("Use UIDrawCall.activeList")]
  public static BetterList<UIDrawCall> list
  {
    get
    {
      return UIDrawCall.mActiveList;
    }
  }

  public static BetterList<UIDrawCall> activeList
  {
    get
    {
      return UIDrawCall.mActiveList;
    }
  }

  public static BetterList<UIDrawCall> inactiveList
  {
    get
    {
      return UIDrawCall.mInactiveList;
    }
  }

  public int renderQueue
  {
    get
    {
      return this.mRenderQueue;
    }
    set
    {
      if (this.mRenderQueue == value)
        return;
      this.mRenderQueue = value;
      if (!((UnityEngine.Object) this.mDynamicMat != (UnityEngine.Object) null))
        return;
      this.mDynamicMat.renderQueue = value;
    }
  }

  public int sortingOrder
  {
    get
    {
      if ((UnityEngine.Object) this.mRenderer != (UnityEngine.Object) null)
        return this.mRenderer.sortingOrder;
      return 0;
    }
    set
    {
      if (!((UnityEngine.Object) this.mRenderer != (UnityEngine.Object) null) || this.mRenderer.sortingOrder == value)
        return;
      this.mRenderer.sortingOrder = value;
    }
  }

  public string sortingLayerName
  {
    get
    {
      if (!string.IsNullOrEmpty(this.mSortingLayerName))
        return this.mSortingLayerName;
      if ((UnityEngine.Object) this.mRenderer == (UnityEngine.Object) null)
        return (string) null;
      this.mSortingLayerName = this.mRenderer.sortingLayerName;
      return this.mSortingLayerName;
    }
    set
    {
      if (!((UnityEngine.Object) this.mRenderer != (UnityEngine.Object) null) || !(this.mSortingLayerName != value))
        return;
      this.mSortingLayerName = value;
      this.mRenderer.sortingLayerName = value;
    }
  }

  public int finalRenderQueue
  {
    get
    {
      if ((UnityEngine.Object) this.mDynamicMat != (UnityEngine.Object) null)
        return this.mDynamicMat.renderQueue;
      return this.mRenderQueue;
    }
  }

  public Transform cachedTransform
  {
    get
    {
      if ((UnityEngine.Object) this.mTrans == (UnityEngine.Object) null)
        this.mTrans = this.transform;
      return this.mTrans;
    }
  }

  public Material baseMaterial
  {
    get
    {
      return this.mMaterial;
    }
    set
    {
      if (!((UnityEngine.Object) this.mMaterial != (UnityEngine.Object) value))
        return;
      this.mMaterial = value;
      this.mRebuildMat = true;
    }
  }

  public Material dynamicMaterial
  {
    get
    {
      return this.mDynamicMat;
    }
  }

  public Texture mainTexture
  {
    get
    {
      return this.mTexture;
    }
    set
    {
      this.mTexture = value;
      if (!((UnityEngine.Object) this.mDynamicMat != (UnityEngine.Object) null))
        return;
      this.mDynamicMat.mainTexture = value;
    }
  }

  public Shader shader
  {
    get
    {
      return this.mShader;
    }
    set
    {
      if (!((UnityEngine.Object) this.mShader != (UnityEngine.Object) value))
        return;
      this.mShader = value;
      this.mRebuildMat = true;
    }
  }

  public int triangles
  {
    get
    {
      if ((UnityEngine.Object) this.mMesh != (UnityEngine.Object) null)
        return this.mTriangles;
      return 0;
    }
  }

  public bool isClipped
  {
    get
    {
      return this.mClipCount != 0;
    }
  }

  private void CreateMaterial()
  {
    this.mTextureClip = false;
    this.mLegacyShader = false;
    this.mClipCount = !((UnityEngine.Object) this.panel != (UnityEngine.Object) null) ? 0 : this.panel.clipCount;
    string str = (!((UnityEngine.Object) this.mShader != (UnityEngine.Object) null) ? (!((UnityEngine.Object) this.mMaterial != (UnityEngine.Object) null) ? "Unlit/Transparent Colored" : this.mMaterial.shader.name) : this.mShader.name).Replace("GUI/Text Shader", "Unlit/Text");
    if (str.Length > 2 && (int) str[str.Length - 2] == 32)
    {
      int num = (int) str[str.Length - 1];
      if (num > 48 && num <= 57)
        str = str.Substring(0, str.Length - 2);
    }
    if (str.StartsWith("Hidden/"))
      str = str.Substring(7);
    string name = str.Replace(" (SoftClip)", string.Empty).Replace(" (TextureClip)", string.Empty);
    if ((UnityEngine.Object) this.panel != (UnityEngine.Object) null && this.panel.clipping == UIDrawCall.Clipping.TextureMask)
    {
      this.mTextureClip = true;
      this.shader = Shader.Find("Hidden/" + name + " (TextureClip)");
    }
    else if (this.mClipCount != 0)
    {
      this.shader = Shader.Find("Hidden/" + name + " " + (object) this.mClipCount);
      if ((UnityEngine.Object) this.shader == (UnityEngine.Object) null)
        this.shader = Shader.Find(name + " " + (object) this.mClipCount);
      if ((UnityEngine.Object) this.shader == (UnityEngine.Object) null && this.mClipCount == 1)
      {
        this.mLegacyShader = true;
        this.shader = Shader.Find(name + " (SoftClip)");
      }
    }
    else
      this.shader = Shader.Find(name);
    if ((UnityEngine.Object) this.shader == (UnityEngine.Object) null)
      this.shader = Shader.Find("Unlit/Transparent Colored");
    if ((UnityEngine.Object) this.mMaterial != (UnityEngine.Object) null)
    {
      this.mDynamicMat = new Material(this.mMaterial);
      this.mDynamicMat.name = "[NGUI] " + this.mMaterial.name;
      this.mDynamicMat.hideFlags = HideFlags.DontSave | HideFlags.NotEditable;
      this.mDynamicMat.CopyPropertiesFromMaterial(this.mMaterial);
      foreach (string shaderKeyword in this.mMaterial.shaderKeywords)
        this.mDynamicMat.EnableKeyword(shaderKeyword);
      if ((UnityEngine.Object) this.shader != (UnityEngine.Object) null)
      {
        this.mDynamicMat.shader = this.shader;
      }
      else
      {
        if (this.mClipCount == 0)
          return;
        Debug.LogError((object) (name + " shader doesn't have a clipped shader version for " + (object) this.mClipCount + " clip regions"));
      }
    }
    else
    {
      this.mDynamicMat = new Material(this.shader);
      this.mDynamicMat.name = "[NGUI] " + this.shader.name;
      this.mDynamicMat.hideFlags = HideFlags.DontSave | HideFlags.NotEditable;
    }
  }

  private Material RebuildMaterial()
  {
    NGUITools.DestroyImmediate((UnityEngine.Object) this.mDynamicMat);
    this.CreateMaterial();
    this.mDynamicMat.renderQueue = this.mRenderQueue;
    if ((UnityEngine.Object) this.mTexture != (UnityEngine.Object) null)
      this.mDynamicMat.mainTexture = this.mTexture;
    if ((UnityEngine.Object) this.mRenderer != (UnityEngine.Object) null)
      this.mRenderer.sharedMaterials = new Material[1]
      {
        this.mDynamicMat
      };
    return this.mDynamicMat;
  }

  private void UpdateMaterials()
  {
    if ((UnityEngine.Object) this.panel == (UnityEngine.Object) null)
      return;
    if (this.mRebuildMat || (UnityEngine.Object) this.mDynamicMat == (UnityEngine.Object) null || (this.mClipCount != this.panel.clipCount || this.mTextureClip != (this.panel.clipping == UIDrawCall.Clipping.TextureMask)))
    {
      this.RebuildMaterial();
      this.mRebuildMat = false;
    }
    else
    {
      if (!((UnityEngine.Object) this.mRenderer.sharedMaterial != (UnityEngine.Object) this.mDynamicMat))
        return;
      this.mRenderer.sharedMaterials = new Material[1]
      {
        this.mDynamicMat
      };
    }
  }

  public void UpdateGeometry(int widgetCount)
  {
    this.widgetCount = widgetCount;
    int size = this.verts.size;
    if (size > 0 && size == this.uvs.size && (size == this.cols.size && size % 4 == 0))
    {
      if (UIDrawCall.mColorSpace == ColorSpace.Uninitialized)
        UIDrawCall.mColorSpace = QualitySettings.activeColorSpace;
      if (UIDrawCall.mColorSpace == ColorSpace.Linear)
      {
        for (int index = 0; index < this.cols.size; ++index)
        {
          Color col = this.cols[index];
          col.r = Mathf.GammaToLinearSpace(col.r);
          col.g = Mathf.GammaToLinearSpace(col.g);
          col.b = Mathf.GammaToLinearSpace(col.b);
          col.a = Mathf.GammaToLinearSpace(col.a);
          this.cols[index] = col;
        }
      }
      if ((UnityEngine.Object) this.mFilter == (UnityEngine.Object) null)
        this.mFilter = this.gameObject.GetComponent<MeshFilter>();
      if ((UnityEngine.Object) this.mFilter == (UnityEngine.Object) null)
        this.mFilter = this.gameObject.AddComponent<MeshFilter>();
      if (this.verts.size < 65000)
      {
        int indexCount = (size >> 1) * 3;
        bool flag1 = this.mIndices == null || this.mIndices.Length != indexCount;
        if ((UnityEngine.Object) this.mMesh == (UnityEngine.Object) null)
        {
          this.mMesh = new Mesh();
          this.mMesh.hideFlags = HideFlags.DontSave;
          this.mMesh.name = !((UnityEngine.Object) this.mMaterial != (UnityEngine.Object) null) ? "[NGUI] Mesh" : "[NGUI] " + this.mMaterial.name;
          if (UIDrawCall.dx9BugWorkaround == 0)
            this.mMesh.MarkDynamic();
          flag1 = true;
        }
        bool flag2 = this.uvs.buffer.Length != this.verts.buffer.Length || this.cols.buffer.Length != this.verts.buffer.Length || this.norms.buffer != null && this.norms.buffer.Length != this.verts.buffer.Length || this.tans.buffer != null && this.tans.buffer.Length != this.verts.buffer.Length;
        if (!flag2 && (UnityEngine.Object) this.panel != (UnityEngine.Object) null && this.panel.renderQueue != UIPanel.RenderQueue.Automatic)
          flag2 = (UnityEngine.Object) this.mMesh == (UnityEngine.Object) null || this.mMesh.vertexCount != this.verts.buffer.Length;
        if (!flag2 && this.verts.size << 1 < this.verts.buffer.Length)
          flag2 = true;
        this.mTriangles = this.verts.size >> 1;
        if (flag2 || this.verts.buffer.Length > 65000)
        {
          if (flag2 || this.mMesh.vertexCount != this.verts.size)
          {
            this.mMesh.Clear();
            flag1 = true;
          }
          this.mMesh.vertices = this.verts.ToArray();
          this.mMesh.uv = this.uvs.ToArray();
          this.mMesh.colors = this.cols.ToArray();
          if (this.norms != null)
            this.mMesh.normals = this.norms.ToArray();
          if (this.tans != null)
            this.mMesh.tangents = this.tans.ToArray();
        }
        else
        {
          if (this.mMesh.vertexCount != this.verts.buffer.Length)
          {
            this.mMesh.Clear();
            flag1 = true;
          }
          this.mMesh.vertices = this.verts.buffer;
          this.mMesh.uv = this.uvs.buffer;
          this.mMesh.colors = this.cols.buffer;
          if (this.norms != null)
            this.mMesh.normals = this.norms.buffer;
          if (this.tans != null)
            this.mMesh.tangents = this.tans.buffer;
        }
        if (flag1)
        {
          this.mIndices = this.GenerateCachedIndexBuffer(size, indexCount);
          this.mMesh.triangles = this.mIndices;
        }
        if (flag2 || !this.alwaysOnScreen)
          this.mMesh.RecalculateBounds();
        this.mFilter.mesh = this.mMesh;
      }
      else
      {
        this.mTriangles = 0;
        if ((UnityEngine.Object) this.mFilter.mesh != (UnityEngine.Object) null)
          this.mFilter.mesh.Clear();
        Debug.LogError((object) ("Too many vertices on one panel: " + (object) this.verts.size));
      }
      if ((UnityEngine.Object) this.mRenderer == (UnityEngine.Object) null)
        this.mRenderer = this.gameObject.GetComponent<MeshRenderer>();
      if ((UnityEngine.Object) this.mRenderer == (UnityEngine.Object) null)
        this.mRenderer = this.gameObject.AddComponent<MeshRenderer>();
      this.UpdateMaterials();
    }
    else
    {
      if ((UnityEngine.Object) this.mFilter.mesh != (UnityEngine.Object) null)
        this.mFilter.mesh.Clear();
      Debug.LogError((object) ("UIWidgets must fill the buffer with 4 vertices per quad. Found " + (object) size));
    }
    this.verts.Clear();
    this.uvs.Clear();
    this.cols.Clear();
    this.norms.Clear();
    this.tans.Clear();
  }

  private int[] GenerateCachedIndexBuffer(int vertexCount, int indexCount)
  {
    int index1 = 0;
    for (int count = UIDrawCall.mCache.Count; index1 < count; ++index1)
    {
      int[] numArray = UIDrawCall.mCache[index1];
      if (numArray != null && numArray.Length == indexCount)
        return numArray;
    }
    int[] numArray1 = new int[indexCount];
    int num1 = 0;
    int num2 = 0;
    while (num2 < vertexCount)
    {
      int[] numArray2 = numArray1;
      int index2 = num1;
      int num3 = 1;
      int num4 = index2 + num3;
      int num5 = num2;
      numArray2[index2] = num5;
      int[] numArray3 = numArray1;
      int index3 = num4;
      int num6 = 1;
      int num7 = index3 + num6;
      int num8 = num2 + 1;
      numArray3[index3] = num8;
      int[] numArray4 = numArray1;
      int index4 = num7;
      int num9 = 1;
      int num10 = index4 + num9;
      int num11 = num2 + 2;
      numArray4[index4] = num11;
      int[] numArray5 = numArray1;
      int index5 = num10;
      int num12 = 1;
      int num13 = index5 + num12;
      int num14 = num2 + 2;
      numArray5[index5] = num14;
      int[] numArray6 = numArray1;
      int index6 = num13;
      int num15 = 1;
      int num16 = index6 + num15;
      int num17 = num2 + 3;
      numArray6[index6] = num17;
      int[] numArray7 = numArray1;
      int index7 = num16;
      int num18 = 1;
      num1 = index7 + num18;
      int num19 = num2;
      numArray7[index7] = num19;
      num2 += 4;
    }
    if (UIDrawCall.mCache.Count > 10)
      UIDrawCall.mCache.RemoveAt(0);
    UIDrawCall.mCache.Add(numArray1);
    return numArray1;
  }

  private void OnWillRenderObject()
  {
    this.UpdateMaterials();
    if (this.onRender != null)
      this.onRender(this.mDynamicMat ?? this.mMaterial);
    if ((UnityEngine.Object) this.mDynamicMat == (UnityEngine.Object) null || this.mClipCount == 0)
      return;
    if (this.mTextureClip)
    {
      Vector4 drawCallClipRange = this.panel.drawCallClipRange;
      Vector2 clipSoftness = this.panel.clipSoftness;
      Vector2 vector2 = new Vector2(1000f, 1000f);
      if ((double) clipSoftness.x > 0.0)
        vector2.x = drawCallClipRange.z / clipSoftness.x;
      if ((double) clipSoftness.y > 0.0)
        vector2.y = drawCallClipRange.w / clipSoftness.y;
      this.mDynamicMat.SetVector(UIDrawCall.ClipRange[0], new Vector4(-drawCallClipRange.x / drawCallClipRange.z, -drawCallClipRange.y / drawCallClipRange.w, 1f / drawCallClipRange.z, 1f / drawCallClipRange.w));
      this.mDynamicMat.SetTexture("_ClipTex", (Texture) this.clipTexture);
    }
    else if (!this.mLegacyShader)
    {
      UIPanel uiPanel = this.panel;
      int num = 0;
      for (; (UnityEngine.Object) uiPanel != (UnityEngine.Object) null; uiPanel = uiPanel.parentPanel)
      {
        if (uiPanel.hasClipping)
        {
          float angle = 0.0f;
          Vector4 drawCallClipRange = uiPanel.drawCallClipRange;
          if ((UnityEngine.Object) uiPanel != (UnityEngine.Object) this.panel)
          {
            Vector3 vector3_1 = uiPanel.cachedTransform.InverseTransformPoint(this.panel.cachedTransform.position);
            drawCallClipRange.x -= vector3_1.x;
            drawCallClipRange.y -= vector3_1.y;
            Vector3 eulerAngles = this.panel.cachedTransform.rotation.eulerAngles;
            Vector3 vector3_2 = uiPanel.cachedTransform.rotation.eulerAngles - eulerAngles;
            vector3_2.x = NGUIMath.WrapAngle(vector3_2.x);
            vector3_2.y = NGUIMath.WrapAngle(vector3_2.y);
            vector3_2.z = NGUIMath.WrapAngle(vector3_2.z);
            if ((double) Mathf.Abs(vector3_2.x) > 1.0 / 1000.0 || (double) Mathf.Abs(vector3_2.y) > 1.0 / 1000.0)
              Debug.LogWarning((object) "Panel can only be clipped properly if X and Y rotation is left at 0", (UnityEngine.Object) this.panel);
            angle = vector3_2.z;
          }
          this.SetClipping(num++, drawCallClipRange, uiPanel.clipSoftness, angle);
        }
      }
    }
    else
    {
      Vector2 clipSoftness = this.panel.clipSoftness;
      Vector4 drawCallClipRange = this.panel.drawCallClipRange;
      Vector2 vector2_1 = new Vector2(-drawCallClipRange.x / drawCallClipRange.z, -drawCallClipRange.y / drawCallClipRange.w);
      Vector2 vector2_2 = new Vector2(1f / drawCallClipRange.z, 1f / drawCallClipRange.w);
      Vector2 vector2_3 = new Vector2(1000f, 1000f);
      if ((double) clipSoftness.x > 0.0)
        vector2_3.x = drawCallClipRange.z / clipSoftness.x;
      if ((double) clipSoftness.y > 0.0)
        vector2_3.y = drawCallClipRange.w / clipSoftness.y;
      this.mDynamicMat.mainTextureOffset = vector2_1;
      this.mDynamicMat.mainTextureScale = vector2_2;
      this.mDynamicMat.SetVector("_ClipSharpness", (Vector4) vector2_3);
    }
  }

  private void SetClipping(int index, Vector4 cr, Vector2 soft, float angle)
  {
    angle *= -1f * (float) Math.PI / 180f;
    Vector2 vector2 = new Vector2(1000f, 1000f);
    if ((double) soft.x > 0.0)
      vector2.x = cr.z / soft.x;
    if ((double) soft.y > 0.0)
      vector2.y = cr.w / soft.y;
    if (index >= UIDrawCall.ClipRange.Length)
      return;
    this.mDynamicMat.SetVector(UIDrawCall.ClipRange[index], new Vector4(-cr.x / cr.z, -cr.y / cr.w, 1f / cr.z, 1f / cr.w));
    this.mDynamicMat.SetVector(UIDrawCall.ClipArgs[index], new Vector4(vector2.x, vector2.y, Mathf.Sin(angle), Mathf.Cos(angle)));
  }

  private void Awake()
  {
    if (UIDrawCall.dx9BugWorkaround == -1)
    {
      int num;
      switch (Application.platform)
      {
        case RuntimePlatform.WindowsPlayer:
        case RuntimePlatform.XBOX360:
          if (SystemInfo.graphicsShaderLevel < 40 && SystemInfo.graphicsDeviceVersion.Contains("Direct3D"))
          {
            num = 1;
            break;
          }
          goto default;
        default:
          num = 0;
          break;
      }
      UIDrawCall.dx9BugWorkaround = num;
    }
    if (UIDrawCall.ClipRange == null)
      UIDrawCall.ClipRange = new int[4]
      {
        Shader.PropertyToID("_ClipRange0"),
        Shader.PropertyToID("_ClipRange1"),
        Shader.PropertyToID("_ClipRange2"),
        Shader.PropertyToID("_ClipRange4")
      };
    if (UIDrawCall.ClipArgs != null)
      return;
    UIDrawCall.ClipArgs = new int[4]
    {
      Shader.PropertyToID("_ClipArgs0"),
      Shader.PropertyToID("_ClipArgs1"),
      Shader.PropertyToID("_ClipArgs2"),
      Shader.PropertyToID("_ClipArgs3")
    };
  }

  private void OnEnable()
  {
    this.mRebuildMat = true;
  }

  private void OnDisable()
  {
    this.depthStart = int.MaxValue;
    this.depthEnd = int.MinValue;
    this.panel = (UIPanel) null;
    this.manager = (UIPanel) null;
    this.mMaterial = (Material) null;
    this.mTexture = (Texture) null;
    this.clipTexture = (Texture2D) null;
    if ((UnityEngine.Object) this.mRenderer != (UnityEngine.Object) null)
      this.mRenderer.sharedMaterials = new Material[0];
    NGUITools.DestroyImmediate((UnityEngine.Object) this.mDynamicMat);
    this.mDynamicMat = (Material) null;
  }

  private void OnDestroy()
  {
    NGUITools.DestroyImmediate((UnityEngine.Object) this.mMesh);
    this.mMesh = (Mesh) null;
  }

  public static UIDrawCall Create(UIPanel panel, Material mat, Texture tex, Shader shader)
  {
    return UIDrawCall.Create((string) null, panel, mat, tex, shader);
  }

  private static UIDrawCall Create(string name, UIPanel pan, Material mat, Texture tex, Shader shader)
  {
    UIDrawCall uiDrawCall = UIDrawCall.Create(name);
    uiDrawCall.gameObject.layer = pan.cachedGameObject.layer;
    uiDrawCall.baseMaterial = mat;
    uiDrawCall.mainTexture = tex;
    uiDrawCall.shader = shader;
    uiDrawCall.renderQueue = pan.startingRenderQueue;
    uiDrawCall.sortingOrder = pan.sortingOrder;
    uiDrawCall.manager = pan;
    return uiDrawCall;
  }

  private static UIDrawCall Create(string name)
  {
    while (UIDrawCall.mInactiveList.size > 0)
    {
      UIDrawCall uiDrawCall = UIDrawCall.mInactiveList.Pop();
      if ((UnityEngine.Object) uiDrawCall != (UnityEngine.Object) null)
      {
        UIDrawCall.mActiveList.Add(uiDrawCall);
        if (name != null)
          uiDrawCall.name = name;
        NGUITools.SetActive(uiDrawCall.gameObject, true);
        return uiDrawCall;
      }
    }
    GameObject gameObject = new GameObject(name);
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    UIDrawCall uiDrawCall1 = gameObject.AddComponent<UIDrawCall>();
    UIDrawCall.mActiveList.Add(uiDrawCall1);
    return uiDrawCall1;
  }

  public static void ClearAll()
  {
    bool isPlaying = Application.isPlaying;
    int size = UIDrawCall.mActiveList.size;
    while (size > 0)
    {
      UIDrawCall mActive = UIDrawCall.mActiveList[--size];
      if ((bool) ((UnityEngine.Object) mActive))
      {
        if (isPlaying)
          NGUITools.SetActive(mActive.gameObject, false);
        else
          NGUITools.DestroyImmediate((UnityEngine.Object) mActive.gameObject);
      }
    }
    UIDrawCall.mActiveList.Clear();
  }

  public static void ReleaseAll()
  {
    UIDrawCall.ClearAll();
    UIDrawCall.ReleaseInactive();
  }

  public static void ReleaseInactive()
  {
    int size = UIDrawCall.mInactiveList.size;
    while (size > 0)
    {
      UIDrawCall mInactive = UIDrawCall.mInactiveList[--size];
      if ((bool) ((UnityEngine.Object) mInactive))
        NGUITools.DestroyImmediate((UnityEngine.Object) mInactive.gameObject);
    }
    UIDrawCall.mInactiveList.Clear();
  }

  public static int Count(UIPanel panel)
  {
    int num = 0;
    for (int index = 0; index < UIDrawCall.mActiveList.size; ++index)
    {
      if ((UnityEngine.Object) UIDrawCall.mActiveList[index].manager == (UnityEngine.Object) panel)
        ++num;
    }
    return num;
  }

  public static void Destroy(UIDrawCall dc)
  {
    if (!(bool) ((UnityEngine.Object) dc))
      return;
    dc.onRender = (UIDrawCall.OnRenderCallback) null;
    if (Application.isPlaying)
    {
      if (!UIDrawCall.mActiveList.Remove(dc))
        return;
      NGUITools.SetActive(dc.gameObject, false);
      UIDrawCall.mInactiveList.Add(dc);
    }
    else
    {
      UIDrawCall.mActiveList.Remove(dc);
      NGUITools.DestroyImmediate((UnityEngine.Object) dc.gameObject);
    }
  }

  public static void MoveToScene(Scene scene)
  {
    foreach (Component active in UIDrawCall.activeList)
      SceneManager.MoveGameObjectToScene(active.gameObject, scene);
    foreach (Component inactive in UIDrawCall.inactiveList)
      SceneManager.MoveGameObjectToScene(inactive.gameObject, scene);
  }

  public enum Clipping
  {
    None = 0,
    TextureMask = 1,
    SoftClip = 3,
    ConstrainButDontClip = 4,
  }

  public delegate void OnRenderCallback(Material mat);
}
