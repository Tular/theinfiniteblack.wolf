﻿// Decompiled with JetBrains decompiler
// Type: EasyTouch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

public class EasyTouch : MonoBehaviour
{
  private EasyTouchInput input = new EasyTouchInput();
  private Finger[] fingers = new Finger[100];
  private TwoFingerGesture twoFinger = new TwoFingerGesture();
  private EasyTouch.DoubleTap[] singleDoubleTap = new EasyTouch.DoubleTap[100];
  private Finger[] tmpArray = new Finger[100];
  private EasyTouch.PickedObject pickedObject = new EasyTouch.PickedObject();
  private List<RaycastResult> uiRaycastResultCache = new List<RaycastResult>();
  public static EasyTouch _instance;
  public bool enable;
  public bool enableRemote;
  public EasyTouch.GesturePriority gesturePriority;
  public float StationaryTolerance;
  public float longTapTime;
  public float swipeTolerance;
  public float minPinchLength;
  public float minTwistAngle;
  public EasyTouch.DoubleTapDetection doubleTapDetection;
  public float doubleTapTime;
  public bool enable2FingersGesture;
  public bool enableTwist;
  public bool enablePinch;
  public bool enable2FingersSwipe;
  public EasyTouch.TwoFingerPickMethod twoFingerPickMethod;
  public List<ECamera> touchCameras;
  public bool autoSelect;
  public LayerMask pickableLayers3D;
  public bool enable2D;
  public LayerMask pickableLayers2D;
  public bool autoUpdatePickedObject;
  public bool allowUIDetection;
  public bool enableUIMode;
  public bool autoUpdatePickedUI;
  public bool enabledNGuiMode;
  public LayerMask nGUILayers;
  public List<Camera> nGUICameras;
  public bool enableSimulation;
  public KeyCode twistKey;
  public KeyCode swipeKey;
  public bool showGuiInspector;
  public bool showSelectInspector;
  public bool showGestureInspector;
  public bool showTwoFingerInspector;
  public bool showSecondFingerInspector;
  public Texture secondFingerTexture;
  private int oldTouchCount;
  private PointerEventData uiPointerEventData;
  private EventSystem uiEventSystem;

  public EasyTouch()
  {
    this.enable = true;
    this.allowUIDetection = true;
    this.enableUIMode = true;
    this.autoUpdatePickedUI = false;
    this.enabledNGuiMode = false;
    this.nGUICameras = new List<Camera>();
    this.autoSelect = true;
    this.touchCameras = new List<ECamera>();
    this.pickableLayers3D = (LayerMask) 1;
    this.enable2D = false;
    this.pickableLayers2D = (LayerMask) 1;
    this.gesturePriority = EasyTouch.GesturePriority.Tap;
    this.StationaryTolerance = 15f;
    this.longTapTime = 1f;
    this.doubleTapDetection = EasyTouch.DoubleTapDetection.BySystem;
    this.doubleTapTime = 0.3f;
    this.swipeTolerance = 0.85f;
    this.enable2FingersGesture = true;
    this.twoFingerPickMethod = EasyTouch.TwoFingerPickMethod.Finger;
    this.enable2FingersSwipe = true;
    this.enablePinch = true;
    this.minPinchLength = 0.1f;
    this.enableTwist = true;
    this.minTwistAngle = 0.1f;
    this.enableSimulation = true;
    this.twistKey = KeyCode.LeftAlt;
    this.swipeKey = KeyCode.LeftControl;
  }

  public static event EasyTouch.TouchCancelHandler On_Cancel;

  public static event EasyTouch.Cancel2FingersHandler On_Cancel2Fingers;

  public static event EasyTouch.TouchStartHandler On_TouchStart;

  public static event EasyTouch.TouchDownHandler On_TouchDown;

  public static event EasyTouch.TouchUpHandler On_TouchUp;

  public static event EasyTouch.SimpleTapHandler On_SimpleTap;

  public static event EasyTouch.DoubleTapHandler On_DoubleTap;

  public static event EasyTouch.LongTapStartHandler On_LongTapStart;

  public static event EasyTouch.LongTapHandler On_LongTap;

  public static event EasyTouch.LongTapEndHandler On_LongTapEnd;

  public static event EasyTouch.DragStartHandler On_DragStart;

  public static event EasyTouch.DragHandler On_Drag;

  public static event EasyTouch.DragEndHandler On_DragEnd;

  public static event EasyTouch.SwipeStartHandler On_SwipeStart;

  public static event EasyTouch.SwipeHandler On_Swipe;

  public static event EasyTouch.SwipeEndHandler On_SwipeEnd;

  public static event EasyTouch.TouchStart2FingersHandler On_TouchStart2Fingers;

  public static event EasyTouch.TouchDown2FingersHandler On_TouchDown2Fingers;

  public static event EasyTouch.TouchUp2FingersHandler On_TouchUp2Fingers;

  public static event EasyTouch.SimpleTap2FingersHandler On_SimpleTap2Fingers;

  public static event EasyTouch.DoubleTap2FingersHandler On_DoubleTap2Fingers;

  public static event EasyTouch.LongTapStart2FingersHandler On_LongTapStart2Fingers;

  public static event EasyTouch.LongTap2FingersHandler On_LongTap2Fingers;

  public static event EasyTouch.LongTapEnd2FingersHandler On_LongTapEnd2Fingers;

  public static event EasyTouch.TwistHandler On_Twist;

  public static event EasyTouch.TwistEndHandler On_TwistEnd;

  public static event EasyTouch.PinchHandler On_Pinch;

  public static event EasyTouch.PinchInHandler On_PinchIn;

  public static event EasyTouch.PinchOutHandler On_PinchOut;

  public static event EasyTouch.PinchEndHandler On_PinchEnd;

  public static event EasyTouch.DragStart2FingersHandler On_DragStart2Fingers;

  public static event EasyTouch.Drag2FingersHandler On_Drag2Fingers;

  public static event EasyTouch.DragEnd2FingersHandler On_DragEnd2Fingers;

  public static event EasyTouch.SwipeStart2FingersHandler On_SwipeStart2Fingers;

  public static event EasyTouch.Swipe2FingersHandler On_Swipe2Fingers;

  public static event EasyTouch.SwipeEnd2FingersHandler On_SwipeEnd2Fingers;

  public static event EasyTouch.EasyTouchIsReadyHandler On_EasyTouchIsReady;

  public static event EasyTouch.OverUIElementHandler On_OverUIElement;

  public static event EasyTouch.UIElementTouchUpHandler On_UIElementTouchUp;

  public static EasyTouch instance
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) EasyTouch._instance))
      {
        EasyTouch._instance = UnityEngine.Object.FindObjectOfType(typeof (EasyTouch)) as EasyTouch;
        if (!(bool) ((UnityEngine.Object) EasyTouch._instance))
          EasyTouch._instance = new GameObject("Easytouch").AddComponent<EasyTouch>();
      }
      return EasyTouch._instance;
    }
  }

  private void OnEnable()
  {
    if (!Application.isPlaying || !Application.isEditor)
      return;
    this.Init();
  }

  private void Awake()
  {
    this.Init();
  }

  private void Start()
  {
    for (int index = 0; index < 100; ++index)
      this.singleDoubleTap[index] = new EasyTouch.DoubleTap();
    if (this.touchCameras.FindIndex((Predicate<ECamera>) (c => (UnityEngine.Object) c.camera == (UnityEngine.Object) Camera.main)) < 0)
      this.touchCameras.Add(new ECamera(Camera.main, false));
    if (EasyTouch.On_EasyTouchIsReady == null)
      return;
    EasyTouch.On_EasyTouchIsReady();
  }

  private void Init()
  {
    if (!((UnityEngine.Object) this.secondFingerTexture == (UnityEngine.Object) null) || !this.enableSimulation)
      return;
    this.secondFingerTexture = Resources.Load("secondFinger") as Texture;
  }

  private void OnGUI()
  {
    if (!this.enableSimulation || this.enableRemote)
      return;
    Vector2 secondFingerPosition = this.input.GetSecondFingerPosition();
    if (!(secondFingerPosition != new Vector2(-1f, -1f)))
      return;
    GUI.DrawTexture(new Rect(secondFingerPosition.x - 16f, (float) ((double) Screen.height - (double) secondFingerPosition.y - 16.0), 32f, 32f), this.secondFingerTexture);
  }

  private void OnDrawGizmos()
  {
  }

  private void Update()
  {
    if (!this.enable || !((UnityEngine.Object) EasyTouch.instance == (UnityEngine.Object) this))
      return;
    int touchCount = this.input.TouchCount();
    if (this.oldTouchCount == 2 && touchCount != 2 && touchCount > 0)
      this.CreateGesture2Finger(EasyTouch.EventName.On_Cancel2Fingers, Vector2.zero, Vector2.zero, Vector2.zero, 0.0f, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, 0.0f);
    this.UpdateTouches(false, touchCount);
    this.twoFinger.oldPickedObject = this.twoFinger.pickedObject;
    if (this.enable2FingersGesture && touchCount == 2)
      this.TwoFinger();
    for (int fingerIndex = 0; fingerIndex < 100; ++fingerIndex)
    {
      if (this.fingers[fingerIndex] != null)
        this.OneFinger(fingerIndex);
    }
    this.oldTouchCount = touchCount;
  }

  private void UpdateTouches(bool realTouch, int touchCount)
  {
    this.fingers.CopyTo((Array) this.tmpArray, 0);
    if (realTouch || this.enableRemote)
    {
      this.ResetTouches();
      for (int index1 = 0; index1 < touchCount; ++index1)
      {
        UnityEngine.Touch touch = Input.GetTouch(index1);
        for (int index2 = 0; index2 < 100 && this.fingers[index1] == null; ++index2)
        {
          if (this.tmpArray[index2] != null && this.tmpArray[index2].fingerIndex == touch.fingerId)
            this.fingers[index1] = this.tmpArray[index2];
        }
        if (this.fingers[index1] == null)
        {
          this.fingers[index1] = new Finger();
          this.fingers[index1].fingerIndex = touch.fingerId;
          this.fingers[index1].gesture = EasyTouch.GestureType.None;
          this.fingers[index1].phase = TouchPhase.Began;
        }
        else
          this.fingers[index1].phase = touch.phase;
        if (this.fingers[index1].phase != TouchPhase.Began)
          this.fingers[index1].deltaPosition = touch.position - this.fingers[index1].position;
        else
          this.fingers[index1].deltaPosition = Vector2.zero;
        this.fingers[index1].position = touch.position;
        this.fingers[index1].tapCount = touch.tapCount;
        this.fingers[index1].deltaTime = touch.deltaTime;
        this.fingers[index1].touchCount = touchCount;
      }
    }
    else
    {
      for (int fingerIndex = 0; fingerIndex < touchCount; ++fingerIndex)
      {
        this.fingers[fingerIndex] = this.input.GetMouseTouch(fingerIndex, this.fingers[fingerIndex]);
        this.fingers[fingerIndex].touchCount = touchCount;
      }
    }
  }

  private void ResetTouches()
  {
    for (int index = 0; index < 100; ++index)
      this.fingers[index] = (Finger) null;
  }

  private void OneFinger(int fingerIndex)
  {
    if (this.fingers[fingerIndex].gesture == EasyTouch.GestureType.None)
    {
      if (!this.singleDoubleTap[fingerIndex].inDoubleTap)
      {
        this.singleDoubleTap[fingerIndex].inDoubleTap = true;
        this.singleDoubleTap[fingerIndex].time = 0.0f;
        this.singleDoubleTap[fingerIndex].count = 1;
      }
      this.fingers[fingerIndex].startTimeAction = Time.realtimeSinceStartup;
      this.fingers[fingerIndex].gesture = EasyTouch.GestureType.Acquisition;
      this.fingers[fingerIndex].startPosition = this.fingers[fingerIndex].position;
      if (this.autoSelect && this.GetPickedGameObject(this.fingers[fingerIndex], false))
      {
        this.fingers[fingerIndex].pickedObject = this.pickedObject.pickedObj;
        this.fingers[fingerIndex].isGuiCamera = this.pickedObject.isGUI;
        this.fingers[fingerIndex].pickedCamera = this.pickedObject.pickedCamera;
      }
      if (this.allowUIDetection)
      {
        this.fingers[fingerIndex].isOverGui = this.IsScreenPositionOverUI(this.fingers[fingerIndex].position);
        this.fingers[fingerIndex].pickedUIElement = this.GetFirstUIElementFromCache();
      }
      this.CreateGesture(fingerIndex, EasyTouch.EventName.On_TouchStart, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
    }
    if (this.singleDoubleTap[fingerIndex].inDoubleTap)
      this.singleDoubleTap[fingerIndex].time += Time.deltaTime;
    this.fingers[fingerIndex].actionTime = Time.realtimeSinceStartup - this.fingers[fingerIndex].startTimeAction;
    if (this.fingers[fingerIndex].phase == TouchPhase.Canceled)
      this.fingers[fingerIndex].gesture = EasyTouch.GestureType.Cancel;
    if (this.fingers[fingerIndex].phase != TouchPhase.Ended && this.fingers[fingerIndex].phase != TouchPhase.Canceled)
    {
      if (this.fingers[fingerIndex].phase == TouchPhase.Stationary && (double) this.fingers[fingerIndex].actionTime >= (double) this.longTapTime && this.fingers[fingerIndex].gesture == EasyTouch.GestureType.Acquisition)
      {
        this.fingers[fingerIndex].gesture = EasyTouch.GestureType.LongTap;
        this.CreateGesture(fingerIndex, EasyTouch.EventName.On_LongTapStart, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
      }
      if ((this.fingers[fingerIndex].gesture == EasyTouch.GestureType.Acquisition || this.fingers[fingerIndex].gesture == EasyTouch.GestureType.LongTap) && (this.fingers[fingerIndex].phase == TouchPhase.Moved && this.gesturePriority == EasyTouch.GesturePriority.Slips) || (this.fingers[fingerIndex].gesture == EasyTouch.GestureType.Acquisition || this.fingers[fingerIndex].gesture == EasyTouch.GestureType.LongTap) && (!this.FingerInTolerance(this.fingers[fingerIndex]) && this.gesturePriority == EasyTouch.GesturePriority.Tap))
      {
        if (this.fingers[fingerIndex].gesture == EasyTouch.GestureType.LongTap)
        {
          this.fingers[fingerIndex].gesture = EasyTouch.GestureType.Cancel;
          this.CreateGesture(fingerIndex, EasyTouch.EventName.On_LongTapEnd, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
          this.fingers[fingerIndex].gesture = EasyTouch.GestureType.Acquisition;
        }
        else
        {
          this.fingers[fingerIndex].oldSwipeType = EasyTouch.SwipeDirection.None;
          if ((bool) ((UnityEngine.Object) this.fingers[fingerIndex].pickedObject))
          {
            this.fingers[fingerIndex].gesture = EasyTouch.GestureType.Drag;
            this.CreateGesture(fingerIndex, EasyTouch.EventName.On_DragStart, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
          }
          else
          {
            this.fingers[fingerIndex].gesture = EasyTouch.GestureType.Swipe;
            this.CreateGesture(fingerIndex, EasyTouch.EventName.On_SwipeStart, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
          }
        }
      }
      EasyTouch.EventName message = EasyTouch.EventName.None;
      switch (this.fingers[fingerIndex].gesture)
      {
        case EasyTouch.GestureType.Drag:
          message = EasyTouch.EventName.On_Drag;
          break;
        case EasyTouch.GestureType.Swipe:
          message = EasyTouch.EventName.On_Swipe;
          break;
        case EasyTouch.GestureType.LongTap:
          message = EasyTouch.EventName.On_LongTap;
          break;
      }
      EasyTouch.SwipeDirection swipe = this.GetSwipe(new Vector2(0.0f, 0.0f), this.fingers[fingerIndex].deltaPosition);
      if (message != EasyTouch.EventName.None)
      {
        this.fingers[fingerIndex].oldSwipeType = swipe;
        this.CreateGesture(fingerIndex, message, this.fingers[fingerIndex], swipe, 0.0f, this.fingers[fingerIndex].deltaPosition);
      }
      this.CreateGesture(fingerIndex, EasyTouch.EventName.On_TouchDown, this.fingers[fingerIndex], swipe, 0.0f, this.fingers[fingerIndex].deltaPosition);
    }
    else
    {
      switch (this.fingers[fingerIndex].gesture)
      {
        case EasyTouch.GestureType.Drag:
          this.CreateGesture(fingerIndex, EasyTouch.EventName.On_DragEnd, this.fingers[fingerIndex], this.GetSwipe(this.fingers[fingerIndex].startPosition, this.fingers[fingerIndex].position), (this.fingers[fingerIndex].startPosition - this.fingers[fingerIndex].position).magnitude, this.fingers[fingerIndex].position - this.fingers[fingerIndex].startPosition);
          break;
        case EasyTouch.GestureType.Swipe:
          this.CreateGesture(fingerIndex, EasyTouch.EventName.On_SwipeEnd, this.fingers[fingerIndex], this.GetSwipe(this.fingers[fingerIndex].startPosition, this.fingers[fingerIndex].position), (this.fingers[fingerIndex].position - this.fingers[fingerIndex].startPosition).magnitude, this.fingers[fingerIndex].position - this.fingers[fingerIndex].startPosition);
          break;
        case EasyTouch.GestureType.LongTap:
          this.CreateGesture(fingerIndex, EasyTouch.EventName.On_LongTapEnd, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
          break;
        case EasyTouch.GestureType.Cancel:
          this.CreateGesture(fingerIndex, EasyTouch.EventName.On_Cancel, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
          break;
        case EasyTouch.GestureType.Acquisition:
          if (this.doubleTapDetection == EasyTouch.DoubleTapDetection.BySystem)
          {
            if (this.FingerInTolerance(this.fingers[fingerIndex]))
            {
              if (this.fingers[fingerIndex].tapCount < 2)
              {
                this.CreateGesture(fingerIndex, EasyTouch.EventName.On_SimpleTap, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
                break;
              }
              this.CreateGesture(fingerIndex, EasyTouch.EventName.On_DoubleTap, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
              break;
            }
            break;
          }
          if (!this.singleDoubleTap[fingerIndex].inWait)
          {
            this.singleDoubleTap[fingerIndex].finger = this.fingers[fingerIndex];
            this.StartCoroutine(this.SingleOrDouble(fingerIndex));
            break;
          }
          ++this.singleDoubleTap[fingerIndex].count;
          break;
      }
      this.CreateGesture(fingerIndex, EasyTouch.EventName.On_TouchUp, this.fingers[fingerIndex], EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero);
      this.fingers[fingerIndex] = (Finger) null;
    }
  }

  [DebuggerHidden]
  private IEnumerator SingleOrDouble(int fingerIndex)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EasyTouch.\u003CSingleOrDouble\u003Ec__Iterator0()
    {
      fingerIndex = fingerIndex,
      \u003C\u0024\u003EfingerIndex = fingerIndex,
      \u003C\u003Ef__this = this
    };
  }

  private void CreateGesture(int touchIndex, EasyTouch.EventName message, Finger finger, EasyTouch.SwipeDirection swipe, float swipeLength, Vector2 swipeVector)
  {
    bool flag = true;
    if (this.autoUpdatePickedUI && this.allowUIDetection)
    {
      finger.isOverGui = this.IsScreenPositionOverUI(finger.position);
      finger.pickedUIElement = this.GetFirstUIElementFromCache();
    }
    if (this.enabledNGuiMode && message == EasyTouch.EventName.On_TouchStart)
      finger.isOverGui = finger.isOverGui || this.IsTouchOverNGui(finger.position, false);
    if (this.enableUIMode || this.enabledNGuiMode)
      flag = !finger.isOverGui;
    Gesture gesture = finger.GetGesture();
    if (this.autoUpdatePickedObject && this.autoSelect && (message != EasyTouch.EventName.On_Drag && message != EasyTouch.EventName.On_DragEnd) && message != EasyTouch.EventName.On_DragStart)
    {
      if (this.GetPickedGameObject(finger, false))
      {
        gesture.pickedObject = this.pickedObject.pickedObj;
        gesture.pickedCamera = this.pickedObject.pickedCamera;
        gesture.isGuiCamera = this.pickedObject.isGUI;
      }
      else
      {
        gesture.pickedObject = (GameObject) null;
        gesture.pickedCamera = (Camera) null;
        gesture.isGuiCamera = false;
      }
    }
    gesture.swipe = swipe;
    gesture.swipeLength = swipeLength;
    gesture.swipeVector = swipeVector;
    gesture.deltaPinch = 0.0f;
    gesture.twistAngle = 0.0f;
    if (flag)
    {
      this.RaiseEvent(message, gesture);
    }
    else
    {
      if (!finger.isOverGui)
        return;
      if (message == EasyTouch.EventName.On_TouchUp)
        this.RaiseEvent(EasyTouch.EventName.On_UIElementTouchUp, gesture);
      else
        this.RaiseEvent(EasyTouch.EventName.On_OverUIElement, gesture);
    }
  }

  private void TwoFinger()
  {
    bool flag = false;
    if (this.twoFinger.currentGesture == EasyTouch.GestureType.None)
    {
      if (!this.singleDoubleTap[99].inDoubleTap)
      {
        this.singleDoubleTap[99].inDoubleTap = true;
        this.singleDoubleTap[99].time = 0.0f;
        this.singleDoubleTap[99].count = 1;
      }
      this.twoFinger.finger0 = this.GetTwoFinger(-1);
      this.twoFinger.finger1 = this.GetTwoFinger(this.twoFinger.finger0);
      this.twoFinger.startTimeAction = Time.realtimeSinceStartup;
      this.twoFinger.currentGesture = EasyTouch.GestureType.Acquisition;
      this.fingers[this.twoFinger.finger0].startPosition = this.fingers[this.twoFinger.finger0].position;
      this.fingers[this.twoFinger.finger1].startPosition = this.fingers[this.twoFinger.finger1].position;
      this.fingers[this.twoFinger.finger0].oldPosition = this.fingers[this.twoFinger.finger0].position;
      this.fingers[this.twoFinger.finger1].oldPosition = this.fingers[this.twoFinger.finger1].position;
      this.twoFinger.oldFingerDistance = Mathf.Abs(Vector2.Distance(this.fingers[this.twoFinger.finger0].position, this.fingers[this.twoFinger.finger1].position));
      this.twoFinger.startPosition = new Vector2((float) (((double) this.fingers[this.twoFinger.finger0].position.x + (double) this.fingers[this.twoFinger.finger1].position.x) / 2.0), (float) (((double) this.fingers[this.twoFinger.finger0].position.y + (double) this.fingers[this.twoFinger.finger1].position.y) / 2.0));
      this.twoFinger.position = this.twoFinger.startPosition;
      this.twoFinger.oldStartPosition = this.twoFinger.startPosition;
      this.twoFinger.deltaPosition = Vector2.zero;
      if (this.autoSelect)
      {
        if (this.GetTwoFingerPickedObject())
        {
          this.twoFinger.pickedObject = this.pickedObject.pickedObj;
          this.twoFinger.pickedCamera = this.pickedObject.pickedCamera;
          this.twoFinger.isGuiCamera = this.pickedObject.isGUI;
        }
        else
          this.twoFinger.ClearPickedObjectData();
      }
      if (this.allowUIDetection)
      {
        if (this.GetTwoFingerPickedUIElement())
        {
          this.twoFinger.pickedUIElement = this.pickedObject.pickedObj;
          this.twoFinger.isOverGui = true;
        }
        else
          this.twoFinger.ClearPickedUIData();
      }
      this.CreateGesture2Finger(EasyTouch.EventName.On_TouchStart2Fingers, this.twoFinger.startPosition, this.twoFinger.startPosition, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, this.twoFinger.oldFingerDistance);
    }
    if (this.singleDoubleTap[99].inDoubleTap)
      this.singleDoubleTap[99].time += Time.deltaTime;
    this.twoFinger.timeSinceStartAction = Time.realtimeSinceStartup - this.twoFinger.startTimeAction;
    this.twoFinger.position = new Vector2((float) (((double) this.fingers[this.twoFinger.finger0].position.x + (double) this.fingers[this.twoFinger.finger1].position.x) / 2.0), (float) (((double) this.fingers[this.twoFinger.finger0].position.y + (double) this.fingers[this.twoFinger.finger1].position.y) / 2.0));
    this.twoFinger.deltaPosition = this.twoFinger.position - this.twoFinger.oldStartPosition;
    this.twoFinger.fingerDistance = Mathf.Abs(Vector2.Distance(this.fingers[this.twoFinger.finger0].position, this.fingers[this.twoFinger.finger1].position));
    if (this.fingers[this.twoFinger.finger0].phase == TouchPhase.Canceled || this.fingers[this.twoFinger.finger1].phase == TouchPhase.Canceled)
      this.twoFinger.currentGesture = EasyTouch.GestureType.Cancel;
    if (this.fingers[this.twoFinger.finger0].phase != TouchPhase.Ended && this.fingers[this.twoFinger.finger1].phase != TouchPhase.Ended && this.twoFinger.currentGesture != EasyTouch.GestureType.Cancel)
    {
      if (this.twoFinger.currentGesture == EasyTouch.GestureType.Acquisition && (double) this.twoFinger.timeSinceStartAction >= (double) this.longTapTime && (this.FingerInTolerance(this.fingers[this.twoFinger.finger0]) && this.FingerInTolerance(this.fingers[this.twoFinger.finger1])))
      {
        this.twoFinger.currentGesture = EasyTouch.GestureType.LongTap;
        this.CreateGesture2Finger(EasyTouch.EventName.On_LongTapStart2Fingers, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, this.twoFinger.fingerDistance);
      }
      if ((!this.FingerInTolerance(this.fingers[this.twoFinger.finger0]) || !this.FingerInTolerance(this.fingers[this.twoFinger.finger1])) && this.gesturePriority == EasyTouch.GesturePriority.Tap || (this.fingers[this.twoFinger.finger0].phase == TouchPhase.Moved || this.fingers[this.twoFinger.finger1].phase == TouchPhase.Moved) && this.gesturePriority == EasyTouch.GesturePriority.Slips)
        flag = true;
      if (flag && this.twoFinger.currentGesture != EasyTouch.GestureType.Tap)
      {
        if (this.enable2FingersSwipe)
        {
          float num = Vector2.Dot(this.fingers[this.twoFinger.finger0].deltaPosition.normalized, this.fingers[this.twoFinger.finger1].deltaPosition.normalized);
          if ((double) num > 0.0)
          {
            if (this.twoFinger.oldGesture == EasyTouch.GestureType.LongTap)
            {
              this.CreateStateEnd2Fingers(this.twoFinger.currentGesture, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, false, this.twoFinger.fingerDistance);
              this.twoFinger.startTimeAction = Time.realtimeSinceStartup;
            }
            if ((bool) ((UnityEngine.Object) this.twoFinger.pickedObject) && !this.twoFinger.dragStart)
            {
              this.twoFinger.currentGesture = EasyTouch.GestureType.Drag;
              this.CreateGesture2Finger(EasyTouch.EventName.On_DragStart2Fingers, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, this.twoFinger.fingerDistance);
              this.twoFinger.dragStart = true;
            }
            else if (!(bool) ((UnityEngine.Object) this.twoFinger.pickedObject) && !this.twoFinger.swipeStart)
            {
              this.twoFinger.currentGesture = EasyTouch.GestureType.Swipe;
              this.CreateGesture2Finger(EasyTouch.EventName.On_SwipeStart2Fingers, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, this.twoFinger.fingerDistance);
              this.twoFinger.swipeStart = true;
            }
          }
          else if ((double) num < 0.0)
          {
            this.twoFinger.dragStart = false;
            this.twoFinger.swipeStart = false;
          }
          if (this.twoFinger.dragStart)
            this.CreateGesture2Finger(EasyTouch.EventName.On_Drag2Fingers, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, this.GetSwipe(this.twoFinger.oldStartPosition, this.twoFinger.position), 0.0f, this.twoFinger.deltaPosition, 0.0f, 0.0f, this.twoFinger.fingerDistance);
          if (this.twoFinger.swipeStart)
            this.CreateGesture2Finger(EasyTouch.EventName.On_Swipe2Fingers, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, this.GetSwipe(this.twoFinger.oldStartPosition, this.twoFinger.position), 0.0f, this.twoFinger.deltaPosition, 0.0f, 0.0f, this.twoFinger.fingerDistance);
        }
        Vector2 to = this.fingers[this.twoFinger.finger0].position - this.fingers[this.twoFinger.finger1].position;
        Vector2 from = this.fingers[this.twoFinger.finger0].oldPosition - this.fingers[this.twoFinger.finger1].oldPosition;
        float num1 = to.magnitude - from.magnitude;
        if (this.enablePinch && ((double) Mathf.Abs(num1) >= (double) this.minPinchLength && this.twoFinger.currentGesture != EasyTouch.GestureType.Pinch || this.twoFinger.currentGesture == EasyTouch.GestureType.Pinch))
        {
          if ((double) num1 != 0.0 && this.twoFinger.oldGesture == EasyTouch.GestureType.LongTap)
          {
            this.CreateStateEnd2Fingers(this.twoFinger.currentGesture, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, false, this.twoFinger.fingerDistance);
            this.twoFinger.startTimeAction = Time.realtimeSinceStartup;
          }
          this.twoFinger.currentGesture = EasyTouch.GestureType.Pinch;
          if ((double) num1 > 0.0)
            this.CreateGesture2Finger(EasyTouch.EventName.On_PinchOut, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, this.GetSwipe(this.twoFinger.startPosition, this.twoFinger.position), 0.0f, Vector2.zero, 0.0f, Mathf.Abs(this.twoFinger.fingerDistance - this.twoFinger.oldFingerDistance), this.twoFinger.fingerDistance);
          if ((double) num1 < 0.0)
            this.CreateGesture2Finger(EasyTouch.EventName.On_PinchIn, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, this.GetSwipe(this.twoFinger.startPosition, this.twoFinger.position), 0.0f, Vector2.zero, 0.0f, Mathf.Abs(this.twoFinger.fingerDistance - this.twoFinger.oldFingerDistance), this.twoFinger.fingerDistance);
          if ((double) num1 < 0.0 || (double) num1 > 0.0)
            this.CreateGesture2Finger(EasyTouch.EventName.On_Pinch, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, this.GetSwipe(this.twoFinger.startPosition, this.twoFinger.position), 0.0f, Vector2.zero, 0.0f, num1, this.twoFinger.fingerDistance);
        }
        if (this.enableTwist)
        {
          float num2 = Vector2.Angle(from, to);
          if (from == to)
            num2 = 0.0f;
          if ((double) Mathf.Abs(num2) >= (double) this.minTwistAngle && this.twoFinger.currentGesture != EasyTouch.GestureType.Twist || this.twoFinger.currentGesture == EasyTouch.GestureType.Twist)
          {
            if (this.twoFinger.oldGesture == EasyTouch.GestureType.LongTap)
            {
              this.CreateStateEnd2Fingers(this.twoFinger.currentGesture, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, false, this.twoFinger.fingerDistance);
              this.twoFinger.startTimeAction = Time.realtimeSinceStartup;
            }
            this.twoFinger.currentGesture = EasyTouch.GestureType.Twist;
            if ((double) num2 != 0.0)
              num2 *= Mathf.Sign(Vector3.Cross((Vector3) from, (Vector3) to).z);
            this.CreateGesture2Finger(EasyTouch.EventName.On_Twist, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, num2, 0.0f, this.twoFinger.fingerDistance);
          }
        }
      }
      else if (this.twoFinger.currentGesture == EasyTouch.GestureType.LongTap)
        this.CreateGesture2Finger(EasyTouch.EventName.On_LongTap2Fingers, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, this.twoFinger.fingerDistance);
      this.CreateGesture2Finger(EasyTouch.EventName.On_TouchDown2Fingers, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, this.GetSwipe(this.twoFinger.oldStartPosition, this.twoFinger.position), 0.0f, this.twoFinger.deltaPosition, 0.0f, 0.0f, this.twoFinger.fingerDistance);
      this.fingers[this.twoFinger.finger0].oldPosition = this.fingers[this.twoFinger.finger0].position;
      this.fingers[this.twoFinger.finger1].oldPosition = this.fingers[this.twoFinger.finger1].position;
      this.twoFinger.oldFingerDistance = this.twoFinger.fingerDistance;
      this.twoFinger.oldStartPosition = this.twoFinger.position;
      this.twoFinger.oldGesture = this.twoFinger.currentGesture;
    }
    else if (this.twoFinger.currentGesture != EasyTouch.GestureType.Acquisition && this.twoFinger.currentGesture != EasyTouch.GestureType.Tap)
    {
      this.CreateStateEnd2Fingers(this.twoFinger.currentGesture, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, true, this.twoFinger.fingerDistance);
      this.twoFinger.currentGesture = EasyTouch.GestureType.None;
      this.twoFinger.pickedObject = (GameObject) null;
      this.twoFinger.swipeStart = false;
      this.twoFinger.dragStart = false;
    }
    else
    {
      this.twoFinger.currentGesture = EasyTouch.GestureType.Tap;
      this.CreateStateEnd2Fingers(this.twoFinger.currentGesture, this.twoFinger.startPosition, this.twoFinger.position, this.twoFinger.deltaPosition, this.twoFinger.timeSinceStartAction, true, this.twoFinger.fingerDistance);
    }
  }

  private void CreateStateEnd2Fingers(EasyTouch.GestureType gesture, Vector2 startPosition, Vector2 position, Vector2 deltaPosition, float time, bool realEnd, float fingerDistance)
  {
    switch (gesture)
    {
      case EasyTouch.GestureType.Tap:
      case EasyTouch.GestureType.Acquisition:
        if (this.doubleTapDetection == EasyTouch.DoubleTapDetection.BySystem)
        {
          if (this.fingers[this.twoFinger.finger0].tapCount < 2 && this.fingers[this.twoFinger.finger1].tapCount < 2)
            this.CreateGesture2Finger(EasyTouch.EventName.On_SimpleTap2Fingers, startPosition, position, deltaPosition, time, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, fingerDistance);
          else
            this.CreateGesture2Finger(EasyTouch.EventName.On_DoubleTap2Fingers, startPosition, position, deltaPosition, time, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, fingerDistance);
          this.twoFinger.currentGesture = EasyTouch.GestureType.None;
          this.twoFinger.pickedObject = (GameObject) null;
          this.twoFinger.swipeStart = false;
          this.twoFinger.dragStart = false;
          this.singleDoubleTap[99].Stop();
          this.StopCoroutine("SingleOrDouble2Fingers");
          break;
        }
        if (!this.singleDoubleTap[99].inWait)
        {
          this.StartCoroutine("SingleOrDouble2Fingers");
          break;
        }
        ++this.singleDoubleTap[99].count;
        break;
      case EasyTouch.GestureType.LongTap:
        this.CreateGesture2Finger(EasyTouch.EventName.On_LongTapEnd2Fingers, startPosition, position, deltaPosition, time, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, fingerDistance);
        break;
      case EasyTouch.GestureType.Pinch:
        this.CreateGesture2Finger(EasyTouch.EventName.On_PinchEnd, startPosition, position, deltaPosition, time, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, fingerDistance);
        break;
      case EasyTouch.GestureType.Twist:
        this.CreateGesture2Finger(EasyTouch.EventName.On_TwistEnd, startPosition, position, deltaPosition, time, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, fingerDistance);
        break;
    }
    if (!realEnd)
      return;
    if (this.twoFinger.dragStart)
      this.CreateGesture2Finger(EasyTouch.EventName.On_DragEnd2Fingers, startPosition, position, deltaPosition, time, this.GetSwipe(startPosition, position), (position - startPosition).magnitude, position - startPosition, 0.0f, 0.0f, fingerDistance);
    if (this.twoFinger.swipeStart)
      this.CreateGesture2Finger(EasyTouch.EventName.On_SwipeEnd2Fingers, startPosition, position, deltaPosition, time, this.GetSwipe(startPosition, position), (position - startPosition).magnitude, position - startPosition, 0.0f, 0.0f, fingerDistance);
    this.CreateGesture2Finger(EasyTouch.EventName.On_TouchUp2Fingers, startPosition, position, deltaPosition, time, EasyTouch.SwipeDirection.None, 0.0f, Vector2.zero, 0.0f, 0.0f, fingerDistance);
  }

  [DebuggerHidden]
  private IEnumerator SingleOrDouble2Fingers()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EasyTouch.\u003CSingleOrDouble2Fingers\u003Ec__Iterator1()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void CreateGesture2Finger(EasyTouch.EventName message, Vector2 startPosition, Vector2 position, Vector2 deltaPosition, float actionTime, EasyTouch.SwipeDirection swipe, float swipeLength, Vector2 swipeVector, float twist, float pinch, float twoDistance)
  {
    bool flag = true;
    Gesture gesture = new Gesture();
    gesture.isOverGui = false;
    if (this.enabledNGuiMode && message == EasyTouch.EventName.On_TouchStart2Fingers)
      gesture.isOverGui = gesture.isOverGui || this.IsTouchOverNGui(this.twoFinger.position, false) && this.IsTouchOverNGui(this.twoFinger.position, false);
    gesture.touchCount = 2;
    gesture.fingerIndex = -1;
    gesture.startPosition = startPosition;
    gesture.position = position;
    gesture.deltaPosition = deltaPosition;
    gesture.actionTime = actionTime;
    gesture.deltaTime = Time.deltaTime;
    gesture.swipe = swipe;
    gesture.swipeLength = swipeLength;
    gesture.swipeVector = swipeVector;
    gesture.deltaPinch = pinch;
    gesture.twistAngle = twist;
    gesture.twoFingerDistance = twoDistance;
    gesture.pickedObject = this.twoFinger.pickedObject;
    gesture.pickedCamera = this.twoFinger.pickedCamera;
    gesture.isGuiCamera = this.twoFinger.isGuiCamera;
    if (this.autoUpdatePickedObject && message != EasyTouch.EventName.On_Drag && (message != EasyTouch.EventName.On_DragEnd && message != EasyTouch.EventName.On_Twist) && (message != EasyTouch.EventName.On_TwistEnd && message != EasyTouch.EventName.On_Pinch && (message != EasyTouch.EventName.On_PinchEnd && message != EasyTouch.EventName.On_PinchIn)) && message != EasyTouch.EventName.On_PinchOut)
    {
      if (this.GetTwoFingerPickedObject())
      {
        gesture.pickedObject = this.pickedObject.pickedObj;
        gesture.pickedCamera = this.pickedObject.pickedCamera;
        gesture.isGuiCamera = this.pickedObject.isGUI;
      }
      else
        this.twoFinger.ClearPickedObjectData();
    }
    gesture.pickedUIElement = this.twoFinger.pickedUIElement;
    gesture.isOverGui = this.twoFinger.isOverGui;
    if (this.allowUIDetection && this.autoUpdatePickedUI && (message != EasyTouch.EventName.On_Drag && message != EasyTouch.EventName.On_DragEnd) && (message != EasyTouch.EventName.On_Twist && message != EasyTouch.EventName.On_TwistEnd && (message != EasyTouch.EventName.On_Pinch && message != EasyTouch.EventName.On_PinchEnd)) && (message != EasyTouch.EventName.On_PinchIn && message != EasyTouch.EventName.On_PinchOut && message == EasyTouch.EventName.On_SimpleTap2Fingers))
    {
      if (this.GetTwoFingerPickedUIElement())
      {
        gesture.pickedUIElement = this.pickedObject.pickedObj;
        gesture.isOverGui = true;
      }
      else
        this.twoFinger.ClearPickedUIData();
    }
    if (this.enableUIMode || this.enabledNGuiMode && this.allowUIDetection)
      flag = !gesture.isOverGui;
    if (flag)
    {
      this.RaiseEvent(message, gesture);
    }
    else
    {
      if (!gesture.isOverGui)
        return;
      if (message == EasyTouch.EventName.On_TouchUp2Fingers)
        this.RaiseEvent(EasyTouch.EventName.On_UIElementTouchUp, gesture);
      else
        this.RaiseEvent(EasyTouch.EventName.On_OverUIElement, gesture);
    }
  }

  private int GetTwoFinger(int index)
  {
    int index1 = index + 1;
    for (bool flag = false; index1 < 10 && !flag; ++index1)
    {
      if (this.fingers[index1] != null && index1 >= index)
        flag = true;
    }
    return index1 - 1;
  }

  private bool GetTwoFingerPickedObject()
  {
    bool flag = false;
    if (this.twoFingerPickMethod == EasyTouch.TwoFingerPickMethod.Finger)
    {
      if (this.GetPickedGameObject(this.fingers[this.twoFinger.finger0], false) && (this.GetPickedGameObject(this.fingers[this.twoFinger.finger1], false) && (UnityEngine.Object) this.pickedObject.pickedObj == (UnityEngine.Object) this.pickedObject.pickedObj))
        flag = true;
    }
    else if (this.GetPickedGameObject(this.fingers[this.twoFinger.finger0], true))
      flag = true;
    return flag;
  }

  private bool GetTwoFingerPickedUIElement()
  {
    bool flag = false;
    if (this.fingers[this.twoFinger.finger0] == null)
      return false;
    if (this.twoFingerPickMethod == EasyTouch.TwoFingerPickMethod.Finger)
    {
      if (this.IsScreenPositionOverUI(this.fingers[this.twoFinger.finger0].position))
      {
        GameObject elementFromCache1 = this.GetFirstUIElementFromCache();
        if (this.IsScreenPositionOverUI(this.fingers[this.twoFinger.finger1].position))
        {
          GameObject elementFromCache2 = this.GetFirstUIElementFromCache();
          if ((UnityEngine.Object) elementFromCache2 == (UnityEngine.Object) elementFromCache1 || elementFromCache2.transform.IsChildOf(elementFromCache1.transform) || elementFromCache1.transform.IsChildOf(elementFromCache2.transform))
          {
            this.pickedObject.pickedObj = elementFromCache1;
            this.pickedObject.isGUI = true;
            flag = true;
          }
        }
      }
    }
    else if (this.IsScreenPositionOverUI(this.twoFinger.position))
    {
      this.pickedObject.pickedObj = this.GetFirstUIElementFromCache();
      this.pickedObject.isGUI = true;
      flag = true;
    }
    return flag;
  }

  private void RaiseEvent(EasyTouch.EventName evnt, Gesture gesture)
  {
    switch (evnt)
    {
      case EasyTouch.EventName.On_TouchStart:
        if (EasyTouch.On_TouchStart == null)
          break;
        EasyTouch.On_TouchStart(gesture);
        break;
      case EasyTouch.EventName.On_TouchDown:
        if (EasyTouch.On_TouchDown == null)
          break;
        EasyTouch.On_TouchDown(gesture);
        break;
      case EasyTouch.EventName.On_TouchUp:
        if (EasyTouch.On_TouchUp == null)
          break;
        EasyTouch.On_TouchUp(gesture);
        break;
      case EasyTouch.EventName.On_SimpleTap:
        if (EasyTouch.On_SimpleTap == null)
          break;
        EasyTouch.On_SimpleTap(gesture);
        break;
      case EasyTouch.EventName.On_DoubleTap:
        if (EasyTouch.On_DoubleTap == null)
          break;
        EasyTouch.On_DoubleTap(gesture);
        break;
      case EasyTouch.EventName.On_LongTapStart:
        if (EasyTouch.On_LongTapStart == null)
          break;
        EasyTouch.On_LongTapStart(gesture);
        break;
      case EasyTouch.EventName.On_LongTap:
        if (EasyTouch.On_LongTap == null)
          break;
        EasyTouch.On_LongTap(gesture);
        break;
      case EasyTouch.EventName.On_LongTapEnd:
        if (EasyTouch.On_LongTapEnd == null)
          break;
        EasyTouch.On_LongTapEnd(gesture);
        break;
      case EasyTouch.EventName.On_DragStart:
        if (EasyTouch.On_DragStart == null)
          break;
        EasyTouch.On_DragStart(gesture);
        break;
      case EasyTouch.EventName.On_Drag:
        if (EasyTouch.On_Drag == null)
          break;
        EasyTouch.On_Drag(gesture);
        break;
      case EasyTouch.EventName.On_DragEnd:
        if (EasyTouch.On_DragEnd == null)
          break;
        EasyTouch.On_DragEnd(gesture);
        break;
      case EasyTouch.EventName.On_SwipeStart:
        if (EasyTouch.On_SwipeStart == null)
          break;
        EasyTouch.On_SwipeStart(gesture);
        break;
      case EasyTouch.EventName.On_Swipe:
        if (EasyTouch.On_Swipe == null)
          break;
        EasyTouch.On_Swipe(gesture);
        break;
      case EasyTouch.EventName.On_SwipeEnd:
        if (EasyTouch.On_SwipeEnd == null)
          break;
        EasyTouch.On_SwipeEnd(gesture);
        break;
      case EasyTouch.EventName.On_TouchStart2Fingers:
        if (EasyTouch.On_TouchStart2Fingers == null)
          break;
        EasyTouch.On_TouchStart2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_TouchDown2Fingers:
        if (EasyTouch.On_TouchDown2Fingers == null)
          break;
        EasyTouch.On_TouchDown2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_TouchUp2Fingers:
        if (EasyTouch.On_TouchUp2Fingers == null)
          break;
        EasyTouch.On_TouchUp2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_SimpleTap2Fingers:
        if (EasyTouch.On_SimpleTap2Fingers == null)
          break;
        EasyTouch.On_SimpleTap2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_DoubleTap2Fingers:
        if (EasyTouch.On_DoubleTap2Fingers == null)
          break;
        EasyTouch.On_DoubleTap2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_LongTapStart2Fingers:
        if (EasyTouch.On_LongTapStart2Fingers == null)
          break;
        EasyTouch.On_LongTapStart2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_LongTap2Fingers:
        if (EasyTouch.On_LongTap2Fingers == null)
          break;
        EasyTouch.On_LongTap2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_LongTapEnd2Fingers:
        if (EasyTouch.On_LongTapEnd2Fingers == null)
          break;
        EasyTouch.On_LongTapEnd2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_Twist:
        if (EasyTouch.On_Twist == null)
          break;
        EasyTouch.On_Twist(gesture);
        break;
      case EasyTouch.EventName.On_TwistEnd:
        if (EasyTouch.On_TwistEnd == null)
          break;
        EasyTouch.On_TwistEnd(gesture);
        break;
      case EasyTouch.EventName.On_Pinch:
        if (EasyTouch.On_Pinch == null)
          break;
        EasyTouch.On_Pinch(gesture);
        break;
      case EasyTouch.EventName.On_PinchIn:
        if (EasyTouch.On_PinchIn == null)
          break;
        EasyTouch.On_PinchIn(gesture);
        break;
      case EasyTouch.EventName.On_PinchOut:
        if (EasyTouch.On_PinchOut == null)
          break;
        EasyTouch.On_PinchOut(gesture);
        break;
      case EasyTouch.EventName.On_PinchEnd:
        if (EasyTouch.On_PinchEnd == null)
          break;
        EasyTouch.On_PinchEnd(gesture);
        break;
      case EasyTouch.EventName.On_DragStart2Fingers:
        if (EasyTouch.On_DragStart2Fingers == null)
          break;
        EasyTouch.On_DragStart2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_Drag2Fingers:
        if (EasyTouch.On_Drag2Fingers == null)
          break;
        EasyTouch.On_Drag2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_DragEnd2Fingers:
        if (EasyTouch.On_DragEnd2Fingers == null)
          break;
        EasyTouch.On_DragEnd2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_SwipeStart2Fingers:
        if (EasyTouch.On_SwipeStart2Fingers == null)
          break;
        EasyTouch.On_SwipeStart2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_Swipe2Fingers:
        if (EasyTouch.On_Swipe2Fingers == null)
          break;
        EasyTouch.On_Swipe2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_SwipeEnd2Fingers:
        if (EasyTouch.On_SwipeEnd2Fingers == null)
          break;
        EasyTouch.On_SwipeEnd2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_Cancel:
        if (EasyTouch.On_Cancel == null)
          break;
        EasyTouch.On_Cancel(gesture);
        break;
      case EasyTouch.EventName.On_Cancel2Fingers:
        if (EasyTouch.On_Cancel2Fingers == null)
          break;
        EasyTouch.On_Cancel2Fingers(gesture);
        break;
      case EasyTouch.EventName.On_OverUIElement:
        if (EasyTouch.On_OverUIElement == null)
          break;
        EasyTouch.On_OverUIElement(gesture);
        break;
      case EasyTouch.EventName.On_UIElementTouchUp:
        if (EasyTouch.On_UIElementTouchUp == null)
          break;
        EasyTouch.On_UIElementTouchUp(gesture);
        break;
    }
  }

  private bool GetPickedGameObject(Finger finger, bool isTowFinger = false)
  {
    if (finger == null)
      return false;
    this.pickedObject.isGUI = false;
    this.pickedObject.pickedObj = (GameObject) null;
    this.pickedObject.pickedCamera = (Camera) null;
    if (this.touchCameras.Count > 0)
    {
      for (int index = 0; index < this.touchCameras.Count; ++index)
      {
        if ((UnityEngine.Object) this.touchCameras[index].camera != (UnityEngine.Object) null && this.touchCameras[index].camera.enabled)
        {
          Vector2 zero = Vector2.zero;
          Vector2 vector2 = isTowFinger ? this.twoFinger.position : finger.position;
          Ray ray = this.touchCameras[index].camera.ScreenPointToRay((Vector3) vector2);
          if (this.enable2D)
          {
            LayerMask pickableLayers2D = this.pickableLayers2D;
            RaycastHit2D[] results = new RaycastHit2D[1];
            if (Physics2D.GetRayIntersectionNonAlloc(ray, results, float.PositiveInfinity, (int) pickableLayers2D) > 0)
            {
              this.pickedObject.pickedCamera = this.touchCameras[index].camera;
              this.pickedObject.isGUI = this.touchCameras[index].guiCamera;
              this.pickedObject.pickedObj = results[0].collider.gameObject;
              return true;
            }
          }
          LayerMask pickableLayers3D = this.pickableLayers3D;
          RaycastHit hitInfo;
          if (Physics.Raycast(ray, out hitInfo, float.MaxValue, (int) pickableLayers3D))
          {
            this.pickedObject.pickedCamera = this.touchCameras[index].camera;
            this.pickedObject.isGUI = this.touchCameras[index].guiCamera;
            this.pickedObject.pickedObj = hitInfo.collider.gameObject;
            return true;
          }
        }
      }
    }
    else
      UnityEngine.Debug.LogWarning((object) "No camera is assigned to EasyTouch");
    return false;
  }

  private EasyTouch.SwipeDirection GetSwipe(Vector2 start, Vector2 end)
  {
    Vector2 normalized = (end - start).normalized;
    if ((double) Vector2.Dot(normalized, Vector2.up) >= (double) this.swipeTolerance)
      return EasyTouch.SwipeDirection.Up;
    if ((double) Vector2.Dot(normalized, -Vector2.up) >= (double) this.swipeTolerance)
      return EasyTouch.SwipeDirection.Down;
    if ((double) Vector2.Dot(normalized, Vector2.right) >= (double) this.swipeTolerance)
      return EasyTouch.SwipeDirection.Right;
    if ((double) Vector2.Dot(normalized, -Vector2.right) >= (double) this.swipeTolerance)
      return EasyTouch.SwipeDirection.Left;
    if ((double) Vector2.Dot(normalized, new Vector2(0.5f, 0.5f).normalized) >= (double) this.swipeTolerance)
      return EasyTouch.SwipeDirection.UpRight;
    if ((double) Vector2.Dot(normalized, new Vector2(0.5f, -0.5f).normalized) >= (double) this.swipeTolerance)
      return EasyTouch.SwipeDirection.DownRight;
    if ((double) Vector2.Dot(normalized, new Vector2(-0.5f, 0.5f).normalized) >= (double) this.swipeTolerance)
      return EasyTouch.SwipeDirection.UpLeft;
    return (double) Vector2.Dot(normalized, new Vector2(-0.5f, -0.5f).normalized) >= (double) this.swipeTolerance ? EasyTouch.SwipeDirection.DownLeft : EasyTouch.SwipeDirection.Other;
  }

  private bool FingerInTolerance(Finger finger)
  {
    return (double) (finger.position - finger.startPosition).sqrMagnitude <= (double) this.StationaryTolerance * (double) this.StationaryTolerance;
  }

  private bool IsTouchOverNGui(Vector2 position, bool isTwoFingers = false)
  {
    bool flag = false;
    if (this.enabledNGuiMode)
    {
      LayerMask nGuiLayers = this.nGUILayers;
      for (int index = 0; !flag && index < this.nGUICameras.Count; ++index)
      {
        Vector2 zero = Vector2.zero;
        Vector2 vector2 = isTwoFingers ? this.twoFinger.position : position;
        RaycastHit hitInfo;
        flag = Physics.Raycast(this.nGUICameras[index].ScreenPointToRay((Vector3) vector2), out hitInfo, float.MaxValue, (int) nGuiLayers);
      }
    }
    return flag;
  }

  private Finger GetFinger(int finderId)
  {
    int index = 0;
    Finger finger;
    for (finger = (Finger) null; index < 10 && finger == null; ++index)
    {
      if (this.fingers[index] != null && this.fingers[index].fingerIndex == finderId)
        finger = this.fingers[index];
    }
    return finger;
  }

  private bool IsScreenPositionOverUI(Vector2 position)
  {
    this.uiEventSystem = EventSystem.current;
    if (!((UnityEngine.Object) this.uiEventSystem != (UnityEngine.Object) null))
      return false;
    this.uiPointerEventData = new PointerEventData(this.uiEventSystem);
    this.uiPointerEventData.position = position;
    this.uiEventSystem.RaycastAll(this.uiPointerEventData, this.uiRaycastResultCache);
    return this.uiRaycastResultCache.Count > 0;
  }

  private GameObject GetFirstUIElementFromCache()
  {
    if (this.uiRaycastResultCache.Count > 0)
      return this.uiRaycastResultCache[0].gameObject;
    return (GameObject) null;
  }

  private GameObject GetFirstUIElement(Vector2 position)
  {
    if (this.IsScreenPositionOverUI(position))
      return this.GetFirstUIElementFromCache();
    return (GameObject) null;
  }

  public static bool IsFingerOverUIElement(int fingerIndex)
  {
    if (!((UnityEngine.Object) EasyTouch.instance != (UnityEngine.Object) null))
      return false;
    Finger finger = EasyTouch.instance.GetFinger(fingerIndex);
    if (finger != null)
      return EasyTouch.instance.IsScreenPositionOverUI(finger.position);
    return false;
  }

  public static GameObject GetCurrentPickedUIElement(int fingerIndex)
  {
    if (!((UnityEngine.Object) EasyTouch.instance != (UnityEngine.Object) null))
      return (GameObject) null;
    Finger finger = EasyTouch.instance.GetFinger(fingerIndex);
    if (finger != null)
      return EasyTouch.instance.GetFirstUIElement(finger.position);
    return (GameObject) null;
  }

  public static GameObject GetCurrentPickedObject(int fingerIndex)
  {
    if (!((UnityEngine.Object) EasyTouch.instance != (UnityEngine.Object) null))
      return (GameObject) null;
    Finger finger = EasyTouch.instance.GetFinger(fingerIndex);
    if (finger != null && EasyTouch.instance.GetPickedGameObject(finger, false))
      return EasyTouch.instance.pickedObject.pickedObj;
    return (GameObject) null;
  }

  public static int GetTouchCount()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.input.TouchCount();
    return 0;
  }

  public static void ResetTouch(int fingerIndex)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.GetFinger(fingerIndex).gesture = EasyTouch.GestureType.None;
  }

  public static void SetEnabled(bool enable)
  {
    EasyTouch.instance.enable = enable;
    if (!enable)
      return;
    EasyTouch.instance.ResetTouches();
  }

  public static bool GetEnabled()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.enable;
    return false;
  }

  public static void SetUICompatibily(bool value)
  {
    if (!((UnityEngine.Object) EasyTouch.instance != (UnityEngine.Object) null))
      return;
    EasyTouch.instance.enableUIMode = value;
  }

  public static bool GetUIComptability()
  {
    if ((UnityEngine.Object) EasyTouch.instance != (UnityEngine.Object) null)
      return EasyTouch.instance.enableUIMode;
    return false;
  }

  public static void SetAutoUpdateUI(bool value)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.autoUpdatePickedUI = value;
  }

  public static bool GetAutoUpdateUI()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.autoUpdatePickedUI;
    return false;
  }

  public static void SetNGUICompatibility(bool value)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.enabledNGuiMode = value;
  }

  public static bool GetNGUICompatibility()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.enabledNGuiMode;
    return false;
  }

  public static void SetEnableAutoSelect(bool value)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.autoSelect = value;
  }

  public static bool GetEnableAutoSelect()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.autoSelect;
    return false;
  }

  public static void SetAutoUpdatePickedObject(bool value)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.autoUpdatePickedObject = value;
  }

  public static bool GetAutoUpdatePickedObject()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.autoUpdatePickedObject;
    return false;
  }

  public static void Set3DPickableLayer(LayerMask mask)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.pickableLayers3D = mask;
  }

  public static LayerMask Get3DPickableLayer()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.pickableLayers3D;
    return (LayerMask) LayerMask.GetMask("Default");
  }

  public static void AddCamera(Camera cam, bool guiCam = false)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.touchCameras.Add(new ECamera(cam, guiCam));
  }

  public static void RemoveCamera(Camera cam)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    int index = EasyTouch.instance.touchCameras.FindIndex((Predicate<ECamera>) (c => (UnityEngine.Object) c.camera == (UnityEngine.Object) cam));
    if (index <= -1)
      return;
    EasyTouch.instance.touchCameras[index] = (ECamera) null;
    EasyTouch.instance.touchCameras.RemoveAt(index);
  }

  public static Camera GetCamera(int index = 0)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return (Camera) null;
    if (index < EasyTouch.instance.touchCameras.Count)
      return EasyTouch.instance.touchCameras[index].camera;
    return (Camera) null;
  }

  public static void SetEnable2DCollider(bool value)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.enable2D = value;
  }

  public static bool GetEnable2DCollider()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.enable2D;
    return false;
  }

  public static void Set2DPickableLayer(LayerMask mask)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.pickableLayers2D = mask;
  }

  public static LayerMask Get2DPickableLayer()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.pickableLayers2D;
    return (LayerMask) LayerMask.GetMask("Default");
  }

  public static void SetGesturePriority(EasyTouch.GesturePriority value)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.gesturePriority = value;
  }

  public static EasyTouch.GesturePriority GetGesturePriority()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.gesturePriority;
    return EasyTouch.GesturePriority.Tap;
  }

  public static void SetStationaryTolerance(float tolerance)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.StationaryTolerance = tolerance;
  }

  public static float GetStationaryTolerance()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.StationaryTolerance;
    return -1f;
  }

  public static void SetLongTapTime(float time)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.longTapTime = time;
  }

  public static float GetlongTapTime()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.longTapTime;
    return -1f;
  }

  public static void SetDoubleTapTime(float time)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.doubleTapTime = time;
  }

  public static float GetDoubleTapTime()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.doubleTapTime;
    return -1f;
  }

  public static void SetSwipeTolerance(float tolerance)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.swipeTolerance = tolerance;
  }

  public static float GetSwipeTolerance()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.swipeTolerance;
    return -1f;
  }

  public static void SetEnable2FingersGesture(bool enable)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.enable2FingersGesture = enable;
  }

  public static bool GetEnable2FingersGesture()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.enable2FingersGesture;
    return false;
  }

  public static void SetTwoFingerPickMethod(EasyTouch.TwoFingerPickMethod pickMethod)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.twoFingerPickMethod = pickMethod;
  }

  public static EasyTouch.TwoFingerPickMethod GetTwoFingerPickMethod()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.twoFingerPickMethod;
    return EasyTouch.TwoFingerPickMethod.Finger;
  }

  public static void SetEnablePinch(bool enable)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.enablePinch = enable;
  }

  public static bool GetEnablePinch()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.enablePinch;
    return false;
  }

  public static void SetMinPinchLength(float length)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.minPinchLength = length;
  }

  public static float GetMinPinchLength()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.minPinchLength;
    return -1f;
  }

  public static void SetEnableTwist(bool enable)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.enableTwist = enable;
  }

  public static bool GetEnableTwist()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.enableTwist;
    return false;
  }

  public static void SetMinTwistAngle(float angle)
  {
    if (!(bool) ((UnityEngine.Object) EasyTouch.instance))
      return;
    EasyTouch.instance.minTwistAngle = angle;
  }

  public static float GetMinTwistAngle()
  {
    if ((bool) ((UnityEngine.Object) EasyTouch.instance))
      return EasyTouch.instance.minTwistAngle;
    return -1f;
  }

  public static bool GetSecondeFingerSimulation()
  {
    if ((UnityEngine.Object) EasyTouch.instance != (UnityEngine.Object) null)
      return EasyTouch.instance.enableSimulation;
    return false;
  }

  public static void SetSecondFingerSimulation(bool value)
  {
    if (!((UnityEngine.Object) EasyTouch.instance != (UnityEngine.Object) null))
      return;
    EasyTouch.instance.enableSimulation = value;
  }

  [Serializable]
  private class DoubleTap
  {
    public bool inDoubleTap;
    public bool inWait;
    public float time;
    public int count;
    public Finger finger;

    public void Stop()
    {
      this.inDoubleTap = false;
      this.inWait = false;
      this.time = 0.0f;
      this.count = 0;
    }
  }

  private class PickedObject
  {
    public GameObject pickedObj;
    public Camera pickedCamera;
    public bool isGUI;
  }

  public enum GesturePriority
  {
    Tap,
    Slips,
  }

  public enum DoubleTapDetection
  {
    BySystem,
    ByTime,
  }

  public enum GestureType
  {
    Tap,
    Drag,
    Swipe,
    None,
    LongTap,
    Pinch,
    Twist,
    Cancel,
    Acquisition,
  }

  public enum SwipeDirection
  {
    None,
    Left,
    Right,
    Up,
    Down,
    UpLeft,
    UpRight,
    DownLeft,
    DownRight,
    Other,
  }

  public enum TwoFingerPickMethod
  {
    Finger,
    Average,
  }

  public enum EventName
  {
    None,
    On_TouchStart,
    On_TouchDown,
    On_TouchUp,
    On_SimpleTap,
    On_DoubleTap,
    On_LongTapStart,
    On_LongTap,
    On_LongTapEnd,
    On_DragStart,
    On_Drag,
    On_DragEnd,
    On_SwipeStart,
    On_Swipe,
    On_SwipeEnd,
    On_TouchStart2Fingers,
    On_TouchDown2Fingers,
    On_TouchUp2Fingers,
    On_SimpleTap2Fingers,
    On_DoubleTap2Fingers,
    On_LongTapStart2Fingers,
    On_LongTap2Fingers,
    On_LongTapEnd2Fingers,
    On_Twist,
    On_TwistEnd,
    On_Pinch,
    On_PinchIn,
    On_PinchOut,
    On_PinchEnd,
    On_DragStart2Fingers,
    On_Drag2Fingers,
    On_DragEnd2Fingers,
    On_SwipeStart2Fingers,
    On_Swipe2Fingers,
    On_SwipeEnd2Fingers,
    On_EasyTouchIsReady,
    On_Cancel,
    On_Cancel2Fingers,
    On_OverUIElement,
    On_UIElementTouchUp,
  }

  public delegate void TouchCancelHandler(Gesture gesture);

  public delegate void Cancel2FingersHandler(Gesture gesture);

  public delegate void TouchStartHandler(Gesture gesture);

  public delegate void TouchDownHandler(Gesture gesture);

  public delegate void TouchUpHandler(Gesture gesture);

  public delegate void SimpleTapHandler(Gesture gesture);

  public delegate void DoubleTapHandler(Gesture gesture);

  public delegate void LongTapStartHandler(Gesture gesture);

  public delegate void LongTapHandler(Gesture gesture);

  public delegate void LongTapEndHandler(Gesture gesture);

  public delegate void DragStartHandler(Gesture gesture);

  public delegate void DragHandler(Gesture gesture);

  public delegate void DragEndHandler(Gesture gesture);

  public delegate void SwipeStartHandler(Gesture gesture);

  public delegate void SwipeHandler(Gesture gesture);

  public delegate void SwipeEndHandler(Gesture gesture);

  public delegate void TouchStart2FingersHandler(Gesture gesture);

  public delegate void TouchDown2FingersHandler(Gesture gesture);

  public delegate void TouchUp2FingersHandler(Gesture gesture);

  public delegate void SimpleTap2FingersHandler(Gesture gesture);

  public delegate void DoubleTap2FingersHandler(Gesture gesture);

  public delegate void LongTapStart2FingersHandler(Gesture gesture);

  public delegate void LongTap2FingersHandler(Gesture gesture);

  public delegate void LongTapEnd2FingersHandler(Gesture gesture);

  public delegate void TwistHandler(Gesture gesture);

  public delegate void TwistEndHandler(Gesture gesture);

  public delegate void PinchInHandler(Gesture gesture);

  public delegate void PinchOutHandler(Gesture gesture);

  public delegate void PinchEndHandler(Gesture gesture);

  public delegate void PinchHandler(Gesture gesture);

  public delegate void DragStart2FingersHandler(Gesture gesture);

  public delegate void Drag2FingersHandler(Gesture gesture);

  public delegate void DragEnd2FingersHandler(Gesture gesture);

  public delegate void SwipeStart2FingersHandler(Gesture gesture);

  public delegate void Swipe2FingersHandler(Gesture gesture);

  public delegate void SwipeEnd2FingersHandler(Gesture gesture);

  public delegate void EasyTouchIsReadyHandler();

  public delegate void OverUIElementHandler(Gesture gesture);

  public delegate void UIElementTouchUpHandler(Gesture gesture);
}
