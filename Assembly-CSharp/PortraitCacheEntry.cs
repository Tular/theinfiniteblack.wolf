﻿// Decompiled with JetBrains decompiler
// Type: PortraitCacheEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class PortraitCacheEntry : IComparable<PortraitCacheEntry>
{
  public readonly string playerName;
  public Texture2D texture;
  public float expireTime;

  public PortraitCacheEntry(string player, Texture2D texture)
  {
    this.playerName = player;
    this.texture = texture;
  }

  public void SetTexture(Texture2D texture2D)
  {
    if (!((UnityEngine.Object) this.texture != (UnityEngine.Object) null))
      return;
    Texture2D texture = this.texture;
    this.texture = texture2D;
    UnityEngine.Object.Destroy((UnityEngine.Object) texture);
  }

  public int CompareTo(PortraitCacheEntry other)
  {
    if (other == null)
      return 1;
    return this.expireTime.CompareTo(other.expireTime);
  }
}
