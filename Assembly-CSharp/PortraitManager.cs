﻿// Decompiled with JetBrains decompiler
// Type: PortraitManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;

[AdvancedInspector.AdvancedInspector]
public class PortraitManager : PreInstantiatedSingleton<PortraitManager>
{
  [Inspect(0)]
  public int defaultMaxCacheSize = 5;
  private readonly Dictionary<int, PortraitCache> _portraitCache = new Dictionary<int, PortraitCache>();
  private const string uriKey = "portraitResources";
  [Inspect(1)]
  public float defaultCacheExpireTime;
  [Inspect(10)]
  public PortraitCacheInfo[] cacheDefinitions;
  private string _baseUrl;
  private bool _prevPortraitOption;

  [Inspect(100)]
  private void DumpCacheContents()
  {
    using (Dictionary<int, PortraitCache>.Enumerator enumerator = this._portraitCache.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.DumpContents();
    }
  }

  [DebuggerHidden]
  public IEnumerator LoadPortrait(string playerName, int size, UITexture portrait)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PortraitManager.\u003CLoadPortrait\u003Ec__Iterator2F()
    {
      size = size,
      portrait = portrait,
      playerName = playerName,
      \u003C\u0024\u003Esize = size,
      \u003C\u0024\u003Eportrait = portrait,
      \u003C\u0024\u003EplayerName = playerName,
      \u003C\u003Ef__this = this
    };
  }

  protected override void Awake()
  {
    base.Awake();
    for (int index = 0; index < this.cacheDefinitions.Length; ++index)
    {
      PortraitCache portraitCache = PortraitCache.Build(this.cacheDefinitions[index]);
      this._portraitCache.Add(this.cacheDefinitions[index].imageSize, portraitCache);
    }
  }

  private void Start()
  {
    if (!PreInstantiatedSingleton<WebFileManager>.Instance.definedUriLocations.TryGetValue("portraitResources", out this._baseUrl))
    {
      KeyNotFoundException notFoundException = new KeyNotFoundException("Could not find base portrait URI in WebFileManager");
      this.LogE((Exception) notFoundException);
      throw notFoundException;
    }
    this._prevPortraitOption = TibProxy.accountManager.systemSettings.ShowPortraits;
    TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
    TibProxy.accountManager.onSettingsUpdatedEvent += new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
  }

  private void OnDisable()
  {
    this.Flush();
  }

  private void Flush()
  {
    using (Dictionary<int, PortraitCache>.KeyCollection.Enumerator enumerator = this._portraitCache.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this._portraitCache[enumerator.Current].Flush();
    }
  }

  private void AccountManager_onSettingsUpdatedEvent(object sender, SettingsUpdatedEventEventArgs e)
  {
    if (this._prevPortraitOption == TibProxy.accountManager.systemSettings.ShowPortraits)
      return;
    this.Flush();
  }
}
