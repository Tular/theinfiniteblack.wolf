﻿// Decompiled with JetBrains decompiler
// Type: UIGridAnchor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIAnchor))]
[RequireComponent(typeof (UIGrid))]
[ExecuteInEditMode]
public class UIGridAnchor : MonoBehaviour
{
  public UIGrid grid;
  public UIAnchor anchor;
  public GameObject anchorContainer;
  public Bounds gridBounds;
  public float cellWidth;
  public float cellHeight;
  public bool execute;

  protected void Awake()
  {
    this.grid = this.GetComponent<UIGrid>();
    this.anchor = this.GetComponent<UIAnchor>();
    this.anchorContainer = this.anchor.container;
  }

  protected void Start()
  {
  }

  protected void Update()
  {
    if (!this.execute)
      return;
    this.execute = false;
    this.gridBounds = NGUIMath.CalculateRelativeWidgetBounds(this.grid.transform, false);
    this.cellWidth = this.grid.cellWidth;
    this.cellHeight = this.grid.cellHeight;
    this.anchor.side = UIAnchor.Side.Bottom;
    this.anchor.pixelOffset = new Vector2(-(this.cellWidth / 2f), -(this.cellHeight / 2f));
  }
}
