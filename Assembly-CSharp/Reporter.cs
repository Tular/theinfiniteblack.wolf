﻿// Decompiled with JetBrains decompiler
// Type: Reporter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reporter : MonoBehaviour
{
  private List<Reporter.Sample> samples = new List<Reporter.Sample>(216000);
  private List<Reporter.Log> logs = new List<Reporter.Log>();
  private List<Reporter.Log> collapsedLogs = new List<Reporter.Log>();
  private List<Reporter.Log> currentLog = new List<Reporter.Log>();
  private MultiKeyDictionary<string, string, Reporter.Log> logsDic = new MultiKeyDictionary<string, string, Reporter.Log>();
  private Dictionary<string, string> cachedString = new Dictionary<string, string>();
  private bool showLog = true;
  private bool showWarning = true;
  private bool showError = true;
  private bool showClearOnNewSceneLoadedButton = true;
  private bool showTimeButton = true;
  private bool showSceneButton = true;
  private bool showMemButton = true;
  private bool showFpsButton = true;
  private bool showSearchText = true;
  public string UserData = string.Empty;
  private Reporter.ReportView currentView = Reporter.ReportView.Logs;
  public Vector2 size = new Vector2(32f, 32f);
  public float maxSize = 20f;
  public int numOfCircleToShow = 1;
  private string filterText = string.Empty;
  private GUIContent tempContent = new GUIContent();
  private float graphSize = 4f;
  private List<Vector2> gestureDetector = new List<Vector2>();
  private Vector2 gestureSum = Vector2.zero;
  private float lastClickTime = -1f;
  private bool firstTime = true;
  private List<Reporter.Log> threadedLogs = new List<Reporter.Log>();
  private const int requiredFrames = 10;
  private const float updateInterval = 0.25f;
  [HideInInspector]
  public bool show;
  private bool collapse;
  private bool clearOnNewSceneLoaded;
  private bool showTime;
  private bool showScene;
  private bool showMemory;
  private bool showFps;
  private bool showGraph;
  private int numOfLogs;
  private int numOfLogsWarning;
  private int numOfLogsError;
  private int numOfCollapsedLogs;
  private int numOfCollapsedLogsWarning;
  private int numOfCollapsedLogsError;
  private string buildDate;
  private string logDate;
  private float logsMemUsage;
  private float graphMemUsage;
  private float gcTotalMemory;
  public float fps;
  public string fpsText;
  private static bool created;
  public Images images;
  private GUIContent clearContent;
  private GUIContent collapseContent;
  private GUIContent clearOnNewSceneContent;
  private GUIContent showTimeContent;
  private GUIContent showSceneContent;
  private GUIContent userContent;
  private GUIContent showMemoryContent;
  private GUIContent softwareContent;
  private GUIContent dateContent;
  private GUIContent showFpsContent;
  private GUIContent infoContent;
  private GUIContent searchContent;
  private GUIContent closeContent;
  private GUIContent buildFromContent;
  private GUIContent systemInfoContent;
  private GUIContent graphicsInfoContent;
  private GUIContent backContent;
  private GUIContent logContent;
  private GUIContent warningContent;
  private GUIContent errorContent;
  private GUIStyle barStyle;
  private GUIStyle buttonActiveStyle;
  private GUIStyle nonStyle;
  private GUIStyle lowerLeftFontStyle;
  private GUIStyle backStyle;
  private GUIStyle evenLogStyle;
  private GUIStyle oddLogStyle;
  private GUIStyle logButtonStyle;
  private GUIStyle selectedLogStyle;
  private GUIStyle selectedLogFontStyle;
  private GUIStyle stackLabelStyle;
  private GUIStyle scrollerStyle;
  private GUIStyle searchStyle;
  private GUIStyle sliderBackStyle;
  private GUIStyle sliderThumbStyle;
  private GUISkin toolbarScrollerSkin;
  private GUISkin logScrollerSkin;
  private GUISkin graphScrollerSkin;
  private static string[] scenes;
  private string currentScene;
  private string deviceModel;
  private string deviceType;
  private string deviceName;
  private string graphicsMemorySize;
  private string maxTextureSize;
  private string systemMemorySize;
  public bool Initialized;
  private Rect screenRect;
  private Rect toolBarRect;
  private Rect logsRect;
  private Rect stackRect;
  private Rect graphRect;
  private Rect graphMinRect;
  private Rect graphMaxRect;
  private Rect buttomRect;
  private Vector2 stackRectTopLeft;
  private Rect detailRect;
  private Vector2 scrollPosition;
  private Vector2 scrollPosition2;
  private Vector2 toolbarScrollPosition;
  private Reporter.Log selectedLog;
  private float toolbarOldDrag;
  private float oldDrag;
  private float oldDrag2;
  private float oldDrag3;
  private int startIndex;
  private Rect countRect;
  private Rect timeRect;
  private Rect timeLabelRect;
  private Rect sceneRect;
  private Rect sceneLabelRect;
  private Rect memoryRect;
  private Rect memoryLabelRect;
  private Rect fpsRect;
  private Rect fpsLabelRect;
  private Vector2 infoScrollPosition;
  private Vector2 oldInfoDrag;
  private Rect tempRect;
  private int startFrame;
  private int currentFrame;
  private Vector3 tempVector1;
  private Vector3 tempVector2;
  private Vector2 graphScrollerPos;
  private float maxFpsValue;
  private float minFpsValue;
  private float maxMemoryValue;
  private float minMemoryValue;
  private float gestureLength;
  private int gestureCount;
  private Vector2 startPos;
  private Vector2 downPos;
  private Vector2 mousePosition;
  private int frames;
  private float lastUpdate;

  public float TotalMemUsage
  {
    get
    {
      return this.logsMemUsage + this.graphMemUsage;
    }
  }

  private void Awake()
  {
    if (this.Initialized)
      return;
    this.Initialize();
  }

  private void OnEnable()
  {
    if (this.logs.Count != 0)
      return;
    this.clear();
  }

  private void OnDisable()
  {
  }

  private void addSample()
  {
    this.samples.Add(new Reporter.Sample()
    {
      fps = this.fps,
      fpsText = this.fpsText,
      loadedScene = (byte) SceneManager.GetActiveScene().buildIndex,
      time = Time.realtimeSinceStartup,
      memory = this.gcTotalMemory
    });
    this.graphMemUsage = (float) ((double) this.samples.Count * (double) Reporter.Sample.MemSize() / 1024.0 / 1024.0);
  }

  public void Initialize()
  {
    if (!Reporter.created)
    {
      try
      {
        this.gameObject.SendMessage("OnPreStart");
      }
      catch (Exception ex)
      {
        UnityEngine.Debug.LogException(ex);
      }
      Reporter.scenes = new string[SceneManager.sceneCountInBuildSettings];
      this.currentScene = SceneManager.GetActiveScene().name;
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
      Application.logMessageReceivedThreaded += new Application.LogCallback(this.CaptureLogThread);
      Reporter.created = true;
      this.clearContent = new GUIContent(string.Empty, (Texture) this.images.clearImage, "Clear logs");
      this.collapseContent = new GUIContent(string.Empty, (Texture) this.images.collapseImage, "Collapse logs");
      this.clearOnNewSceneContent = new GUIContent(string.Empty, (Texture) this.images.clearOnNewSceneImage, "Clear logs on new scene loaded");
      this.showTimeContent = new GUIContent(string.Empty, (Texture) this.images.showTimeImage, "Show Hide Time");
      this.showSceneContent = new GUIContent(string.Empty, (Texture) this.images.showSceneImage, "Show Hide Scene");
      this.showMemoryContent = new GUIContent(string.Empty, (Texture) this.images.showMemoryImage, "Show Hide Memory");
      this.softwareContent = new GUIContent(string.Empty, (Texture) this.images.softwareImage, "Software");
      this.dateContent = new GUIContent(string.Empty, (Texture) this.images.dateImage, "Date");
      this.showFpsContent = new GUIContent(string.Empty, (Texture) this.images.showFpsImage, "Show Hide fps");
      this.infoContent = new GUIContent(string.Empty, (Texture) this.images.infoImage, "Information about application");
      this.searchContent = new GUIContent(string.Empty, (Texture) this.images.searchImage, "Search for logs");
      this.closeContent = new GUIContent(string.Empty, (Texture) this.images.closeImage, "Hide logs");
      this.userContent = new GUIContent(string.Empty, (Texture) this.images.userImage, "User");
      this.buildFromContent = new GUIContent(string.Empty, (Texture) this.images.buildFromImage, "Build From");
      this.systemInfoContent = new GUIContent(string.Empty, (Texture) this.images.systemInfoImage, "System Info");
      this.graphicsInfoContent = new GUIContent(string.Empty, (Texture) this.images.graphicsInfoImage, "Graphics Info");
      this.backContent = new GUIContent(string.Empty, (Texture) this.images.backImage, "Back");
      this.logContent = new GUIContent(string.Empty, (Texture) this.images.logImage, "show or hide logs");
      this.warningContent = new GUIContent(string.Empty, (Texture) this.images.warningImage, "show or hide warnings");
      this.errorContent = new GUIContent(string.Empty, (Texture) this.images.errorImage, "show or hide errors");
      this.currentView = (Reporter.ReportView) PlayerPrefs.GetInt("Reporter_currentView", 1);
      this.show = PlayerPrefs.GetInt("Reporter_show") == 1;
      this.collapse = PlayerPrefs.GetInt("Reporter_collapse") == 1;
      this.clearOnNewSceneLoaded = PlayerPrefs.GetInt("Reporter_clearOnNewSceneLoaded") == 1;
      this.showTime = PlayerPrefs.GetInt("Reporter_showTime") == 1;
      this.showScene = PlayerPrefs.GetInt("Reporter_showScene") == 1;
      this.showMemory = PlayerPrefs.GetInt("Reporter_showMemory") == 1;
      this.showFps = PlayerPrefs.GetInt("Reporter_showFps") == 1;
      this.showGraph = PlayerPrefs.GetInt("Reporter_showGraph") == 1;
      this.showLog = PlayerPrefs.GetInt("Reporter_showLog", 1) == 1;
      this.showWarning = PlayerPrefs.GetInt("Reporter_showWarning", 1) == 1;
      this.showError = PlayerPrefs.GetInt("Reporter_showError", 1) == 1;
      this.filterText = PlayerPrefs.GetString("Reporter_filterText");
      this.size.x = this.size.y = PlayerPrefs.GetFloat("Reporter_size", 32f);
      this.showClearOnNewSceneLoadedButton = PlayerPrefs.GetInt("Reporter_showClearOnNewSceneLoadedButton", 1) == 1;
      this.showTimeButton = PlayerPrefs.GetInt("Reporter_showTimeButton", 1) == 1;
      this.showSceneButton = PlayerPrefs.GetInt("Reporter_showSceneButton", 1) == 1;
      this.showMemButton = PlayerPrefs.GetInt("Reporter_showMemButton", 1) == 1;
      this.showFpsButton = PlayerPrefs.GetInt("Reporter_showFpsButton", 1) == 1;
      this.showSearchText = PlayerPrefs.GetInt("Reporter_showSearchText", 1) == 1;
      this.initializeStyle();
      this.Initialized = true;
      if (this.show)
        this.doShow();
      this.deviceModel = SystemInfo.deviceModel.ToString();
      this.deviceType = ((Enum) SystemInfo.deviceType).ToString();
      this.deviceName = SystemInfo.deviceName.ToString();
      this.graphicsMemorySize = SystemInfo.graphicsMemorySize.ToString();
      this.maxTextureSize = SystemInfo.maxTextureSize.ToString();
      this.systemMemorySize = SystemInfo.systemMemorySize.ToString();
    }
    else
    {
      UnityEngine.Debug.LogWarning((object) "tow manager is exists delete the second");
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.gameObject, true);
    }
  }

  private void initializeStyle()
  {
    int num1 = (int) ((double) this.size.x * 0.200000002980232);
    int num2 = (int) ((double) this.size.y * 0.200000002980232);
    this.nonStyle = new GUIStyle();
    this.nonStyle.clipping = TextClipping.Clip;
    this.nonStyle.border = new RectOffset(0, 0, 0, 0);
    this.nonStyle.normal.background = (Texture2D) null;
    this.nonStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.nonStyle.alignment = TextAnchor.MiddleCenter;
    this.lowerLeftFontStyle = new GUIStyle();
    this.lowerLeftFontStyle.clipping = TextClipping.Clip;
    this.lowerLeftFontStyle.border = new RectOffset(0, 0, 0, 0);
    this.lowerLeftFontStyle.normal.background = (Texture2D) null;
    this.lowerLeftFontStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.lowerLeftFontStyle.fontStyle = FontStyle.Bold;
    this.lowerLeftFontStyle.alignment = TextAnchor.LowerLeft;
    this.barStyle = new GUIStyle();
    this.barStyle.border = new RectOffset(1, 1, 1, 1);
    this.barStyle.normal.background = this.images.barImage;
    this.barStyle.active.background = this.images.button_activeImage;
    this.barStyle.alignment = TextAnchor.MiddleCenter;
    this.barStyle.margin = new RectOffset(1, 1, 1, 1);
    this.barStyle.clipping = TextClipping.Clip;
    this.barStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.buttonActiveStyle = new GUIStyle();
    this.buttonActiveStyle.border = new RectOffset(1, 1, 1, 1);
    this.buttonActiveStyle.normal.background = this.images.button_activeImage;
    this.buttonActiveStyle.alignment = TextAnchor.MiddleCenter;
    this.buttonActiveStyle.margin = new RectOffset(1, 1, 1, 1);
    this.buttonActiveStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.backStyle = new GUIStyle();
    this.backStyle.normal.background = this.images.even_logImage;
    this.backStyle.clipping = TextClipping.Clip;
    this.backStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.evenLogStyle = new GUIStyle();
    this.evenLogStyle.normal.background = this.images.even_logImage;
    this.evenLogStyle.fixedHeight = this.size.y;
    this.evenLogStyle.clipping = TextClipping.Clip;
    this.evenLogStyle.alignment = TextAnchor.UpperLeft;
    this.evenLogStyle.imagePosition = ImagePosition.ImageLeft;
    this.evenLogStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.oddLogStyle = new GUIStyle();
    this.oddLogStyle.normal.background = this.images.odd_logImage;
    this.oddLogStyle.fixedHeight = this.size.y;
    this.oddLogStyle.clipping = TextClipping.Clip;
    this.oddLogStyle.alignment = TextAnchor.UpperLeft;
    this.oddLogStyle.imagePosition = ImagePosition.ImageLeft;
    this.oddLogStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.logButtonStyle = new GUIStyle();
    this.logButtonStyle.fixedHeight = this.size.y;
    this.logButtonStyle.clipping = TextClipping.Clip;
    this.logButtonStyle.alignment = TextAnchor.UpperLeft;
    this.logButtonStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.logButtonStyle.padding = new RectOffset(num1, num1, num2, num2);
    this.selectedLogStyle = new GUIStyle();
    this.selectedLogStyle.normal.background = this.images.selectedImage;
    this.selectedLogStyle.fixedHeight = this.size.y;
    this.selectedLogStyle.clipping = TextClipping.Clip;
    this.selectedLogStyle.alignment = TextAnchor.UpperLeft;
    this.selectedLogStyle.normal.textColor = Color.white;
    this.selectedLogStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.selectedLogFontStyle = new GUIStyle();
    this.selectedLogFontStyle.normal.background = this.images.selectedImage;
    this.selectedLogFontStyle.fixedHeight = this.size.y;
    this.selectedLogFontStyle.clipping = TextClipping.Clip;
    this.selectedLogFontStyle.alignment = TextAnchor.UpperLeft;
    this.selectedLogFontStyle.normal.textColor = Color.white;
    this.selectedLogFontStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.selectedLogFontStyle.padding = new RectOffset(num1, num1, num2, num2);
    this.stackLabelStyle = new GUIStyle();
    this.stackLabelStyle.wordWrap = true;
    this.stackLabelStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.stackLabelStyle.padding = new RectOffset(num1, num1, num2, num2);
    this.scrollerStyle = new GUIStyle();
    this.scrollerStyle.normal.background = this.images.barImage;
    this.searchStyle = new GUIStyle();
    this.searchStyle.clipping = TextClipping.Clip;
    this.searchStyle.alignment = TextAnchor.LowerCenter;
    this.searchStyle.fontSize = (int) ((double) this.size.y / 2.0);
    this.searchStyle.wordWrap = true;
    this.sliderBackStyle = new GUIStyle();
    this.sliderBackStyle.normal.background = this.images.barImage;
    this.sliderBackStyle.fixedHeight = this.size.y;
    this.sliderBackStyle.border = new RectOffset(1, 1, 1, 1);
    this.sliderThumbStyle = new GUIStyle();
    this.sliderThumbStyle.normal.background = this.images.selectedImage;
    this.sliderThumbStyle.fixedWidth = this.size.x;
    GUISkin reporterScrollerSkin = this.images.reporterScrollerSkin;
    this.toolbarScrollerSkin = UnityEngine.Object.Instantiate<GUISkin>(reporterScrollerSkin);
    this.toolbarScrollerSkin.verticalScrollbar.fixedWidth = 0.0f;
    this.toolbarScrollerSkin.horizontalScrollbar.fixedHeight = 0.0f;
    this.toolbarScrollerSkin.verticalScrollbarThumb.fixedWidth = 0.0f;
    this.toolbarScrollerSkin.horizontalScrollbarThumb.fixedHeight = 0.0f;
    this.logScrollerSkin = UnityEngine.Object.Instantiate<GUISkin>(reporterScrollerSkin);
    this.logScrollerSkin.verticalScrollbar.fixedWidth = this.size.x * 2f;
    this.logScrollerSkin.horizontalScrollbar.fixedHeight = 0.0f;
    this.logScrollerSkin.verticalScrollbarThumb.fixedWidth = this.size.x * 2f;
    this.logScrollerSkin.horizontalScrollbarThumb.fixedHeight = 0.0f;
    this.graphScrollerSkin = UnityEngine.Object.Instantiate<GUISkin>(reporterScrollerSkin);
    this.graphScrollerSkin.verticalScrollbar.fixedWidth = 0.0f;
    this.graphScrollerSkin.horizontalScrollbar.fixedHeight = this.size.x * 2f;
    this.graphScrollerSkin.verticalScrollbarThumb.fixedWidth = 0.0f;
    this.graphScrollerSkin.horizontalScrollbarThumb.fixedHeight = this.size.x * 2f;
  }

  private void Start()
  {
    this.logDate = DateTime.Now.ToString();
    this.StartCoroutine("readInfo");
  }

  private void clear()
  {
    this.logs.Clear();
    this.collapsedLogs.Clear();
    this.currentLog.Clear();
    this.logsDic.Clear();
    this.selectedLog = (Reporter.Log) null;
    this.numOfLogs = 0;
    this.numOfLogsWarning = 0;
    this.numOfLogsError = 0;
    this.numOfCollapsedLogs = 0;
    this.numOfCollapsedLogsWarning = 0;
    this.numOfCollapsedLogsError = 0;
    this.logsMemUsage = 0.0f;
    this.graphMemUsage = 0.0f;
    this.samples.Clear();
    GC.Collect();
    this.selectedLog = (Reporter.Log) null;
  }

  private void calculateCurrentLog()
  {
    bool flag = !string.IsNullOrEmpty(this.filterText);
    string str = string.Empty;
    if (flag)
      str = this.filterText.ToLower();
    this.currentLog.Clear();
    if (this.collapse)
    {
      for (int index = 0; index < this.collapsedLogs.Count; ++index)
      {
        Reporter.Log collapsedLog = this.collapsedLogs[index];
        if ((collapsedLog.logType != Reporter._LogType.Log || this.showLog) && (collapsedLog.logType != Reporter._LogType.Warning || this.showWarning) && ((collapsedLog.logType != Reporter._LogType.Error || this.showError) && (collapsedLog.logType != Reporter._LogType.Assert || this.showError)) && (collapsedLog.logType != Reporter._LogType.Exception || this.showError))
        {
          if (flag)
          {
            if (collapsedLog.condition.ToLower().Contains(str))
              this.currentLog.Add(collapsedLog);
          }
          else
            this.currentLog.Add(collapsedLog);
        }
      }
    }
    else
    {
      for (int index = 0; index < this.logs.Count; ++index)
      {
        Reporter.Log log = this.logs[index];
        if ((log.logType != Reporter._LogType.Log || this.showLog) && (log.logType != Reporter._LogType.Warning || this.showWarning) && ((log.logType != Reporter._LogType.Error || this.showError) && (log.logType != Reporter._LogType.Assert || this.showError)) && (log.logType != Reporter._LogType.Exception || this.showError))
        {
          if (flag)
          {
            if (log.condition.ToLower().Contains(str))
              this.currentLog.Add(log);
          }
          else
            this.currentLog.Add(log);
        }
      }
    }
    if (this.selectedLog == null)
      return;
    int num1 = this.currentLog.IndexOf(this.selectedLog);
    if (num1 == -1)
    {
      int num2 = this.currentLog.IndexOf(this.logsDic[this.selectedLog.condition][this.selectedLog.stacktrace]);
      if (num2 == -1)
        return;
      this.scrollPosition.y = (float) num2 * this.size.y;
    }
    else
      this.scrollPosition.y = (float) num1 * this.size.y;
  }

  private void DrawInfo()
  {
    GUILayout.BeginArea(this.screenRect, this.backStyle);
    Vector2 drag = this.getDrag();
    if ((double) drag.x != 0.0 && this.downPos != Vector2.zero)
      this.infoScrollPosition.x -= drag.x - this.oldInfoDrag.x;
    if ((double) drag.y != 0.0 && this.downPos != Vector2.zero)
      this.infoScrollPosition.y += drag.y - this.oldInfoDrag.y;
    this.oldInfoDrag = drag;
    GUI.skin = this.toolbarScrollerSkin;
    this.infoScrollPosition = GUILayout.BeginScrollView(this.infoScrollPosition);
    GUILayout.Space(this.size.x);
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.buildFromContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.buildDate, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.systemInfoContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.deviceModel, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.deviceType, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.deviceName, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.graphicsInfoContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(SystemInfo.graphicsDeviceName, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.graphicsMemorySize, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.maxTextureSize, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.graphicsInfoContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(((Enum) SystemInfo.graphicsDeviceType).ToString(), this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    switch (SystemInfo.graphicsShaderLevel)
    {
      case 40:
        GUILayout.Label("Shade Model 4.0", this.nonStyle, new GUILayoutOption[1]
        {
          GUILayout.Height(this.size.y)
        });
        break;
      case 41:
        GUILayout.Label("Shade Model 4.1", this.nonStyle, new GUILayoutOption[1]
        {
          GUILayout.Height(this.size.y)
        });
        break;
      case 20:
        GUILayout.Label("Shade Model 2.x", this.nonStyle, new GUILayoutOption[1]
        {
          GUILayout.Height(this.size.y)
        });
        break;
      case 30:
        GUILayout.Label("Shade Model 3.0", this.nonStyle, new GUILayoutOption[1]
        {
          GUILayout.Height(this.size.y)
        });
        break;
      case 50:
        GUILayout.Label("Shade Model 5.0", this.nonStyle, new GUILayoutOption[1]
        {
          GUILayout.Height(this.size.y)
        });
        break;
      default:
        GUILayout.Label("Cannot determine Shade Model", this.nonStyle, new GUILayoutOption[1]
        {
          GUILayout.Height(this.size.y)
        });
        break;
    }
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Space(this.size.x);
    GUILayout.Space(this.size.x);
    GUILayout.Label("Screen Width " + (object) Screen.width, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    GUILayout.Label("Screen Height " + (object) Screen.height, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.showMemoryContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.systemMemorySize + " mb", this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Space(this.size.x);
    GUILayout.Space(this.size.x);
    GUILayout.Label("Mem Usage Of Logs " + this.logsMemUsage.ToString("0.000") + " mb", this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    GUILayout.Label("GC Memory " + this.gcTotalMemory.ToString("0.000") + " mb", this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.softwareContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(SystemInfo.operatingSystem, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.dateContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(DateTime.Now.ToString(), this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Label(" - Application Started At " + this.logDate, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.showTimeContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(Time.realtimeSinceStartup.ToString("000"), this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.showFpsContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.fpsText, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.userContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.UserData, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.showSceneContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label(this.currentScene, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Box(this.showSceneContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Space(this.size.x);
    GUILayout.Label("Unity Version = " + Application.unityVersion, this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    this.drawInfo_enableDisableToolBarButtons();
    GUILayout.FlexibleSpace();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Label("Size = " + this.size.x.ToString("0.0"), this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    float num = GUILayout.HorizontalSlider(this.size.x, 16f, 64f, this.sliderBackStyle, this.sliderThumbStyle, GUILayout.Width((float) Screen.width * 0.5f));
    if ((double) this.size.x != (double) num)
    {
      this.size.x = this.size.y = num;
      this.initializeStyle();
    }
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    if (GUILayout.Button(this.backContent, this.barStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.currentView = Reporter.ReportView.Logs;
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.EndScrollView();
    GUILayout.EndArea();
  }

  private void drawInfo_enableDisableToolBarButtons()
  {
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    GUILayout.Label("Hide or Show tool bar buttons", this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.Space(this.size.x);
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Space(this.size.x);
    if (GUILayout.Button(this.clearOnNewSceneContent, !this.showClearOnNewSceneLoadedButton ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.showClearOnNewSceneLoadedButton = !this.showClearOnNewSceneLoadedButton;
    if (GUILayout.Button(this.showTimeContent, !this.showTimeButton ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.showTimeButton = !this.showTimeButton;
    this.tempRect = GUILayoutUtility.GetLastRect();
    GUI.Label(this.tempRect, Time.realtimeSinceStartup.ToString("0.0"), this.lowerLeftFontStyle);
    if (GUILayout.Button(this.showSceneContent, !this.showSceneButton ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.showSceneButton = !this.showSceneButton;
    this.tempRect = GUILayoutUtility.GetLastRect();
    GUI.Label(this.tempRect, this.currentScene, this.lowerLeftFontStyle);
    if (GUILayout.Button(this.showMemoryContent, !this.showMemButton ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.showMemButton = !this.showMemButton;
    this.tempRect = GUILayoutUtility.GetLastRect();
    GUI.Label(this.tempRect, this.gcTotalMemory.ToString("0.0"), this.lowerLeftFontStyle);
    if (GUILayout.Button(this.showFpsContent, !this.showFpsButton ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.showFpsButton = !this.showFpsButton;
    this.tempRect = GUILayoutUtility.GetLastRect();
    GUI.Label(this.tempRect, this.fpsText, this.lowerLeftFontStyle);
    if (GUILayout.Button(this.searchContent, !this.showSearchText ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.showSearchText = !this.showSearchText;
    this.tempRect = GUILayoutUtility.GetLastRect();
    GUI.TextField(this.tempRect, this.filterText, this.searchStyle);
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
  }

  private void DrawReport()
  {
    this.screenRect.x = 0.0f;
    this.screenRect.y = 0.0f;
    this.screenRect.width = (float) Screen.width;
    this.screenRect.height = (float) Screen.height;
    GUILayout.BeginArea(this.screenRect, this.backStyle);
    GUILayout.BeginVertical();
    GUILayout.FlexibleSpace();
    GUILayout.BeginHorizontal();
    GUILayout.FlexibleSpace();
    GUILayout.Label("Select Photo", this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label("Coming Soon", this.nonStyle, new GUILayoutOption[1]
    {
      GUILayout.Height(this.size.y)
    });
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.FlexibleSpace();
    if (GUILayout.Button(this.backContent, this.barStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y)))
      this.currentView = Reporter.ReportView.Logs;
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.FlexibleSpace();
    GUILayout.EndVertical();
    GUILayout.EndArea();
  }

  private void drawToolBar()
  {
    this.toolBarRect.x = 0.0f;
    this.toolBarRect.y = 0.0f;
    this.toolBarRect.width = (float) Screen.width;
    this.toolBarRect.height = this.size.y * 2f;
    GUI.skin = this.toolbarScrollerSkin;
    Vector2 drag = this.getDrag();
    if ((double) drag.x != 0.0 && this.downPos != Vector2.zero && (double) this.downPos.y > (double) Screen.height - (double) this.size.y * 2.0)
      this.toolbarScrollPosition.x -= drag.x - this.toolbarOldDrag;
    this.toolbarOldDrag = drag.x;
    GUILayout.BeginArea(this.toolBarRect);
    this.toolbarScrollPosition = GUILayout.BeginScrollView(this.toolbarScrollPosition);
    GUILayout.BeginHorizontal(this.barStyle, new GUILayoutOption[0]);
    if (GUILayout.Button(this.clearContent, this.barStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.clear();
    if (GUILayout.Button(this.collapseContent, !this.collapse ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.collapse = !this.collapse;
      this.calculateCurrentLog();
    }
    if (this.showClearOnNewSceneLoadedButton)
    {
      if (GUILayout.Button(this.clearOnNewSceneContent, !this.clearOnNewSceneLoaded ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
        this.clearOnNewSceneLoaded = !this.clearOnNewSceneLoaded;
    }
    if (this.showTimeButton)
    {
      if (GUILayout.Button(this.showTimeContent, !this.showTime ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
        this.showTime = !this.showTime;
    }
    if (this.showSceneButton)
    {
      this.tempRect = GUILayoutUtility.GetLastRect();
      GUI.Label(this.tempRect, Time.realtimeSinceStartup.ToString("0.0"), this.lowerLeftFontStyle);
      if (GUILayout.Button(this.showSceneContent, !this.showScene ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
        this.showScene = !this.showScene;
      this.tempRect = GUILayoutUtility.GetLastRect();
      GUI.Label(this.tempRect, this.currentScene, this.lowerLeftFontStyle);
    }
    if (this.showMemButton)
    {
      if (GUILayout.Button(this.showMemoryContent, !this.showMemory ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
        this.showMemory = !this.showMemory;
      this.tempRect = GUILayoutUtility.GetLastRect();
      GUI.Label(this.tempRect, this.gcTotalMemory.ToString("0.0"), this.lowerLeftFontStyle);
    }
    if (this.showFpsButton)
    {
      if (GUILayout.Button(this.showFpsContent, !this.showFps ? this.barStyle : this.buttonActiveStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
        this.showFps = !this.showFps;
      this.tempRect = GUILayoutUtility.GetLastRect();
      GUI.Label(this.tempRect, this.fpsText, this.lowerLeftFontStyle);
    }
    if (this.showSearchText)
    {
      GUILayout.Box(this.searchContent, this.barStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f));
      this.tempRect = GUILayoutUtility.GetLastRect();
      string str = GUI.TextField(this.tempRect, this.filterText, this.searchStyle);
      if (str != this.filterText)
      {
        this.filterText = str;
        this.calculateCurrentLog();
      }
    }
    if (GUILayout.Button(this.infoContent, this.barStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
      this.currentView = Reporter.ReportView.Info;
    GUILayout.FlexibleSpace();
    string str1 = " ";
    string text1 = !this.collapse ? str1 + (object) this.numOfLogs : str1 + (object) this.numOfCollapsedLogs;
    string str2 = " ";
    string text2 = !this.collapse ? str2 + (object) this.numOfLogsWarning : str2 + (object) this.numOfCollapsedLogsWarning;
    string str3 = " ";
    string text3 = !this.collapse ? str3 + (object) this.numOfLogsError : str3 + (object) this.numOfCollapsedLogsError;
    GUILayout.BeginHorizontal(!this.showLog ? this.barStyle : this.buttonActiveStyle, new GUILayoutOption[0]);
    if (GUILayout.Button(this.logContent, this.nonStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.showLog = !this.showLog;
      this.calculateCurrentLog();
    }
    if (GUILayout.Button(text1, this.nonStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.showLog = !this.showLog;
      this.calculateCurrentLog();
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal(!this.showWarning ? this.barStyle : this.buttonActiveStyle, new GUILayoutOption[0]);
    if (GUILayout.Button(this.warningContent, this.nonStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.showWarning = !this.showWarning;
      this.calculateCurrentLog();
    }
    if (GUILayout.Button(text2, this.nonStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.showWarning = !this.showWarning;
      this.calculateCurrentLog();
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal(!this.showError ? this.nonStyle : this.buttonActiveStyle, new GUILayoutOption[0]);
    if (GUILayout.Button(this.errorContent, this.nonStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.showError = !this.showError;
      this.calculateCurrentLog();
    }
    if (GUILayout.Button(text3, this.nonStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.showError = !this.showError;
      this.calculateCurrentLog();
    }
    GUILayout.EndHorizontal();
    if (GUILayout.Button(this.closeContent, this.barStyle, GUILayout.Width(this.size.x * 2f), GUILayout.Height(this.size.y * 2f)))
    {
      this.show = false;
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.gameObject.GetComponent<ReporterGUI>());
      try
      {
        this.gameObject.SendMessage("OnHideReporter");
      }
      catch (Exception ex)
      {
        UnityEngine.Debug.LogException(ex);
      }
    }
    GUILayout.EndHorizontal();
    GUILayout.EndScrollView();
    GUILayout.EndArea();
  }

  private void DrawLogs()
  {
    GUILayout.BeginArea(this.logsRect, this.backStyle);
    GUI.skin = this.logScrollerSkin;
    Vector2 drag = this.getDrag();
    if ((double) drag.y != 0.0 && this.logsRect.Contains(new Vector2(this.downPos.x, (float) Screen.height - this.downPos.y)))
      this.scrollPosition.y += drag.y - this.oldDrag;
    this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition);
    this.oldDrag = drag.y;
    int a = (int) ((double) Screen.height * 0.75 / (double) this.size.y);
    int count = this.currentLog.Count;
    int num1 = Mathf.Min(a, count - this.startIndex);
    int num2 = 0;
    int num3 = (int) ((double) this.startIndex * (double) this.size.y);
    if (num3 > 0)
    {
      GUILayout.BeginHorizontal(GUILayout.Height((float) num3));
      GUILayout.Label("---");
      GUILayout.EndHorizontal();
    }
    int num4 = Mathf.Clamp(this.startIndex + num1, 0, count);
    bool flag = num1 < count;
    for (int startIndex = this.startIndex; this.startIndex + num2 < num4 && startIndex < this.currentLog.Count; ++startIndex)
    {
      Reporter.Log log = this.currentLog[startIndex];
      if ((log.logType != Reporter._LogType.Log || this.showLog) && (log.logType != Reporter._LogType.Warning || this.showWarning) && ((log.logType != Reporter._LogType.Error || this.showError) && (log.logType != Reporter._LogType.Assert || this.showError)) && (log.logType != Reporter._LogType.Exception || this.showError))
      {
        if (num2 < num1)
        {
          GUIContent content = log.logType != Reporter._LogType.Log ? (log.logType != Reporter._LogType.Warning ? this.errorContent : this.warningContent) : this.logContent;
          GUIStyle style = (this.startIndex + num2) % 2 != 0 ? this.oddLogStyle : this.evenLogStyle;
          if (log == this.selectedLog)
            style = this.selectedLogStyle;
          this.tempContent.text = log.count.ToString();
          float num5 = 0.0f;
          if (this.collapse)
            num5 = this.barStyle.CalcSize(this.tempContent).x + 3f;
          this.countRect.x = (float) Screen.width - num5;
          this.countRect.y = this.size.y * (float) startIndex;
          if (num3 > 0)
            this.countRect.y += 8f;
          this.countRect.width = num5;
          this.countRect.height = this.size.y;
          if (flag)
            this.countRect.x -= this.size.x * 2f;
          Reporter.Sample sample = this.samples[log.sampleId];
          this.fpsRect = this.countRect;
          if (this.showFps)
          {
            this.tempContent.text = sample.fpsText;
            float num6 = style.CalcSize(this.tempContent).x + this.size.x;
            this.fpsRect.x -= num6;
            this.fpsRect.width = this.size.x;
            this.fpsLabelRect = this.fpsRect;
            this.fpsLabelRect.x += this.size.x;
            this.fpsLabelRect.width = num6 - this.size.x;
          }
          this.memoryRect = this.fpsRect;
          if (this.showMemory)
          {
            this.tempContent.text = sample.memory.ToString("0.000");
            float num6 = style.CalcSize(this.tempContent).x + this.size.x;
            this.memoryRect.x -= num6;
            this.memoryRect.width = this.size.x;
            this.memoryLabelRect = this.memoryRect;
            this.memoryLabelRect.x += this.size.x;
            this.memoryLabelRect.width = num6 - this.size.x;
          }
          this.sceneRect = this.memoryRect;
          if (this.showScene)
          {
            this.tempContent.text = sample.GetSceneName();
            float num6 = style.CalcSize(this.tempContent).x + this.size.x;
            this.sceneRect.x -= num6;
            this.sceneRect.width = this.size.x;
            this.sceneLabelRect = this.sceneRect;
            this.sceneLabelRect.x += this.size.x;
            this.sceneLabelRect.width = num6 - this.size.x;
          }
          this.timeRect = this.sceneRect;
          if (this.showTime)
          {
            this.tempContent.text = sample.time.ToString("0.000");
            float num6 = style.CalcSize(this.tempContent).x + this.size.x;
            this.timeRect.x -= num6;
            this.timeRect.width = this.size.x;
            this.timeLabelRect = this.timeRect;
            this.timeLabelRect.x += this.size.x;
            this.timeLabelRect.width = num6 - this.size.x;
          }
          GUILayout.BeginHorizontal(style, new GUILayoutOption[0]);
          if (log == this.selectedLog)
          {
            GUILayout.Box(content, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
            GUILayout.Label(log.condition, this.selectedLogFontStyle, new GUILayoutOption[0]);
            if (this.showTime)
            {
              GUI.Box(this.timeRect, this.showTimeContent, style);
              GUI.Label(this.timeLabelRect, sample.time.ToString("0.000"), style);
            }
            if (this.showScene)
            {
              GUI.Box(this.sceneRect, this.showSceneContent, style);
              GUI.Label(this.sceneLabelRect, sample.GetSceneName(), style);
            }
            if (this.showMemory)
            {
              GUI.Box(this.memoryRect, this.showMemoryContent, style);
              GUI.Label(this.memoryLabelRect, sample.memory.ToString("0.000") + " mb", style);
            }
            if (this.showFps)
            {
              GUI.Box(this.fpsRect, this.showFpsContent, style);
              GUI.Label(this.fpsLabelRect, sample.fpsText, style);
            }
          }
          else
          {
            if (GUILayout.Button(content, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y)))
              this.selectedLog = log;
            if (GUILayout.Button(log.condition, this.logButtonStyle, new GUILayoutOption[0]))
              this.selectedLog = log;
            if (this.showTime)
            {
              GUI.Box(this.timeRect, this.showTimeContent, style);
              GUI.Label(this.timeLabelRect, sample.time.ToString("0.000"), style);
            }
            if (this.showScene)
            {
              GUI.Box(this.sceneRect, this.showSceneContent, style);
              GUI.Label(this.sceneLabelRect, sample.GetSceneName(), style);
            }
            if (this.showMemory)
            {
              GUI.Box(this.memoryRect, this.showMemoryContent, style);
              GUI.Label(this.memoryLabelRect, sample.memory.ToString("0.000") + " mb", style);
            }
            if (this.showFps)
            {
              GUI.Box(this.fpsRect, this.showFpsContent, style);
              GUI.Label(this.fpsLabelRect, sample.fpsText, style);
            }
          }
          if (this.collapse)
            GUI.Label(this.countRect, log.count.ToString(), this.barStyle);
          GUILayout.EndHorizontal();
          ++num2;
        }
        else
          break;
      }
    }
    int num7 = (int) ((double) (count - (this.startIndex + num1)) * (double) this.size.y);
    if (num7 > 0)
    {
      GUILayout.BeginHorizontal(GUILayout.Height((float) num7));
      GUILayout.Label(" ");
      GUILayout.EndHorizontal();
    }
    GUILayout.EndScrollView();
    GUILayout.EndArea();
    this.buttomRect.x = 0.0f;
    this.buttomRect.y = (float) Screen.height - this.size.y;
    this.buttomRect.width = (float) Screen.width;
    this.buttomRect.height = this.size.y;
    if (this.showGraph)
      this.drawGraph();
    else
      this.drawStack();
  }

  private void drawGraph()
  {
    this.graphRect = this.stackRect;
    this.graphRect.height = (float) Screen.height * 0.25f;
    GUI.skin = this.graphScrollerSkin;
    Vector2 drag = this.getDrag();
    if (this.graphRect.Contains(new Vector2(this.downPos.x, (float) Screen.height - this.downPos.y)))
    {
      if ((double) drag.x != 0.0)
      {
        this.graphScrollerPos.x -= drag.x - this.oldDrag3;
        this.graphScrollerPos.x = Mathf.Max(0.0f, this.graphScrollerPos.x);
      }
      Vector2 downPos = this.downPos;
      if (downPos != Vector2.zero)
        this.currentFrame = this.startFrame + (int) ((double) downPos.x / (double) this.graphSize);
    }
    this.oldDrag3 = drag.x;
    GUILayout.BeginArea(this.graphRect, this.backStyle);
    this.graphScrollerPos = GUILayout.BeginScrollView(this.graphScrollerPos);
    this.startFrame = (int) ((double) this.graphScrollerPos.x / (double) this.graphSize);
    if ((double) this.graphScrollerPos.x >= (double) this.samples.Count * (double) this.graphSize - (double) Screen.width)
      this.graphScrollerPos.x += this.graphSize;
    GUILayout.Label(" ", new GUILayoutOption[1]
    {
      GUILayout.Width((float) this.samples.Count * this.graphSize)
    });
    GUILayout.EndScrollView();
    GUILayout.EndArea();
    this.maxFpsValue = 0.0f;
    this.minFpsValue = 100000f;
    this.maxMemoryValue = 0.0f;
    this.minMemoryValue = 100000f;
    for (int index1 = 0; (double) index1 < (double) Screen.width / (double) this.graphSize; ++index1)
    {
      int index2 = this.startFrame + index1;
      if (index2 < this.samples.Count)
      {
        Reporter.Sample sample = this.samples[index2];
        if ((double) this.maxFpsValue < (double) sample.fps)
          this.maxFpsValue = sample.fps;
        if ((double) this.minFpsValue > (double) sample.fps)
          this.minFpsValue = sample.fps;
        if ((double) this.maxMemoryValue < (double) sample.memory)
          this.maxMemoryValue = sample.memory;
        if ((double) this.minMemoryValue > (double) sample.memory)
          this.minMemoryValue = sample.memory;
      }
      else
        break;
    }
    if (this.currentFrame != -1 && this.currentFrame < this.samples.Count)
    {
      Reporter.Sample sample = this.samples[this.currentFrame];
      GUILayout.BeginArea(this.buttomRect, this.backStyle);
      GUILayout.BeginHorizontal();
      GUILayout.Box(this.showTimeContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.time.ToString("0.0"), this.nonStyle, new GUILayoutOption[0]);
      GUILayout.Space(this.size.x);
      GUILayout.Box(this.showSceneContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.GetSceneName(), this.nonStyle, new GUILayoutOption[0]);
      GUILayout.Space(this.size.x);
      GUILayout.Box(this.showMemoryContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.memory.ToString("0.000"), this.nonStyle, new GUILayoutOption[0]);
      GUILayout.Space(this.size.x);
      GUILayout.Box(this.showFpsContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.fpsText, this.nonStyle, new GUILayoutOption[0]);
      GUILayout.Space(this.size.x);
      GUILayout.FlexibleSpace();
      GUILayout.EndHorizontal();
      GUILayout.EndArea();
    }
    this.graphMaxRect = this.stackRect;
    this.graphMaxRect.height = this.size.y;
    GUILayout.BeginArea(this.graphMaxRect);
    GUILayout.BeginHorizontal();
    GUILayout.Box(this.showMemoryContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Label(this.maxMemoryValue.ToString("0.000"), this.nonStyle, new GUILayoutOption[0]);
    GUILayout.Box(this.showFpsContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Label(this.maxFpsValue.ToString("0.000"), this.nonStyle, new GUILayoutOption[0]);
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.EndArea();
    this.graphMinRect = this.stackRect;
    this.graphMinRect.y = this.stackRect.y + this.stackRect.height - this.size.y;
    this.graphMinRect.height = this.size.y;
    GUILayout.BeginArea(this.graphMinRect);
    GUILayout.BeginHorizontal();
    GUILayout.Box(this.showMemoryContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Label(this.minMemoryValue.ToString("0.000"), this.nonStyle, new GUILayoutOption[0]);
    GUILayout.Box(this.showFpsContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
    GUILayout.Label(this.minFpsValue.ToString("0.000"), this.nonStyle, new GUILayoutOption[0]);
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.EndArea();
  }

  private void drawStack()
  {
    if (this.selectedLog != null)
    {
      Vector2 drag = this.getDrag();
      if ((double) drag.y != 0.0 && this.stackRect.Contains(new Vector2(this.downPos.x, (float) Screen.height - this.downPos.y)))
        this.scrollPosition2.y += drag.y - this.oldDrag2;
      this.oldDrag2 = drag.y;
      GUILayout.BeginArea(this.stackRect, this.backStyle);
      this.scrollPosition2 = GUILayout.BeginScrollView(this.scrollPosition2);
      Reporter.Sample sample = (Reporter.Sample) null;
      try
      {
        sample = this.samples[this.selectedLog.sampleId];
      }
      catch (Exception ex)
      {
        UnityEngine.Debug.LogException(ex);
      }
      GUILayout.BeginHorizontal();
      GUILayout.Label(this.selectedLog.condition, this.stackLabelStyle, new GUILayoutOption[0]);
      GUILayout.EndHorizontal();
      GUILayout.Space(this.size.y * 0.25f);
      GUILayout.BeginHorizontal();
      GUILayout.Label(this.selectedLog.stacktrace, this.stackLabelStyle, new GUILayoutOption[0]);
      GUILayout.EndHorizontal();
      GUILayout.Space(this.size.y);
      GUILayout.EndScrollView();
      GUILayout.EndArea();
      GUILayout.BeginArea(this.buttomRect, this.backStyle);
      GUILayout.BeginHorizontal();
      GUILayout.Box(this.showTimeContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.time.ToString("0.000"), this.nonStyle, new GUILayoutOption[0]);
      GUILayout.Space(this.size.x);
      GUILayout.Box(this.showSceneContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.GetSceneName(), this.nonStyle, new GUILayoutOption[0]);
      GUILayout.Space(this.size.x);
      GUILayout.Box(this.showMemoryContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.memory.ToString("0.000"), this.nonStyle, new GUILayoutOption[0]);
      GUILayout.Space(this.size.x);
      GUILayout.Box(this.showFpsContent, this.nonStyle, GUILayout.Width(this.size.x), GUILayout.Height(this.size.y));
      GUILayout.Label(sample.fpsText, this.nonStyle, new GUILayoutOption[0]);
      GUILayout.FlexibleSpace();
      GUILayout.EndHorizontal();
      GUILayout.EndArea();
    }
    else
    {
      GUILayout.BeginArea(this.stackRect, this.backStyle);
      GUILayout.EndArea();
      GUILayout.BeginArea(this.buttomRect, this.backStyle);
      GUILayout.EndArea();
    }
  }

  public void OnGUIDraw()
  {
    if (!this.show)
      return;
    this.screenRect.x = 0.0f;
    this.screenRect.y = 0.0f;
    this.screenRect.width = (float) Screen.width;
    this.screenRect.height = (float) Screen.height;
    this.getDownPos();
    this.logsRect.x = 0.0f;
    this.logsRect.y = this.size.y * 2f;
    this.logsRect.width = (float) Screen.width;
    this.logsRect.height = (float) ((double) Screen.height * 0.75 - (double) this.size.y * 2.0);
    this.stackRectTopLeft.x = 0.0f;
    this.stackRect.x = 0.0f;
    this.stackRectTopLeft.y = (float) Screen.height * 0.75f;
    this.stackRect.y = (float) Screen.height * 0.75f;
    this.stackRect.width = (float) Screen.width;
    this.stackRect.height = (float) Screen.height * 0.25f - this.size.y;
    this.detailRect.x = 0.0f;
    this.detailRect.y = (float) Screen.height - this.size.y * 3f;
    this.detailRect.width = (float) Screen.width;
    this.detailRect.height = this.size.y * 3f;
    if (this.currentView == Reporter.ReportView.Info)
    {
      this.DrawInfo();
    }
    else
    {
      if (this.currentView != Reporter.ReportView.Logs)
        return;
      this.drawToolBar();
      this.DrawLogs();
    }
  }

  private bool isGestureDone()
  {
    if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
    {
      if (Input.touches.Length != 1)
      {
        this.gestureDetector.Clear();
        this.gestureCount = 0;
      }
      else if (Input.touches[0].phase == TouchPhase.Canceled || Input.touches[0].phase == TouchPhase.Ended)
        this.gestureDetector.Clear();
      else if (Input.touches[0].phase == TouchPhase.Moved)
      {
        Vector2 position = Input.touches[0].position;
        if (this.gestureDetector.Count == 0 || (double) (position - this.gestureDetector[this.gestureDetector.Count - 1]).magnitude > 10.0)
          this.gestureDetector.Add(position);
      }
    }
    else if (Input.GetMouseButtonUp(0))
    {
      this.gestureDetector.Clear();
      this.gestureCount = 0;
    }
    else if (Input.GetMouseButton(0))
    {
      Vector2 vector2 = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
      if (this.gestureDetector.Count == 0 || (double) (vector2 - this.gestureDetector[this.gestureDetector.Count - 1]).magnitude > 10.0)
        this.gestureDetector.Add(vector2);
    }
    if (this.gestureDetector.Count < 10)
      return false;
    this.gestureSum = Vector2.zero;
    this.gestureLength = 0.0f;
    Vector2 rhs = Vector2.zero;
    for (int index = 0; index < this.gestureDetector.Count - 2; ++index)
    {
      Vector2 lhs = this.gestureDetector[index + 1] - this.gestureDetector[index];
      float magnitude = lhs.magnitude;
      this.gestureSum += lhs;
      this.gestureLength += magnitude;
      if ((double) Vector2.Dot(lhs, rhs) < 0.0)
      {
        this.gestureDetector.Clear();
        this.gestureCount = 0;
        return false;
      }
      rhs = lhs;
    }
    int num = (Screen.width + Screen.height) / 4;
    if ((double) this.gestureLength > (double) num && (double) this.gestureSum.magnitude < (double) (num / 2))
    {
      this.gestureDetector.Clear();
      ++this.gestureCount;
      if (this.gestureCount >= this.numOfCircleToShow)
        return true;
    }
    return false;
  }

  private bool isDoubleClickDone()
  {
    if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
    {
      if (Input.touches.Length != 1)
        this.lastClickTime = -1f;
      else if (Input.touches[0].phase == TouchPhase.Began)
      {
        if ((double) this.lastClickTime == -1.0)
        {
          this.lastClickTime = Time.realtimeSinceStartup;
        }
        else
        {
          if ((double) Time.realtimeSinceStartup - (double) this.lastClickTime < 0.200000002980232)
          {
            this.lastClickTime = -1f;
            return true;
          }
          this.lastClickTime = Time.realtimeSinceStartup;
        }
      }
    }
    else if (Input.GetMouseButtonDown(0))
    {
      if ((double) this.lastClickTime == -1.0)
      {
        this.lastClickTime = Time.realtimeSinceStartup;
      }
      else
      {
        if ((double) Time.realtimeSinceStartup - (double) this.lastClickTime < 0.200000002980232)
        {
          this.lastClickTime = -1f;
          return true;
        }
        this.lastClickTime = Time.realtimeSinceStartup;
      }
    }
    return false;
  }

  private Vector2 getDownPos()
  {
    if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
    {
      if (Input.touches.Length == 1 && Input.touches[0].phase == TouchPhase.Began)
      {
        this.downPos = Input.touches[0].position;
        return this.downPos;
      }
    }
    else if (Input.GetMouseButtonDown(0))
    {
      this.downPos.x = Input.mousePosition.x;
      this.downPos.y = Input.mousePosition.y;
      return this.downPos;
    }
    return Vector2.zero;
  }

  private Vector2 getDrag()
  {
    if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
    {
      if (Input.touches.Length != 1)
        return Vector2.zero;
      return Input.touches[0].position - this.downPos;
    }
    if (!Input.GetMouseButton(0))
      return Vector2.zero;
    this.mousePosition = (Vector2) Input.mousePosition;
    return this.mousePosition - this.downPos;
  }

  private void calculateStartIndex()
  {
    this.startIndex = (int) ((double) this.scrollPosition.y / (double) this.size.y);
    this.startIndex = Mathf.Clamp(this.startIndex, 0, this.currentLog.Count);
  }

  private void doShow()
  {
    if (SceneManager.GetActiveScene().buildIndex < 2)
      return;
    this.show = true;
    this.currentView = Reporter.ReportView.Logs;
    this.gameObject.AddComponent<ReporterGUI>();
    try
    {
      this.gameObject.SendMessage("OnShowReporter");
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogException(ex);
    }
  }

  private void Update()
  {
    this.fpsText = this.fps.ToString("0.000");
    this.gcTotalMemory = (float) ((double) GC.GetTotalMemory(false) / 1024.0 / 1024.0);
    int buildIndex = SceneManager.GetActiveScene().buildIndex;
    if (buildIndex != -1 && string.IsNullOrEmpty(Reporter.scenes[buildIndex]))
      Reporter.scenes[SceneManager.GetActiveScene().buildIndex] = SceneManager.GetActiveScene().name;
    this.calculateStartIndex();
    if (!this.show && this.isGestureDone())
      this.doShow();
    if (this.threadedLogs.Count > 0)
    {
      lock (this.threadedLogs)
      {
        for (int index = 0; index < this.threadedLogs.Count; ++index)
        {
          Reporter.Log threadedLog = this.threadedLogs[index];
          this.AddLog(threadedLog.condition, threadedLog.stacktrace, (LogType) threadedLog.logType);
        }
        this.threadedLogs.Clear();
      }
    }
    if (this.firstTime)
    {
      this.firstTime = false;
      this.lastUpdate = Time.realtimeSinceStartup;
      this.frames = 0;
    }
    else
    {
      ++this.frames;
      float num = Time.realtimeSinceStartup - this.lastUpdate;
      if ((double) num <= 0.25 || this.frames <= 10)
        return;
      this.fps = (float) this.frames / num;
      this.lastUpdate = Time.realtimeSinceStartup;
      this.frames = 0;
    }
  }

  private void CaptureLog(string condition, string stacktrace, LogType type)
  {
    this.AddLog(condition, stacktrace, type);
  }

  private void AddLog(string condition, string stacktrace, LogType type)
  {
    float num = 0.0f;
    string empty1 = string.Empty;
    string index;
    if (this.cachedString.ContainsKey(condition))
    {
      index = this.cachedString[condition];
    }
    else
    {
      index = condition;
      this.cachedString.Add(index, index);
      num = num + (!string.IsNullOrEmpty(index) ? (float) (index.Length * 2) : 0.0f) + (float) IntPtr.Size;
    }
    string empty2 = string.Empty;
    string key;
    if (this.cachedString.ContainsKey(stacktrace))
    {
      key = this.cachedString[stacktrace];
    }
    else
    {
      key = stacktrace;
      this.cachedString.Add(key, key);
      num = num + (!string.IsNullOrEmpty(key) ? (float) (key.Length * 2) : 0.0f) + (float) IntPtr.Size;
    }
    bool flag1 = false;
    this.addSample();
    Reporter.Log log = new Reporter.Log()
    {
      logType = (Reporter._LogType) type,
      condition = index,
      stacktrace = key,
      sampleId = this.samples.Count - 1
    };
    this.logsMemUsage += (float) ((double) (num + log.GetMemoryUsage()) / 1024.0 / 1024.0);
    if ((double) this.TotalMemUsage > (double) this.maxSize)
    {
      this.clear();
      UnityEngine.Debug.Log((object) ("Memory Usage Reach" + (object) this.maxSize + " mb So It is Cleared"));
    }
    else
    {
      bool flag2;
      if (this.logsDic.ContainsKey(index, stacktrace))
      {
        flag2 = false;
        ++this.logsDic[index][stacktrace].count;
      }
      else
      {
        flag2 = true;
        this.collapsedLogs.Add(log);
        this.logsDic[index][stacktrace] = log;
        if (type == LogType.Log)
          ++this.numOfCollapsedLogs;
        else if (type == LogType.Warning)
          ++this.numOfCollapsedLogsWarning;
        else
          ++this.numOfCollapsedLogsError;
      }
      if (type == LogType.Log)
        ++this.numOfLogs;
      else if (type == LogType.Warning)
        ++this.numOfLogsWarning;
      else
        ++this.numOfLogsError;
      this.logs.Add(log);
      if (!this.collapse || flag2)
      {
        bool flag3 = false;
        if (log.logType == Reporter._LogType.Log && !this.showLog)
          flag3 = true;
        if (log.logType == Reporter._LogType.Warning && !this.showWarning)
          flag3 = true;
        if (log.logType == Reporter._LogType.Error && !this.showError)
          flag3 = true;
        if (log.logType == Reporter._LogType.Assert && !this.showError)
          flag3 = true;
        if (log.logType == Reporter._LogType.Exception && !this.showError)
          flag3 = true;
        if (!flag3 && (string.IsNullOrEmpty(this.filterText) || log.condition.ToLower().Contains(this.filterText.ToLower())))
        {
          this.currentLog.Add(log);
          flag1 = true;
        }
      }
      if (flag1)
      {
        this.calculateStartIndex();
        if (this.startIndex >= this.currentLog.Count - (int) ((double) Screen.height * 0.75 / (double) this.size.y))
          this.scrollPosition.y += this.size.y;
      }
      try
      {
        this.gameObject.SendMessage("OnLog", (object) log);
      }
      catch (Exception ex)
      {
        UnityEngine.Debug.LogException(ex);
      }
    }
  }

  private void CaptureLogThread(string condition, string stacktrace, LogType type)
  {
    Reporter.Log log = new Reporter.Log()
    {
      condition = condition,
      stacktrace = stacktrace,
      logType = (Reporter._LogType) type
    };
    lock (this.threadedLogs)
      this.threadedLogs.Add(log);
  }

  private void OnLevelWasLoaded()
  {
    if (this.clearOnNewSceneLoaded)
      this.clear();
    this.currentScene = SceneManager.GetActiveScene().name;
    UnityEngine.Debug.Log((object) ("Scene " + SceneManager.GetActiveScene().name + " is loaded"));
  }

  private void OnApplicationQuit()
  {
    PlayerPrefs.SetInt("Reporter_currentView", (int) this.currentView);
    PlayerPrefs.SetInt("Reporter_show", 0);
    PlayerPrefs.SetInt("Reporter_collapse", !this.collapse ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_clearOnNewSceneLoaded", !this.clearOnNewSceneLoaded ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showTime", !this.showTime ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showScene", !this.showScene ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showMemory", !this.showMemory ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showFps", !this.showFps ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showGraph", !this.showGraph ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showLog", !this.showLog ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showWarning", !this.showWarning ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showError", !this.showError ? 0 : 1);
    PlayerPrefs.SetString("Reporter_filterText", this.filterText);
    PlayerPrefs.SetFloat("Reporter_size", this.size.x);
    PlayerPrefs.SetInt("Reporter_showClearOnNewSceneLoadedButton", !this.showClearOnNewSceneLoadedButton ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showTimeButton", !this.showTimeButton ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showSceneButton", !this.showSceneButton ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showMemButton", !this.showMemButton ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showFpsButton", !this.showFpsButton ? 0 : 1);
    PlayerPrefs.SetInt("Reporter_showSearchText", !this.showSearchText ? 0 : 1);
    PlayerPrefs.Save();
  }

  [DebuggerHidden]
  private IEnumerator readInfo()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Reporter.\u003CreadInfo\u003Ec__Iterator36()
    {
      \u003C\u003Ef__this = this
    };
  }

  public enum _LogType
  {
    Error,
    Assert,
    Warning,
    Log,
    Exception,
  }

  public class Sample
  {
    public float time;
    public byte loadedScene;
    public float memory;
    public float fps;
    public string fpsText;

    public static float MemSize()
    {
      return 13f;
    }

    public string GetSceneName()
    {
      if ((int) this.loadedScene == -1)
        return "AssetBundleScene";
      return Reporter.scenes[(int) this.loadedScene];
    }
  }

  public class Log
  {
    public int count = 1;
    public Reporter._LogType logType;
    public string condition;
    public string stacktrace;
    public int sampleId;

    public Reporter.Log CreateCopy()
    {
      return (Reporter.Log) this.MemberwiseClone();
    }

    public float GetMemoryUsage()
    {
      return (float) (8 + this.condition.Length * 2 + this.stacktrace.Length * 2 + 4);
    }
  }

  private enum ReportView
  {
    None,
    Logs,
    Info,
    Snapshot,
  }

  private enum DetailView
  {
    None,
    StackTrace,
    Graph,
  }
}
