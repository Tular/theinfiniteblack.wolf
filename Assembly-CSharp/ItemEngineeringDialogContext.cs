﻿// Decompiled with JetBrains decompiler
// Type: ItemEngineeringDialogContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Items;

public class ItemEngineeringDialogContext
{
  public List<EventDelegate> onSelectedInventoryItemChanged = new List<EventDelegate>();
  private EquipmentItem _selectedInventoryItem;

  public ItemEngineeringDialogContext(UIPanel dlgPanel)
  {
    this.dialogPanel = dlgPanel;
  }

  public UIPanel dialogPanel { get; private set; }

  public EquipmentItem selectedInventoryItem
  {
    get
    {
      return this._selectedInventoryItem;
    }
    set
    {
      if (this._selectedInventoryItem == value)
        return;
      this._selectedInventoryItem = value;
      if (this._selectedInventoryItem == null)
        return;
      EventDelegate.Execute(this.onSelectedInventoryItemChanged);
    }
  }
}
