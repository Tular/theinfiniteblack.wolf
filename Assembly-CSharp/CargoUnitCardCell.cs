﻿// Decompiled with JetBrains decompiler
// Type: CargoUnitCardCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;

public class CargoUnitCardCell : UnitCardCellBase
{
  public string bdSpriteName = "entity_cargo_blackdollars";
  public string creditSpriteName = "entity_cargo_credits";
  public string resourceSpritePrefix = "entity_cargo_";
  public string cargoItemSprite = "entity_cargo";
  public UISprite cargoSprite;

  public void SetCargoResource(ResourceType resourceType)
  {
    this.cargoSprite.spriteName = string.Format("{0}{1}", (object) this.resourceSpritePrefix, (object) resourceType);
  }

  public void SetCargoItem()
  {
    this.cargoSprite.spriteName = this.cargoItemSprite;
  }

  public void SetCargoMoney(bool isBlackDollars)
  {
    this.cargoSprite.spriteName = !isBlackDollars ? this.creditSpriteName : this.bdSpriteName;
  }

  public override void ResetVisuals()
  {
    base.ResetVisuals();
    this.cargoSprite.spriteName = string.Empty;
  }
}
