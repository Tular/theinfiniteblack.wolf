﻿// Decompiled with JetBrains decompiler
// Type: MultiCameraTouch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class MultiCameraTouch : MonoBehaviour
{
  public Text label;

  private void OnEnable()
  {
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.On_TouchUp);
  }

  private void OnDestroy()
  {
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.On_TouchUp);
  }

  private void On_TouchDown(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject != (Object) null))
      return;
    this.label.text = "You touch : " + gesture.pickedObject.name + " on camera : " + gesture.pickedCamera.name;
  }

  private void On_TouchUp(Gesture gesture)
  {
    this.label.text = string.Empty;
  }
}
