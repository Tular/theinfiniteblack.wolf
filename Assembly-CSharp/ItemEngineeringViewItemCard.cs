﻿// Decompiled with JetBrains decompiler
// Type: ItemEngineeringViewItemCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

[AdvancedInspector.AdvancedInspector]
public class ItemEngineeringViewItemCard : MonoBehaviour
{
  [Inspect(1)]
  public string resourcePath = "items";
  [Inspect(0)]
  public UITexture icon;
  [Inspect(2)]
  [Expandable(false)]
  public UILabel title;
  [Inspect(3)]
  [Expandable(false)]
  public UILabel subTitle;
  [Expandable(false)]
  [Inspect(4)]
  public UILabel description;
  [Inspect(5)]
  [Expandable(false)]
  public UILabel equipPoints;
  [Expandable(false)]
  [Inspect(6)]
  public UILabel durability;
  [Expandable(false)]
  [Inspect(7)]
  public UILabel creditValue;
  [Inspect(8)]
  [Expandable(false)]
  public UITextList detailsTextList;
  private bool _isUpdating;
  private EquipmentItem _myItem;

  public void Show(EquipmentItem item)
  {
    if (!this.gameObject.activeSelf)
      this.gameObject.SetActive(true);
    this._isUpdating = false;
    this.StartCoroutine(this.UpdateVisualsCoroutine(item));
  }

  public void Hide()
  {
    NGUITools.SetActiveSelf(this.gameObject, false);
  }

  [DebuggerHidden]
  private IEnumerator UpdateVisualsCoroutine(EquipmentItem item)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ItemEngineeringViewItemCard.\u003CUpdateVisualsCoroutine\u003Ec__Iterator18()
    {
      item = item,
      \u003C\u0024\u003Eitem = item,
      \u003C\u003Ef__this = this
    };
  }

  private void LoadItemTexture(ItemIcon itemIcon)
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) itemIcon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.icon.mainTexture = (Texture) texture2D;
  }

  private void ResetVisuals()
  {
    if ((bool) ((Object) this.detailsTextList))
      this.detailsTextList.Clear();
    this.title.text = string.Empty;
    this.subTitle.text = string.Empty;
    this.description.text = string.Empty;
    this.equipPoints.text = string.Empty;
    this.durability.text = string.Empty;
    this.creditValue.text = string.Empty;
    if ((Object) this.icon.mainTexture == (Object) null)
      return;
    Texture mainTexture = this.icon.mainTexture;
    this.icon.mainTexture = (Texture) null;
  }

  private void OnDisable()
  {
    this._isUpdating = false;
    this.LogD("ItemEngineeringViewItemCard.OnDisable() Called on - " + this.gameObject.name);
    this._myItem = (EquipmentItem) null;
    this.ResetVisuals();
  }
}
