﻿// Decompiled with JetBrains decompiler
// Type: EchoDevicePathInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class EchoDevicePathInfo : MonoBehaviour
{
  public UITextList output;

  private void Start()
  {
    if ((Object) this.output == (Object) null)
      return;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.AppendFormat("platform: {0}\n", (object) Application.platform);
    stringBuilder.AppendFormat("dataPath: {0}\n", (object) Application.dataPath);
    stringBuilder.AppendFormat("persistentDataPath: {0}\n", (object) Application.persistentDataPath);
    stringBuilder.AppendFormat("temporaryCachePath: {0}\n", (object) Application.temporaryCachePath);
    this.output.Add(stringBuilder.ToString());
  }
}
