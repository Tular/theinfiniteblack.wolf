﻿// Decompiled with JetBrains decompiler
// Type: EventLogEntryCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIWidget))]
public class EventLogEntryCell : MonoBehaviour, IScrollViewCell
{
  public int xPadding = 4;
  public UILabel entry;

  public Vector3 extents
  {
    get
    {
      return new Vector3((float) this.entry.width / 2f, (float) this.entry.height / 2f, 0.0f);
    }
  }

  public int fontSize
  {
    get
    {
      return this.entry.fontSize;
    }
    set
    {
      this.entry.fontSize = value;
    }
  }

  public void ResetVisuals()
  {
    this.entry.text = string.Empty;
  }

  private void Awake()
  {
    this.entry.text = string.Empty;
  }
}
