﻿// Decompiled with JetBrains decompiler
// Type: PlayerDirectory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class PlayerDirectory : MonoBehaviour
{
  public PlayerDirectoryScrollView view;
  public Transform prefPlayerEntry;
  public Transform prefGroupHeading;
  public Transform prefAlphaHeading;
  private PlayerDirectoryIndex _directoryIndex;
  private LocalPlayersDirectoryIndex _sectorIndex;
  private CorpDirectoryIndex _corpIndex;
  private OnlinePlayersDirectoryIndex _onlineIndex;
  private AllianceDirectoryIndex _allianceIndex;
  private bool _isInitialized;

  [ContextMenu("Current Index Count")]
  public void CurrentIndexCount()
  {
    this.LogD(string.Format("index count: {0}", (object) this._directoryIndex.Count));
  }

  public void ShowSectorIndex()
  {
    this._directoryIndex = (PlayerDirectoryIndex) this._sectorIndex;
    this.view.data = (IPooledScrollViewCellInifoCollection<PlayerDirectoryCellInfo>) this._directoryIndex;
  }

  public void ShowCorpIndex()
  {
    this._directoryIndex = (PlayerDirectoryIndex) this._corpIndex;
    this.view.data = (IPooledScrollViewCellInifoCollection<PlayerDirectoryCellInfo>) this._directoryIndex;
    this.view.reposition = true;
  }

  public void ShowAllianceIndex()
  {
    this._directoryIndex = (PlayerDirectoryIndex) this._allianceIndex;
    this.view.data = (IPooledScrollViewCellInifoCollection<PlayerDirectoryCellInfo>) this._allianceIndex;
    this.view.reposition = true;
  }

  public void ShowOnlinePlayersIndex()
  {
    this._directoryIndex = (PlayerDirectoryIndex) this._onlineIndex;
    this.view.data = (IPooledScrollViewCellInifoCollection<PlayerDirectoryCellInfo>) this._directoryIndex;
    this.view.reposition = true;
  }

  private void Awake()
  {
    TibProxy.Instance.onPlayerOnlineStatusChangedEvent -= new EventHandler<PlayerOnlineStatusChangedEventArgs>(this.TibProxy_onPlayerOnlineStatusChangedEvent);
    TibProxy.Instance.onPlayerOnlineStatusChangedEvent += new EventHandler<PlayerOnlineStatusChangedEventArgs>(this.TibProxy_onPlayerOnlineStatusChangedEvent);
    this._sectorIndex = new LocalPlayersDirectoryIndex(this.prefGroupHeading);
    this._corpIndex = new CorpDirectoryIndex(this.prefGroupHeading);
    this._onlineIndex = new OnlinePlayersDirectoryIndex(this.prefGroupHeading, this.prefAlphaHeading);
    this._allianceIndex = new AllianceDirectoryIndex(this.prefGroupHeading);
    this._directoryIndex = (PlayerDirectoryIndex) this._onlineIndex;
  }

  private void OnEnable()
  {
    if (TibProxy.Instance == null || TibProxy.gameState == null || this._directoryIndex == null)
      return;
    this._directoryIndex.Clear();
    foreach (ClientPlayer clientPlayer in TibProxy.gameState.Players.get_Values())
    {
      if (clientPlayer.Online && !this._directoryIndex.ContainsInfoFor(clientPlayer))
        this._directoryIndex.Add(new PlayerDirectoryCellInfo(this.prefPlayerEntry, clientPlayer));
    }
    this._isInitialized = true;
  }

  [DebuggerHidden]
  private IEnumerable PopulateDirectory_Coroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    PlayerDirectory.\u003CPopulateDirectory_Coroutine\u003Ec__Iterator12 coroutineCIterator12 = new PlayerDirectory.\u003CPopulateDirectory_Coroutine\u003Ec__Iterator12()
    {
      \u003C\u003Ef__this = this
    };
    // ISSUE: reference to a compiler-generated field
    coroutineCIterator12.\u0024PC = -2;
    return (IEnumerable) coroutineCIterator12;
  }

  private void OnDisable()
  {
  }

  private void OnDestroy()
  {
    TibProxy.Instance.onPlayerOnlineStatusChangedEvent -= new EventHandler<PlayerOnlineStatusChangedEventArgs>(this.TibProxy_onPlayerOnlineStatusChangedEvent);
    this._sectorIndex.Clear();
    this._corpIndex.Clear();
    this._onlineIndex.Clear();
    this._allianceIndex.Clear();
  }

  private void TibProxy_onPlayerOnlineStatusChangedEvent(object sender, PlayerOnlineStatusChangedEventArgs e)
  {
    if (e.IsOnline && !this._directoryIndex.ContainsInfoFor(e.Source))
    {
      this._directoryIndex.Add(new PlayerDirectoryCellInfo(this.prefPlayerEntry, e.Source));
    }
    else
    {
      if (e.IsOnline)
        return;
      this._directoryIndex.Remove(e.Source);
    }
  }
}
