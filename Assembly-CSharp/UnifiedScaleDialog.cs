﻿// Decompiled with JetBrains decompiler
// Type: UnifiedScaleDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.SettingsUI;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class UnifiedScaleDialog : DialogBase
{
  public UISlider uiScaleSlider;
  public UILabel uiScaleThumbLabel;
  public UISlider entityListScaleSlider;
  public UILabel entityListScaleThumbLabel;
  public UISlider chatFontSizeSlider;
  public UILabel chatFontSizeThumbLabel;
  public UISlider eventLogFontSizeSlider;
  public UILabel eventLogFontSizeThumbLabel;
  public float minUiScaleMultiplier;
  public float maxUiScaleMultiplier;
  public float minEntityListScaleMultiplier;
  public float maxEntityListScaleMultiplier;
  public int defaultChatFontSize;
  public int minChatFontSize;
  public int maxChatFontSize;
  public int defaultEventLogFontSize;
  public int minEventLogFontSize;
  public int maxEventLogFontSize;
  public string uiScalerAlias;
  public string entityListScalerAlias;
  private AdjustableScale _uiScaler;
  private AdjustableScale _entityListScaler;
  private ChatScrollView _chatScrollView;

  protected override void OnShow()
  {
    base.OnShow();
    this._uiScaler = AdjustableScale.GetScaler(this.uiScalerAlias);
    this.minUiScaleMultiplier = this._uiScaler.min;
    this.maxUiScaleMultiplier = this._uiScaler.max;
    this.uiScaleSlider.value = (float) (((double) this._uiScaler.initialValue - 1.0) / ((double) this.maxUiScaleMultiplier - (double) this.minUiScaleMultiplier));
    this.uiScaleSlider.ForceUpdate();
    this._entityListScaler = AdjustableScale.GetScaler(this.entityListScalerAlias);
    if ((bool) ((Object) this._entityListScaler))
    {
      this.minEntityListScaleMultiplier = this._entityListScaler.min;
      this.maxEntityListScaleMultiplier = this._entityListScaler.max;
      this.entityListScaleSlider.value = (float) (((double) this._entityListScaler.initialValue - 1.0) / ((double) this.maxEntityListScaleMultiplier - (double) this.minEntityListScaleMultiplier));
      this.entityListScaleSlider.ForceUpdate();
    }
    this._chatScrollView = DialogManager.GetDialog<ChatDialog>().view;
    this.chatFontSizeSlider.value = (float) (this._chatScrollView.paragraphFontSize - this.minChatFontSize) / (float) (this.maxChatFontSize - this.minChatFontSize);
    this.entityListScaleSlider.ForceUpdate();
    this.eventLogFontSizeSlider.value = (float) (EventLog.Instance.eventLogView.fontSize - this.minEventLogFontSize) / (float) (this.maxEventLogFontSize - this.minEventLogFontSize);
    this.eventLogFontSizeSlider.ForceUpdate();
  }

  public void OnUiScaleDefaultClicked()
  {
    this.uiScaleSlider.value = (float) (0.5 / ((double) this._uiScaler.max - (double) this._uiScaler.min));
  }

  public void OnEntityListScaleDefaultClicked()
  {
    this.entityListScaleSlider.value = (float) (0.5 / ((double) this._entityListScaler.max - (double) this._entityListScaler.min));
  }

  public void OnChatFontSizeDefaultClicked()
  {
    this.chatFontSizeSlider.value = (float) (this.defaultChatFontSize - this.minChatFontSize) / (float) (this.maxChatFontSize - this.minChatFontSize);
  }

  public void OnEventLogFontSizeDefaultClicked()
  {
    this.eventLogFontSizeSlider.value = (float) (this.defaultEventLogFontSize - this.minEventLogFontSize) / (float) (this.maxEventLogFontSize - this.minEventLogFontSize);
  }

  private void OnUiScaleChange()
  {
    if ((Object) UIProgressBar.current != (Object) this.uiScaleSlider)
      return;
    float scaleMultiplier = (float) (1.0 + ((double) this._uiScaler.max - (double) this._uiScaler.min) * (double) UIProgressBar.current.value);
    this._uiScaler.UpdateScale(scaleMultiplier);
    this.uiScaleThumbLabel.text = string.Format("{0:n2}x", (object) scaleMultiplier);
  }

  private void OnEntityListScaleChange()
  {
    if ((Object) UIProgressBar.current != (Object) this.entityListScaleSlider)
      return;
    float scaleMultiplier = (float) (1.0 + ((double) this._uiScaler.max - (double) this._uiScaler.min) * (double) UIProgressBar.current.value);
    this._entityListScaler.UpdateScale(scaleMultiplier);
    this.entityListScaleThumbLabel.text = string.Format("{0:n2}x", (object) scaleMultiplier);
  }

  private void OnChatFontSizeChange()
  {
    if ((Object) UIProgressBar.current != (Object) this.chatFontSizeSlider)
      return;
    int num = (int) ((double) this.minChatFontSize + (double) (this.maxChatFontSize - this.minChatFontSize) * (double) UIProgressBar.current.value);
    this._chatScrollView.paragraphFontSize = num;
    TibProxy.mySettings.Social.chatFontSize = num;
    this.chatFontSizeThumbLabel.text = string.Format("{0}", (object) num);
  }

  private void OnEventLogFontSizeChange()
  {
    if ((Object) UIProgressBar.current != (Object) this.eventLogFontSizeSlider)
      return;
    int num = (int) ((double) this.minEventLogFontSize + (double) (this.maxEventLogFontSize - this.minEventLogFontSize) * (double) UIProgressBar.current.value);
    EventLog.Instance.eventLogView.fontSize = num;
    TibProxy.mySettings.Social.eventLogFontSize = num;
    this.eventLogFontSizeThumbLabel.text = string.Format("{0}", (object) num);
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    EventDelegate.Add(this.uiScaleSlider.onChange, new EventDelegate.Callback(this.OnUiScaleChange));
    EventDelegate.Add(this.entityListScaleSlider.onChange, new EventDelegate.Callback(this.OnEntityListScaleChange));
    EventDelegate.Add(this.chatFontSizeSlider.onChange, new EventDelegate.Callback(this.OnChatFontSizeChange));
    EventDelegate.Add(this.eventLogFontSizeSlider.onChange, new EventDelegate.Callback(this.OnEventLogFontSizeChange));
  }

  protected override void OnHide()
  {
    base.OnHide();
    if (TibProxy.accountManager == null)
      return;
    TibProxy.accountManager.SaveAll();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
