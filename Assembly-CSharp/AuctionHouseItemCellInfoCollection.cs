﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseItemCellInfoCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AuctionHouseItemCellInfoCollection : PooledScrollViewCellInfoCollectionBase<AuctionHouseItemCellInfo>
{
  public Transform prefAuctionItemCell;

  public AuctionHouseItemCellInfoCollection(Transform prefCell)
  {
    this.prefAuctionItemCell = prefCell;
  }

  public AuctionHouseItemListingScrollViewContext scrollViewContext { get; set; }

  protected override IEnumerable<AuctionHouseItemCellInfo> GetValues()
  {
    return (IEnumerable<AuctionHouseItemCellInfo>) this.mInfoList;
  }
}
