﻿// Decompiled with JetBrains decompiler
// Type: EchoMessages
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EchoMessages : MonoBehaviour
{
  public string sender;

  private void Awake()
  {
    Debug.Log((object) string.Format("{0} - Awake()", (object) this.sender));
  }

  private void Start()
  {
    Debug.Log((object) string.Format("{0} - Start()", (object) this.sender));
  }

  private void OnEnable()
  {
    Debug.Log((object) string.Format("{0} - OnEnable()", (object) this.sender));
  }

  private void Update()
  {
  }
}
