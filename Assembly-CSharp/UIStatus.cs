﻿// Decompiled with JetBrains decompiler
// Type: UIStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIStatus : MonoBehaviour
{
  private static BetterList<UIStatus.Entry> mEntries = new BetterList<UIStatus.Entry>();
  public AnimationCurve curve = new AnimationCurve(new Keyframe[2]
  {
    new Keyframe(0.0f, 0.0f),
    new Keyframe(1f, 1f)
  });
  public float fadeInTime = 0.25f;
  public float shownDuration = 4f;
  public float transitionTime = 0.25f;
  public float fadeOutTime = 4f;
  public Vector2 borderPadding = Vector2.zero;
  private Color mInvisible = new Color(0.0f, 0.0f, 0.0f, 0.0f);
  private Color mBackColor = Color.black;
  private static UIStatus mInst;
  public UILabel label;
  public UISprite background;
  private float mShownTime;
  private float mAlpha;
  private bool mFadingIn;
  private bool mForceFadeOut;
  private UIStatus.Entry mCurrent;
  private Transform mTrans;

  private void Awake()
  {
    UIStatus.mInst = this;
    this.mTrans = this.transform;
    this.mBackColor = this.background.color;
  }

  private void Start()
  {
    UIStatus.mEntries.Clear();
    this.label.enabled = false;
    this.background.enabled = false;
  }

  private void OnDestroy()
  {
    if (!((Object) UIStatus.mInst == (Object) this))
      return;
    UIStatus.mInst = (UIStatus) null;
  }

  private void UpdateText()
  {
    this.label.text = this.mCurrent.text;
    this.label.color = this.mCurrent.color;
    this.label.enabled = true;
    Vector2 localSize = this.label.localSize;
    Vector4 border = this.background.border;
    localSize.x += border.x + border.z + this.borderPadding.x;
    localSize.y += border.y + border.w + this.borderPadding.y;
    this.background.width = Mathf.RoundToInt(localSize.x);
    this.background.height = Mathf.RoundToInt(localSize.y);
    this.background.enabled = true;
  }

  private void Update()
  {
    if ((double) this.mAlpha == 0.0)
    {
      if (UIStatus.mEntries.size > 0)
      {
        this.mCurrent = UIStatus.mEntries[0];
        UIStatus.mEntries.RemoveAt(0);
        this.mFadingIn = true;
        this.mForceFadeOut = false;
        this.UpdateText();
      }
      else if (!this.mFadingIn)
        return;
    }
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if (this.mCurrent.persistent && UIStatus.mEntries.size == 0)
      this.mShownTime = realtimeSinceStartup;
    if (UIStatus.mEntries.size > 0)
      this.mForceFadeOut = true;
    float num = !this.mForceFadeOut ? this.mShownTime + this.shownDuration : this.mShownTime;
    if (this.mFadingIn)
    {
      this.mAlpha += RealTime.deltaTime / this.fadeInTime;
      if ((double) this.mAlpha > 1.0)
      {
        this.mTrans.localScale = Vector3.one;
        this.background.color = this.mBackColor;
        this.label.color = this.mCurrent.color;
        this.mShownTime = realtimeSinceStartup;
        this.mFadingIn = false;
      }
      else
      {
        float t = this.curve.Evaluate(this.mAlpha);
        this.mTrans.localScale = new Vector3((float) (0.200000002980232 + 0.800000011920929 * (double) t), 1f, 1f);
        this.background.color = Color.Lerp(this.mInvisible, this.mBackColor, t);
        this.label.color = Color.Lerp(this.mInvisible, this.mCurrent.color, t * t);
      }
    }
    else
    {
      if ((double) num >= (double) realtimeSinceStartup)
        return;
      this.mAlpha -= RealTime.deltaTime / (!this.mForceFadeOut ? this.fadeOutTime : this.transitionTime);
      if ((double) this.mAlpha < 0.00999999977648258)
      {
        this.label.enabled = false;
        this.background.enabled = false;
        this.mAlpha = 0.0f;
      }
      else
      {
        this.background.color = Color.Lerp(this.mInvisible, this.mBackColor, this.mAlpha);
        this.label.color = Color.Lerp(this.mInvisible, this.mCurrent.color, this.mAlpha * this.mAlpha);
      }
    }
  }

  public static void Show(string text)
  {
    UIStatus.Show(text, new Color(0.7f, 0.7f, 0.7f), false, false);
  }

  public static void Show(string text, Color c)
  {
    UIStatus.Show(text, c, false, false);
  }

  public static void Show(string text, Color c, bool persistent, bool instant)
  {
    if ((Object) UIStatus.mInst != (Object) null && instant && (UIStatus.mInst.mFadingIn || (double) UIStatus.mInst.mCurrent.color.a == 1.0))
    {
      UIStatus.mEntries.Clear();
      UIStatus.mInst.mCurrent.text = text;
      UIStatus.mInst.mCurrent.color = c;
      UIStatus.mInst.UpdateText();
    }
    else
    {
      UIStatus.mEntries.Add(new UIStatus.Entry()
      {
        text = text,
        color = c,
        persistent = persistent
      });
      if (!((Object) UIStatus.mInst != (Object) null))
        return;
      UIStatus.mInst.mForceFadeOut = false;
    }
  }

  public static void Hide()
  {
    UIStatus.mEntries.Clear();
    if (!((Object) UIStatus.mInst != (Object) null))
      return;
    UIStatus.mInst.mCurrent.persistent = false;
    UIStatus.mInst.mFadingIn = false;
    UIStatus.mInst.mForceFadeOut = true;
  }

  private struct Entry
  {
    public string text;
    public Color color;
    public bool persistent;
  }
}
