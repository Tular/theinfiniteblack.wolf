﻿// Decompiled with JetBrains decompiler
// Type: ChatHelpOverlay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ChatHelpOverlay : MonoBehaviour
{
  public UIScrollView contentScrollView;
  public UILabel content;
  public string webfileKey;
  public string defaultMessage;
  private bool _contentIsSet;

  private void OnEnable()
  {
    if (this._contentIsSet)
    {
      this.contentScrollView.ResetPosition();
    }
    else
    {
      this.content.alignment = NGUIText.Alignment.Center;
      this.content.text = this.defaultMessage;
      this.StartCoroutine(this.ParseHelpFileCoroutine());
    }
  }

  [DebuggerHidden]
  private IEnumerator ParseHelpFileCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ChatHelpOverlay.\u003CParseHelpFileCoroutine\u003Ec__Iterator10()
    {
      \u003C\u003Ef__this = this
    };
  }
}
