﻿// Decompiled with JetBrains decompiler
// Type: MemoryUsage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class MemoryUsage : MonoBehaviour
{
  public float updateInterval = 2f;
  public UILabel monoUsedSize;
  public UILabel monoHeapSize;
  public UILabel totalAllocated;
  public UILabel totalReserved;
  public UILabel totalUnusedReserved;

  [DebuggerHidden]
  private IEnumerator UpdateLabels()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MemoryUsage.\u003CUpdateLabels\u003Ec__Iterator23()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void OnEnable()
  {
    this.StartCoroutine("UpdateLabels");
  }

  private void OnDisable()
  {
    this.StopAllCoroutines();
  }
}
