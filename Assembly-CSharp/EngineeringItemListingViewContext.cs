﻿// Decompiled with JetBrains decompiler
// Type: EngineeringItemListingViewContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;

public class EngineeringItemListingViewContext : IItemScrollViewContext, IGeneralScrollViewContext<EquipmentItem>
{
  private ItemType _itemType = ItemType.NULL;
  private bool _hasChanged;
  private List<EquipmentItem> _items;
  private ItemEngineeringDialogContext _dialogContext;

  public EngineeringItemListingViewContext(ItemEngineeringDialogContext dlgContext)
  {
    this._dialogContext = dlgContext;
    this._hasChanged = true;
  }

  public ItemType itemType
  {
    get
    {
      return this._itemType;
    }
    set
    {
      this._itemType = value;
      this._hasChanged = true;
    }
  }

  public bool hasChanged
  {
    get
    {
      this._hasChanged = this._hasChanged || this._items != TibProxy.gameState.MyShip.get_Inventory();
      return this._hasChanged;
    }
  }

  public IEnumerable<EquipmentItem> items
  {
    get
    {
      this._items = TibProxy.gameState.MyShip.get_Inventory();
      this._hasChanged = false;
      return this._items.Where<EquipmentItem>((Func<EquipmentItem, bool>) (i =>
      {
        if (i.Rarity != ItemRarity.COMMON)
          return i.Type == this.itemType;
        return false;
      }));
    }
  }

  public ItemViewContext viewContext { get; private set; }

  public void SelectItemToEngineer(EquipmentItem item)
  {
    this._dialogContext.selectedInventoryItem = item;
  }
}
