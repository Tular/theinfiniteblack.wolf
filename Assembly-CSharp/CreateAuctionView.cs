﻿// Decompiled with JetBrains decompiler
// Type: CreateAuctionView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class CreateAuctionView : DialogViewBase<CreateAuctionDialogContext>
{
  public string resourcePath = "items";
  public UITexture itemIcon;
  public UILabel itemTitle;
  public UILabel itemSubtitle;
  public UILabel itemDescription;
  public UILabel itemEquipPoints;
  public UILabel itemDurability;
  public UIInput itemStartBid;
  public UIInput itemBuyout;
  public UITextList itemDetails;
  public UILabel auctionPostingFee;
  private UIEventTrigger _startBidTrigger;
  private UIEventTrigger _buyoutTrigger;
  private bool _isUpdating;

  public void OnStartBidClicked()
  {
    if (this.itemStartBid.enabled)
      return;
    EventDelegate.Add(this._startBidTrigger.onDeselect, new EventDelegate.Callback(this.itemStartBid.Submit), true);
    this.itemStartBid.value = string.Empty;
    this.itemStartBid.validation = UIInput.Validation.Integer;
    this.itemStartBid.enabled = true;
    this.itemStartBid.isSelected = true;
  }

  public void OnSetBuyoutClicked()
  {
    if (this.itemBuyout.enabled)
      return;
    EventDelegate.Add(this._buyoutTrigger.onDeselect, new EventDelegate.Callback(this.itemBuyout.Submit), true);
    this.itemBuyout.value = string.Empty;
    this.itemBuyout.validation = UIInput.Validation.Integer;
    this.itemBuyout.enabled = true;
    this.itemBuyout.isSelected = true;
  }

  public void OnSubmitAuctionClicked()
  {
    TibProxy.gameState.DoAuctionPost(this.mDialogContext.item, this.mDialogContext.proposedBid, this.mDialogContext.proposedBuyout);
    this.mDialogContext.dialog.Hide();
  }

  public void OnCancelClicked()
  {
    this.mDialogContext.dialog.Hide();
  }

  private void SetFormattedIntInputValue(UIInput input, string val)
  {
    input.validation = UIInput.Validation.None;
    input.value = val;
    input.isSelected = false;
    input.enabled = false;
  }

  private void SetFormattedIntInputValue(UIInput input, int val, string format)
  {
    input.validation = UIInput.Validation.None;
    input.value = string.Format(format, (object) val);
    input.isSelected = false;
    input.enabled = false;
  }

  private void OnStartBidEntered()
  {
    EventDelegate.Remove(this._startBidTrigger.onDeselect, new EventDelegate.Callback(this.OnStartBidEntered));
    int result;
    if (int.TryParse(this.itemStartBid.value, out result))
      this.mDialogContext.proposedBid = result;
    else
      result = this.mDialogContext.proposedBid;
    this.SetFormattedIntInputValue(this.itemStartBid, result, "{0:$#,###0}");
  }

  private void OnBuyoutEntered()
  {
    EventDelegate.Remove(this._buyoutTrigger.onDeselect, new EventDelegate.Callback(this.OnBuyoutEntered));
    int result;
    if (int.TryParse(this.itemBuyout.value, out result))
      this.mDialogContext.proposedBuyout = result;
    else
      result = this.mDialogContext.proposedBuyout;
    if (this.mDialogContext.buyoutSet)
      this.SetFormattedIntInputValue(this.itemBuyout, result, "{0:$#,###0}");
    else
      this.SetFormattedIntInputValue(this.itemBuyout, "-= NO BUYOUT -=");
  }

  protected override void OnAwake()
  {
    this.itemStartBid.enabled = false;
    this.itemBuyout.enabled = false;
    this.itemStartBid.validation = UIInput.Validation.None;
    this.itemBuyout.validation = UIInput.Validation.None;
    EventDelegate.Add(this.itemStartBid.onSubmit, new EventDelegate.Callback(this.OnStartBidEntered));
    EventDelegate.Add(this.itemBuyout.onSubmit, new EventDelegate.Callback(this.OnBuyoutEntered));
    this._startBidTrigger = this.itemStartBid.GetComponent<UIEventTrigger>();
    this._buyoutTrigger = this.itemBuyout.GetComponent<UIEventTrigger>();
    if (!(bool) ((Object) this._startBidTrigger))
      this._startBidTrigger = this.itemStartBid.gameObject.AddComponent<UIEventTrigger>();
    if ((bool) ((Object) this._buyoutTrigger))
      return;
    this._buyoutTrigger = this.itemBuyout.gameObject.AddComponent<UIEventTrigger>();
  }

  protected override void OnShow()
  {
    base.OnShow();
    this.itemStartBid.enabled = false;
    this.itemBuyout.enabled = false;
    this.StartCoroutine(this.UpdateVisualsCoroutine());
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.itemStartBid.enabled = false;
    this.itemBuyout.enabled = false;
    this.ResetVisuals();
  }

  [DebuggerHidden]
  private IEnumerator UpdateVisualsCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CreateAuctionView.\u003CUpdateVisualsCoroutine\u003Ec__Iterator17()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateAuctionDetails()
  {
    this.auctionPostingFee.text = string.Format("{0:$#,###0}", (object) this.mDialogContext.postingFee);
    this.itemStartBid.value = string.Format("{0:$#,###0}", (object) this.mDialogContext.proposedBid);
    this.itemBuyout.value = !this.mDialogContext.buyoutSet ? "-= NO BUYOUT =-" : string.Format("{0:$#,###0}", (object) this.mDialogContext.proposedBuyout);
  }

  private void UpdateVisuals()
  {
    EquipmentItem equipmentItem = this.mDialogContext.item;
    this.LoadItemTexture(equipmentItem.Icon);
    StringBuilder stringBuilder1 = new StringBuilder(100);
    StringBuilder stringBuilder2 = new StringBuilder(100);
    StringBuilder stringBuilder3 = new StringBuilder(500);
    equipmentItem.AppendName(stringBuilder1);
    Markup.GetNGUI(stringBuilder1);
    this.itemTitle.text = stringBuilder1.ToString();
    stringBuilder2.Append(equipmentItem.Rarity.GetMarkup());
    equipmentItem.AppendSubTitle(stringBuilder2);
    Markup.GetNGUI(stringBuilder2);
    this.itemSubtitle.text = stringBuilder2.ToString();
    equipmentItem.AppendDescription(stringBuilder3);
    Markup.GetNGUI(stringBuilder3);
    this.itemDescription.text = stringBuilder3.ToString();
    this.itemEquipPoints.text = string.Format("{0}", (object) equipmentItem.EPCost);
    this.itemDurability.text = string.Format("{0}%", (object) equipmentItem.Durability);
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.AppendFormat("{0}", (object) "[g]");
    string newValue1 = "\n[g]";
    stringBuilder4.AppendLine(equipmentItem.Pros.Replace("\n", newValue1));
    stringBuilder4.AppendFormat("{0}", (object) "[r]");
    string newValue2 = "\n[r]";
    stringBuilder4.AppendLine(equipmentItem.Cons.Replace("\n", newValue2));
    Markup.GetNGUI(stringBuilder4);
    if ((double) ((UIScrollBar) this.itemDetails.scrollBar).barSize < 1.0)
      NGUITools.SetActiveSelf(this.itemDetails.scrollBar.gameObject, true);
    else
      NGUITools.SetActiveSelf(this.itemDetails.scrollBar.gameObject, false);
    this.itemDetails.Clear();
    this.itemDetails.Add(stringBuilder4.ToString());
    this.itemDetails.scrollValue = 0.0f;
  }

  private void ResetVisuals()
  {
    if ((bool) ((Object) this.itemDetails))
      this.itemDetails.Clear();
    this.itemIcon.mainTexture = (Texture) null;
    this.itemTitle.text = string.Empty;
    this.itemSubtitle.text = string.Empty;
    this.itemDescription.text = string.Empty;
    this.itemEquipPoints.text = string.Empty;
    this.itemDurability.text = string.Empty;
    this.itemDetails.Clear();
    this.itemStartBid.value = string.Empty;
    this.itemBuyout.value = string.Empty;
    this.auctionPostingFee.text = string.Empty;
  }

  private void LoadItemTexture(ItemIcon icon)
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) icon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.itemIcon.mainTexture = (Texture) texture2D;
  }
}
