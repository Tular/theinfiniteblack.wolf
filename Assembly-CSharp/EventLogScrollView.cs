﻿// Decompiled with JetBrains decompiler
// Type: EventLogScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using UnityEngine;

public class EventLogScrollView : PooledScrollViewBase<EventLogCellInfo>
{
  public int stickDuration = 6;
  public int historyLimit = 10;
  public int purgeSize = 5;
  public Vector2 cellOffset = Vector2.zero;
  private int _fontSize;
  private EventLogEntryCell _prefEntry;
  private int _panelWidth;

  public int fontSize
  {
    get
    {
      return this._fontSize;
    }
    set
    {
      this._fontSize = value;
      this.UpdateFontSize();
      this.ResetPosition();
      this.UpdateCellPositions();
      this.InvalidateBounds();
      this.UpdateVisibleCellData();
      this.ResetPosition();
    }
  }

  private void UpdateFontSize()
  {
    this._prefEntry.fontSize = this._fontSize;
    foreach (EventLogEntryCell componentsInChild in this.GetComponentsInChildren<EventLogEntryCell>(true))
      componentsInChild.fontSize = this._fontSize;
    if (this.data == null)
      return;
    foreach (EventLogCellInfo eventLogCellInfo in this.data.values)
      eventLogCellInfo.UpdateDimensions();
  }

  protected override void UpdateCellPositions()
  {
    ((EventLogCellInfoCollection) this.data).Cleanup();
    Vector3 zero = Vector3.zero;
    bool flag = this._panelWidth != Mathf.RoundToInt(this.panel.width);
    this._panelWidth = Mathf.RoundToInt(this.panel.width);
    foreach (EventLogCellInfo eventLogCellInfo in this.data.values)
    {
      if (flag)
        eventLogCellInfo.UpdateDimensions(this._panelWidth);
      switch (this.contentPivot)
      {
        case UIWidget.Pivot.Top:
          eventLogCellInfo.localPosition = zero - new Vector3(-this.cellPadding.x + this.cellOffset.x, eventLogCellInfo.extents.y + this.cellOffset.y);
          zero -= new Vector3(0.0f, 2f * eventLogCellInfo.extents.y + this.cellPadding.y);
          continue;
        case UIWidget.Pivot.Bottom:
          eventLogCellInfo.localPosition = zero + new Vector3(this.cellPadding.x + this.cellOffset.x, eventLogCellInfo.extents.y + this.cellOffset.y);
          zero += new Vector3(0.0f, 2f * eventLogCellInfo.extents.y + this.cellPadding.y);
          continue;
        default:
          continue;
      }
    }
  }

  protected override void OnInit()
  {
    this.data = (IPooledScrollViewCellInifoCollection<EventLogCellInfo>) new EventLogCellInfoCollection(this.historyLimit, this.purgeSize, this.stickDuration);
    this._prefEntry = EventLog.Instance.prefEventLogEntry.GetComponent<EventLogEntryCell>();
  }

  protected override void OnEnable()
  {
    base.OnEnable();
    if (TibProxy.mySettings == null)
      return;
    this._fontSize = TibProxy.mySettings.Social.eventLogFontSize;
    this.UpdateFontSize();
  }
}
