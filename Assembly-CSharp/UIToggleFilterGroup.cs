﻿// Decompiled with JetBrains decompiler
// Type: UIToggleFilterGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[ExecuteInEditMode]
public class UIToggleFilterGroup : MonoBehaviour
{
  public string label;
  public int sortOrder;
  public GameObject togglePrefab;
  public bool update;
  public UITable optionsRoot;
  public UILabel headingLabel;
  public UIToggleFilterGroup.FilterInfo[] filters;

  protected void Awake()
  {
  }

  protected void Start()
  {
  }

  protected void Update()
  {
    if (!this.update)
      return;
    this.update = false;
    this.headingLabel.text = this.label;
    foreach (UIToggleFilterGroup.FilterInfo filter in this.filters)
    {
      UISwitchControl toggle = filter.toggle;
      filter.toggle = (UISwitchControl) null;
      if ((UnityEngine.Object) toggle != (UnityEngine.Object) null)
        NGUITools.DestroyImmediate((UnityEngine.Object) toggle.gameObject);
    }
    int num = 0;
    foreach (UIToggleFilterGroup.FilterInfo filterInfo in (IEnumerable<UIToggleFilterGroup.FilterInfo>) ((IEnumerable<UIToggleFilterGroup.FilterInfo>) this.filters).OrderBy<UIToggleFilterGroup.FilterInfo, int>((Func<UIToggleFilterGroup.FilterInfo, int>) (o => o.sortOrder)))
    {
      UISwitchControl component = this.optionsRoot.gameObject.AddChild(this.togglePrefab).GetComponent<UISwitchControl>();
      component.value = filterInfo.initialState;
      component.controlLabel.text = filterInfo.label;
      component.name = string.Format("{0} - {1}", (object) num++, (object) filterInfo.label);
      filterInfo.toggle = component;
    }
    this.optionsRoot.Reposition();
  }

  [Serializable]
  public class FilterInfo
  {
    public string label;
    public bool initialState;
    public int sortOrder;
    public UISwitchControl toggle;
  }
}
