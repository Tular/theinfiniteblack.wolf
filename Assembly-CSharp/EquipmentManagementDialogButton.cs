﻿// Decompiled with JetBrains decompiler
// Type: EquipmentManagementDialogButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (UIWidget))]
[RequireComponent(typeof (SBUIButton))]
public class EquipmentManagementDialogButton : MonoBehaviour
{
  public UILabel buttonLabel;
  public bool activeForEquippedItem;
  public string equippedItemText;
  public SBUIButtonComponent[] equippedItemStates;
  public List<EventDelegate> equippedItemOnClick;
  public bool activeForUnequippedItem;
  public string unequippedItemText;
  public SBUIButtonComponent[] unequippedItemStates;
  public List<EventDelegate> unequippedItemOnClick;
  private SBUIButton _button;

  public string text
  {
    get
    {
      if ((Object) this.buttonLabel != (Object) null)
        return this.buttonLabel.text;
      return string.Empty;
    }
    set
    {
      if ((Object) this.buttonLabel == (Object) null)
        return;
      this.buttonLabel.text = value;
    }
  }

  public void SetEquippedState(bool isEquippedItem)
  {
    if (isEquippedItem)
    {
      if (!this.activeForEquippedItem)
      {
        this.buttonLabel.text = string.Empty;
        this._button.onClick = (List<EventDelegate>) null;
        this._button.components = (SBUIButtonComponent[]) null;
        NGUITools.SetActiveSelf(this.gameObject, false);
      }
      else
      {
        this.buttonLabel.text = this.equippedItemText;
        this._button.components = this.equippedItemStates;
        this._button.onClick = this.equippedItemOnClick;
        NGUITools.SetActiveSelf(this.gameObject, true);
      }
    }
    else if (!this.activeForUnequippedItem)
    {
      this.buttonLabel.text = string.Empty;
      this._button.onClick = (List<EventDelegate>) null;
      this._button.components = (SBUIButtonComponent[]) null;
      NGUITools.SetActiveSelf(this.gameObject, false);
    }
    else
    {
      this.buttonLabel.text = this.unequippedItemText;
      this._button.components = this.unequippedItemStates;
      this._button.onClick = this.unequippedItemOnClick;
      NGUITools.SetActiveSelf(this.gameObject, true);
    }
  }

  private void Awake()
  {
    this._button = this.GetComponent<SBUIButton>();
  }

  private void OnEnable()
  {
    foreach (UIRect componentsInChild in this.GetComponentsInChildren<UIWidget>(true))
      componentsInChild.UpdateAnchors();
  }
}
