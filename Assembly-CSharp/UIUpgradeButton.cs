﻿// Decompiled with JetBrains decompiler
// Type: UIUpgradeButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIButton))]
public class UIUpgradeButton : MonoBehaviour
{
  private UIButton mButton;

  private void OnEnable()
  {
    this.OnAccessLevelChanged();
  }

  private void OnAccessLevelChanged()
  {
    this.GetComponent<UIButton>().isEnabled = !PlayerProfile.fullAccess;
  }

  private void OnClick()
  {
    PlayerProfile.fullAccess = true;
    UIMessageBox.Show(Localization.Get("Upgraded"), Localization.Get("Upgraded Info"));
  }
}
