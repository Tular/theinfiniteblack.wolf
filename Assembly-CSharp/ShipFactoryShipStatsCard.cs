﻿// Decompiled with JetBrains decompiler
// Type: ShipFactoryShipStatsCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public class ShipFactoryShipStatsCard : MonoBehaviour
{
  public UILabel shipClassLabel;
  public UILabel shipEvasionLabel;
  public UILabel shipSpeedLabel;
  public UILabel shipEquipPointsLabel;
  public UILabel shipClassSpecialsLabel;
  public Color shipStatsPositiveValue;
  public Color shipStatsNegativeValue;
  private ShipClass _prevShipClass;

  public void UpdateVisuals(ShipClass shipClass)
  {
    if (shipClass == ShipClass.NULL || shipClass == ShipClass.None)
    {
      this.ResetVisuals();
    }
    else
    {
      if (this._prevShipClass == shipClass)
        return;
      if ((bool) ((Object) this.shipClassLabel))
        this.shipClassLabel.text = shipClass.Name();
      this.shipEvasionLabel.text = string.Format("{0:+###;-###;+0} Evasion", (object) shipClass.EvasionBonus());
      this.shipEvasionLabel.color = 0.0 > (double) shipClass.EvasionBonus() ? this.shipStatsNegativeValue : this.shipStatsPositiveValue;
      this.shipSpeedLabel.text = string.Format("{0:0.#;-0.#;0}s Move Speed", (object) (float) ((8000.0 + (double) shipClass.MoveSpeedPenalty()) / 1000.0));
      this.shipSpeedLabel.color = 0 > shipClass.BaseEquipPoints() ? this.shipStatsNegativeValue : this.shipStatsPositiveValue;
      this.shipEquipPointsLabel.text = string.Format("{0} Equipment Points", (object) shipClass.BaseEquipPoints());
      switch (shipClass)
      {
        case ShipClass.WyrdInvader:
          this.shipClassSpecialsLabel.text = "+5 Splash Chance";
          break;
        case ShipClass.WyrdAssassin:
          this.shipClassSpecialsLabel.text = "Stealth Engine";
          break;
        case ShipClass.WyrdReaper:
          this.shipClassSpecialsLabel.text = string.Format("{0}\n{1}", (object) "+8 Stun Chance", (object) "+1s Stun Duration");
          break;
        case ShipClass.Carrier:
          this.shipClassSpecialsLabel.text = string.Format("{0}\n{1}", (object) "Fleet Leadership", (object) "Ultimate Deflector");
          break;
        default:
          this.shipClassSpecialsLabel.text = string.Empty;
          break;
      }
    }
  }

  public void ResetVisuals()
  {
    if ((bool) ((Object) this.shipClassLabel))
      this.shipClassLabel.text = string.Empty;
    this._prevShipClass = ShipClass.None;
    this.shipEvasionLabel.text = string.Empty;
    this.shipSpeedLabel.text = string.Empty;
    this.shipEquipPointsLabel.text = string.Empty;
    this.shipClassSpecialsLabel.text = string.Empty;
  }
}
