﻿// Decompiled with JetBrains decompiler
// Type: MultiLayerUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MultiLayerUI : MonoBehaviour
{
  public void SetAutoSelect(bool value)
  {
    EasyTouch.SetEnableAutoSelect(value);
  }

  public void SetAutoUpdate(bool value)
  {
    EasyTouch.SetAutoUpdatePickedObject(value);
  }

  public void Layer1(bool value)
  {
    LayerMask layerMask = EasyTouch.Get3DPickableLayer();
    EasyTouch.Set3DPickableLayer(!value ? (LayerMask) (~((int) (LayerMask) (~(int) layerMask) | 256)) : (LayerMask) ((int) layerMask | 256));
  }

  public void Layer2(bool value)
  {
    LayerMask layerMask = EasyTouch.Get3DPickableLayer();
    EasyTouch.Set3DPickableLayer(!value ? (LayerMask) (~((int) (LayerMask) (~(int) layerMask) | 512)) : (LayerMask) ((int) layerMask | 512));
  }

  public void Layer3(bool value)
  {
    LayerMask layerMask = EasyTouch.Get3DPickableLayer();
    EasyTouch.Set3DPickableLayer(!value ? (LayerMask) (~((int) (LayerMask) (~(int) layerMask) | 1024)) : (LayerMask) ((int) layerMask | 1024));
  }
}
