﻿// Decompiled with JetBrains decompiler
// Type: CombatUnitCardCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using UnityEngine;

public class CombatUnitCardCell : UnitCardCellBase
{
  public float dmgFlashDuration = 0.1f;
  private readonly Color _hullYellow = new Color(1f, 1f, 0.0f);
  private readonly Color _hullOrange = new Color(1f, 0.68f, 0.0f);
  private readonly Color _hullRed = new Color(1f, 0.0f, 0.0f);
  public UISprite grappleIcon;
  public UISprite stunnedIcon;
  public UISprite attackTargetIcon;
  public UISprite dmgFlash;
  public UILabel hullLabel;
  public UILabel attackTimerLabel;
  public Color selfBorder;
  public Color allyBorder;
  public Color neutralBorder;
  public Color enemyBorder;
  public Color normalBackground;
  public Color followTargetBackground;
  public Color attackTargetBackground;
  public Color grappledBackground;
  public Color stunnedBackground;
  public string dmgFlashHealIcon;
  public string dmgFlashHitIcon;
  public Color dmgFlashHitColor;
  public Color dmgFlashHealColor;
  private float _prevHull;
  private float _flashHideTime;
  private bool _newlySpawned;
  private TweenAlpha _dmgFlashTweener;

  public float hull { get; set; }

  public float maxHull { get; set; }

  public float cooldownRemaining { get; set; }

  public string attackTimerLabelText
  {
    get
    {
      if ((Object) this.attackTimerLabel != (Object) null)
        return this.attackTimerLabel.text;
      return string.Empty;
    }
    set
    {
      if (!((Object) this.attackTimerLabel != (Object) null))
        return;
      this.attackTimerLabel.text = value;
    }
  }

  public bool isFollowTarget { get; set; }

  public bool isAttackTarget { get; set; }

  public bool isGrappled { get; set; }

  public bool isStunned { get; set; }

  public RelationType relationType { get; set; }

  public override void ResetVisuals()
  {
    this.backgroundColor = this.normalBackground;
    this.isFollowTarget = false;
    this.isAttackTarget = false;
    this.isGrappled = false;
    this.isStunned = false;
    this.relationType = RelationType.NEUTRAL;
  }

  public override void UpdateVisuals()
  {
    base.UpdateVisuals();
    if (this._newlySpawned)
    {
      this._prevHull = this.hull;
      this._newlySpawned = false;
    }
    if ((Object) null != (Object) this.grappleIcon)
      this.grappleIcon.gameObject.SetActive(this.isGrappled);
    if ((Object) null != (Object) this.stunnedIcon)
      this.stunnedIcon.gameObject.SetActive(this.isStunned);
    this.attackTargetIcon.enabled = this.isAttackTarget;
    this.attackTimerLabel.enabled = this.isAttackTarget && (double) this.cooldownRemaining > 0.00999999977648258;
    if (this.attackTimerLabel.enabled)
      this.attackTimerLabel.text = string.Format("{0:n1}", (object) this.cooldownRemaining);
    this.UpdateDmgFlash();
    this.SetBorderColor();
    this.SetBackgroundColor();
    this.SetHullTextColor();
  }

  public override void OnSpawned()
  {
    base.OnSpawned();
    if ((Object) this.dmgFlash != (Object) null)
      this.dmgFlash.alpha = 0.0f;
    this._newlySpawned = true;
  }

  protected override void Awake()
  {
    base.Awake();
    this._dmgFlashTweener = this.dmgFlash.gameObject.AddComponent<TweenAlpha>();
    this._dmgFlashTweener.enabled = false;
    this._dmgFlashTweener.duration = 1f;
    this._dmgFlashTweener.from = 1f;
    this._dmgFlashTweener.to = 0.0f;
  }

  private void UpdateDmgFlash()
  {
    if ((Object) this.dmgFlash == (Object) null)
      return;
    if ((double) this._prevHull < (double) this.hull)
    {
      this.dmgFlash.color = this.dmgFlashHealColor;
      this.dmgFlash.spriteName = this.dmgFlashHealIcon;
      this._dmgFlashTweener.ResetToBeginning();
      this._dmgFlashTweener.PlayForward();
    }
    else if ((double) this._prevHull > (double) this.hull)
    {
      this.dmgFlash.color = this.dmgFlashHitColor;
      this.dmgFlash.spriteName = this.dmgFlashHitIcon;
      this._dmgFlashTweener.ResetToBeginning();
      this._dmgFlashTweener.PlayForward();
    }
    this._prevHull = this.hull;
  }

  private void SetHullTextColor()
  {
    float num = (float) (100.0 * ((double) this.hull / (double) this.maxHull));
    if ((double) num >= 90.0)
      this.hullLabel.color = Color.white;
    else if ((double) num >= 50.0)
      this.hullLabel.color = this._hullYellow;
    else if ((double) num >= 20.0)
      this.hullLabel.color = this._hullOrange;
    else
      this.hullLabel.color = this._hullRed;
    this.hullLabel.text = string.Format("{0:n0}", (object) this.hull);
  }

  private void SetBorderColor()
  {
    switch (this.relationType)
    {
      case RelationType.NEUTRAL:
        this.borderColor = this.neutralBorder;
        break;
      case RelationType.SELF:
        this.borderColor = this.selfBorder;
        break;
      case RelationType.FRIEND:
        this.borderColor = this.allyBorder;
        break;
      case RelationType.ENEMY:
        this.borderColor = this.enemyBorder;
        break;
      default:
        this.borderColor = this.neutralBorder;
        break;
    }
  }

  private void SetBackgroundColor()
  {
    if (this.isGrappled)
      this.backgroundColor = this.grappledBackground;
    else if (this.isStunned)
      this.backgroundColor = this.stunnedBackground;
    else if (this.isFollowTarget)
      this.backgroundColor = this.followTargetBackground;
    else if (this.isAttackTarget)
      this.backgroundColor = this.attackTargetBackground;
    else
      this.backgroundColor = this.normalBackground;
  }
}
