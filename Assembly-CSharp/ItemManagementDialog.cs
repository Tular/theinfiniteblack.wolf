﻿// Decompiled with JetBrains decompiler
// Type: ItemManagementDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class ItemManagementDialog : DialogBase
{
  private readonly List<EquipmentItem> _currentItemList = new List<EquipmentItem>(50);
  public UIToggle equippedFilterToggle;
  public UIToggle inventoryFilterToggle;
  public UIToggle bankFilterToggle;
  public UILabel titleLabel;
  public UILabel equipmentPointsLabel;
  public UILabel inventorySlotsLabel;
  public UILabel bankSlotsLabel;
  public ShipStats shipStats;
  public GeneralEquipmentItemScrollView itemScrollView;
  private Ship _ship;
  private EquipmentItemActionContext _currentItemActionContext;
  private bool _isClosing;

  protected override bool ProcessArgs(object[] args)
  {
    if (args.Length != 2)
      throw new ArgumentException("Expected args length is 2.");
    ItemManagementDialog.DisplayMode displayMode = (ItemManagementDialog.DisplayMode) args[1];
    this._ship = (Ship) args[0];
    this._currentItemActionContext = displayMode != ItemManagementDialog.DisplayMode.EquippedItems ? (TibProxy.gameState.MyShip != this._ship ? EquipmentItemActionContext.InventoryOther : EquipmentItemActionContext.InventorySelf) : (TibProxy.gameState.MyShip != this._ship ? EquipmentItemActionContext.EquippedOther : EquipmentItemActionContext.EquippedSelf);
    return true;
  }

  public void EquipSelectedItem()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    TibProxy.gameState.DoItemEquip(component.item);
  }

  public void UnEquipSelectedItem()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    TibProxy.gameState.DoItemUnequip(component.item);
  }

  public void SelectedItemToBank()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    TibProxy.gameState.DoItemToBank(component.item);
  }

  public void SelectedItemFromBank()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    TibProxy.gameState.DoItemFromBank(component.item);
  }

  public void SelectedItemToAuction()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    DialogManager.ShowDialog<CreateAuctionDialog>(new object[1]
    {
      (object) component.item
    });
  }

  public void JettisonSelectedItem()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    TibProxy.gameState.DoItemJettison(component.item);
  }

  public void UpgradeSelectedItem()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    DialogManager.ShowDialog<ItemEngineeringDialog>(new object[1]
    {
      (object) component.item
    });
  }

  public void ScrapSelectedItem()
  {
    if ((UnityEngine.Object) this.itemScrollView.selectedGameObject == (UnityEngine.Object) null)
      return;
    GeneralEquipmentItemCell component = this.itemScrollView.selectedGameObject.GetComponent<GeneralEquipmentItemCell>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null || component.item == null)
      return;
    TibProxy.gameState.DoItemSell(TibProxy.gameState.Local.StarPort, component.item);
  }

  public void OnItemFilterChanged()
  {
    if ((UnityEngine.Object) UIToggle.current == (UnityEngine.Object) null || !UIToggle.current.value)
      return;
    if ((UnityEngine.Object) UIToggle.current == (UnityEngine.Object) this.equippedFilterToggle && UIToggle.current.value)
      this._currentItemActionContext = EquipmentItemActionContext.EquippedSelf;
    else if ((UnityEngine.Object) UIToggle.current == (UnityEngine.Object) this.inventoryFilterToggle && UIToggle.current.value)
    {
      this._currentItemActionContext = EquipmentItemActionContext.InventorySelf;
    }
    else
    {
      if (!((UnityEngine.Object) UIToggle.current == (UnityEngine.Object) this.bankFilterToggle) || !UIToggle.current.value)
        return;
      this._currentItemActionContext = EquipmentItemActionContext.Bank;
    }
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  protected override void OnShow()
  {
    this._isClosing = false;
    this.shipStats.UpdateVisuals(this._ship);
    this.titleLabel.text = string.Format("{0}'s {1}", (object) this._ship.Title, (object) this._ship.Class);
    NGUITools.SetActiveSelf(this.bankFilterToggle.gameObject, TibProxy.gameState.MyShip == this._ship);
    switch (this._currentItemActionContext)
    {
      case EquipmentItemActionContext.InventorySelf:
      case EquipmentItemActionContext.InventoryOther:
        this.inventoryFilterToggle.value = true;
        break;
      case EquipmentItemActionContext.EquippedSelf:
      case EquipmentItemActionContext.EquippedOther:
        this.equippedFilterToggle.value = true;
        break;
      case EquipmentItemActionContext.Bank:
        NGUITools.SetActiveSelf(this.bankFilterToggle.gameObject, true);
        this.bankFilterToggle.value = true;
        break;
      default:
        this.equippedFilterToggle.value = true;
        break;
    }
  }

  [ContextMenu("Dump CurrentItemList")]
  public void DumpItemList()
  {
    using (List<EquipmentItem>.Enumerator enumerator = this._currentItemList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EquipmentItem current = enumerator.Current;
        this.LogD(string.Format("{0} - {1}:Equipped - {2}", (object) current.LongName, (object) current.Type, (object) TibProxy.gameState.MyShip.GetEquippedItem(current.Type)));
      }
    }
  }

  protected override void OnHide()
  {
    this._isClosing = true;
    this._currentItemList.Clear();
    this.itemScrollView.ClearData();
    this._ship = (Ship) null;
    this.equipmentPointsLabel.text = string.Empty;
    this.inventorySlotsLabel.text = string.Empty;
    this.bankSlotsLabel.text = string.Empty;
    this.equippedFilterToggle.value = true;
  }

  private void UpdateInventoryItems()
  {
    this._currentItemList.Clear();
    if (this._ship == null)
      return;
    this._currentItemList.AddRange((IEnumerable<EquipmentItem>) this._ship.get_Inventory());
    this._currentItemActionContext = this._ship != TibProxy.gameState.MyShip ? EquipmentItemActionContext.InventoryOther : EquipmentItemActionContext.InventorySelf;
  }

  private void UpdateBankItems()
  {
    this._currentItemList.Clear();
    if (TibProxy.gameState == null || TibProxy.gameState.Bank == null)
      return;
    this._currentItemList.AddRange(TibProxy.gameState.Bank.get_Items());
    this._currentItemActionContext = EquipmentItemActionContext.Bank;
  }

  private void UpdateEquippedItems()
  {
    this._currentItemList.Clear();
    if (this._ship == null)
      return;
    EquipmentItem equippedItem1 = this._ship.GetEquippedItem(ItemType.ENGINE);
    EquipmentItem equippedItem2 = this._ship.GetEquippedItem(ItemType.WEAPON);
    EquipmentItem equippedItem3 = this._ship.GetEquippedItem(ItemType.COMPUTER);
    EquipmentItem equippedItem4 = this._ship.GetEquippedItem(ItemType.ARMOR);
    EquipmentItem equippedItem5 = this._ship.GetEquippedItem(ItemType.HARVESTER);
    EquipmentItem equippedItem6 = this._ship.GetEquippedItem(ItemType.STORAGE);
    EquipmentItem equippedItem7 = this._ship.GetEquippedItem(ItemType.SPECIAL);
    if (equippedItem1 != null)
      this._currentItemList.Add(equippedItem1);
    if (equippedItem2 != null)
      this._currentItemList.Add(equippedItem2);
    if (equippedItem3 != null)
      this._currentItemList.Add(equippedItem3);
    if (equippedItem4 != null)
      this._currentItemList.Add(equippedItem4);
    if (equippedItem5 != null)
      this._currentItemList.Add(equippedItem5);
    if (equippedItem6 != null)
      this._currentItemList.Add(equippedItem6);
    if (equippedItem7 != null)
      this._currentItemList.Add(equippedItem7);
    this._currentItemActionContext = this._ship != TibProxy.gameState.MyShip ? EquipmentItemActionContext.EquippedOther : EquipmentItemActionContext.EquippedSelf;
  }

  private void UpdateFilterLabels()
  {
    if (this._ship == null)
      return;
    this.equipmentPointsLabel.text = string.Format("{0} of {1} EquipPoints", (object) this._ship.UsedEP, (object) this._ship.MaxEP);
    if (TibProxy.gameState.MyShip == this._ship)
    {
      this.inventorySlotsLabel.text = string.Format("{0} of {1} Inventory Slots", (object) this._ship.get_Inventory().Count, (object) TibProxy.gameState.Buyables.InventoryCap);
      this.bankSlotsLabel.text = string.Format("{0} of {1} Bank Slots", (object) TibProxy.gameState.Bank.ItemCount, (object) TibProxy.gameState.Buyables.BankCap);
    }
    else
      this.inventorySlotsLabel.text = string.Format("{0} Items", (object) this._ship.get_Inventory().Count);
  }

  private void Update()
  {
    if (this._isClosing || this._ship == null)
      return;
    this.shipStats.UpdateVisuals(this._ship);
    switch (this._currentItemActionContext)
    {
      case EquipmentItemActionContext.InventorySelf:
      case EquipmentItemActionContext.InventoryOther:
        this.UpdateInventoryItems();
        break;
      case EquipmentItemActionContext.EquippedSelf:
      case EquipmentItemActionContext.EquippedOther:
        this.UpdateEquippedItems();
        break;
      case EquipmentItemActionContext.Bank:
        this.UpdateBankItems();
        break;
      default:
        this.UpdateInventoryItems();
        break;
    }
    this.UpdateFilterLabels();
    this.itemScrollView.SetData(this._currentItemList, this._currentItemActionContext);
  }

  public enum DisplayMode
  {
    EquippedItems,
    Inventory,
  }
}
