﻿// Decompiled with JetBrains decompiler
// Type: PlanetDialogShipFactoryView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class PlanetDialogShipFactoryView : MonoBehaviour
{
  public UIGrid buttonGrid;
  public SBUIButton buyWithCredits;
  public SBUIButton buyWithBlackDollars;
  public SBUIButton buyWithRewardPoints;
  public SBUIButton buyWithCombatPoints;
  public SBUIButton useShip;
  public SBUIButton showTerraformingView;
  public UILabel creditValueLabel;
  public UILabel blackDollarValueLabel;
  public UILabel rewardPointValueLabel;
  public UILabel combatPointValueLabel;
  public Color canPurchaseWithCurrencyColor;
  public Color canNotAffordColor;
  public Color notPurchasableWithCurrencyColor;
  public ShipFactoryShipStatsCard shipStats;
  private GameObject _btnUseShipGo;
  private GameObject _btnBuyWithCreditsGo;
  private GameObject _btnBuyWithBlackDollarsGo;
  private GameObject _btnBuyWithCombatPointsGo;
  private GameObject _btnBuyWithRewardPointsGo;
  private SBUIButtonComponent _btnBuyWithCreditsBorder;
  private SBUIButtonComponent _btnBuyWithBlackDollarsBorder;
  private SBUIButtonComponent _btnBuyWithCombatPointsBorder;
  private SBUIButtonComponent _btnBuyWithRewardPointsBorder;

  public PlanetDialogContext dialogContext { get; set; }

  public void BuyWithCredits()
  {
    TibProxy.gameState.DoBuyShip(this.dialogContext.selectedShipClass, CurrencyType.Credits);
  }

  public void BuyWithBlackDollars()
  {
    if (TibProxy.gameState.Bank.BlackDollars < this.dialogContext.selectedShipClass.BlackDollarCost() && this.dialogContext.selectedShipClass.CanBuy(CurrencyType.BlackDollars))
      DialogManager.ShowDialogAsPopup<BlackDollarPopupDialog>().messageText.text = string.Format("That requires {0:##,###} BlackDollars!", (object) this.dialogContext.selectedShipClass.BlackDollarCost());
    else
      TibProxy.gameState.DoBuyShip(this.dialogContext.selectedShipClass, CurrencyType.BlackDollars);
  }

  public void BuyWithRewardPoints()
  {
    TibProxy.gameState.DoBuyShip(this.dialogContext.selectedShipClass, CurrencyType.RewardPoints);
  }

  public void BuyWithCombatPoints()
  {
    TibProxy.gameState.DoBuyShip(this.dialogContext.selectedShipClass, CurrencyType.CombatPoints);
  }

  public void UseShip()
  {
    TibProxy.gameState.DoSwapShips(this.dialogContext.selectedShipClass, (sbyte) 0);
  }

  public void UpdateVisuals()
  {
    if (this.dialogContext.selectedShipClass == ShipClass.None || this.dialogContext.selectedShipClass == ShipClass.NULL || this.dialogContext.selectedShipClass == ShipClass.LAST)
    {
      this.ResetVisuals();
    }
    else
    {
      this.creditValueLabel.text = !this.dialogContext.selectedShipClass.CanBuy(CurrencyType.Credits) ? "N/A" : this.dialogContext.selectedShipClassCreditCostText;
      this.blackDollarValueLabel.text = !this.dialogContext.selectedShipClass.CanBuy(CurrencyType.BlackDollars) ? "N/A" : this.dialogContext.selectedShipClassBlackDollarCost;
      this.rewardPointValueLabel.text = !this.dialogContext.selectedShipClass.CanBuy(CurrencyType.RewardPoints) ? "N/A" : this.dialogContext.selectedShipClassRewardPointCost;
      this.combatPointValueLabel.text = !this.dialogContext.selectedShipClass.CanBuy(CurrencyType.CombatPoints) ? "N/A" : this.dialogContext.selectedShipClassCombatPointCost;
      this.shipStats.UpdateVisuals(this.dialogContext.selectedShipClass);
      this.UpdateButtonVisibility();
    }
  }

  public void ResetVisuals()
  {
    this.dialogContext.dialogSubtitleText = string.Empty;
    this.creditValueLabel.text = string.Empty;
    this.blackDollarValueLabel.text = string.Empty;
    this.combatPointValueLabel.text = string.Empty;
    this.rewardPointValueLabel.text = string.Empty;
    this.shipStats.ResetVisuals();
  }

  private void Awake()
  {
    this._btnUseShipGo = this.useShip.gameObject;
    this._btnBuyWithCreditsGo = this.buyWithCredits.gameObject;
    this._btnBuyWithBlackDollarsGo = this.buyWithBlackDollars.gameObject;
    this._btnBuyWithCombatPointsGo = this.buyWithCombatPoints.gameObject;
    this._btnBuyWithRewardPointsGo = this.buyWithRewardPoints.gameObject;
    this._btnBuyWithCreditsBorder = this.buyWithCredits.GetButtonComponentForObject("Border");
    this._btnBuyWithBlackDollarsBorder = this.buyWithBlackDollars.GetButtonComponentForObject("Border");
    this._btnBuyWithCombatPointsBorder = this.buyWithCombatPoints.GetButtonComponentForObject("Border");
    this._btnBuyWithRewardPointsBorder = this.buyWithRewardPoints.GetButtonComponentForObject("Border");
    this.ResetVisuals();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
  }

  public void Hide()
  {
    this.ResetVisuals();
    this.gameObject.SetActive(false);
  }

  private void UpdateButtonVisibility()
  {
    IGameState gameState = TibProxy.gameState;
    if (gameState == null)
      return;
    if (this.dialogContext.selectedShipClass == ShipClass.NULL || this.dialogContext.selectedShipClass == ShipClass.None || this.dialogContext.selectedShipClass == ShipClass.LAST)
    {
      NGUITools.SetActiveSelf(this._btnUseShipGo, false);
      NGUITools.SetActiveSelf(this._btnBuyWithCreditsGo, false);
      NGUITools.SetActiveSelf(this._btnBuyWithBlackDollarsGo, false);
      NGUITools.SetActiveSelf(this._btnBuyWithCombatPointsGo.gameObject, false);
      NGUITools.SetActiveSelf(this._btnBuyWithRewardPointsGo.gameObject, false);
    }
    else
    {
      if (gameState.Ships[this.dialogContext.selectedShipClass])
      {
        NGUITools.SetActiveSelf(this._btnUseShipGo, this.dialogContext.selectedShipClass != TibProxy.gameState.MyShip.Class);
        NGUITools.SetActiveSelf(this._btnBuyWithCreditsGo, false);
        NGUITools.SetActiveSelf(this._btnBuyWithBlackDollarsGo, false);
        NGUITools.SetActiveSelf(this._btnBuyWithCombatPointsGo.gameObject, false);
        NGUITools.SetActiveSelf(this._btnBuyWithRewardPointsGo.gameObject, false);
      }
      else
      {
        bool isBuyable1 = this.dialogContext.selectedShipClass.CanBuy(CurrencyType.Credits);
        bool isBuyable2 = this.dialogContext.selectedShipClass.CanBuy(CurrencyType.BlackDollars);
        bool isBuyable3 = this.dialogContext.selectedShipClass.CanBuy(CurrencyType.RewardPoints);
        bool isBuyable4 = this.dialogContext.selectedShipClass.CanBuy(CurrencyType.CombatPoints);
        bool canPurchase1 = TibProxy.gameState.CanBuyShip(this.dialogContext.selectedShipClass, CurrencyType.Credits);
        TibProxy.gameState.CanBuyShip(this.dialogContext.selectedShipClass, CurrencyType.BlackDollars);
        bool canPurchase2 = TibProxy.gameState.CanBuyShip(this.dialogContext.selectedShipClass, CurrencyType.CombatPoints);
        bool canPurchase3 = TibProxy.gameState.CanBuyShip(this.dialogContext.selectedShipClass, CurrencyType.RewardPoints);
        this.SetCurrencyButtonState(isBuyable1, canPurchase1, this.buyWithCredits, this._btnBuyWithCreditsBorder);
        this.SetCurrencyButtonState(isBuyable2, true, this.buyWithBlackDollars, this._btnBuyWithBlackDollarsBorder);
        this.SetCurrencyButtonState(isBuyable4, canPurchase2, this.buyWithCombatPoints, this._btnBuyWithCombatPointsBorder);
        this.SetCurrencyButtonState(isBuyable3, canPurchase3, this.buyWithRewardPoints, this._btnBuyWithRewardPointsBorder);
        NGUITools.SetActiveSelf(this._btnUseShipGo, false);
        NGUITools.SetActiveSelf(this._btnBuyWithCreditsGo, true);
        NGUITools.SetActiveSelf(this._btnBuyWithBlackDollarsGo, true);
        NGUITools.SetActiveSelf(this._btnBuyWithCombatPointsGo.gameObject, true);
        NGUITools.SetActiveSelf(this._btnBuyWithRewardPointsGo.gameObject, true);
      }
      NGUITools.SetActiveSelf(this.showTerraformingView.gameObject, !this.dialogContext.planet.IsEarth);
      this.buttonGrid.Reposition();
    }
  }

  private void SetCurrencyButtonState(bool isBuyable, bool canPurchase, SBUIButton button, SBUIButtonComponent btnBorderComponent)
  {
    if (isBuyable)
    {
      if (canPurchase)
      {
        if (btnBorderComponent.normal.color == this.canPurchaseWithCurrencyColor && btnBorderComponent.hover.color == this.canPurchaseWithCurrencyColor && btnBorderComponent.pressed.color == this.canPurchaseWithCurrencyColor)
          return;
        btnBorderComponent.normal.color = this.canPurchaseWithCurrencyColor;
        btnBorderComponent.hover.color = this.canPurchaseWithCurrencyColor;
        btnBorderComponent.pressed.color = this.canPurchaseWithCurrencyColor;
        button.SetState(SBUIButtonComponent.State.Normal);
      }
      else
      {
        if (btnBorderComponent.normal.color == this.canNotAffordColor && btnBorderComponent.hover.color == this.canNotAffordColor && btnBorderComponent.pressed.color == this.canNotAffordColor)
          return;
        btnBorderComponent.normal.color = this.canNotAffordColor;
        btnBorderComponent.hover.color = this.canNotAffordColor;
        btnBorderComponent.pressed.color = this.canNotAffordColor;
        button.SetState(SBUIButtonComponent.State.Normal);
      }
    }
    else
    {
      if (btnBorderComponent.normal.color == this.notPurchasableWithCurrencyColor && btnBorderComponent.hover.color == this.notPurchasableWithCurrencyColor && btnBorderComponent.pressed.color == this.notPurchasableWithCurrencyColor)
        return;
      btnBorderComponent.normal.color = this.notPurchasableWithCurrencyColor;
      btnBorderComponent.hover.color = this.notPurchasableWithCurrencyColor;
      btnBorderComponent.pressed.color = this.notPurchasableWithCurrencyColor;
      button.SetState(SBUIButtonComponent.State.Normal);
    }
  }

  private void Update()
  {
    if (TibProxy.gameState == null)
      return;
    this.UpdateVisuals();
  }
}
