﻿// Decompiled with JetBrains decompiler
// Type: UIMusicVolume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UISlider))]
public class UIMusicVolume : MonoBehaviour
{
  private UISlider mSlider;

  private void Awake()
  {
    this.mSlider = this.GetComponent<UISlider>();
    this.mSlider.value = Music.volume;
    EventDelegate.Add(this.mSlider.onChange, new EventDelegate.Callback(this.OnChange));
  }

  private void OnChange()
  {
    Music.volume = UIProgressBar.current.value;
  }
}
