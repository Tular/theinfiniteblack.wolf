﻿// Decompiled with JetBrains decompiler
// Type: UIPauseButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIPauseButton : MonoBehaviour
{
  public bool animateWhenPaused = true;
  private Vector3 mFrom = new Vector3(1f, 1f, 1f);
  private Vector3 mTo = new Vector3(1.25f, 1.25f, 1.25f);
  private Transform mTrans;
  private bool mPaused;

  private void Awake()
  {
    this.mTrans = this.transform;
  }

  private void Start()
  {
    if (GameManager.gameType != GameManager.GameType.Multiplayer)
      return;
    NGUITools.SetActiveChildren(this.gameObject, false);
  }

  private void OnClick()
  {
    if (this.mPaused)
    {
      this.mPaused = false;
      GameManager.Unpause();
    }
    else
    {
      if (GameManager.gameType == GameManager.GameType.Multiplayer)
        return;
      this.mPaused = true;
      GameManager.Pause();
    }
  }

  private void Update()
  {
    if (!this.animateWhenPaused)
      return;
    if (this.mPaused || (double) Time.timeScale < 0.100000001490116)
      this.mTrans.localScale = Vector3.Lerp(this.mTrans.localScale, Vector3.Lerp(this.mFrom, this.mTo, Mathf.PingPong(Time.realtimeSinceStartup, 0.5f)), 1f - Time.timeScale);
    else
      this.mTrans.localScale = this.mFrom;
  }
}
