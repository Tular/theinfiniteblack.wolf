﻿// Decompiled with JetBrains decompiler
// Type: ReporterMessageReceiver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ReporterMessageReceiver : MonoBehaviour
{
  private Reporter reporter;
  private UICamera _mainUiCamera;
  private Camera _mainCamera;
  private int _mainCameraCullingMask;

  private void Start()
  {
    this.reporter = this.gameObject.GetComponent<Reporter>();
  }

  private void OnPreStart()
  {
    if ((Object) this.reporter == (Object) null)
      this.reporter = this.gameObject.GetComponent<Reporter>();
    if (Screen.width < 1000)
      this.reporter.size = new Vector2(32f, 32f);
    else
      this.reporter.size = new Vector2(48f, 48f);
  }

  private void OnHideReporter()
  {
    EasyTouch.instance.enable = true;
    this._mainCamera.cullingMask = this._mainCameraCullingMask;
    this._mainCamera = (Camera) null;
    this._mainUiCamera = (UICamera) null;
  }

  private void OnShowReporter()
  {
    EasyTouch.instance.enable = false;
    this._mainCamera = UICamera.mainCamera;
    this._mainUiCamera = this._mainCamera.GetComponent<UICamera>();
    this._mainCameraCullingMask = this._mainCamera.cullingMask;
    this._mainCamera.cullingMask = 0;
  }

  private void OnLog(Reporter.Log log)
  {
  }
}
