﻿// Decompiled with JetBrains decompiler
// Type: InlineWebBrowser
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class InlineWebBrowser : DialogBase
{
  public GameObject webView;
  public UIWidget dialogContent;
  public UIWidget fullScreenToolbar;
  public UIWidget windowedToolbar;
  public UIWidget webViewSizer;
  private string _url;
  private bool _isFullScreen;
  private int _screenWidth;
  private int _screenHeight;
  private bool _browserIsShowing;

  public bool isFullScreen
  {
    get
    {
      return this._isFullScreen;
    }
    set
    {
      this._isFullScreen = value;
      this.fullScreenToolbar.gameObject.SetActive(this._isFullScreen);
      this.windowedToolbar.gameObject.SetActive(!this._isFullScreen);
      if (this._isFullScreen)
      {
        this.dialogContent.topAnchor.relative = 1f;
        this.dialogContent.topAnchor.absolute = 0;
        this.dialogContent.bottomAnchor.relative = 0.0f;
        this.dialogContent.bottomAnchor.absolute = 0;
        this.dialogContent.UpdateAnchors();
        this.webViewSizer.topAnchor.absolute = -(this.fullScreenToolbar.height + 10);
      }
      else
      {
        this.dialogContent.topAnchor.relative = 1f;
        this.dialogContent.topAnchor.absolute = -29;
        this.dialogContent.bottomAnchor.relative = 0.1f;
        this.dialogContent.bottomAnchor.absolute = 0;
        this.dialogContent.UpdateAnchors();
        this.webViewSizer.topAnchor.absolute = -5;
      }
    }
  }

  public static void ShowFullScreen(string pUrl)
  {
    Application.OpenURL(pUrl);
  }

  public static void Show(string pUrl)
  {
    Application.OpenURL(pUrl);
  }

  public void BrowserBack()
  {
  }

  public void BrowserForward()
  {
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this.isFullScreen = false;
  }

  protected override void OnShow()
  {
    base.OnShow();
  }

  protected override void OnHide()
  {
    this.isFullScreen = false;
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
