﻿// Decompiled with JetBrains decompiler
// Type: PinchMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PinchMe : MonoBehaviour
{
  private TextMesh textMesh;

  private void OnEnable()
  {
    EasyTouch.On_TouchStart2Fingers += new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    EasyTouch.On_PinchIn += new EasyTouch.PinchInHandler(this.On_PinchIn);
    EasyTouch.On_PinchOut += new EasyTouch.PinchOutHandler(this.On_PinchOut);
    EasyTouch.On_PinchEnd += new EasyTouch.PinchEndHandler(this.On_PinchEnd);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    EasyTouch.On_PinchIn -= new EasyTouch.PinchInHandler(this.On_PinchIn);
    EasyTouch.On_PinchOut -= new EasyTouch.PinchOutHandler(this.On_PinchOut);
    EasyTouch.On_PinchEnd -= new EasyTouch.PinchEndHandler(this.On_PinchEnd);
  }

  private void Start()
  {
    this.textMesh = this.GetComponentInChildren<TextMesh>();
  }

  private void On_TouchStart2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    EasyTouch.SetEnableTwist(false);
    EasyTouch.SetEnablePinch(true);
  }

  private void On_PinchIn(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    float num = Time.deltaTime * gesture.deltaPinch;
    Vector3 localScale = this.transform.localScale;
    this.transform.localScale = new Vector3(localScale.x - num, localScale.y - num, localScale.z - num);
    this.textMesh.text = "Delta pinch : " + gesture.deltaPinch.ToString();
  }

  private void On_PinchOut(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    float num = Time.deltaTime * gesture.deltaPinch;
    Vector3 localScale = this.transform.localScale;
    this.transform.localScale = new Vector3(localScale.x + num, localScale.y + num, localScale.z + num);
    this.textMesh.text = "Delta pinch : " + gesture.deltaPinch.ToString();
  }

  private void On_PinchEnd(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
    EasyTouch.SetEnableTwist(true);
    this.textMesh.text = "Pinch me";
  }
}
