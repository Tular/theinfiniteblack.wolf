﻿// Decompiled with JetBrains decompiler
// Type: UIOptionWifi
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIToggle))]
public class UIOptionWifi : MonoBehaviour
{
  public UILabel info;

  private void OnClick()
  {
    this.info.text = Localization.Get("Carrier Info");
  }

  private void Awake()
  {
    NGUITools.SetActive(this.gameObject, false);
  }
}
