﻿// Decompiled with JetBrains decompiler
// Type: ItemSelectPopupItemListingCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using System.Collections.Generic;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

[RequireComponent(typeof (SBUITab))]
public class ItemSelectPopupItemListingCard : GeneralScrollViewCardBase
{
  public string resourcePath = "items";
  public UITexture icon;
  public UILabel title;
  public UILabel subtitle;
  private EquipmentItem _item;
  public System.Action<EquipmentItem> setSelectedItem;
  private SBUITab _toggle;

  public EquipmentItem item
  {
    get
    {
      return this._item;
    }
    set
    {
      this._item = value;
      if (this._item != null)
      {
        StringBuilder stringBuilder1 = new StringBuilder(200);
        StringBuilder stringBuilder2 = new StringBuilder(200);
        this._item.AppendName(stringBuilder1);
        this._item.AppendSubTitle(stringBuilder2);
        Markup.GetNGUI(stringBuilder1);
        Markup.GetNGUI(stringBuilder2);
        this.title.text = stringBuilder1.ToString();
        this.subtitle.text = stringBuilder2.ToString();
        this.LoadItemTexture(this._item.Icon);
      }
      else
      {
        this.title.text = string.Empty;
        this.subtitle.text = string.Empty;
        this.icon.mainTexture = (Texture) null;
      }
    }
  }

  public int toggleGroup
  {
    get
    {
      return this._toggle.toggleGroup;
    }
    set
    {
      this._toggle.toggleGroup = value;
    }
  }

  public List<EventDelegate> onSelected
  {
    get
    {
      return this._toggle.onClick;
    }
  }

  public override void Refresh()
  {
    throw new NotImplementedException();
  }

  private void LoadItemTexture(ItemIcon itemIcon)
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) itemIcon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((UnityEngine.Object) texture2D == (UnityEngine.Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.icon.mainTexture = (Texture) texture2D;
  }

  private void OnToggleClicked()
  {
    if (!((UnityEngine.Object) UIToggle.current != (UnityEngine.Object) null) || !UIToggle.current.value || this.setSelectedItem == null)
      return;
    this.setSelectedItem(this.item);
  }

  private void Awake()
  {
    this._toggle = this.GetComponent<SBUITab>();
    EventDelegate.Add(this._toggle.onClick, new EventDelegate.Callback(this.OnToggleClicked));
    this.item = (EquipmentItem) null;
  }

  private void OnSpawned()
  {
    this.LogD(string.Format("ItemSelectPopupItemListingCard.OnSpawned()"));
    this._toggle.ResetToggle();
  }

  private void OnDespawned()
  {
    this.LogD(string.Format("ItemSelectPopupItemListingCard.OnDespawned()"));
    this.item = (EquipmentItem) null;
    this.setSelectedItem = (System.Action<EquipmentItem>) null;
  }
}
