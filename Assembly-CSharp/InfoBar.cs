﻿// Decompiled with JetBrains decompiler
// Type: InfoBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using UnityEngine;

public class InfoBar : MonoBehaviour
{
  private readonly Color _hullYellow = new Color(1f, 1f, 0.0f);
  private readonly Color _hullOrange = new Color(1f, 0.68f, 0.0f);
  private readonly Color _hullRed = new Color(1f, 0.0f, 0.0f);
  public UILabel creditsLabel;
  public UILabel blackDollarsLabel;
  public UILabel metalsLabel;
  public UILabel organicsLabel;
  public UILabel gasLabel;
  public UILabel raidoactivesLabel;
  public UILabel darkMatterLabel;
  public UILabel hullLabel;

  private void UpdateVisuals()
  {
    this.creditsLabel.text = string.Format("{0:n0}", (object) TibProxy.gameState.Bank.Credits);
    this.blackDollarsLabel.text = string.Format("{0:n0}", (object) TibProxy.gameState.Bank.BlackDollars);
    this.metalsLabel.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Metals);
    this.organicsLabel.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Organics);
    this.gasLabel.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Gas);
    this.raidoactivesLabel.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Radioactives);
    this.darkMatterLabel.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.DarkMatter);
    float hull = (float) TibProxy.gameState.MyShip.Hull;
    float maxHull = (float) TibProxy.gameState.MyShip.MaxHull;
    float num = (float) (100.0 * ((double) hull / (double) maxHull));
    if ((double) num >= 90.0)
      this.hullLabel.color = Color.white;
    else if ((double) num >= 50.0)
      this.hullLabel.color = this._hullYellow;
    else if ((double) num >= 20.0)
      this.hullLabel.color = this._hullOrange;
    else
      this.hullLabel.color = this._hullRed;
    this.hullLabel.text = string.Format("{0:n0}", (object) hull);
  }

  private void Start()
  {
  }

  private void Update()
  {
    if (TibProxy.gameState == null || TibProxy.gameState.MyShip == null)
      return;
    this.UpdateVisuals();
  }
}
