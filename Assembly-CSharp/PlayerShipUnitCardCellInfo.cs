﻿// Decompiled with JetBrains decompiler
// Type: PlayerShipUnitCardCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public class PlayerShipUnitCardCellInfo : CombatUnitCardCellInfoBase<Ship, PlayerShipUnitCardCell>
{
  public PlayerShipUnitCardCellInfo(Transform prefab, Ship data)
    : base(prefab, data)
  {
  }

  protected override void OnUpdateCellVisuals()
  {
    base.OnUpdateCellVisuals();
    this.mCellInstance.level = this.mCellData.Level;
    this.mCellInstance.shipClass = this.mCellData.Class;
    this.mCellInstance.isPvP = this.mCellData.PvPFlag;
    this.mCellInstance.isSpecialVisible = false;
  }
}
