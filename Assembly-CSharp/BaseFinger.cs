﻿// Decompiled with JetBrains decompiler
// Type: BaseFinger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BaseFinger
{
  public int fingerIndex;
  public int touchCount;
  public Vector2 startPosition;
  public Vector2 position;
  public Vector2 deltaPosition;
  public float actionTime;
  public float deltaTime;
  public Camera pickedCamera;
  public GameObject pickedObject;
  public bool isGuiCamera;
  public bool isOverGui;
  public GameObject pickedUIElement;

  public Gesture GetGesture()
  {
    Gesture gesture = new Gesture();
    gesture.fingerIndex = this.fingerIndex;
    gesture.touchCount = this.touchCount;
    gesture.startPosition = this.startPosition;
    gesture.position = this.position;
    gesture.deltaPosition = this.deltaPosition;
    gesture.actionTime = this.actionTime;
    gesture.deltaTime = this.deltaTime;
    gesture.isOverGui = this.isOverGui;
    gesture.pickedCamera = this.pickedCamera;
    gesture.pickedObject = this.pickedObject;
    gesture.isGuiCamera = this.isGuiCamera;
    gesture.pickedUIElement = this.pickedUIElement;
    return gesture;
  }
}
