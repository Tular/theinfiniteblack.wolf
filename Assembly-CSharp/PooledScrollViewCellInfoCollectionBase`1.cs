﻿// Decompiled with JetBrains decompiler
// Type: PooledScrollViewCellInfoCollectionBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Unity;
using UnityEngine;

public abstract class PooledScrollViewCellInfoCollectionBase<T> : IPooledScrollViewCellInifoCollection<T> where T : class, IScrollViewCellInfo
{
  protected readonly IList<T> mInfoList = (IList<T>) new List<T>();
  protected readonly Dictionary<object, T> mInfoLookup = new Dictionary<object, T>();
  private bool _dataWasReset;
  private int _dataResetFrame;
  private bool _dataWasModified;
  private int _dataModifiedFrame;
  private List<T> _evaluatedValues;
  private int _lastFrameEvaluated;

  protected PooledScrollViewCellInfoCollectionBase()
  {
    this._lastFrameEvaluated = int.MinValue;
  }

  public int Count
  {
    get
    {
      return this.mInfoList.Count;
    }
  }

  public virtual bool dataWasReset
  {
    get
    {
      this.EvaluateValuesExpression();
      return this._dataResetFrame == this._lastFrameEvaluated;
    }
    protected set
    {
      this._dataWasReset = value;
      if (!this._dataWasReset)
        return;
      this._dataWasModified = true;
    }
  }

  public virtual bool dataWasModified
  {
    get
    {
      this.EvaluateValuesExpression();
      return this._dataModifiedFrame == this._lastFrameEvaluated;
    }
    set
    {
      this._dataWasModified = value;
    }
  }

  public virtual T first
  {
    get
    {
      return this.values.FirstOrDefault<T>();
    }
  }

  public virtual T last
  {
    get
    {
      return this.values.LastOrDefault<T>();
    }
  }

  private void EvaluateValuesExpression()
  {
    if (this._lastFrameEvaluated != Time.frameCount && this._dataWasModified)
    {
      this._evaluatedValues = this.GetValues().ToList<T>();
      if (this._dataWasModified)
        this._dataModifiedFrame = Time.frameCount;
      if (this._dataWasReset)
        this._dataResetFrame = Time.frameCount;
      this._dataWasModified = false;
      this._dataWasReset = false;
    }
    this._lastFrameEvaluated = Time.frameCount;
  }

  public IEnumerable<T> values
  {
    get
    {
      this.EvaluateValuesExpression();
      return (IEnumerable<T>) this._evaluatedValues;
    }
  }

  protected abstract IEnumerable<T> GetValues();

  public virtual void Add(T cellInfo)
  {
    if (this.mInfoLookup.ContainsKey(cellInfo.cellData))
      return;
    this.mInfoList.Add(cellInfo);
    this.mInfoLookup.Add(cellInfo.cellData, cellInfo);
    this.dataWasModified = true;
  }

  public virtual void Remove(T cellInfo)
  {
    if (!this.mInfoLookup.ContainsKey(cellInfo.cellData))
      return;
    if (TibProxy.Instance.isInDebugMode)
      this.CheckConsistency(cellInfo);
    this.mInfoLookup.Remove(cellInfo.cellData);
    this.mInfoList.Remove(cellInfo);
    this.dataWasModified = true;
  }

  public virtual void Clear()
  {
    this.mInfoLookup.Clear();
    this.mInfoList.Clear();
    this.dataWasReset = true;
  }

  private void CheckConsistency(T cellInfo)
  {
    bool flag1 = this.mInfoList.Contains(cellInfo);
    bool flag2 = this.mInfoLookup.ContainsKey(cellInfo.cellData);
    if (flag1 && !flag2)
    {
      PooledScrollViewCellInfoCollectionBase<T>.InconsistentDataException inconsistentDataException = new PooledScrollViewCellInfoCollectionBase<T>.InconsistentDataException("cellInfo exists in _infoList but not in _infoLookup");
      TheInfiniteBlack.Library.Log.E((object) this, "Add(IScrollListCellInfo)", (Exception) inconsistentDataException);
      throw inconsistentDataException;
    }
    if (flag2 && !flag1)
    {
      PooledScrollViewCellInfoCollectionBase<T>.InconsistentDataException inconsistentDataException = new PooledScrollViewCellInfoCollectionBase<T>.InconsistentDataException("cellInfo exists in _infoLookup but not in _infoList");
      TheInfiniteBlack.Library.Log.E((object) this, "Add(IScrollListCellInfo)", (Exception) inconsistentDataException);
      throw inconsistentDataException;
    }
  }

  public class InconsistentDataException : Exception
  {
    public InconsistentDataException(string message)
      : base(message)
    {
    }
  }
}
