﻿// Decompiled with JetBrains decompiler
// Type: EventLogScrollViewTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class EventLogScrollViewTester : MonoBehaviour
{
  public EventLogScrollView scrollView;
  public Transform prefEventLogEntry;
  public string sender;
  public string receiver;
  public string chatLine1;
  public string chatLine2;
  public ChatType chatTypeToAdd;
  public int messageCount;

  [ContextMenu("Add Chat Event")]
  public void OnAddChatEvent()
  {
    ChatEventArgs chatEventArgs = new ChatEventArgs(TibProxy.gameState, string.Format("{0}: {1}", (object) this.messageCount, !string.IsNullOrEmpty(this.chatLine2) ? (object) string.Format("{0}\n{1}", (object) this.chatLine1, (object) this.chatLine2) : (object) this.chatLine1), this.receiver, this.sender, this.chatTypeToAdd);
    ++this.messageCount;
    this.chatTypeToAdd = ChatType.EVENT;
  }
}
