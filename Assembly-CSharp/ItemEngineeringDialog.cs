﻿// Decompiled with JetBrains decompiler
// Type: ItemEngineeringDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class ItemEngineeringDialog : DialogBase
{
  public EngineeringItemListingView itemListingView;
  public ItemEngineeringView itemEngineeringView;
  private int _initialPanelDepth;

  public ItemEngineeringDialogContext dialogContext { get; private set; }

  protected override bool ProcessArgs(object[] args)
  {
    if (args.Length != 1)
      throw new ArgumentException("Expected args length is 1");
    this.panelDepth = UIPanel.nextUnusedDepth;
    this.dialogContext.selectedInventoryItem = (EquipmentItem) args[0];
    return false;
  }

  public void ShowItemListingView()
  {
    this.dialogContext.selectedInventoryItem = (EquipmentItem) null;
    NGUITools.SetActiveSelf(this.itemEngineeringView.gameObject, false);
    NGUITools.SetActiveSelf(this.itemListingView.gameObject, true);
  }

  public void ShowItemEngineeringView()
  {
    NGUITools.SetActiveSelf(this.itemListingView.gameObject, false);
    NGUITools.SetActiveSelf(this.itemEngineeringView.gameObject, true);
  }

  protected override void OnShow()
  {
    base.OnShow();
    this.ShowItemEngineeringView();
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.dialogContext.selectedInventoryItem = (EquipmentItem) null;
    Resources.UnloadUnusedAssets();
    this.panel.depth = this._initialPanelDepth;
    NGUITools.SetActiveSelf(this.itemEngineeringView.gameObject, false);
    NGUITools.SetActiveSelf(this.itemListingView.gameObject, false);
    Resources.UnloadUnusedAssets();
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this._initialPanelDepth = this.panel.depth;
    this.dialogContext = new ItemEngineeringDialogContext(this.panel);
    EventDelegate.Add(this.dialogContext.onSelectedInventoryItemChanged, new EventDelegate.Callback(this.ShowItemEngineeringView), false);
    NGUITools.SetActiveSelf(this.itemEngineeringView.gameObject, false);
    NGUITools.SetActiveSelf(this.itemListingView.gameObject, false);
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
