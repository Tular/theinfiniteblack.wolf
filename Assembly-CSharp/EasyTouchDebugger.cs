﻿// Decompiled with JetBrains decompiler
// Type: EasyTouchDebugger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class EasyTouchDebugger : MonoBehaviour
{
  public GameObject prefSpot;
  public GameObject prefGreenRing;
  public GameObject prefRedRing;
  public GameObject prefBlueRing;
  public GameObject prefTrail;
  public GameObject prefGreenGlowDisk;
  public GameObject prefRedGlowDisk;
  public GameObject prefBlueGlowDisk;
  public Camera cam;
  public UITextList output;
  public UILabel finger1Info;
  public UILabel finger2Info;
  private GameObject _trail_1;
  private GameObject _trail_2;
  private GameObject _finger_1_ring;
  private GameObject _finger_2_ring;
  private GameObject _finger_3_ring;
  private GameObject _finger_1_disk;
  private GameObject _finger_2_disk;
  private GameObject _finger_3_disk;
  private bool _swipeEnding;
  private bool _swipeStarted;
  private Vector2 _dragMomentum;
  private Vector3 _currentWorldSwipePoint;

  private void OnEnable()
  {
    this.RegisterTouchHandlers();
  }

  private void OnDisable()
  {
    this.UnregisterTouchHandlers();
  }

  private void Update()
  {
  }

  private void FallthroughNGUIEventHandler_onMouseScrollDelegate(object sender, MouseScrollWheelEventArgs e)
  {
  }

  private void RegisterTouchHandlers()
  {
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.EasyTouch_On_TouchStart);
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.EasyTouch_On_TouchStart);
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.EasyTouch_On_TouchDown);
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.EasyTouch_On_TouchDown);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.EasyTouch_On_TouchUp);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.EasyTouch_On_TouchUp);
    EasyTouch.On_TouchDown2Fingers -= new EasyTouch.TouchDown2FingersHandler(this.EasyTouch_On_TouchDown2Fingers);
    EasyTouch.On_TouchDown2Fingers += new EasyTouch.TouchDown2FingersHandler(this.EasyTouch_On_TouchDown2Fingers);
    EasyTouch.On_TouchUp2Fingers -= new EasyTouch.TouchUp2FingersHandler(this.EasyTouch_On_TouchUp2Fingers);
    EasyTouch.On_TouchUp2Fingers += new EasyTouch.TouchUp2FingersHandler(this.EasyTouch_On_TouchUp2Fingers);
    EasyTouch.On_DragStart -= new EasyTouch.DragStartHandler(this.EasyTouch_On_DragStart);
    EasyTouch.On_DragStart += new EasyTouch.DragStartHandler(this.EasyTouch_On_DragStart);
    EasyTouch.On_Drag -= new EasyTouch.DragHandler(this.EasyTouch_On_Drag);
    EasyTouch.On_Drag += new EasyTouch.DragHandler(this.EasyTouch_On_Drag);
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
    EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
    EasyTouch.On_DragEnd -= new EasyTouch.DragEndHandler(this.EasyTouch_On_DragEnd);
    EasyTouch.On_DragEnd += new EasyTouch.DragEndHandler(this.EasyTouch_On_DragEnd);
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.EasyTouch_On_Swipe);
    EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.EasyTouch_On_Swipe);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
    EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
    EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_On_TouchStart2Fingers);
    EasyTouch.On_TouchStart2Fingers += new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_On_TouchStart2Fingers);
    EasyTouch.On_PinchIn -= new EasyTouch.PinchInHandler(this.EasyTouch_On_PinchIn);
    EasyTouch.On_PinchIn += new EasyTouch.PinchInHandler(this.EasyTouch_On_PinchIn);
    EasyTouch.On_PinchOut -= new EasyTouch.PinchOutHandler(this.EasyTouch_On_PinchOut);
    EasyTouch.On_PinchOut += new EasyTouch.PinchOutHandler(this.EasyTouch_On_PinchOut);
    EasyTouch.On_PinchEnd -= new EasyTouch.PinchEndHandler(this.EasyTouch_On_PinchEnd);
    EasyTouch.On_PinchEnd += new EasyTouch.PinchEndHandler(this.EasyTouch_On_PinchEnd);
    EasyTouch.On_SimpleTap -= new EasyTouch.SimpleTapHandler(this.EasyTouch_On_SimpleTap);
    EasyTouch.On_SimpleTap += new EasyTouch.SimpleTapHandler(this.EasyTouch_On_SimpleTap);
    FallthroughNGUIEventHandler.onMouseScrollEventHandler = new EventHandler<MouseScrollWheelEventArgs>(this.FallthroughNGUIEventHandler_onMouseScrollDelegate);
    EasyTouch.On_Cancel -= new EasyTouch.TouchCancelHandler(this.EzEcho_On_TouchCancel);
    EasyTouch.On_Cancel += new EasyTouch.TouchCancelHandler(this.EzEcho_On_TouchCancel);
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.EzEcho_On_TouchStart);
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.EzEcho_On_TouchStart);
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.EzEcho_On_TouchDown);
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.EzEcho_On_TouchDown);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.EzEcho_On_TouchUp);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.EzEcho_On_TouchUp);
    EasyTouch.On_SimpleTap -= new EasyTouch.SimpleTapHandler(this.EzEcho_On_SimpleTap);
    EasyTouch.On_SimpleTap += new EasyTouch.SimpleTapHandler(this.EzEcho_On_SimpleTap);
    EasyTouch.On_DoubleTap -= new EasyTouch.DoubleTapHandler(this.EzEcho_On_DoubleTap);
    EasyTouch.On_DoubleTap += new EasyTouch.DoubleTapHandler(this.EzEcho_On_DoubleTap);
    EasyTouch.On_LongTapStart -= new EasyTouch.LongTapStartHandler(this.EzEcho_On_LongTapStart);
    EasyTouch.On_LongTapStart += new EasyTouch.LongTapStartHandler(this.EzEcho_On_LongTapStart);
    EasyTouch.On_LongTap -= new EasyTouch.LongTapHandler(this.EzEcho_On_LongTap);
    EasyTouch.On_LongTap += new EasyTouch.LongTapHandler(this.EzEcho_On_LongTap);
    EasyTouch.On_DragStart -= new EasyTouch.DragStartHandler(this.EzEcho_On_DragStart);
    EasyTouch.On_DragStart += new EasyTouch.DragStartHandler(this.EzEcho_On_DragStart);
    EasyTouch.On_Drag -= new EasyTouch.DragHandler(this.EzEcho_On_Drag);
    EasyTouch.On_Drag += new EasyTouch.DragHandler(this.EzEcho_On_Drag);
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EzEcho_On_SwipeStart);
    EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.EzEcho_On_SwipeStart);
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.EzEcho_On_Swipe);
    EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.EzEcho_On_Swipe);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.EzEcho_On_SwipeEnd);
    EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.EzEcho_On_SwipeEnd);
  }

  private void UnregisterTouchHandlers()
  {
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.EasyTouch_On_TouchStart);
    EasyTouch.On_DragStart -= new EasyTouch.DragStartHandler(this.EasyTouch_On_DragStart);
    EasyTouch.On_Drag -= new EasyTouch.DragHandler(this.EasyTouch_On_Drag);
    EasyTouch.On_DragEnd -= new EasyTouch.DragEndHandler(this.EasyTouch_On_DragEnd);
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.EasyTouch_On_Swipe);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
    EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_On_TouchStart2Fingers);
    EasyTouch.On_PinchIn -= new EasyTouch.PinchInHandler(this.EasyTouch_On_PinchIn);
    EasyTouch.On_PinchOut -= new EasyTouch.PinchOutHandler(this.EasyTouch_On_PinchOut);
    EasyTouch.On_PinchEnd -= new EasyTouch.PinchEndHandler(this.EasyTouch_On_PinchEnd);
    EasyTouch.On_SimpleTap -= new EasyTouch.SimpleTapHandler(this.EasyTouch_On_SimpleTap);
    FallthroughNGUIEventHandler.onMouseScrollEventHandler = (EventHandler<MouseScrollWheelEventArgs>) null;
  }

  private void EzEcho_On_TouchCancel(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_TouchCancel - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_TouchStart(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_TouchStart - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_TouchDown(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_TouchDown - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_TouchUp(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_TouchUp - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_SimpleTap(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_SimpleTap - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_DoubleTap(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_DoubleTap - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_LongTapStart(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_LongTapStart - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_LongTap(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_LongTap - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_DragStart(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_DragStart - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_Drag(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_Drag - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_SwipeStart(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_SwipeStart - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_Swipe(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_Swipe - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EzEcho_On_SwipeEnd(Gesture gesture)
  {
    this.output.Add(string.Format("EzEcho_On_SwipeEnd - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
  }

  private void EasyTouch_On_TouchStart(Gesture gesture)
  {
    if (gesture.fingerIndex > 1)
      return;
    this.output.Add(string.Format("EasyTouch_On_TouchStart - FingerIndex: {0} ScreenPositition: {1}", (object) gesture.fingerIndex, (object) gesture.position));
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._finger_1_ring == (UnityEngine.Object) null)
    {
      this._finger_1_ring = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefGreenRing, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.0f)), Quaternion.identity);
      this._finger_1_ring.layer = this.gameObject.layer;
    }
    if (gesture.fingerIndex == 1 && (UnityEngine.Object) this._finger_2_ring == (UnityEngine.Object) null)
    {
      this._finger_2_ring = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefRedRing, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.0f)), Quaternion.identity);
      this._finger_2_ring.layer = this.gameObject.layer;
    }
    if (gesture.fingerIndex == 2 && (UnityEngine.Object) this._finger_3_ring == (UnityEngine.Object) null)
    {
      this._finger_3_ring = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefBlueRing, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.0f)), Quaternion.identity);
      this._finger_3_ring.layer = this.gameObject.layer;
    }
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._finger_1_disk == (UnityEngine.Object) null)
    {
      this._finger_1_disk = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefGreenGlowDisk, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.1f)), Quaternion.identity);
      this._finger_1_disk.layer = this.gameObject.layer;
    }
    if (gesture.fingerIndex == 1 && (UnityEngine.Object) this._finger_2_disk == (UnityEngine.Object) null)
    {
      this._finger_2_disk = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefRedGlowDisk, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.1f)), Quaternion.identity);
      this._finger_2_disk.layer = this.gameObject.layer;
    }
    if (gesture.fingerIndex != 2 || !((UnityEngine.Object) this._finger_3_disk == (UnityEngine.Object) null))
      return;
    this._finger_3_disk = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefBlueGlowDisk, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.1f)), Quaternion.identity);
    this._finger_3_disk.layer = this.gameObject.layer;
  }

  private void EasyTouch_On_TouchDown(Gesture gesture)
  {
    if (gesture.fingerIndex > 1)
      return;
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._finger_1_ring != (UnityEngine.Object) null)
      this._finger_1_ring.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.0f));
    if (gesture.fingerIndex == 1 && (UnityEngine.Object) this._finger_2_ring != (UnityEngine.Object) null)
      this._finger_2_ring.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.0f));
    if (gesture.fingerIndex == 2 && (UnityEngine.Object) this._finger_3_ring != (UnityEngine.Object) null)
      this._finger_3_ring.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.0f));
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._finger_1_disk != (UnityEngine.Object) null)
      this._finger_1_disk.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.1f));
    if (gesture.fingerIndex == 1 && (UnityEngine.Object) this._finger_2_disk != (UnityEngine.Object) null)
      this._finger_2_disk.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.1f));
    if (gesture.fingerIndex != 2 || !((UnityEngine.Object) this._finger_3_disk != (UnityEngine.Object) null))
      return;
    this._finger_3_disk.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.1f));
  }

  private void EasyTouch_On_TouchUp(Gesture gesture)
  {
    if (gesture.fingerIndex > 1)
      return;
    this.output.Add(string.Format("EasyTouch_On_TouchUp - FingerIndex: {0}", (object) gesture.fingerIndex));
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._finger_1_ring != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._finger_1_ring);
    if (gesture.fingerIndex == 1 && (UnityEngine.Object) this._finger_2_ring != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._finger_2_ring);
    if (gesture.fingerIndex == 2 && (UnityEngine.Object) this._finger_3_ring != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._finger_3_ring);
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._finger_1_disk != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._finger_1_disk);
    if (gesture.fingerIndex == 1 && (UnityEngine.Object) this._finger_2_disk != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._finger_2_disk);
    if (gesture.fingerIndex != 2 || !((UnityEngine.Object) this._finger_3_disk != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._finger_3_disk);
  }

  private void EasyTouch_On_TouchStart2Fingers(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_TouchStart2Fingers - FingerIndex: {0}", (object) gesture.fingerIndex));
  }

  private void EasyTouch_On_TouchDown2Fingers(Gesture gesture)
  {
  }

  private void EasyTouch_On_TouchUp2Fingers(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_TouchUp2Fingers - FingerIndex: {0}", (object) gesture.fingerIndex));
  }

  private void EasyTouch_On_DragStart(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_DragStart - FingerIndex: {0}", (object) gesture.fingerIndex));
  }

  private void EasyTouch_On_Drag(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_Drag - FingerIndex: {0}", (object) gesture.fingerIndex));
  }

  private void EasyTouch_On_DragEnd(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_DragEnd - FingerIndex: {0}", (object) gesture.fingerIndex));
  }

  private void EasyTouch_On_SwipeStart(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_SwipeStart - FingerIndex: {0}", (object) gesture.fingerIndex));
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._trail_1 == (UnityEngine.Object) null)
    {
      this._trail_1 = UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefTrail, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.5f)), Quaternion.identity) as GameObject;
      this._trail_1.layer = this.gameObject.layer;
    }
    if (gesture.fingerIndex != 1 || !((UnityEngine.Object) this._trail_2 == (UnityEngine.Object) null))
      return;
    this._trail_2 = UnityEngine.Object.Instantiate((UnityEngine.Object) this.prefTrail, this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.5f)), Quaternion.identity) as GameObject;
    this._trail_2.layer = this.gameObject.layer;
  }

  private void EasyTouch_On_Swipe(Gesture gesture)
  {
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._trail_1 != (UnityEngine.Object) null)
      this._trail_1.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.5f));
    if (gesture.fingerIndex != 1 || !((UnityEngine.Object) this._trail_2 != (UnityEngine.Object) null))
      return;
    this._trail_2.transform.position = this.cam.ScreenToWorldPoint(new Vector3(gesture.position.x, gesture.position.y, 0.5f));
  }

  private void EasyTouch_On_SwipeEnd(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_SwipeEnd - FingerIndex: {0}", (object) gesture.fingerIndex));
    if (gesture.fingerIndex == 0 && (UnityEngine.Object) this._trail_1 != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this._trail_1);
    if (gesture.fingerIndex != 1 || !((UnityEngine.Object) this._trail_2 != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._trail_2);
  }

  private void EasyTouch_On_SimpleTap(Gesture gesture)
  {
    if (gesture.fingerIndex > 0)
      return;
    this.output.Add(string.Format("EasyTouch_On_SimpleTap - FingerIndex: {0}", (object) gesture.fingerIndex));
  }

  private void EasyTouch_On_PinchIn(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_PinchIn - Index: {0} Delta: {1}", (object) gesture.fingerIndex, (object) gesture.deltaPinch));
  }

  private void EasyTouch_On_PinchOut(Gesture gesture)
  {
    this.output.Add(string.Format("EasyTouch_On_PinchOut - Index: {0} Delta {1}", (object) gesture.fingerIndex, (object) gesture.deltaPinch));
  }

  private void EasyTouch_On_PinchEnd(Gesture gesture)
  {
  }
}
