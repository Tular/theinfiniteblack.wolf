﻿// Decompiled with JetBrains decompiler
// Type: GarrisonTechView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class GarrisonTechView : MonoBehaviour
{
  public GarrisonTechCard prefTechCard;
  public UILabel titleLabel;
  public UILabel researchPointsLabel;
  public UILabel researchProgressLabel;
  public UILabel researchUnitValue;
  public UILabel shipMetals;
  public UILabel shipOrganics;
  public UILabel shipGas;
  public UILabel shipRadioactives;
  public UILabel shipDarkMatter;
  public UIScrollView techScrollView;
  public GameObject techListingRoot;
  public UIGrid researchButtonRoot;
  private GarrisonDialogContext _dialogContext;

  public GarrisonDialogContext dialogContext
  {
    get
    {
      return this._dialogContext;
    }
    set
    {
      this._dialogContext = value;
      if (this._dialogContext.garrison != null)
        return;
      this.ResetVisualState();
    }
  }

  private void InitializeCards()
  {
    Array values = Enum.GetValues(typeof (TechnologyType));
    for (int index = 0; index < values.Length; ++index)
    {
      GarrisonTechCard component = this.techListingRoot.gameObject.AddChild(this.prefTechCard.gameObject).GetComponent<GarrisonTechCard>();
      component.techType = (TechnologyType) values.GetValue(index);
      component.context = this.dialogContext;
    }
  }

  private void ResetVisualState()
  {
    this.titleLabel.text = string.Empty;
    this.researchPointsLabel.text = string.Empty;
    this.researchProgressLabel.text = string.Empty;
    this.researchUnitValue.text = string.Empty;
    this.shipMetals.text = string.Empty;
    this.shipOrganics.text = string.Empty;
    this.shipGas.text = string.Empty;
    this.shipRadioactives.text = string.Empty;
    this.shipDarkMatter.text = string.Empty;
    this.techScrollView.ResetPosition();
  }

  private void Awake()
  {
    if (this.dialogContext == null)
      this.dialogContext = new GarrisonDialogContext();
    this.InitializeCards();
  }

  private void OnEnable()
  {
    this.ResetVisualState();
    this.techScrollView.ResetPosition();
  }

  private void Update()
  {
    if (TibProxy.gameState == null || TibProxy.gameState.MyShip == null || (this._dialogContext == null || this._dialogContext.garrison == null))
      return;
    this.titleLabel.text = this._dialogContext.FormattedTitleText;
    this.researchPointsLabel.text = string.Format("{0} / {1} Points Used", (object) this._dialogContext.garrison.Tech.PointsUsed, (object) this._dialogContext.garrison.Level);
    this.researchProgressLabel.text = string.Format("{0:0.0####}", (object) this._dialogContext.garrison.Progress);
    this.researchUnitValue.text = string.Format("1 Research Unit (RU) = {0}", (object) ((PlanetClass) this._dialogContext.garrison.Level).ResearchUnitProgressValue());
    this.shipMetals.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Metals);
    this.shipOrganics.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Organics);
    this.shipGas.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Gas);
    this.shipRadioactives.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.Radioactives);
    this.shipDarkMatter.text = string.Format("{0}", (object) TibProxy.gameState.MyShip.DarkMatter);
  }
}
