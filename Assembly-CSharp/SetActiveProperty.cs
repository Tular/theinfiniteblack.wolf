﻿// Decompiled with JetBrains decompiler
// Type: SetActiveProperty
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SetActiveProperty : MonoBehaviour
{
  public bool activeState = true;
  public GameObject target;
  private Collider mCol;

  private void Awake()
  {
    this.mCol = this.GetComponent<Collider>();
    Debug.Log((object) "Needed!");
  }

  private void Start()
  {
    this.Update();
  }

  private void Update()
  {
    if (this.target.activeSelf == this.activeState)
      return;
    this.target.SetActive(this.activeState);
    if (!((Object) this.mCol != (Object) null))
      return;
    this.mCol.enabled = this.activeState;
  }
}
