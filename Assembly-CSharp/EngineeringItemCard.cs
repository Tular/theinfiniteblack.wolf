﻿// Decompiled with JetBrains decompiler
// Type: EngineeringItemCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class EngineeringItemCard : EquipmentItemCardBase
{
  public UILabel title;
  public UILabel subTitle;
  public UILabel description;
  public UILabel itemPros;
  public UILabel itemCons;
  public UILabel equipPoints;
  public UILabel durability;
  public UILabel creditValue;
  public SBUIButton itemEngineering;
  public SBUIButton itemDetails;
  public int minHeight;
  private EngineeringItemListingViewContext _viewContext;

  public void SelectForEngineering()
  {
    this._viewContext.SelectItemToEngineer(this.item);
  }

  public void ShowItemProgress()
  {
    DialogManager.ShowDialog<ItemDetailsPopup>(new object[1]
    {
      (object) this.item
    });
  }

  private void Start()
  {
    EngineeringItemScrollView inParents = NGUITools.FindInParents<EngineeringItemScrollView>(this.gameObject);
    if (!((UnityEngine.Object) inParents != (UnityEngine.Object) null))
      return;
    this._viewContext = inParents.context;
  }

  protected override void UpdateVisuals()
  {
    base.UpdateVisuals();
    StringBuilder stringBuilder1 = new StringBuilder(500);
    StringBuilder stringBuilder2 = new StringBuilder(500);
    StringBuilder stringBuilder3 = new StringBuilder(500);
    StringBuilder stringBuilder4 = new StringBuilder(500);
    StringBuilder stringBuilder5 = new StringBuilder(500);
    this.item.AppendName(stringBuilder1);
    Markup.GetNGUI(stringBuilder1);
    this.title.text = stringBuilder1.ToString();
    stringBuilder2.Append(this.item.Rarity.GetMarkup());
    this.item.AppendSubTitle(stringBuilder2);
    stringBuilder2.Append("[-] ");
    Markup.GetNGUI(stringBuilder2);
    this.subTitle.text = stringBuilder2.ToString();
    stringBuilder3.Append("[d1f1ff]");
    this.item.AppendDescription(stringBuilder3);
    stringBuilder3.Append("[-]");
    this.description.text = stringBuilder3.ToString();
    this.item.AppendProsAndCons(stringBuilder4, stringBuilder5);
    stringBuilder4.Insert(0, "[00ff00]");
    stringBuilder5.Insert(0, "[ff4400]");
    stringBuilder4.Append("[-]");
    stringBuilder5.Append("[-]");
    this.itemPros.text = stringBuilder4.ToString();
    this.itemCons.text = stringBuilder5.ToString();
    this.mWidget.height = Math.Max(this.minHeight, Math.Max(Mathf.RoundToInt(-this.itemPros.transform.localPosition.y), Mathf.RoundToInt(-this.itemCons.transform.localPosition.y))) + 12;
    this.equipPoints.text = string.Format("{0}", (object) this.item.EPCost);
    this.durability.text = string.Format("{0}%", (object) this.item.Durability);
  }

  protected override void OnSpawned()
  {
    base.OnSpawned();
    this.title.text = string.Empty;
    this.subTitle.text = string.Empty;
    this.description.text = string.Empty;
    this.itemPros.text = string.Empty;
    this.itemCons.text = string.Empty;
    this.equipPoints.text = string.Empty;
    this.durability.text = string.Empty;
    this.mWidget.height = this.minHeight;
    if (this._viewContext != null)
      return;
    EngineeringItemScrollView inParents = NGUITools.FindInParents<EngineeringItemScrollView>(this.gameObject);
    if (!((UnityEngine.Object) inParents != (UnityEngine.Object) null))
      return;
    this._viewContext = inParents.context;
  }

  protected override void OnDespawned()
  {
    base.OnDespawned();
  }
}
