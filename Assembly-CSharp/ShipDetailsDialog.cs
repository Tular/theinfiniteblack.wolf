﻿// Decompiled with JetBrains decompiler
// Type: ShipDetailsDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class ShipDetailsDialog : DialogBase
{
  public int portraitSize = 256;
  public string shipImageFilenamePrefix = "profile_ship_";
  public string shipImagePath;
  public UITexture portrait;
  public UITexture image;
  public UILabel offensiveStats;
  public UILabel defensiveStats;
  public UILabel title;
  public UILabel subtitle;
  public UILabel shipClass;
  public UILabel equipPoints;
  private Ship _ship;
  private EquipmentItemSlot[] _itemSlots;
  private ShipClass _lastShipClass;
  private int _shipLastChangedId;

  protected override bool ProcessArgs(object[] args)
  {
    if (args.Length != 1)
      throw new ArgumentException("Expected args length is 1");
    this._ship = (Ship) args[0];
    return true;
  }

  public void ShowLeaderboardPage()
  {
    if (this._ship == null)
      return;
    InlineWebBrowser.Show(WebFiles.GetPlayerLeaderboardUrl(this._ship.Player, TibProxy.Instance.serverId));
  }

  public void ShowInventory()
  {
    DialogManager.ShowDialog<ItemManagementDialog>((object) this._ship, (object) ItemManagementDialog.DisplayMode.Inventory);
    this.Hide();
  }

  public void ShowEquipped()
  {
    DialogManager.ShowDialog<ItemManagementDialog>((object) this._ship, (object) ItemManagementDialog.DisplayMode.EquippedItems);
    this.Hide();
  }

  protected void UpdateVisuals()
  {
    this.offensiveStats.text = ShipStatsFormatter.FormattedOffensiveStats(this._ship);
    this.defensiveStats.text = ShipStatsFormatter.FormattedDefensiveStats(this._ship);
    this.title.text = this._ship.Player.Name;
    this.subtitle.text = this._ship.SubTitle;
    this.shipClass.text = ((Enum) this._ship.Class).ToString();
    this.equipPoints.text = string.Format("{0} of {1} Equip Points", (object) this._ship.UsedEP, (object) this._ship.MaxEP);
    for (int index = 0; index < this._itemSlots.Length; ++index)
      this._itemSlots[index].SetItem(this._ship.GetEquippedItem(this._itemSlots[index].itemType));
  }

  protected override void OnShow()
  {
    base.OnShow();
    this.LoadShipImage();
    this.UpdateVisuals();
    this._shipLastChangedId = 0;
    this._lastShipClass = this._ship.Class;
    this.StartCoroutine(this.LoadPortrait());
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this.ResetVisuals();
    this._itemSlots = this.GetComponentsInChildren<EquipmentItemSlot>();
  }

  protected override void OnHide()
  {
    base.OnHide();
    this._ship = (Ship) null;
    this.portrait.mainTexture = (Texture) null;
    this.ResetVisuals();
    this.UnloadPortrait();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  private void ResetVisuals()
  {
    this.image.mainTexture = (Texture) null;
    this.offensiveStats.text = string.Empty;
    this.defensiveStats.text = string.Empty;
    this.title.text = string.Empty;
    this.subtitle.text = string.Empty;
    this.shipClass.text = string.Empty;
    this.equipPoints.text = string.Empty;
  }

  private void LoadShipImage()
  {
    if (this._ship == null)
      return;
    string path = string.Format("{0}/{1}{2}", (object) this.shipImagePath, (object) this.shipImageFilenamePrefix, (object) this._ship.Class);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((UnityEngine.Object) texture2D == (UnityEngine.Object) null)
    {
      this.LogD(string.Format("Could not load texture: {0}", (object) path));
    }
    else
    {
      this.image.mainTexture = (Texture) texture2D;
      this.image.keepAspectRatio = UIWidget.AspectRatioSource.Free;
      this.image.width = texture2D.width;
      this.image.height = texture2D.height;
      this.image.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnWidth;
    }
  }

  private void UnloadShipImage()
  {
    if (!((UnityEngine.Object) this.image.mainTexture != (UnityEngine.Object) null))
      return;
    Resources.UnloadAsset((UnityEngine.Object) this.image.mainTexture);
  }

  [DebuggerHidden]
  private IEnumerator LoadPortrait()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShipDetailsDialog.\u003CLoadPortrait\u003Ec__Iterator1E()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void UnloadPortrait()
  {
    if ((UnityEngine.Object) this.portrait.mainTexture == (UnityEngine.Object) null)
      ;
  }

  private void Update()
  {
    if (this._ship == null || this._ship.LastChangeFlagID == this._shipLastChangedId)
      return;
    this.UpdateVisuals();
    this._shipLastChangedId = this._ship.LastChangeFlagID;
    if (this._ship.Class == this._lastShipClass)
      return;
    this.UnloadShipImage();
    this.LoadShipImage();
    this._lastShipClass = this._ship.Class;
  }
}
