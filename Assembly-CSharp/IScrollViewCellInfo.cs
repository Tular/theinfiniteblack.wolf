﻿// Decompiled with JetBrains decompiler
// Type: IScrollViewCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface IScrollViewCellInfo
{
  Vector3 localPosition { get; set; }

  Vector3 extents { get; }

  object cellData { get; }

  System.Type cellType { get; }

  bool isBound { get; }

  Transform cellPrefab { get; }

  Transform cellTransform { get; }

  bool IsContainedIn(Bounds bounds);

  void BindCell(Transform cellTrans);

  void Unbind();
}
