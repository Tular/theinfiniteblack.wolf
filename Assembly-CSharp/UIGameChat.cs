﻿// Decompiled with JetBrains decompiler
// Type: UIGameChat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIGameChat : UIChat
{
  public AudioClip notificationSound;

  protected override void OnSubmit(string text)
  {
    text = string.Format("[{0}]: {1}", (object) PlayerProfile.playerName, (object) text);
    UIChat.Add(text, Color.white);
    NGUITools.PlaySound(this.notificationSound);
    UIInput.current.isSelected = false;
  }
}
