﻿// Decompiled with JetBrains decompiler
// Type: PooledScrollViewExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public static class PooledScrollViewExtensions
{
  public static Vector2 CalculateTextSize(this UILabel label, string text)
  {
    Vector4 drawRegion = label.drawRegion;
    float num1 = drawRegion.z - drawRegion.x;
    float num2 = drawRegion.w - drawRegion.y;
    NGUIText.rectWidth = label.width;
    NGUIText.rectHeight = label.height;
    NGUIText.regionWidth = (double) num1 == 1.0 ? NGUIText.rectWidth : Mathf.RoundToInt((float) NGUIText.rectWidth * num1);
    NGUIText.regionHeight = (double) num2 == 1.0 ? NGUIText.rectHeight : Mathf.RoundToInt((float) NGUIText.rectHeight * num2);
    int num3 = Mathf.Abs(label.defaultFontSize);
    if (NGUIText.regionWidth < 1 || NGUIText.regionHeight < 0)
    {
      Debug.Log((object) "CalculateTextSize() - regionWidth < 1 || regionHeight < 0");
      return Vector2.zero;
    }
    label.UpdateNGUIText();
    if (label.overflowMethod == UILabel.Overflow.ResizeFreely)
    {
      NGUIText.rectWidth = 1000000;
      NGUIText.regionWidth = 1000000;
    }
    if (label.overflowMethod == UILabel.Overflow.ResizeFreely || label.overflowMethod == UILabel.Overflow.ResizeHeight)
    {
      NGUIText.rectHeight = 1000000;
      NGUIText.regionHeight = 1000000;
    }
    bool flag1 = label.keepCrispWhenShrunk != UILabel.Crispness.Never;
    bool flag2 = false;
    Vector2 vector2 = Vector2.zero;
    int num4 = 0;
    int num5 = 0;
    int num6;
    for (int index = num3; index > 0; index = num6 - 1)
    {
      if (flag2)
      {
        num3 = index;
        NGUIText.fontSize = num3;
      }
      else
      {
        float num7 = (float) index / (float) num3;
        NGUIText.fontScale = (float) label.fontSize / (float) label.defaultFontSize * num7;
      }
      NGUIText.Update(false);
      string finalText;
      bool flag3 = NGUIText.WrapText(text, out finalText, true);
      if (label.overflowMethod == UILabel.Overflow.ShrinkContent && !flag3)
      {
        if ((num6 = index - 1) <= 1)
          break;
      }
      else
      {
        if (label.overflowMethod == UILabel.Overflow.ResizeFreely)
        {
          vector2 = NGUIText.CalculatePrintedSize(finalText);
          int num7 = Mathf.Max(label.minWidth, Mathf.RoundToInt(vector2.x));
          if ((double) num1 != 1.0)
            num7 = Mathf.RoundToInt((float) num7 / num1);
          int num8 = Mathf.Max(label.minHeight, Mathf.RoundToInt(vector2.y));
          if ((double) num2 != 1.0)
            num8 = Mathf.RoundToInt((float) num8 / num2);
          if ((num7 & 1) == 1)
            num4 = num7 + 1;
          if ((num8 & 1) == 1)
          {
            num5 = num8 + 1;
            break;
          }
          break;
        }
        if (label.overflowMethod == UILabel.Overflow.ResizeHeight)
        {
          vector2 = NGUIText.CalculatePrintedSize(finalText);
          int num7 = Mathf.Max(label.minHeight, Mathf.RoundToInt(vector2.y));
          if ((double) num2 != 1.0)
            num7 = Mathf.RoundToInt((float) num7 / num2);
          if ((num7 & 1) == 1)
          {
            num5 = num7 + 1;
            break;
          }
          break;
        }
        vector2 = NGUIText.CalculatePrintedSize(finalText);
        break;
      }
    }
    Debug.Log((object) string.Format("{0} - CalculateTextSize: {1}", (object) label.name, (object) vector2));
    return vector2;
  }

  public static Vector2 CalculateTextSize2(this UILabel label, string text)
  {
    label.text = text;
    label.ProcessText();
    Vector2 printedSize = label.printedSize;
    label.text = string.Empty;
    return printedSize;
  }

  public static Vector3 CalculateExtents(this GeneralEquipmentItemCell cell, EquipmentItem item, out StringBuilder titleSb, out StringBuilder subtitleSb, out StringBuilder descriptionSb, out StringBuilder prosSb, out StringBuilder consSb)
  {
    titleSb = new StringBuilder(500);
    subtitleSb = new StringBuilder(500);
    descriptionSb = new StringBuilder(500);
    prosSb = new StringBuilder(500);
    consSb = new StringBuilder(500);
    item.AppendName(titleSb);
    subtitleSb.Append(item.Rarity.GetMarkup());
    item.AppendSubTitle(subtitleSb);
    subtitleSb.Append("[-] ");
    item.AppendDescription(descriptionSb);
    item.AppendProsAndCons(prosSb, consSb);
    float num = Mathf.Max((float) ((double) cell.title.CalculateTextSize2(titleSb.ToString()).y + (double) cell.subTitle.CalculateTextSize2(subtitleSb.ToString()).y + (double) cell.description.CalculateTextSize2(descriptionSb.ToString()).y + (double) Mathf.Max(cell.itemPros.CalculateTextSize2(prosSb.ToString()).y, cell.itemCons.CalculateTextSize2(consSb.ToString()).y) + 36.0), (float) cell.minHeight);
    return new Vector3((float) cell.width / 2f, num / 2f, 0.0f);
  }

  public static Vector3 CalculateExtents(this AuctionHouseItemCell cell, ClientAuction auction, out StringBuilder titleSb, out StringBuilder subtitleSb, out StringBuilder descriptionSb, out StringBuilder prosSb, out StringBuilder consSb, string auctionDetails)
  {
    titleSb = new StringBuilder(500);
    subtitleSb = new StringBuilder(500);
    descriptionSb = new StringBuilder(500);
    prosSb = new StringBuilder(500);
    consSb = new StringBuilder(500);
    auction.Item.AppendName(titleSb);
    subtitleSb.Append(auction.Item.Rarity.GetMarkup());
    auction.Item.AppendSubTitle(subtitleSb);
    subtitleSb.Append("[-] ");
    auction.Item.AppendDescription(descriptionSb);
    auction.Item.AppendProsAndCons(prosSb, consSb);
    float num = Mathf.Max((float) ((double) cell.title.CalculateTextSize2(titleSb.ToString()).y + (double) cell.subTitle.CalculateTextSize2(subtitleSb.ToString()).y + (double) cell.description.CalculateTextSize2(descriptionSb.ToString()).y + (double) Mathf.Max(cell.itemPros.CalculateTextSize2(prosSb.ToString()).y, cell.itemCons.CalculateTextSize2(consSb.ToString()).y) + 10.0) + cell.auctionDetails.CalculateTextSize2(auctionDetails).y, (float) cell.minHeight);
    return new Vector3((float) cell.width / 2f, num / 2f, 0.0f);
  }
}
