﻿// Decompiled with JetBrains decompiler
// Type: DragMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragMe : MonoBehaviour
{
  private TextMesh textMesh;
  private Color startColor;
  private Vector3 deltaPosition;
  private int fingerIndex;

  private void OnEnable()
  {
    EasyTouch.On_Drag += new EasyTouch.DragHandler(this.On_Drag);
    EasyTouch.On_DragStart += new EasyTouch.DragStartHandler(this.On_DragStart);
    EasyTouch.On_DragEnd += new EasyTouch.DragEndHandler(this.On_DragEnd);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_Drag -= new EasyTouch.DragHandler(this.On_Drag);
    EasyTouch.On_DragStart -= new EasyTouch.DragStartHandler(this.On_DragStart);
    EasyTouch.On_DragEnd -= new EasyTouch.DragEndHandler(this.On_DragEnd);
  }

  private void Start()
  {
    this.textMesh = this.GetComponentInChildren<TextMesh>();
    this.startColor = this.gameObject.GetComponent<Renderer>().material.color;
  }

  private void On_DragStart(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.fingerIndex = gesture.fingerIndex;
    this.RandomColor();
    this.deltaPosition = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position) - this.transform.position;
  }

  private void On_Drag(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject) || this.fingerIndex != gesture.fingerIndex)
      return;
    this.transform.position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position) - this.deltaPosition;
    this.textMesh.text = gesture.GetSwipeOrDragAngle().ToString("f2") + " / " + gesture.swipe.ToString();
  }

  private void On_DragEnd(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.gameObject.GetComponent<Renderer>().material.color = this.startColor;
    this.textMesh.text = "Drag me";
  }

  private void RandomColor()
  {
    this.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1f), Random.Range(0.0f, 1f), Random.Range(0.0f, 1f));
  }
}
