﻿// Decompiled with JetBrains decompiler
// Type: ListCellViewBase`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;

public abstract class ListCellViewBase<T, TD> : ListCellViewBase<T> where T : class, new() where TD : DataBindingBase<T>
{
  public TD dataBinding;

  public override T data
  {
    get
    {
      if ((UnityEngine.Object) this.dataBinding == (UnityEngine.Object) null)
        return (T) null;
      return this.dataBinding.data;
    }
  }

  public override void Initialize(T dataObj, BoundListControllerBase<T> controller)
  {
    if (!(bool) ((UnityEngine.Object) this.dataBinding))
      this.dataBinding = this.GetComponent<TD>();
    this.dataBinding.Bind(dataObj);
    this.mController = controller;
    this.OnInitialize();
  }

  public override bool isDirty
  {
    get
    {
      return this.dataBinding.isDirty;
    }
  }

  public override void SetDirty()
  {
    base.SetDirty();
    this.dataBinding.SetDirty();
  }

  protected virtual void Disable()
  {
    this.dataBinding.UpdateDataSource();
  }

  public override void Delete()
  {
    T data = this.data;
    this.dataBinding.UnBind();
    if ((UnityEngine.Object) this.mController == (UnityEngine.Object) null)
      throw new NullReferenceException("mController is null");
    this.mController.Remove(this.gameObject, data);
  }
}
