﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.SBUI.SBUITab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Spellbook.Unity.SBUI
{
  [RequireComponent(typeof (UIToggle))]
  public class SBUITab : SBUIButton
  {
    private UIToggle _toggle;
    private int _lastFrameToggleChanged;

    public bool isSelected
    {
      get
      {
        return this._toggle.value;
      }
      set
      {
        this._toggle.value = value;
      }
    }

    public int toggleGroup
    {
      get
      {
        return this._toggle.group;
      }
      set
      {
        this._toggle.group = value;
      }
    }

    public virtual void ResetToggle()
    {
      if (!this.isSelected)
        return;
      this._toggle.optionCanBeNone = true;
      this.isSelected = false;
      this._toggle.optionCanBeNone = false;
    }

    public virtual void OnToggleChanged()
    {
      if (this._lastFrameToggleChanged == Time.frameCount)
        return;
      if ((Object) UIToggle.current != (Object) null && UIToggle.current.value)
      {
        this.SetState(SBUIButtonComponent.State.Pressed);
        this.DoClick();
      }
      else
        this.SetState(SBUIButtonComponent.State.Normal);
      this._lastFrameToggleChanged = Time.frameCount;
    }

    protected override void OnInit()
    {
      base.OnInit();
      this._toggle = this.GetComponent<UIToggle>();
      EventDelegate.Add(this._toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
    }

    protected override void OnPressDown()
    {
      if (!this.clickOnPress)
        return;
      this.mState = SBUIButtonComponent.State.Pressed;
      this.SetState(this.mState);
    }

    protected override void OnPressUp()
    {
      if (!this.clickOnPress || this._lastFrameToggleChanged == Time.frameCount || (!this._toggle.enabled || !((Object) UICamera.selectedObject == (Object) this.gameObject)))
        return;
      this._toggle.value = !this._toggle.value;
    }

    protected override void OnDrag(Vector2 delta)
    {
    }

    protected override void OnClick()
    {
      if (this.isSelected || this.clickOnPress || (this._lastFrameToggleChanged == Time.frameCount || !this._toggle.enabled) || !((Object) UICamera.selectedObject == (Object) this.gameObject))
        return;
      this._toggle.value = !this._toggle.value;
    }

    protected override void OnHover(bool isOver)
    {
      if (UICamera.currentScheme == UICamera.ControlScheme.Touch || this.isSelected)
        return;
      base.OnHover(isOver);
    }

    protected override void OnEnable()
    {
      if (this._toggle.value)
        this.SetState(SBUIButtonComponent.State.Pressed);
      else
        base.OnEnable();
    }
  }
}
