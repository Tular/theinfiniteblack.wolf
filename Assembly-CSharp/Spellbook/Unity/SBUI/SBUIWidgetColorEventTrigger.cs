﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.SBUI.SBUIWidgetColorEventTrigger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Spellbook.Unity.SBUI
{
  public class SBUIWidgetColorEventTrigger : SBUIWidgetColorGroupController
  {
    public Color normal;
    public Color hover;
    public Color pressed;
    public Color selected;
  }
}
