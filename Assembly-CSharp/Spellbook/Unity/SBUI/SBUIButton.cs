﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.SBUI.SBUIButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Unity;
using UnityEngine;

namespace Spellbook.Unity.SBUI
{
  public class SBUIButton : MonoBehaviour
  {
    [HideInInspector]
    public bool clickOnPress = true;
    [HideInInspector]
    public List<EventDelegate> onClick = new List<EventDelegate>();
    [HideInInspector]
    public List<EventDelegate> onLongPress = new List<EventDelegate>();
    [HideInInspector]
    public List<EventDelegate> onHoverStart = new List<EventDelegate>();
    [HideInInspector]
    public List<EventDelegate> onHoverEnd = new List<EventDelegate>();
    public SBUIButtonComponent[] components;
    [NonSerialized]
    protected SBUIButtonComponent.State mState;
    [NonSerialized]
    protected bool mInitDone;
    private int _lastFrameClicked;
    private float _pressDelta;
    private float _pressStart;
    private bool _isDown;
    private int _lastDoClickFrame;

    public virtual bool isEnabled
    {
      get
      {
        return this.enabled;
      }
      set
      {
        if (this.isEnabled != value)
          this.SetState(!value ? SBUIButtonComponent.State.Disabled : SBUIButtonComponent.State.Normal);
        this.enabled = value;
      }
    }

    public SBUIButtonComponent GetButtonComponentForObject(GameObject obj)
    {
      for (int index = 0; index < this.components.Length; ++index)
      {
        if ((UnityEngine.Object) this.components[index].target == (UnityEngine.Object) obj)
          return this.components[index];
      }
      return (SBUIButtonComponent) null;
    }

    public SBUIButtonComponent GetButtonComponentForObject(string objectName)
    {
      for (int index = 0; index < this.components.Length; ++index)
      {
        if (this.components[index].target.name == objectName)
          return this.components[index];
      }
      return (SBUIButtonComponent) null;
    }

    public virtual void SetState(SBUIButtonComponent.State state)
    {
      if (this.components == null)
        return;
      for (int index = 0; index < this.components.Length; ++index)
        this.components[index].SetState(state);
      Collider component = this.GetComponent<Collider>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.enabled = state != SBUIButtonComponent.State.Disabled;
      this.enabled = state != SBUIButtonComponent.State.Disabled;
    }

    protected virtual void OnInit()
    {
      this.mInitDone = true;
    }

    private void Awake()
    {
      if (this.mInitDone)
        return;
      this.OnInit();
    }

    protected virtual void OnEnable()
    {
      if (this.isEnabled)
      {
        if (this.mInitDone)
        {
          if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
            this.OnHover((UnityEngine.Object) UICamera.selectedObject == (UnityEngine.Object) this.gameObject);
          else if (UICamera.currentScheme == UICamera.ControlScheme.Mouse)
            this.OnHover((UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) this.gameObject);
          else
            this.SetState(SBUIButtonComponent.State.Normal);
        }
      }
      else
        this.SetState(SBUIButtonComponent.State.Disabled);
      Collider component = this.GetComponent<Collider>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.enabled = true;
      this._pressDelta = 0.0f;
      this._pressStart = 0.0f;
      this._isDown = false;
    }

    protected virtual void OnDisable()
    {
      Collider component = this.GetComponent<Collider>();
      if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
        return;
      component.enabled = false;
    }

    private void Start()
    {
      if (this.enabled)
        return;
      this.SetState(SBUIButtonComponent.State.Disabled);
    }

    protected virtual void OnHover(bool isOver)
    {
      if (UICamera.currentScheme == UICamera.ControlScheme.Touch || !this.isEnabled)
        return;
      if (!this.mInitDone)
        this.OnInit();
      this.mState = !isOver ? SBUIButtonComponent.State.Normal : SBUIButtonComponent.State.Hover;
      this.SetState(this.mState);
      EventDelegate.Execute(!isOver ? this.onHoverEnd : this.onHoverStart);
    }

    protected virtual void Update()
    {
      if (this.onLongPress.Count <= 0 || this.mState != SBUIButtonComponent.State.Pressed)
        return;
      this._pressDelta = RealTime.time - this._pressStart;
      if (!this._isDown || (double) this._pressDelta <= 0.5)
        return;
      this.OnPress(false);
    }

    protected virtual void OnPress(bool isDown)
    {
      if (!this.isEnabled)
        return;
      if (!this.mInitDone)
        this.OnInit();
      if (isDown && !this._isDown)
      {
        this.OnPressDown();
        this._isDown = true;
        if (this.onLongPress.Count <= 0)
          return;
        this._pressStart = RealTime.time;
        this._pressDelta = 0.0f;
      }
      else
      {
        if (isDown || !this._isDown)
          return;
        if (this.onLongPress.Count > 0 && (double) this._pressDelta >= 0.5)
          this.OnMyLongPress();
        else
          this.OnPressUp();
        this._isDown = false;
        this._pressStart = 0.0f;
      }
    }

    protected virtual void OnMyLongPress()
    {
      this.mState = SBUIButtonComponent.State.Normal;
      this.SetState(this.mState);
      EventDelegate.Execute(this.onLongPress);
    }

    protected virtual void OnPressDown()
    {
      this.mState = SBUIButtonComponent.State.Pressed;
      this.SetState(this.mState);
    }

    protected virtual void OnPressUp()
    {
      this.mState = SBUIButtonComponent.State.Normal;
      this.SetState(this.mState);
      if (!this.clickOnPress)
        return;
      this.LogD("SBUIButton.OnPress calling DoClick()");
      this.DoClick();
    }

    protected void CancelPress()
    {
      this.SetState(SBUIButtonComponent.State.Normal);
      this._isDown = false;
      this._pressDelta = 0.0f;
    }

    protected virtual void OnDrag(Vector2 delta)
    {
      if (UICamera.currentTouch == null || !((UnityEngine.Object) null == (UnityEngine.Object) UICamera.currentTouch.current) && !((UnityEngine.Object) UICamera.currentTouch.current != (UnityEngine.Object) this.gameObject))
        return;
      this.CancelPress();
    }

    protected virtual void OnClick()
    {
      if (!this.enabled || this.clickOnPress || this.onLongPress.Count > 0 && (double) this._pressDelta > 0.5)
        return;
      this.DoClick();
    }

    protected virtual void DoClick()
    {
      if (this._lastFrameClicked > Time.frameCount)
        return;
      EventDelegate.Execute(this.onClick);
      this._lastFrameClicked = Time.frameCount + 1;
      this._isDown = false;
      this._pressStart = 0.0f;
    }
  }
}
