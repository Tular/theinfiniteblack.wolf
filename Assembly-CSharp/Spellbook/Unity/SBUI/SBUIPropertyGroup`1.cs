﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.SBUI.SBUIPropertyGroup`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Spellbook.Unity.SBUI
{
  public abstract class SBUIPropertyGroup<T> : MonoBehaviour where T : UIWidget
  {
    public bool defaultOnDisable = true;
    public int groupId;

    public T target { get; private set; }

    public abstract void ResetToDefault();

    protected virtual void OnAwake()
    {
    }

    private void Awake()
    {
      this.target = this.GetComponent<T>();
      this.OnAwake();
    }

    private void OnDisable()
    {
      if (!this.defaultOnDisable)
        return;
      this.ResetToDefault();
    }
  }
}
