﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.SBUI.SBUIButtonComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Spellbook.Unity.SBUI
{
  [Serializable]
  public class SBUIButtonComponent
  {
    public GameObject target;
    public SBUIButtonComponentVisualState normal;
    public SBUIButtonComponentVisualState pressed;
    public SBUIButtonComponentVisualState hover;
    public SBUIButtonComponentVisualState disabled;

    public void SetState(SBUIButtonComponent.State state)
    {
      if (!(bool) ((UnityEngine.Object) this.target))
        return;
      UIWidget component = this.target.GetComponent<UIWidget>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        return;
      UISprite uiSprite = component as UISprite;
      switch (state)
      {
        case SBUIButtonComponent.State.Normal:
          component.color = this.normal.color;
          if (!((UnityEngine.Object) uiSprite != (UnityEngine.Object) null) || string.IsNullOrEmpty(this.normal.sprite))
            break;
          uiSprite.spriteName = this.normal.sprite;
          break;
        case SBUIButtonComponent.State.Hover:
          component.color = this.hover.color;
          if (!((UnityEngine.Object) uiSprite != (UnityEngine.Object) null) || string.IsNullOrEmpty(this.hover.sprite))
            break;
          uiSprite.spriteName = this.hover.sprite;
          break;
        case SBUIButtonComponent.State.Pressed:
          component.color = this.pressed.color;
          if (!((UnityEngine.Object) uiSprite != (UnityEngine.Object) null) || string.IsNullOrEmpty(this.pressed.sprite))
            break;
          uiSprite.spriteName = this.pressed.sprite;
          break;
        case SBUIButtonComponent.State.Disabled:
          component.color = this.disabled.color;
          if (!((UnityEngine.Object) uiSprite != (UnityEngine.Object) null) || string.IsNullOrEmpty(this.disabled.sprite))
            break;
          uiSprite.spriteName = this.disabled.sprite;
          break;
      }
    }

    public enum State
    {
      Normal,
      Hover,
      Pressed,
      Disabled,
    }
  }
}
