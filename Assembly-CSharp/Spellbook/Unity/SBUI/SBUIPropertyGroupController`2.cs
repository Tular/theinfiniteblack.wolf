﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.SBUI.SBUIPropertyGroupController`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Spellbook.Unity.SBUI
{
  public abstract class SBUIPropertyGroupController<T, V> : MonoBehaviour where T : SBUIPropertyGroup<V> where V : UIWidget
  {
    public GameObject groupRoot;
    public int groupId;
    public bool includeInactive;
    protected T[] mManagedGroups;
    private bool _isStarted;

    protected virtual void OnStart()
    {
    }

    protected virtual void OnInit()
    {
    }

    public void RefreshList()
    {
      this.mManagedGroups = this.groupRoot.GetComponentsInChildren<T>(this.includeInactive);
    }

    public void ResetToDefault()
    {
      for (int index = 0; index < this.mManagedGroups.Length; ++index)
        this.mManagedGroups[index].ResetToDefault();
    }

    private void Awake()
    {
      if ((bool) ((Object) this.groupRoot))
        return;
      this.groupRoot = this.gameObject;
    }

    private void Start()
    {
      this._isStarted = true;
    }

    private void OnEnable()
    {
    }
  }
}
