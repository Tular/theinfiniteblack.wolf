﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.Steam.SbSteamBootstrap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity;
using UnityEngine;

namespace Spellbook.Unity.Steam
{
  public class SbSteamBootstrap : MonoBehaviour
  {
    private void Awake()
    {
      if (TibProxy.Instance.distribution == DistributionType.Steam_Windows || TibProxy.Instance.distribution == DistributionType.Steam_Linux || TibProxy.Instance.distribution == DistributionType.Steam_OSX)
        return;
      Object.Destroy((Object) this);
    }

    private void Start()
    {
      this.StartCoroutine(this.InitializeSteamComponents());
    }

    [DebuggerHidden]
    private IEnumerator InitializeSteamComponents()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SbSteamBootstrap.\u003CInitializeSteamComponents\u003Ec__Iterator6()
      {
        \u003C\u003Ef__this = this
      };
    }
  }
}
