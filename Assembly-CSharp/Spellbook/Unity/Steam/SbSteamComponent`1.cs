﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.Steam.SbSteamComponent`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Steamworks;
using UnityEngine;

namespace Spellbook.Unity.Steam
{
  public abstract class SbSteamComponent<T> : SbSteamComponent where T : SbSteamComponent
  {
    private static T _instance;

    public static T Instance
    {
      get
      {
        return SbSteamComponent<T>._instance;
      }
    }

    protected void Awake()
    {
      if ((Object) SbSteamComponent<T>._instance != (Object) null)
        Object.Destroy((Object) this);
      else
        SbSteamComponent<T>._instance = this.gameObject.GetComponent<T>();
    }

    protected void OnEnable()
    {
      if (!SbSteamManager.Initialized)
        return;
      if (!SbSteamComponent._isGameIdSet)
      {
        SbSteamComponent._isGameIdSet = true;
        SbSteamComponent.GameId = new CGameID(SteamUtils.GetAppID());
      }
      this.DoOnEnable();
    }

    protected void Update()
    {
      if (!SbSteamManager.Initialized)
        return;
      this.DoOnUpdate();
    }
  }
}
