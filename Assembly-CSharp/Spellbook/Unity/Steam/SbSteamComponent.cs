﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.Steam.SbSteamComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Steamworks;
using UnityEngine;

namespace Spellbook.Unity.Steam
{
  public abstract class SbSteamComponent : MonoBehaviour
  {
    protected static CGameID GameId;
    protected static bool _isGameIdSet;

    protected abstract void DoOnEnable();

    protected abstract void DoOnUpdate();
  }
}
