﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.Steam.SbSteamAchievements
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Steamworks;
using UnityEngine;

namespace Spellbook.Unity.Steam
{
  public class SbSteamAchievements : SbSteamComponent<SbSteamAchievements>
  {
    private bool m_bRequestedStats;
    private bool m_bStatsValid;
    private bool m_bStoreStats;
    protected Callback<UserStatsReceived_t> m_UserStatsReceived;
    protected Callback<UserStatsStored_t> m_UserStatsStored;
    protected Callback<UserAchievementStored_t> m_UserAchievementStored;

    public void UnlockAchievement(string achievementAppId)
    {
      Debug.Log((object) ("Unlocking achievement: " + achievementAppId));
      if (!SteamUserStats.SetAchievement(achievementAppId))
        Debug.Log((object) ("Failed to unlock achievement: " + achievementAppId));
      else
        this.m_bStoreStats = true;
    }

    public void ResetAllStats()
    {
      SteamUserStats.ResetAllStats(true);
      SteamUserStats.RequestCurrentStats();
      this.m_bStoreStats = true;
    }

    protected override void DoOnEnable()
    {
      if (!SbSteamManager.Initialized)
        return;
      this.m_UserStatsReceived = Callback<UserStatsReceived_t>.Create(new Callback<UserStatsReceived_t>.DispatchDelegate(this.OnUserStatsReceived));
      this.m_UserStatsStored = Callback<UserStatsStored_t>.Create(new Callback<UserStatsStored_t>.DispatchDelegate(this.OnUserStatsStored));
      this.m_UserAchievementStored = Callback<UserAchievementStored_t>.Create(new Callback<UserAchievementStored_t>.DispatchDelegate(this.OnAchievementStored));
      this.m_bRequestedStats = false;
      this.m_bStatsValid = false;
    }

    protected override void DoOnUpdate()
    {
      if (!this.m_bStoreStats)
        return;
      this.m_bStoreStats = !SteamUserStats.StoreStats();
    }

    private void OnUserStatsReceived(UserStatsReceived_t pCallback)
    {
    }

    private void OnUserStatsStored(UserStatsStored_t pCallback)
    {
      if ((long) (ulong) SbSteamComponent.GameId != (long) pCallback.m_nGameID)
        return;
      if (pCallback.m_eResult == EResult.k_EResultOK)
        Debug.Log((object) "StoreStats - success");
      else if (pCallback.m_eResult == EResult.k_EResultInvalidParam)
      {
        Debug.Log((object) "StoreStats - some failed to validate");
        this.OnUserStatsReceived(new UserStatsReceived_t()
        {
          m_eResult = EResult.k_EResultOK,
          m_nGameID = (ulong) SbSteamComponent.GameId
        });
      }
      else
        Debug.Log((object) ("StoreStats - failed, " + (object) pCallback.m_eResult));
    }

    private void OnAchievementStored(UserAchievementStored_t pCallback)
    {
      if ((long) (ulong) SbSteamComponent.GameId != (long) pCallback.m_nGameID)
        return;
      if ((int) pCallback.m_nMaxProgress == 0)
        Debug.Log((object) ("Achievement '" + pCallback.m_rgchAchievementName + "' unlocked!"));
      else
        Debug.Log((object) ("Achievement '" + pCallback.m_rgchAchievementName + "' progress callback, (" + (object) pCallback.m_nCurProgress + "," + (object) pCallback.m_nMaxProgress + ")"));
    }
  }
}
