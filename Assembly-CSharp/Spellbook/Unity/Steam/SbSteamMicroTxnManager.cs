﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.Steam.SbSteamMicroTxnManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Steamworks;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;

namespace Spellbook.Unity.Steam
{
  public class SbSteamMicroTxnManager : SbSteamComponent<SbSteamMicroTxnManager>
  {
    public int steamAppId = 489000;
    public float steamServicesTimeout = 10f;
    private string sbStartSteamPurchaseUrl;
    private string sbFinalizeSteamPurchaseUrl;
    protected Callback<MicroTxnAuthorizationResponse_t> m_MicroTxnAuthorizationResponse;

    public void StartStartSbSteamPurchaseTxn(string productId, string devPayload)
    {
      this.StartCoroutine(this.SbSteamPurchaseTxn_Coroutine(productId, devPayload));
    }

    protected override void DoOnEnable()
    {
      this.m_MicroTxnAuthorizationResponse = Callback<MicroTxnAuthorizationResponse_t>.Create(new Callback<MicroTxnAuthorizationResponse_t>.DispatchDelegate(this.OnMicroTxnAuthorizationResponse));
    }

    protected override void DoOnUpdate()
    {
    }

    private void OnMicroTxnAuthorizationResponse(MicroTxnAuthorizationResponse_t pCallback)
    {
      this.StartCoroutine(this.OnMicroTxnAuthorizationResponse_Coroutine(pCallback));
    }

    private void Start()
    {
      PreInstantiatedSingleton<WebFileManager>.Instance.definedUriLocations.TryGetValue("sbStartSteamPurchaseUrl", out this.sbStartSteamPurchaseUrl);
      PreInstantiatedSingleton<WebFileManager>.Instance.definedUriLocations.TryGetValue("sbFinalizeSteamPurchaseUrl", out this.sbFinalizeSteamPurchaseUrl);
    }

    [DebuggerHidden]
    private IEnumerator SbSteamPurchaseTxn_Coroutine(string productId, string devPayload)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SbSteamMicroTxnManager.\u003CSbSteamPurchaseTxn_Coroutine\u003Ec__Iterator7()
      {
        productId = productId,
        devPayload = devPayload,
        \u003C\u0024\u003EproductId = productId,
        \u003C\u0024\u003EdevPayload = devPayload,
        \u003C\u003Ef__this = this
      };
    }

    [DebuggerHidden]
    private IEnumerator OnMicroTxnAuthorizationResponse_Coroutine(MicroTxnAuthorizationResponse_t pCallback)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SbSteamMicroTxnManager.\u003COnMicroTxnAuthorizationResponse_Coroutine\u003Ec__Iterator8()
      {
        pCallback = pCallback,
        \u003C\u0024\u003EpCallback = pCallback,
        \u003C\u003Ef__this = this
      };
    }
  }
}
