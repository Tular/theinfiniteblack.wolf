﻿// Decompiled with JetBrains decompiler
// Type: Spellbook.Unity.Steam.SbSteamManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Steamworks;
using System;
using System.Text;
using UnityEngine;

namespace Spellbook.Unity.Steam
{
  [DisallowMultipleComponent]
  public class SbSteamManager : MonoBehaviour
  {
    private CGameID m_GameID;
    private static SbSteamManager s_instance;
    private static bool s_EverInialized;
    private bool m_bInitialized;
    private SteamAPIWarningMessageHook_t m_SteamAPIWarningMessageHook;

    public static SbSteamManager Instance
    {
      get
      {
        return SbSteamManager.s_instance ?? new GameObject("TibSteamManager").AddComponent<SbSteamManager>();
      }
    }

    public static bool Initialized
    {
      get
      {
        return SbSteamManager.Instance.m_bInitialized;
      }
    }

    public static ulong GameID
    {
      get
      {
        if (SbSteamManager.Initialized)
          return (ulong) SbSteamManager.Instance.m_GameID;
        return 0;
      }
    }

    private static void SteamAPIDebugTextHook(int nSeverity, StringBuilder pchDebugText)
    {
      Debug.LogWarning((object) pchDebugText);
    }

    public bool TryGameLanguage(out string languageCode)
    {
      languageCode = string.Empty;
      if (!this.m_bInitialized)
        return false;
      languageCode = SteamApps.GetCurrentGameLanguage();
      return true;
    }

    private void Awake()
    {
      if ((UnityEngine.Object) SbSteamManager.s_instance != (UnityEngine.Object) null)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
      }
      else
      {
        SbSteamManager.s_instance = this;
        if (SbSteamManager.s_EverInialized)
          throw new Exception("Tried to Initialize the SteamAPI twice in one session!");
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
        if (!Packsize.Test())
          Debug.LogError((object) "[Steamworks.NET] Packsize Test returned false, the wrong version of Steamworks.NET is being run in this platform.", (UnityEngine.Object) this);
        if (!DllCheck.Test())
          Debug.LogError((object) "[Steamworks.NET] DllCheck Test returned false, One or more of the Steamworks binaries seems to be the wrong version.", (UnityEngine.Object) this);
        try
        {
          if (SteamAPI.RestartAppIfNecessary(new AppId_t(489000U)))
          {
            Application.Quit();
            return;
          }
        }
        catch (DllNotFoundException ex)
        {
          Debug.LogError((object) ("[Steamworks.NET] Could not load [lib]steam_api.dll/so/dylib. It's likely not in the correct location. Refer to the README for more details.\n" + (object) ex), (UnityEngine.Object) this);
          Application.Quit();
          return;
        }
        this.m_bInitialized = SteamAPI.Init();
        if (!this.m_bInitialized)
          Debug.LogError((object) "[Steamworks.NET] SteamAPI_Init() failed. Refer to Valve's documentation or the comment above this line for more information.", (UnityEngine.Object) this);
        else
          SbSteamManager.s_EverInialized = true;
      }
    }

    private void OnEnable()
    {
      if ((UnityEngine.Object) SbSteamManager.s_instance == (UnityEngine.Object) null)
        SbSteamManager.s_instance = this;
      if (!this.m_bInitialized)
        return;
      if (this.m_SteamAPIWarningMessageHook == null)
      {
        this.m_SteamAPIWarningMessageHook = new SteamAPIWarningMessageHook_t(SbSteamManager.SteamAPIDebugTextHook);
        SteamClient.SetWarningMessageHook(this.m_SteamAPIWarningMessageHook);
      }
      this.m_GameID = new CGameID(SteamUtils.GetAppID());
    }

    private void OnDestroy()
    {
      if ((UnityEngine.Object) SbSteamManager.s_instance != (UnityEngine.Object) this)
        return;
      SbSteamManager.s_instance = (SbSteamManager) null;
      if (!this.m_bInitialized)
        return;
      SteamAPI.Shutdown();
    }

    private void Update()
    {
      if (!this.m_bInitialized)
        return;
      SteamAPI.RunCallbacks();
    }
  }
}
