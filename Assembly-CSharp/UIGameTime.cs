﻿// Decompiled with JetBrains decompiler
// Type: UIGameTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIGameTime : MonoBehaviour
{
  private int mLast = -1;
  public UILabel label;
  public UISprite background;
  public bool showRemaining;
  public int showThreshold;

  private void OnEnable()
  {
    if ((Object) this.label == (Object) null)
      this.label = this.GetComponent<UILabel>();
    this.Update();
  }

  private void Update()
  {
    int num1 = Mathf.FloorToInt(!this.showRemaining ? GameManager.gameTime : GameManager.timeLimit - GameManager.gameTime);
    if (num1 < 0)
      num1 = 0;
    int num2 = num1 / 60;
    if (this.showThreshold != 0)
    {
      if (num1 > this.showThreshold)
      {
        if (!this.label.enabled)
          return;
        this.label.enabled = false;
        if (!((Object) this.background != (Object) null))
          return;
        this.background.enabled = false;
        return;
      }
      if (!this.label.enabled)
      {
        this.label.enabled = true;
        if ((Object) this.background != (Object) null)
          this.background.enabled = true;
        if ((Object) this.GetComponent<Animation>() != (Object) null)
          this.GetComponent<Animation>().Play();
      }
    }
    if (this.mLast == num1)
      return;
    this.mLast = num1;
    int num3 = num1 - num2 * 60;
    this.label.text = num2.ToString() + (num3 >= 10 ? (object) ":" : (object) ":0") + (object) num3;
  }
}
