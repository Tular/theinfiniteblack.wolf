﻿// Decompiled with JetBrains decompiler
// Type: ChatDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class ChatDialog : DialogBase
{
  public int chatChannelMaxSize = 1000;
  public int chatChannelPruneSize = 100;
  private readonly Dictionary<string, ChatChannel> _commonChatChannels = new Dictionary<string, ChatChannel>();
  private bool _sideMenuVisible = true;
  public GameObject chatHelpRoot;
  public UIWidget chatClientRoot;
  public UIChatInput chatInput;
  public ChatScrollView view;
  public PrivateChatChannelManager privateChannels;
  public PlayerDirectory playerDirectory;
  public GameObject sideMenuRoot;
  public UIWidget chatViewRoot;
  public Transform prefParagraph;
  public Transform prefPrivateChannelTab;
  private int _chatRightAnchorDefault;
  private ChatChannel _currentChannel;

  public void OpenScaleDialogAsPopup()
  {
    DialogManager.GetDialog<UnifiedScaleDialog>().ShowOnTop();
  }

  public void OnToggleSideMenu()
  {
    if (this._sideMenuVisible)
    {
      this._chatRightAnchorDefault = this.chatViewRoot.rightAnchor.absolute;
      this.sideMenuRoot.SetActive(false);
      this.chatViewRoot.rightAnchor.absolute = -6;
      this.chatViewRoot.UpdateAnchors();
      this.view.InvalidateBounds();
      this._sideMenuVisible = false;
      UICamera.selectedObject = (GameObject) null;
    }
    else
    {
      this.sideMenuRoot.SetActive(true);
      this.chatViewRoot.rightAnchor.absolute = this._chatRightAnchorDefault;
      this.chatViewRoot.UpdateAnchors();
      this.view.InvalidateBounds();
      this._sideMenuVisible = true;
      UICamera.selectedObject = (GameObject) null;
    }
  }

  public void OnReplyButtonPressed()
  {
    string nextReply = TibProxy.gameState.GetNextReply();
    if (string.IsNullOrEmpty(nextReply))
      return;
    this.chatInput.BeginMessageTo(string.Format("{0}", (object) nextReply));
  }

  public void ShowUniverseChat()
  {
    this.ShowSession(((Enum) ChatType.UNIVERSE).ToString());
    this.playerDirectory.ShowOnlinePlayersIndex();
  }

  public void ShowServerChat()
  {
    this.ShowSession(((Enum) ChatType.SERVER).ToString());
    this.playerDirectory.ShowOnlinePlayersIndex();
  }

  public void ShowSectorChat()
  {
    this.ShowSession(((Enum) ChatType.SECTOR).ToString());
    this.playerDirectory.ShowSectorIndex();
  }

  public void ShowCorpChat()
  {
    this.ShowSession(((Enum) ChatType.CORP).ToString());
    this.playerDirectory.ShowCorpIndex();
  }

  public void ShowAllianceChat()
  {
    this.ShowSession(((Enum) ChatType.ALLIANCE).ToString());
    this.playerDirectory.ShowAllianceIndex();
  }

  public void ShowMarketChat()
  {
    this.ShowSession(((Enum) ChatType.MARKET).ToString());
    this.playerDirectory.ShowOnlinePlayersIndex();
  }

  public void ShowEventsChat()
  {
    this.ShowSession(((Enum) ChatType.EVENT).ToString());
    this.playerDirectory.ShowOnlinePlayersIndex();
  }

  public ChatChannel currentChannel
  {
    get
    {
      return this._currentChannel;
    }
    set
    {
      this._currentChannel = value;
      this.view.data = (IPooledScrollViewCellInifoCollection<ChatParagraphCellInfo>) value.paragraphs;
      this.view.UpdateFontSize();
      this.chatInput.defaultText = value.channelAddress;
    }
  }

  [ContextMenu("ShowChatHelp")]
  public void ShowChatHelp()
  {
    this.chatClientRoot.alpha = 0.0f;
    this.chatHelpRoot.SetActive(true);
  }

  [ContextMenu("HideChatHelp")]
  public void HideChatHelp()
  {
    this.chatHelpRoot.SetActive(false);
    this.chatClientRoot.alpha = 1f;
  }

  public void BackgroundColliderOnClick()
  {
    if (this.chatHelpRoot.activeSelf)
      this.HideChatHelp();
    else
      this.Hide();
  }

  public void BeginMessageInput(string to)
  {
    this.chatInput.BeginMessageTo(string.Format("/{0} ", (object) to));
  }

  public void BeginMessageInput()
  {
    this.chatInput.BeginMessageTo();
  }

  public void OnInputSubmit()
  {
    if (TibProxy.gameState == null)
      return;
    string text = this.chatInput.value;
    if (text.Length == 0)
      return;
    if (this.currentChannel.isPrivate)
      TibProxy.gameState.DoPrivateChat(text, this.currentChannel.channelAddress);
    else
      TibProxy.gameState.DoChat(text, this.currentChannel.chatType != ChatType.EVENT ? this.currentChannel.chatType : ChatType.SECTOR);
    this.chatInput.value = string.Empty;
  }

  private void ShowSession(string channelKey)
  {
    if (!this._commonChatChannels.ContainsKey(channelKey))
      return;
    this.currentChannel = this._commonChatChannels[channelKey];
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
    this.LogD("ChatDialog - cleaning up registered events");
    if (TibProxy.Instance != null)
    {
      TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
      TibProxy.Instance.onGameEvent -= new EventHandler<GameEventArgs>(this.TibProxy_onGameEvent);
      TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.TibProxy_onDisconnectEvent);
    }
    if (TibProxy.accountManager == null)
      return;
    TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.TibProxy_AccountManager_onSettingsUpdatedEvent);
  }

  protected override void OnShow()
  {
    UICamera.selectedObject = (GameObject) null;
    this.chatHelpRoot.SetActive(false);
    this.chatClientRoot.alpha = 1f;
  }

  protected override void OnHide()
  {
    if (!string.IsNullOrEmpty(this.chatInput.value))
      return;
    this.chatInput.value = string.Empty;
  }

  protected override void OnAwake()
  {
    TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
    TibProxy.Instance.onLoginSuccessEvent += new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
    TibProxy.Instance.onGameEvent -= new EventHandler<GameEventArgs>(this.TibProxy_onGameEvent);
    TibProxy.Instance.onGameEvent += new EventHandler<GameEventArgs>(this.TibProxy_onGameEvent);
    TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.TibProxy_onDisconnectEvent);
    TibProxy.Instance.onDisconnectEvent += new EventHandler<DisconnectEventArgs>(this.TibProxy_onDisconnectEvent);
    TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.TibProxy_AccountManager_onSettingsUpdatedEvent);
    TibProxy.accountManager.onSettingsUpdatedEvent += new EventHandler<SettingsUpdatedEventEventArgs>(this.TibProxy_AccountManager_onSettingsUpdatedEvent);
  }

  private void TibProxy_AccountManager_onSettingsUpdatedEvent(object sender, SettingsUpdatedEventEventArgs e)
  {
  }

  private void AddStandardChatChannel(ChatType type, int sortOrder, GameEventsFilter filter)
  {
    string str = ((Enum) type).ToString();
    if (this._commonChatChannels.ContainsKey(str))
      return;
    ChatChannel chatChannel = new ChatChannel(type, str, sortOrder, this.prefParagraph, filter, (UIScrollView) this.view);
    if (type == ChatType.EVENT)
      chatChannel.channelAddress = string.Empty;
    chatChannel.maxSize = this.chatChannelMaxSize;
    chatChannel.pruneSize = this.chatChannelPruneSize;
    this._commonChatChannels.Add(str, chatChannel);
  }

  private void ProcessGameEvent(GameEventArgs e)
  {
    using (Dictionary<string, ChatChannel>.ValueCollection.Enumerator enumerator = this._commonChatChannels.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChatChannel current = enumerator.Current;
        if (!current.isPrivate && current.filter != null && e.IsVisible(current.filter))
          current.AddParagraph(e);
      }
    }
    ChatEventArgs e1 = e as ChatEventArgs;
    if (e.EventType != GameEventType.Chat || e1 == null || e1.Type != ChatType.PRIVATE)
      return;
    this.privateChannels.ProcessPrivateChatEvent(e1);
  }

  private void TibProxy_onLoginSuccessEvent(object sender, LoginSuccessEventArgs e)
  {
    this.view.paragraphFontSize = TibProxy.mySettings.Social.chatFontSize;
    this.AddStandardChatChannel(ChatType.UNIVERSE, 1, TibProxy.mySettings.Social.UniverseChat);
    this.AddStandardChatChannel(ChatType.SERVER, 2, TibProxy.mySettings.Social.ServerChat);
    this.AddStandardChatChannel(ChatType.SECTOR, 3, TibProxy.mySettings.Social.SectorChat);
    this.AddStandardChatChannel(ChatType.CORP, 4, TibProxy.mySettings.Social.CorpChat);
    this.AddStandardChatChannel(ChatType.ALLIANCE, 5, TibProxy.mySettings.Social.AllianceChat);
    this.AddStandardChatChannel(ChatType.MARKET, 6, TibProxy.mySettings.Social.MarketChat);
    this.AddStandardChatChannel(ChatType.EVENT, 7, TibProxy.mySettings.Social.EventsChat);
  }

  private void TibProxy_onDisconnectEvent(object sender, DisconnectEventArgs e)
  {
  }

  private void TibProxy_onGameEvent(object sender, GameEventArgs e)
  {
    this.ProcessGameEvent(e);
    using (Dictionary<string, ChatChannel>.ValueCollection.Enumerator enumerator = this._commonChatChannels.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.PruneHistory();
    }
  }
}
