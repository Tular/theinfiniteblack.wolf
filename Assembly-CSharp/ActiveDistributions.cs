﻿// Decompiled with JetBrains decompiler
// Type: ActiveDistributions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class ActiveDistributions : MonoBehaviour
{
  public List<DistributionType> activeDistributions;

  private void Awake()
  {
    if (this.activeDistributions.Contains(TibProxy.Instance.distribution))
      return;
    this.gameObject.SetActive(false);
  }
}
