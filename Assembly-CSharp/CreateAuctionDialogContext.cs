﻿// Decompiled with JetBrains decompiler
// Type: CreateAuctionDialogContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class CreateAuctionDialogContext : DialogContextBase<CreateAuctionDialog>
{
  private EquipmentItem _item;
  private bool _auctionHasChanged;
  private int _auctionChangeLastFrameChecked;
  private int _proposedBid;
  private int _proposedBuyout;

  public CreateAuctionDialogContext(CreateAuctionDialog dlg)
    : base(dlg)
  {
  }

  public EquipmentItem item
  {
    get
    {
      return this._item;
    }
    set
    {
      if (value == null)
      {
        this.proposedBid = int.MinValue;
        this.proposedBuyout = int.MinValue;
      }
      else
      {
        this._item = value;
        this.proposedBid = this._item.ActualCreditValue / 10;
        this.proposedBuyout = 0;
        this.auctionHasChanged = true;
      }
    }
  }

  public bool auctionHasChanged
  {
    get
    {
      this._auctionChangeLastFrameChecked = Time.frameCount;
      if (!this._auctionHasChanged && this._auctionChangeLastFrameChecked != Time.frameCount)
        return false;
      this._auctionHasChanged = false;
      return true;
    }
    private set
    {
      this._auctionHasChanged = value;
    }
  }

  public int proposedBid
  {
    get
    {
      return this._proposedBid;
    }
    set
    {
      this._proposedBid = value;
      this.auctionHasChanged = true;
    }
  }

  public int proposedBuyout
  {
    get
    {
      return this._proposedBuyout;
    }
    set
    {
      this._proposedBuyout = value;
      this.auctionHasChanged = true;
    }
  }

  public bool buyoutSet
  {
    get
    {
      return this.proposedBuyout > 0;
    }
  }

  public int postingFee
  {
    get
    {
      if (this.item == null)
        return int.MinValue;
      return this.item.AuctionPostCost;
    }
  }

  public override void Reset()
  {
    this.item = (EquipmentItem) null;
    this._auctionHasChanged = false;
    this._auctionChangeLastFrameChecked = int.MinValue;
  }
}
