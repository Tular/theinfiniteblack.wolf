﻿// Decompiled with JetBrains decompiler
// Type: PooledEntityListTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.SettingsUI;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class PooledEntityListTester : MonoBehaviour
{
  public PooledEntityListManager listManager;
  public int countA;
  public int countB;
  public int countC;
  public int countD;
  public bool isActiveListA;
  public bool isActiveListB;
  public bool isActiveListC;
  public bool isActiveListD;

  [ContextMenu("Toggle List A")]
  public void ToggleListA()
  {
    this.listManager.isActiveListA = !this.listManager.isActiveListA;
  }

  [ContextMenu("Toggle List B")]
  public void ToggleListB()
  {
    this.listManager.isActiveListB = !this.listManager.isActiveListB;
  }

  [ContextMenu("Toggle List C")]
  public void ToggleListC()
  {
    this.listManager.isActiveListC = !this.listManager.isActiveListC;
  }

  [ContextMenu("Toggle List D")]
  public void ToggleListD()
  {
    this.listManager.isActiveListD = !this.listManager.isActiveListD;
  }

  [ContextMenu("Open Settings Window")]
  public void OpenSettings()
  {
    DialogManager.GetDialog<SettingsWindow>().Show();
  }

  [ContextMenu("Add Local Asteroid")]
  public void AddLocalAsteroid()
  {
    MockTibProxy.MockInstance.localEntities.AddEntity(this.CreateEntity(EntityType.ASTEROID));
  }

  [ContextMenu("Add 50 Local Npcs")]
  public void AddLocalNpc50()
  {
    for (int index = 0; index < 50; ++index)
      this.AddLocalNpc();
  }

  [ContextMenu("Add Local Npc")]
  public void AddLocalNpc()
  {
    MockTibProxy.MockInstance.localEntities.AddEntity(this.CreateEntity(EntityType.NPC));
  }

  [ContextMenu("Add Local Player Ship")]
  public void AddLocalPlayerShip()
  {
    MockTibProxy.MockInstance.localEntities.AddEntity(this.CreateEntity(EntityType.SHIP));
  }

  private Entity CreateEntity(EntityType eType)
  {
    switch (eType)
    {
      case EntityType.ASTEROID:
        return (Entity) MockAsteroid.GenerateRandom(TibProxy.gameState);
      case EntityType.SHIP:
        MockShip random = MockShip.GenerateRandom(TibProxy.gameState);
        random.SetLocation(MockTibProxy.MockInstance.moqGameState.moqMyLoc);
        return (Entity) random;
      case EntityType.NPC:
        return (Entity) MockNpc.GenerateRandom(TibProxy.gameState);
      default:
        return (Entity) null;
    }
  }

  private void Update()
  {
    if (this.listManager.listA.data != null)
      this.countA = this.listManager.listA.data.Count;
    if (this.listManager.listB.data != null)
      this.countB = this.listManager.listB.data.Count;
    if (this.listManager.listC.data != null)
      this.countC = this.listManager.listC.data.Count;
    if (this.listManager.listD.data != null)
      this.countD = this.listManager.listD.data.Count;
    this.isActiveListA = this.listManager.isActiveListA;
    this.isActiveListB = this.listManager.isActiveListB;
    this.isActiveListC = this.listManager.isActiveListC;
    this.isActiveListD = this.listManager.isActiveListD;
  }
}
