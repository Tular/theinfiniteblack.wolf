﻿// Decompiled with JetBrains decompiler
// Type: CorpDirectoryIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class CorpDirectoryIndex : PlayerDirectoryIndex
{
  private CorpStructure _corpStructure;

  public CorpDirectoryIndex(Transform headingCellPrefab)
    : base(headingCellPrefab)
  {
    this._corpStructure = new CorpStructure(headingCellPrefab);
  }

  protected override IEnumerable<PlayerDirectoryCellInfo> FilteredValues()
  {
    if (TibProxy.gameState == null || TibProxy.gameState.MyPlayer == null)
      return (IEnumerable<PlayerDirectoryCellInfo>) new List<PlayerDirectoryCellInfo>(0);
    this._corpStructure.corporation = TibProxy.gameState.MyPlayer.MyCorporation;
    return (IEnumerable<PlayerDirectoryCellInfo>) this._corpStructure.GetPlayerInfos(PlayerDirectoryIndex.OnlinePlayerInfos).ToList<PlayerDirectoryCellInfo>();
  }
}
