﻿// Decompiled with JetBrains decompiler
// Type: UIWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIWindow : MonoBehaviour
{
  private static BetterList<UIPanel> mHistory = new BetterList<UIPanel>();
  private static BetterList<UIPanel> mFading = new BetterList<UIPanel>();
  private static UIWindow mInst;
  private static UIPanel mActive;

  public static UIPanel current
  {
    get
    {
      return UIWindow.mActive;
    }
  }

  private static void CreateInstance()
  {
    if (!((Object) UIWindow.mInst == (Object) null))
      return;
    GameObject gameObject = new GameObject("_UIWindow");
    UIWindow.mInst = gameObject.AddComponent<UIWindow>();
    Object.DontDestroyOnLoad((Object) gameObject);
  }

  public static void Add(UIPanel window)
  {
    if ((Object) UIWindow.mActive == (Object) window)
      return;
    UIWindow.CreateInstance();
    if (!((Object) UIWindow.mActive == (Object) null))
      return;
    UIWindow.mActive = window;
  }

  public static void Show(UIPanel window)
  {
    if ((Object) UIWindow.mActive == (Object) window)
      return;
    UIWindow.CreateInstance();
    if ((Object) UIWindow.mActive != (Object) null)
    {
      UIWindow.mFading.Add(UIWindow.mActive);
      UIWindow.mHistory.Add(UIWindow.mActive);
    }
    if (UIWindow.mHistory.Remove(window))
      UIWindow.mFading.Remove(window);
    else if ((Object) window != (Object) null)
      window.alpha = 0.0f;
    UIWindow.mActive = window;
    if (!((Object) UIWindow.mActive != (Object) null))
      return;
    UIWindow.mActive.gameObject.SetActive(true);
  }

  public static void Hide(UIPanel window)
  {
    if (!((Object) UIWindow.mActive == (Object) window))
      return;
    UIWindow.GoBack();
  }

  public static void GoBack()
  {
    UIWindow.CreateInstance();
    if (UIWindow.mHistory.size <= 0)
      return;
    if ((Object) UIWindow.mActive != (Object) null)
    {
      UIWindow.mFading.Add(UIWindow.mActive);
      UIWindow.mActive = (UIPanel) null;
    }
    while ((Object) UIWindow.mActive == (Object) null)
    {
      UIWindow.mActive = UIWindow.mHistory.Pop();
      if ((Object) UIWindow.mActive != (Object) null)
      {
        UIWindow.mActive.alpha = 0.0f;
        UIWindow.mActive.gameObject.SetActive(true);
        UIWindow.mFading.Remove(UIWindow.mActive);
        break;
      }
    }
  }

  public static void Close()
  {
    if ((Object) UIWindow.mActive != (Object) null)
    {
      UIWindow.CreateInstance();
      UIWindow.mFading.Add(UIWindow.mActive);
      UIWindow.mHistory.Add(UIWindow.mActive);
      UIWindow.mActive = (UIPanel) null;
    }
    UIWindow.mHistory.Clear();
  }

  private void Update()
  {
    int size = UIWindow.mFading.size;
    while (size > 0)
    {
      UIPanel uiPanel = UIWindow.mFading[--size];
      if ((Object) uiPanel != (Object) null)
      {
        uiPanel.alpha = Mathf.Clamp01(uiPanel.alpha - RealTime.deltaTime * 6f);
        uiPanel.transform.localScale = Vector3.Lerp(Vector3.one * 0.9f, Vector3.one, uiPanel.alpha);
        if ((double) uiPanel.alpha > 0.0)
          continue;
      }
      UIWindow.mFading.RemoveAt(size);
      uiPanel.gameObject.SetActive(false);
    }
    if (UIWindow.mFading.size != 0 || !((Object) UIWindow.mActive != (Object) null) || (double) UIWindow.mActive.alpha >= 1.0)
      return;
    UIWindow.mActive.alpha = Mathf.Clamp01(UIWindow.mActive.alpha + RealTime.deltaTime * 6f);
    Transform transform = UIWindow.mActive.transform;
    transform.localScale = Vector3.Lerp(Vector3.one * 0.9f, Vector3.one, UIWindow.mActive.alpha);
    if (UIWindow.mActive.gameObject.layer != 10)
      return;
    transform.localRotation = Quaternion.Euler(0.0f, (float) ((double) UIWindow.mActive.alpha * 24.0 - 18.0), 0.0f);
  }
}
