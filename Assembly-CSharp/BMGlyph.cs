﻿// Decompiled with JetBrains decompiler
// Type: BMGlyph
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

[Serializable]
public class BMGlyph
{
  public int index;
  public int x;
  public int y;
  public int width;
  public int height;
  public int offsetX;
  public int offsetY;
  public int advance;
  public int channel;
  public List<int> kerning;

  public int GetKerning(int previousChar)
  {
    if (this.kerning != null && previousChar != 0)
    {
      int index = 0;
      int count = this.kerning.Count;
      while (index < count)
      {
        if (this.kerning[index] == previousChar)
          return this.kerning[index + 1];
        index += 2;
      }
    }
    return 0;
  }

  public void SetKerning(int previousChar, int amount)
  {
    if (this.kerning == null)
      this.kerning = new List<int>();
    int index = 0;
    while (index < this.kerning.Count)
    {
      if (this.kerning[index] == previousChar)
      {
        this.kerning[index + 1] = amount;
        return;
      }
      index += 2;
    }
    this.kerning.Add(previousChar);
    this.kerning.Add(amount);
  }

  public void Trim(int xMin, int yMin, int xMax, int yMax)
  {
    int num1 = this.x + this.width;
    int num2 = this.y + this.height;
    if (this.x < xMin)
    {
      int num3 = xMin - this.x;
      this.x += num3;
      this.width -= num3;
      this.offsetX += num3;
    }
    if (this.y < yMin)
    {
      int num3 = yMin - this.y;
      this.y += num3;
      this.height -= num3;
      this.offsetY += num3;
    }
    if (num1 > xMax)
      this.width -= num1 - xMax;
    if (num2 <= yMax)
      return;
    this.height -= num2 - yMax;
  }
}
