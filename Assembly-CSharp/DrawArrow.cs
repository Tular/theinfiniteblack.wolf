﻿// Decompiled with JetBrains decompiler
// Type: DrawArrow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class DrawArrow
{
  public static void ForGizmo(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20f)
  {
    Gizmos.DrawRay(pos, direction);
    Vector3 vector3_1 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f + arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Vector3 vector3_2 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f - arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Gizmos.DrawRay(pos + direction, vector3_1 * arrowHeadLength);
    Gizmos.DrawRay(pos + direction, vector3_2 * arrowHeadLength);
  }

  public static void ForGizmo(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20f)
  {
    Gizmos.color = color;
    Gizmos.DrawRay(pos, direction);
    Vector3 vector3_1 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f + arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Vector3 vector3_2 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f - arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Gizmos.DrawRay(pos + direction, vector3_1 * arrowHeadLength);
    Gizmos.DrawRay(pos + direction, vector3_2 * arrowHeadLength);
  }

  public static void ForDebug(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20f)
  {
    Debug.DrawRay(pos, direction);
    Vector3 vector3_1 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f + arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Vector3 vector3_2 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f - arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Debug.DrawRay(pos + direction, vector3_1 * arrowHeadLength);
    Debug.DrawRay(pos + direction, vector3_2 * arrowHeadLength);
  }

  public static void ForDebug(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20f)
  {
    Debug.DrawRay(pos, direction, color);
    Vector3 vector3_1 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f + arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Vector3 vector3_2 = Quaternion.LookRotation(direction) * Quaternion.Euler(0.0f, 180f - arrowHeadAngle, 0.0f) * new Vector3(0.0f, 0.0f, 1f);
    Debug.DrawRay(pos + direction, vector3_1 * arrowHeadLength, color);
    Debug.DrawRay(pos + direction, vector3_2 * arrowHeadLength, color);
  }
}
