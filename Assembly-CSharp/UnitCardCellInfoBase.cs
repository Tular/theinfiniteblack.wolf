﻿// Decompiled with JetBrains decompiler
// Type: UnitCardCellInfoBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class UnitCardCellInfoBase : IScrollViewCellInfo
{
  private Vector3 _localPosition;

  protected UnitCardCellInfoBase(Transform prefab)
  {
    this.cellPrefab = prefab;
  }

  public abstract Vector3 extents { get; }

  public abstract object cellData { get; }

  public abstract System.Type cellType { get; }

  public abstract int entityId { get; }

  public Transform cellPrefab { get; private set; }

  public abstract Transform cellTransform { get; }

  public bool isBound { get; private set; }

  public Vector3 localPosition
  {
    get
    {
      return this._localPosition;
    }
    set
    {
      this._localPosition = value;
      if (!((UnityEngine.Object) this.cellTransform != (UnityEngine.Object) null))
        return;
      this.cellTransform.localPosition = value;
    }
  }

  public void UpdateInfoState()
  {
  }

  public abstract void UpdateCellVisuals();

  public bool IsContainedIn(Bounds bounds)
  {
    if (!bounds.Contains(this.localPosition + this.extents))
      return bounds.Contains(this.localPosition - this.extents);
    return true;
  }

  public void BindCell(Transform cellTrans)
  {
    this.OnBind(cellTrans);
    this.cellTransform.localPosition = this._localPosition;
    this.isBound = true;
  }

  public void Unbind()
  {
    this.OnUnbind();
    this.isBound = false;
  }

  protected abstract void OnUnbind();

  protected abstract void OnBind(Transform cellTrans);
}
