﻿// Decompiled with JetBrains decompiler
// Type: SystemDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.SettingsUI;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class SystemDialog : DialogBase
{
  public void ShowSettingsDialog()
  {
    this.Hide();
    DialogManager.ShowDialog<SettingsWindow>(false);
  }

  public void OpenArmory()
  {
    InlineWebBrowser.Show("http://www.TheInfiniteBlack.com/Community/TIB/Leaderboards/");
  }

  public void BuyBlackDollars()
  {
    if (TibProxy.Instance.distribution == DistributionType.Steam_Windows || TibProxy.Instance.distribution == DistributionType.Steam_OSX || TibProxy.Instance.distribution == DistributionType.Linux)
      DialogManager.ShowDialogAsPopup<BlackDollarPopupDialog>().messageText.text = "Purchase BlackDollars";
    else
      Application.OpenURL(string.Format("https://spellbook.com/purchase/tib/blackdollars.php?playername={0}&mobile=1", (object) TibProxy.gameState.MyPlayer.Name));
  }

  public void OpenTutorial()
  {
    InlineWebBrowser.Show("http://www.spellbook.com/tib/tutorial.php?mobile=1");
  }

  public void OpenForums()
  {
    InlineWebBrowser.Show("https://www.spellbook.com/forum/?cat=2&mobile=1");
  }

  public void OpenWiki()
  {
    InlineWebBrowser.Show("https://www.spellbook.com/tib/wiki/");
  }

  public void ExitGame()
  {
    Application.Quit();
  }

  public void ExitToMOTD()
  {
    TibProxy.Instance.Disconnect("User Logoff", true);
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
