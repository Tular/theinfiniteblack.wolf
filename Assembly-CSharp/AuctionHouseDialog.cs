﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class AuctionHouseDialog : DialogBase<AuctionHouseDialogContext>
{
  public DialogViewBase itemListingView;
  public DialogViewBase auctionFilterView;
  public UILabel title;
  public UILabel subTitle;
  private DialogViewBase _currentView;

  public void ShowFilterView()
  {
    if ((Object) this._currentView != (Object) null)
      this._currentView.Hide();
    this._currentView = this.auctionFilterView;
    this._currentView.Show();
  }

  public void ShowFilteredItemListing()
  {
    if ((Object) this._currentView != (Object) null)
      this._currentView.Hide();
    this._currentView = this.itemListingView;
    this._currentView.Show();
  }

  protected override AuctionHouseDialogContext CreateContext()
  {
    return new AuctionHouseDialogContext(this);
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    NGUITools.SetActiveSelf(this.itemListingView.gameObject, false);
    NGUITools.SetActiveSelf(this.auctionFilterView.gameObject, false);
  }

  protected override void OnShow()
  {
    base.OnShow();
    this.auctionFilterView.Hide();
    this.context.filter = (IAuctionFilter) new MyWatchListFilter();
    this.ShowFilteredItemListing();
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.auctionFilterView.Hide();
    this.itemListingView.Hide();
  }
}
