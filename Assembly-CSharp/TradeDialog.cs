﻿// Decompiled with JetBrains decompiler
// Type: TradeDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class TradeDialog : DialogBase
{
  public TradeComponentCard myOfferCard;
  public TradeComponentCard theirOfferCard;
  public GameObject selectedItemRoot;
  public GameObject itemSelectButtonRoot;
  public UILabel acceptButtonText;
  public UIInput creditsInput;
  public UISprite creditsInputBackground;
  public UIInput blackDollarsInput;
  public UISprite blackDollarsInputBackground;
  private TradeEventArgs _trade;
  private bool _creditsInputSubmitCalled;
  private bool _blackDollarsInputWasSubmitCalled;
  private int _initialPanelDepth;

  protected override bool ProcessArgs(object[] args)
  {
    if (args.Length != 1)
      throw new ArgumentException("Expected args length is 1.");
    this.RequestNewTradeInternal((ClientPlayer) args[0]);
    return true;
  }

  private void RequestNewTradeInternal(ClientPlayer target)
  {
    this._trade = new TradeEventArgs(TibProxy.gameState);
    this._trade.A.Player = TibProxy.gameState.MyPlayer;
    this._trade.B.Player = target;
    this._trade.IsFinal = false;
    this.myOfferCard.tradeComponent = this._trade.MyOffer;
    this.theirOfferCard.tradeComponent = this._trade.TheirOffer;
  }

  public void AcceptTrade()
  {
    if (this._trade.MyOffer.BlackDollars != this.myOfferCard.blackDollars)
    {
      this._trade.MyOffer.BlackDollars = this.myOfferCard.blackDollars;
      this._trade.IsFinal = false;
    }
    if (this._trade.MyOffer.Credits != this.myOfferCard.credits)
    {
      this._trade.MyOffer.Credits = this.myOfferCard.credits;
      this._trade.IsFinal = false;
    }
    if (this._trade.MyOffer.Item != this.myOfferCard.item)
    {
      this._trade.MyOffer.Item = this.myOfferCard.item;
      this._trade.IsFinal = false;
    }
    TibProxy.gameState.Net.Send(this._trade.Serialize());
    this.Hide();
  }

  public void CancelTrade()
  {
    if (this._trade != null)
      this._trade.Cancel();
    this.Hide();
  }

  public void SelectItemFromInventory()
  {
    ItemSelectPopup itemSelectPopup = DialogManager.ShowDialogAsPopup<ItemSelectPopup>();
    itemSelectPopup.onClosePopupAction = new System.Action<EquipmentItem>(this.SetMyOfferItem);
    itemSelectPopup.onGetItemList = new Func<List<EquipmentItem>>(TradeDialog.CurrentInventoryItems);
  }

  public void OnMyOfferedCreditsSubmit()
  {
    if (this._creditsInputSubmitCalled)
      return;
    this._creditsInputSubmitCalled = true;
    try
    {
      int result;
      if (!int.TryParse(this.creditsInput.value, out result))
        result = 0;
      this.SetMyOfferCreditsAmount(result);
    }
    catch (FormatException ex)
    {
      this.LogW(string.Format("Could not convert credit amount to string"));
    }
    catch (OverflowException ex)
    {
      this.LogW(string.Format("Could not convert credit amount to string"));
    }
    if (!this.creditsInput.isSelected)
      return;
    this.creditsInput.isSelected = false;
  }

  public void OnMyOfferedBlackDollarsSubmit()
  {
    if (this._blackDollarsInputWasSubmitCalled)
      return;
    this._blackDollarsInputWasSubmitCalled = true;
    try
    {
      int result;
      if (!int.TryParse(this.blackDollarsInput.value, out result))
        result = 0;
      this.SetMyOfferBlackDollarsAmount(result);
    }
    catch (FormatException ex)
    {
      this.LogW(string.Format("Could not convert credit amount to string"));
    }
    catch (OverflowException ex)
    {
      this.LogW(string.Format("Could not convert credit amount to string"));
    }
  }

  private void SetMyOfferCreditsAmount(int credits)
  {
    this.myOfferCard.credits = credits;
  }

  private void SetMyOfferBlackDollarsAmount(int blackDollars)
  {
    this.myOfferCard.blackDollars = blackDollars;
  }

  private void SetMyOfferItem(EquipmentItem item)
  {
    this.myOfferCard.item = item;
  }

  protected override void OnInitialize()
  {
    base.OnInitialize();
    TibProxy.Instance.onTradeEvent -= new EventHandler<TradeEventArgs>(this.TibProxy_onTradeEvent);
    TibProxy.Instance.onTradeEvent += new EventHandler<TradeEventArgs>(this.TibProxy_onTradeEvent);
    TibProxy.Instance.onTradeFailedEvent -= new EventHandler<TradeFailedEventArgs>(this.TibProxy_onTradeFailedEvent);
    TibProxy.Instance.onTradeFailedEvent += new EventHandler<TradeFailedEventArgs>(this.TibProxy_onTradeFailedEvent);
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.ResetState();
    this.panel.depth = this._initialPanelDepth;
    this.itemSelectButtonRoot.SetActive(true);
    this.selectedItemRoot.SetActive(false);
  }

  protected override void OnCleanup()
  {
    base.OnCleanup();
    TibProxy.Instance.onTradeEvent -= new EventHandler<TradeEventArgs>(this.TibProxy_onTradeEvent);
    TibProxy.Instance.onTradeFailedEvent -= new EventHandler<TradeFailedEventArgs>(this.TibProxy_onTradeFailedEvent);
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
    TibProxy.Instance.onTradeEvent -= new EventHandler<TradeEventArgs>(this.TibProxy_onTradeEvent);
  }

  private void Update()
  {
    if (!this.creditsInput.isSelected && this.creditsInputBackground.enabled)
      this.OnMyOfferedCreditsSubmit();
    if (!this.blackDollarsInput.isSelected && this.blackDollarsInputBackground.enabled)
      this.OnMyOfferedBlackDollarsSubmit();
    this.creditsInputBackground.enabled = this.creditsInput.isSelected;
    this.blackDollarsInputBackground.enabled = this.blackDollarsInput.isSelected;
    if (this._trade == null || this._trade.MyOffer.Item == null)
    {
      NGUITools.SetActiveSelf(this.selectedItemRoot, false);
      NGUITools.SetActiveSelf(this.itemSelectButtonRoot, true);
    }
    else
    {
      NGUITools.SetActiveSelf(this.itemSelectButtonRoot, false);
      NGUITools.SetActiveSelf(this.selectedItemRoot, true);
    }
  }

  private void LateUpdate()
  {
    this._creditsInputSubmitCalled = false;
    this._blackDollarsInputWasSubmitCalled = false;
  }

  private void TibProxy_onTradeEvent(object sender, TradeEventArgs e)
  {
    this._trade = e;
    this.myOfferCard.tradeComponent = this._trade.MyOffer;
    this.theirOfferCard.tradeComponent = this._trade.TheirOffer;
    if (this._trade.TheirOffer != null)
      this.theirOfferCard.tradeComponent = this._trade.TheirOffer;
    this.panel.depth = UIPanel.nextUnusedDepth;
    DialogManager.ShowingDialog((DialogBase) this, false);
    this.OnShow();
  }

  private void TibProxy_onTradeFailedEvent(object sender, TradeFailedEventArgs e)
  {
    this._trade = (TradeEventArgs) null;
    this.myOfferCard.tradeComponent = (TradeComponent) null;
    this.theirOfferCard.tradeComponent = (TradeComponent) null;
    this.Hide();
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this._initialPanelDepth = this.panel.depth;
    this.ResetState();
  }

  private void ResetState()
  {
    this._trade = (TradeEventArgs) null;
    this.myOfferCard.tradeComponent = (TradeComponent) null;
    this.theirOfferCard.tradeComponent = (TradeComponent) null;
    this.creditsInput.value = "0";
    this.creditsInput.defaultText = "Credits To Trade";
    this.blackDollarsInput.value = "0";
    this.blackDollarsInput.defaultText = "BlackDollars To Trade";
  }

  private static List<EquipmentItem> CurrentInventoryItems()
  {
    Debug.Log((object) "TradeDialog.CurrentInventoryItems");
    if (TibProxy.gameState == null || TibProxy.gameState.MyShip == null)
      return new List<EquipmentItem>();
    return TibProxy.gameState.MyShip.get_Inventory();
  }
}
