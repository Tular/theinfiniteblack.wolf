﻿// Decompiled with JetBrains decompiler
// Type: EchoNguiEvents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EchoNguiEvents : MonoBehaviour
{
  private void OnHover(bool isOver)
  {
    Debug.Log((object) string.Format("{0} - OnHover:{1}", (object) this.name, (object) isOver));
  }

  private void OnPress(bool isDown)
  {
    Debug.Log((object) string.Format("{0} - OnPress:{1}", (object) this.name, (object) isDown));
  }

  private void OnSelect(bool selected)
  {
    Debug.Log((object) string.Format("{0} - OnSelect:{1}", (object) this.name, (object) selected));
  }

  private void OnClick()
  {
    Debug.Log((object) string.Format("{0} - OnClick", (object) this.name));
  }

  private void OnDoubleClick()
  {
  }

  private void OnDragStart()
  {
    Debug.Log((object) string.Format("{0} - OnDragStart", (object) this.name));
  }

  private void OnDrag(Vector2 delta)
  {
  }

  private void OnDragOver(object draggedObject)
  {
  }

  private void OnDragEnd()
  {
    Debug.Log((object) string.Format("{0} - OnDragEnd", (object) this.name));
  }

  private void OnInput(string text)
  {
  }

  private void OnToolitip(bool show)
  {
  }

  private void OnScroll(float delta)
  {
  }

  private void OnKey(KeyCode key)
  {
  }
}
