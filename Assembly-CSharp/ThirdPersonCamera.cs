﻿// Decompiled with JetBrains decompiler
// Type: ThirdPersonCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
  public float distanceAway;
  public float distanceUp;
  public float smooth;
  private GameObject hovercraft;
  private Vector3 targetPosition;
  private Transform follow;

  private void Start()
  {
    this.follow = GameObject.FindWithTag("Player").transform;
  }

  private void LateUpdate()
  {
    this.targetPosition = this.follow.position + Vector3.up * this.distanceUp - this.follow.forward * this.distanceAway;
    this.transform.position = Vector3.Lerp(this.transform.position, this.targetPosition, Time.deltaTime * this.smooth);
    this.transform.LookAt(this.follow);
  }
}
