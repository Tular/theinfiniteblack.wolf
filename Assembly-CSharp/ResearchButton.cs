﻿// Decompiled with JetBrains decompiler
// Type: ResearchButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[RequireComponent(typeof (SBUIButton))]
public class ResearchButton : MonoBehaviour
{
  private int _prevQuantity = int.MinValue;
  public UILabel ruValueLabel;
  public ResearchButton.ResearchComponentType componentType;
  public ResearchButton.ResearchTargetType targetType;
  private SBUIButton _button;
  private PlayerCombatEntity _developmentTarget;

  private void DoResearchDevelopment()
  {
    this._developmentTarget = (PlayerCombatEntity) null;
    switch (this.targetType)
    {
      case ResearchButton.ResearchTargetType.Garrison:
        this._developmentTarget = (PlayerCombatEntity) TibProxy.gameState.Local.Garrison;
        break;
      case ResearchButton.ResearchTargetType.Planet:
        this._developmentTarget = (PlayerCombatEntity) TibProxy.gameState.Local.Planet;
        break;
    }
    if (this._developmentTarget == null)
      return;
    switch (this.componentType)
    {
      case ResearchButton.ResearchComponentType.BlackDollars:
        DialogManager.ShowDialog<OnScreenNumericPad>((object) new System.Action<string, int>(this.OnKeypadEntryAccept), (object) new System.Action(this.OnKeypadEntryCancel), (object) "{0:#,###0}");
        break;
      case ResearchButton.ResearchComponentType.Metals:
        TibProxy.gameState.DoDevelopment(this._developmentTarget, ResourceType.METAL);
        this._developmentTarget = (PlayerCombatEntity) null;
        break;
      case ResearchButton.ResearchComponentType.Organics:
        TibProxy.gameState.DoDevelopment(this._developmentTarget, ResourceType.ORGANIC);
        this._developmentTarget = (PlayerCombatEntity) null;
        break;
      case ResearchButton.ResearchComponentType.Gas:
        TibProxy.gameState.DoDevelopment(this._developmentTarget, ResourceType.GAS);
        this._developmentTarget = (PlayerCombatEntity) null;
        break;
      case ResearchButton.ResearchComponentType.Radioactives:
        TibProxy.gameState.DoDevelopment(this._developmentTarget, ResourceType.RADIOACTIVE);
        this._developmentTarget = (PlayerCombatEntity) null;
        break;
      case ResearchButton.ResearchComponentType.Darkmatter:
        TibProxy.gameState.DoDevelopment(this._developmentTarget, ResourceType.DARKMATTER);
        this._developmentTarget = (PlayerCombatEntity) null;
        break;
    }
  }

  private void OnKeypadEntryAccept(string stringVal, int intVal)
  {
    TibProxy.gameState.DoDevelopment(this._developmentTarget, intVal);
    this._developmentTarget = (PlayerCombatEntity) null;
  }

  private void OnKeypadEntryCancel()
  {
  }

  private void SetFormattedIntInputValue(UIInput input, string val)
  {
    input.validation = UIInput.Validation.None;
    input.value = val;
    input.isSelected = false;
    input.enabled = false;
  }

  private void SetFormattedIntInputValue(UIInput input, int val, string format)
  {
    input.validation = UIInput.Validation.None;
    input.value = string.Format(format, (object) val);
    input.isSelected = false;
    input.enabled = false;
  }

  private void UpdateState()
  {
    Ship myShip = TibProxy.gameState.MyShip;
    switch (this.componentType)
    {
      case ResearchButton.ResearchComponentType.BlackDollars:
        this.ruValueLabel.text = string.Format("{0} RU", (object) 20);
        break;
      case ResearchButton.ResearchComponentType.Metals:
        this._prevQuantity = (int) myShip.Metals;
        if ((int) myShip.Metals <= 0)
        {
          this.ruValueLabel.text = "0 RU";
          this._button.SetState(SBUIButtonComponent.State.Disabled);
          break;
        }
        this._button.SetState(SBUIButtonComponent.State.Normal);
        this.ruValueLabel.text = string.Format("{0} RU", (object) ResourceType.METAL.ResearchUnitValue((int) myShip.Metals));
        break;
      case ResearchButton.ResearchComponentType.Organics:
        if (this._prevQuantity == (int) myShip.Organics)
          break;
        this._prevQuantity = (int) myShip.Organics;
        if ((int) myShip.Organics <= 0)
        {
          this.ruValueLabel.text = "0 RU";
          this._button.SetState(SBUIButtonComponent.State.Disabled);
          break;
        }
        this._button.SetState(SBUIButtonComponent.State.Normal);
        this.ruValueLabel.text = string.Format("{0} RU", (object) ResourceType.ORGANIC.ResearchUnitValue((int) myShip.Organics));
        break;
      case ResearchButton.ResearchComponentType.Gas:
        if (this._prevQuantity == (int) myShip.Gas)
          break;
        this._prevQuantity = (int) myShip.Gas;
        if ((int) myShip.Gas <= 0)
        {
          this.ruValueLabel.text = "0 RU";
          this._button.SetState(SBUIButtonComponent.State.Disabled);
          break;
        }
        this._button.SetState(SBUIButtonComponent.State.Normal);
        this.ruValueLabel.text = string.Format("{0} RU", (object) ResourceType.GAS.ResearchUnitValue((int) myShip.Gas));
        break;
      case ResearchButton.ResearchComponentType.Radioactives:
        if (this._prevQuantity == (int) myShip.Radioactives)
          break;
        this._prevQuantity = (int) myShip.Radioactives;
        if ((int) myShip.Radioactives <= 0)
        {
          this.ruValueLabel.text = "0 RU";
          this._button.SetState(SBUIButtonComponent.State.Disabled);
          break;
        }
        this._button.SetState(SBUIButtonComponent.State.Normal);
        this.ruValueLabel.text = string.Format("{0} RU", (object) ResourceType.RADIOACTIVE.ResearchUnitValue((int) myShip.Radioactives));
        break;
      case ResearchButton.ResearchComponentType.Darkmatter:
        if (this._prevQuantity == (int) myShip.DarkMatter)
          break;
        this._prevQuantity = (int) myShip.DarkMatter;
        if ((int) myShip.DarkMatter <= 0)
        {
          this.ruValueLabel.text = "0 RU";
          this._button.SetState(SBUIButtonComponent.State.Disabled);
          break;
        }
        this._button.SetState(SBUIButtonComponent.State.Normal);
        this.ruValueLabel.text = string.Format("{0} RU", (object) ResourceType.DARKMATTER.ResearchUnitValue((int) myShip.DarkMatter));
        break;
    }
  }

  private void Awake()
  {
    this._button = this.GetComponent<SBUIButton>();
    this.ruValueLabel.text = string.Empty;
    EventDelegate.Add(this._button.onClick, new EventDelegate.Callback(this.DoResearchDevelopment));
  }

  private void OnEnable()
  {
    this._prevQuantity = int.MinValue;
  }

  private void Update()
  {
    if (TibProxy.gameState == null || this.targetType == ResearchButton.ResearchTargetType.Garrison && TibProxy.gameState.Local.Garrison == null || this.targetType == ResearchButton.ResearchTargetType.Planet && TibProxy.gameState.Local.Planet == null)
      return;
    this.UpdateState();
  }

  public enum ResearchComponentType
  {
    BlackDollars,
    Metals,
    Organics,
    Gas,
    Radioactives,
    Darkmatter,
  }

  public enum ResearchTargetType
  {
    Garrison,
    Planet,
  }
}
