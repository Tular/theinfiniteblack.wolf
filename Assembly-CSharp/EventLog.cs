﻿// Decompiled with JetBrains decompiler
// Type: EventLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class EventLog : Singleton<EventLog>
{
  public int entryPaddingX = 2;
  public Transform prefEventLogEntry;
  public EventLogScrollView eventLogView;
  private bool _isStarted;

  public static EventLog Instance
  {
    get
    {
      return (EventLog) Singleton<EventLog>.mInstance;
    }
    set
    {
      Singleton<EventLog>.mInstance = (Singleton<EventLog>) value;
    }
  }

  private void Start()
  {
    this._isStarted = true;
  }

  protected void OnEnable()
  {
    TibProxy.Instance.onGameEvent -= new EventHandler<GameEventArgs>(this.TibProxy_OnGameEvent);
    TibProxy.Instance.onGameEvent += new EventHandler<GameEventArgs>(this.TibProxy_OnGameEvent);
  }

  protected void OnDisable()
  {
    TibProxy.Instance.onGameEvent -= new EventHandler<GameEventArgs>(this.TibProxy_OnGameEvent);
  }

  private void TibProxy_OnGameEvent(object sender, GameEventArgs e)
  {
    if (!this._isStarted)
      return;
    int count = this.eventLogView.data.Count;
    this.eventLogView.ResetPosition();
    if (e.State.MySettings != null && !e.IsVisible(e.State.MySettings.EventLog))
      return;
    this.eventLogView.AddCell(new EventLogCellInfo(this.prefEventLogEntry, e, Mathf.RoundToInt(this.eventLogView.panel.width - (float) (2 * this.entryPaddingX))));
  }
}
