﻿// Decompiled with JetBrains decompiler
// Type: AllianceDirectoryIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class AllianceDirectoryIndex : PlayerDirectoryIndex
{
  private readonly AllianceStructure _allianceStructure;

  public AllianceDirectoryIndex(Transform headingPrefab)
    : base(headingPrefab)
  {
    this._allianceStructure = new AllianceStructure(headingPrefab);
  }

  protected override IEnumerable<PlayerDirectoryCellInfo> FilteredValues()
  {
    if (TibProxy.gameState == null || TibProxy.gameState.MyPlayer == null)
      return (IEnumerable<PlayerDirectoryCellInfo>) null;
    this._allianceStructure.alliance = TibProxy.gameState.MyPlayer.MyAlliance;
    return (IEnumerable<PlayerDirectoryCellInfo>) this._allianceStructure.GetPlayerInfos(PlayerDirectoryIndex.OnlinePlayerInfos).ToList<PlayerDirectoryCellInfo>();
  }
}
