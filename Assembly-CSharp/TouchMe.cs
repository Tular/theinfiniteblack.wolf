﻿// Decompiled with JetBrains decompiler
// Type: TouchMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TouchMe : MonoBehaviour
{
  private TextMesh textMesh;
  private Color startColor;

  private void OnEnable()
  {
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.On_TouchUp);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.On_TouchUp);
  }

  private void Start()
  {
    this.textMesh = this.GetComponentInChildren<TextMesh>();
    this.startColor = this.gameObject.GetComponent<Renderer>().material.color;
  }

  private void On_TouchStart(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.RandomColor();
  }

  private void On_TouchDown(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.textMesh.text = "Down since :" + gesture.actionTime.ToString("f2");
  }

  private void On_TouchUp(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.gameObject.GetComponent<Renderer>().material.color = this.startColor;
    this.textMesh.text = "Touch me";
  }

  private void RandomColor()
  {
    this.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1f), Random.Range(0.0f, 1f), Random.Range(0.0f, 1f));
  }
}
