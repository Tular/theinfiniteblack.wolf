﻿// Decompiled with JetBrains decompiler
// Type: BoundListControllerBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class BoundListControllerBase<T> : MonoBehaviour where T : class, new()
{
  protected List<GameObject> cells = new List<GameObject>();
  public GameObject itemPrefab;
  public GameObject itemsRoot;
  [HideInInspector]
  public List<EventDelegate> onRefreshView;
  [HideInInspector]
  public List<EventDelegate> onItemAdded;
  [HideInInspector]
  public List<EventDelegate> onItemRemoved;
  [HideInInspector]
  public List<EventDelegate> onSelectionChanged;
  protected List<T> bindingSource;
  protected ListCellViewBase<T> mFocusedCellView;

  public virtual void CreateNewCell()
  {
    this.SetFocused(this.Add(Activator.CreateInstance<T>()));
  }

  public void SetFocused(ListCellViewBase<T> cellView)
  {
    Debug.Log((object) string.Format("BoundListControllerBase.SetFocused()"));
    if ((UnityEngine.Object) cellView == (UnityEngine.Object) null)
    {
      if (!((UnityEngine.Object) this.mFocusedCellView != (UnityEngine.Object) null))
        return;
      this.mFocusedCellView.hasFocus = false;
      this.mFocusedCellView = (ListCellViewBase<T>) null;
    }
    else if ((UnityEngine.Object) this.mFocusedCellView == (UnityEngine.Object) null)
    {
      this.mFocusedCellView = cellView;
      this.mFocusedCellView.hasFocus = true;
    }
    else
    {
      if (this.mFocusedCellView.Equals((object) cellView))
        return;
      Debug.Log((object) string.Format("BoundListControllerBase.SetFocused() - Changing focused element"));
      this.mFocusedCellView.hasFocus = false;
      this.mFocusedCellView = cellView;
      this.mFocusedCellView.hasFocus = true;
    }
  }

  public void ClearFocus()
  {
  }

  public void RefreshView()
  {
    EventDelegate.Execute(this.onRefreshView);
  }

  public void Reset()
  {
    using (List<GameObject>.Enumerator enumerator = this.cells.GetEnumerator())
    {
      while (enumerator.MoveNext())
        NGUITools.DestroyImmediate((UnityEngine.Object) enumerator.Current);
    }
    this.cells.Clear();
  }

  protected virtual void HandleResponse(object data, EditDialogBase.EditDialogAction action)
  {
    switch (action)
    {
      case EditDialogBase.EditDialogAction.Create:
        this.Add((T) data);
        EventDelegate.Execute(this.onItemAdded);
        break;
      case EditDialogBase.EditDialogAction.Save:
        EventDelegate.Execute(this.onRefreshView);
        break;
      case EditDialogBase.EditDialogAction.Delete:
        this.Remove(this.mFocusedCellView.gameObject, this.mFocusedCellView.data);
        this.mFocusedCellView = (ListCellViewBase<T>) null;
        EventDelegate.Execute(this.onItemRemoved);
        break;
    }
  }

  public void Bind(List<T> obj)
  {
    this.bindingSource = obj;
    using (List<T>.Enumerator enumerator = this.bindingSource.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        T current = enumerator.Current;
        GameObject go = this.itemsRoot.AddChild(this.itemPrefab);
        ListCellViewBase<T> component = go.GetComponent<ListCellViewBase<T>>();
        this.cells.Add(component.gameObject);
        component.Initialize(current, this);
        if (!NGUITools.GetActive(go))
          NGUITools.SetActiveSelf(go, true);
      }
    }
    EventDelegate.Execute(this.onItemAdded);
    EventDelegate.Execute(this.onRefreshView);
  }

  public ListCellViewBase<T> Add(T data)
  {
    GameObject go = this.itemsRoot.AddChild(this.itemPrefab);
    ListCellViewBase<T> component = go.GetComponent<ListCellViewBase<T>>();
    this.cells.Add(component.gameObject);
    this.bindingSource.Add(data);
    component.Initialize(data, this);
    if (!NGUITools.GetActive(go))
      NGUITools.SetActiveSelf(go, true);
    EventDelegate.Execute(this.onItemAdded);
    EventDelegate.Execute(this.onRefreshView);
    return component;
  }

  public void Remove(GameObject cell, T data)
  {
    if (!this.bindingSource.Remove(data))
      return;
    NGUITools.SetActiveSelf(cell, false);
    this.cells.Remove(cell);
    NGUITools.Destroy((UnityEngine.Object) cell);
    EventDelegate.Execute(this.onItemRemoved);
  }
}
