﻿// Decompiled with JetBrains decompiler
// Type: MarketEquipmentItemCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;

public class MarketEquipmentItemCell : GeneralEquipmentItemCell
{
  public void BuyWithCredits()
  {
    if (TibProxy.gameState.Local.StarPort == null)
      return;
    TibProxy.gameState.DoItemBuy(TibProxy.gameState.Local.StarPort, this.item, CurrencyType.Credits);
  }

  public void BuyWithBlackDollars()
  {
    if (TibProxy.gameState.Local.StarPort == null)
      return;
    TibProxy.gameState.DoItemBuy(TibProxy.gameState.Local.StarPort, this.item, CurrencyType.BlackDollars);
  }
}
