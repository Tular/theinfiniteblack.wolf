﻿// Decompiled with JetBrains decompiler
// Type: ItemSelectPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity.UI;

public class ItemSelectPopup : DialogBase
{
  public ItemSelectPopupItemListingScrollView itemListingScrollView;
  public SBUIButton acceptItemSelection;
  public SBUIButton cancelItemSelection;
  private int _initialPanelDepth;
  private System.Action<EquipmentItem> _onClosePopupAction;

  public ItemSelectPopupContext context { get; private set; }

  public int basePanelDepth
  {
    get
    {
      return this.panel.depth;
    }
    set
    {
      this.panel.depth = value;
      this.itemListingScrollView.panelDepth = this.panel.depth + 1;
    }
  }

  public System.Action<EquipmentItem> onClosePopupAction
  {
    get
    {
      return this._onClosePopupAction;
    }
    set
    {
      this._onClosePopupAction = value;
    }
  }

  public Func<List<EquipmentItem>> onGetItemList
  {
    get
    {
      return this.context.getItemList;
    }
    set
    {
      this.itemListingScrollView.Clear();
      this.context.getItemList = value;
      this.itemListingScrollView.RefreshView();
    }
  }

  public void OnAcceptItemSelectionClicked()
  {
    this.Hide();
  }

  public void OnCancelItemSelectionClicked()
  {
    this.context.selectedItem = (EquipmentItem) null;
    this.Hide();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  protected override void OnShow()
  {
    base.OnShow();
    this.itemListingScrollView.panelDepth = this.panel.depth + 1;
    this.itemListingScrollView.Clear();
    this.itemListingScrollView.RefreshView();
  }

  protected override void OnHide()
  {
    base.OnHide();
    if (this.onClosePopupAction != null)
      this.onClosePopupAction(this.context.selectedItem);
    this.onClosePopupAction = (System.Action<EquipmentItem>) null;
    this.context.ResetContext();
  }

  protected override void OnAwake()
  {
    this.context = new ItemSelectPopupContext();
    this.itemListingScrollView.context = this.context;
    this._initialPanelDepth = this.panel.depth;
  }

  private void OnDisable()
  {
    this.basePanelDepth = this._initialPanelDepth;
  }
}
