﻿// Decompiled with JetBrains decompiler
// Type: UIChat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIChat : MonoBehaviour
{
  public int backgroundPadding = 4;
  public int maxLines = 10;
  public float fadeOutStart = 10f;
  public float fadeOutDuration = 5f;
  public bool allowChatFading = true;
  public bool activateOnReturnKey = true;
  private BetterList<UIChat.ChatEntry> mChatEntries = new BetterList<UIChat.ChatEntry>();
  private int mBackgroundHeight = -1;
  private static UIChat mInst;
  public UIFont font;
  public UIInput input;
  public UISprite background;
  public GameObject history;
  public int lineWidth;
  private bool mIgnoreNextEnter;

  private void Awake()
  {
    UIChat.mInst = this;
    if (!((Object) this.input != (Object) null))
      return;
    EventDelegate.Set(this.input.onSubmit, new EventDelegate.Callback(this.OnSubmitInternal));
    this.input.defaultText = Localization.Get("Chat");
    UIEventListener.Get(this.input.gameObject).onSelect = new UIEventListener.BoolDelegate(this.OnSelectInput);
  }

  private void OnDestroy()
  {
    UIChat.mInst = (UIChat) null;
  }

  private void Start()
  {
    this.ResizeBackground(0, true);
  }

  private void OnSelectInput(GameObject go, bool isSelected)
  {
    this.allowChatFading = !isSelected;
  }

  private void OnSubmitInternal()
  {
    string text1 = UIInput.current.value;
    if (!string.IsNullOrEmpty(text1))
    {
      string text2 = NGUIText.StripSymbols(text1);
      this.input.value = string.Empty;
      this.OnSubmit(text2);
    }
    this.mIgnoreNextEnter = true;
  }

  protected virtual void OnSubmit(string text)
  {
  }

  private void InternalAdd(string text, Color color)
  {
    UIChat.ChatEntry chatEntry = new UIChat.ChatEntry();
    chatEntry.time = RealTime.time;
    chatEntry.color = color;
    this.mChatEntries.Add(chatEntry);
    GameObject gameObject = (!((Object) this.history != (Object) null) ? this.gameObject : this.history).AddChild();
    chatEntry.label = gameObject.AddComponent<UILabel>();
    chatEntry.label.pivot = UIWidget.Pivot.BottomLeft;
    chatEntry.label.cachedTransform.localScale = new Vector3(1f, 1f / 1000f, 1f);
    chatEntry.label.cachedTransform.localPosition = Vector3.zero;
    chatEntry.label.width = this.lineWidth;
    chatEntry.label.bitmapFont = this.font;
    chatEntry.label.color = !chatEntry.label.bitmapFont.premultipliedAlphaShader ? new Color(color.r, color.g, color.b, 0.0f) : new Color(0.0f, 0.0f, 0.0f, 0.0f);
    chatEntry.label.text = text;
    chatEntry.label.MakePixelPerfect();
    chatEntry.lines = chatEntry.label.processedText.Split('\n').Length;
    int size = this.mChatEntries.size;
    int num1 = 0;
    while (size > 0)
    {
      UIChat.ChatEntry mChatEntry = this.mChatEntries[--size];
      if (size + 1 == this.mChatEntries.size)
      {
        num1 += mChatEntry.lines;
      }
      else
      {
        int num2 = num1 * this.font.defaultSize;
        if (num1 + mChatEntry.lines > this.maxLines)
        {
          mChatEntry.isExpired = true;
          mChatEntry.shouldBeDestroyed = true;
          if ((double) mChatEntry.alpha == 0.0)
          {
            this.mChatEntries.RemoveAt(size);
            Object.Destroy((Object) mChatEntry.label.gameObject);
            continue;
          }
        }
        num1 += mChatEntry.lines;
        TweenPosition.Begin(mChatEntry.label.gameObject, 0.2f, new Vector3(0.0f, (float) num2, 0.0f));
      }
    }
  }

  protected void Update()
  {
    if (this.activateOnReturnKey && Input.GetKeyUp(KeyCode.Return))
    {
      if (!this.mIgnoreNextEnter)
        this.input.isSelected = true;
      this.mIgnoreNextEnter = false;
    }
    int height = 0;
    int index = 0;
    while (index < this.mChatEntries.size)
    {
      UIChat.ChatEntry mChatEntry = this.mChatEntries[index];
      float num = !mChatEntry.isExpired ? (!this.allowChatFading || (double) RealTime.time - (double) mChatEntry.time < (double) this.fadeOutStart ? Mathf.Clamp01(mChatEntry.alpha + RealTime.deltaTime * 5f) : ((double) RealTime.time - ((double) mChatEntry.time + (double) this.fadeOutStart) >= (double) this.fadeOutDuration ? Mathf.Clamp01(mChatEntry.alpha - RealTime.deltaTime) : Mathf.Clamp01(mChatEntry.alpha - RealTime.deltaTime / this.fadeOutDuration))) : Mathf.Clamp01(mChatEntry.alpha - RealTime.deltaTime);
      if ((double) mChatEntry.alpha != (double) num)
      {
        mChatEntry.alpha = num;
        if (!mChatEntry.fadedIn && !mChatEntry.isExpired)
        {
          float y = Mathf.Lerp(1f / 1000f, 1f, mChatEntry.alpha);
          mChatEntry.label.cachedTransform.localScale = new Vector3(1f, y, 1f);
        }
        if (mChatEntry.label.bitmapFont.premultipliedAlphaShader)
          mChatEntry.label.color = Color.Lerp(new Color(0.0f, 0.0f, 0.0f, 0.0f), mChatEntry.color, mChatEntry.alpha);
        else
          mChatEntry.label.alpha = mChatEntry.alpha;
        if ((double) num == 1.0)
          mChatEntry.fadedIn = true;
        else if ((double) num == 0.0 && mChatEntry.shouldBeDestroyed)
        {
          this.mChatEntries.RemoveAt(index);
          Object.Destroy((Object) mChatEntry.label.gameObject);
          continue;
        }
      }
      if ((double) mChatEntry.alpha > 0.00999999977648258)
        height += mChatEntry.lines * this.font.defaultSize;
      ++index;
    }
    if (!((Object) this.background != (Object) null) || this.mBackgroundHeight == height)
      return;
    this.ResizeBackground(height, !this.allowChatFading);
  }

  private void ResizeBackground(int height, bool instant)
  {
    this.mBackgroundHeight = height;
    if (height == 0)
    {
      if (instant)
      {
        this.background.height = 2;
        this.background.enabled = false;
      }
      else
        EventDelegate.Add(TweenHeight.Begin((UIWidget) this.background, 0.2f, 2).onFinished, new EventDelegate.Callback(this.DisableBackground), true);
    }
    else
    {
      this.background.enabled = true;
      if (instant)
      {
        UITweener component = (UITweener) this.background.GetComponent<TweenScale>();
        if ((Object) component != (Object) null)
          component.enabled = false;
        this.background.height = height + this.backgroundPadding;
      }
      else
        TweenHeight.Begin((UIWidget) this.background, 0.2f, height + this.backgroundPadding);
    }
  }

  private void DisableBackground()
  {
    this.background.enabled = false;
  }

  public static void Add(string text)
  {
    UIChat.Add(text, new Color(0.7f, 0.7f, 0.7f, 1f));
  }

  public static void Add(string text, Color color)
  {
    if (!((Object) UIChat.mInst != (Object) null))
      return;
    UIChat.mInst.InternalAdd(text, color);
  }

  private class ChatEntry
  {
    public UILabel label;
    public Color color;
    public float time;
    public int lines;
    public float alpha;
    public bool isExpired;
    public bool shouldBeDestroyed;
    public bool fadedIn;
  }
}
