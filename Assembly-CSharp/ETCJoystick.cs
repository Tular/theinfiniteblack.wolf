﻿// Decompiled with JetBrains decompiler
// Type: ETCJoystick
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class ETCJoystick : ETCBase, IPointerUpHandler, IPointerEnterHandler, IEventSystemHandler, IPointerDownHandler, IDragHandler, IBeginDragHandler
{
  [SerializeField]
  public ETCJoystick.OnMoveStartHandler onMoveStart;
  [SerializeField]
  public ETCJoystick.OnMoveHandler onMove;
  [SerializeField]
  public ETCJoystick.OnMoveSpeedHandler onMoveSpeed;
  [SerializeField]
  public ETCJoystick.OnMoveEndHandler onMoveEnd;
  [SerializeField]
  public ETCJoystick.OnTouchStartHandler onTouchStart;
  [SerializeField]
  public ETCJoystick.OnTouchUpHandler onTouchUp;
  [SerializeField]
  public ETCJoystick.OnDownUpHandler OnDownUp;
  [SerializeField]
  public ETCJoystick.OnDownDownHandler OnDownDown;
  [SerializeField]
  public ETCJoystick.OnDownLeftHandler OnDownLeft;
  [SerializeField]
  public ETCJoystick.OnDownRightHandler OnDownRight;
  [SerializeField]
  public ETCJoystick.OnDownUpHandler OnPressUp;
  [SerializeField]
  public ETCJoystick.OnDownDownHandler OnPressDown;
  [SerializeField]
  public ETCJoystick.OnDownLeftHandler OnPressLeft;
  [SerializeField]
  public ETCJoystick.OnDownRightHandler OnPressRight;
  public ETCJoystick.JoystickType joystickType;
  public bool allowJoystickOverTouchPad;
  public ETCJoystick.RadiusBase radiusBase;
  public ETCAxis axisX;
  public ETCAxis axisY;
  public RectTransform thumb;
  public ETCJoystick.JoystickArea joystickArea;
  public RectTransform userArea;
  private Vector2 thumbPosition;
  private bool isDynamicActif;
  private Vector2 tmpAxis;
  private Vector2 OldTmpAxis;
  private bool isOnTouch;
  [SerializeField]
  private bool isNoReturnThumb;
  private Vector2 noReturnPosition;
  private Vector2 noReturnOffset;
  [SerializeField]
  private bool isNoOffsetThumb;

  public ETCJoystick()
  {
    this.joystickType = ETCJoystick.JoystickType.Static;
    this.allowJoystickOverTouchPad = false;
    this.radiusBase = ETCJoystick.RadiusBase.Width;
    this.axisX = new ETCAxis("Horizontal");
    this.axisY = new ETCAxis("Vertical");
    this._visible = true;
    this._activated = true;
    this.joystickArea = ETCJoystick.JoystickArea.FullScreen;
    this.isDynamicActif = false;
    this.isOnDrag = false;
    this.isOnTouch = false;
    this.axisX.positivekey = KeyCode.RightArrow;
    this.axisX.negativeKey = KeyCode.LeftArrow;
    this.axisY.positivekey = KeyCode.UpArrow;
    this.axisY.negativeKey = KeyCode.DownArrow;
    this.enableKeySimulation = true;
    this.isNoReturnThumb = false;
    this.showPSInspector = true;
    this.showAxesInspector = false;
    this.showEventInspector = false;
    this.showSpriteInspector = false;
  }

  public bool IsNoReturnThumb
  {
    get
    {
      return this.isNoReturnThumb;
    }
    set
    {
      this.isNoReturnThumb = value;
    }
  }

  public bool IsNoOffsetThumb
  {
    get
    {
      return this.isNoOffsetThumb;
    }
    set
    {
      this.isNoOffsetThumb = value;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    if (this.joystickType != ETCJoystick.JoystickType.Dynamic)
      return;
    this.rectTransform().anchorMin = new Vector2(0.5f, 0.5f);
    this.rectTransform().anchorMax = new Vector2(0.5f, 0.5f);
    this.rectTransform().SetAsLastSibling();
    this.visible = false;
  }

  private void Start()
  {
    this.tmpAxis = Vector2.zero;
    this.OldTmpAxis = Vector2.zero;
    this.axisX.InitAxis();
    this.axisY.InitAxis();
    this.noReturnPosition = (Vector2) this.thumb.position;
    this.pointId = -1;
    if (this.joystickType != ETCJoystick.JoystickType.Dynamic)
      return;
    this.visible = false;
  }

  public override void Update()
  {
    base.Update();
    if (this.joystickType != ETCJoystick.JoystickType.Dynamic || this._visible || !this._activated)
      return;
    Vector2 zero1 = Vector2.zero;
    Vector2 zero2 = Vector2.zero;
    if (!this.isTouchOverJoystickArea(ref zero1, ref zero2))
      return;
    GameObject firstUiElement = this.GetFirstUIElement(zero2);
    if (!((UnityEngine.Object) firstUiElement == (UnityEngine.Object) null) && (!this.allowJoystickOverTouchPad || !(bool) ((UnityEngine.Object) firstUiElement.GetComponent<ETCTouchPad>())) && (!((UnityEngine.Object) firstUiElement != (UnityEngine.Object) null) || !(bool) ((UnityEngine.Object) firstUiElement.GetComponent<ETCArea>())))
      return;
    this.cachedRectTransform.anchoredPosition = zero1;
    this.visible = true;
  }

  protected override void UpdateControlState()
  {
    this.UpdateJoystick();
  }

  public void OnPointerEnter(PointerEventData eventData)
  {
    if (this.joystickType == ETCJoystick.JoystickType.Dynamic && !this.isDynamicActif && (this._activated && this.pointId == -1))
    {
      eventData.pointerDrag = this.gameObject;
      eventData.pointerPress = this.gameObject;
      this.isDynamicActif = true;
      this.pointId = eventData.pointerId;
    }
    if (this.joystickType != ETCJoystick.JoystickType.Dynamic || eventData.eligibleForClick)
      return;
    this.OnPointerUp(eventData);
  }

  public void OnPointerDown(PointerEventData eventData)
  {
    this.onTouchStart.Invoke();
    this.pointId = eventData.pointerId;
    if (!this.isNoOffsetThumb)
      return;
    this.OnDrag(eventData);
  }

  public void OnBeginDrag(PointerEventData eventData)
  {
  }

  public void OnDrag(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId)
      return;
    this.isOnDrag = true;
    this.isOnTouch = true;
    float radius = this.GetRadius();
    this.thumbPosition = this.isNoReturnThumb ? (eventData.position - this.noReturnPosition) / this.cachedRootCanvas.rectTransform().localScale.x + this.noReturnOffset : (eventData.position - eventData.pressPosition) / this.cachedRootCanvas.rectTransform().localScale.x;
    if (this.isNoOffsetThumb)
      this.thumbPosition = (eventData.position - (Vector2) this.cachedRectTransform.position) / this.cachedRootCanvas.rectTransform().localScale.x;
    this.thumbPosition.x = (float) Mathf.FloorToInt(this.thumbPosition.x);
    this.thumbPosition.y = (float) Mathf.FloorToInt(this.thumbPosition.y);
    if (!this.axisX.enable)
      this.thumbPosition.x = 0.0f;
    if (!this.axisY.enable)
      this.thumbPosition.y = 0.0f;
    if ((double) this.thumbPosition.magnitude > (double) radius)
      this.thumbPosition = this.isNoReturnThumb ? this.thumbPosition.normalized * radius : this.thumbPosition.normalized * radius;
    this.thumb.anchoredPosition = this.thumbPosition;
  }

  public void OnPointerUp(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId)
      return;
    this.OnUp(true);
  }

  private void OnUp(bool real = true)
  {
    this.isOnDrag = false;
    this.isOnTouch = false;
    if (this.isNoReturnThumb)
    {
      this.noReturnPosition = (Vector2) this.thumb.position;
      this.noReturnOffset = this.thumbPosition;
    }
    if (!this.isNoReturnThumb)
    {
      this.thumbPosition = Vector2.zero;
      this.thumb.anchoredPosition = Vector2.zero;
      this.axisX.axisState = ETCAxis.AxisState.None;
      this.axisY.axisState = ETCAxis.AxisState.None;
    }
    if (!this.axisX.isEnertia && !this.axisY.isEnertia)
    {
      this.axisX.ResetAxis();
      this.axisY.ResetAxis();
      this.tmpAxis = Vector2.zero;
      this.OldTmpAxis = Vector2.zero;
      if (real)
        this.onMoveEnd.Invoke();
    }
    if (this.joystickType == ETCJoystick.JoystickType.Dynamic)
    {
      this.visible = false;
      this.isDynamicActif = false;
    }
    this.pointId = -1;
    if (!real)
      return;
    this.onTouchUp.Invoke();
  }

  private void UpdateJoystick()
  {
    if (this.enableKeySimulation && !this.isOnTouch && (this._activated && this._visible))
    {
      if (!this.isNoReturnThumb)
        this.thumb.localPosition = (Vector3) Vector2.zero;
      this.isOnDrag = false;
      if (Input.GetKey(this.axisX.positivekey))
      {
        this.isOnDrag = true;
        this.thumb.localPosition = (Vector3) new Vector2(this.GetRadius(), this.thumb.localPosition.y);
      }
      else if (Input.GetKey(this.axisX.negativeKey))
      {
        this.isOnDrag = true;
        this.thumb.localPosition = (Vector3) new Vector2(-this.GetRadius(), this.thumb.localPosition.y);
      }
      if (Input.GetKey(this.axisY.positivekey))
      {
        this.isOnDrag = true;
        this.thumb.localPosition = (Vector3) new Vector2(this.thumb.localPosition.x, this.GetRadius());
      }
      else if (Input.GetKey(this.axisY.negativeKey))
      {
        this.isOnDrag = true;
        this.thumb.localPosition = (Vector3) new Vector2(this.thumb.localPosition.x, -this.GetRadius());
      }
      this.thumbPosition = (Vector2) this.thumb.localPosition;
    }
    this.OldTmpAxis.x = this.axisX.axisValue;
    this.OldTmpAxis.y = this.axisY.axisValue;
    this.tmpAxis = this.thumbPosition / this.GetRadius();
    this.axisX.UpdateAxis(this.tmpAxis.x, this.isOnDrag, ETCBase.ControlType.Joystick, true);
    this.axisY.UpdateAxis(this.tmpAxis.y, this.isOnDrag, ETCBase.ControlType.Joystick, true);
    this.axisX.DoGravity();
    this.axisY.DoGravity();
    if (((double) this.axisX.axisValue != 0.0 || (double) this.axisY.axisValue != 0.0) && this.OldTmpAxis == Vector2.zero)
      this.onMoveStart.Invoke();
    if ((double) this.axisX.axisValue != 0.0 || (double) this.axisY.axisValue != 0.0)
    {
      if (this.axisX.actionOn == ETCAxis.ActionOn.Down && (this.axisX.axisState == ETCAxis.AxisState.DownLeft || this.axisX.axisState == ETCAxis.AxisState.DownRight))
        this.axisX.DoDirectAction();
      else if (this.axisX.actionOn == ETCAxis.ActionOn.Press)
        this.axisX.DoDirectAction();
      if (this.axisY.actionOn == ETCAxis.ActionOn.Down && (this.axisY.axisState == ETCAxis.AxisState.DownUp || this.axisY.axisState == ETCAxis.AxisState.DownDown))
        this.axisY.DoDirectAction();
      else if (this.axisY.actionOn == ETCAxis.ActionOn.Press)
        this.axisY.DoDirectAction();
      this.onMove.Invoke(new Vector2(this.axisX.axisValue, this.axisY.axisValue));
      this.onMoveSpeed.Invoke(new Vector2(this.axisX.axisSpeedValue, this.axisY.axisSpeedValue));
    }
    else if ((double) this.axisX.axisValue == 0.0 && (double) this.axisY.axisValue == 0.0 && this.OldTmpAxis != Vector2.zero)
      this.onMoveEnd.Invoke();
    float num1 = 1f;
    if (this.axisX.invertedAxis)
      num1 = -1f;
    if ((double) Mathf.Abs(this.OldTmpAxis.x) < (double) this.axisX.axisThreshold && (double) Mathf.Abs(this.axisX.axisValue) >= (double) this.axisX.axisThreshold)
    {
      if ((double) this.axisX.axisValue * (double) num1 > 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.DownRight;
        this.OnDownRight.Invoke();
      }
      else if ((double) this.axisX.axisValue * (double) num1 < 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.DownLeft;
        this.OnDownLeft.Invoke();
      }
      else
        this.axisX.axisState = ETCAxis.AxisState.None;
    }
    else if (this.axisX.axisState != ETCAxis.AxisState.None)
    {
      if ((double) this.axisX.axisValue * (double) num1 > 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.PressRight;
        this.OnPressRight.Invoke();
      }
      else if ((double) this.axisX.axisValue * (double) num1 < 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.PressLeft;
        this.OnPressLeft.Invoke();
      }
      else
        this.axisX.axisState = ETCAxis.AxisState.None;
    }
    float num2 = 1f;
    if (this.axisY.invertedAxis)
      num2 = -1f;
    if ((double) Mathf.Abs(this.OldTmpAxis.y) < (double) this.axisY.axisThreshold && (double) Mathf.Abs(this.axisY.axisValue) >= (double) this.axisY.axisThreshold)
    {
      if ((double) this.axisY.axisValue * (double) num2 > 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.DownUp;
        this.OnDownUp.Invoke();
      }
      else if ((double) this.axisY.axisValue * (double) num2 < 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.DownDown;
        this.OnDownDown.Invoke();
      }
      else
        this.axisY.axisState = ETCAxis.AxisState.None;
    }
    else
    {
      if (this.axisY.axisState == ETCAxis.AxisState.None)
        return;
      if ((double) this.axisY.axisValue * (double) num2 > 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.PressUp;
        this.OnPressUp.Invoke();
      }
      else if ((double) this.axisY.axisValue * (double) num2 < 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.PressDown;
        this.OnPressDown.Invoke();
      }
      else
        this.axisY.axisState = ETCAxis.AxisState.None;
    }
  }

  private bool isTouchOverJoystickArea(ref Vector2 localPosition, ref Vector2 screenPosition)
  {
    bool flag1 = false;
    bool flag2 = false;
    screenPosition = Vector2.zero;
    int touchCount = this.GetTouchCount();
    for (int index = 0; index < touchCount && !flag1; ++index)
    {
      if (Input.GetMouseButtonDown(0))
      {
        screenPosition = (Vector2) Input.mousePosition;
        flag2 = true;
      }
      if (flag2 && this.isScreenPointOverArea(screenPosition, ref localPosition))
        flag1 = true;
    }
    return flag1;
  }

  private bool isScreenPointOverArea(Vector2 screenPosition, ref Vector2 localPosition)
  {
    bool flag = false;
    if (this.joystickArea != ETCJoystick.JoystickArea.UserDefined)
    {
      if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this.cachedRootCanvas.rectTransform(), screenPosition, (Camera) null, out localPosition))
      {
        switch (this.joystickArea)
        {
          case ETCJoystick.JoystickArea.FullScreen:
            flag = true;
            break;
          case ETCJoystick.JoystickArea.Left:
            if ((double) localPosition.x < 0.0)
            {
              flag = true;
              break;
            }
            break;
          case ETCJoystick.JoystickArea.Right:
            if ((double) localPosition.x > 0.0)
            {
              flag = true;
              break;
            }
            break;
          case ETCJoystick.JoystickArea.Top:
            if ((double) localPosition.y > 0.0)
            {
              flag = true;
              break;
            }
            break;
          case ETCJoystick.JoystickArea.Bottom:
            if ((double) localPosition.y < 0.0)
            {
              flag = true;
              break;
            }
            break;
          case ETCJoystick.JoystickArea.TopLeft:
            if ((double) localPosition.y > 0.0 && (double) localPosition.x < 0.0)
            {
              flag = true;
              break;
            }
            break;
          case ETCJoystick.JoystickArea.TopRight:
            if ((double) localPosition.y > 0.0 && (double) localPosition.x > 0.0)
            {
              flag = true;
              break;
            }
            break;
          case ETCJoystick.JoystickArea.BottomLeft:
            if ((double) localPosition.y < 0.0 && (double) localPosition.x < 0.0)
            {
              flag = true;
              break;
            }
            break;
          case ETCJoystick.JoystickArea.BottomRight:
            if ((double) localPosition.y < 0.0 && (double) localPosition.x > 0.0)
            {
              flag = true;
              break;
            }
            break;
        }
      }
    }
    else if (RectTransformUtility.RectangleContainsScreenPoint(this.userArea, screenPosition, this.cachedRootCanvas.worldCamera))
    {
      RectTransformUtility.ScreenPointToLocalPointInRectangle(this.cachedRootCanvas.rectTransform(), screenPosition, this.cachedRootCanvas.worldCamera, out localPosition);
      flag = true;
    }
    return flag;
  }

  private int GetTouchCount()
  {
    return Input.GetMouseButton(0) || Input.GetMouseButtonUp(0) ? 1 : 0;
  }

  private float GetRadius()
  {
    float num = 0.0f;
    switch (this.radiusBase)
    {
      case ETCJoystick.RadiusBase.Width:
        num = this.cachedRectTransform.sizeDelta.x * 0.5f;
        break;
      case ETCJoystick.RadiusBase.Height:
        num = this.cachedRectTransform.sizeDelta.y * 0.5f;
        break;
    }
    return num;
  }

  protected override void SetActivated()
  {
    this.GetComponent<CanvasGroup>().blocksRaycasts = this._activated;
    if (this._activated)
      return;
    this.OnUp(false);
  }

  protected override void SetVisible()
  {
    this.GetComponent<Image>().enabled = this._visible;
    this.thumb.GetComponent<Image>().enabled = this._visible;
    this.GetComponent<CanvasGroup>().blocksRaycasts = this._activated;
  }

  [Serializable]
  public class OnMoveStartHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnMoveSpeedHandler : UnityEvent<Vector2>
  {
  }

  [Serializable]
  public class OnMoveHandler : UnityEvent<Vector2>
  {
  }

  [Serializable]
  public class OnMoveEndHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnTouchStartHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnTouchUpHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownUpHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownDownHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownLeftHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownRightHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressUpHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressDownHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressLeftHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressRightHandler : UnityEvent
  {
  }

  public enum JoystickArea
  {
    UserDefined,
    FullScreen,
    Left,
    Right,
    Top,
    Bottom,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
  }

  public enum JoystickType
  {
    Dynamic,
    Static,
  }

  public enum RadiusBase
  {
    Width,
    Height,
  }
}
