﻿// Decompiled with JetBrains decompiler
// Type: MapGrid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.Map;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class MapGrid : MonoBehaviour
{
  public float halfDetailViewThreshold = 7f;
  public float minCharacterSize = 0.05f;
  public float maxCharacterSize = 0.6f;
  public float characterSizeFactor = 0.02f;
  public float lineWidthRatio = 0.0035f;
  public GameObject prefXline;
  public GameObject xRoot;
  public GameObject prefYline;
  public GameObject yRoot;
  public float tileSizeMultiplier;
  public List<GridLine> lines;
  private Camera _mapCamera;
  private float _prevCameraSize;
  private GridLine.ViewDetail _viewDetail;
  private bool _isInitialized;

  public void Initialize()
  {
    if (this._isInitialized)
      return;
    for (int index = 1; index < 16; ++index)
    {
      float num = this.tileSizeMultiplier * (float) (5 * index - 1);
      GameObject gameObject = this.xRoot.gameObject.AddChild(this.prefXline);
      gameObject.name = string.Format("X:{0}", (object) (5 * index));
      gameObject.transform.localPosition = new Vector3(0.0f, -num, 0.0f);
      GridLine component = gameObject.GetComponent<GridLine>();
      component.cellSizeFactor = this.tileSizeMultiplier;
      component.axis = GridLine.LineAxis.X;
      component.BuildLine(5 * index, string.Format("{0}", (object) (5 * index)));
      this.lines.Add(component);
    }
    for (int index = 1; index < 16; ++index)
    {
      float x = this.tileSizeMultiplier * (float) (5 * index - 1);
      GameObject gameObject = this.yRoot.gameObject.AddChild(this.prefYline);
      gameObject.name = string.Format("Y:{0}", (object) (5 * index));
      gameObject.transform.localPosition = new Vector3(x, 0.0f, 0.0f);
      GridLine component = gameObject.GetComponent<GridLine>();
      component.cellSizeFactor = this.tileSizeMultiplier;
      component.axis = GridLine.LineAxis.Y;
      component.BuildLine(5 * index, string.Format("{0}", (object) (5 * index)));
      this.lines.Add(component);
    }
    this._isInitialized = true;
  }

  private void Awake()
  {
    this.Initialize();
    TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
    TibProxy.accountManager.onSettingsUpdatedEvent += new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
  }

  private void Start()
  {
    this.gameObject.SetActive(TibProxy.accountManager.systemSettings.Gridlines);
  }

  private void OnEnable()
  {
    this._mapCamera = PreInstantiatedSingleton<SectorMap>.Instance.controller.mapCamera;
    this._prevCameraSize = float.MinValue;
    this._viewDetail = GridLine.ViewDetail.Full;
  }

  private void Update()
  {
    if ((UnityEngine.Object) this._mapCamera == (UnityEngine.Object) null || Mathf.Approximately(this._mapCamera.orthographicSize, this._prevCameraSize))
      return;
    this._prevCameraSize = this._mapCamera.orthographicSize;
    float num = this._prevCameraSize * this.lineWidthRatio * this.tileSizeMultiplier;
    GridLine.ViewDetail targetLevel = (double) this._prevCameraSize <= (double) this.halfDetailViewThreshold ? GridLine.ViewDetail.Full : GridLine.ViewDetail.Half;
    bool flag = this._viewDetail != targetLevel;
    using (List<GridLine>.Enumerator enumerator = this.lines.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GridLine current = enumerator.Current;
        if (flag)
          current.SetViewDetailLevel(targetLevel);
        current.lineWidth = num;
        current.characterSize = Mathf.Clamp(this._prevCameraSize * this.characterSizeFactor, this.minCharacterSize, this.maxCharacterSize);
      }
    }
    this._viewDetail = targetLevel;
  }

  private void OnDestroy()
  {
    if (TibProxy.accountManager == null)
      return;
    TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
  }

  private void OnDisable()
  {
    this._mapCamera = (Camera) null;
    this._prevCameraSize = float.MinValue;
  }

  private void AccountManager_onSettingsUpdatedEvent(object sender, SettingsUpdatedEventEventArgs e)
  {
    if (TibProxy.accountManager == null)
      return;
    this.gameObject.SetActive(TibProxy.accountManager.systemSettings.Gridlines);
  }
}
