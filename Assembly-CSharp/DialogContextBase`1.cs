﻿// Decompiled with JetBrains decompiler
// Type: DialogContextBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity.UI;

public abstract class DialogContextBase<T> : DialogContextBase where T : DialogBase
{
  protected DialogContextBase(T dlg)
  {
    this.dialog = dlg;
    this.dialogPanel = this.dialog.GetComponent<UIPanel>();
  }

  public T dialog { get; private set; }

  public UIPanel dialogPanel { get; private set; }
}
