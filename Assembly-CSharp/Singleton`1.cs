﻿// Decompiled with JetBrains decompiler
// Type: Singleton`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
  private static T _mInstance;

  protected static Singleton<T> mInstance
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) Singleton<T>._mInstance))
      {
        T[] sceneObjectsOfType = UnityEngine.Object.FindSceneObjectsOfType(typeof (T)) as T[];
        if (sceneObjectsOfType.Length != 0)
        {
          if (sceneObjectsOfType.Length == 1)
          {
            Singleton<T>._mInstance = sceneObjectsOfType[0];
            Singleton<T>._mInstance.gameObject.name = typeof (T).Name;
            return (Singleton<T>) Singleton<T>._mInstance;
          }
          Debug.LogError((object) ("You have more than one " + typeof (T).Name + " in the scene. You only need 1, it's a singleton!"));
          foreach (T obj in sceneObjectsOfType)
            UnityEngine.Object.Destroy((UnityEngine.Object) obj.gameObject);
        }
        GameObject gameObject = new GameObject(typeof (T).Name, new System.Type[1]
        {
          typeof (T)
        });
        Singleton<T>._mInstance = gameObject.GetComponent<T>();
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
      }
      return (Singleton<T>) Singleton<T>._mInstance;
    }
    set
    {
      Singleton<T>._mInstance = value as T;
    }
  }
}
