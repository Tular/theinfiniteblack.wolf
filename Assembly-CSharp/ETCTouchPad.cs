﻿// Decompiled with JetBrains decompiler
// Type: ETCTouchPad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class ETCTouchPad : ETCBase, IPointerUpHandler, IPointerEnterHandler, IEventSystemHandler, IPointerDownHandler, IPointerExitHandler, IDragHandler, IBeginDragHandler
{
  [SerializeField]
  public ETCTouchPad.OnMoveStartHandler onMoveStart;
  [SerializeField]
  public ETCTouchPad.OnMoveHandler onMove;
  [SerializeField]
  public ETCTouchPad.OnMoveSpeedHandler onMoveSpeed;
  [SerializeField]
  public ETCTouchPad.OnMoveEndHandler onMoveEnd;
  [SerializeField]
  public ETCTouchPad.OnTouchStartHandler onTouchStart;
  [SerializeField]
  public ETCTouchPad.OnTouchUPHandler onTouchUp;
  [SerializeField]
  public ETCTouchPad.OnDownUpHandler OnDownUp;
  [SerializeField]
  public ETCTouchPad.OnDownDownHandler OnDownDown;
  [SerializeField]
  public ETCTouchPad.OnDownLeftHandler OnDownLeft;
  [SerializeField]
  public ETCTouchPad.OnDownRightHandler OnDownRight;
  [SerializeField]
  public ETCTouchPad.OnDownUpHandler OnPressUp;
  [SerializeField]
  public ETCTouchPad.OnDownDownHandler OnPressDown;
  [SerializeField]
  public ETCTouchPad.OnDownLeftHandler OnPressLeft;
  [SerializeField]
  public ETCTouchPad.OnDownRightHandler OnPressRight;
  public ETCAxis axisX;
  public ETCAxis axisY;
  public bool isDPI;
  private Image cachedImage;
  private Vector2 tmpAxis;
  private Vector2 OldTmpAxis;
  private GameObject previousDargObject;
  private bool isOut;
  private bool isOnTouch;
  private bool cachedVisible;

  public ETCTouchPad()
  {
    this.axisX = new ETCAxis("Horizontal");
    this.axisX.speed = 1f;
    this.axisY = new ETCAxis("Vertical");
    this.axisY.speed = 1f;
    this._visible = true;
    this._activated = true;
    this.showPSInspector = true;
    this.showSpriteInspector = false;
    this.showBehaviourInspector = false;
    this.showEventInspector = false;
    this.tmpAxis = Vector2.zero;
    this.isOnDrag = false;
    this.isOnTouch = false;
    this.axisX.positivekey = KeyCode.RightArrow;
    this.axisX.negativeKey = KeyCode.LeftArrow;
    this.axisY.positivekey = KeyCode.UpArrow;
    this.axisY.negativeKey = KeyCode.DownArrow;
    this.enableKeySimulation = true;
    this.enableKeySimulation = false;
    this.isOut = false;
    this.axisX.axisState = ETCAxis.AxisState.None;
    this.useFixedUpdate = false;
    this.isDPI = false;
  }

  protected override void Awake()
  {
    base.Awake();
    this.cachedVisible = this._visible;
    this.cachedImage = this.GetComponent<Image>();
  }

  public override void OnEnable()
  {
    base.OnEnable();
    if (this.cachedVisible)
      return;
    this.cachedImage.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
  }

  private void Start()
  {
    this.tmpAxis = Vector2.zero;
    this.OldTmpAxis = Vector2.zero;
    this.axisX.InitAxis();
    this.axisY.InitAxis();
  }

  protected override void UpdateControlState()
  {
    this.UpdateTouchPad();
  }

  public void OnPointerEnter(PointerEventData eventData)
  {
    if (!this.isSwipeIn || this.axisX.axisState != ETCAxis.AxisState.None || (!this._activated || this.isOnTouch))
      return;
    if ((UnityEngine.Object) eventData.pointerDrag != (UnityEngine.Object) null && (UnityEngine.Object) eventData.pointerDrag != (UnityEngine.Object) this.gameObject)
      this.previousDargObject = eventData.pointerDrag;
    else if ((UnityEngine.Object) eventData.pointerPress != (UnityEngine.Object) null && (UnityEngine.Object) eventData.pointerPress != (UnityEngine.Object) this.gameObject)
      this.previousDargObject = eventData.pointerPress;
    eventData.pointerDrag = this.gameObject;
    eventData.pointerPress = this.gameObject;
    this.OnPointerDown(eventData);
  }

  public void OnBeginDrag(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId)
      return;
    this.onMoveStart.Invoke();
  }

  public void OnDrag(PointerEventData eventData)
  {
    if (!this.activated || this.isOut || this.pointId != eventData.pointerId)
      return;
    this.isOnTouch = true;
    this.isOnDrag = true;
    this.tmpAxis = !this.isDPI ? new Vector2(eventData.delta.x, eventData.delta.y) : new Vector2((float) ((double) eventData.delta.x / (double) Screen.width * 1000.0), (float) ((double) eventData.delta.y / (double) Screen.height * 1000.0));
    if (!this.axisX.enable)
      this.tmpAxis.x = 0.0f;
    if (this.axisY.enable)
      return;
    this.tmpAxis.y = 0.0f;
  }

  public void OnPointerDown(PointerEventData eventData)
  {
    if (!this._activated || this.isOnTouch)
      return;
    this.axisX.axisState = ETCAxis.AxisState.Down;
    this.tmpAxis = eventData.delta;
    this.isOut = false;
    this.isOnTouch = true;
    this.pointId = eventData.pointerId;
    this.onTouchStart.Invoke();
  }

  public void OnPointerUp(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId)
      return;
    this.isOnDrag = false;
    this.isOnTouch = false;
    this.tmpAxis = Vector2.zero;
    this.OldTmpAxis = Vector2.zero;
    this.axisX.axisState = ETCAxis.AxisState.None;
    this.axisY.axisState = ETCAxis.AxisState.None;
    if (!this.axisX.isEnertia && !this.axisY.isEnertia)
    {
      this.axisX.ResetAxis();
      this.axisY.ResetAxis();
      this.onMoveEnd.Invoke();
    }
    this.onTouchUp.Invoke();
    if ((bool) ((UnityEngine.Object) this.previousDargObject))
    {
      ExecuteEvents.Execute<IPointerUpHandler>(this.previousDargObject, (BaseEventData) eventData, ExecuteEvents.pointerUpHandler);
      this.previousDargObject = (GameObject) null;
    }
    this.pointId = -1;
  }

  public void OnPointerExit(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId || this.isSwipeOut)
      return;
    this.isOut = true;
    this.OnPointerUp(eventData);
  }

  private void UpdateTouchPad()
  {
    if (this.enableKeySimulation && !this.isOnTouch && (this._activated && this._visible))
    {
      this.isOnDrag = false;
      this.tmpAxis = Vector2.zero;
      if (Input.GetKey(this.axisX.positivekey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(1f, this.tmpAxis.y);
      }
      else if (Input.GetKey(this.axisX.negativeKey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(-1f, this.tmpAxis.y);
      }
      if (Input.GetKey(this.axisY.positivekey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(this.tmpAxis.x, 1f);
      }
      else if (Input.GetKey(this.axisY.negativeKey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(this.tmpAxis.x, -1f);
      }
    }
    this.OldTmpAxis.x = this.axisX.axisValue;
    this.OldTmpAxis.y = this.axisY.axisValue;
    this.axisX.UpdateAxis(this.tmpAxis.x, this.isOnDrag, ETCBase.ControlType.DPad, true);
    this.axisY.UpdateAxis(this.tmpAxis.y, this.isOnDrag, ETCBase.ControlType.DPad, true);
    this.axisX.DoGravity();
    this.axisY.DoGravity();
    if ((double) this.axisX.axisValue != 0.0 || (double) this.axisY.axisValue != 0.0)
    {
      if (this.axisX.actionOn == ETCAxis.ActionOn.Down && (this.axisX.axisState == ETCAxis.AxisState.DownLeft || this.axisX.axisState == ETCAxis.AxisState.DownRight))
        this.axisX.DoDirectAction();
      else if (this.axisX.actionOn == ETCAxis.ActionOn.Press)
        this.axisX.DoDirectAction();
      if (this.axisY.actionOn == ETCAxis.ActionOn.Down && (this.axisY.axisState == ETCAxis.AxisState.DownUp || this.axisY.axisState == ETCAxis.AxisState.DownDown))
        this.axisY.DoDirectAction();
      else if (this.axisY.actionOn == ETCAxis.ActionOn.Press)
        this.axisY.DoDirectAction();
      this.onMove.Invoke(new Vector2(this.axisX.axisValue, this.axisY.axisValue));
      this.onMoveSpeed.Invoke(new Vector2(this.axisX.axisSpeedValue, this.axisY.axisSpeedValue));
    }
    else if ((double) this.axisX.axisValue == 0.0 && (double) this.axisY.axisValue == 0.0 && this.OldTmpAxis != Vector2.zero)
      this.onMoveEnd.Invoke();
    float num1 = 1f;
    if (this.axisX.invertedAxis)
      num1 = -1f;
    if ((double) this.OldTmpAxis.x == 0.0 && (double) Mathf.Abs(this.axisX.axisValue) > 0.0)
    {
      if ((double) this.axisX.axisValue * (double) num1 > 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.DownRight;
        this.OnDownRight.Invoke();
      }
      else if ((double) this.axisX.axisValue * (double) num1 < 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.DownLeft;
        this.OnDownLeft.Invoke();
      }
      else
        this.axisX.axisState = ETCAxis.AxisState.None;
    }
    else if (this.axisX.axisState != ETCAxis.AxisState.None)
    {
      if ((double) this.axisX.axisValue * (double) num1 > 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.PressRight;
        this.OnPressRight.Invoke();
      }
      else if ((double) this.axisX.axisValue * (double) num1 < 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.PressLeft;
        this.OnPressLeft.Invoke();
      }
      else
        this.axisX.axisState = ETCAxis.AxisState.None;
    }
    float num2 = 1f;
    if (this.axisY.invertedAxis)
      num2 = -1f;
    if ((double) this.OldTmpAxis.y == 0.0 && (double) Mathf.Abs(this.axisY.axisValue) > 0.0)
    {
      if ((double) this.axisY.axisValue * (double) num2 > 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.DownUp;
        this.OnDownUp.Invoke();
      }
      else if ((double) this.axisY.axisValue * (double) num2 < 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.DownDown;
        this.OnDownDown.Invoke();
      }
      else
        this.axisY.axisState = ETCAxis.AxisState.None;
    }
    else if (this.axisY.axisState != ETCAxis.AxisState.None)
    {
      if ((double) this.axisY.axisValue * (double) num2 > 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.PressUp;
        this.OnPressUp.Invoke();
      }
      else if ((double) this.axisY.axisValue * (double) num2 < 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.PressDown;
        this.OnPressDown.Invoke();
      }
      else
        this.axisY.axisState = ETCAxis.AxisState.None;
    }
    this.tmpAxis = Vector2.zero;
  }

  protected override void SetVisible()
  {
    if (!Application.isPlaying)
      return;
    if (!this._visible)
      this.cachedImage.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    else
      this.cachedImage.color = new Color(1f, 1f, 1f, 1f);
  }

  protected override void SetActivated()
  {
    if (this._activated)
      return;
    this.isOnDrag = false;
    this.isOnTouch = false;
    this.tmpAxis = Vector2.zero;
    this.OldTmpAxis = Vector2.zero;
    this.axisX.axisState = ETCAxis.AxisState.None;
    this.axisY.axisState = ETCAxis.AxisState.None;
    if (!this.axisX.isEnertia && !this.axisY.isEnertia)
    {
      this.axisX.ResetAxis();
      this.axisY.ResetAxis();
    }
    this.pointId = -1;
  }

  [Serializable]
  public class OnMoveStartHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnMoveHandler : UnityEvent<Vector2>
  {
  }

  [Serializable]
  public class OnMoveSpeedHandler : UnityEvent<Vector2>
  {
  }

  [Serializable]
  public class OnMoveEndHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnTouchStartHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnTouchUPHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownUpHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownDownHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownLeftHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownRightHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressUpHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressDownHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressLeftHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressRightHandler : UnityEvent
  {
  }
}
