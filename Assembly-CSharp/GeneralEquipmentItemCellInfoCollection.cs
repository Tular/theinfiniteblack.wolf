﻿// Decompiled with JetBrains decompiler
// Type: GeneralEquipmentItemCellInfoCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class GeneralEquipmentItemCellInfoCollection : IPooledScrollViewCellInfoCollection
{
  private readonly List<IScrollViewCellInfo> _evaluatedValues = new List<IScrollViewCellInfo>(100);
  private readonly Dictionary<EquipmentItem, FuckUp> _fuckUps = new Dictionary<EquipmentItem, FuckUp>(100);
  public Transform prefHeading;
  public Transform prefItemCell;
  private int _dataResetFrame;
  private int _dataModifiedFrame;
  private List<EquipmentItem> _itemsData;
  private readonly Dictionary<string, GeneralEquipmentItemScrollGroupHeadingCellInfo> _headingPool;
  private int _lastFrameEvaluated;

  public GeneralEquipmentItemCellInfoCollection(Transform cellPrefab, Transform headingPrefab)
  {
    this.prefItemCell = cellPrefab;
    this.prefHeading = headingPrefab;
    this.dataWasReset = false;
    this._headingPool = new Dictionary<string, GeneralEquipmentItemScrollGroupHeadingCellInfo>(100);
  }

  public int Count
  {
    get
    {
      return this.values.Count<IScrollViewCellInfo>();
    }
  }

  public IScrollViewCellInfo first
  {
    get
    {
      return this.values.FirstOrDefault<IScrollViewCellInfo>();
    }
  }

  public IScrollViewCellInfo last
  {
    get
    {
      return this.values.LastOrDefault<IScrollViewCellInfo>();
    }
  }

  public bool dataWasReset
  {
    get
    {
      return this._dataResetFrame == Time.frameCount - 1;
    }
    private set
    {
      if (!value)
        return;
      this._dataResetFrame = Time.frameCount;
    }
  }

  public bool dataWasModified
  {
    get
    {
      return true;
    }
    private set
    {
      if (!value)
        return;
      this._dataModifiedFrame = Time.frameCount;
    }
  }

  public IEnumerable<IScrollViewCellInfo> values
  {
    get
    {
      this.EvaluateValuesExpression();
      return (IEnumerable<IScrollViewCellInfo>) this._evaluatedValues;
    }
  }

  public void DumpDictionaryData()
  {
  }

  private void EvaluateValuesExpression()
  {
    if (this._itemsData == null)
      return;
    if (this._lastFrameEvaluated != Time.frameCount)
    {
      ItemType itemType = ItemType.NULL;
      for (int index = 0; index < this._itemsData.Count; ++index)
      {
        if (!this._fuckUps.ContainsKey(this._itemsData[index]))
          this._fuckUps.Add(this._itemsData[index], new FuckUp(this._itemsData[index], this.prefItemCell));
        this._fuckUps[this._itemsData[index]].Add();
      }
      this._evaluatedValues.Clear();
      foreach (EquipmentItem index in (IEnumerable<EquipmentItem>) this._fuckUps.Keys.OrderBy<EquipmentItem, ItemType>((Func<EquipmentItem, ItemType>) (i => i.Type)).ThenBy<EquipmentItem, string>((Func<EquipmentItem, string>) (i => i.LongName)))
      {
        if (this._fuckUps[index].hasItems)
        {
          if (itemType != index.Type)
          {
            itemType = index.Type;
            this._evaluatedValues.Add((IScrollViewCellInfo) this.GetHeadingInfo(itemType));
          }
          this._fuckUps[index].AddTo(this._evaluatedValues);
        }
      }
    }
    this._lastFrameEvaluated = Time.frameCount;
  }

  private GeneralEquipmentItemScrollGroupHeadingCellInfo GetHeadingInfo(ItemType itemType)
  {
    string key = ((Enum) itemType).ToString();
    if (!this._headingPool.ContainsKey(key))
      this._headingPool.Add(key, new GeneralEquipmentItemScrollGroupHeadingCellInfo(this.prefHeading)
      {
        headingText = key
      });
    return this._headingPool[key];
  }

  public void Add(IScrollViewCellInfo cellInfo)
  {
    throw new NotImplementedException();
  }

  public void Remove(IScrollViewCellInfo cellInfo)
  {
    throw new NotImplementedException();
  }

  public void UpdateWith(List<EquipmentItem> items)
  {
    this._itemsData = items.OrderBy<EquipmentItem, ItemType>((Func<EquipmentItem, ItemType>) (i => i.Type)).ToList<EquipmentItem>();
  }

  public void Clear()
  {
    this._itemsData = (List<EquipmentItem>) null;
    using (Dictionary<EquipmentItem, FuckUp>.KeyCollection.Enumerator enumerator = this._fuckUps.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this._fuckUps[enumerator.Current].Clear();
    }
    this._evaluatedValues.Clear();
    this.dataWasReset = true;
  }
}
