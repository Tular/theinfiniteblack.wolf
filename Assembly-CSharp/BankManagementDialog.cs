﻿// Decompiled with JetBrains decompiler
// Type: BankManagementDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class BankManagementDialog : DialogBase
{
  public GameObject itemTypeTabsRoot;
  public UILabel title;
  public UILabel space;
  public int itemTypeToggleGroup;
  public BankItemScrollView itemsScrollView;
  private BankViewContext _viewContext;
  private System.Action _listRefreshAction;
  private int _bankLastChangedId;

  public void ShowEngines()
  {
    this._viewContext.itemType = ItemType.ENGINE;
    this.itemsScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowEngines);
  }

  public void ShowWeapons()
  {
    this._viewContext.itemType = ItemType.WEAPON;
    this.itemsScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowWeapons);
  }

  public void ShowComputers()
  {
    this._viewContext.itemType = ItemType.COMPUTER;
    this.itemsScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowComputers);
  }

  public void ShowArmor()
  {
    this._viewContext.itemType = ItemType.ARMOR;
    this.itemsScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowArmor);
  }

  public void ShowHarvesters()
  {
    this._viewContext.itemType = ItemType.HARVESTER;
    this.itemsScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowHarvesters);
  }

  public void ShowStorage()
  {
    this._viewContext.itemType = ItemType.STORAGE;
    this.itemsScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowStorage);
  }

  public void ShowSpecials()
  {
    this._viewContext.itemType = ItemType.SPECIAL;
    this.itemsScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowSpecials);
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  protected override void OnAwake()
  {
    foreach (UIToggle componentsInChild in this.itemTypeTabsRoot.GetComponentsInChildren<UIToggle>())
      componentsInChild.group = this.itemTypeToggleGroup;
  }

  protected override void OnShow()
  {
    base.OnShow();
    this._bankLastChangedId = 0;
    this._viewContext = new BankViewContext();
    this.itemsScrollView.context = this._viewContext;
    this.title.text = string.Format("{0}' Bank", (object) TibProxy.gameState.MyPlayer.Name);
    this.space.text = string.Format("{0} of {1} bank slots used", (object) TibProxy.gameState.Bank.ItemCount, (object) TibProxy.gameState.Buyables.BankCap);
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.itemsScrollView.Clear();
  }

  private void Update()
  {
    if (TibProxy.gameState.Bank.LastChangeFlagID == this._bankLastChangedId)
      return;
    if (this._listRefreshAction != null)
      this._listRefreshAction();
    this._bankLastChangedId = TibProxy.gameState.Bank.LastChangeFlagID;
  }
}
