﻿// Decompiled with JetBrains decompiler
// Type: TwoSwipe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class TwoSwipe : MonoBehaviour
{
  public GameObject trail;
  public Text swipeData;

  private void OnEnable()
  {
    EasyTouch.On_SwipeStart2Fingers += new EasyTouch.SwipeStart2FingersHandler(this.On_SwipeStart2Fingers);
    EasyTouch.On_Swipe2Fingers += new EasyTouch.Swipe2FingersHandler(this.On_Swipe2Fingers);
    EasyTouch.On_SwipeEnd2Fingers += new EasyTouch.SwipeEnd2FingersHandler(this.On_SwipeEnd2Fingers);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_SwipeStart2Fingers -= new EasyTouch.SwipeStart2FingersHandler(this.On_SwipeStart2Fingers);
    EasyTouch.On_Swipe2Fingers -= new EasyTouch.Swipe2FingersHandler(this.On_Swipe2Fingers);
    EasyTouch.On_SwipeEnd2Fingers -= new EasyTouch.SwipeEnd2FingersHandler(this.On_SwipeEnd2Fingers);
  }

  private void On_SwipeStart2Fingers(Gesture gesture)
  {
    this.swipeData.text = "You start a swipe";
  }

  private void On_Swipe2Fingers(Gesture gesture)
  {
    this.trail.transform.position = gesture.GetTouchToWorldPoint(5f);
  }

  private void On_SwipeEnd2Fingers(Gesture gesture)
  {
    float swipeOrDragAngle = gesture.GetSwipeOrDragAngle();
    this.swipeData.text = "Last swipe : " + gesture.swipe.ToString() + " /  vector : " + (object) gesture.swipeVector.normalized + " / angle : " + swipeOrDragAngle.ToString("f2");
  }
}
