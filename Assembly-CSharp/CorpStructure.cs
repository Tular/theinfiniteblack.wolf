﻿// Decompiled with JetBrains decompiler
// Type: CorpStructure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using UnityEngine;

public class CorpStructure
{
  private readonly Dictionary<CorpRankType, List<PlayerDirectoryCellInfo>> _rankLookup = new Dictionary<CorpRankType, List<PlayerDirectoryCellInfo>>();
  private readonly PlayerDirectoryCellInfo _corpNameHeading;
  private readonly PlayerDirectoryCellInfo _leadersHeading;
  private readonly PlayerDirectoryCellInfo _supersHeading;
  private readonly PlayerDirectoryCellInfo _officersHeading;
  private readonly PlayerDirectoryCellInfo _membersHeading;
  private readonly PlayerDirectoryCellInfo _recruitsHeading;
  private ClientCorporation _corp;

  public CorpStructure(Transform headingPrefab)
  {
    this._corpNameHeading = new PlayerDirectoryCellInfo(headingPrefab, "> NO CORP <");
    this._leadersHeading = new PlayerDirectoryCellInfo(headingPrefab, "- LEADERS -", "-LEADERS-");
    this._supersHeading = new PlayerDirectoryCellInfo(headingPrefab, "- SUPERS -", "-SUPERS-");
    this._officersHeading = new PlayerDirectoryCellInfo(headingPrefab, "- OFFICERS -", "-OFFICERS-");
    this._membersHeading = new PlayerDirectoryCellInfo(headingPrefab, "- MEMBERS -", "-MEMBERS-");
    this._recruitsHeading = new PlayerDirectoryCellInfo(headingPrefab, "- RECRUITS -", "-RECRUITS-");
    this._rankLookup.Add(CorpRankType.LEADER, new List<PlayerDirectoryCellInfo>());
    this._rankLookup.Add(CorpRankType.SUPEROFFICER, new List<PlayerDirectoryCellInfo>());
    this._rankLookup.Add(CorpRankType.OFFICER, new List<PlayerDirectoryCellInfo>());
    this._rankLookup.Add(CorpRankType.MEMBER, new List<PlayerDirectoryCellInfo>());
    this._rankLookup.Add(CorpRankType.RECRUIT, new List<PlayerDirectoryCellInfo>());
  }

  public ClientCorporation corporation
  {
    get
    {
      return this._corp;
    }
    set
    {
      this._corp = value;
      if (this._corp == null)
        return;
      StringBuilder stringBuilder = new StringBuilder(256);
      this._corp.AppendName(stringBuilder, true);
      Markup.GetNGUI(stringBuilder);
      stringBuilder.Append("[-][-][-][/b][/i][/u][/s]");
      this._corpNameHeading.SetDisplayText(stringBuilder.ToString());
    }
  }

  [DebuggerHidden]
  public IEnumerable<PlayerDirectoryCellInfo> GetPlayerInfos(Dictionary<int, PlayerDirectoryCellInfo> onlinePlayersLookup)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CorpStructure.\u003CGetPlayerInfos\u003Ec__Iterator15 infosCIterator15 = new CorpStructure.\u003CGetPlayerInfos\u003Ec__Iterator15()
    {
      onlinePlayersLookup = onlinePlayersLookup,
      \u003C\u0024\u003EonlinePlayersLookup = onlinePlayersLookup,
      \u003C\u003Ef__this = this
    };
    // ISSUE: reference to a compiler-generated field
    infosCIterator15.\u0024PC = -2;
    return (IEnumerable<PlayerDirectoryCellInfo>) infosCIterator15;
  }

  private void ClearRankLookup()
  {
    this._rankLookup[CorpRankType.LEADER].Clear();
    this._rankLookup[CorpRankType.SUPEROFFICER].Clear();
    this._rankLookup[CorpRankType.OFFICER].Clear();
    this._rankLookup[CorpRankType.MEMBER].Clear();
    this._rankLookup[CorpRankType.RECRUIT].Clear();
  }
}
