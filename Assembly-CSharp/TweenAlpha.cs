﻿// Decompiled with JetBrains decompiler
// Type: TweenAlpha
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[AddComponentMenu("NGUI/Tween/Tween Alpha")]
public class TweenAlpha : UITweener
{
  [Range(0.0f, 1f)]
  public float from = 1f;
  [Range(0.0f, 1f)]
  public float to = 1f;
  private bool mCached;
  private UIRect mRect;
  private Material mMat;
  private SpriteRenderer mSr;

  [Obsolete("Use 'value' instead")]
  public float alpha
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  private void Cache()
  {
    this.mCached = true;
    this.mRect = this.GetComponent<UIRect>();
    this.mSr = this.GetComponent<SpriteRenderer>();
    if (!((UnityEngine.Object) this.mRect == (UnityEngine.Object) null) || !((UnityEngine.Object) this.mSr == (UnityEngine.Object) null))
      return;
    Renderer component = this.GetComponent<Renderer>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      this.mMat = component.material;
    if (!((UnityEngine.Object) this.mMat == (UnityEngine.Object) null))
      return;
    this.mRect = this.GetComponentInChildren<UIRect>();
  }

  public float value
  {
    get
    {
      if (!this.mCached)
        this.Cache();
      if ((UnityEngine.Object) this.mRect != (UnityEngine.Object) null)
        return this.mRect.alpha;
      if ((UnityEngine.Object) this.mSr != (UnityEngine.Object) null)
        return this.mSr.color.a;
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.color.a;
      return 1f;
    }
    set
    {
      if (!this.mCached)
        this.Cache();
      if ((UnityEngine.Object) this.mRect != (UnityEngine.Object) null)
        this.mRect.alpha = value;
      else if ((UnityEngine.Object) this.mSr != (UnityEngine.Object) null)
      {
        Color color = this.mSr.color;
        color.a = value;
        this.mSr.color = color;
      }
      else
      {
        if (!((UnityEngine.Object) this.mMat != (UnityEngine.Object) null))
          return;
        Color color = this.mMat.color;
        color.a = value;
        this.mMat.color = color;
      }
    }
  }

  protected override void OnUpdate(float factor, bool isFinished)
  {
    this.value = Mathf.Lerp(this.from, this.to, factor);
  }

  public static TweenAlpha Begin(GameObject go, float duration, float alpha)
  {
    TweenAlpha tweenAlpha = UITweener.Begin<TweenAlpha>(go, duration);
    tweenAlpha.from = tweenAlpha.value;
    tweenAlpha.to = alpha;
    if ((double) duration <= 0.0)
    {
      tweenAlpha.Sample(1f, true);
      tweenAlpha.enabled = false;
    }
    return tweenAlpha;
  }

  public override void SetStartToCurrentValue()
  {
    this.from = this.value;
  }

  public override void SetEndToCurrentValue()
  {
    this.to = this.value;
  }
}
