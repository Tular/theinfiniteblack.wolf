﻿// Decompiled with JetBrains decompiler
// Type: ShipProfileTexture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using UnityEngine;

[RequireComponent(typeof (UITexture))]
public class ShipProfileTexture : MonoBehaviour
{
  public string resourcePath = "profile";
  private UITexture _shipTexture;
  private bool _isVisible;
  private ShipClass _currentShipClass;

  public bool isVisible
  {
    get
    {
      return this._isVisible;
    }
    set
    {
      this._isVisible = value;
      this._shipTexture.enabled = this._isVisible;
      if (this._isVisible)
        return;
      this.UnloadTexture();
    }
  }

  public void Show(ShipClass shipClass)
  {
    this.isVisible = true;
    this.UpdateShipProfile(shipClass);
  }

  public void Hide()
  {
    this.isVisible = false;
  }

  public void Unload()
  {
    this.isVisible = false;
    this.UnloadTexture();
  }

  private void UpdateShipProfile(ShipClass shipClass)
  {
    if (this._currentShipClass == shipClass)
      return;
    this.UnloadTexture();
    this._currentShipClass = shipClass;
    if (this._currentShipClass == ShipClass.NULL || this._currentShipClass == ShipClass.None || this._currentShipClass == ShipClass.LAST)
      return;
    string path = string.Format("{0}/profile_ship_{1}", (object) this.resourcePath, (object) shipClass);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
    {
      this.LogW(string.Format("did not load {0}", (object) path));
    }
    else
    {
      this._shipTexture.mainTexture = (Texture) texture2D;
      this._shipTexture.width = texture2D.width;
      this._shipTexture.height = texture2D.height;
      Transform transform = this._shipTexture.transform;
      Vector3 vector3_1;
      if (shipClass == ShipClass.Shuttle)
      {
        Vector3 vector3_2 = new Vector3(0.5f, 0.5f, 0.5f);
        this._shipTexture.transform.localScale = vector3_2;
        vector3_1 = vector3_2;
      }
      else
      {
        Vector3 one = Vector3.one;
        this._shipTexture.transform.localScale = one;
        vector3_1 = one;
      }
      transform.localScale = vector3_1;
    }
  }

  private void UnloadTexture()
  {
    if ((Object) this._shipTexture.mainTexture != (Object) null)
    {
      Texture mainTexture = this._shipTexture.mainTexture;
      this._shipTexture.mainTexture = (Texture) null;
      Resources.UnloadAsset((Object) mainTexture);
    }
    this._currentShipClass = ShipClass.NULL;
  }

  private void Awake()
  {
    this._shipTexture = this.GetComponent<UITexture>();
    this._currentShipClass = ShipClass.NULL;
    this.Unload();
  }

  private void OnDisable()
  {
    this.UnloadTexture();
  }
}
