﻿// Decompiled with JetBrains decompiler
// Type: UIProfileButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIButton))]
public class UIProfileButton : MonoBehaviour
{
  public UIToggle checkbox;
  private UIButton mBtn;

  private void Awake()
  {
    this.mBtn = this.GetComponent<UIButton>();
    this.mBtn.isEnabled = (Object) this.checkbox != (Object) null && this.checkbox.value;
  }

  private void OnEnable()
  {
    if (!((Object) this.checkbox != (Object) null))
      return;
    EventDelegate.Add(this.checkbox.onChange, new EventDelegate.Callback(this.OnCheckboxState));
  }

  private void OnDisable()
  {
    if (!((Object) this.checkbox != (Object) null))
      return;
    EventDelegate.Remove(this.checkbox.onChange, new EventDelegate.Callback(this.OnCheckboxState));
  }

  private void OnCheckboxState()
  {
    this.mBtn.isEnabled = UIToggle.current.value;
  }
}
