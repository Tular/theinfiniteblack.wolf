﻿// Decompiled with JetBrains decompiler
// Type: TestChatDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class TestChatDialog : MonoBehaviour
{
  private float _nextEventTime = float.MaxValue;
  public UILabel eventCountLabel;
  public float eventDelay;
  public int eventsSent;
  private bool _isRunning;

  [ContextMenu("Add Local Player")]
  public void AddLocalPlayer()
  {
    MockSector moqMyLoc = MockTibProxy.MockInstance.moqGameState.moqMyLoc;
    MockShip random = MockShip.GenerateRandom((IGameState) MockTibProxy.MockInstance.moqGameState);
    random.SetLocation(moqMyLoc);
    random.moqPlayer.Online = true;
    TibProxy.Instance.Show(new PlayerOnlineStatusChangedEventArgs((IGameState) MockTibProxy.MockInstance.moqGameState, (ClientPlayer) random.moqPlayer, random.moqPlayer.Online));
  }

  [ContextMenu("Add Non-Local Friend")]
  public void AddNonLocalFriend()
  {
    MockClientPlayer random = MockClientPlayer.GenerateRandom((IGameState) MockTibProxy.MockInstance.moqGameState);
    random.Online = true;
    TibProxy.mySettings.Social.AddFriend(random.Name);
    TibProxy.Instance.Show(new PlayerOnlineStatusChangedEventArgs((IGameState) MockTibProxy.MockInstance.moqGameState, (ClientPlayer) random, random.Online));
  }

  [ContextMenu("Add Local Friend")]
  public void AddLocalFriend()
  {
    MockSector moqMyLoc = MockTibProxy.MockInstance.moqGameState.moqMyLoc;
    MockShip random = MockShip.GenerateRandom((IGameState) MockTibProxy.MockInstance.moqGameState);
    random.SetLocation(moqMyLoc);
    random.moqPlayer.Online = true;
    TibProxy.mySettings.Social.AddFriend(random.moqPlayer.Name);
    TibProxy.Instance.Show(new PlayerOnlineStatusChangedEventArgs((IGameState) MockTibProxy.MockInstance.moqGameState, (ClientPlayer) random.moqPlayer, random.moqPlayer.Online));
  }

  [ContextMenu("Single Chat")]
  public void SendSingleChatEvent()
  {
    this.SendChatEvent();
  }

  [ContextMenu("Start")]
  public void StartMockChat()
  {
    this._nextEventTime = RealTime.time + this.eventDelay;
    this._isRunning = true;
  }

  [ContextMenu("Stop")]
  public void StopMockChat()
  {
    this._nextEventTime = float.MaxValue;
    this._isRunning = false;
  }

  protected void SendChatEvent()
  {
    Debug.Log((object) string.Format("sending test chat"));
    TibProxy.Instance.Show(new ChatEventArgs(TibProxy.gameState, "text", "receiver", "sender", (ChatType) Random.Range(0, 10)));
  }

  private void Update()
  {
    if (!this._isRunning || (double) this._nextEventTime > (double) RealTime.time)
      return;
    this.SendChatEvent();
    ++this.eventsSent;
    if ((bool) ((Object) this.eventCountLabel))
      this.eventCountLabel.text = string.Format("events sent: {0}", (object) this.eventsSent);
    this._nextEventTime = this._nextEventTime + this.eventDelay;
  }
}
