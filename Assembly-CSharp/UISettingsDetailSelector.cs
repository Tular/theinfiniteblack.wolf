﻿// Decompiled with JetBrains decompiler
// Type: UISettingsDetailSelector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Unity;
using UnityEngine;

[RequireComponent(typeof (UISprite))]
[ExecuteInEditMode]
[RequireComponent(typeof (UIToggle))]
public class UISettingsDetailSelector : MonoBehaviour
{
  public string labelText;
  [NonSerialized]
  public bool update;
  public UILabel label;
  protected UISprite mSprite;
  protected UIToggle mToggle;
  public UISettingsGroup mSettingsGroup;
  public UIWidget mSettingsWidget;

  public string text
  {
    get
    {
      if ((UnityEngine.Object) this.label != (UnityEngine.Object) null)
        return this.label.text;
      return string.Empty;
    }
    set
    {
      this.labelText = value;
      if (!((UnityEngine.Object) this.label != (UnityEngine.Object) null))
        return;
      this.label.text = value;
    }
  }

  public bool isSelected
  {
    get
    {
      return this.mToggle.value;
    }
    set
    {
      if (this.mToggle.value == value)
        return;
      this.mToggle.value = value;
    }
  }

  protected void OnDisable()
  {
    this.mToggle.activeSprite.alpha = 0.0f;
    this.isSelected = false;
  }

  public void Initialize()
  {
    if ((UnityEngine.Object) this.mSprite == (UnityEngine.Object) null)
      this.mSprite = this.GetComponent<UISprite>();
    if ((UnityEngine.Object) this.mToggle == (UnityEngine.Object) null)
      this.mToggle = this.GetComponent<UIToggle>();
    if ((UnityEngine.Object) this.mSettingsGroup == (UnityEngine.Object) null)
      this.mSettingsGroup = NGUITools.FindInParents<UISettingsGroup>(this.gameObject);
    if ((UnityEngine.Object) this.mSettingsGroup != (UnityEngine.Object) null)
    {
      if (!((UnityEngine.Object) this.mSettingsWidget == (UnityEngine.Object) null))
        return;
      this.mSettingsWidget = this.mSettingsGroup.GetComponent<UIWidget>();
    }
    else
    {
      Exception e = new Exception("UISettingsDetailSelector must have an ancester of type UISettingsGroup");
      this.LogE(e);
      throw e;
    }
  }

  protected void Awake()
  {
    this.Initialize();
  }

  public void UpdateAnchors()
  {
    if ((UnityEngine.Object) this.mSettingsWidget == (UnityEngine.Object) null)
    {
      this.LogD("mSettingsWidget is null");
    }
    else
    {
      this.mSprite.leftAnchor.target = this.mSettingsWidget.transform;
      this.mSprite.rightAnchor.target = this.mSettingsWidget.transform;
      this.mSprite.ResetAnchors();
    }
  }

  public void OnToggleChanged()
  {
    if (!((UnityEngine.Object) this.mSettingsGroup != (UnityEngine.Object) null))
      return;
    this.mSettingsGroup.OnDetailSelectorChanged(this, this.mToggle.value);
  }

  protected void Start()
  {
    this.label.text = this.labelText;
  }

  protected void Update()
  {
    if (!Application.isEditor || !this.update)
      return;
    this.update = false;
    this.UpdateAnchors();
  }
}
