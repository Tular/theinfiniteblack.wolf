﻿// Decompiled with JetBrains decompiler
// Type: PlanetDialogContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;

public class PlanetDialogContext
{
  private ShipClass _selectedShipClass;

  public Planet planet { get; set; }

  public ShipClass selectedShipClass
  {
    get
    {
      return this._selectedShipClass;
    }
    set
    {
      this._selectedShipClass = value;
      this.selectedShipClassName = this._selectedShipClass.Name();
      this.selectedShipClassCreditCostText = string.Format("{0:$###,###}", (object) this._selectedShipClass.CreditCost());
      this.selectedShipClassBlackDollarCost = string.Format("{0:###,###}", (object) this._selectedShipClass.BlackDollarCost());
      this.selectedShipClassRewardPointCost = string.Format("{0:###,###}", (object) this._selectedShipClass.RewardPointCost());
      this.selectedShipClassCombatPointCost = string.Format("{0:###,###}", (object) this._selectedShipClass.CombatPointCost());
    }
  }

  public string dialogSubtitleText { get; set; }

  public string selectedShipClassName { get; private set; }

  public string selectedShipClassCreditCostText { get; private set; }

  public string selectedShipClassBlackDollarCost { get; private set; }

  public string selectedShipClassRewardPointCost { get; private set; }

  public string selectedShipClassCombatPointCost { get; private set; }
}
