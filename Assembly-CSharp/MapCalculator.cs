﻿// Decompiled with JetBrains decompiler
// Type: MapCalculator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MapCalculator : MonoBehaviour
{
  public float cameraSize = 1f;
  public Vector2 viewportPosition;
  public Rect viewport;
  public int mapSize;
  public int sectorWidth;
  public int sectorHeight;
  public int connectorLength;
  public float baseViewportWidth;
  public float baseViewportHeight;
  public Vector2 minViewportPosition;
  public Vector2 maxViewportPosition;
  public float minVisibleX;
  public float maxVisibleX;
  public float minVisibleY;
  public float maxVisibleY;
  public Rect mapBounds;
  public List<Vector2> visibleSectors;

  private void Update()
  {
    this.viewport.width = this.baseViewportWidth * this.cameraSize;
    this.viewport.height = this.baseViewportHeight * this.cameraSize;
    this.mapBounds.width = (float) (this.mapSize * this.sectorWidth + (this.mapSize - 1) * this.connectorLength);
    this.mapBounds.height = (float) (this.mapSize * this.sectorHeight + (this.mapSize - 1) * this.connectorLength);
    this.minViewportPosition.x = this.viewport.width / 2f;
    this.minViewportPosition.y = this.viewport.height / 2f;
    this.maxViewportPosition.x = this.mapBounds.width - this.viewport.width / 2f;
    this.maxViewportPosition.y = this.mapBounds.height - this.viewport.height / 2f;
    if ((double) this.viewportPosition.x < (double) this.minViewportPosition.x)
      this.viewportPosition.x = this.minViewportPosition.x;
    else if ((double) this.viewportPosition.x > (double) this.maxViewportPosition.x)
      this.viewportPosition.x = this.maxViewportPosition.x;
    if ((double) this.viewportPosition.y < (double) this.minViewportPosition.y)
      this.viewportPosition.y = this.minViewportPosition.y;
    else if ((double) this.viewportPosition.y > (double) this.maxViewportPosition.y)
      this.viewportPosition.y = this.maxViewportPosition.y;
    this.viewport.center = this.viewportPosition;
    this.minVisibleX = Mathf.Floor(this.viewport.x / (float) (this.sectorWidth + this.connectorLength));
    this.minVisibleY = Mathf.Floor(this.viewport.y / (float) (this.sectorHeight + this.connectorLength));
    this.maxVisibleX = this.minVisibleX + Mathf.Floor(this.viewport.width / (float) (this.sectorWidth + this.connectorLength));
    this.maxVisibleY = this.minVisibleY + Mathf.Floor(this.viewport.height / (float) (this.sectorHeight + this.connectorLength));
  }
}
