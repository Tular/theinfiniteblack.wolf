﻿// Decompiled with JetBrains decompiler
// Type: EasyTouchTrigger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Event/EasyTouch/Trigger")]
[Serializable]
public class EasyTouchTrigger : MonoBehaviour
{
  [SerializeField]
  public List<EasyTouchTrigger.EasyTouchReceiver> receivers = new List<EasyTouchTrigger.EasyTouchReceiver>();

  private void Start()
  {
    if (!(UnityEngine.Object.FindObjectOfType(typeof (EasyTouch)) == (UnityEngine.Object) null))
      return;
    Debug.LogWarning((object) "EasyTouch doesn't exist in your scene");
  }

  private void OnEnable()
  {
    this.SubscribeEasyTouchEvent();
  }

  private void OnDisable()
  {
    this.UnsubscribeEasyTouchEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEasyTouchEvent();
  }

  private void SubscribeEasyTouchEvent()
  {
    if (this.IsRecevier4(EasyTouch.EventName.On_Cancel))
      EasyTouch.On_Cancel += new EasyTouch.TouchCancelHandler(this.On_Cancel);
    if (this.IsRecevier4(EasyTouch.EventName.On_TouchStart))
      EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.On_TouchStart);
    if (this.IsRecevier4(EasyTouch.EventName.On_TouchDown))
      EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.On_TouchDown);
    if (this.IsRecevier4(EasyTouch.EventName.On_TouchUp))
      EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.On_TouchUp);
    if (this.IsRecevier4(EasyTouch.EventName.On_SimpleTap))
      EasyTouch.On_SimpleTap += new EasyTouch.SimpleTapHandler(this.On_SimpleTap);
    if (this.IsRecevier4(EasyTouch.EventName.On_LongTapStart))
      EasyTouch.On_LongTapStart += new EasyTouch.LongTapStartHandler(this.On_LongTapStart);
    if (this.IsRecevier4(EasyTouch.EventName.On_LongTap))
      EasyTouch.On_LongTap += new EasyTouch.LongTapHandler(this.On_LongTap);
    if (this.IsRecevier4(EasyTouch.EventName.On_LongTapEnd))
      EasyTouch.On_LongTapEnd += new EasyTouch.LongTapEndHandler(this.On_LongTapEnd);
    if (this.IsRecevier4(EasyTouch.EventName.On_DoubleTap))
      EasyTouch.On_DoubleTap += new EasyTouch.DoubleTapHandler(this.On_DoubleTap);
    if (this.IsRecevier4(EasyTouch.EventName.On_DragStart))
      EasyTouch.On_DragStart += new EasyTouch.DragStartHandler(this.On_DragStart);
    if (this.IsRecevier4(EasyTouch.EventName.On_Drag))
      EasyTouch.On_Drag += new EasyTouch.DragHandler(this.On_Drag);
    if (this.IsRecevier4(EasyTouch.EventName.On_DragEnd))
      EasyTouch.On_DragEnd += new EasyTouch.DragEndHandler(this.On_DragEnd);
    if (this.IsRecevier4(EasyTouch.EventName.On_SwipeStart))
      EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.On_SwipeStart);
    if (this.IsRecevier4(EasyTouch.EventName.On_Swipe))
      EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.On_Swipe);
    if (this.IsRecevier4(EasyTouch.EventName.On_SwipeEnd))
      EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
    if (this.IsRecevier4(EasyTouch.EventName.On_TouchStart2Fingers))
      EasyTouch.On_TouchStart2Fingers += new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_TouchDown2Fingers))
      EasyTouch.On_TouchDown2Fingers += new EasyTouch.TouchDown2FingersHandler(this.On_TouchDown2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_TouchUp2Fingers))
      EasyTouch.On_TouchUp2Fingers += new EasyTouch.TouchUp2FingersHandler(this.On_TouchUp2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_SimpleTap2Fingers))
      EasyTouch.On_SimpleTap2Fingers += new EasyTouch.SimpleTap2FingersHandler(this.On_SimpleTap2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_LongTapStart2Fingers))
      EasyTouch.On_LongTapStart2Fingers += new EasyTouch.LongTapStart2FingersHandler(this.On_LongTapStart2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_LongTap2Fingers))
      EasyTouch.On_LongTap2Fingers += new EasyTouch.LongTap2FingersHandler(this.On_LongTap2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_LongTapEnd2Fingers))
      EasyTouch.On_LongTapEnd2Fingers += new EasyTouch.LongTapEnd2FingersHandler(this.On_LongTapEnd2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_DoubleTap2Fingers))
      EasyTouch.On_DoubleTap2Fingers += new EasyTouch.DoubleTap2FingersHandler(this.On_DoubleTap2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_SwipeStart2Fingers))
      EasyTouch.On_SwipeStart2Fingers += new EasyTouch.SwipeStart2FingersHandler(this.On_SwipeStart2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_Swipe2Fingers))
      EasyTouch.On_Swipe2Fingers += new EasyTouch.Swipe2FingersHandler(this.On_Swipe2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_SwipeEnd2Fingers))
      EasyTouch.On_SwipeEnd2Fingers += new EasyTouch.SwipeEnd2FingersHandler(this.On_SwipeEnd2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_DragStart2Fingers))
      EasyTouch.On_DragStart2Fingers += new EasyTouch.DragStart2FingersHandler(this.On_DragStart2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_Drag2Fingers))
      EasyTouch.On_Drag2Fingers += new EasyTouch.Drag2FingersHandler(this.On_Drag2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_DragEnd2Fingers))
      EasyTouch.On_DragEnd2Fingers += new EasyTouch.DragEnd2FingersHandler(this.On_DragEnd2Fingers);
    if (this.IsRecevier4(EasyTouch.EventName.On_Pinch))
      EasyTouch.On_Pinch += new EasyTouch.PinchHandler(this.On_Pinch);
    if (this.IsRecevier4(EasyTouch.EventName.On_PinchIn))
      EasyTouch.On_PinchIn += new EasyTouch.PinchInHandler(this.On_PinchIn);
    if (this.IsRecevier4(EasyTouch.EventName.On_PinchOut))
      EasyTouch.On_PinchOut += new EasyTouch.PinchOutHandler(this.On_PinchOut);
    if (this.IsRecevier4(EasyTouch.EventName.On_PinchEnd))
      EasyTouch.On_PinchEnd += new EasyTouch.PinchEndHandler(this.On_PinchEnd);
    if (this.IsRecevier4(EasyTouch.EventName.On_Twist))
      EasyTouch.On_Twist += new EasyTouch.TwistHandler(this.On_Twist);
    if (this.IsRecevier4(EasyTouch.EventName.On_TwistEnd))
      EasyTouch.On_TwistEnd += new EasyTouch.TwistEndHandler(this.On_TwistEnd);
    if (this.IsRecevier4(EasyTouch.EventName.On_OverUIElement))
      EasyTouch.On_OverUIElement += new EasyTouch.OverUIElementHandler(this.On_OverUIElement);
    if (!this.IsRecevier4(EasyTouch.EventName.On_UIElementTouchUp))
      return;
    EasyTouch.On_UIElementTouchUp += new EasyTouch.UIElementTouchUpHandler(this.On_UIElementTouchUp);
  }

  private void UnsubscribeEasyTouchEvent()
  {
    EasyTouch.On_Cancel -= new EasyTouch.TouchCancelHandler(this.On_Cancel);
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.On_TouchUp);
    EasyTouch.On_SimpleTap -= new EasyTouch.SimpleTapHandler(this.On_SimpleTap);
    EasyTouch.On_LongTapStart -= new EasyTouch.LongTapStartHandler(this.On_LongTapStart);
    EasyTouch.On_LongTap -= new EasyTouch.LongTapHandler(this.On_LongTap);
    EasyTouch.On_LongTapEnd -= new EasyTouch.LongTapEndHandler(this.On_LongTapEnd);
    EasyTouch.On_DoubleTap -= new EasyTouch.DoubleTapHandler(this.On_DoubleTap);
    EasyTouch.On_DragStart -= new EasyTouch.DragStartHandler(this.On_DragStart);
    EasyTouch.On_Drag -= new EasyTouch.DragHandler(this.On_Drag);
    EasyTouch.On_DragEnd -= new EasyTouch.DragEndHandler(this.On_DragEnd);
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.On_SwipeStart);
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
    EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    EasyTouch.On_TouchDown2Fingers -= new EasyTouch.TouchDown2FingersHandler(this.On_TouchDown2Fingers);
    EasyTouch.On_TouchUp2Fingers -= new EasyTouch.TouchUp2FingersHandler(this.On_TouchUp2Fingers);
    EasyTouch.On_SimpleTap2Fingers -= new EasyTouch.SimpleTap2FingersHandler(this.On_SimpleTap2Fingers);
    EasyTouch.On_LongTapStart2Fingers -= new EasyTouch.LongTapStart2FingersHandler(this.On_LongTapStart2Fingers);
    EasyTouch.On_LongTap2Fingers -= new EasyTouch.LongTap2FingersHandler(this.On_LongTap2Fingers);
    EasyTouch.On_LongTapEnd2Fingers -= new EasyTouch.LongTapEnd2FingersHandler(this.On_LongTapEnd2Fingers);
    EasyTouch.On_DoubleTap2Fingers -= new EasyTouch.DoubleTap2FingersHandler(this.On_DoubleTap2Fingers);
    EasyTouch.On_SwipeStart2Fingers -= new EasyTouch.SwipeStart2FingersHandler(this.On_SwipeStart2Fingers);
    EasyTouch.On_Swipe2Fingers -= new EasyTouch.Swipe2FingersHandler(this.On_Swipe2Fingers);
    EasyTouch.On_SwipeEnd2Fingers -= new EasyTouch.SwipeEnd2FingersHandler(this.On_SwipeEnd2Fingers);
    EasyTouch.On_DragStart2Fingers -= new EasyTouch.DragStart2FingersHandler(this.On_DragStart2Fingers);
    EasyTouch.On_Drag2Fingers -= new EasyTouch.Drag2FingersHandler(this.On_Drag2Fingers);
    EasyTouch.On_DragEnd2Fingers -= new EasyTouch.DragEnd2FingersHandler(this.On_DragEnd2Fingers);
    EasyTouch.On_Pinch -= new EasyTouch.PinchHandler(this.On_Pinch);
    EasyTouch.On_PinchIn -= new EasyTouch.PinchInHandler(this.On_PinchIn);
    EasyTouch.On_PinchOut -= new EasyTouch.PinchOutHandler(this.On_PinchOut);
    EasyTouch.On_PinchEnd -= new EasyTouch.PinchEndHandler(this.On_PinchEnd);
    EasyTouch.On_Twist -= new EasyTouch.TwistHandler(this.On_Twist);
    EasyTouch.On_TwistEnd -= new EasyTouch.TwistEndHandler(this.On_TwistEnd);
    EasyTouch.On_OverUIElement += new EasyTouch.OverUIElementHandler(this.On_OverUIElement);
    EasyTouch.On_UIElementTouchUp += new EasyTouch.UIElementTouchUpHandler(this.On_UIElementTouchUp);
  }

  private void On_TouchStart(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_TouchStart, gesture);
  }

  private void On_TouchDown(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_TouchDown, gesture);
  }

  private void On_TouchUp(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_TouchUp, gesture);
  }

  private void On_SimpleTap(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_SimpleTap, gesture);
  }

  private void On_DoubleTap(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_DoubleTap, gesture);
  }

  private void On_LongTapStart(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_LongTapStart, gesture);
  }

  private void On_LongTap(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_LongTap, gesture);
  }

  private void On_LongTapEnd(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_LongTapEnd, gesture);
  }

  private void On_SwipeStart(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_SwipeStart, gesture);
  }

  private void On_Swipe(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_Swipe, gesture);
  }

  private void On_SwipeEnd(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_SwipeEnd, gesture);
  }

  private void On_DragStart(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_DragStart, gesture);
  }

  private void On_Drag(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_Drag, gesture);
  }

  private void On_DragEnd(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_DragEnd, gesture);
  }

  private void On_Cancel(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_Cancel, gesture);
  }

  private void On_TouchStart2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_TouchStart2Fingers, gesture);
  }

  private void On_TouchDown2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_TouchDown2Fingers, gesture);
  }

  private void On_TouchUp2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_TouchUp2Fingers, gesture);
  }

  private void On_LongTapStart2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_LongTapStart2Fingers, gesture);
  }

  private void On_LongTap2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_LongTap2Fingers, gesture);
  }

  private void On_LongTapEnd2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_LongTapEnd2Fingers, gesture);
  }

  private void On_DragStart2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_DragStart2Fingers, gesture);
  }

  private void On_Drag2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_Drag2Fingers, gesture);
  }

  private void On_DragEnd2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_DragEnd2Fingers, gesture);
  }

  private void On_SwipeStart2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_SwipeStart2Fingers, gesture);
  }

  private void On_Swipe2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_Swipe2Fingers, gesture);
  }

  private void On_SwipeEnd2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_SwipeEnd2Fingers, gesture);
  }

  private void On_Twist(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_Twist, gesture);
  }

  private void On_TwistEnd(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_TwistEnd, gesture);
  }

  private void On_Pinch(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_Pinch, gesture);
  }

  private void On_PinchOut(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_PinchOut, gesture);
  }

  private void On_PinchIn(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_PinchIn, gesture);
  }

  private void On_PinchEnd(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_PinchEnd, gesture);
  }

  private void On_SimpleTap2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_SimpleTap2Fingers, gesture);
  }

  private void On_DoubleTap2Fingers(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_DoubleTap2Fingers, gesture);
  }

  private void On_UIElementTouchUp(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_UIElementTouchUp, gesture);
  }

  private void On_OverUIElement(Gesture gesture)
  {
    this.TriggerScheduler(EasyTouch.EventName.On_OverUIElement, gesture);
  }

  public void AddTrigger(EasyTouch.EventName ev)
  {
    this.receivers.Add(new EasyTouchTrigger.EasyTouchReceiver()
    {
      enable = true,
      restricted = true,
      eventName = ev,
      gameObject = (GameObject) null,
      otherReceiver = false,
      name = "New trigger"
    });
    if (!Application.isPlaying)
      return;
    this.UnsubscribeEasyTouchEvent();
    this.SubscribeEasyTouchEvent();
  }

  public bool SetTriggerEnable(string triggerName, bool value)
  {
    EasyTouchTrigger.EasyTouchReceiver trigger = this.GetTrigger(triggerName);
    if (trigger == null)
      return false;
    trigger.enable = value;
    return true;
  }

  public bool GetTriggerEnable(string triggerName)
  {
    EasyTouchTrigger.EasyTouchReceiver trigger = this.GetTrigger(triggerName);
    if (trigger != null)
      return trigger.enable;
    return false;
  }

  private void TriggerScheduler(EasyTouch.EventName evnt, Gesture gesture)
  {
    using (List<EasyTouchTrigger.EasyTouchReceiver>.Enumerator enumerator = this.receivers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EasyTouchTrigger.EasyTouchReceiver current = enumerator.Current;
        if (current.enable && current.eventName == evnt && (current.restricted && ((UnityEngine.Object) gesture.pickedObject == (UnityEngine.Object) this.gameObject && current.triggerType == EasyTouchTrigger.ETTType.Object3D || (UnityEngine.Object) gesture.pickedUIElement == (UnityEngine.Object) this.gameObject && current.triggerType == EasyTouchTrigger.ETTType.UI) || !current.restricted && ((UnityEngine.Object) current.gameObject == (UnityEngine.Object) null || (UnityEngine.Object) current.gameObject == (UnityEngine.Object) gesture.pickedObject && current.triggerType == EasyTouchTrigger.ETTType.Object3D || (UnityEngine.Object) gesture.pickedUIElement == (UnityEngine.Object) current.gameObject && current.triggerType == EasyTouchTrigger.ETTType.UI)))
        {
          GameObject gameObject = this.gameObject;
          if (current.otherReceiver && (UnityEngine.Object) current.gameObjectReceiver != (UnityEngine.Object) null)
            gameObject = current.gameObjectReceiver;
          switch (current.parameter)
          {
            case EasyTouchTrigger.ETTParameter.None:
              gameObject.SendMessage(current.methodName, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Gesture:
              gameObject.SendMessage(current.methodName, (object) gesture, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Finger_Id:
              gameObject.SendMessage(current.methodName, (object) gesture.fingerIndex, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Touch_Count:
              gameObject.SendMessage(current.methodName, (object) gesture.touchCount, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Start_Position:
              gameObject.SendMessage(current.methodName, (object) gesture.startPosition, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Position:
              gameObject.SendMessage(current.methodName, (object) gesture.position, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Delta_Position:
              gameObject.SendMessage(current.methodName, (object) gesture.deltaPosition, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Swipe_Type:
              gameObject.SendMessage(current.methodName, (object) gesture.swipe, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Swipe_Length:
              gameObject.SendMessage(current.methodName, (object) gesture.swipeLength, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Swipe_Vector:
              gameObject.SendMessage(current.methodName, (object) gesture.swipeVector, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Delta_Pinch:
              gameObject.SendMessage(current.methodName, (object) gesture.deltaPinch, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.Twist_Anlge:
              gameObject.SendMessage(current.methodName, (object) gesture.twistAngle, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.ActionTime:
              gameObject.SendMessage(current.methodName, (object) gesture.actionTime, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.DeltaTime:
              gameObject.SendMessage(current.methodName, (object) gesture.deltaTime, SendMessageOptions.DontRequireReceiver);
              continue;
            case EasyTouchTrigger.ETTParameter.PickedObject:
              if ((UnityEngine.Object) gesture.pickedObject != (UnityEngine.Object) null)
              {
                gameObject.SendMessage(current.methodName, (object) gesture.pickedObject, SendMessageOptions.DontRequireReceiver);
                continue;
              }
              continue;
            case EasyTouchTrigger.ETTParameter.PickedUIElement:
              if ((UnityEngine.Object) gesture.pickedUIElement != (UnityEngine.Object) null)
              {
                gameObject.SendMessage(current.methodName, (object) gesture.pickedObject, SendMessageOptions.DontRequireReceiver);
                continue;
              }
              continue;
            default:
              continue;
          }
        }
      }
    }
  }

  private bool IsRecevier4(EasyTouch.EventName evnt)
  {
    return this.receivers.FindIndex((Predicate<EasyTouchTrigger.EasyTouchReceiver>) (e => e.eventName == evnt)) > -1;
  }

  private EasyTouchTrigger.EasyTouchReceiver GetTrigger(string triggerName)
  {
    return this.receivers.Find((Predicate<EasyTouchTrigger.EasyTouchReceiver>) (n => n.name == triggerName));
  }

  public enum ETTParameter
  {
    None,
    Gesture,
    Finger_Id,
    Touch_Count,
    Start_Position,
    Position,
    Delta_Position,
    Swipe_Type,
    Swipe_Length,
    Swipe_Vector,
    Delta_Pinch,
    Twist_Anlge,
    ActionTime,
    DeltaTime,
    PickedObject,
    PickedUIElement,
  }

  public enum ETTType
  {
    Object3D,
    UI,
  }

  [Serializable]
  public class EasyTouchReceiver
  {
    public bool enable;
    public EasyTouchTrigger.ETTType triggerType;
    public string name;
    public bool restricted;
    public GameObject gameObject;
    public bool otherReceiver;
    public GameObject gameObjectReceiver;
    public EasyTouch.EventName eventName;
    public string methodName;
    public EasyTouchTrigger.ETTParameter parameter;
  }
}
