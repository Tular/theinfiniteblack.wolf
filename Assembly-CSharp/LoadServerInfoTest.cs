﻿// Decompiled with JetBrains decompiler
// Type: LoadServerInfoTest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LoadServerInfoTest : MonoBehaviour
{
  public bool execute;
  public string result;
  public UITextList motd;
  public string serverTest;
  public string motdTest;
  public List<LoadServerInfoTest.ServerStatus> serverStatus;
  public List<LoadServerInfoTest.MotdUrlInfo> motdUrls;
  private LoadServerInfoTest.TestStatus _serverStatusTest;
  private LoadServerInfoTest.TestStatus _motdTestStatus;

  private void Start()
  {
    this._serverStatusTest = LoadServerInfoTest.TestStatus.None;
    this._motdTestStatus = LoadServerInfoTest.TestStatus.None;
  }

  private void Update()
  {
    this.serverTest = this._serverStatusTest.ToString();
    this.motdTest = this._motdTestStatus.ToString();
    if (!this.execute)
      return;
    this.execute = false;
    this.StartCoroutine(this.LoadServerInfo());
  }

  [DebuggerHidden]
  protected IEnumerator LoadServerInfo()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LoadServerInfoTest.\u003CLoadServerInfo\u003Ec__Iterator31()
    {
      \u003C\u003Ef__this = this
    };
  }

  [Serializable]
  public class ServerStatus
  {
    public sbyte ID;
    public string Name;
    public string Launch;
    public string Rules;
    public string Players;
    public bool Open;
  }

  [Serializable]
  public class MotdUrlInfo
  {
    public string name;
    public string url;
  }

  public enum TestStatus
  {
    None,
    Running,
    Complete,
  }
}
