﻿// Decompiled with JetBrains decompiler
// Type: DragUnitCardCellPooledScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragUnitCardCellPooledScrollView : MonoBehaviour
{
  public UnitCardCellPooledScrollView scrollView;

  private void OnPress(bool pressed)
  {
    if (!(bool) ((Object) this.scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this.scrollView.Press(pressed);
  }

  public void OnSpawned()
  {
    this.scrollView = NGUITools.FindInParents<UnitCardCellPooledScrollView>(this.transform);
    if (!(bool) ((Object) this.scrollView))
      this.scrollView = NGUITools.FindInParents<UnitCardCellPooledScrollView>(this.transform);
    if (!((Object) this.scrollView != (Object) null))
      return;
    UIDragScrollView component = this.GetComponent<UIDragScrollView>();
    if (!((Object) component != (Object) null))
      return;
    component.enabled = false;
  }

  private void OnDrag(Vector2 delta)
  {
    if (!(bool) ((Object) this.scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this.scrollView.Drag();
  }

  private void OnScroll(float delta)
  {
    if (!(bool) ((Object) this.scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this.scrollView.Scroll(delta);
  }
}
