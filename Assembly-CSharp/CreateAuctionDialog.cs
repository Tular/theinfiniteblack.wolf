﻿// Decompiled with JetBrains decompiler
// Type: CreateAuctionDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity.UI;

public class CreateAuctionDialog : DialogBase<CreateAuctionDialogContext>
{
  public CreateAuctionView newAuctionView;

  protected override bool ProcessArgs(object[] args)
  {
    if (args.Length != 1)
      throw new ArgumentException("Expected args length is 1.");
    this.context.item = (EquipmentItem) args[0];
    this.panelDepth = UIPanel.nextUnusedDepth;
    return false;
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  protected override void OnShow()
  {
    this.newAuctionView.Show();
  }

  protected override CreateAuctionDialogContext CreateContext()
  {
    return new CreateAuctionDialogContext(this);
  }
}
