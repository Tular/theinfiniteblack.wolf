﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayPurchaseProcessor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine.Purchasing;

public class GooglePlayPurchaseProcessor : IPurchaseProcessor
{
  private readonly Dictionary<string, string> _productNameLookup = new Dictionary<string, string>(5);
  private readonly string _publicKey;

  public GooglePlayPurchaseProcessor(string publicKey)
  {
    this._publicKey = publicKey;
  }

  public string GetProductId(IProductInfo product)
  {
    return product.googlePlayId;
  }

  public void ConfigureBuilder(ConfigurationBuilder builder)
  {
    builder.Configure<IGooglePlayConfiguration>().SetPublicKey(this._publicKey);
  }

  public PendingOrderDetail ParseReceipt(IInAppPurchasingContext context, string strReceipt)
  {
    Dictionary<string, object> dictionary1 = MiniJsonExtensions.HashtableFromJson(strReceipt);
    string str1 = dictionary1["Store"] as string;
    string str2 = dictionary1["TransactionID"] as string;
    Dictionary<string, object> dictionary2 = MiniJsonExtensions.HashtableFromJson(dictionary1["Payload"] as string);
    string str3 = dictionary2["json"] as string;
    string str4 = dictionary2["signature"] as string;
    Dictionary<string, object> dictionary3 = MiniJsonExtensions.HashtableFromJson(str3);
    string str5 = dictionary3["developerPayload"] as string;
    string str6 = dictionary3["orderId"] as string;
    return new PendingOrderDetail()
    {
      orderId = str6,
      characterName = str5,
      json = str3,
      signature = str4
    };
  }

  public bool TryGetPostString(PendingOrderDetail order, out string postString)
  {
    postString = string.Empty;
    if (order.json == null || order.signature == null)
      return false;
    postString = string.Format("<PendingOrderDetail><json>{0}</json><signature>{1}</signature></PendingOrderDetail>", (object) order.json, (object) order.signature);
    return true;
  }
}
