﻿// Decompiled with JetBrains decompiler
// Type: UnitCardCellInfoCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public class UnitCardCellInfoCollection : IPooledScrollViewCellInifoCollection<UnitCardCellInfoBase>
{
  private readonly List<UnitCardCellInfoBase> _sortedCellInfos = new List<UnitCardCellInfoBase>(1000);
  private readonly Dictionary<int, UnitCardCellInfoBase> _cellInfoLookup = new Dictionary<int, UnitCardCellInfoBase>();
  private readonly List<Entity> _newSortedEntities = new List<Entity>(1000);
  private readonly Dictionary<int, Entity> _newSortedEntitiesLookup = new Dictionary<int, Entity>(1000);
  public Transform prefAsteroidCard;
  public Transform prefCapturePointCard;
  public Transform prefCargoCard;
  public Transform prefDefensePlatformCard;
  public Transform prefFighterCard;
  public Transform prefGarrisonCard;
  public Transform prefIntradictorCard;
  public Transform prefMinesCard;
  public Transform prefNpcCard;
  public Transform prefPlanetCard;
  public Transform prefRepairDroneCard;
  public Transform prefShipCard;
  public Transform prefStarPortCard;
  private int _dataResetFrame;
  private int _dataModifiedFrame;

  public int Count
  {
    get
    {
      return this._sortedCellInfos.Count;
    }
  }

  public IEnumerable<UnitCardCellInfoBase> values
  {
    get
    {
      return (IEnumerable<UnitCardCellInfoBase>) this._sortedCellInfos;
    }
  }

  public UnitCardCellInfoBase first
  {
    get
    {
      return this._sortedCellInfos.FirstOrDefault<UnitCardCellInfoBase>();
    }
  }

  public UnitCardCellInfoBase last
  {
    get
    {
      return this._sortedCellInfos.LastOrDefault<UnitCardCellInfoBase>();
    }
  }

  public bool dataWasReset
  {
    get
    {
      return this._dataResetFrame == Time.frameCount;
    }
    private set
    {
      if (!value)
        return;
      this._dataResetFrame = Time.frameCount;
    }
  }

  public bool dataWasModified
  {
    get
    {
      return this._dataModifiedFrame == Time.frameCount;
    }
    private set
    {
      if (!value)
        return;
      this._dataModifiedFrame = Time.frameCount;
    }
  }

  public void UpdateInfoState()
  {
    using (Dictionary<int, UnitCardCellInfoBase>.ValueCollection.Enumerator enumerator = this._cellInfoLookup.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateCellVisuals();
    }
  }

  public void Add(UnitCardCellInfoBase cellInfo)
  {
    if (this._cellInfoLookup.ContainsKey(cellInfo.entityId))
      return;
    this._cellInfoLookup.Add(cellInfo.entityId, cellInfo);
    this.dataWasModified = true;
  }

  public void Remove(UnitCardCellInfoBase cellInfo)
  {
    if (!this._cellInfoLookup.ContainsKey(cellInfo.entityId))
      return;
    this._cellInfoLookup.Remove(cellInfo.entityId);
    this.dataWasModified = true;
  }

  public void RefreshList(IEnumerable<Entity> entities)
  {
    this._newSortedEntities.Clear();
    this._newSortedEntitiesLookup.Clear();
    this._newSortedEntities.AddRange(entities);
    for (int index = 0; index < this._newSortedEntities.Count; ++index)
      this._newSortedEntitiesLookup.Add(this._newSortedEntities[index].ID, this._newSortedEntities[index]);
    int[] array = this._newSortedEntitiesLookup.Keys.ToArray<int>();
    foreach (int key in this._cellInfoLookup.Keys.ToArray<int>())
    {
      if (!this._newSortedEntitiesLookup.ContainsKey(key))
        this.Remove(this._cellInfoLookup[key]);
    }
    for (int index = 0; index < array.Length; ++index)
    {
      int key = array[index];
      if (!this._cellInfoLookup.ContainsKey(key))
        this.Add(this.CreateInfo(this._newSortedEntitiesLookup[key]));
    }
    this._sortedCellInfos.Clear();
    for (int index = 0; index < this._newSortedEntities.Count; ++index)
      this._sortedCellInfos.Add(this._cellInfoLookup[this._newSortedEntities[index].ID]);
    this._newSortedEntities.Clear();
    this._newSortedEntitiesLookup.Clear();
    this.dataWasModified = true;
  }

  public void Clear()
  {
    for (int index = 0; index < this._cellInfoLookup.Count; ++index)
    {
      UnitCardCellInfoBase cardCellInfoBase = this._cellInfoLookup.Values.ElementAt<UnitCardCellInfoBase>(index);
      if (cardCellInfoBase.isBound)
        cardCellInfoBase.Unbind();
    }
    this._sortedCellInfos.Clear();
    this._cellInfoLookup.Clear();
    this.dataWasReset = true;
  }

  private UnitCardCellInfoBase CreateInfo(Entity entity)
  {
    switch (entity.Type)
    {
      case EntityType.PLANET:
        return (UnitCardCellInfoBase) new PlanetUnitCardCellInfo(this.prefPlanetCard, (Planet) entity);
      case EntityType.ASTEROID:
        return (UnitCardCellInfoBase) new AsteroidUnitCardCellInfo(this.prefAsteroidCard, (Asteroid) entity);
      case EntityType.STARPORT:
        return (UnitCardCellInfoBase) new StarPortUnitCardCellInfo(this.prefStarPortCard, (StarPort) entity);
      case EntityType.SHIP:
        return (UnitCardCellInfoBase) new PlayerShipUnitCardCellInfo(this.prefShipCard, (Ship) entity);
      case EntityType.NPC:
        return (UnitCardCellInfoBase) new NpcUnitCardCellInfo(this.prefNpcCard, (Npc) entity);
      case EntityType.FIGHTER:
        return (UnitCardCellInfoBase) new FighterUnitCardCellInfo(this.prefFighterCard, (Fighter) entity);
      case EntityType.DEFENSE_PLATFORM:
        return (UnitCardCellInfoBase) new DefensePlatformUnitCardCellInfo(this.prefDefensePlatformCard, (DefensePlatform) entity);
      case EntityType.MINES:
        return (UnitCardCellInfoBase) new MinesUnitCardCellInfo(this.prefMinesCard, (Mines) entity);
      case EntityType.INTRADICTOR:
        return (UnitCardCellInfoBase) new IntradictorUnitCardCellInfo(this.prefIntradictorCard, (Intradictor) entity);
      case EntityType.REPAIR_DRONE:
        return (UnitCardCellInfoBase) new RepairDroneUnitCardCellInfo(this.prefRepairDroneCard, (RepairDrone) entity);
      case EntityType.GARRISON:
        return (UnitCardCellInfoBase) new GarrisonUnitCardCellInfo(this.prefGarrisonCard, (Garrison) entity);
      case EntityType.CARGO_MONEY:
      case EntityType.CARGO_ITEM:
      case EntityType.CARGO_RESOURCE:
        return (UnitCardCellInfoBase) new CargoUnitCardCellInfo(this.prefCargoCard, (CargoEntity) entity);
      case EntityType.CAPTURE_POINT:
        throw new NotImplementedException();
      default:
        return (UnitCardCellInfoBase) null;
    }
  }
}
