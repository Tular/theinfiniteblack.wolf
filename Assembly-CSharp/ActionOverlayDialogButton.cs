﻿// Decompiled with JetBrains decompiler
// Type: ActionOverlayDialogButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using UnityEngine;

[RequireComponent(typeof (UIToggle))]
public class ActionOverlayDialogButton : SBUIButton
{
  public UISprite icon;
  public UILabel label;
  private UIToggle _toggle;
  private bool _supressOnToggleDelegates;
  private EventDelegate.Callback _onToggleDown;
  private EventDelegate.Callback _onToggleUp;
  private System.Action<ActionOverlayDialogButton> _handleToggleChange;
  private int _lastFrameToggleChanged;
  private SBUIButtonComponent _iconStateComponent;
  private int _initFrame;

  public string text
  {
    get
    {
      if ((bool) ((UnityEngine.Object) this.label))
        return this.label.text;
      return string.Empty;
    }
    set
    {
      if (!(bool) ((UnityEngine.Object) this.label))
        return;
      this.label.text = value;
    }
  }

  public string iconSpriteName
  {
    get
    {
      if ((bool) ((UnityEngine.Object) this.icon))
        return this.icon.spriteName;
      return string.Empty;
    }
    set
    {
      if (!(bool) ((UnityEngine.Object) this.icon))
        return;
      this.icon.spriteName = value;
      this._iconStateComponent.normal.sprite = value;
      this._iconStateComponent.pressed.sprite = value;
      this._iconStateComponent.hover.sprite = value;
      this._iconStateComponent.disabled.sprite = value;
    }
  }

  public bool isSelected
  {
    get
    {
      if (this.isToggle)
        return this._toggle.value;
      return false;
    }
    set
    {
      if (!this.isToggle)
        return;
      this._toggle.value = value;
    }
  }

  public bool isToggle
  {
    get
    {
      return this._toggle.enabled;
    }
    set
    {
      this._toggle.enabled = value;
    }
  }

  public int toggleGroup
  {
    get
    {
      return this._toggle.group;
    }
    set
    {
      this._toggle.group = value;
    }
  }

  private void OnToggleChanged()
  {
    if (this._lastFrameToggleChanged == Time.frameCount || this._initFrame == Time.frameCount)
      return;
    if ((UnityEngine.Object) UIToggle.current != (UnityEngine.Object) null && (UnityEngine.Object) UIToggle.current == (UnityEngine.Object) this._toggle && UIToggle.current.value)
    {
      this.SetState(SBUIButtonComponent.State.Pressed);
      if (this._supressOnToggleDelegates)
        return;
      if (this._onToggleDown != null)
        this._onToggleDown();
    }
    else
    {
      this.SetState(SBUIButtonComponent.State.Normal);
      if (this._supressOnToggleDelegates)
        return;
      if (this._onToggleUp != null)
        this._onToggleUp();
    }
    this._lastFrameToggleChanged = Time.frameCount;
  }

  public ActionOverlayDialogButton SetIconColors(Color normal, Color pressed, Color hover, Color disabled)
  {
    this._iconStateComponent.normal.color = normal;
    this._iconStateComponent.pressed.color = pressed;
    this._iconStateComponent.hover.color = hover;
    this._iconStateComponent.disabled.color = disabled;
    return this;
  }

  public ActionOverlayDialogButton SetHandleToggleChange(System.Action<ActionOverlayDialogButton> handleToggleChange)
  {
    this._handleToggleChange = handleToggleChange;
    return this;
  }

  public ActionOverlayDialogButton AddOnClickDelegate(EventDelegate.Callback onClickCallback)
  {
    EventDelegate.Add(this.onClick, onClickCallback);
    return this;
  }

  public ActionOverlayDialogButton SetToggleState(bool selected)
  {
    this._supressOnToggleDelegates = true;
    this._toggle.value = selected;
    if (selected)
      this.SetState(SBUIButtonComponent.State.Pressed);
    else
      this.SetState(SBUIButtonComponent.State.Normal);
    this._supressOnToggleDelegates = false;
    return this;
  }

  public ActionOverlayDialogButton InitializeToggleState(int group, bool selected, EventDelegate.Callback onToggleDown, EventDelegate.Callback onToggleUp)
  {
    if (!this.isToggle)
      return this;
    this._onToggleDown = (EventDelegate.Callback) null;
    this._onToggleUp = (EventDelegate.Callback) null;
    this.SetToggleState(selected);
    this.SetState(!selected ? SBUIButtonComponent.State.Normal : SBUIButtonComponent.State.Pressed);
    this._toggle.group = group;
    this._onToggleDown = onToggleDown;
    this._onToggleUp = onToggleUp;
    this._initFrame = Time.frameCount;
    return this;
  }

  public ActionOverlayDialogButton Show(string btnLabel, string btnIconName)
  {
    return this.Show(btnLabel, btnIconName, false);
  }

  public ActionOverlayDialogButton Show(string btnLabel, string btnIconName, bool toggle)
  {
    this.text = btnLabel;
    this.iconSpriteName = btnIconName;
    this.isToggle = toggle;
    NGUITools.SetActive(this.gameObject, true);
    return this;
  }

  public void Hide()
  {
    this._handleToggleChange = (System.Action<ActionOverlayDialogButton>) null;
    this._onToggleUp = (EventDelegate.Callback) null;
    this._onToggleDown = (EventDelegate.Callback) null;
    this.onClick.Clear();
    NGUITools.SetActive(this.gameObject, false);
  }

  protected override void OnInit()
  {
    base.OnInit();
    this._toggle = this.GetComponent<UIToggle>();
    this._toggle.optionCanBeNone = true;
    EventDelegate.Add(this._toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
    GameObject gameObject = this.icon.gameObject;
    for (int index = 0; index < this.components.Length; ++index)
    {
      if (!((UnityEngine.Object) this.components[index].target != (UnityEngine.Object) gameObject))
      {
        this._iconStateComponent = this.components[index];
        break;
      }
    }
  }

  protected override void OnPress(bool isPressed)
  {
    if (this.isToggle)
    {
      if (isPressed)
        base.OnPress(true);
      else if (this._toggle.enabled && this._lastFrameToggleChanged != Time.frameCount && (UnityEngine.Object) this.gameObject == (UnityEngine.Object) UICamera.currentTouch.current)
        this._toggle.value = !this._toggle.value;
      else
        base.OnPress(false);
    }
    else
      base.OnPress(isPressed);
  }

  protected override void OnClick()
  {
    if (this.isToggle)
      return;
    base.OnClick();
  }

  protected override void OnHover(bool isOver)
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Touch || this.isToggle && this.isSelected)
      return;
    base.OnHover(isOver);
  }

  private new void OnDisable()
  {
    this.onClick.Clear();
    this._toggle.group = 0;
    this._onToggleUp = (EventDelegate.Callback) null;
    this._onToggleDown = (EventDelegate.Callback) null;
    this._handleToggleChange = (System.Action<ActionOverlayDialogButton>) null;
    this.isToggle = false;
    this.SetIconColors(Color.white, Color.white, Color.white, Color.clear);
  }

  protected override void Update()
  {
    if (this._handleToggleChange == null)
      return;
    this._handleToggleChange(this);
  }
}
