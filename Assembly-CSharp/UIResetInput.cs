﻿// Decompiled with JetBrains decompiler
// Type: UIResetInput
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIResetInput : MonoBehaviour
{
  public UIInput input;

  private void OnClick()
  {
    this.input.value = string.Empty;
  }
}
