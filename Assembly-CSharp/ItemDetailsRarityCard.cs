﻿// Decompiled with JetBrains decompiler
// Type: ItemDetailsRarityCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class ItemDetailsRarityCard : MonoBehaviour
{
  public string resourcePath = "items";
  public UITexture icon;
  public UISprite borderSprite;
  public UILabel label;
  public Color normalBorderColor;
  public Color selectedBorderColor;
  private ItemRarity _rarity;
  private int _lastIconRefreshFrame;

  public ItemDetailsContext dialogContext { get; set; }

  public ItemRarity rarity
  {
    get
    {
      return this._rarity;
    }
    set
    {
      this._rarity = value;
      this.name = string.Format("{0}-{1}", (object) this._rarity, (object) this._rarity);
      this.label.text = this._rarity.Name();
    }
  }

  private void OnClick()
  {
    this.dialogContext.selectedRarity = this.rarity;
  }

  [DebuggerHidden]
  private IEnumerator LoadItemTextureAsync()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ItemDetailsRarityCard.\u003CLoadItemTextureAsync\u003Ec__Iterator19()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void Update()
  {
    if (this.dialogContext == null)
      return;
    if (this._lastIconRefreshFrame < this.dialogContext.lastUpdateFrame)
    {
      this.StopCoroutine(this.LoadItemTextureAsync());
      this.StartCoroutine(this.LoadItemTextureAsync());
    }
    this._lastIconRefreshFrame = Time.frameCount;
    this.borderSprite.color = this.dialogContext.selectedRarity != this.rarity ? this.normalBorderColor : this.selectedBorderColor;
  }

  private void OnDisable()
  {
    Texture mainTexture = this.icon.mainTexture;
    this.icon.mainTexture = (Texture) null;
  }
}
