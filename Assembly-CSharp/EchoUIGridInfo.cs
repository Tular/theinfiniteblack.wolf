﻿// Decompiled with JetBrains decompiler
// Type: EchoUIGridInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class EchoUIGridInfo : MonoBehaviour
{
  public UIGrid grid;
  public Bounds relativeWidget;
  public bool recalculate;

  private void Start()
  {
    if (!(bool) ((Object) this.grid))
      return;
    this.relativeWidget = NGUIMath.CalculateRelativeWidgetBounds(this.grid.transform, true);
  }

  private void Update()
  {
    if (!(bool) ((Object) this.grid) || !this.recalculate)
      return;
    this.relativeWidget = NGUIMath.CalculateRelativeWidgetBounds(this.grid.transform, true);
  }
}
