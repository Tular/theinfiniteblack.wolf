﻿// Decompiled with JetBrains decompiler
// Type: PrivateChatChannelCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System.Collections.Generic;
using UnityEngine;

public class PrivateChatChannelCell : SBUITab, IScrollViewCell
{
  public SBUIButton closeButton;
  public UILabel label;
  private UIWidget _widget;

  public Vector3 extents
  {
    get
    {
      if (!(bool) ((Object) this._widget))
        this._widget = this.GetComponent<UIWidget>();
      return new Vector3((float) this._widget.width / 2f, (float) this._widget.height / 2f, 0.0f);
    }
  }

  public List<EventDelegate> onCloseClicked
  {
    get
    {
      return this.closeButton.onClick;
    }
  }

  public void ResetVisuals()
  {
    this.label.text = string.Empty;
    this.onClick.Clear();
    this.closeButton.onClick.Clear();
    this.onLongPress.Clear();
  }

  protected override void OnInit()
  {
    base.OnInit();
    this._widget = this.GetComponent<UIWidget>();
  }

  private void OnDespawned()
  {
    this.onClick.Clear();
    this.onLongPress.Clear();
    this.closeButton.onClick.Clear();
  }
}
