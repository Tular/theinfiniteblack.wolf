﻿// Decompiled with JetBrains decompiler
// Type: ETCAxis
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class ETCAxis
{
  public string name;
  public bool enable;
  public ETCAxis.AxisRange range;
  public bool invertedAxis;
  public float speed;
  public float deadValue;
  public bool isEnertia;
  public float inertia;
  public float inertiaThreshold;
  public bool isAutoStab;
  public float autoStabThreshold;
  public float autoStabSpeed;
  private float startAngle;
  public bool isClampRotation;
  public float maxAngle;
  public float minAngle;
  public bool isValueOverTime;
  public float overTimeStep;
  public float maxOverTimeValue;
  public float axisValue;
  public float axisSpeedValue;
  public float axisThreshold;
  public ETCAxis.AxisState axisState;
  [SerializeField]
  private Transform _directTransform;
  public ETCAxis.DirectAction directAction;
  public ETCAxis.AxisInfluenced axisInfluenced;
  public ETCAxis.ActionOn actionOn;
  public CharacterController directCharacterController;
  public Rigidbody directRigidBody;
  public float gravity;
  public KeyCode positivekey;
  public KeyCode negativeKey;

  public ETCAxis(string axisName)
  {
    this.name = axisName;
    this.enable = true;
    this.range = ETCAxis.AxisRange.Classical;
    this.speed = 15f;
    this.invertedAxis = false;
    this.isEnertia = false;
    this.inertia = 0.0f;
    this.inertiaThreshold = 0.08f;
    this.axisValue = 0.0f;
    this.axisSpeedValue = 0.0f;
    this.gravity = 0.0f;
    this.isAutoStab = false;
    this.autoStabThreshold = 0.01f;
    this.autoStabSpeed = 10f;
    this.maxAngle = 90f;
    this.minAngle = 90f;
    this.axisState = ETCAxis.AxisState.None;
    this.maxOverTimeValue = 1f;
    this.overTimeStep = 1f;
    this.isValueOverTime = false;
    this.axisThreshold = 0.5f;
    this.deadValue = 0.1f;
    this.actionOn = ETCAxis.ActionOn.Press;
  }

  public Transform directTransform
  {
    get
    {
      return this._directTransform;
    }
    set
    {
      this._directTransform = value;
      if ((UnityEngine.Object) this._directTransform != (UnityEngine.Object) null)
      {
        this.directCharacterController = this._directTransform.GetComponent<CharacterController>();
        this.directRigidBody = this._directTransform.GetComponent<Rigidbody>();
      }
      else
        this.directCharacterController = (CharacterController) null;
    }
  }

  public void InitAxis()
  {
    this.startAngle = this.GetAngle();
  }

  public void UpdateAxis(float realValue, bool isOnDrag, ETCBase.ControlType type, bool deltaTime = true)
  {
    if (this.isAutoStab && (double) this.axisValue == 0.0)
      this.DoAutoStabilisation();
    if (this.invertedAxis)
      realValue *= -1f;
    if (this.isValueOverTime && (double) realValue != 0.0)
    {
      this.axisValue += this.overTimeStep * Mathf.Sign(realValue) * Time.deltaTime;
      this.axisValue = (double) Mathf.Sign(this.axisValue) <= 0.0 ? Mathf.Clamp(this.axisValue, -this.maxOverTimeValue, 0.0f) : Mathf.Clamp(this.axisValue, 0.0f, this.maxOverTimeValue);
    }
    this.ComputAxisValue(realValue, type, isOnDrag, deltaTime);
  }

  public void UpdateButton()
  {
    if (this.isValueOverTime)
    {
      this.axisValue += this.overTimeStep * Time.deltaTime;
      this.axisValue = Mathf.Clamp(this.axisValue, 0.0f, this.maxOverTimeValue);
    }
    else
      this.axisValue = this.axisState == ETCAxis.AxisState.Press || this.axisState == ETCAxis.AxisState.Down ? 1f : 0.0f;
    this.axisSpeedValue = this.axisValue * this.speed * Time.deltaTime;
    switch (this.actionOn)
    {
      case ETCAxis.ActionOn.Down:
        if (this.axisState != ETCAxis.AxisState.Down)
          break;
        this.DoDirectAction();
        break;
      case ETCAxis.ActionOn.Press:
        if (this.axisState != ETCAxis.AxisState.Press)
          break;
        this.DoDirectAction();
        break;
    }
  }

  public void ResetAxis()
  {
    if (this.isEnertia && (!this.isEnertia || (double) Mathf.Abs(this.axisValue) >= (double) this.inertiaThreshold))
      return;
    this.axisValue = 0.0f;
    this.axisSpeedValue = 0.0f;
  }

  public void DoDirectAction()
  {
    if ((bool) ((UnityEngine.Object) this.directTransform))
    {
      Vector3 influencedAxis = this.GetInfluencedAxis();
      switch (this.directAction)
      {
        case ETCAxis.DirectAction.Rotate:
          this.directTransform.Rotate(influencedAxis * this.axisSpeedValue, Space.World);
          break;
        case ETCAxis.DirectAction.RotateLocal:
          this.directTransform.Rotate(influencedAxis * this.axisSpeedValue, Space.Self);
          break;
        case ETCAxis.DirectAction.Translate:
          if ((UnityEngine.Object) this.directCharacterController == (UnityEngine.Object) null)
          {
            this.directTransform.Translate(influencedAxis * this.axisSpeedValue, Space.World);
            break;
          }
          int num1 = (int) this.directCharacterController.Move(influencedAxis * this.axisSpeedValue);
          break;
        case ETCAxis.DirectAction.TranslateLocal:
          if ((UnityEngine.Object) this.directCharacterController == (UnityEngine.Object) null)
          {
            this.directTransform.Translate(influencedAxis * this.axisSpeedValue, Space.Self);
            break;
          }
          int num2 = (int) this.directCharacterController.Move(this.directCharacterController.transform.TransformDirection(influencedAxis) * this.axisSpeedValue);
          break;
        case ETCAxis.DirectAction.Scale:
          this.directTransform.localScale += influencedAxis * this.axisSpeedValue;
          break;
        case ETCAxis.DirectAction.Force:
          if ((UnityEngine.Object) this.directRigidBody != (UnityEngine.Object) null)
          {
            this.directRigidBody.AddForce(influencedAxis * this.axisValue * this.speed);
            break;
          }
          Debug.LogWarning((object) ("ETCAxis : " + this.name + " No rigidbody on gameobject : " + this._directTransform.name));
          break;
        case ETCAxis.DirectAction.RelativeForce:
          if ((UnityEngine.Object) this.directRigidBody != (UnityEngine.Object) null)
          {
            this.directRigidBody.AddRelativeForce(influencedAxis * this.axisValue * this.speed);
            break;
          }
          Debug.LogWarning((object) ("ETCAxis : " + this.name + " No rigidbody on gameobject : " + this._directTransform.name));
          break;
        case ETCAxis.DirectAction.Torque:
          if ((UnityEngine.Object) this.directRigidBody != (UnityEngine.Object) null)
          {
            this.directRigidBody.AddTorque(influencedAxis * this.axisValue * this.speed);
            break;
          }
          Debug.LogWarning((object) ("ETCAxis : " + this.name + " No rigidbody on gameobject : " + this._directTransform.name));
          break;
        case ETCAxis.DirectAction.RelativeTorque:
          if ((UnityEngine.Object) this.directRigidBody != (UnityEngine.Object) null)
          {
            this.directRigidBody.AddRelativeTorque(influencedAxis * this.axisValue * this.speed);
            break;
          }
          Debug.LogWarning((object) ("ETCAxis : " + this.name + " No rigidbody on gameobject : " + this._directTransform.name));
          break;
      }
    }
    if (!this.isClampRotation || this.directAction != ETCAxis.DirectAction.RotateLocal)
      return;
    this.DoAngleLimitation();
  }

  public void DoGravity()
  {
    if (!((UnityEngine.Object) this.directCharacterController != (UnityEngine.Object) null) || (double) this.gravity == 0.0)
      return;
    int num = (int) this.directCharacterController.Move(Vector3.down * this.gravity * Time.deltaTime);
  }

  private void ComputAxisValue(float realValue, ETCBase.ControlType type, bool isOnDrag, bool deltaTime)
  {
    if (this.enable)
    {
      if (type == ETCBase.ControlType.Joystick)
      {
        float num1 = Mathf.Max(Mathf.Abs(realValue), 1f / 1000f);
        float num2 = Mathf.Max(num1 - this.deadValue, 0.0f) / (1f - this.deadValue) / num1;
        realValue *= num2;
      }
      if (this.isEnertia)
      {
        realValue -= this.axisValue;
        realValue /= this.inertia;
        this.axisValue += realValue;
        if ((double) Mathf.Abs(this.axisValue) < (double) this.inertiaThreshold && !isOnDrag)
          this.axisValue = 0.0f;
      }
      else if (!this.isValueOverTime || this.isValueOverTime && (double) realValue == 0.0)
        this.axisValue = realValue;
      if (deltaTime)
        this.axisSpeedValue = this.axisValue * this.speed * Time.deltaTime;
      else
        this.axisSpeedValue = this.axisValue * this.speed;
    }
    else
    {
      this.axisValue = 0.0f;
      this.axisSpeedValue = 0.0f;
    }
  }

  private Vector3 GetInfluencedAxis()
  {
    Vector3 vector3 = Vector3.zero;
    switch (this.axisInfluenced)
    {
      case ETCAxis.AxisInfluenced.X:
        vector3 = Vector3.right;
        break;
      case ETCAxis.AxisInfluenced.Y:
        vector3 = Vector3.up;
        break;
      case ETCAxis.AxisInfluenced.Z:
        vector3 = Vector3.forward;
        break;
    }
    return vector3;
  }

  private float GetAngle()
  {
    float num = 0.0f;
    if ((UnityEngine.Object) this._directTransform != (UnityEngine.Object) null)
    {
      switch (this.axisInfluenced)
      {
        case ETCAxis.AxisInfluenced.X:
          num = this._directTransform.localRotation.eulerAngles.x;
          break;
        case ETCAxis.AxisInfluenced.Y:
          num = this._directTransform.localRotation.eulerAngles.y;
          break;
        case ETCAxis.AxisInfluenced.Z:
          num = this._directTransform.localRotation.eulerAngles.z;
          break;
      }
      if ((double) num <= 360.0 && (double) num >= 180.0)
        num -= 360f;
    }
    return num;
  }

  private void DoAutoStabilisation()
  {
    float angle = this.GetAngle();
    if ((double) angle <= 360.0 && (double) angle >= 180.0)
      angle -= 360f;
    if ((double) angle <= (double) this.startAngle - (double) this.autoStabThreshold && (double) angle >= (double) this.startAngle + (double) this.autoStabThreshold)
      return;
    float num = 0.0f;
    Vector3 euler = Vector3.zero;
    if ((double) angle > (double) this.startAngle - (double) this.autoStabThreshold)
      num = angle + (float) ((double) this.autoStabSpeed / 100.0 * (double) Mathf.Abs(angle - this.startAngle) * (double) Time.deltaTime * -1.0);
    if ((double) angle < (double) this.startAngle + (double) this.autoStabThreshold)
      num = angle + this.autoStabSpeed / 100f * Mathf.Abs(angle - this.startAngle) * Time.deltaTime;
    switch (this.axisInfluenced)
    {
      case ETCAxis.AxisInfluenced.X:
        euler = new Vector3(num, this._directTransform.localRotation.eulerAngles.y, this._directTransform.localRotation.eulerAngles.z);
        break;
      case ETCAxis.AxisInfluenced.Y:
        euler = new Vector3(this._directTransform.localRotation.eulerAngles.x, num, this._directTransform.localRotation.eulerAngles.z);
        break;
      case ETCAxis.AxisInfluenced.Z:
        euler = new Vector3(this._directTransform.localRotation.eulerAngles.x, this._directTransform.localRotation.eulerAngles.y, num);
        break;
    }
    this._directTransform.localRotation = Quaternion.Euler(euler);
  }

  private void DoAngleLimitation()
  {
    Quaternion localRotation = this._directTransform.localRotation;
    localRotation.x /= localRotation.w;
    localRotation.y /= localRotation.w;
    localRotation.z /= localRotation.w;
    localRotation.w = 1f;
    switch (this.axisInfluenced)
    {
      case ETCAxis.AxisInfluenced.X:
        float num1 = Mathf.Clamp(114.5916f * Mathf.Atan(localRotation.x), -this.minAngle, this.maxAngle);
        localRotation.x = Mathf.Tan((float) Math.PI / 360f * num1);
        break;
      case ETCAxis.AxisInfluenced.Y:
        float num2 = Mathf.Clamp(114.5916f * Mathf.Atan(localRotation.y), -this.minAngle, this.maxAngle);
        localRotation.y = Mathf.Tan((float) Math.PI / 360f * num2);
        break;
      case ETCAxis.AxisInfluenced.Z:
        float num3 = Mathf.Clamp(114.5916f * Mathf.Atan(localRotation.z), -this.minAngle, this.maxAngle);
        localRotation.z = Mathf.Tan((float) Math.PI / 360f * num3);
        break;
    }
    this._directTransform.localRotation = localRotation;
  }

  public enum DirectAction
  {
    Rotate,
    RotateLocal,
    Translate,
    TranslateLocal,
    Scale,
    Force,
    RelativeForce,
    Torque,
    RelativeTorque,
  }

  public enum AxisInfluenced
  {
    X,
    Y,
    Z,
  }

  public enum AxisRange
  {
    Classical,
    Positif,
  }

  public enum AxisState
  {
    None,
    Down,
    Press,
    Up,
    DownUp,
    DownDown,
    DownLeft,
    DownRight,
    PressUp,
    PressDown,
    PressLeft,
    PressRight,
  }

  public enum ActionOn
  {
    Down,
    Press,
  }
}
