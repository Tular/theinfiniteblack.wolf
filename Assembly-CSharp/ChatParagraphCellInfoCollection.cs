﻿// Decompiled with JetBrains decompiler
// Type: ChatParagraphCellInfoCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;

public class ChatParagraphCellInfoCollection : PooledScrollViewCellInfoCollectionBase<ChatParagraphCellInfo>
{
  protected override IEnumerable<ChatParagraphCellInfo> GetValues()
  {
    List<ChatParagraphCellInfo> list = this.mInfoList.ToList<ChatParagraphCellInfo>();
    list.Reverse();
    return (IEnumerable<ChatParagraphCellInfo>) list;
  }

  public bool PruneHistory(int maxSize, int pruneSize)
  {
    if (this.mInfoList.Count < maxSize)
      return false;
    using (List<ChatParagraphCellInfo>.Enumerator enumerator = this.mInfoList.Take<ChatParagraphCellInfo>(pruneSize).ToList<ChatParagraphCellInfo>().GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.mInfoList.Remove(enumerator.Current);
    }
    return true;
  }
}
