﻿// Decompiled with JetBrains decompiler
// Type: UIButtonComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIButtonComponent : MonoBehaviour
{
  public UIButtonComponent.Action action = UIButtonComponent.Action.Disable;
  public MonoBehaviour target;

  private void Awake()
  {
    Debug.Log((object) "Needed!");
  }

  private void OnClick()
  {
    if (!((Object) this.target != (Object) null))
      return;
    if (this.action == UIButtonComponent.Action.Disable)
      this.target.enabled = false;
    else if (this.action == UIButtonComponent.Action.Enable)
      this.target.enabled = true;
    else
      this.target.enabled = !this.target.enabled;
  }

  public enum Action
  {
    Enable,
    Disable,
    Toggle,
  }
}
