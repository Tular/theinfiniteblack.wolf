﻿// Decompiled with JetBrains decompiler
// Type: UITwist
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UITwist : MonoBehaviour
{
  public void OnEnable()
  {
    EasyTouch.On_Twist += new EasyTouch.TwistHandler(this.On_Twist);
  }

  public void OnDestroy()
  {
    EasyTouch.On_Twist -= new EasyTouch.TwistHandler(this.On_Twist);
  }

  private void On_Twist(Gesture gesture)
  {
    if (!gesture.isOverGui || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform))
      return;
    this.transform.Rotate(new Vector3(0.0f, 0.0f, gesture.twistAngle));
  }
}
