﻿// Decompiled with JetBrains decompiler
// Type: TwoFingerGesture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TwoFingerGesture
{
  public EasyTouch.GestureType currentGesture = EasyTouch.GestureType.None;
  public EasyTouch.GestureType oldGesture = EasyTouch.GestureType.None;
  public int finger0;
  public int finger1;
  public float startTimeAction;
  public float timeSinceStartAction;
  public Vector2 startPosition;
  public Vector2 position;
  public Vector2 deltaPosition;
  public Vector2 oldStartPosition;
  public float fingerDistance;
  public float oldFingerDistance;
  public GameObject pickedObject;
  public GameObject oldPickedObject;
  public Camera pickedCamera;
  public bool isGuiCamera;
  public bool isOverGui;
  public GameObject pickedUIElement;
  public bool dragStart;
  public bool swipeStart;
  public bool inSingleDoubleTaps;
  public float tapCurentTime;

  public void ClearPickedObjectData()
  {
    this.pickedObject = (GameObject) null;
    this.oldPickedObject = (GameObject) null;
    this.pickedCamera = (Camera) null;
    this.isGuiCamera = false;
  }

  public void ClearPickedUIData()
  {
    this.isOverGui = false;
    this.pickedUIElement = (GameObject) null;
  }
}
