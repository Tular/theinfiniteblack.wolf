﻿// Decompiled with JetBrains decompiler
// Type: ETCBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

[Serializable]
public abstract class ETCBase : MonoBehaviour
{
  public int pointId = -1;
  private List<RaycastResult> uiRaycastResultCache = new List<RaycastResult>();
  private bool visibleAtStart = true;
  private bool activatedAtStart = true;
  protected RectTransform cachedRectTransform;
  protected Canvas cachedRootCanvas;
  [SerializeField]
  protected ETCBase.RectAnchor _anchor;
  [SerializeField]
  protected Vector2 _anchorOffet;
  [SerializeField]
  protected bool _visible;
  [SerializeField]
  protected bool _activated;
  public bool enableKeySimulation;
  public bool allowSimulationStandalone;
  public ETCBase.DPadAxis dPadAxisCount;
  public bool useFixedUpdate;
  private PointerEventData uiPointerEventData;
  private EventSystem uiEventSystem;
  public bool isOnDrag;
  public bool isSwipeIn;
  public bool isSwipeOut;
  public bool showPSInspector;
  public bool showSpriteInspector;
  public bool showEventInspector;
  public bool showBehaviourInspector;
  public bool showAxesInspector;
  public bool showTouchEventInspector;
  public bool showDownEventInspector;
  public bool showPressEventInspector;
  public bool isUnregisterAtDisable;

  public ETCBase.RectAnchor anchor
  {
    get
    {
      return this._anchor;
    }
    set
    {
      if (value == this._anchor)
        return;
      this._anchor = value;
      this.SetAnchorPosition();
    }
  }

  public Vector2 anchorOffet
  {
    get
    {
      return this._anchorOffet;
    }
    set
    {
      if (!(value != this._anchorOffet))
        return;
      this._anchorOffet = value;
      this.SetAnchorPosition();
    }
  }

  public bool visible
  {
    get
    {
      return this._visible;
    }
    set
    {
      if (value == this._visible)
        return;
      this._visible = value;
      this.SetVisible();
    }
  }

  public bool activated
  {
    get
    {
      return this._activated;
    }
    set
    {
      if (value == this._activated)
        return;
      this._activated = value;
      this.SetActivated();
    }
  }

  protected virtual void Awake()
  {
    this.cachedRectTransform = this.transform as RectTransform;
    this.cachedRootCanvas = this.transform.parent.GetComponent<Canvas>();
    if (!this.allowSimulationStandalone)
      this.enableKeySimulation = false;
    this.visibleAtStart = this._visible;
    this.activatedAtStart = this._activated;
    if (this.isUnregisterAtDisable)
      return;
    ETCInput.instance.RegisterControl(this);
  }

  public virtual void OnEnable()
  {
    if (this.isUnregisterAtDisable)
      ETCInput.instance.RegisterControl(this);
    this.visible = this.visibleAtStart;
    this.activated = this.activatedAtStart;
  }

  private void OnDisable()
  {
    if ((bool) ((UnityEngine.Object) ETCInput._instance) && this.isUnregisterAtDisable)
      ETCInput.instance.UnRegisterControl(this);
    this.visibleAtStart = this._visible;
    this.activated = this._activated;
    this.visible = false;
    this.activated = false;
  }

  private void OnDestroy()
  {
    if (!(bool) ((UnityEngine.Object) ETCInput._instance))
      return;
    ETCInput.instance.UnRegisterControl(this);
  }

  public virtual void Update()
  {
    if (this.useFixedUpdate)
      return;
    this.StartCoroutine("UpdateVirtualControl");
  }

  public virtual void FixedUpdate()
  {
    if (!this.useFixedUpdate)
      return;
    this.StartCoroutine("UpdateVirtualControl");
  }

  [DebuggerHidden]
  private IEnumerator UpdateVirtualControl()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ETCBase.\u003CUpdateVirtualControl\u003Ec__Iterator2()
    {
      \u003C\u003Ef__this = this
    };
  }

  protected virtual void UpdateControlState()
  {
  }

  protected virtual void SetVisible()
  {
  }

  protected virtual void SetActivated()
  {
  }

  public void SetAnchorPosition()
  {
    switch (this._anchor)
    {
      case ETCBase.RectAnchor.BottomLeft:
        this.rectTransform().anchorMin = new Vector2(0.0f, 0.0f);
        this.rectTransform().anchorMax = new Vector2(0.0f, 0.0f);
        this.rectTransform().anchoredPosition = new Vector2(this.rectTransform().sizeDelta.x / 2f + this._anchorOffet.x, this.rectTransform().sizeDelta.y / 2f + this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.BottomCenter:
        this.rectTransform().anchorMin = new Vector2(0.5f, 0.0f);
        this.rectTransform().anchorMax = new Vector2(0.5f, 0.0f);
        this.rectTransform().anchoredPosition = new Vector2(this._anchorOffet.x, this.rectTransform().sizeDelta.y / 2f + this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.BottonRight:
        this.rectTransform().anchorMin = new Vector2(1f, 0.0f);
        this.rectTransform().anchorMax = new Vector2(1f, 0.0f);
        this.rectTransform().anchoredPosition = new Vector2((float) (-(double) this.rectTransform().sizeDelta.x / 2.0) - this._anchorOffet.x, this.rectTransform().sizeDelta.y / 2f + this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.CenterLeft:
        this.rectTransform().anchorMin = new Vector2(0.0f, 0.5f);
        this.rectTransform().anchorMax = new Vector2(0.0f, 0.5f);
        this.rectTransform().anchoredPosition = new Vector2(this.rectTransform().sizeDelta.x / 2f + this._anchorOffet.x, this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.Center:
        this.rectTransform().anchorMin = new Vector2(0.5f, 0.5f);
        this.rectTransform().anchorMax = new Vector2(0.5f, 0.5f);
        this.rectTransform().anchoredPosition = new Vector2(this._anchorOffet.x, this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.CenterRight:
        this.rectTransform().anchorMin = new Vector2(1f, 0.5f);
        this.rectTransform().anchorMax = new Vector2(1f, 0.5f);
        this.rectTransform().anchoredPosition = new Vector2((float) (-(double) this.rectTransform().sizeDelta.x / 2.0) - this._anchorOffet.x, this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.TopLeft:
        this.rectTransform().anchorMin = new Vector2(0.0f, 1f);
        this.rectTransform().anchorMax = new Vector2(0.0f, 1f);
        this.rectTransform().anchoredPosition = new Vector2(this.rectTransform().sizeDelta.x / 2f + this._anchorOffet.x, (float) (-(double) this.rectTransform().sizeDelta.y / 2.0) - this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.TopCenter:
        this.rectTransform().anchorMin = new Vector2(0.5f, 1f);
        this.rectTransform().anchorMax = new Vector2(0.5f, 1f);
        this.rectTransform().anchoredPosition = new Vector2(this._anchorOffet.x, (float) (-(double) this.rectTransform().sizeDelta.y / 2.0) - this._anchorOffet.y);
        break;
      case ETCBase.RectAnchor.TopRight:
        this.rectTransform().anchorMin = new Vector2(1f, 1f);
        this.rectTransform().anchorMax = new Vector2(1f, 1f);
        this.rectTransform().anchoredPosition = new Vector2((float) (-(double) this.rectTransform().sizeDelta.x / 2.0) - this._anchorOffet.x, (float) (-(double) this.rectTransform().sizeDelta.y / 2.0) - this._anchorOffet.y);
        break;
    }
  }

  protected GameObject GetFirstUIElement(Vector2 position)
  {
    this.uiEventSystem = EventSystem.current;
    if (!((UnityEngine.Object) this.uiEventSystem != (UnityEngine.Object) null))
      return (GameObject) null;
    this.uiPointerEventData = new PointerEventData(this.uiEventSystem);
    this.uiPointerEventData.position = position;
    this.uiEventSystem.RaycastAll(this.uiPointerEventData, this.uiRaycastResultCache);
    if (this.uiRaycastResultCache.Count > 0)
      return this.uiRaycastResultCache[0].gameObject;
    return (GameObject) null;
  }

  public enum ControlType
  {
    Joystick,
    TouchPad,
    DPad,
    Button,
  }

  public enum RectAnchor
  {
    UserDefined,
    BottomLeft,
    BottomCenter,
    BottonRight,
    CenterLeft,
    Center,
    CenterRight,
    TopLeft,
    TopCenter,
    TopRight,
  }

  public enum DPadAxis
  {
    Two_Axis,
    Four_Axis,
  }
}
