﻿// Decompiled with JetBrains decompiler
// Type: EventLogCellInfoCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EventLogCellInfoCollection : IPooledScrollViewCellInifoCollection<EventLogCellInfo>
{
  private int _lastValuesEvaluationFrame = int.MinValue;
  private readonly List<EventLogCellInfo> _eventHistory = new List<EventLogCellInfo>(200);
  private readonly List<EventLogCellInfo> _values = new List<EventLogCellInfo>(200);
  private int _dataResetFrame;
  private bool _dataWasReset;
  private int _dataModifiedFrame;
  private bool _dataWasModified;
  private int _maxHistory;
  private int _purgeSize;
  private int _stickDuration;
  private EventLogCellInfo _stuckCell;
  private float _unstickTime;

  public EventLogCellInfoCollection(int maxHistory, int purgeSize, int stickDuration)
  {
    this._maxHistory = maxHistory;
    this._purgeSize = purgeSize;
    this._stickDuration = stickDuration;
  }

  public int Count
  {
    get
    {
      return this._eventHistory.Count;
    }
  }

  public bool dataWasReset
  {
    get
    {
      this.EvaluateValuesExpression();
      return this._dataResetFrame == this._lastValuesEvaluationFrame;
    }
    protected set
    {
      this._dataWasReset = value;
      if (!this._dataWasReset)
        return;
      this.dataWasModified = true;
    }
  }

  public bool dataWasModified
  {
    get
    {
      this.EvaluateValuesExpression();
      return this._dataModifiedFrame == this._lastValuesEvaluationFrame;
    }
    private set
    {
      this._dataWasModified = value;
    }
  }

  public EventLogCellInfo first
  {
    get
    {
      return this.values.FirstOrDefault<EventLogCellInfo>();
    }
  }

  public EventLogCellInfo last
  {
    get
    {
      return this.values.LastOrDefault<EventLogCellInfo>();
    }
  }

  private void EvaluateValuesExpression()
  {
    if (this._lastValuesEvaluationFrame != Time.frameCount && this._dataWasModified)
    {
      this._lastValuesEvaluationFrame = Time.frameCount;
      if (this._dataWasModified)
        this._dataModifiedFrame = this._lastValuesEvaluationFrame;
      if (this._dataWasReset)
        this._dataResetFrame = this._lastValuesEvaluationFrame;
      this._values.Clear();
      if (this._stuckCell != null)
        this._values.Add(this._stuckCell);
      for (int index = this._eventHistory.Count - 1; index >= 0; --index)
        this._values.Add(this._eventHistory[index]);
      this._dataWasModified = false;
      this._dataWasReset = false;
    }
    this._lastValuesEvaluationFrame = Time.frameCount;
  }

  public IEnumerable<EventLogCellInfo> values
  {
    get
    {
      this.EvaluateValuesExpression();
      return (IEnumerable<EventLogCellInfo>) this._values;
    }
  }

  public void Add(EventLogCellInfo cellInfo)
  {
    if (cellInfo.isSticky)
    {
      if (this._stuckCell != null)
        this._eventHistory.Add(this._stuckCell);
      this._stuckCell = cellInfo;
      this._unstickTime = RealTime.time + (float) this._stickDuration;
    }
    else
      this._eventHistory.Add(cellInfo);
    this.dataWasModified = true;
  }

  public void Remove(EventLogCellInfo cellData)
  {
    throw new NotImplementedException();
  }

  public void Clear()
  {
    this._eventHistory.Clear();
    this.dataWasReset = true;
  }

  public bool Cleanup()
  {
    if (this._stuckCell != null && (double) this._unstickTime < (double) RealTime.time)
    {
      this._eventHistory.Add(this._stuckCell);
      this._stuckCell = (EventLogCellInfo) null;
    }
    if (this._eventHistory.Count < this._maxHistory)
      return false;
    while (this._eventHistory.Count >= this._maxHistory)
      this._eventHistory.RemoveAt(0);
    return true;
  }
}
