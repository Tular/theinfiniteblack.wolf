﻿// Decompiled with JetBrains decompiler
// Type: PurchaseHistoryFileManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.IO;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using UnityEngine;

public class PurchaseHistoryFileManager : IFileManager
{
  public PurchaseHistoryFileManager()
  {
    this.dataPath = Application.persistentDataPath;
  }

  public string dataPath { get; private set; }

  public FileStream Get(string fileName, FileMode mode)
  {
    string path = Path.Combine(this.dataPath, fileName);
    if (!File.Exists(path))
    {
      string xml = XmlFileHelper.GetXml<PendingOrders>(new PendingOrders());
      using (FileStream fileStream = File.Create(path))
      {
        using (StreamWriter streamWriter = new StreamWriter((Stream) fileStream))
        {
          streamWriter.Write(xml);
          streamWriter.Flush();
        }
      }
    }
    return new FileStream(path, mode);
  }
}
