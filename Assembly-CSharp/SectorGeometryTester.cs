﻿// Decompiled with JetBrains decompiler
// Type: SectorGeometryTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Collections.Generic;
using TheInfiniteBlack.Unity.Map;
using UnityEngine;

[AdvancedInspector.AdvancedInspector]
[RequireComponent(typeof (MeshFilter), typeof (MeshRenderer))]
public class SectorGeometryTester : MonoBehaviour
{
  private List<MapSection> mapSections = new List<MapSection>();
  [Inspect(0)]
  public UIAtlas atlas;
  [Inspect(1)]
  public int mapSize;
  [Inspect(2)]
  public int sectionSize;
  [Inspect(3)]
  public int tileSize;
  [Inspect(4)]
  public int spriteSize;
  [Inspect(5)]
  public Vector3 sectorOffset;
  [Inspect(10)]
  public string[] orderedLayers;
  [Inspect(20)]
  public Vector3 nodeMargin;
  [Inspect(22)]
  public string[] visibleLayers;
  private SectorGeometryBuilder _geometryBuilderBuilder;
  private MeshFilter _meshFilter;

  private void CreateGeometry()
  {
    float num = (float) this.tileSize / (float) this.spriteSize;
    try
    {
    }
    catch (Exception ex)
    {
      Debug.LogException(ex);
    }
  }

  private void BuildSection(int size, int startX, int startY)
  {
    int num1 = startX + size;
    int num2 = startY + size;
    int length1 = size * size * this.visibleLayers.Length * 4;
    int length2 = size * size * this.visibleLayers.Length * 6;
    Vector3[] verts = new Vector3[length1];
    Vector2[] uvs = new Vector2[length1];
    Vector3[] normals = new Vector3[length1];
    int[] tris = new int[length2];
    Vector3 sectorPos = Vector3.zero;
    int startIdx = 0;
    try
    {
      for (int index1 = startY; index1 < num2; ++index1)
      {
        for (int index2 = startX; index2 < num1; ++index2)
        {
          this._geometryBuilderBuilder.BuildSector(this.visibleLayers, ref sectorPos, ref verts, ref uvs, ref normals, ref tris, ref startIdx);
          sectorPos += new Vector3((float) this.tileSize, 0.0f, 0.0f);
        }
        sectorPos = new Vector3(0.0f, sectorPos.y - (float) this.tileSize, 0.0f);
      }
    }
    catch (Exception ex)
    {
      Debug.LogException(ex);
    }
    this._meshFilter.mesh.Clear();
    this._meshFilter.mesh.vertices = verts;
    this._meshFilter.mesh.uv = uvs;
    this._meshFilter.mesh.normals = normals;
    this._meshFilter.mesh.triangles = tris;
  }

  [Inspect(33)]
  private void BuildMap()
  {
    float num1 = Mathf.Ceil((float) this.mapSize / (float) this.sectionSize);
    float num2 = (float) this.tileSize / (float) this.spriteSize;
    SectorGeometryBuilder builder = new SectorGeometryBuilder(this.atlas, this.orderedLayers, (float) this.tileSize, this.spriteSize);
    Vector3 startPos = this.transform.localPosition;
    for (int sectionY = 0; (double) sectionY < (double) num1; ++sectionY)
    {
      for (int sectionX = 0; (double) sectionX < (double) num1; ++sectionX)
      {
        this.mapSections.Add(MapSection.Create(this.gameObject, builder, startPos, this.nodeMargin, this.sectionSize, sectionX, sectionY));
        int num3 = this.sectionSize * this.tileSize;
        startPos += new Vector3((float) num3, 0.0f, 0.0f);
      }
      startPos = this.transform.localPosition + new Vector3(0.0f, (float) (-(sectionY + 1) * (this.sectionSize * this.tileSize)), 0.0f);
    }
  }

  [Inspect(34)]
  private void DrawMap()
  {
  }

  private void Awake()
  {
    this._meshFilter = this.GetComponent<MeshFilter>();
    this._meshFilter.mesh = new Mesh();
  }
}
