﻿// Decompiled with JetBrains decompiler
// Type: TibPing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using UnityEngine;

[RequireComponent(typeof (UILabel))]
public class TibPing : MonoBehaviour
{
  private UILabel _label;

  private void UpdateVisuals()
  {
    this._label.text = string.Format("{0}ms", (object) TibProxy.gameState.Net.Ping);
  }

  private void Start()
  {
    this._label = this.GetComponent<UILabel>();
  }

  private void OnEnable()
  {
    if ((bool) ((Object) this._label))
      return;
    this._label = this.GetComponent<UILabel>();
  }

  private void Update()
  {
    if (TibProxy.gameState == null)
      return;
    this.UpdateVisuals();
  }
}
