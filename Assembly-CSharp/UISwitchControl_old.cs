﻿// Decompiled with JetBrains decompiler
// Type: UISwitchControl_old
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UISwitchControl_old : MonoBehaviour
{
  [HideInInspector]
  public List<EventDelegate> onChange = new List<EventDelegate>();
  public bool initialState;
  public UILabel label;
  public TweenPosition knobTween;
  public TweenColor backgroundTween;
  private bool _value;

  public bool value
  {
    get
    {
      return this._value;
    }
    set
    {
      this.Set(value);
    }
  }

  protected void Start()
  {
  }

  protected void Update()
  {
  }

  protected virtual void Set(bool state)
  {
    if (state == this._value)
      return;
    this._value = state;
    if (Application.isEditor && !Application.isPlaying)
    {
      this.knobTween.transform.localPosition = !state ? this.knobTween.from : this.knobTween.to;
      this.backgroundTween.GetComponent<UISprite>().color = !state ? this.backgroundTween.from : this.backgroundTween.to;
    }
    else
    {
      if (!this.gameObject.activeInHierarchy)
      {
        this.knobTween.transform.localPosition = !state ? this.knobTween.from : this.knobTween.to;
        this.backgroundTween.GetComponent<UISprite>().color = !state ? this.backgroundTween.from : this.backgroundTween.to;
      }
      else
      {
        this.knobTween.Play(state);
        this.backgroundTween.Play(state);
      }
      if (!EventDelegate.IsValid(this.onChange))
        return;
      EventDelegate.Execute(this.onChange);
    }
  }

  protected void OnClick()
  {
    this.Set(!this._value);
  }
}
