﻿// Decompiled with JetBrains decompiler
// Type: UIWindowButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIWindowButton : MonoBehaviour
{
  public UIWindowButton.Action action = UIWindowButton.Action.Hide;
  public UIPanel window;
  public bool requiresFullVersion;
  public bool eraseHistory;

  private void Start()
  {
    UIPanel inParents = NGUITools.FindInParents<UIPanel>(this.gameObject);
    if (!((Object) inParents != (Object) null))
      return;
    UIWindow.Add(inParents);
  }

  private void OnClick()
  {
    if (this.requiresFullVersion && !PlayerProfile.fullAccess)
    {
      UIUpgradeWindow.Show();
    }
    else
    {
      switch (this.action)
      {
        case UIWindowButton.Action.Show:
          if (!((Object) this.window != (Object) null))
            break;
          if (this.eraseHistory)
            UIWindow.Close();
          UIWindow.Show(this.window);
          break;
        case UIWindowButton.Action.Hide:
          UIWindow.Close();
          break;
        case UIWindowButton.Action.GoBack:
          UIWindow.GoBack();
          break;
      }
    }
  }

  public enum Action
  {
    Show,
    Hide,
    GoBack,
  }
}
