﻿// Decompiled with JetBrains decompiler
// Type: SWPSDrag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SWPSDrag : MonoBehaviour
{
  public Vector3 scale = new Vector3(1f, 1f, 0.0f);
  public float moveSpeed = 1f;
  public UIPanel panel;
  public SWPSDrag.MoveType moveType;
  public int touchId;
  public bool dragStarted;
  public bool isPressed;
  public Vector3 targetPos;
  public Vector3 lastPos;
  public Plane plane;
  private Vector3 nextPos;

  private void OnPress(bool pressed)
  {
    if (!this.enabled)
      return;
    if (pressed)
    {
      if (this.isPressed)
        return;
      this.touchId = UICamera.currentTouchID;
      this.isPressed = true;
      this.dragStarted = false;
      Transform transform = UICamera.currentCamera.transform;
      this.plane = new Plane(this.panel.cachedTransform.rotation * Vector3.back, UICamera.lastHit.point);
      this.targetPos = this.transform.position;
    }
    else
    {
      if (!this.isPressed || this.touchId != UICamera.currentTouchID)
        return;
      this.isPressed = false;
    }
  }

  private void OnDrag(Vector2 delta)
  {
    if (!this.isPressed || this.touchId != UICamera.currentTouchID || (!this.enabled || !NGUITools.GetActive(this.gameObject)))
      return;
    UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;
    Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos);
    float enter = 0.0f;
    if (!this.plane.Raycast(ray, out enter))
      return;
    Vector3 point = ray.GetPoint(enter);
    Vector3 direction1 = point - this.lastPos;
    this.lastPos = point;
    if (!this.dragStarted)
    {
      this.dragStarted = true;
      direction1 = Vector3.zero;
    }
    Transform transform = this.transform;
    if ((double) direction1.x != 0.0 || (double) direction1.y != 0.0)
    {
      Vector3 direction2 = transform.InverseTransformDirection(direction1);
      direction2.Scale(this.scale);
      direction1 = transform.TransformDirection(direction2);
    }
    this.targetPos = this.targetPos + direction1;
  }

  private void Move()
  {
    if (!this.dragStarted)
      return;
    float num = (Time.time - Time.fixedTime) / Time.fixedDeltaTime;
    this.transform.position = Vector3.MoveTowards(this.transform.position, this.targetPos, Time.deltaTime);
  }

  private void Awake()
  {
    this.panel = NGUITools.FindInParents<UIPanel>(this.gameObject);
  }

  private void FixedUpdate()
  {
    if (this.moveType != SWPSDrag.MoveType.FixedUpdate)
      return;
    this.nextPos = Vector3.MoveTowards(this.transform.position, this.targetPos, Time.deltaTime);
  }

  private void Update()
  {
    if (this.moveType != SWPSDrag.MoveType.Update)
      return;
    this.nextPos = Vector3.MoveTowards(this.transform.position, this.targetPos, Time.deltaTime);
  }

  private void LateUpdate()
  {
    if (this.moveType != SWPSDrag.MoveType.LateUpdate)
      return;
    this.nextPos = Vector3.MoveTowards(this.transform.position, this.targetPos, Time.deltaTime);
  }

  private void OnDrawGizmos()
  {
    Gizmos.matrix = this.transform.localToWorldMatrix;
    Gizmos.color = Color.magenta;
    if (Application.isPlaying)
      Gizmos.DrawSphere(this.transform.InverseTransformPoint(this.targetPos), 10f);
    else
      Gizmos.DrawSphere(this.transform.position, 10f);
  }

  public enum MoveType
  {
    Update,
    LateUpdate,
    FixedUpdate,
  }
}
