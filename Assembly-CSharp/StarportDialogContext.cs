﻿// Decompiled with JetBrains decompiler
// Type: StarportDialogContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class StarportDialogContext : IItemScrollViewContext, IGeneralScrollViewContext<EquipmentItem>
{
  public int lastChangeFlagId;
  public StarPort starport;

  public ItemViewContext viewContext
  {
    get
    {
      return ItemViewContext.Starport;
    }
  }

  public bool hasChanged
  {
    get
    {
      return true;
    }
  }

  public IEnumerable<EquipmentItem> items
  {
    get
    {
      if (TibProxy.gameState.Local.StarPort == null)
        return (IEnumerable<EquipmentItem>) new List<EquipmentItem>();
      return (IEnumerable<EquipmentItem>) TibProxy.gameState.Local.StarPort.get_Inventory();
    }
  }

  public Ship ship
  {
    get
    {
      return TibProxy.gameState.MyShip;
    }
  }

  public int shipCargoSpace
  {
    get
    {
      return TibProxy.gameState.MyShip.ResourceCapacity;
    }
  }

  public int shipMetals
  {
    get
    {
      return (int) TibProxy.gameState.MyShip.Metals;
    }
  }

  public int shipOrganics
  {
    get
    {
      return (int) TibProxy.gameState.MyShip.Organics;
    }
  }

  public int shipGas
  {
    get
    {
      return (int) TibProxy.gameState.MyShip.Gas;
    }
  }

  public int shipRadioactives
  {
    get
    {
      return (int) TibProxy.gameState.MyShip.Radioactives;
    }
  }

  public int shipDarkmatter
  {
    get
    {
      return (int) TibProxy.gameState.MyShip.DarkMatter;
    }
  }

  public int maxBuyMetals
  {
    get
    {
      return this.shipCargoSpace - this.shipMetals;
    }
  }

  public int halfBuyMetals
  {
    get
    {
      return Mathf.RoundToInt((float) this.maxBuyMetals / 2f);
    }
  }

  public int maxSellMetals
  {
    get
    {
      return this.shipMetals;
    }
  }

  public int halfSellMetals
  {
    get
    {
      return Mathf.RoundToInt((float) this.shipMetals / 2f);
    }
  }

  public int maxBuyOrganics
  {
    get
    {
      return this.shipCargoSpace - this.shipOrganics;
    }
  }

  public int halfBuyOrganics
  {
    get
    {
      return Mathf.RoundToInt((float) this.maxBuyOrganics / 2f);
    }
  }

  public int maxSellOrganics
  {
    get
    {
      return this.shipOrganics;
    }
  }

  public int halfSellOrganics
  {
    get
    {
      return Mathf.RoundToInt((float) this.shipOrganics / 2f);
    }
  }

  public int maxBuyGas
  {
    get
    {
      return this.shipCargoSpace - this.shipGas;
    }
  }

  public int halfBuyGas
  {
    get
    {
      return Mathf.RoundToInt((float) this.maxBuyGas / 2f);
    }
  }

  public int maxSellGas
  {
    get
    {
      return this.shipGas;
    }
  }

  public int halfSellGas
  {
    get
    {
      return Mathf.RoundToInt((float) this.shipGas / 2f);
    }
  }

  public int maxBuyRadioactives
  {
    get
    {
      return this.shipCargoSpace - this.shipRadioactives;
    }
  }

  public int halfBuyRadioactives
  {
    get
    {
      return Mathf.RoundToInt((float) this.maxBuyRadioactives / 2f);
    }
  }

  public int maxSellRadioactives
  {
    get
    {
      return this.shipRadioactives;
    }
  }

  public int halfSellRadioactives
  {
    get
    {
      return Mathf.RoundToInt((float) this.shipRadioactives / 2f);
    }
  }

  public int maxBuyDarkmatter
  {
    get
    {
      return this.shipCargoSpace - this.shipDarkmatter;
    }
  }

  public int halfBuyDarkmatter
  {
    get
    {
      return Mathf.RoundToInt((float) this.maxBuyDarkmatter / 2f);
    }
  }

  public int maxSellDarkmatter
  {
    get
    {
      return this.shipDarkmatter;
    }
  }

  public int halfSellDarkmatter
  {
    get
    {
      return Mathf.RoundToInt((float) this.shipDarkmatter / 2f);
    }
  }
}
