﻿// Decompiled with JetBrains decompiler
// Type: FingerTouch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FingerTouch : MonoBehaviour
{
  public Vector3 deltaPosition = (Vector3) Vector2.zero;
  public int fingerId = -1;
  private TextMesh textMesh;

  private void OnEnable()
  {
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.On_TouchUp);
    EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_Drag += new EasyTouch.DragHandler(this.On_Drag);
    EasyTouch.On_DoubleTap += new EasyTouch.DoubleTapHandler(this.On_DoubleTap);
    this.textMesh = this.GetComponentInChildren<TextMesh>();
  }

  private void OnDestroy()
  {
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.On_TouchUp);
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_Drag -= new EasyTouch.DragHandler(this.On_Drag);
    EasyTouch.On_DoubleTap -= new EasyTouch.DoubleTapHandler(this.On_DoubleTap);
  }

  private void On_Drag(Gesture gesture)
  {
    if (!gesture.pickedObject.transform.IsChildOf(this.gameObject.transform) || this.fingerId != gesture.fingerIndex)
      return;
    this.transform.position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position) - this.deltaPosition;
  }

  private void On_Swipe(Gesture gesture)
  {
    if (this.fingerId != gesture.fingerIndex)
      return;
    this.transform.position = gesture.GetTouchToWorldPoint(this.transform.position) - this.deltaPosition;
  }

  private void On_TouchStart(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject != (Object) null) || !gesture.pickedObject.transform.IsChildOf(this.gameObject.transform))
      return;
    this.fingerId = gesture.fingerIndex;
    this.textMesh.text = this.fingerId.ToString();
    this.deltaPosition = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position) - this.transform.position;
  }

  private void On_TouchUp(Gesture gesture)
  {
    if (gesture.fingerIndex != this.fingerId)
      return;
    this.fingerId = -1;
    this.textMesh.text = string.Empty;
  }

  public void InitTouch(int ind)
  {
    this.fingerId = ind;
    this.textMesh.text = this.fingerId.ToString();
  }

  private void On_DoubleTap(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject != (Object) null) || !gesture.pickedObject.transform.IsChildOf(this.gameObject.transform))
      return;
    Object.DestroyImmediate((Object) this.transform.gameObject);
  }
}
