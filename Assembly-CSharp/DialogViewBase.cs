﻿// Decompiled with JetBrains decompiler
// Type: DialogViewBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class DialogViewBase : MonoBehaviour
{
  protected bool mIsInitialized;
  private bool _isShowing;

  public virtual int panelDepth { get; set; }

  protected virtual void TryInitialize()
  {
    if (this.mIsInitialized)
      return;
    this.OnInitialize();
    this.mIsInitialized = true;
    Debug.Log((object) string.Format("DialogView - {0} Initialized", (object) this.name));
  }

  public void Show()
  {
    if (this._isShowing)
      return;
    NGUITools.SetActiveSelf(this.gameObject, true);
    this.OnShow();
    this._isShowing = true;
  }

  public void Hide()
  {
    if (!this._isShowing)
      return;
    this.OnHide();
    NGUITools.SetActiveSelf(this.gameObject, false);
    this._isShowing = false;
  }

  protected virtual void OnInitialize()
  {
  }

  protected virtual void OnUpdateContent()
  {
  }

  protected virtual void OnShow()
  {
  }

  protected virtual void OnHide()
  {
  }

  protected virtual void OnAwake()
  {
  }

  protected void Awake()
  {
    if (!this.mIsInitialized)
      this.TryInitialize();
    this.OnAwake();
  }

  protected void OnEnable()
  {
    if (!this.mIsInitialized)
      this.TryInitialize();
    if (!this.mIsInitialized)
      ;
  }

  protected void OnDisable()
  {
    this.OnHide();
    this._isShowing = false;
  }
}
