﻿// Decompiled with JetBrains decompiler
// Type: UIPinch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIPinch : MonoBehaviour
{
  public void OnEnable()
  {
    EasyTouch.On_Pinch += new EasyTouch.PinchHandler(this.On_Pinch);
  }

  public void OnDestroy()
  {
    EasyTouch.On_Pinch -= new EasyTouch.PinchHandler(this.On_Pinch);
  }

  private void On_Pinch(Gesture gesture)
  {
    if (!gesture.isOverGui || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform))
      return;
    this.transform.localScale = new Vector3(this.transform.localScale.x + gesture.deltaPinch * Time.deltaTime, this.transform.localScale.y + gesture.deltaPinch * Time.deltaTime, this.transform.localScale.z);
  }
}
