﻿// Decompiled with JetBrains decompiler
// Type: ValueWrapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Xml.Serialization;

public abstract class ValueWrapper
{
  [XmlIgnore]
  public bool HasValueChangedSinceLastToString { get; protected set; }

  public virtual string ToString(string format)
  {
    return this.ToString();
  }
}
