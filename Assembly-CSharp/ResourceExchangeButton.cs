﻿// Decompiled with JetBrains decompiler
// Type: ResourceExchangeButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using UnityEngine;

[RequireComponent(typeof (UIWidget))]
[RequireComponent(typeof (BoxCollider))]
[RequireComponent(typeof (SBUIButton))]
public class ResourceExchangeButton : MonoBehaviour
{
  public UILabel quantityLabel;
  public UILabel costLabel;
  private int _quantity;
  private int _cost;

  public int quantity
  {
    get
    {
      return this._quantity;
    }
    set
    {
      this._quantity = value;
      this.quantityLabel.text = string.Format("{0:+0.#;-0.#}", (object) this._quantity);
    }
  }

  public int cost
  {
    get
    {
      return this._cost;
    }
    set
    {
      this._cost = value;
      this.costLabel.text = string.Format("{0:+$0.#;-$0.#}", (object) this._cost);
    }
  }

  private void Awake()
  {
    this.quantityLabel.text = string.Empty;
    this.costLabel.text = string.Empty;
  }
}
