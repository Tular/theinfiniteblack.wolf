﻿// Decompiled with JetBrains decompiler
// Type: TestDataBindingChatfilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.SettingsUI;
using UnityEngine;

public class TestDataBindingChatfilter : MonoBehaviour
{
  public ChatFilterViewModel testObject;
  public TestDataBindingChatfilter.Command command;
  public bool executeCommand;
  public bool alertValue;
  public bool allianceValue;
  public bool corpValue;
  public bool eventValue;
  public bool marketValue;
  public bool marketEventValue;
  public bool privateValue;
  public bool sectorValue;
  public bool serverValue;
  public bool universeValue;
  private ChatFilter _data;
  private bool _dataBound;

  public void ClearData()
  {
    this._data = (ChatFilter) null;
  }

  public void SetData()
  {
    this._data = new ChatFilter()
    {
      Alert = this.alertValue,
      Alliance = this.allianceValue,
      Corp = this.corpValue,
      Event = this.eventValue,
      Market = this.marketValue,
      MarketEvent = this.marketEventValue,
      Private = this.privateValue,
      Sector = this.sectorValue,
      Server = this.serverValue,
      Universe = this.universeValue
    };
  }

  public void BindData()
  {
    this._dataBound = true;
    this.testObject.Bind(this._data);
  }

  public void UnBindData()
  {
    this._dataBound = false;
    this.testObject.UnBind();
  }

  protected void Update()
  {
    if (this.executeCommand)
    {
      this.executeCommand = false;
      switch (this.command)
      {
        case TestDataBindingChatfilter.Command.SetData:
          this.SetData();
          break;
        case TestDataBindingChatfilter.Command.BindData:
          this.BindData();
          break;
        case TestDataBindingChatfilter.Command.UnbindData:
          this.UnBindData();
          break;
        case TestDataBindingChatfilter.Command.ClearData:
          this.ClearData();
          break;
      }
    }
    if (!this._dataBound)
      return;
    this.alertValue = this.testObject.data.Alert;
    this.allianceValue = this.testObject.data.Alliance;
    this.corpValue = this.testObject.data.Corp;
    this.eventValue = this.testObject.data.Event;
    this.marketValue = this.testObject.data.Market;
    this.marketEventValue = this.testObject.data.MarketEvent;
    this.privateValue = this.testObject.data.Private;
    this.sectorValue = this.testObject.data.Sector;
    this.serverValue = this.testObject.data.Server;
    this.universeValue = this.testObject.data.Universe;
  }

  public enum Command
  {
    SetData,
    BindData,
    UnbindData,
    ClearData,
  }
}
