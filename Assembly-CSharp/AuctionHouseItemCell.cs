﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseItemCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[RequireComponent(typeof (UIWidget))]
public class AuctionHouseItemCell : MonoBehaviour, IScrollViewCell
{
  public string resourcePath = "items";
  public UITexture itemIcon;
  public UILabel title;
  public UILabel subTitle;
  public UILabel description;
  public UILabel itemPros;
  public UILabel itemCons;
  public UILabel equipPoints;
  public UILabel durability;
  public UILabel auctionDetails;
  public UIToggle watchToggle;
  public UIGrid actionBarGrid;
  public SBUIButton cancel;
  public SBUIButton bid;
  public SBUIButton buyoutCredits;
  public SBUIButton buyoutBlackDollars;
  public int minHeight;
  public EventDelegate.Callback onUpdateCallback;
  private UIWidget _widget;
  private UIWidget[] _widgets;
  private int _spawnedFrame;

  public ClientAuction auction { get; set; }

  public int width
  {
    get
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      return this._widget.width;
    }
    set
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      this._widget.width = value;
    }
  }

  public int height
  {
    get
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      return this._widget.height;
    }
    set
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      this._widget.height = value;
    }
  }

  public Vector3 extents
  {
    get
    {
      return new Vector3((float) this._widget.width * 0.05f, (float) this._widget.height * 0.5f, 0.0f);
    }
  }

  public void OnToggleWatch()
  {
    if (this.auction == null || (Object) UIToggle.current == (Object) null || ((Object) UIToggle.current != (Object) this.watchToggle || UIToggle.current.value == this.auction.IsWatched))
      return;
    this.auction.IsWatched = UIToggle.current.value;
  }

  public void OnItemDetailsClicked()
  {
    DialogManager.ShowDialog<ItemDetailsPopup>(new object[1]
    {
      (object) this.auction.Item
    });
  }

  public void OnCancelAuction()
  {
    this.auction.DoCancel();
  }

  public void OnBid()
  {
    this.auction.DoBid();
  }

  public void OnBuyoutCredits()
  {
    this.auction.DoBuyout(CurrencyType.Credits);
  }

  public void OnBuyoutBlackDollars()
  {
    if (TibProxy.gameState.Bank.BlackDollars < this.auction.BuyoutBlackDollars)
      DialogManager.ShowDialogAsPopup<BlackDollarPopupDialog>().messageText.text = string.Format("That requires {0:##,###} BlackDollars!", (object) this.auction.BuyoutBlackDollars);
    else
      this.auction.DoBuyout(CurrencyType.BlackDollars);
  }

  public void ResetVisuals()
  {
    this.auction = (ClientAuction) null;
    if ((bool) ((Object) this.itemIcon))
      this.itemIcon.mainTexture = (Texture) null;
    if ((bool) ((Object) this.title))
      this.title.text = string.Empty;
    if ((bool) ((Object) this.subTitle))
      this.subTitle.text = string.Empty;
    if ((bool) ((Object) this.description))
      this.description.text = string.Empty;
    if ((bool) ((Object) this.itemPros))
      this.itemPros.text = string.Empty;
    if ((bool) ((Object) this.itemCons))
      this.itemCons.text = string.Empty;
    if ((bool) ((Object) this.equipPoints))
      this.equipPoints.text = string.Empty;
    if ((bool) ((Object) this.durability))
      this.durability.text = string.Empty;
    if ((bool) ((Object) this.auctionDetails))
      this.auctionDetails.text = string.Empty;
    for (int index = 0; index < this._widgets.Length; ++index)
    {
      if ((Object) this._widgets[index] == (Object) this._widget)
        this._widget.MarkAsChanged();
      else
        this._widgets[index].enabled = false;
    }
  }

  private void UpdateActionBar()
  {
    if (this.auction.CanCancel)
    {
      if (!this.cancel.gameObject.activeSelf)
        this.cancel.gameObject.SetActive(true);
      if (this.bid.gameObject.activeSelf)
        this.bid.gameObject.SetActive(false);
      if (this.buyoutCredits.gameObject.activeSelf)
        this.buyoutCredits.gameObject.SetActive(false);
      if (!this.buyoutBlackDollars.gameObject.activeSelf)
        return;
      this.buyoutBlackDollars.gameObject.SetActive(false);
    }
    else
    {
      if (this.cancel.gameObject.activeSelf)
        this.cancel.gameObject.SetActive(false);
      if (!this.bid.gameObject.activeSelf)
        this.bid.gameObject.SetActive(true);
      if (!this.buyoutCredits.gameObject.activeSelf)
        this.buyoutCredits.gameObject.SetActive(true);
      if (!this.buyoutBlackDollars.gameObject.activeSelf)
        this.buyoutBlackDollars.gameObject.SetActive(true);
      this.bid.isEnabled = this.auction.CanBid;
      this.buyoutCredits.isEnabled = this.auction.CanBuyout(CurrencyType.Credits);
      this.buyoutBlackDollars.isEnabled = this.auction.CanBuyout(CurrencyType.BlackDollars) || this.auction.BuyoutBlackDollars > 0;
      if (this.auction.CanBuyout(CurrencyType.BlackDollars))
        NGUITools.SetActiveSelf(this.buyoutBlackDollars.gameObject, this.auction.CanBuyout(CurrencyType.BlackDollars));
      this.actionBarGrid.Reposition();
    }
  }

  [DebuggerHidden]
  private IEnumerator LoadItemTexture()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AuctionHouseItemCell.\u003CLoadItemTexture\u003Ec__Iterator33()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void Awake()
  {
    this._widget = this.GetComponent<UIWidget>();
    this._widgets = this.GetComponentsInChildren<UIWidget>(true);
  }

  private void Update()
  {
    this.UpdateActionBar();
    if (this.onUpdateCallback == null)
      return;
    this.onUpdateCallback();
  }

  private void LateUpdate()
  {
    if (this._spawnedFrame != Time.frameCount - 1)
      return;
    for (int index = 0; index < this._widgets.Length; ++index)
    {
      UILabel widget = this._widgets[index] as UILabel;
      if ((Object) widget != (Object) null)
        widget.ProcessText();
      this._widgets[index].MarkAsChanged();
      this._widgets[index].enabled = true;
    }
  }

  private void OnSpawned()
  {
    this._spawnedFrame = Time.frameCount;
    this.ResetVisuals();
    this.StartCoroutine(this.LoadItemTexture());
  }

  private void OnDespawned()
  {
    this.StopAllCoroutines();
    this.height = this.minHeight;
    this.ResetVisuals();
    this.onUpdateCallback = (EventDelegate.Callback) null;
    for (int index = 0; index < this._widgets.Length; ++index)
    {
      if ((Object) this._widgets[index] == (Object) this._widget)
        this._widget.MarkAsChanged();
      else
        this._widgets[index].enabled = false;
    }
  }
}
