﻿// Decompiled with JetBrains decompiler
// Type: OnlinePlayersDirectoryIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class OnlinePlayersDirectoryIndex : PlayerDirectoryIndex
{
  private readonly Dictionary<string, PlayerDirectoryCellInfo> _headingInfos = new Dictionary<string, PlayerDirectoryCellInfo>();
  private PlayerDirectoryCellInfo _mainHeading;
  private string _filter;
  private OnlinePlayersDirectoryIndex.ViewLevel _currentView;

  public OnlinePlayersDirectoryIndex(Transform headingPrefab, Transform alphaHeadingPrefab)
    : base(headingPrefab)
  {
    this._mainHeading = new PlayerDirectoryCellInfo(headingPrefab, "- ONLINE -");
    this._headingInfos.Add("A", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "A"));
    this._headingInfos.Add("B", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "B"));
    this._headingInfos.Add("C", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "C"));
    this._headingInfos.Add("D", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "D"));
    this._headingInfos.Add("E", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "E"));
    this._headingInfos.Add("F", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "F"));
    this._headingInfos.Add("G", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "G"));
    this._headingInfos.Add("H", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "H"));
    this._headingInfos.Add("I", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "I"));
    this._headingInfos.Add("J", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "J"));
    this._headingInfos.Add("K", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "K"));
    this._headingInfos.Add("L", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "L"));
    this._headingInfos.Add("M", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "M"));
    this._headingInfos.Add("N", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "N"));
    this._headingInfos.Add("O", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "O"));
    this._headingInfos.Add("P", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "P"));
    this._headingInfos.Add("Q", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "Q"));
    this._headingInfos.Add("R", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "R"));
    this._headingInfos.Add("S", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "S"));
    this._headingInfos.Add("T", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "T"));
    this._headingInfos.Add("U", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "U"));
    this._headingInfos.Add("V", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "V"));
    this._headingInfos.Add("W", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "W"));
    this._headingInfos.Add("X", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "X"));
    this._headingInfos.Add("Y", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "Y"));
    this._headingInfos.Add("Z", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "Z"));
    this._headingInfos.Add("0", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "0"));
    this._headingInfos.Add("1", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "1"));
    this._headingInfos.Add("2", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "2"));
    this._headingInfos.Add("3", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "3"));
    this._headingInfos.Add("4", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "4"));
    this._headingInfos.Add("5", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "5"));
    this._headingInfos.Add("6", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "6"));
    this._headingInfos.Add("7", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "7"));
    this._headingInfos.Add("8", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "8"));
    this._headingInfos.Add("9", new PlayerDirectoryCellInfo(alphaHeadingPrefab, "9"));
    this._filter = string.Empty;
    this._currentView = OnlinePlayersDirectoryIndex.ViewLevel.Groups;
  }

  [DebuggerHidden]
  protected override IEnumerable<PlayerDirectoryCellInfo> FilteredValues()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    OnlinePlayersDirectoryIndex.\u003CFilteredValues\u003Ec__Iterator14 valuesCIterator14 = new OnlinePlayersDirectoryIndex.\u003CFilteredValues\u003Ec__Iterator14()
    {
      \u003C\u003Ef__this = this
    };
    // ISSUE: reference to a compiler-generated field
    valuesCIterator14.\u0024PC = -2;
    return (IEnumerable<PlayerDirectoryCellInfo>) valuesCIterator14;
  }

  protected override void OnCellClicked(PlayerDirectoryCellInfo cellInfo)
  {
    if (cellInfo == this._mainHeading)
      return;
    if (this._currentView == OnlinePlayersDirectoryIndex.ViewLevel.Details || PlayerDirectoryIndex.ModList.Contains(cellInfo))
    {
      if (cellInfo.isHeading)
      {
        this._filter = string.Empty;
        this._currentView = OnlinePlayersDirectoryIndex.ViewLevel.Groups;
        this.dataWasReset = true;
      }
      else
        base.OnCellClicked(cellInfo);
    }
    else
    {
      if (!cellInfo.isHeading)
        return;
      this._filter = cellInfo.text;
      this._currentView = OnlinePlayersDirectoryIndex.ViewLevel.Details;
      this.dataWasReset = true;
    }
  }

  private enum ViewLevel
  {
    Groups,
    Details,
  }
}
