﻿// Decompiled with JetBrains decompiler
// Type: PlayerProfile
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class PlayerProfile
{
  private static int mFull = -1;
  private static int mHints = -1;
  private static int mWifi = -1;
  private static int mExp = -1;
  private static int mPowerSaving = -1;
  public const int expPerLevel = 50000;
  private static string mName;

  public static bool fullAccess
  {
    get
    {
      if (PlayerProfile.mFull == -1)
        PlayerProfile.mFull = PlayerPrefs.GetInt("Full", 0);
      return PlayerProfile.mFull == 1;
    }
    set
    {
      int num = !value ? 0 : 1;
      if (PlayerProfile.mFull == num)
        return;
      PlayerProfile.mFull = num;
      PlayerPrefs.SetInt("Full", num);
      NGUITools.Broadcast("OnAccessLevelChanged");
    }
  }

  public static string playerName
  {
    get
    {
      if (string.IsNullOrEmpty(PlayerProfile.mName))
        PlayerProfile.mName = PlayerPrefs.GetString("Name", "Guest");
      return PlayerProfile.mName;
    }
    set
    {
      if (string.IsNullOrEmpty(value.Trim()))
        value = "Guest";
      if (!(PlayerProfile.mName != value))
        return;
      PlayerProfile.mName = value;
      PlayerPrefs.SetString("Name", value);
    }
  }

  public static int experience
  {
    get
    {
      if (PlayerProfile.mExp == -1)
        PlayerProfile.mExp = PlayerPrefs.GetInt("Experience", 0);
      return PlayerProfile.mExp;
    }
    set
    {
      if (PlayerProfile.mExp == value)
        return;
      PlayerProfile.mExp = value;
      PlayerPrefs.SetInt("Experience", value);
    }
  }

  public static bool hints
  {
    get
    {
      if (PlayerProfile.mHints == -1)
        PlayerProfile.mHints = PlayerPrefs.GetInt("Hints", 1);
      return PlayerProfile.mHints == 1;
    }
    set
    {
      int num = !value ? 0 : 1;
      if (num == PlayerProfile.mHints)
        return;
      PlayerProfile.mHints = num;
      PlayerPrefs.SetInt("Hints", PlayerProfile.mHints);
    }
  }

  public static bool allow3G
  {
    get
    {
      if (PlayerProfile.mWifi == -1)
        PlayerProfile.mWifi = PlayerPrefs.GetInt("Wifi", 0);
      return PlayerProfile.mWifi == 1;
    }
    set
    {
      int num = !value ? 0 : 1;
      if (num == PlayerProfile.mWifi)
        return;
      PlayerProfile.mWifi = num;
      PlayerPrefs.SetInt("Wifi", PlayerProfile.mWifi);
    }
  }

  public static bool allowedToAccessInternet
  {
    get
    {
      if (!PlayerProfile.allow3G)
        return Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork;
      return true;
    }
  }

  public static bool powerSavingMode
  {
    get
    {
      if (PlayerProfile.mPowerSaving == -1)
      {
        Application.targetFrameRate = PlayerPrefs.GetInt("FPS", 60);
        PlayerProfile.mPowerSaving = Application.targetFrameRate != 30 ? 0 : 1;
      }
      return PlayerProfile.mPowerSaving == 1;
    }
    set
    {
      int num = !value ? 0 : 1;
      if (PlayerProfile.mPowerSaving == num)
        return;
      PlayerProfile.mPowerSaving = num;
      Application.targetFrameRate = !value ? 60 : 30;
      PlayerPrefs.SetInt("FPS", Application.targetFrameRate);
    }
  }

  public static int maxExp
  {
    get
    {
      return 3450000;
    }
  }

  public static int abilityPoints
  {
    get
    {
      return 12;
    }
  }

  public static int level
  {
    get
    {
      return PlayerProfile.experience / 50000;
    }
  }

  public static float progressToNextLevel
  {
    get
    {
      return PlayerProfile.GetProgressToNextLevel(PlayerProfile.experience);
    }
  }

  public static float GetProgressToNextLevel(int exp)
  {
    int num = PlayerProfile.GetLevelByExp(exp) - 1;
    return (float) (exp - num * 50000) / 50000f;
  }

  public static string GetTitle(int lvl)
  {
    return Localization.Get("Title " + (object) (lvl / 5));
  }

  public static int GetLevelByExp(int exp)
  {
    if (exp > PlayerProfile.maxExp)
      exp = PlayerProfile.maxExp;
    return 1 + exp / 50000;
  }

  public static string GetTitleByExp(int exp)
  {
    return PlayerProfile.GetTitle(PlayerProfile.GetLevelByExp(exp) - 1);
  }
}
