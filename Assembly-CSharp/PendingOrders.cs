﻿// Decompiled with JetBrains decompiler
// Type: PendingOrders
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text;

public class PendingOrders
{
  public List<PendingOrderDetail> orders;

  public PendingOrders()
  {
    this.orders = new List<PendingOrderDetail>();
  }

  public override string ToString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < this.orders.Count; ++index)
      stringBuilder.AppendLine(this.orders[index].ToString());
    return stringBuilder.ToString();
  }
}
