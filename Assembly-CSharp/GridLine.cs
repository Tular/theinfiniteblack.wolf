﻿// Decompiled with JetBrains decompiler
// Type: GridLine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GridLine : MonoBehaviour
{
  public GameObject prefText;
  public LineRenderer lineRenderer;
  public GridLine.LineAxis axis;
  public float cellSizeFactor;
  public List<TextMesh> text;
  [SerializeField]
  public int _lineNumber;

  public float characterSize
  {
    set
    {
      using (List<TextMesh>.Enumerator enumerator = this.text.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.characterSize = value;
      }
    }
  }

  public float lineWidth
  {
    set
    {
      if (!this.lineRenderer.enabled)
        return;
      this.lineRenderer.SetWidth(value, value);
    }
  }

  [ContextMenu("BuildLine")]
  public void BuildLine(int lineNumber, string label)
  {
    this._lineNumber = lineNumber;
    this.text.Clear();
    switch (this.axis)
    {
      case GridLine.LineAxis.X:
        for (int index = 1; index < 16; ++index)
        {
          GameObject gameObject = this.gameObject.AddChild(this.prefText);
          gameObject.name = string.Format("{0}", (object) (index * 5));
          TextMesh component = gameObject.GetComponent<TextMesh>();
          component.text = label;
          float num = this.cellSizeFactor * (float) (5.0 * (double) index - 1.0);
          gameObject.transform.localPosition = new Vector3(num - this.cellSizeFactor * 2.5f, 0.0f, 0.0f);
          this.text.Add(component);
        }
        break;
      case GridLine.LineAxis.Y:
        for (int index = 1; index < 16; ++index)
        {
          GameObject gameObject = this.gameObject.AddChild(this.prefText);
          gameObject.name = string.Format("{0}", (object) (index * 5));
          TextMesh component = gameObject.GetComponent<TextMesh>();
          component.text = label;
          float num = this.cellSizeFactor * (float) (5.0 * (double) index - 1.0);
          gameObject.transform.localPosition = new Vector3(0.0f, (float) -((double) num - (double) this.cellSizeFactor * 2.5), 0.0f);
          this.text.Add(component);
        }
        break;
    }
  }

  public void SetViewDetailLevel(GridLine.ViewDetail targetLevel)
  {
    switch (targetLevel)
    {
      case GridLine.ViewDetail.Full:
        this.ShowFull();
        break;
      case GridLine.ViewDetail.Half:
        this.ShowHalf();
        break;
    }
  }

  private void ShowFull()
  {
    this.gameObject.SetActive(true);
    for (int index = 0; index < this.text.Count; ++index)
    {
      float num = this.cellSizeFactor * (float) ((index + 1) * 5 - 1);
      this.text[index].GetComponent<Renderer>().enabled = true;
      switch (this.axis)
      {
        case GridLine.LineAxis.X:
          this.text[index].transform.localPosition = new Vector3(num - this.cellSizeFactor * 2.5f, 0.0f, 0.0f);
          break;
        case GridLine.LineAxis.Y:
          this.text[index].transform.localPosition = new Vector3(0.0f, (float) -((double) num - (double) this.cellSizeFactor * 2.5), 0.0f);
          break;
      }
    }
  }

  private void ShowHalf()
  {
    if (this._lineNumber % 2 != 0)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      for (int index = 0; index < this.text.Count; ++index)
      {
        if (index % 2 == 0)
        {
          float num = this.cellSizeFactor * (float) ((index + 1) * 5 - 1);
          switch (this.axis)
          {
            case GridLine.LineAxis.X:
              this.text[index].transform.localPosition = new Vector3(num + this.cellSizeFactor * 0.5f, 0.0f, 0.0f);
              continue;
            case GridLine.LineAxis.Y:
              this.text[index].transform.localPosition = new Vector3(0.0f, (float) -((double) num + (double) this.cellSizeFactor * 0.5), 0.0f);
              continue;
            default:
              continue;
          }
        }
        else
          this.text[index].GetComponent<Renderer>().enabled = false;
      }
    }
  }

  public enum ViewDetail
  {
    Full,
    Half,
  }

  public enum LineAxis
  {
    X,
    Y,
  }
}
