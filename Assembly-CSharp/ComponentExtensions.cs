﻿// Decompiled with JetBrains decompiler
// Type: ComponentExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class ComponentExtensions
{
  public static RectTransform rectTransform(this Component cp)
  {
    return cp.transform as RectTransform;
  }

  public static float Remap(this float value, float from1, float to1, float from2, float to2)
  {
    return (float) (((double) value - (double) from1) / ((double) to1 - (double) from1) * ((double) to2 - (double) from2)) + from2;
  }
}
