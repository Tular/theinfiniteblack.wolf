﻿// Decompiled with JetBrains decompiler
// Type: MyWatchListFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client;

public class MyWatchListFilter : IAuctionFilter
{
  public string formattedName
  {
    get
    {
      return "MY WATCH LIST";
    }
  }

  [DebuggerHidden]
  public IEnumerable<ClientAuction> GetFilteredAuctions(IEnumerable<ClientAuction> auctions)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MyWatchListFilter.\u003CGetFilteredAuctions\u003Ec__IteratorD auctionsCIteratorD1 = new MyWatchListFilter.\u003CGetFilteredAuctions\u003Ec__IteratorD();
    // ISSUE: variable of a compiler-generated type
    MyWatchListFilter.\u003CGetFilteredAuctions\u003Ec__IteratorD auctionsCIteratorD2 = auctionsCIteratorD1;
    int num = -2;
    // ISSUE: reference to a compiler-generated field
    auctionsCIteratorD2.\u0024PC = num;
    return (IEnumerable<ClientAuction>) auctionsCIteratorD2;
  }

  public void ResetFilter()
  {
  }
}
