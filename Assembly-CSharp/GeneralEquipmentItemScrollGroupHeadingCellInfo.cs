﻿// Decompiled with JetBrains decompiler
// Type: GeneralEquipmentItemScrollGroupHeadingCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GeneralEquipmentItemScrollGroupHeadingCellInfo : IScrollViewCellInfo
{
  private Vector3 _localPosition;
  private GeneralEquipmentItemScrollGroupHeadingCell _cellInstance;

  public GeneralEquipmentItemScrollGroupHeadingCellInfo(Transform prefab)
  {
    this.cellPrefab = prefab;
    GeneralEquipmentItemScrollGroupHeadingCell component = this.cellPrefab.gameObject.GetComponent<GeneralEquipmentItemScrollGroupHeadingCell>();
    this.extents = new Vector3((float) component.width * 0.5f, (float) component.height * 0.5f);
  }

  public Vector3 localPosition
  {
    get
    {
      return this._localPosition;
    }
    set
    {
      this._localPosition = value;
      if (!((UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null))
        return;
      this._cellInstance.transform.localPosition = this._localPosition;
    }
  }

  public Vector3 extents { get; protected set; }

  public virtual object cellData
  {
    get
    {
      return (object) this.headingText;
    }
  }

  public virtual System.Type cellType
  {
    get
    {
      return typeof (GeneralEquipmentItemScrollGroupHeadingCellInfo);
    }
  }

  public bool isBound
  {
    get
    {
      return (UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null;
    }
  }

  public Transform cellPrefab { get; private set; }

  public Transform cellTransform
  {
    get
    {
      if ((UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null)
        return this._cellInstance.transform;
      return (Transform) null;
    }
  }

  public string headingText { get; set; }

  public bool IsContainedIn(Bounds bounds)
  {
    if (!bounds.Contains(this.localPosition + this.extents))
      return bounds.Contains(this.localPosition - this.extents);
    return true;
  }

  public virtual void BindCell(Transform cellTrans)
  {
    this._cellInstance = cellTrans.GetComponent<GeneralEquipmentItemScrollGroupHeadingCell>();
    this._cellInstance.transform.localPosition = this.localPosition;
    this._cellInstance.title.text = this.headingText;
  }

  public void Unbind()
  {
    this._cellInstance.ResetVisuals();
    this._cellInstance = (GeneralEquipmentItemScrollGroupHeadingCell) null;
  }
}
