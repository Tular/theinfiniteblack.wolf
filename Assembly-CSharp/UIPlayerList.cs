﻿// Decompiled with JetBrains decompiler
// Type: UIPlayerList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIPlayerList : MonoBehaviour
{
  public TweenPosition tween;
  public GameObject prefab;
  private UIPlayerName mPlayer;
  private bool mShown;

  private void Start()
  {
    GameObject gameObject = this.tween.gameObject;
    this.mPlayer = this.tween.gameObject.AddChild(this.prefab).GetComponent<UIPlayerName>();
    this.mPlayer.playerName = PlayerProfile.playerName;
    this.mPlayer.UpdateInfo(this.mShown);
    NGUITools.AddWidgetCollider(gameObject);
    UIEventListener.Get(gameObject).onClick = new UIEventListener.VoidDelegate(this.ToggleList);
    gameObject.BroadcastMessage("CreatePanel", SendMessageOptions.DontRequireReceiver);
    UIPanel inParents = NGUITools.FindInParents<UIPanel>(this.gameObject);
    if (!((Object) inParents != (Object) null))
      return;
    inParents.Refresh();
  }

  private void ToggleList(GameObject go)
  {
    this.mShown = !this.mShown;
    this.tween.Toggle();
    this.mPlayer.UpdateInfo(this.mShown);
  }
}
