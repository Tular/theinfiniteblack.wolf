﻿// Decompiled with JetBrains decompiler
// Type: MultiLayerTouch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class MultiLayerTouch : MonoBehaviour
{
  public Text label;
  public Text label2;

  private void OnEnable()
  {
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.On_TouchUp);
  }

  private void OnDestroy()
  {
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.On_TouchUp);
  }

  private void On_TouchDown(Gesture gesture)
  {
    if ((Object) gesture.pickedObject != (Object) null)
    {
      if (!EasyTouch.GetAutoUpdatePickedObject())
        this.label.text = "Picked object from event : " + gesture.pickedObject.name + " : " + (object) gesture.position;
      else
        this.label.text = "Picked object from event : " + gesture.pickedObject.name + " : " + (object) gesture.position;
    }
    else
      this.label.text = EasyTouch.GetAutoUpdatePickedObject() ? "Picked object from event : none" : "Picked object from event :  none";
    this.label2.text = string.Empty;
    if (EasyTouch.GetAutoUpdatePickedObject())
      return;
    GameObject currentPickedObject = gesture.GetCurrentPickedObject();
    if ((Object) currentPickedObject != (Object) null)
      this.label2.text = "Picked object from GetCurrentPickedObject : " + currentPickedObject.name;
    else
      this.label2.text = "Picked object from GetCurrentPickedObject : none";
  }

  private void On_TouchUp(Gesture gesture)
  {
    this.label.text = string.Empty;
    this.label2.text = string.Empty;
  }
}
