﻿// Decompiled with JetBrains decompiler
// Type: JettisonResourcesDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;

public class JettisonResourcesDialog : DialogBase
{
  public UILabel metalsCargo;
  public UILabel organicsCargo;
  public UILabel gasCargo;
  public UILabel radioactivesCargo;
  public UILabel darkmatterCargo;
  public SBUIButton metalsSingle;
  public SBUIButton metalsHalf;
  public SBUIButton metalsAll;
  public SBUIButton organicsSingle;
  public SBUIButton organicsHalf;
  public SBUIButton organicsAll;
  public SBUIButton gasSingle;
  public SBUIButton gasHalf;
  public SBUIButton gasAll;
  public SBUIButton radioactivesSingle;
  public SBUIButton radioactivesHalf;
  public SBUIButton radioactivesAll;
  public SBUIButton darkmatterSingle;
  public SBUIButton darkmatterHalf;
  public SBUIButton darkmatterAll;
  public UILabel metalsToJettison;
  public UILabel organicsToJettison;
  public UILabel gasToJettison;
  public UILabel radioactivesToJettison;
  public UILabel darkmatterToJettison;
  private int _cargoMetals;
  private int _cargoOrganics;
  private int _cargoGas;
  private int _cargoRadioactives;
  private int _cargoDarkmatter;
  private int _jettisonMetals;
  private int _jettisonOrganics;
  private int _jettisonGas;
  private int _jettisonRadioactives;
  private int _jettisonDarkmatter;

  public void MoveSingleMetal()
  {
    --this._cargoMetals;
    ++this._jettisonMetals;
    this.RefreshVisuals();
  }

  public void MoveHalfMetals()
  {
    int num = (int) ((double) this._cargoMetals / 2.0);
    if (num <= this._cargoMetals)
    {
      this._cargoMetals -= num;
      this._jettisonMetals += num;
    }
    this.RefreshVisuals();
  }

  public void MoveAllMetals()
  {
    this._jettisonMetals = (int) TibProxy.gameState.MyShip.Metals;
    this._cargoMetals = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleOrganic()
  {
    --this._cargoOrganics;
    ++this._jettisonOrganics;
    this.RefreshVisuals();
  }

  public void MoveHalfOrganics()
  {
    int num = (int) ((double) this._cargoOrganics / 2.0);
    if (num <= this._cargoOrganics)
    {
      this._cargoOrganics -= num;
      this._jettisonOrganics += num;
    }
    this.RefreshVisuals();
  }

  public void MoveAllOrganics()
  {
    this._jettisonOrganics = (int) TibProxy.gameState.MyShip.Organics;
    this._cargoOrganics = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleGas()
  {
    --this._cargoGas;
    ++this._jettisonGas;
    this.RefreshVisuals();
  }

  public void MoveHalfGas()
  {
    int num = (int) ((double) this._cargoGas / 2.0);
    if (num <= this._cargoGas)
    {
      this._cargoGas -= num;
      this._jettisonGas += num;
    }
    this.RefreshVisuals();
  }

  public void MoveAllGas()
  {
    this._jettisonGas = (int) TibProxy.gameState.MyShip.Gas;
    this._cargoGas = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleRadioactive()
  {
    --this._cargoRadioactives;
    ++this._jettisonRadioactives;
    this.RefreshVisuals();
  }

  public void MoveHalfRadioactives()
  {
    int num = (int) ((double) this._cargoRadioactives / 2.0);
    if (num <= this._cargoRadioactives)
    {
      this._cargoRadioactives -= num;
      this._jettisonRadioactives += num;
    }
    this.RefreshVisuals();
  }

  public void MoveAllRadioactives()
  {
    this._jettisonRadioactives = (int) TibProxy.gameState.MyShip.Radioactives;
    this._cargoRadioactives = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleDarkmatter()
  {
    --this._cargoDarkmatter;
    ++this._jettisonDarkmatter;
    this.RefreshVisuals();
  }

  public void MoveHalfDarkmatter()
  {
    int num = (int) ((double) this._cargoDarkmatter / 2.0);
    if (num <= this._cargoDarkmatter)
    {
      this._cargoDarkmatter -= num;
      this._jettisonDarkmatter += num;
    }
    this.RefreshVisuals();
  }

  public void MoveAllDarkmatter()
  {
    this._jettisonDarkmatter = (int) TibProxy.gameState.MyShip.DarkMatter;
    this._cargoDarkmatter = 0;
    this.RefreshVisuals();
  }

  public void AcceptJettisonRequest()
  {
    TibProxy.gameState.DoResourcesJettison(this._jettisonOrganics, this._jettisonGas, this._jettisonMetals, this._jettisonRadioactives, this._jettisonDarkmatter);
    this.Hide();
  }

  public void ClearJettisonRequest()
  {
    this.ResetState();
  }

  public void CancelJettisonRequest()
  {
    this.Hide();
  }

  protected void RefreshVisuals()
  {
    this.metalsCargo.text = string.Format("{0}", (object) this._cargoMetals);
    this.organicsCargo.text = string.Format("{0}", (object) this._cargoOrganics);
    this.gasCargo.text = string.Format("{0}", (object) this._cargoGas);
    this.radioactivesCargo.text = string.Format("{0}", (object) this._cargoRadioactives);
    this.darkmatterCargo.text = string.Format("{0}", (object) this._cargoDarkmatter);
    this.metalsToJettison.text = string.Format("{0}", (object) this._jettisonMetals);
    this.organicsToJettison.text = string.Format("{0}", (object) this._jettisonOrganics);
    this.gasToJettison.text = string.Format("{0}", (object) this._jettisonGas);
    this.radioactivesToJettison.text = string.Format("{0}", (object) this._jettisonRadioactives);
    this.darkmatterToJettison.text = string.Format("{0}", (object) this._jettisonDarkmatter);
    SBUIButton metalsSingle = this.metalsSingle;
    bool flag1 = this._cargoMetals > 0;
    this.metalsAll.isEnabled = flag1;
    int num1 = flag1 ? 1 : 0;
    metalsSingle.isEnabled = num1 != 0;
    this.metalsHalf.isEnabled = this._cargoMetals > 1;
    SBUIButton organicsSingle = this.organicsSingle;
    bool flag2 = this._cargoOrganics > 0;
    this.organicsAll.isEnabled = flag2;
    bool flag3 = flag2;
    this.organicsHalf.isEnabled = flag3;
    int num2 = flag3 ? 1 : 0;
    organicsSingle.isEnabled = num2 != 0;
    this.organicsHalf.isEnabled = this._cargoOrganics > 1;
    SBUIButton gasSingle = this.gasSingle;
    bool flag4 = this._cargoGas > 0;
    this.gasAll.isEnabled = flag4;
    int num3 = flag4 ? 1 : 0;
    gasSingle.isEnabled = num3 != 0;
    this.gasHalf.isEnabled = this._cargoGas > 1;
    SBUIButton radioactivesSingle = this.radioactivesSingle;
    bool flag5 = this._cargoRadioactives > 0;
    this.radioactivesAll.isEnabled = flag5;
    int num4 = flag5 ? 1 : 0;
    radioactivesSingle.isEnabled = num4 != 0;
    this.radioactivesHalf.isEnabled = this._cargoRadioactives > 1;
    SBUIButton darkmatterSingle = this.darkmatterSingle;
    bool flag6 = this._cargoDarkmatter > 0;
    this.darkmatterAll.isEnabled = flag6;
    int num5 = flag6 ? 1 : 0;
    darkmatterSingle.isEnabled = num5 != 0;
    this.darkmatterHalf.isEnabled = this._cargoDarkmatter > 1;
  }

  protected void RefreshState()
  {
    this.ClampResources(ref this._cargoMetals, ref this._jettisonMetals, (int) TibProxy.gameState.MyShip.Metals);
    this.ClampResources(ref this._cargoOrganics, ref this._jettisonOrganics, (int) TibProxy.gameState.MyShip.Organics);
    this.ClampResources(ref this._cargoGas, ref this._jettisonGas, (int) TibProxy.gameState.MyShip.Gas);
    this.ClampResources(ref this._cargoRadioactives, ref this._jettisonRadioactives, (int) TibProxy.gameState.MyShip.Radioactives);
    this.ClampResources(ref this._cargoDarkmatter, ref this._jettisonDarkmatter, (int) TibProxy.gameState.MyShip.DarkMatter);
  }

  protected void ResetVisuals()
  {
    this.metalsCargo.text = string.Empty;
    this.organicsCargo.text = string.Empty;
    this.gasCargo.text = string.Empty;
    this.radioactivesCargo.text = string.Empty;
    this.darkmatterCargo.text = string.Empty;
    this.metalsToJettison.text = string.Empty;
    this.organicsToJettison.text = string.Empty;
    this.gasToJettison.text = string.Empty;
    this.radioactivesToJettison.text = string.Empty;
    this.darkmatterToJettison.text = string.Empty;
    this.metalsSingle.isEnabled = false;
    this.metalsHalf.isEnabled = false;
    this.metalsAll.isEnabled = false;
    this.organicsSingle.isEnabled = false;
    this.organicsHalf.isEnabled = false;
    this.organicsAll.isEnabled = false;
    this.gasSingle.isEnabled = false;
    this.gasHalf.isEnabled = false;
    this.gasAll.isEnabled = false;
    this.radioactivesSingle.isEnabled = false;
    this.radioactivesHalf.isEnabled = false;
    this.radioactivesAll.isEnabled = false;
    this.darkmatterSingle.isEnabled = false;
    this.darkmatterHalf.isEnabled = false;
    this.darkmatterAll.isEnabled = false;
  }

  protected void ResetState()
  {
    this._jettisonMetals = 0;
    this._jettisonOrganics = 0;
    this._jettisonGas = 0;
    this._jettisonRadioactives = 0;
    this._jettisonDarkmatter = 0;
    this._cargoMetals = 0;
    this._cargoOrganics = 0;
    this._cargoGas = 0;
    this._cargoRadioactives = 0;
    this._cargoDarkmatter = 0;
  }

  private void ClampResources(ref int cargo, ref int transfer, int total)
  {
    if (cargo + transfer == total)
      return;
    if (transfer <= total)
    {
      cargo = total - transfer;
    }
    else
    {
      cargo = 0;
      transfer = total;
    }
  }

  protected override void OnShow()
  {
    base.OnShow();
    this._jettisonMetals = 0;
    this._jettisonOrganics = 0;
    this._jettisonGas = 0;
    this._jettisonRadioactives = 0;
    this._jettisonDarkmatter = 0;
    this.RefreshState();
    this.RefreshVisuals();
  }

  private void Update()
  {
    this.RefreshState();
    this.RefreshVisuals();
  }

  protected override void OnHide()
  {
    this.ResetVisuals();
    this.ResetState();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
