﻿// Decompiled with JetBrains decompiler
// Type: EchoScreenInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EchoScreenInfo : MonoBehaviour
{
  public Camera camera;
  public UICamera uiCamera;
  public UIPanel panel;
  public float cameraFustumHeight;

  private void Start()
  {
    TheInfiniteBlack.Library.Log.I((object) this, nameof (Start), string.Format("screen dpi: {0}", (object) Screen.dpi));
  }

  private void Update()
  {
  }
}
