﻿// Decompiled with JetBrains decompiler
// Type: SerializationTest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Unity.Config;
using UnityEngine;

public class SerializationTest : MonoBehaviour
{
  public UITextList output;
  public string fileName;
  public SerializationTest.TestOperation operation;
  public bool execute;
  public SerializationTest.TestStatus status;
  public List<UserInfo> userInfos;
  private SerializationTest.TestStatus _status;

  protected void Start()
  {
  }

  protected void Update()
  {
    this.status = this._status;
    if (!this.execute)
      return;
    this.execute = false;
    switch (this.operation)
    {
      case SerializationTest.TestOperation.Serialize:
        string text = XMLMan.Serialize<List<UserInfo>>(this.userInfos);
        if (!((Object) this.output != (Object) null))
          break;
        this.output.Add(text);
        break;
    }
  }

  public enum TestOperation
  {
    Serialize,
    Deserialize,
  }

  public enum TestStatus
  {
    None,
    Running,
    Error,
    Completed,
  }
}
