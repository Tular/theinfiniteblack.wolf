﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ConfigManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Settings;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class ConfigManager : IConfigManager
  {
    private readonly SettingsFileManager _fileManager;

    public ConfigManager()
    {
      this.accountManager = new AccountManager();
      this._fileManager = new SettingsFileManager();
    }

    public event EventHandler<SettingsUpdatedEventEventArgs> onSettingsUpdatedEvent;

    public AccountManager accountManager { get; private set; }

    public SystemSettings systemSettings
    {
      get
      {
        return this.accountManager.System;
      }
    }

    public IEnumerable<Account> accounts
    {
      get
      {
        if (this.accountManager == null)
          return (IEnumerable<Account>) new List<Account>(1);
        return (IEnumerable<Account>) this.accountManager.get_Accounts();
      }
    }

    public IEnumerable<AccountCharacterTuple> KnownServerCharacters(sbyte serverId)
    {
      // ISSUE: object of a compiler-generated type is created
      return this.accounts.SelectMany<Account, Character, \u003C\u003E__AnonType0<Account, Character>>((Func<Account, IEnumerable<Character>>) (a => (IEnumerable<Character>) a.get_Characters()), (Func<Account, Character, \u003C\u003E__AnonType0<Account, Character>>) ((a, c) => new \u003C\u003E__AnonType0<Account, Character>(a, c))).Where<\u003C\u003E__AnonType0<Account, Character>>((Func<\u003C\u003E__AnonType0<Account, Character>, bool>) (_param1 => _param1.c.ServerID.Equals(serverId))).Select<\u003C\u003E__AnonType0<Account, Character>, AccountCharacterTuple>((Func<\u003C\u003E__AnonType0<Account, Character>, AccountCharacterTuple>) (_param0 => new AccountCharacterTuple(_param0.a, _param0.c)));
    }

    public sbyte lastServerId
    {
      get
      {
        return this.accountManager.System.LastServerID;
      }
      set
      {
        this.accountManager.System.LastServerID = value;
      }
    }

    public void LoadAll()
    {
      this.LoadAccounts();
    }

    public void SaveAll()
    {
      ClientLog.Instance.I((object) this, "SaveAll()", "Saving account settings");
      if (this.accountManager.get_Accounts().Count == 0)
        return;
      this.SaveAccounts();
      if (this.onSettingsUpdatedEvent == null)
        return;
      this.onSettingsUpdatedEvent((object) this, new SettingsUpdatedEventEventArgs());
    }

    public virtual void LoadAccounts()
    {
      Debug.Log((object) "----- Load Accounts");
      TheInfiniteBlack.Library.Log.D((object) this, nameof (LoadAccounts), string.Format("- {0}", (object) this._fileManager.dataPath));
      this.accountManager.Load((IFileManager) this._fileManager);
      NGUITools.soundVolume = this.accountManager.System.UiSound;
    }

    public virtual void SaveAccounts()
    {
      Debug.Log((object) "----- Save Accounts");
      TheInfiniteBlack.Library.Log.D((object) this, nameof (SaveAccounts), string.Format("- {0}", (object) this._fileManager.dataPath));
      this.accountManager.Save((IFileManager) this._fileManager);
      NGUITools.soundVolume = this.accountManager.System.UiSound;
    }
  }
}
