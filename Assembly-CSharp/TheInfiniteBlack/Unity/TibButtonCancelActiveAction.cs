﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibButtonCancelActiveAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity.Map;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class TibButtonCancelActiveAction : MonoBehaviour
  {
    public SBUIButton cancelAttackButton;
    public SBUIButton cancelFollowButton;
    public UILabel attackCooldownLabel;
    public UILabel followCooldownLabel;
    private UITable _table;

    public void OnCancelAttack()
    {
      TibProxy.gameState.DoAttack((CombatEntity) null);
    }

    public void OnCancelFollow()
    {
      TibProxy.gameState.DoFollow((CombatEntity) null);
    }

    public void OnRecenterMap()
    {
      PreInstantiatedSingleton<SectorMap>.Instance.controller.TranslateToPlayerSector(false);
    }

    private void Awake()
    {
      this._table = this.GetComponent<UITable>();
      EventDelegate.Add(this.cancelAttackButton.onClick, new EventDelegate.Callback(this.OnCancelAttack));
      EventDelegate.Add(this.cancelFollowButton.onClick, new EventDelegate.Callback(this.OnCancelFollow));
    }

    private void Update()
    {
      if (TibProxy.gameState == null)
        return;
      IGameState gameState = TibProxy.gameState;
      if (gameState.MyAttackTarget != null)
      {
        if (!this.cancelAttackButton.enabled)
        {
          this.cancelAttackButton.enabled = true;
          this.cancelAttackButton.GetComponent<UIWidget>().alpha = 1f;
          if ((Object) this._table != (Object) null)
            this._table.repositionNow = true;
        }
        this.attackCooldownLabel.text = !gameState.AttackCooldown.IsFinished ? gameState.AttackCooldown.RemainingSecondsString : string.Empty;
      }
      else
      {
        this.attackCooldownLabel.text = string.Empty;
        this.cancelAttackButton.GetComponent<UIWidget>().alpha = 0.0f;
        this.cancelAttackButton.enabled = false;
        if ((Object) this._table != (Object) null)
          this._table.repositionNow = true;
      }
      if (gameState.MyFollowTarget != null)
      {
        if (!this.cancelFollowButton.enabled)
        {
          this.cancelFollowButton.enabled = true;
          this.cancelFollowButton.GetComponent<UIWidget>().alpha = 1f;
          if ((Object) this._table != (Object) null)
            this._table.repositionNow = true;
        }
        this.followCooldownLabel.text = !gameState.MoveCooldown.IsFinished ? gameState.MoveCooldown.RemainingSecondsString : string.Empty;
        if (!((Object) this._table != (Object) null))
          return;
        this._table.repositionNow = true;
      }
      else
      {
        this.followCooldownLabel.text = string.Empty;
        this.cancelFollowButton.GetComponent<UIWidget>().alpha = 0.0f;
        this.cancelFollowButton.enabled = false;
        if (!((Object) this._table != (Object) null))
          return;
        this._table.repositionNow = true;
      }
    }

    private void OnDisable()
    {
      this.attackCooldownLabel.text = string.Empty;
      this.followCooldownLabel.text = string.Empty;
      this.cancelAttackButton.gameObject.SetActive(false);
      this.cancelFollowButton.gameObject.SetActive(false);
    }
  }
}
