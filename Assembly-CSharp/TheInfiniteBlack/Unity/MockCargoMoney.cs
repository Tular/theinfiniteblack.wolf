﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockCargoMoney
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Reflection;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Unity
{
  public class MockCargoMoney : CargoMoney
  {
    private static readonly FieldInfo _fldBlackDollarAmt = typeof (CargoResource).GetField("_blackDollars", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldCreditAmt = typeof (CargoResource).GetField("_credits", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    public Action onClick;
    public Action onLongPress;

    public MockCargoMoney(IGameState state, int id)
      : base(state, id)
    {
    }

    public int BlackDollarAmt
    {
      set
      {
        MockCargoMoney._fldBlackDollarAmt.SetValue((object) this, (object) value);
      }
    }

    public int CreditAmt
    {
      set
      {
        MockCargoMoney._fldCreditAmt.SetValue((object) this, (object) value);
      }
    }

    public override void OnClick()
    {
      if (this.onClick != null)
        this.onClick();
      base.OnClick();
    }

    public override void OnLongPress()
    {
      if (this.onLongPress != null)
        this.onLongPress();
      base.OnLongPress();
    }
  }
}
