﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MapLoadTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity.Map;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [RequireComponent(typeof (UILabel))]
  public class MapLoadTime : MonoBehaviour
  {
    public SectorMap map;
    private UILabel _label;

    private void Awake()
    {
      this._label = this.GetComponent<UILabel>();
    }

    private void Update()
    {
      this._label.text = string.Format("{0}s", (object) this.map.totalLoadTime);
    }
  }
}
