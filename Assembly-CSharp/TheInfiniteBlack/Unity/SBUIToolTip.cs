﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SBUIToolTip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [RequireComponent(typeof (UIWidget))]
  public class SBUIToolTip : MonoBehaviour
  {
    public float transitionDuration = 0.5f;
    public float showDuration = 2f;
    public Vector3 startScale = new Vector3(1f, 0.0f, 1f);
    public Vector3 endScale = new Vector3(1f, 1f, 1f);
    protected float mHideTime = float.MaxValue;
    public UILabel label;
    public string text;
    public float startDelay;
    protected UIWidget mWidget;
    protected TweenScale mTweener;
    private bool _isInitialized;

    public void Show(float delay, bool autoHide)
    {
      this.Activate();
      this.mTweener.delay = delay;
      if (autoHide)
        this.mHideTime = RealTime.time + this.startDelay + this.transitionDuration + this.showDuration;
      this.mTweener.PlayForward();
    }

    public void ShowOnHover()
    {
      this.Show(0.0f, false);
    }

    public void Hide()
    {
      this.mHideTime = float.MaxValue;
      EventDelegate.Add(this.mTweener.onFinished, new EventDelegate.Callback(this.Deactivate), true);
      this.mTweener.PlayReverse();
    }

    private void Deactivate()
    {
      this.mTweener.enabled = false;
      this.mWidget.transform.localScale = this.startScale;
      this.mWidget.alpha = 0.0f;
    }

    private void Activate()
    {
      this.mWidget.transform.localScale = this.startScale;
      this.mWidget.alpha = 1f;
      this.mTweener.enabled = false;
    }

    public void OnTooltip(bool show)
    {
      if (show)
        this.Show(0.0f, false);
      else
        this.Hide();
    }

    private void Init()
    {
      this.mTweener.duration = this.transitionDuration;
      this.label.text = this.text;
      this.mTweener.ignoreTimeScale = true;
      this.mTweener.from = this.startScale;
      this.mTweener.to = this.endScale;
      this.mTweener.enabled = false;
      this.mWidget.alpha = 0.0f;
      this.mWidget.transform.localScale = this.startScale;
      this._isInitialized = true;
    }

    private void Awake()
    {
      this.mWidget = this.GetComponent<UIWidget>();
      if (!(bool) ((Object) this.label))
        this.label = this.GetComponentInChildren<UILabel>();
      this.mTweener = this.gameObject.AddComponent<TweenScale>();
    }

    private void Start()
    {
      if (this._isInitialized)
        return;
      this.Init();
    }

    private void OnEnable()
    {
      if (!this._isInitialized)
        this.Init();
      this.Show(this.startDelay, true);
    }

    private void OnDisable()
    {
      this.mTweener.onFinished.Clear();
      this.mTweener.PlayForward();
      this.mTweener.ResetToBeginning();
      this.Deactivate();
    }

    private void Update()
    {
      if ((double) RealTime.time <= (double) this.mHideTime)
        return;
      this.Hide();
    }
  }
}
