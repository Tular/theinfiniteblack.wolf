﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MovementButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity.Map;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class MovementButton : MonoBehaviour
  {
    private const float _longPressDelay = 0.5f;
    public UILabel buttonLabel;
    public UISprite borderSprite;
    public UISprite backgroundSprite;
    public string jumpReadyText;
    public string jumpReadyCourseSetText;
    public string followModeText;
    public string courseNeededText;
    public Color normalBackground;
    public Color normalLight;
    public Color normalDark;
    public Color coursePlottedLight;
    public Color coursePlottedDark;
    public Color followingLight;
    public Color followingDark;
    public float backgroundAlpha;
    private bool _isJumpReady;
    private bool _isPaused;
    private bool _isFollowing;
    private bool _isCourseSet;
    private Color _alphaNormalBackground;
    private Color _alphaNormalLight;
    private Color _alphaNormalDark;
    private Color _alphaCoursePlottedLight;
    private Color _alphaCoursePlottedDark;
    private Color _alphaFollowingLight;
    private Color _alphaFollowingDark;
    private string _buttonText;
    private float _pressStart;
    private bool _isPressed;

    private Color AlphaColor(Color color)
    {
      return new Color(color.r, color.g, color.b, this.backgroundAlpha);
    }

    private void UpdateState()
    {
      this._isFollowing = TibProxy.gameState.MyFollowTarget != null;
      this._isPaused = TibProxy.gameState.Course.Paused;
      this._isJumpReady = TibProxy.gameState.MoveCooldown.IsFinished;
      this._isCourseSet = TibProxy.gameState.Course.Count > 0;
      this._buttonText = TibProxy.gameState.MoveCooldown.ToString();
    }

    private void SetColors()
    {
      if (this._isFollowing)
      {
        this.borderSprite.color = !this._isJumpReady ? this.followingDark : this.followingLight;
        this.backgroundSprite.color = !this._isJumpReady ? this._alphaFollowingDark : this._alphaFollowingLight;
        this.buttonLabel.text = !this._isJumpReady ? this._buttonText : this.followModeText;
      }
      else if (this._isCourseSet)
      {
        if (!this._isPaused)
        {
          this.buttonLabel.text = this._buttonText;
          this.borderSprite.color = this.coursePlottedDark;
          this.backgroundSprite.color = this._alphaCoursePlottedDark;
        }
        else
        {
          this.buttonLabel.text = !this._isJumpReady ? this._buttonText : this.jumpReadyCourseSetText;
          this.borderSprite.color = !this._isJumpReady ? this.coursePlottedLight : this.coursePlottedDark;
          this.backgroundSprite.color = this._alphaNormalBackground;
        }
      }
      else
      {
        this.borderSprite.color = !this._isJumpReady ? this.normalDark : this.normalLight;
        this.backgroundSprite.color = this._alphaNormalBackground;
        this.buttonLabel.text = !this._isJumpReady ? this._buttonText : this.jumpReadyText;
      }
    }

    private void OnPress(bool isPressed)
    {
      if (TibProxy.gameState == null)
        return;
      if (isPressed)
      {
        if ((Object) null != (Object) PreInstantiatedSingleton<SectorMap>.Instance)
          PreInstantiatedSingleton<SectorMap>.Instance.controller.mapControlEventsEnabled = false;
        this._isPressed = true;
        this._pressStart = RealTime.time;
      }
      else
      {
        this._isPressed = false;
        PreInstantiatedSingleton<SectorMap>.Instance.controller.mapControlEventsEnabled = true;
        if ((double) RealTime.time - (double) this._pressStart > 0.5)
        {
          DialogManager.ShowDialog<WaypointsDialog>();
          this._pressStart = float.MinValue;
        }
        else
        {
          this._pressStart = float.MinValue;
          CourseController course = TibProxy.gameState.Course;
          if (course.Count > 0)
          {
            if (course.Paused)
              course.Resume();
            else
              course.Pause();
          }
          else
            TibProxy.gameState.ShowAlert(this.courseNeededText);
        }
      }
    }

    private void Awake()
    {
      this._alphaNormalBackground = this.AlphaColor(this.normalBackground);
      this._alphaNormalLight = this.AlphaColor(this.normalLight);
      this._alphaNormalDark = this.AlphaColor(this.normalDark);
      this._alphaCoursePlottedLight = this.AlphaColor(this.coursePlottedLight);
      this._alphaCoursePlottedDark = this.AlphaColor(this.coursePlottedDark);
      this._alphaFollowingDark = this.AlphaColor(this.followingDark);
      this._alphaFollowingLight = this.AlphaColor(this.followingLight);
    }

    private void Update()
    {
      if (TibProxy.gameState == null)
        return;
      if (this._isPressed && (double) RealTime.time - (double) this._pressStart > 0.5)
        this.OnPress(false);
      this.UpdateState();
      this.SetColors();
    }
  }
}
