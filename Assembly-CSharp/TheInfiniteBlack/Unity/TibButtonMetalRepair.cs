﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibButtonMetalRepair
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class TibButtonMetalRepair : MonoBehaviour
  {
    private UISpriteColorGroupControllerEventListener[] _spriteColorgroupEventListeners;
    private bool _canRepair;

    private bool canRepair
    {
      set
      {
        if (value == this._canRepair)
          return;
        this._canRepair = value;
        foreach (UISpriteColorGroupControllerEventListener colorgroupEventListener in this._spriteColorgroupEventListeners)
        {
          if (this._canRepair)
            colorgroupEventListener.SetPressed(false, false);
          else
            colorgroupEventListener.isEnabled = false;
        }
      }
    }

    private static void DoMetalRepair()
    {
      if (TibProxy.gameState == null)
        return;
      TibProxy.gameState.DoRepair((PlayerCombatEntity) TibProxy.gameState.MyShip);
    }

    private void OnPress(bool isPressed)
    {
      if (!isPressed)
        return;
      TibButtonMetalRepair.DoMetalRepair();
    }

    private void Awake()
    {
      this._spriteColorgroupEventListeners = this.GetComponents<UISpriteColorGroupControllerEventListener>();
      this._canRepair = false;
      this.canRepair = true;
    }

    private void Update()
    {
      if (TibProxy.gameState == null || TibProxy.gameState.MyLoc == null)
        return;
      this.canRepair = true;
    }
  }
}
