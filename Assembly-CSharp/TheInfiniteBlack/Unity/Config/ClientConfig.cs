﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Config.ClientConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

namespace TheInfiniteBlack.Unity.Config
{
  public class ClientConfig
  {
    public string optionsFileName;
    public string jumpsFileName;
    public string ignoreFileName;
    public string friendFileName;
    public string orderFileName;
    public string accountsFileName;
    public string ahWatchListFileName;
    public string eventLogFileName;
  }
}
