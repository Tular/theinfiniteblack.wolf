﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Config.Order
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Xml.Serialization;

namespace TheInfiniteBlack.Unity.Config
{
  public class Order
  {
    [XmlAttribute("orderId")]
    public string id;
    [XmlAttribute("stoken")]
    public string token;
    [XmlAttribute("packageName")]
    public string packageName;
    [XmlAttribute("sku")]
    public string sku;
  }
}
