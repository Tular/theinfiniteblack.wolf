﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Config.LoginInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Xml.Serialization;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Unity.Config
{
  [Serializable]
  public class LoginInfo
  {
    [XmlIgnore]
    public string password;
    [XmlAttribute("serverId")]
    public int serverIdInt;
    [XmlAttribute("shipClassId")]
    public int shipClassInt;
    [XmlAttribute("isHardcore")]
    public bool isHardcore;
    [XmlAttribute("universeId")]
    public Universe universe;
    [XmlAttribute("level")]
    public float level;

    public LoginInfo()
    {
      this.shipClass = ShipClass.NULL;
      this.serverId = (sbyte) -1;
    }

    [XmlIgnore]
    public sbyte serverId
    {
      get
      {
        return (sbyte) this.serverIdInt;
      }
      set
      {
        this.serverIdInt = (int) value;
      }
    }

    [XmlIgnore]
    public ShipClass shipClass
    {
      get
      {
        if (Enum.IsDefined(typeof (ShipClass), (object) (sbyte) this.shipClassInt))
          return (ShipClass) Enum.ToObject(typeof (ShipClass), (sbyte) this.shipClassInt);
        return ShipClass.None;
      }
      set
      {
        this.shipClassInt = (int) value;
      }
    }
  }
}
