﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Config.ClientOptionsConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

namespace TheInfiniteBlack.Unity.Config
{
  public class ClientOptionsConfig
  {
    public int musicVolume;
    public int soundVolume;
    public bool playChatAlert;
    public bool showGeneralChat;
    public bool showCorpChat;
    public bool showAllianceChat;
    public bool showSectorChat;
    public bool showPrivateChat;
    public bool showMarketChat;
    public bool showMarketEvents;
    public int visualDetailLevel;
    public bool forceTwoColumn;
    public bool portraitView;
    public bool hideAlly;
    public bool hideEnemy;
    public bool showUIScroll;
    public bool tapToRepair;
    public bool visuals;
    public bool trades;
    public bool invites;
    public bool acceptedEULA;
  }
}
