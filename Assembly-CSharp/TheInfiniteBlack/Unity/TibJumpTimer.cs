﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibJumpTimer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [RequireComponent(typeof (UILabel))]
  public class TibJumpTimer : MonoBehaviour
  {
    private UILabel _label;
    private bool _isJumping;

    private void Awake()
    {
      this._label = this.GetComponent<UILabel>();
      this._isJumping = true;
      this.isJumping = false;
    }

    private bool isJumping
    {
      get
      {
        return this._isJumping;
      }
      set
      {
        if (this._isJumping == value)
          return;
        this._isJumping = value;
        if (this._isJumping)
        {
          if (this._label.enabled)
            return;
          this._label.enabled = true;
        }
        else
        {
          if (this._label.enabled)
            this._label.enabled = false;
          this._label.text = string.Empty;
        }
      }
    }

    private void Update()
    {
      if (TibProxy.gameState == null)
        return;
      this.isJumping = !TibProxy.gameState.EarthJumpTime.IsFinished;
      if (!this.isJumping)
        return;
      string str = string.Format("{0:D2}", (object) (int) TibProxy.gameState.EarthJumpTime.RemainingSeconds);
      if (this._label.text.Equals(str))
        return;
      this._label.text = str;
    }
  }
}
