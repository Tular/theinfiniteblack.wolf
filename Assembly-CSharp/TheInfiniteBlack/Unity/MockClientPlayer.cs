﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockClientPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Reflection;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Unity
{
  public class MockClientPlayer : ClientPlayer
  {
    private static readonly FieldInfo _fldName = typeof (ClientPlayer).GetField("_name", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);

    public MockClientPlayer(IGameState state, int id)
      : base(state, id)
    {
      MockTibProxy.MockInstance.moqGameState.AddPlayer(this);
    }

    static MockClientPlayer()
    {
      if (MockClientPlayer._fldName == null)
        throw new Exception("Name FieldInfo is null");
    }

    public static MockClientPlayer GenerateRandom(IGameState state)
    {
      MockClientPlayer mockClientPlayer = new MockClientPlayer(state, MockTibProxy.nextEntityId);
      mockClientPlayer.SetName(FakeNames.GetRandom());
      return mockClientPlayer;
    }

    public string moqName { get; private set; }

    public MockClientPlayer SetPlayerName(string name)
    {
      this.moqName = name;
      MockClientPlayer._fldName.SetValue((object) this, (object) this.moqName);
      return this;
    }
  }
}
