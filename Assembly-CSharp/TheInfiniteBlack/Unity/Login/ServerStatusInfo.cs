﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.ServerStatusInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;

namespace TheInfiniteBlack.Unity.Login
{
  [Serializable]
  public class ServerStatusInfo
  {
    public sbyte id;
    public string name;
    public string launchDate;
    public string rules;
    public string players;
    public bool isOpen;
    public bool isAcceptingNewPlayers;

    public string statsText
    {
      get
      {
        return string.Format("{0}\n{1}\n{2}", (object) this.launchDate, (object) this.rules, (object) this.players);
      }
    }

    public override string ToString()
    {
      return string.Format("server:{0} {1} {2} {3} {4} {5} {6}", (object) this.id, (object) this.name, (object) this.launchDate, (object) this.rules, (object) this.players, (object) this.isOpen, (object) this.isAcceptingNewPlayers);
    }
  }
}
