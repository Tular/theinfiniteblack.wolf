﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.SelectableButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  [RequireComponent(typeof (UIWidget))]
  [RequireComponent(typeof (UIToggle))]
  public class SelectableButton : MonoBehaviour
  {
    public UILabel buttonLabel;
    public Color fontColorNormal;
    public Color fontColorPressed;
    public Color fontColorHover;
    public Color fontColorSelected;
    public Color fontColorDisabled;
    public SelectableButton.ButtonState stateEcho;
    [HideInInspector]
    public UISprite target;
    [HideInInspector]
    public string normalSprite;
    [HideInInspector]
    public string pressedSprite;
    [HideInInspector]
    public string hoverSprite;
    [HideInInspector]
    public string selectedSprite;
    [HideInInspector]
    public string disabledSprite;
    [HideInInspector]
    public List<EventDelegate> onSelected;
    [HideInInspector]
    public List<EventDelegate> onDeSelected;
    private SelectableButton.ButtonState _state;
    private bool _hasChanged;
    private UIWidget _widget;
    private UIToggle _toggle;

    public SelectableButton.ButtonState state
    {
      get
      {
        return this._state;
      }
      set
      {
        if (this._state == value)
          return;
        this._state = value;
        this.stateEcho = this.state;
        this._hasChanged = true;
      }
    }

    public void SetToggleTrue()
    {
      this._toggle.value = true;
    }

    public void SetToggleFalse()
    {
      this._toggle.value = false;
    }

    public void OnToggleChanged()
    {
      this.state = !UIToggle.current.value ? SelectableButton.ButtonState.Normal : SelectableButton.ButtonState.Selected;
      if (this.state != SelectableButton.ButtonState.Selected || !EventDelegate.IsValid(this.onSelected))
        return;
      EventDelegate.Execute(this.onSelected);
    }

    protected void OnHover(bool isOver)
    {
      if (this.state == SelectableButton.ButtonState.Disabled || this.state == SelectableButton.ButtonState.Selected)
        return;
      this.state = !isOver ? SelectableButton.ButtonState.Normal : SelectableButton.ButtonState.Hover;
    }

    protected void OnPress(bool isDown)
    {
      if (this.state == SelectableButton.ButtonState.Disabled || this.state != SelectableButton.ButtonState.Selected)
        ;
    }

    protected void UpdateLabelColor(UILabel label)
    {
      if ((Object) label == (Object) null)
        return;
      switch (this.state)
      {
        case SelectableButton.ButtonState.Normal:
          label.color = this.fontColorNormal;
          break;
        case SelectableButton.ButtonState.Pressed:
          label.color = this.fontColorPressed;
          break;
        case SelectableButton.ButtonState.Hover:
          label.color = this.fontColorHover;
          break;
        case SelectableButton.ButtonState.Selected:
          label.color = this.fontColorSelected;
          break;
        case SelectableButton.ButtonState.Disabled:
          label.color = this.fontColorDisabled;
          break;
      }
      label.MakePixelPerfect();
    }

    protected void UpdateSpriteColor(UISprite sprite)
    {
      if ((Object) sprite == (Object) null)
        return;
      switch (this.state)
      {
        case SelectableButton.ButtonState.Normal:
          sprite.spriteName = this.normalSprite;
          break;
        case SelectableButton.ButtonState.Pressed:
          sprite.spriteName = this.pressedSprite;
          break;
        case SelectableButton.ButtonState.Hover:
          sprite.spriteName = this.hoverSprite;
          break;
        case SelectableButton.ButtonState.Selected:
          sprite.spriteName = this.selectedSprite;
          break;
        case SelectableButton.ButtonState.Disabled:
          sprite.spriteName = this.disabledSprite;
          break;
      }
      sprite.MakePixelPerfect();
    }

    protected virtual void UpdateButton()
    {
      this.UpdateSpriteColor(this.target);
      this.UpdateLabelColor(this.buttonLabel);
    }

    private void Awake()
    {
      this._toggle = this.GetComponent<UIToggle>();
      this._toggle.optionCanBeNone = true;
    }

    private void Start()
    {
      EventDelegate.Add(this._toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
    }

    private void OnEnable()
    {
      this.state = SelectableButton.ButtonState.Normal;
      if ((Object) this.GetComponent<Collider>() != (Object) null)
        this.GetComponent<Collider>().enabled = true;
      this.UpdateButton();
    }

    private void OnDisable()
    {
      this.state = SelectableButton.ButtonState.Disabled;
      if ((Object) this.GetComponent<Collider>() != (Object) null)
        this.GetComponent<Collider>().enabled = true;
      this.UpdateButton();
      this._toggle.value = false;
    }

    private void Update()
    {
      if (!this._hasChanged)
        return;
      this.UpdateButton();
    }

    public enum ButtonState
    {
      Normal,
      Pressed,
      Hover,
      Selected,
      Disabled,
    }
  }
}
