﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.UIWindowHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  public class UIWindowHelper : MonoBehaviour
  {
    public UIPanel startWindow;
    public UIPanel devServerWatermark;

    public void Show(UIPanel window)
    {
      UIWindow.Close();
      UIWindow.Show(window);
    }

    private void Start()
    {
      UIWindow.Add(this.startWindow);
    }

    private void Update()
    {
      if (TibProxy.UseDevServer)
        this.devServerWatermark.alpha = 1f;
      else
        this.devServerWatermark.alpha = 0.0f;
    }
  }
}
