﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.MotdWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  public class MotdWindow : MonoBehaviour
  {
    public UITextList motdTextList;
    public UILabel clientVersionLabel;
    public SBUIButton loginButton;
    private bool _motdFileLoaded;
    private string _motdFileLoadError;
    private bool _checkFiles;
    private string[] _motdText;
    private bool _showingError;

    public void OpenTutorial()
    {
      InlineWebBrowser.ShowFullScreen("http://www.spellbook.com/tib/tutorial.php?mobile=1");
    }

    public void OpenForums()
    {
      InlineWebBrowser.ShowFullScreen("https://www.spellbook.com/forum/?cat=2&mobile=1");
    }

    public void OpenWiki()
    {
      InlineWebBrowser.ShowFullScreen("https://www.spellbook.com/tib/wiki/");
    }

    private void Start()
    {
      this.loginButton.isEnabled = false;
    }

    private void OnEnable()
    {
      this.clientVersionLabel.text = TibProxy.Instance.clientVersion;
      this._motdFileLoaded = false;
      this._checkFiles = true;
      this.motdTextList.Clear();
      this.motdTextList.Add("-=[ Loading MOTD, Please wait. ]=-");
      this.motdTextList.Add("-=[ If this does not load please check your network connection ]=-");
      this.StartCoroutine(this.LoadMotdFile());
      this.StartCoroutine(ServerInfo.Instance.LoadFromWeb());
    }

    private void OnDisable()
    {
      this.LogD("Hiding Banner View");
    }

    private void OnApplicationPause(bool isPaused)
    {
      if (isPaused)
      {
        this.loginButton.isEnabled = false;
        this._motdFileLoaded = false;
        this._checkFiles = true;
        this._showingError = false;
      }
      if (isPaused)
        return;
      this.motdTextList.Clear();
      this.motdTextList.Add("-=[ Loading MOTD, Please wait. ]=-");
      this.motdTextList.Add("-=[ If this does not load please check your network connection ]=-");
      this.StartCoroutine(this.LoadMotdFile());
      this.StartCoroutine(ServerInfo.Instance.LoadFromWeb());
    }

    private bool filesLoaded
    {
      get
      {
        if (ServerInfo.Instance.isLoaded)
          return this._motdFileLoaded;
        return false;
      }
    }

    private bool wwwError
    {
      get
      {
        if (string.IsNullOrEmpty(ServerInfo.Instance.loadError))
          return !string.IsNullOrEmpty(this._motdFileLoadError);
        return true;
      }
    }

    private void Update()
    {
      if (this.wwwError)
      {
        this.loginButton.isEnabled = false;
        if (this._showingError)
          return;
        this.motdTextList.Clear();
        this.motdTextList.Add("-=[ Error loading Web files.  Please check your Internet connection. ]=-");
        this._checkFiles = true;
        this._showingError = true;
      }
      else
      {
        if (!this._checkFiles || !this.filesLoaded)
          return;
        this._showingError = false;
        this._checkFiles = false;
        this.motdTextList.Clear();
        for (int index = 0; index < this._motdText.Length; ++index)
          this.motdTextList.Add(this._motdText[index]);
        this._motdText = (string[]) null;
        this.loginButton.isEnabled = true;
      }
    }

    [DebuggerHidden]
    private IEnumerator LoadMotdFile()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new MotdWindow.\u003CLoadMotdFile\u003Ec__Iterator21()
      {
        \u003C\u003Ef__this = this
      };
    }
  }
}
