﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.KnownServerCharacters
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using PathologicalGames;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  [RequireComponent(typeof (SpawnPool))]
  [RequireComponent(typeof (UIGrid))]
  public class KnownServerCharacters : MonoBehaviour
  {
    public CharacterCell prefCharacterCard;
    public int characterCardToggleGroup;
    private UIScrollView _scrollView;
    private UIGrid _grid;
    private SpawnPool _cardPool;
    private Transform _prefCardTrans;
    private Transform _gridTrans;
    private bool _isInitialized;

    public void ShowServerAccounts(sbyte serverId, EventDelegate.Callback onCardSelectionChanged)
    {
      this._scrollView.ResetPosition();
      this._cardPool.DespawnAll();
      foreach (AccountCharacterTuple knownServerCharacter in TibProxy.accountManager.KnownServerCharacters(serverId))
      {
        Transform transform = this._cardPool.Spawn(this._prefCardTrans, this._gridTrans);
        transform.transform.localPosition = Vector3.zero;
        CharacterCell component = transform.GetComponent<CharacterCell>();
        component.toggleGroup = this.characterCardToggleGroup;
        EventDelegate.Add(component.toggle.onChange, onCardSelectionChanged);
        component.Initialize(knownServerCharacter);
      }
      this._grid.Reposition();
      this._scrollView.ResetPosition();
    }

    public void ClearList()
    {
      if (!this._isInitialized)
        this.Initialize();
      this._scrollView.ResetPosition();
      this._cardPool.DespawnAll();
      this._grid.Reposition();
      this._scrollView.ResetPosition();
      this._grid.repositionNow = true;
    }

    private void Awake()
    {
      this.Initialize();
    }

    private void Initialize()
    {
      if (this._isInitialized)
        return;
      this._scrollView = NGUITools.FindInParents<UIScrollView>(this.gameObject);
      this._grid = this.GetComponent<UIGrid>();
      this._cardPool = this.GetComponent<SpawnPool>();
      this._prefCardTrans = this.prefCharacterCard.transform;
      this._gridTrans = this._grid.transform;
      this._isInitialized = true;
    }
  }
}
