﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.ServerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  public class ServerInfo : MonoBehaviour
  {
    private readonly List<ServerStatusInfo> _serverStatus = new List<ServerStatusInfo>(10);

    public static ServerInfo Instance { get; private set; }

    public IEnumerable<ServerStatusInfo> serverStatus
    {
      get
      {
        return (IEnumerable<ServerStatusInfo>) ServerInfo.Instance._serverStatus;
      }
    }

    public bool isLoaded { get; private set; }

    public string loadError { get; private set; }

    [DebuggerHidden]
    public IEnumerator LoadFromWeb()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ServerInfo.\u003CLoadFromWeb\u003Ec__Iterator22()
      {
        \u003C\u003Ef__this = this
      };
    }

    private void Awake()
    {
      if ((Object) ServerInfo.Instance != (Object) null && (Object) ServerInfo.Instance != (Object) this)
      {
        Object.Destroy((Object) this);
      }
      else
      {
        if (!((Object) ServerInfo.Instance == (Object) null))
          return;
        ServerInfo.Instance = this;
      }
    }

    private void OnApplicationPause(bool isPaused)
    {
      if (!isPaused)
        return;
      this.loadError = string.Empty;
      this.isLoaded = false;
    }
  }
}
