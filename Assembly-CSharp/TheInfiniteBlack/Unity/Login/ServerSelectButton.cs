﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.ServerSelectButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  [RequireComponent(typeof (UIWidget))]
  [RequireComponent(typeof (UIToggle))]
  public class ServerSelectButton : MonoBehaviour
  {
    public UILabel nameLabel;
    public UILabel statsLabel;
    public Color serverColor;
    public Color fontColorNormal;
    public Color fontColorPressed;
    public Color fontColorHover;
    public Color fontColorSelected;
    public Color fontColorDisabled;
    [HideInInspector]
    public UISprite target;
    [HideInInspector]
    public string normalSprite;
    [HideInInspector]
    public string pressedSprite;
    [HideInInspector]
    public string hoverSprite;
    [HideInInspector]
    public string selectedSprite;
    [HideInInspector]
    public string disabledSprite;
    private ServerSelectButton.ButtonState _state;
    private bool _hasChanged;
    private UIToggle _toggle;
    private ServerStatusInfo _serverInfo;

    public bool isSelected
    {
      get
      {
        return this._toggle.value;
      }
      set
      {
        this._toggle.value = value;
      }
    }

    public sbyte serverId
    {
      get
      {
        return this._serverInfo.id;
      }
    }

    public ServerStatusInfo serverInfo
    {
      get
      {
        return this._serverInfo;
      }
    }

    public List<EventDelegate> onToggleChanged
    {
      get
      {
        if (!(bool) ((Object) this._toggle))
          this._toggle = this.GetComponent<UIToggle>();
        return this._toggle.onChange;
      }
    }

    public ServerSelectButton.ButtonState state
    {
      get
      {
        return this._state;
      }
      set
      {
        this._state = value;
        this._hasChanged = true;
      }
    }

    public void Initialize(ServerStatusInfo info)
    {
      if (!(bool) ((Object) this._toggle))
        this._toggle = this.GetComponent<UIToggle>();
      this._serverInfo = info;
      this.nameLabel.text = this._serverInfo.name;
      this.statsLabel.text = this._serverInfo.statsText;
      EventDelegate.Add(this._toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
      if (!info.isOpen)
      {
        this._toggle.enabled = false;
        this.state = ServerSelectButton.ButtonState.Disabled;
        this.GetComponent<UIWidget>().alpha = 0.0f;
      }
      else
        this.state = !this.isSelected ? ServerSelectButton.ButtonState.Normal : ServerSelectButton.ButtonState.Selected;
    }

    public void OnToggleChanged()
    {
      this.LogI(string.Format("{0} - toggle value: {1}", (object) this.name, (object) this._toggle.value));
      this.state = !this._toggle.value ? ServerSelectButton.ButtonState.Normal : ServerSelectButton.ButtonState.Selected;
    }

    protected void OnHover(bool isOver)
    {
      if (this.state == ServerSelectButton.ButtonState.Disabled || this.state == ServerSelectButton.ButtonState.Selected)
        return;
      this.state = !isOver ? ServerSelectButton.ButtonState.Normal : ServerSelectButton.ButtonState.Hover;
    }

    protected void OnPress(bool isDown)
    {
      if (this.state == ServerSelectButton.ButtonState.Disabled || this.state == ServerSelectButton.ButtonState.Selected)
        return;
      this.nameLabel.color = !isDown ? this.fontColorNormal : this.fontColorPressed;
      this.statsLabel.color = !isDown ? this.fontColorNormal : this.fontColorPressed;
    }

    protected void UpdateButton()
    {
      switch (this.state)
      {
        case ServerSelectButton.ButtonState.Normal:
          if ((Object) this.target != (Object) null)
            this.target.spriteName = this.normalSprite;
          if ((Object) this.nameLabel != (Object) null)
            this.nameLabel.color = this.fontColorNormal;
          if (!((Object) this.statsLabel != (Object) null))
            break;
          this.statsLabel.color = this.fontColorNormal;
          break;
        case ServerSelectButton.ButtonState.Pressed:
          if ((Object) this.target != (Object) null)
            this.target.spriteName = this.pressedSprite;
          if ((Object) this.nameLabel != (Object) null)
            this.nameLabel.color = this.fontColorPressed;
          if (!((Object) this.statsLabel != (Object) null))
            break;
          this.statsLabel.color = this.fontColorPressed;
          break;
        case ServerSelectButton.ButtonState.Hover:
          if ((Object) this.target != (Object) null)
            this.target.spriteName = this.hoverSprite;
          if ((Object) this.nameLabel != (Object) null)
            this.nameLabel.color = this.fontColorHover;
          if (!((Object) this.statsLabel != (Object) null))
            break;
          this.statsLabel.color = this.fontColorHover;
          break;
        case ServerSelectButton.ButtonState.Selected:
          if ((Object) this.target != (Object) null)
            this.target.spriteName = this.selectedSprite;
          if ((Object) this.nameLabel != (Object) null)
            this.nameLabel.color = this.fontColorSelected;
          if (!((Object) this.statsLabel != (Object) null))
            break;
          this.statsLabel.color = this.fontColorSelected;
          break;
        case ServerSelectButton.ButtonState.Disabled:
          if ((Object) this.target != (Object) null)
            this.target.spriteName = this.disabledSprite;
          if ((Object) this.nameLabel != (Object) null)
            this.nameLabel.color = this.fontColorDisabled;
          if (!((Object) this.statsLabel != (Object) null))
            break;
          this.statsLabel.color = this.fontColorDisabled;
          break;
      }
    }

    private void Awake()
    {
      if ((bool) ((Object) this._toggle))
        return;
      this._toggle = this.GetComponent<UIToggle>();
    }

    private void OnEnable()
    {
      if (!(bool) ((Object) this._toggle))
        this._toggle = this.GetComponent<UIToggle>();
      this._hasChanged = true;
    }

    private void Update()
    {
      if (!this.isSelected && this.state == ServerSelectButton.ButtonState.Selected)
        this.state = ServerSelectButton.ButtonState.Normal;
      if (!this._hasChanged)
        return;
      this.UpdateButton();
      this._hasChanged = false;
    }

    public enum ButtonState
    {
      Normal,
      Pressed,
      Hover,
      Selected,
      Disabled,
    }
  }
}
