﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.LoginWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  public class LoginWindow : MonoBehaviour
  {
    public KnownServerCharacters knownCharacters;
    public ServerSelectButton server0;
    public ServerSelectButton server1;
    public ServerSelectButton server2;
    public ServerSelectButton server3;
    public ServerSelectButton server4;
    public UIInput accountNameInput;
    public UIInput accountPasswordInput;
    public UIToggle rememberPasswordSwitch;
    public string eulaUrl;
    private ServerStatusInfo _selectedServerInfo;
    private CharacterCell _selectedCell;
    private bool _isInitialized;

    public void OnLoginButtonClicked()
    {
      if (this._selectedServerInfo == null)
        TibProxy.Instance.Show(new PopupWindowEventArgs((IGameState) null, "Must select a server!", true));
      else
        TibProxy.Instance.Login(this._selectedServerInfo.id, this.accountNameInput.value, this.accountPasswordInput.value);
    }

    public void OpenEula()
    {
      Application.OpenURL(this.eulaUrl);
    }

    public void OnServerSelectionChanged()
    {
      if ((UnityEngine.Object) UIToggle.current != (UnityEngine.Object) null)
        this.LogI("OnServerSelectionChanged() - Processing: " + UIToggle.current.name);
      else
        this.LogI("OnServerSelectionChanged() - UIToggle.current is null");
      if (!this._isInitialized || (UnityEngine.Object) UIToggle.current == (UnityEngine.Object) null || !UIToggle.current.value)
        return;
      ServerSelectButton component = UIToggle.current.GetComponent<ServerSelectButton>();
      this._selectedServerInfo = component.serverInfo;
      this.LogI(string.Format("selected server ID: {0}", (object) component.serverId));
      this.knownCharacters.ShowServerAccounts(component.serverId, new EventDelegate.Callback(this.OnCharacterSelectionChanged));
      TibProxy.accountManager.systemSettings.LastServerID = component.serverId;
    }

    public void OnCharacterSelectionChanged()
    {
      if (!UIToggle.current.value)
        return;
      this.SetInputValues(UIToggle.current.GetComponent<CharacterCell>());
    }

    private void SetInputValues(CharacterCell selectedCell)
    {
      if ((UnityEngine.Object) selectedCell != (UnityEngine.Object) null)
      {
        this.accountNameInput.value = selectedCell.accountName;
        this.accountPasswordInput.value = selectedCell.accountPassword;
        this.rememberPasswordSwitch.value = selectedCell.isPasswordSaved;
        TheInfiniteBlack.Library.Log.D((object) this, nameof (SetInputValues), string.Format("isPasswordSaved: {0}", (object) selectedCell.isPasswordSaved));
      }
      else
      {
        this.accountNameInput.value = string.Empty;
        this.accountPasswordInput.value = string.Empty;
        if (TibProxy.accountManager != null)
          this.rememberPasswordSwitch.value = TibProxy.accountManager.systemSettings.RememberPassword;
      }
      this._selectedCell = selectedCell;
    }

    protected void InitializeServerButtons()
    {
      TheInfiniteBlack.Library.Log.D((object) this, nameof (InitializeServerButtons), string.Format("LastServerId: {0}", (object) TibProxy.accountManager.systemSettings.LastServerID));
      Dictionary<int, ServerStatusInfo> dictionary = ServerInfo.Instance.serverStatus.ToDictionary<ServerStatusInfo, int, ServerStatusInfo>((Func<ServerStatusInfo, int>) (k => (int) k.id), (Func<ServerStatusInfo, ServerStatusInfo>) (v => v));
      if (dictionary.ContainsKey(0))
      {
        EventDelegate.Add(this.server0.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server0.Initialize(dictionary[0]);
        if (this._isInitialized && this.server0.isSelected)
          this.knownCharacters.ShowServerAccounts(this.server0.serverId, new EventDelegate.Callback(this.OnCharacterSelectionChanged));
      }
      if (dictionary.ContainsKey(1))
      {
        EventDelegate.Add(this.server1.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server1.Initialize(dictionary[1]);
        if (this._isInitialized && this.server1.isSelected)
          this.knownCharacters.ShowServerAccounts(this.server1.serverId, new EventDelegate.Callback(this.OnCharacterSelectionChanged));
      }
      if (dictionary.ContainsKey(2))
      {
        EventDelegate.Add(this.server2.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server2.Initialize(dictionary[2]);
        if (this._isInitialized && this.server2.isSelected)
          this.knownCharacters.ShowServerAccounts(this.server2.serverId, new EventDelegate.Callback(this.OnCharacterSelectionChanged));
      }
      if (dictionary.ContainsKey(3))
      {
        EventDelegate.Add(this.server3.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server3.Initialize(dictionary[3]);
        if (this._isInitialized && this.server3.isSelected)
          this.knownCharacters.ShowServerAccounts(this.server3.serverId, new EventDelegate.Callback(this.OnCharacterSelectionChanged));
      }
      if (dictionary.ContainsKey(4))
      {
        EventDelegate.Add(this.server4.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server4.Initialize(dictionary[4]);
        if (this._isInitialized && this.server4.isSelected)
          this.knownCharacters.ShowServerAccounts(this.server4.serverId, new EventDelegate.Callback(this.OnCharacterSelectionChanged));
      }
      if (this._isInitialized)
        return;
      this._isInitialized = true;
      if ((int) TibProxy.accountManager.systemSettings.LastServerID == 0)
        this.server0.isSelected = true;
      if ((int) TibProxy.accountManager.systemSettings.LastServerID == 1)
        this.server1.isSelected = true;
      if ((int) TibProxy.accountManager.systemSettings.LastServerID == 2)
        this.server2.isSelected = true;
      if ((int) TibProxy.accountManager.systemSettings.LastServerID == 3)
        this.server3.isSelected = true;
      if ((int) TibProxy.accountManager.systemSettings.LastServerID != 4)
        return;
      this.server4.isSelected = true;
    }

    [DebuggerHidden]
    private IEnumerator InitializeCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new LoginWindow.\u003CInitializeCoroutine\u003Ec__Iterator20()
      {
        \u003C\u003Ef__this = this
      };
    }

    private void OnRememberPasswordToggleChanged()
    {
      if (TibProxy.accountManager != null)
        TibProxy.accountManager.systemSettings.RememberPassword = this.rememberPasswordSwitch.value;
      if ((UnityEngine.Object) null == (UnityEngine.Object) this._selectedCell)
        UnityEngine.Debug.Log((object) "OnRememberPasswordToggleChanged - selected cell is null");
      else
        UnityEngine.Debug.Log((object) ("OnRememberPasswordToggleChanged - selectedCell: " + this._selectedCell.accountName));
      UnityEngine.Debug.Log((object) ("OnRememberPasswordToggleChanged - current value: " + (object) this.rememberPasswordSwitch.value));
    }

    private void OnEnable()
    {
      TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
      TibProxy.Instance.onLoginSuccessEvent += new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
      this.knownCharacters.ClearList();
      this.StartCoroutine(this.InitializeCoroutine());
      this.rememberPasswordSwitch.value = TibProxy.accountManager.systemSettings.RememberPassword;
    }

    private void Awake()
    {
      EventDelegate.Add(this.rememberPasswordSwitch.onChange, new EventDelegate.Callback(this.OnRememberPasswordToggleChanged));
    }

    private void OnDisable()
    {
      TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
      this.knownCharacters.ClearList();
      this.SetInputValues((CharacterCell) null);
    }

    private void TibProxy_onLoginSuccessEvent(object sender, LoginSuccessEventArgs e)
    {
      PreInstantiatedSingleton<SceneManager>.Instance.GotoMainGameScene();
    }
  }
}
