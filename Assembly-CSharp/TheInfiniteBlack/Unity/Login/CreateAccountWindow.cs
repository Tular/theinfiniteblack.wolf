﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Login.CreateAccountWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Login
{
  public class CreateAccountWindow : MonoBehaviour
  {
    public ServerSelectButton server0;
    public ServerSelectButton server1;
    public ServerSelectButton server2;
    public ServerSelectButton server3;
    public ServerSelectButton server4;
    public UIInput accountNameInput;
    public UIInput passwordInput1;
    public UIInput passwordInput2;
    public UIToggle rememberPasswordToggle;
    public UIToggle hardcoreToggle;
    private bool _isHardcore;
    private ServerStatusInfo _selectedServerInfo;
    private bool _isInitialized;
    private bool _isStarted;

    public void OnHardcoreToggle()
    {
      this._isHardcore = !this._isHardcore;
    }

    public void OnCreateNewAccount()
    {
      if (this._selectedServerInfo == null)
      {
        TibProxy.Instance.Show(new PopupWindowEventArgs((IGameState) null, "Must select a server!", true));
      }
      else
      {
        if (!this.ValidatePasswordFields())
          return;
        sbyte id = this._selectedServerInfo.id;
        string userName = this.accountNameInput.value;
        string password = this.passwordInput2.value;
        bool isHardcore = this.hardcoreToggle.value;
        TibProxy.accountManager.systemSettings.RememberPassword = this.rememberPasswordToggle.value;
        TibProxy.Instance.CreateNewAccount(id, userName, password, isHardcore);
      }
    }

    public void OnServerSelectionChanged()
    {
      if ((UnityEngine.Object) UIToggle.current != (UnityEngine.Object) null)
        this.LogI("OnServerSelectionChanged() - Processing: " + UIToggle.current.name);
      else
        this.LogI("OnServerSelectionChanged() - UIToggle.current is null");
      if (!this._isInitialized || (UnityEngine.Object) UIToggle.current == (UnityEngine.Object) null || !UIToggle.current.value)
        return;
      ServerSelectButton component = UIToggle.current.GetComponent<ServerSelectButton>();
      this._selectedServerInfo = component.serverInfo;
      TibProxy.accountManager.systemSettings.LastServerID = component.serverId;
    }

    public bool ValidatePasswordFields()
    {
      if (!string.IsNullOrEmpty(this.passwordInput1.value) && !string.IsNullOrEmpty(this.passwordInput2.value) && this.passwordInput1.value.Equals(this.passwordInput2.value))
        return true;
      this.passwordInput2.value = string.Empty;
      TibProxy.Instance.Show(new PopupWindowEventArgs(TibProxy.gameState, "Password fields required and must match", true));
      return false;
    }

    protected void InitializeServerButtons()
    {
      TheInfiniteBlack.Library.Log.D((object) this, nameof (InitializeServerButtons), string.Format("LastServerId: {0}", (object) TibProxy.accountManager.systemSettings.LastServerID));
      Dictionary<int, ServerStatusInfo> dictionary = ServerInfo.Instance.serverStatus.ToDictionary<ServerStatusInfo, int, ServerStatusInfo>((Func<ServerStatusInfo, int>) (k => (int) k.id), (Func<ServerStatusInfo, ServerStatusInfo>) (v => v));
      if (dictionary.ContainsKey(0))
      {
        EventDelegate.Add(this.server0.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server0.Initialize(dictionary[0]);
      }
      if (dictionary.ContainsKey(1))
      {
        EventDelegate.Add(this.server1.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server1.Initialize(dictionary[1]);
      }
      if (dictionary.ContainsKey(2))
      {
        EventDelegate.Add(this.server2.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server2.Initialize(dictionary[2]);
      }
      if (dictionary.ContainsKey(3))
      {
        EventDelegate.Add(this.server3.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server3.Initialize(dictionary[3]);
      }
      if (dictionary.ContainsKey(4))
      {
        EventDelegate.Add(this.server4.onToggleChanged, new EventDelegate.Callback(this.OnServerSelectionChanged));
        this.server4.Initialize(dictionary[4]);
      }
      this._isInitialized = true;
      if (this.server4.serverInfo.isOpen)
        this.server4.isSelected = true;
      else
        this.server3.isSelected = true;
    }

    private void TibProxy_onLoginSuccessEvent(object sender, LoginSuccessEventArgs e)
    {
      TibProxy.Instance.Show(e);
      PreInstantiatedSingleton<SceneManager>.Instance.GotoMainGameScene();
    }

    private void Start()
    {
      this.InitializeServerButtons();
      this.rememberPasswordToggle.value = TibProxy.accountManager.systemSettings.RememberPassword;
      this._isStarted = true;
    }

    [DebuggerHidden]
    private IEnumerator InitializeCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new CreateAccountWindow.\u003CInitializeCoroutine\u003Ec__Iterator1F()
      {
        \u003C\u003Ef__this = this
      };
    }

    private void OnEnable()
    {
      TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
      TibProxy.Instance.onLoginSuccessEvent += new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
      if (!this._isStarted)
        return;
      this.StartCoroutine(this.InitializeCoroutine());
      this.rememberPasswordToggle.value = TibProxy.accountManager.systemSettings.RememberPassword;
    }

    private void OnDisable()
    {
      TibProxy.Instance.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.TibProxy_onLoginSuccessEvent);
    }
  }
}
