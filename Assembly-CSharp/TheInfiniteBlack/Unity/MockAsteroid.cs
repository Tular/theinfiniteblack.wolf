﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockAsteroid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Reflection;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Unity
{
  public class MockAsteroid : Asteroid
  {
    private static readonly FieldInfo _fldClass = typeof (Asteroid).GetField("_class", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    public Action onClick;
    public Action onLongPress;

    public MockAsteroid(IGameState state, int id)
      : base(state, id)
    {
    }

    static MockAsteroid()
    {
      if (MockAsteroid._fldClass == null)
        throw new Exception("AsteroidClass FieldInfo is null");
    }

    public AsteroidClass asteroidClass
    {
      set
      {
        MockAsteroid._fldClass.SetValue((object) this, (object) value);
      }
    }

    public static MockAsteroid GenerateRandom(IGameState state)
    {
      return new MockAsteroid(state, MockTibProxy.nextEntityId)
      {
        asteroidClass = (AsteroidClass) UnityEngine.Random.Range(0, 19)
      };
    }

    public override void OnClick()
    {
      if (this.onClick != null)
        this.onClick();
      base.OnClick();
    }

    public override void OnLongPress()
    {
      if (this.onLongPress != null)
        this.onLongPress();
      base.OnLongPress();
    }
  }
}
