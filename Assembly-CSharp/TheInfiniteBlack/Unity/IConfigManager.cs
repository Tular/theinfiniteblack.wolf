﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.IConfigManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Unity
{
  public interface IConfigManager
  {
    event EventHandler<SettingsUpdatedEventEventArgs> onSettingsUpdatedEvent;

    IEnumerable<Account> accounts { get; }

    SystemSettings systemSettings { get; }

    IEnumerable<AccountCharacterTuple> KnownServerCharacters(sbyte serverId);

    AccountManager accountManager { get; }

    void LoadAll();

    void SaveAll();
  }
}
