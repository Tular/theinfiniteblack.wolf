﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.AdManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using GoogleMobileAds.Api;
using System;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [AdvancedInspector.AdvancedInspector]
  public class AdManager : PreInstantiatedSingleton<AdManager>
  {
    [Inspect(0)]
    public string adUnitId;
    [Inspect(1)]
    public bool runAsTest;
    [Inspect(2, MethodName = "IsRunningAsTest")]
    public string testDeviceId;
    private BannerView _bannerView;
    private ScreenOrientation _bannerOrientation;

    public bool isBannerShowing { get; private set; }

    public void ShowBanner()
    {
      this.LogD("Showing Banner Ad View");
    }

    public void HideBanner()
    {
      this.LogD("Hiding Banner Ad View");
    }

    private void RequestBanner(bool showView)
    {
      if (string.IsNullOrEmpty(this.adUnitId) || this.isBannerShowing)
        return;
      if (this._bannerView == null)
      {
        this._bannerOrientation = Screen.orientation;
        try
        {
          this._bannerView = new BannerView(this.adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
        }
        catch (Exception ex)
        {
          Debug.LogException(ex);
          return;
        }
        this._bannerView.OnAdFailedToLoad += new EventHandler<AdFailedToLoadEventArgs>(this._bannerView_AdFailedToLoad);
        if (!showView)
        {
          this._bannerView.Hide();
        }
        else
        {
          this._bannerView.Show();
          this.isBannerShowing = true;
        }
        this.LogD("Building Banner Ad");
        AdRequest.Builder builder = new AdRequest.Builder();
        if (this.runAsTest)
          builder.AddTestDevice("SIMULATOR").AddTestDevice(this.testDeviceId);
        AdRequest request = builder.Build();
        this.LogD("Loading Banner Ad");
        try
        {
          this._bannerView.LoadAd(request);
        }
        catch (Exception ex)
        {
          Debug.LogException(ex);
        }
      }
      this.LogD("Showing Banner View.");
    }

    private void _bannerView_AdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
      this.LogE(new Exception(e.Message));
      this.isBannerShowing = false;
      if (this._bannerView != null)
        this._bannerView.Destroy();
      this._bannerView = (BannerView) null;
    }

    private bool IsRunningAsTest()
    {
      return this.runAsTest;
    }
  }
}
