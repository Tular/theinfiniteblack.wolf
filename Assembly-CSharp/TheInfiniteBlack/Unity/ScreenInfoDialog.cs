﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ScreenInfoDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Linq;
using System.Text;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class ScreenInfoDialog : DialogBase
  {
    public UILabel label;
    public UISwitchControl adjustByDpiToggle;
    private Camera _uiCamera;

    public void UpdateScreenInfo()
    {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendFormat("Camera Aspect Ratio: {0}\n", (object) this._uiCamera.aspect);
      stringBuilder.AppendFormat("Reported Resolution: {0}x{1}\n", (object) Screen.currentResolution.width, (object) Screen.currentResolution.height);
      stringBuilder.AppendFormat("Reported Height: {0}\n", (object) Screen.height);
      stringBuilder.AppendFormat("Reported Width: {0}\n", (object) Screen.width);
      stringBuilder.AppendFormat("Reported DPI: {0}\n", (object) Screen.dpi);
      stringBuilder.AppendFormat("Height Adjusted By DPI: {0}\n", (object) NGUIMath.AdjustByDPI((float) Screen.height));
      if ((Object) UIRoot.list.FirstOrDefault<UIRoot>() != (Object) null)
      {
        UIRoot inParents = NGUITools.FindInParents<UIRoot>(this.gameObject);
        stringBuilder.AppendFormat("UIRoot Active Height: {0}\n", (object) inParents.activeHeight);
        stringBuilder.AppendFormat("UIRoot Scaling Style: {0}\n", (object) inParents.scalingStyle);
        if (inParents.scalingStyle == UIRoot.Scaling.Constrained)
        {
          stringBuilder.AppendFormat("UIRoot Maximum Height: {0}\n", (object) inParents.maximumHeight);
          stringBuilder.AppendFormat("UIRoot Minimum Height: {0}\n", (object) inParents.minimumHeight);
        }
        else
          stringBuilder.AppendFormat("UIRoot Manual Height: {0}\n", (object) inParents.manualHeight);
        stringBuilder.AppendFormat("UIRoot Adjust by DPI: {0}\n", (object) inParents.adjustByDPI);
        stringBuilder.AppendFormat("UIRoot Pixel Size Adjustment: {0}\n", (object) inParents.pixelSizeAdjustment);
      }
      this.label.text = stringBuilder.ToString();
    }

    protected override void OnRegisterEventHandlers()
    {
    }

    protected override void OnUnRegisterEventHandlers()
    {
    }

    private new void Awake()
    {
    }

    private void Start()
    {
    }

    private new void OnEnable()
    {
      this._uiCamera = NGUITools.FindCameraForLayer(this.gameObject.layer);
    }

    private void Update()
    {
      this.UpdateScreenInfo();
    }
  }
}
