﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.WebFileDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.IO;

namespace TheInfiniteBlack.Unity
{
  [AdvancedInspector.AdvancedInspector]
  [Serializable]
  public class WebFileDefinition
  {
    [Inspect(0)]
    public string fileAlias;
    [Inspect(1)]
    public string fileUrl;
    [Inspect(2)]
    public bool isCached;
    [Inspect(3, MethodName = "IsCached")]
    public float refreshInterval;

    [ReadOnly]
    [Inspect(4)]
    public string fileName
    {
      get
      {
        if (string.IsNullOrEmpty(this.fileUrl))
          return string.Empty;
        return Path.GetFileName(this.fileUrl);
      }
    }

    public override string ToString()
    {
      if (string.IsNullOrEmpty(this.fileAlias))
        return "[No Alias Defined]";
      return this.fileAlias;
    }

    private bool IsCached()
    {
      return this.isCached;
    }
  }
}
