﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.AudioManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using DarkTonic.MasterAudio;
using System;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library;
using UnityEngine;
using UnityEngine.Audio;

namespace TheInfiniteBlack.Unity
{
  [AdvancedInspector.AdvancedInspector]
  public class AudioManager : MonoBehaviour, IDataChanged
  {
    private bool _inHostileTerritory = true;
    [Inspect(0)]
    public AudioMixer mixer;
    [Inspect(1)]
    public string passivePlaylistName;
    [Inspect(2)]
    public string agressivePlaylistName;
    [Inspect(3)]
    public float hostileTerritoryDelay;
    private float _settingsUiVolume;
    private float _settingsMusicVolume;
    private float _settingsSfxVolume;
    private GroupBus _uiBus;
    private GroupBus _sfxBus;
    private DarkTonic.MasterAudio.MasterAudio.Playlist _mPassivePlaylist;
    private DarkTonic.MasterAudio.MasterAudio.Playlist _mAgressivePlaylist;
    private DarkTonic.MasterAudio.MasterAudio.Playlist _mStartupPlaylist;
    private DarkTonic.MasterAudio.MasterAudio.Playlist _mCurrentPlaylist;
    private bool _hasStarted;
    private float _changePlaylistAtTime;

    public event GenericEventHandler OnDataChanged;

    [Inspect(0)]
    [Group("Debug Info", Priority = 10)]
    [ReadOnly]
    public string currentPlaylistName
    {
      get
      {
        if (Application.isPlaying && this._mPlaylistController.ControllerIsReady)
          return this._mCurrentPlaylist.playlistName;
        return string.Empty;
      }
    }

    [Group("Debug Info")]
    [Inspect(1)]
    [ReadOnly]
    public float settingsUiVolume
    {
      get
      {
        return this._settingsUiVolume;
      }
    }

    [ReadOnly]
    [Group("Debug Info")]
    [Inspect(2)]
    public float settingsSfxVolume
    {
      get
      {
        return this._settingsSfxVolume;
      }
    }

    [Group("Debug Info")]
    [ReadOnly]
    [Inspect(3)]
    public float settingsMusicVolume
    {
      get
      {
        return this._settingsMusicVolume;
      }
    }

    [Inspect(4)]
    [ReadOnly]
    [Group("Debug Info")]
    public float maUiVolume
    {
      get
      {
        if (Application.isPlaying && DarkTonic.MasterAudio.MasterAudio.SoundsReady && this._uiBus != null)
          return this._uiBus.volume;
        return 0.0f;
      }
    }

    [Inspect(5)]
    [Group("Debug Info")]
    [ReadOnly]
    public float maSfxVolume
    {
      get
      {
        if (Application.isPlaying && DarkTonic.MasterAudio.MasterAudio.SoundsReady && this._sfxBus != null)
          return this._sfxBus.volume;
        return 0.0f;
      }
    }

    [Inspect(6)]
    [ReadOnly]
    [Group("Debug Info")]
    public float maMusicVolume
    {
      get
      {
        if (Application.isPlaying && this._mPlaylistController.ControllerIsReady)
          return this._mMusicVolume;
        return 0.0f;
      }
    }

    [Group("Debug Info")]
    [Inspect(7)]
    [ReadOnly]
    public bool isPlaylistMuted
    {
      get
      {
        if (Application.isPlaying && this._mPlaylistController.ControllerIsReady)
          return DarkTonic.MasterAudio.MasterAudio.PlaylistsMuted;
        return false;
      }
    }

    [ReadOnly]
    [Inspect(8)]
    [Group("Debug Info")]
    public bool inHostileTerritory
    {
      get
      {
        return this._inHostileTerritory;
      }
    }

    private PlaylistController _mPlaylistController
    {
      get
      {
        if (DarkTonic.MasterAudio.MasterAudio.SoundsReady)
          return DarkTonic.MasterAudio.MasterAudio.OnlyPlaylistController;
        return (PlaylistController) null;
      }
    }

    private float _mMusicVolume
    {
      get
      {
        if (Application.isPlaying && DarkTonic.MasterAudio.MasterAudio.SoundsReady)
          return DarkTonic.MasterAudio.MasterAudio.PlaylistMasterVolume;
        return 0.0f;
      }
      set
      {
        if (!Application.isPlaying || !DarkTonic.MasterAudio.MasterAudio.SoundsReady)
          return;
        DarkTonic.MasterAudio.MasterAudio.PlaylistMasterVolume = value;
      }
    }

    [DebuggerHidden]
    private IEnumerator Start()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AudioManager.\u003CStart\u003Ec__Iterator29()
      {
        \u003C\u003Ef__this = this
      };
    }

    private void UpdateMixerLevels()
    {
      this._settingsMusicVolume = TibProxy.accountManager.accountManager.System.Music;
      this._settingsUiVolume = TibProxy.accountManager.accountManager.System.UiSound;
      this._settingsSfxVolume = TibProxy.accountManager.accountManager.System.CombatSound;
      bool flag = false;
      if (!Mathf.Approximately(this._settingsUiVolume, this._uiBus.volume))
      {
        this._uiBus.volume = this._settingsUiVolume;
        flag = true;
      }
      if (!Mathf.Approximately(this._settingsSfxVolume, this._sfxBus.volume))
      {
        this._sfxBus.volume = this._settingsSfxVolume;
        flag = true;
      }
      if (Mathf.Approximately(this._settingsMusicVolume, this._mMusicVolume))
        return;
      this._mMusicVolume = this._settingsMusicVolume;
      if ((double) this._mMusicVolume <= 0.0)
      {
        DarkTonic.MasterAudio.MasterAudio.MuteAllPlaylists();
        this._mMusicVolume = 0.0f;
      }
      else
      {
        if (DarkTonic.MasterAudio.MasterAudio.PlaylistsMuted)
          DarkTonic.MasterAudio.MasterAudio.UnmuteAllPlaylists();
        if (this._mPlaylistController.PlaylistState == PlaylistController.PlaylistStates.Stopped)
          DarkTonic.MasterAudio.MasterAudio.StartPlaylist(this._mCurrentPlaylist.playlistName);
      }
      flag = true;
    }

    private void UpdateMusicPlaylist(bool stopMutedPlaylists = false)
    {
      DarkTonic.MasterAudio.MasterAudio.Playlist playlist1;
      if (!this.TryGetIntendedPlaylist(out playlist1))
        return;
      DarkTonic.MasterAudio.MasterAudio.Playlist playlist2 = playlist1;
      if (stopMutedPlaylists && DarkTonic.MasterAudio.MasterAudio.PlaylistsMuted)
        this.StopPlaylist();
      else if (this._mCurrentPlaylist != playlist2 && !DarkTonic.MasterAudio.MasterAudio.PlaylistsMuted)
        this._mPlaylistController.StartPlaylist(playlist2.playlistName);
      this._mCurrentPlaylist = playlist2;
    }

    private bool TryGetIntendedPlaylist(out DarkTonic.MasterAudio.MasterAudio.Playlist playlist)
    {
      playlist = (DarkTonic.MasterAudio.MasterAudio.Playlist) null;
      if (TibProxy.gameState == null || TibProxy.gameState.MyLoc == null)
      {
        playlist = this._mStartupPlaylist;
        return true;
      }
      switch (TibProxy.gameState.MyLoc.Class)
      {
        case SectorClass.PROTECTED:
          this._inHostileTerritory = false;
          playlist = this._mPassivePlaylist;
          return true;
        case SectorClass.NEUTRAL:
        case SectorClass.CONTESTED:
          if (DarkTonic.MasterAudio.MasterAudio.PlaylistsMuted)
          {
            this._inHostileTerritory = true;
            this._changePlaylistAtTime = Time.time;
            playlist = this._mAgressivePlaylist;
            return true;
          }
          if (!this._inHostileTerritory)
          {
            this._inHostileTerritory = true;
            this._changePlaylistAtTime = Time.time + this.hostileTerritoryDelay;
          }
          playlist = (double) this._changePlaylistAtTime > (double) Time.time ? this._mPassivePlaylist : this._mAgressivePlaylist;
          return true;
        default:
          return false;
      }
    }

    private void StopPlaylist()
    {
      if (!DarkTonic.MasterAudio.MasterAudio.PlaylistsMuted)
        return;
      this._mMusicVolume = 0.0f;
      DarkTonic.MasterAudio.MasterAudio.UnmuteAllPlaylists();
      DarkTonic.MasterAudio.MasterAudio.StopAllPlaylists();
    }

    private void AccountManager_onSettingsUpdatedEvent(object sender, SettingsUpdatedEventEventArgs e)
    {
      this.UpdateMusicPlaylist(true);
    }

    private void OnEnable()
    {
      this._inHostileTerritory = true;
      this._changePlaylistAtTime = Time.time;
      if (!this._hasStarted)
        return;
      TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
      TibProxy.accountManager.onSettingsUpdatedEvent += new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
    }

    private void OnDisable()
    {
      TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
    }

    private void Update()
    {
      if (!this._hasStarted)
        return;
      this.UpdateMixerLevels();
      this.UpdateMusicPlaylist(false);
    }

    public void DataChanged()
    {
    }

    private void FireDataChanged()
    {
      if (!Application.isEditor || this.OnDataChanged == null)
        return;
      this.OnDataChanged();
    }
  }
}
