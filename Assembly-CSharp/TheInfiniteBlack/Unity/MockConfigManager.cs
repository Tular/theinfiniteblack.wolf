﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockConfigManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class MockConfigManager : ConfigManager
  {
    public bool saveFilesToDisk;
    public bool loadFilesFromDisk;

    public bool executeDiskIO
    {
      set
      {
        this.saveFilesToDisk = value;
        this.loadFilesFromDisk = value;
      }
    }

    public override void LoadAccounts()
    {
      if (this.loadFilesFromDisk)
        base.LoadAccounts();
      else
        Debug.Log((object) string.Format("MockConfigManager.LoadAccounts() - skipping disk load"));
    }

    public override void SaveAccounts()
    {
      if (this.saveFilesToDisk)
        base.SaveAccounts();
      else
        Debug.Log((object) string.Format("MockConfigManager.LoadAccounts() - skipping disk save"));
    }
  }
}
