﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.EchoSystemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Net.NetworkInformation;
using System.Text;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [RequireComponent(typeof (UILabel))]
  public class EchoSystemInfo : MonoBehaviour
  {
    public UILabel target;

    private string GetMacAddress()
    {
      string empty = string.Empty;
      foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
      {
        switch (networkInterface.NetworkInterfaceType)
        {
          case NetworkInterfaceType.Ethernet:
          case NetworkInterfaceType.GigabitEthernet:
          case NetworkInterfaceType.FastEthernetFx:
            empty = networkInterface.GetPhysicalAddress().ToString();
            goto label_5;
          default:
            continue;
        }
      }
label_5:
      return empty;
    }

    private void Start()
    {
      this.target = this.GetComponent<UILabel>();
      StringBuilder stringBuilder = new StringBuilder(256);
      string deviceModel = SystemInfo.deviceModel;
      string deviceName = SystemInfo.deviceName;
      DeviceType deviceType = SystemInfo.deviceType;
      string uniqueIdentifier = SystemInfo.deviceUniqueIdentifier;
      string operatingSystem = SystemInfo.operatingSystem;
      int graphicsDeviceId = SystemInfo.graphicsDeviceID;
      string graphicsDeviceName = SystemInfo.graphicsDeviceName;
      string graphicsDeviceVersion = SystemInfo.graphicsDeviceVersion;
      int graphicsMemorySize = SystemInfo.graphicsMemorySize;
      int graphicsPixelFillrate = SystemInfo.graphicsPixelFillrate;
      int graphicsShaderLevel = SystemInfo.graphicsShaderLevel;
      stringBuilder.AppendLine(string.Format("OS: {0}", (object) operatingSystem));
      stringBuilder.AppendLine(string.Format("Model: {0}", (object) deviceModel));
      stringBuilder.AppendLine(string.Format("Name: {0}", (object) deviceName));
      stringBuilder.AppendLine(string.Format("Type: {0}", (object) deviceType));
      stringBuilder.AppendLine(string.Format("UID: {0}", (object) uniqueIdentifier));
      stringBuilder.AppendLine(string.Format("GPU Name: {0}", (object) graphicsDeviceName));
      stringBuilder.AppendLine(string.Format("GPU ID: {0}", (object) graphicsDeviceId));
      stringBuilder.AppendLine(string.Format("GPU Version: {0}", (object) graphicsDeviceVersion));
      stringBuilder.AppendLine(string.Format("GPU Memory: {0}", (object) graphicsMemorySize));
      stringBuilder.AppendLine(string.Format("GPU Fill: {0}", (object) graphicsPixelFillrate));
      stringBuilder.AppendLine(string.Format("GPU Shader Lvl: {0}", (object) graphicsShaderLevel));
      try
      {
        foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
        {
          NetworkInterfaceType networkInterfaceType = networkInterface.NetworkInterfaceType;
          stringBuilder.AppendLine(string.Format("NIC Type: {0}", (object) networkInterfaceType));
        }
      }
      catch (Exception ex)
      {
        stringBuilder.AppendLine(string.Format("Error getting NIC info: {0}", (object) ex.Message));
      }
      this.target.text = stringBuilder.ToString();
    }
  }
}
