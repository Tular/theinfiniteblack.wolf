﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockWeaponItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Reflection;
using TheInfiniteBlack.Library.Items;

namespace TheInfiniteBlack.Unity
{
  public class MockWeaponItem : WeaponItem
  {
    private static readonly FieldInfo _bindOnEquip;

    protected internal MockWeaponItem(WeaponClass weaponClass, sbyte baseEp, ItemRarity rarity, sbyte durability, bool noDrop, bool bindOnEquip)
      : base(weaponClass, baseEp, rarity, durability, noDrop, bindOnEquip)
    {
    }
  }
}
