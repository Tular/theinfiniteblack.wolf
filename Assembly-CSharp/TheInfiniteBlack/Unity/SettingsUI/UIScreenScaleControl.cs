﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.UIScreenScaleControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class UIScreenScaleControl : DialogBase
  {
    public UISlider uiMultiplierSlider;
    public UILabel uiMultiplierSliderThumbLabel;
    public UILabel dialogTitleLabel;
    public float minMultiplier;
    public float maxMultiplier;
    public float currentMultiplier;
    public bool useMultiplierInverse;

    public event EventHandler<ScaleChangedEventArgs> onScaleChangedEvent;

    public event EventHandler onDialogCloseEvent;

    public string dialogTitle
    {
      set
      {
        this.dialogTitleLabel.text = value;
      }
    }

    public float value
    {
      get
      {
        return this.uiMultiplierSlider.value;
      }
    }

    public void OnSliderChange()
    {
      if (!((UnityEngine.Object) UIProgressBar.current != (UnityEngine.Object) null))
        return;
      this.currentMultiplier = (float) (1.0 + ((double) this.maxMultiplier - (double) this.minMultiplier) * (double) UIProgressBar.current.value);
      this.uiMultiplierSliderThumbLabel.text = string.Format("{0:n2}x", (object) (float) (!this.useMultiplierInverse ? (double) this.currentMultiplier : 1.0 / (double) this.currentMultiplier));
      this.OnOnScaleChanged(new ScaleChangedEventArgs(this.currentMultiplier));
    }

    public void ResetToDefault()
    {
      this.uiMultiplierSlider.value = 0.0f;
    }

    protected override void OnShow()
    {
      this.uiMultiplierSlider.value = (float) (((double) this.currentMultiplier - 1.0) / ((double) this.maxMultiplier - (double) this.minMultiplier));
      this.uiMultiplierSlider.ForceUpdate();
    }

    protected override void OnHide()
    {
      this.OnDialogClose();
      this.dialogTitleLabel.text = string.Empty;
    }

    protected override void OnRegisterEventHandlers()
    {
    }

    protected override void OnUnRegisterEventHandlers()
    {
    }

    protected virtual void OnOnScaleChanged(ScaleChangedEventArgs e)
    {
      EventHandler<ScaleChangedEventArgs> scaleChangedEvent = this.onScaleChangedEvent;
      if (scaleChangedEvent == null)
        return;
      scaleChangedEvent((object) this, e);
    }

    protected virtual void OnDialogClose()
    {
      EventHandler dialogCloseEvent = this.onDialogCloseEvent;
      if (dialogCloseEvent != null)
        dialogCloseEvent((object) this, EventArgs.Empty);
      this.onScaleChangedEvent = (EventHandler<ScaleChangedEventArgs>) null;
      this.onDialogCloseEvent = (EventHandler) null;
      this.ResetToDefault();
    }
  }
}
