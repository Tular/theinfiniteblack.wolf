﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.EntityListScaler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class EntityListScaler : AdjustableScale
  {
    public event EventHandler onUpdateScale;

    public override float initialValue
    {
      get
      {
        return TibProxy.mySettings.EntityList.Scale;
      }
    }

    public override void UpdateScale(float scaleMultiplier)
    {
      base.UpdateScale(scaleMultiplier);
      TibProxy.mySettings.EntityList.Scale = scaleMultiplier;
      if (this.onUpdateScale == null)
        return;
      this.onUpdateScale((object) this, new EventArgs());
    }

    protected override void OnInitialize()
    {
      if (TibProxy.mySettings == null)
        return;
      base.OnInitialize();
    }

    protected override void OnDialogClose()
    {
      if (TibProxy.accountManager == null)
        return;
      TibProxy.accountManager.SaveAll();
    }
  }
}
