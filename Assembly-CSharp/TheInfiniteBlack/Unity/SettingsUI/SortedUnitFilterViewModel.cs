﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.SortedUnitFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class SortedUnitFilterViewModel : DataBindingBase<SortedUnitFilter>
  {
    public SortedToggleFilterViewModel shipSelf;
    public SortedToggleFilterViewModel shipFriend;
    public SortedToggleFilterViewModel shipEnemy;
    public SortedToggleFilterViewModel shipNeutral;
    public SortedToggleFilterViewModel fighterSelf;
    public SortedToggleFilterViewModel fighterFriend;
    public SortedToggleFilterViewModel fighterEnemy;
    public SortedToggleFilterViewModel fighterNeutral;
    public SortedToggleFilterViewModel repairDroneSelf;
    public SortedToggleFilterViewModel repairDroneFriend;
    public SortedToggleFilterViewModel repairDroneEnemy;
    public SortedToggleFilterViewModel repairDroneNeutral;
    public SortedToggleFilterViewModel planet;
    public SortedToggleFilterViewModel garrison;
    public SortedToggleFilterViewModel intradictor;
    public SortedToggleFilterViewModel defensePlatform;
    public SortedToggleFilterViewModel mines;
    public SortedToggleFilterViewModel npc;
    public SortedToggleFilterViewModel starPort;
    public SortedToggleFilterViewModel capturePoint;
    public SortedToggleFilterViewModel cargo;
    public SortedToggleFilterViewModel asteroid;

    protected override void BindData()
    {
      this.shipSelf.Bind(this.mData.ShipSelf);
      this.shipFriend.Bind(this.mData.ShipFriend);
      this.shipEnemy.Bind(this.mData.ShipEnemy);
      this.shipNeutral.Bind(this.mData.ShipNeutral);
      this.fighterSelf.Bind(this.mData.FighterSelf);
      this.fighterFriend.Bind(this.mData.FighterFriend);
      this.fighterEnemy.Bind(this.mData.FighterEnemy);
      this.fighterNeutral.Bind(this.mData.FighterNeutral);
      this.repairDroneSelf.Bind(this.mData.RepairDroneSelf);
      this.repairDroneFriend.Bind(this.mData.RepairDroneFriend);
      this.repairDroneEnemy.Bind(this.mData.RepairDroneEnemy);
      this.repairDroneNeutral.Bind(this.mData.RepairDroneNeutral);
      this.planet.Bind(this.mData.Planet);
      this.garrison.Bind(this.mData.Garrison);
      this.intradictor.Bind(this.mData.Intradictor);
      this.defensePlatform.Bind(this.mData.DefensePlatform);
      this.mines.Bind(this.mData.Mines);
      this.npc.Bind(this.mData.NPC);
      this.starPort.Bind(this.mData.StarPort);
      this.capturePoint.Bind(this.mData.CapturePoint);
      this.cargo.Bind(this.mData.Cargo);
      this.asteroid.Bind(this.mData.Asteroid);
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
      this.shipSelf.UnBind();
      this.shipFriend.UnBind();
      this.shipEnemy.UnBind();
      this.shipNeutral.UnBind();
      this.fighterSelf.UnBind();
      this.fighterFriend.UnBind();
      this.fighterEnemy.UnBind();
      this.fighterNeutral.UnBind();
      this.repairDroneSelf.UnBind();
      this.repairDroneFriend.UnBind();
      this.repairDroneEnemy.UnBind();
      this.repairDroneNeutral.UnBind();
      this.planet.UnBind();
      this.garrison.UnBind();
      this.intradictor.UnBind();
      this.defensePlatform.UnBind();
      this.mines.UnBind();
      this.npc.UnBind();
      this.starPort.UnBind();
      this.capturePoint.UnBind();
      this.cargo.UnBind();
      this.asteroid.UnBind();
    }

    protected override void AddDelegates()
    {
    }

    protected override void RemoveDelegates()
    {
    }
  }
}
