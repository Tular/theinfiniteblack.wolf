﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.ListMenuOptionEventLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class ListMenuOptionEventLog : MonoBehaviour
  {
    public SettingsWindow window;
    public GameEventsFilterViewModel dataContext;
    public GameObject attackFiltersRoot;
    public GameObject chatFiltersRoot;
    public GameObject miscFiltersRoot;
    public GameObject unitFiltersRoot;
    private GameObject _go;

    public void Show()
    {
      if ((Object) this._go == (Object) null)
        this._go = this.gameObject;
      if ((Object) this.window == (Object) null)
        this.window = NGUITools.FindInParents<SettingsWindow>(this._go);
      this.dataContext.Bind(this.window.accountSettings.EventLog);
      NGUITools.SetActiveSelf(this._go, true);
    }

    public void Hide()
    {
      this.ResetContent();
      NGUITools.SetActiveSelf(this._go, false);
    }

    public void ResetContent()
    {
      NGUITools.SetActiveSelf(this.attackFiltersRoot, false);
      NGUITools.SetActiveSelf(this.chatFiltersRoot, false);
      NGUITools.SetActiveSelf(this.miscFiltersRoot, false);
      NGUITools.SetActiveSelf(this.unitFiltersRoot, false);
    }
  }
}
