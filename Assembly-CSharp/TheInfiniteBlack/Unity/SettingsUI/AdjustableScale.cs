﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.AdjustableScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public abstract class AdjustableScale : MonoBehaviour
  {
    private static readonly Dictionary<string, AdjustableScale> _list = new Dictionary<string, AdjustableScale>();
    public Vector3 defaultScale = Vector3.one;
    public string dialogTitle;
    public string alias;
    public float min;
    public float max;
    public bool useMultiplierInverse;
    protected bool mIsInitialized;
    private Transform _trans;

    public abstract float initialValue { get; }

    public static AdjustableScale GetScaler(string scalerAlias)
    {
      if (AdjustableScale._list.ContainsKey(scalerAlias))
        return AdjustableScale._list[scalerAlias];
      return (AdjustableScale) null;
    }

    public static void OpenScaler(string scalerAlias)
    {
      if (!AdjustableScale._list.ContainsKey(scalerAlias))
        return;
      UIScreenScaleControl dialog = DialogManager.GetDialog<UIScreenScaleControl>();
      if ((UnityEngine.Object) dialog == (UnityEngine.Object) null)
        return;
      AdjustableScale adjustableScale = AdjustableScale._list[scalerAlias];
      dialog.onScaleChangedEvent -= new EventHandler<ScaleChangedEventArgs>(adjustableScale.UIScreenScaleControl_onScaleChangedEvent);
      dialog.onScaleChangedEvent += new EventHandler<ScaleChangedEventArgs>(adjustableScale.UIScreenScaleControl_onScaleChangedEvent);
      dialog.onDialogCloseEvent -= new EventHandler(adjustableScale.UIScreenScaleControl_onDialogCloseEvent);
      dialog.onDialogCloseEvent += new EventHandler(adjustableScale.UIScreenScaleControl_onDialogCloseEvent);
      dialog.dialogTitle = AdjustableScale._list[scalerAlias].dialogTitle;
      dialog.useMultiplierInverse = AdjustableScale._list[scalerAlias].useMultiplierInverse;
      dialog.minMultiplier = AdjustableScale._list[scalerAlias].min;
      dialog.maxMultiplier = AdjustableScale._list[scalerAlias].max;
      dialog.currentMultiplier = AdjustableScale._list[scalerAlias].initialValue;
      DialogManager.ShowDialog<UIScreenScaleControl>(false);
    }

    private void UIScreenScaleControl_onDialogCloseEvent(object sender, EventArgs e)
    {
      UIScreenScaleControl screenScaleControl = sender as UIScreenScaleControl;
      if ((UnityEngine.Object) screenScaleControl == (UnityEngine.Object) null)
        return;
      screenScaleControl.onScaleChangedEvent -= new EventHandler<ScaleChangedEventArgs>(this.UIScreenScaleControl_onScaleChangedEvent);
      screenScaleControl.onDialogCloseEvent -= new EventHandler(this.UIScreenScaleControl_onDialogCloseEvent);
      this.OnDialogClose();
    }

    private void UIScreenScaleControl_onScaleChangedEvent(object sender, ScaleChangedEventArgs e)
    {
      this.UpdateScale(e.scaleMultiplier);
    }

    public virtual void UpdateScale(float scaleMultiplier)
    {
      this._trans.localScale = (!this.useMultiplierInverse ? scaleMultiplier : 1f / scaleMultiplier) * this.defaultScale;
    }

    protected virtual void OnDialogClose()
    {
    }

    protected void Initialize()
    {
      this.OnInitialize();
      if (!this.mIsInitialized)
        ;
    }

    protected virtual void OnInitialize()
    {
      this.UpdateScale(this.initialValue);
      this.mIsInitialized = true;
    }

    private void Start()
    {
      if (TibProxy.accountManager == null)
        Debug.Log((object) string.Format("AdjustableScale.Start(): TibProxy.accountManager is null"));
      else
        this.Initialize();
    }

    private void OnEnable()
    {
      if (string.IsNullOrEmpty(this.alias))
        return;
      AdjustableScale._list.Add(this.alias, this);
      this._trans = this.transform;
    }

    private void OnDisable()
    {
      if (!AdjustableScale._list.ContainsKey(this.alias))
        return;
      AdjustableScale._list.Remove(this.alias);
    }

    private void Update()
    {
      if (this.mIsInitialized)
        return;
      this.Initialize();
    }
  }
}
