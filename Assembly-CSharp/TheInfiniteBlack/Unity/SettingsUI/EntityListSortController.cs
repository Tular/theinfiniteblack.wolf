﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.EntityListSortController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Client.Settings;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class EntityListSortController : MonoBehaviour
  {
    public UIGrid listRoot;
    public EntityListSortCell cellPrefab;
    private EntityListSortCell _selectedCell;
    public EntityListSortCell shipSelf;
    public EntityListSortCell shipFriend;
    public EntityListSortCell shipEnemy;
    public EntityListSortCell shipNeutral;
    public EntityListSortCell fighterSelf;
    public EntityListSortCell fighterFriend;
    public EntityListSortCell fighterEnemy;
    public EntityListSortCell fighterNeutral;
    public EntityListSortCell repairDroneSelf;
    public EntityListSortCell repairDroneFriend;
    public EntityListSortCell repairDroneEnemy;
    public EntityListSortCell repairDroneNeutral;
    public EntityListSortCell asteroid;
    public EntityListSortCell capturePoint;
    public EntityListSortCell cargo;
    public EntityListSortCell defensePlatform;
    public EntityListSortCell garrison;
    public EntityListSortCell intradictor;
    public EntityListSortCell mines;
    public EntityListSortCell npc;
    public EntityListSortCell planet;
    public EntityListSortCell starPort;
    private List<EntityListSortCell> _cells;

    public void Bind(SortedUnitFilter filter)
    {
      this.shipSelf.Bind(filter.ShipSelf);
      this.shipFriend.Bind(filter.ShipFriend);
      this.shipEnemy.Bind(filter.ShipEnemy);
      this.shipNeutral.Bind(filter.ShipNeutral);
      this.fighterSelf.Bind(filter.FighterSelf);
      this.fighterFriend.Bind(filter.FighterFriend);
      this.fighterEnemy.Bind(filter.FighterEnemy);
      this.fighterNeutral.Bind(filter.FighterNeutral);
      this.repairDroneSelf.Bind(filter.RepairDroneSelf);
      this.repairDroneFriend.Bind(filter.RepairDroneFriend);
      this.repairDroneEnemy.Bind(filter.RepairDroneEnemy);
      this.repairDroneNeutral.Bind(filter.RepairDroneNeutral);
      this.asteroid.Bind(filter.Asteroid);
      this.capturePoint.Bind(filter.CapturePoint);
      this.cargo.Bind(filter.Cargo);
      this.defensePlatform.Bind(filter.DefensePlatform);
      this.garrison.Bind(filter.Garrison);
      this.intradictor.Bind(filter.Intradictor);
      this.mines.Bind(filter.Mines);
      this.npc.Bind(filter.NPC);
      this.planet.Bind(filter.Planet);
      this.starPort.Bind(filter.StarPort);
      this.listRoot.animateSmoothly = false;
      this.listRoot.repositionNow = true;
    }

    public void UnBind()
    {
      using (List<EntityListSortCell>.Enumerator enumerator = this._cells.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.UnBind();
      }
    }

    public void SetSelected(EntityListSortCell cell)
    {
      if ((UnityEngine.Object) this._selectedCell != (UnityEngine.Object) null)
        this._selectedCell.OnCellDeSelected();
      this._selectedCell = cell;
    }

    public void OnMoveUpClicked()
    {
      if ((UnityEngine.Object) this._selectedCell == (UnityEngine.Object) null || this._selectedCell.sortOrder == 0)
        return;
      int index = this._selectedCell.sortOrder - 1;
      this._cells.OrderBy<EntityListSortCell, int>((Func<EntityListSortCell, int>) (c => c.sortOrder)).ElementAt<EntityListSortCell>(index).sortOrder = this._selectedCell.sortOrder;
      this._selectedCell.sortOrder = index;
      this.listRoot.animateSmoothly = true;
      this.listRoot.repositionNow = true;
    }

    public void OnMoveDownClicked()
    {
      if ((UnityEngine.Object) this._selectedCell == (UnityEngine.Object) null || this._selectedCell.sortOrder == this._cells.Count - 1)
        return;
      int index = this._selectedCell.sortOrder + 1;
      this._cells.OrderBy<EntityListSortCell, int>((Func<EntityListSortCell, int>) (c => c.sortOrder)).ElementAt<EntityListSortCell>(index).sortOrder = this._selectedCell.sortOrder;
      this._selectedCell.sortOrder = index;
      this.listRoot.animateSmoothly = true;
      this.listRoot.repositionNow = true;
    }

    private void Awake()
    {
      this.InitializeCellList();
    }

    private void OnEnable()
    {
      this.listRoot.animateSmoothly = false;
      this.listRoot.Reposition();
    }

    private void InitializeCellList()
    {
      this._cells = new List<EntityListSortCell>()
      {
        this.shipSelf,
        this.shipFriend,
        this.shipEnemy,
        this.shipNeutral,
        this.fighterSelf,
        this.fighterFriend,
        this.fighterEnemy,
        this.fighterNeutral,
        this.repairDroneSelf,
        this.repairDroneFriend,
        this.repairDroneEnemy,
        this.repairDroneNeutral,
        this.asteroid,
        this.capturePoint,
        this.cargo,
        this.defensePlatform,
        this.garrison,
        this.intradictor,
        this.mines,
        this.npc,
        this.planet,
        this.starPort
      };
    }
  }
}
