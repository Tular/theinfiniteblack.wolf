﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.SettingsUIToggleMenuGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class SettingsUIToggleMenuGroup : MonoBehaviour
  {
    public List<SettingsUIToggleMenuOption> groupOptions;
    public UITweener optionsTween;
    [HideInInspector]
    public List<EventDelegate> onSelect;
    [HideInInspector]
    public List<EventDelegate> onDeSelect;
    public bool playTween;
    public bool isActive;
    private SettingsUIToggleMenuOption _selectedOption;

    public void OnToggleChanged()
    {
      if (UIToggle.current.value)
        this.Activate();
      else
        this.DeActivate();
      this.isActive = UIToggle.current.value;
    }

    public void Activate()
    {
      if (EventDelegate.IsValid(this.onSelect))
        EventDelegate.Execute(this.onSelect);
      if (!(bool) ((Object) this.optionsTween) || !this.playTween)
        return;
      NGUITools.SetActiveSelf(this.optionsTween.gameObject, true);
      this.optionsTween.PlayForward();
    }

    public void DeActivate()
    {
      if (!this.isActive)
        return;
      if (EventDelegate.IsValid(this.onDeSelect))
        EventDelegate.Execute(this.onDeSelect);
      using (List<SettingsUIToggleMenuOption>.Enumerator enumerator = this.groupOptions.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          SettingsUIToggleMenuOption current = enumerator.Current;
          Debug.Log((object) string.Format("SettingsUIToggleMenuGroup.DeActivate() {0}", (object) current.name));
          current.DeSelect();
        }
      }
      this._selectedOption = (SettingsUIToggleMenuOption) null;
      if (!this.playTween)
        return;
      EventDelegate.Add(this.optionsTween.onFinished, (EventDelegate.Callback) (() => NGUITools.SetActiveSelf(this.optionsTween.gameObject, false)), true);
      this.optionsTween.PlayReverse();
    }

    public SettingsUIToggleMenuOption selectedOption
    {
      get
      {
        return this._selectedOption;
      }
      set
      {
        if ((Object) this._selectedOption == (Object) null)
        {
          this._selectedOption = value;
          this._selectedOption.Select();
        }
        else
        {
          Debug.Log((object) string.Format("{0} setting selected to {1}", (object) this.name, (object) this.selectedOption.name));
          if (this._selectedOption.Equals((object) value))
            return;
          Debug.Log((object) string.Format("SettingsUIToggleMenuGroup.selectedOption"));
          this._selectedOption.DeSelect();
          this._selectedOption = value;
          this._selectedOption.Select();
        }
      }
    }

    protected void OnEnable()
    {
      if (!((Object) this.optionsTween != (Object) null))
        return;
      this.optionsTween.transform.localScale = new Vector3(0.0f, 1f, 0.0f);
      NGUITools.SetActiveSelf(this.optionsTween.gameObject, false);
    }

    protected void OnDisable()
    {
      using (List<SettingsUIToggleMenuOption>.Enumerator enumerator = this.groupOptions.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.DeSelect();
      }
      this._selectedOption = (SettingsUIToggleMenuOption) null;
      if ((Object) this.optionsTween != (Object) null)
      {
        this.optionsTween.ResetToBeginning();
        NGUITools.SetActiveSelf(this.optionsTween.gameObject, false);
      }
      this.isActive = false;
    }
  }
}
