﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.SettingsUIToggleMenuOption
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class SettingsUIToggleMenuOption : MonoBehaviour
  {
    public SettingsUIToggleMenuGroup parentGroup;
    public List<GameObject> activate;
    [HideInInspector]
    public List<EventDelegate> onSelect;
    [HideInInspector]
    public List<EventDelegate> onDeSelect;
    public bool isSelected;

    public void Select()
    {
      this.isSelected = true;
      if (EventDelegate.IsValid(this.onSelect))
        EventDelegate.Execute(this.onSelect);
      using (List<GameObject>.Enumerator enumerator = this.activate.GetEnumerator())
      {
        while (enumerator.MoveNext())
          NGUITools.SetActiveSelf(enumerator.Current, true);
      }
    }

    public void DeSelect()
    {
      this.isSelected = false;
      if (EventDelegate.IsValid(this.onDeSelect))
        EventDelegate.Execute(this.onDeSelect);
      using (List<GameObject>.Enumerator enumerator = this.activate.GetEnumerator())
      {
        while (enumerator.MoveNext())
          NGUITools.SetActiveSelf(enumerator.Current, false);
      }
    }

    public void OnClick()
    {
      if (this.isSelected)
        return;
      Debug.Log((object) string.Format("SettingsUIToggleMenuOption.OnClick() - {0}", (object) this.name));
      if (!((Object) this.parentGroup != (Object) null))
        return;
      this.parentGroup.selectedOption = this;
    }

    protected void Awake()
    {
      if ((Object) this.parentGroup == (Object) null)
        this.parentGroup = NGUITools.FindInParents<SettingsUIToggleMenuGroup>(this.gameObject);
      if (!((Object) this.parentGroup != (Object) null))
        return;
      this.parentGroup.groupOptions.Add(this);
    }

    protected void OnEnable()
    {
    }

    protected void Start()
    {
    }

    protected void OnDisable()
    {
      this.DeSelect();
    }
  }
}
