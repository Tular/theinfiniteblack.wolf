﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.UnitFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class UnitFilterViewModel : DataBindingBase<UnitFilter>
  {
    public RelationshipFilterViewModel ship;
    public RelationshipFilterViewModel fighter;
    public RelationshipFilterViewModel repairDrone;
    public RelationshipFilterViewModel planet;
    public RelationshipFilterViewModel garrison;
    public RelationshipFilterViewModel intradictor;
    public RelationshipFilterViewModel defensePlatform;
    public RelationshipFilterViewModel mines;
    public UISwitchControl npcSwitch;
    public UISwitchControl starPortSwitch;
    public UISwitchControl capturePointSwitch;
    public UISwitchControl cargoSwitch;
    public UISwitchControl asteroidSwitch;
    private EventDelegate.Callback _npcCallback;
    private EventDelegate.Callback _starPortCallback;
    private EventDelegate.Callback _capturePointCallback;
    private EventDelegate.Callback _cargoCallback;
    private EventDelegate.Callback _asteroidCallback;

    protected override void BindData()
    {
      this.ship.Bind(this.mData.Ship);
      this.fighter.Bind(this.mData.Fighter);
      this.repairDrone.Bind(this.mData.RepairDrone);
      this.planet.Bind(this.mData.Planet);
      this.garrison.Bind(this.mData.Garrison);
      this.intradictor.Bind(this.mData.Intradictor);
      this.defensePlatform.Bind(this.mData.DefensePlatform);
      this.mines.Bind(this.mData.Mines);
      this.npcSwitch.value = this.mData.NPC;
      this.starPortSwitch.value = this.mData.StarPort;
      this.capturePointSwitch.value = this.mData.CapturePoint;
      this.cargoSwitch.value = this.mData.Cargo;
      this.asteroidSwitch.value = this.mData.Asteroid;
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
      this.ship.UnBind();
      this.fighter.UnBind();
      this.repairDrone.UnBind();
      this.planet.UnBind();
      this.garrison.UnBind();
      this.intradictor.UnBind();
      this.defensePlatform.UnBind();
      this.mines.UnBind();
    }

    protected override void AddDelegates()
    {
      this._npcCallback = (EventDelegate.Callback) (() => this.mData.NPC = this.npcSwitch.value);
      this._starPortCallback = (EventDelegate.Callback) (() => this.mData.StarPort = this.starPortSwitch.value);
      this._capturePointCallback = (EventDelegate.Callback) (() => this.mData.CapturePoint = this.capturePointSwitch.value);
      this._cargoCallback = (EventDelegate.Callback) (() => this.mData.Cargo = this.capturePointSwitch.value);
      this._asteroidCallback = (EventDelegate.Callback) (() => this.mData.Asteroid = this.asteroidSwitch.value);
      EventDelegate.Add(this.npcSwitch.onChange, this._npcCallback);
      EventDelegate.Add(this.starPortSwitch.onChange, this._starPortCallback);
      EventDelegate.Add(this.capturePointSwitch.onChange, this._capturePointCallback);
      EventDelegate.Add(this.cargoSwitch.onChange, this._cargoCallback);
      EventDelegate.Add(this.asteroidSwitch.onChange, this._asteroidCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._npcCallback != null)
        EventDelegate.Remove(this.npcSwitch.onChange, this._npcCallback);
      if (this._starPortCallback != null)
        EventDelegate.Remove(this.starPortSwitch.onChange, this._starPortCallback);
      if (this._capturePointCallback != null)
        EventDelegate.Remove(this.capturePointSwitch.onChange, this._capturePointCallback);
      if (this._cargoCallback != null)
        EventDelegate.Remove(this.cargoSwitch.onChange, this._cargoCallback);
      if (this._asteroidCallback == null)
        return;
      EventDelegate.Remove(this.asteroidSwitch.onChange, this._asteroidCallback);
    }
  }
}
