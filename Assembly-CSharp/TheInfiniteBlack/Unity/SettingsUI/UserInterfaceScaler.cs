﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.UserInterfaceScaler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class UserInterfaceScaler : AdjustableScale
  {
    private static readonly List<Transform> _roots = new List<Transform>(10);

    public static void Add(Transform trans)
    {
      if (UserInterfaceScaler._roots.Contains(trans))
        return;
      UserInterfaceScaler._roots.Add(trans);
    }

    public static void Remove(Transform trans)
    {
      UserInterfaceScaler._roots.Remove(trans);
    }

    public override float initialValue
    {
      get
      {
        return TibProxy.accountManager.systemSettings.UiScale;
      }
    }

    public static Vector3 value { get; private set; }

    public override void UpdateScale(float scaleMultiplier)
    {
      TibProxy.accountManager.systemSettings.UiScale = scaleMultiplier;
      UserInterfaceScaler.value = (!this.useMultiplierInverse ? scaleMultiplier : 1f / scaleMultiplier) * this.defaultScale;
      for (int index = 0; index < UserInterfaceScaler._roots.Count; ++index)
        UserInterfaceScaler._roots[index].localScale = UserInterfaceScaler.value;
    }

    protected override void OnDialogClose()
    {
      if (TibProxy.accountManager == null)
        return;
      TibProxy.accountManager.SaveAll();
    }
  }
}
