﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.SettingsWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  [AdvancedInspector.AdvancedInspector]
  public class SettingsWindow : DialogBase
  {
    [Inspect(0)]
    public bool debugMode;
    [Inspect(1)]
    public string entityListScalerAlias;
    [Inspect(2)]
    public string uiScalerAlias;
    [Inspect(3)]
    public UIToggle defaultTab;

    public AccountSettings accountSettings
    {
      get
      {
        if (TibProxy.mySettings == null)
          throw new NullReferenceException("Trying to show SettingsDialog when TibProxy.accountManager is null.");
        return TibProxy.mySettings;
      }
    }

    public SystemSettings systemSettings
    {
      get
      {
        return TibProxy.accountManager.systemSettings;
      }
    }

    protected override void OnRegisterEventHandlers()
    {
    }

    protected override void OnUnRegisterEventHandlers()
    {
    }

    public void SaveSettings()
    {
      if (this.debugMode || TibProxy.accountManager == null)
        return;
      TibProxy.accountManager.SaveAll();
    }

    public void ResetAllSettings()
    {
      DialogManager.ShowDialog<AcceptDeclineDialog>((object) "Reset ALL settings back to default values?", (object) new EventDelegate.Callback(TibProxy.mySettings.Default), null);
    }

    public void LogIntoDevServer()
    {
      DialogManager.ShowDialog<AcceptDeclineDialog>((object) "You will be logged out!\nYour next login will join the test server.\nTest accounts are not synced with the live server.", (object) (EventDelegate.Callback) (() =>
      {
        TibProxy.Instance.SetUseDevServer(true);
        TibProxy.Instance.Disconnect("DevServer Login", true);
      }), null);
    }

    public void ShowUiScaleDialog()
    {
      this.SaveSettings();
      this.Hide();
      DialogManager.ShowDialog<UnifiedScaleDialog>(false);
    }

    public void OpenScreenSizeDialog()
    {
      this.SaveSettings();
      this.Hide();
      DialogManager.ShowDialog<UnifiedScaleDialog>(false);
    }

    [Inspect(100)]
    private void DoShow()
    {
      this.debugMode = true;
      this.defaultTab.value = true;
      DialogManager.ShowingDialog((DialogBase) this, true);
    }

    [Inspect(101)]
    private void DoHide()
    {
      this.defaultTab.value = true;
      DialogManager.ClosingDialog((DialogBase) this);
    }

    protected override void OnHide()
    {
      this.defaultTab.value = true;
      if (this.debugMode)
        return;
      this.SaveSettings();
    }

    protected override void OnShow()
    {
      this.GetComponent<UIToggle>().value = true;
    }
  }
}
