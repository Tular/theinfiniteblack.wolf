﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.MiscFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class MiscFilterViewModel : DataBindingBase<MiscFilter>
  {
    public RelationshipFilterViewModel arrivalsFilter;
    public RelationshipFilterViewModel departuresFilter;
    public RelationshipFilterViewModel gainsFilter;
    public RelationshipFilterViewModel nonCombatFilter;
    public UISwitchControl allAuctionsSwitch;
    public UISwitchControl attackStatusSwitch;
    public UISwitchControl courseStatusSwitch;
    public UISwitchControl followStatusSwitch;
    public UISwitchControl friendsOnlineStatusSwitch;
    public UISwitchControl locationStatusSwitch;
    public UISwitchControl movementStatusSwitch;
    public UISwitchControl myAuctionsSwitch;
    public UISwitchControl pvpStatusSwitch;
    public UISwitchControl debugSwitch;
    private EventDelegate.Callback _friendsOnlineStatusCallback;
    private EventDelegate.Callback _courseStatusCallback;
    private EventDelegate.Callback _movementStatusCallback;
    private EventDelegate.Callback _locationStatusCallback;
    private EventDelegate.Callback _followStatusCallback;
    private EventDelegate.Callback _attackStatusCallback;
    private EventDelegate.Callback _pvpStatusCallback;
    private EventDelegate.Callback _myAuctionsCallback;
    private EventDelegate.Callback _allAuctionsCallback;
    private EventDelegate.Callback _debugCallback;

    protected override void BindData()
    {
      this.arrivalsFilter.Bind(this.mData.Arrivals);
      this.departuresFilter.Bind(this.mData.Departures);
      this.gainsFilter.Bind(this.mData.Gains);
      this.nonCombatFilter.Bind(this.mData.NonCombat);
      this.friendsOnlineStatusSwitch.Set(this.mData.FriendOnlineStatus);
      this.courseStatusSwitch.Set(this.mData.CourseStatus);
      this.movementStatusSwitch.Set(this.mData.MovementStatus);
      this.locationStatusSwitch.Set(this.mData.LocationStatus);
      this.followStatusSwitch.Set(this.mData.FollowStatus);
      this.attackStatusSwitch.Set(this.mData.AttackStatus);
      this.pvpStatusSwitch.Set(this.mData.PvPStatus);
      this.myAuctionsSwitch.Set(this.mData.MyAuctions);
      this.allAuctionsSwitch.Set(this.mData.AllAuctions);
      this.debugSwitch.Set(this.mData.Debug);
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
      this.arrivalsFilter.UnBind();
      this.departuresFilter.UnBind();
      this.gainsFilter.UnBind();
      this.nonCombatFilter.UnBind();
    }

    protected override void AddDelegates()
    {
      this._friendsOnlineStatusCallback = (EventDelegate.Callback) (() => this.mData.FriendOnlineStatus = this.friendsOnlineStatusSwitch.value);
      this._courseStatusCallback = (EventDelegate.Callback) (() => this.mData.CourseStatus = this.courseStatusSwitch.value);
      this._movementStatusCallback = (EventDelegate.Callback) (() => this.mData.MovementStatus = this.movementStatusSwitch.value);
      this._locationStatusCallback = (EventDelegate.Callback) (() => this.mData.LocationStatus = this.locationStatusSwitch.value);
      this._followStatusCallback = (EventDelegate.Callback) (() => this.mData.FollowStatus = this.followStatusSwitch.value);
      this._attackStatusCallback = (EventDelegate.Callback) (() => this.mData.AttackStatus = this.attackStatusSwitch.value);
      this._pvpStatusCallback = (EventDelegate.Callback) (() => this.mData.PvPStatus = this.pvpStatusSwitch.value);
      this._myAuctionsCallback = (EventDelegate.Callback) (() => this.mData.MyAuctions = this.myAuctionsSwitch.value);
      this._allAuctionsCallback = (EventDelegate.Callback) (() => this.mData.AllAuctions = this.allAuctionsSwitch.value);
      this._debugCallback = (EventDelegate.Callback) (() => this.mData.Debug = this.debugSwitch.value);
      EventDelegate.Add(this.friendsOnlineStatusSwitch.onChange, this._friendsOnlineStatusCallback);
      EventDelegate.Add(this.courseStatusSwitch.onChange, this._courseStatusCallback);
      EventDelegate.Add(this.movementStatusSwitch.onChange, this._movementStatusCallback);
      EventDelegate.Add(this.locationStatusSwitch.onChange, this._locationStatusCallback);
      EventDelegate.Add(this.followStatusSwitch.onChange, this._followStatusCallback);
      EventDelegate.Add(this.attackStatusSwitch.onChange, this._attackStatusCallback);
      EventDelegate.Add(this.pvpStatusSwitch.onChange, this._pvpStatusCallback);
      EventDelegate.Add(this.myAuctionsSwitch.onChange, this._myAuctionsCallback);
      EventDelegate.Add(this.allAuctionsSwitch.onChange, this._allAuctionsCallback);
      EventDelegate.Add(this.debugSwitch.onChange, this._debugCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._friendsOnlineStatusCallback != null)
        EventDelegate.Remove(this.friendsOnlineStatusSwitch.onChange, this._friendsOnlineStatusCallback);
      if (this._courseStatusCallback != null)
        EventDelegate.Remove(this.courseStatusSwitch.onChange, this._courseStatusCallback);
      if (this._movementStatusCallback != null)
        EventDelegate.Remove(this.movementStatusSwitch.onChange, this._movementStatusCallback);
      if (this._locationStatusCallback != null)
        EventDelegate.Remove(this.locationStatusSwitch.onChange, this._locationStatusCallback);
      if (this._followStatusCallback != null)
        EventDelegate.Remove(this.followStatusSwitch.onChange, this._followStatusCallback);
      if (this._attackStatusCallback != null)
        EventDelegate.Remove(this.attackStatusSwitch.onChange, this._attackStatusCallback);
      if (this._pvpStatusCallback != null)
        EventDelegate.Remove(this.pvpStatusSwitch.onChange, this._pvpStatusCallback);
      if (this._myAuctionsCallback != null)
        EventDelegate.Remove(this.myAuctionsSwitch.onChange, this._myAuctionsCallback);
      if (this._allAuctionsCallback != null)
        EventDelegate.Remove(this.allAuctionsSwitch.onChange, this._allAuctionsCallback);
      if (this._debugCallback != null)
        EventDelegate.Remove(this.debugSwitch.onChange, this._debugCallback);
      this._friendsOnlineStatusCallback = (EventDelegate.Callback) null;
      this._courseStatusCallback = (EventDelegate.Callback) null;
      this._movementStatusCallback = (EventDelegate.Callback) null;
      this._locationStatusCallback = (EventDelegate.Callback) null;
      this._followStatusCallback = (EventDelegate.Callback) null;
      this._attackStatusCallback = (EventDelegate.Callback) null;
      this._pvpStatusCallback = (EventDelegate.Callback) null;
      this._myAuctionsCallback = (EventDelegate.Callback) null;
      this._allAuctionsCallback = (EventDelegate.Callback) null;
      this._debugCallback = (EventDelegate.Callback) null;
    }
  }
}
