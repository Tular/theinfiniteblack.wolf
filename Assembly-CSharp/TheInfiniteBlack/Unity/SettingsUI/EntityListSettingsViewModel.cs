﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.EntityListSettingsViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class EntityListSettingsViewModel : DataBindingBase<EntityListSettings>
  {
    public UISwitchControl tapToRepair;
    public UISwitchControl tapToAttack;
    public UISwitchControl hideOnMapScroll;
    public UISwitchControl enableColumnA;
    public UISwitchControl enableColumnB;
    public UISwitchControl enableColumnC;
    public UISwitchControl enableColumnD;
    private EventDelegate.Callback _enableColumnACallback;
    private EventDelegate.Callback _enableColumnBCallback;
    private EventDelegate.Callback _enableColumnCCallback;
    private EventDelegate.Callback _enableColumnDCallback;
    private EventDelegate.Callback _hideOnMapScrollCallback;
    private EventDelegate.Callback _tapToAttackCallback;
    private EventDelegate.Callback _tapToRepairCallback;

    protected override void BindData()
    {
      this.enableColumnA.Set(this.mData.ColumnA.Enabled);
      this.enableColumnB.Set(this.mData.ColumnB.Enabled);
      this.enableColumnC.Set(this.mData.ColumnC.Enabled);
      this.enableColumnD.Set(this.mData.ColumnD.Enabled);
      this.hideOnMapScroll.Set(this.mData.HideOnMapScroll);
      this.tapToAttack.Set(this.mData.TapToAttack);
      this.tapToRepair.Set(this.mData.TapToRepair);
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
    }

    protected override void AddDelegates()
    {
      this._enableColumnACallback = (EventDelegate.Callback) (() => this.mData.ColumnA.Enabled = this.enableColumnA.value);
      this._enableColumnBCallback = (EventDelegate.Callback) (() => this.mData.ColumnB.Enabled = this.enableColumnB.value);
      this._enableColumnCCallback = (EventDelegate.Callback) (() => this.mData.ColumnC.Enabled = this.enableColumnC.value);
      this._enableColumnDCallback = (EventDelegate.Callback) (() => this.mData.ColumnD.Enabled = this.enableColumnD.value);
      this._hideOnMapScrollCallback = (EventDelegate.Callback) (() => this.mData.HideOnMapScroll = this.hideOnMapScroll.value);
      this._tapToAttackCallback = (EventDelegate.Callback) (() => this.mData.TapToAttack = this.tapToAttack.value);
      this._tapToRepairCallback = (EventDelegate.Callback) (() => this.mData.TapToRepair = this.tapToRepair.value);
      EventDelegate.Add(this.enableColumnA.onChange, this._enableColumnACallback);
      EventDelegate.Add(this.enableColumnB.onChange, this._enableColumnBCallback);
      EventDelegate.Add(this.enableColumnC.onChange, this._enableColumnCCallback);
      EventDelegate.Add(this.enableColumnD.onChange, this._enableColumnDCallback);
      EventDelegate.Add(this.hideOnMapScroll.onChange, this._hideOnMapScrollCallback);
      EventDelegate.Add(this.tapToAttack.onChange, this._tapToAttackCallback);
      EventDelegate.Add(this.tapToRepair.onChange, this._tapToRepairCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._enableColumnACallback != null)
        EventDelegate.Remove(this.enableColumnA.onChange, this._enableColumnACallback);
      if (this._enableColumnBCallback != null)
        EventDelegate.Remove(this.enableColumnB.onChange, this._enableColumnBCallback);
      if (this._enableColumnCCallback != null)
        EventDelegate.Remove(this.enableColumnC.onChange, this._enableColumnCCallback);
      if (this._enableColumnDCallback != null)
        EventDelegate.Remove(this.enableColumnD.onChange, this._enableColumnDCallback);
      if (this._hideOnMapScrollCallback != null)
        EventDelegate.Remove(this.hideOnMapScroll.onChange, this._hideOnMapScrollCallback);
      if (this._tapToAttackCallback != null)
        EventDelegate.Remove(this.tapToAttack.onChange, this._tapToAttackCallback);
      if (this._tapToRepairCallback != null)
        EventDelegate.Remove(this.tapToRepair.onChange, this._tapToRepairCallback);
      this._enableColumnACallback = (EventDelegate.Callback) null;
      this._enableColumnBCallback = (EventDelegate.Callback) null;
      this._enableColumnCCallback = (EventDelegate.Callback) null;
      this._enableColumnDCallback = (EventDelegate.Callback) null;
      this._hideOnMapScrollCallback = (EventDelegate.Callback) null;
      this._tapToAttackCallback = (EventDelegate.Callback) null;
      this._tapToRepairCallback = (EventDelegate.Callback) null;
    }
  }
}
