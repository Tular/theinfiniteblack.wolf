﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.SortedToggleFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class SortedToggleFilterViewModel : DataBindingBase<SortedToggleFilter>
  {
    public UISwitchControl visibilitySwitch;
    private EventDelegate.Callback _visibilityCallback;

    public virtual int sortOrder
    {
      get
      {
        return this.mData.SortOrder;
      }
      set
      {
        this.mData.SortOrder = value;
      }
    }

    protected override void BindData()
    {
      if (this.mData == null)
        return;
      this.visibilitySwitch.Set(this.mData.Value);
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
    }

    protected override void AddDelegates()
    {
      this._visibilityCallback = (EventDelegate.Callback) (() => this.mData.Value = this.visibilitySwitch.value);
      EventDelegate.Add(this.visibilitySwitch.onChange, this._visibilityCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._visibilityCallback != null)
        EventDelegate.Remove(this.visibilitySwitch.onChange, this._visibilityCallback);
      this._visibilityCallback = (EventDelegate.Callback) null;
    }
  }
}
