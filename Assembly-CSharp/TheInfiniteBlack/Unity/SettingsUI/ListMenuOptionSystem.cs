﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.ListMenuOptionSystem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class ListMenuOptionSystem : MonoBehaviour
  {
    public SettingsWindow window;
    public SystemSettingsViewModel dataContext;

    public void Show()
    {
      if ((Object) this.window == (Object) null)
        this.window = NGUITools.FindInParents<SettingsWindow>(this.gameObject);
      if ((Object) this.dataContext != (Object) null)
        this.dataContext.Bind(this.window.systemSettings, this.window.accountSettings.Social);
      NGUITools.SetActiveSelf(this.gameObject, true);
    }

    public void Hide()
    {
      NGUITools.SetActiveSelf(this.gameObject, false);
      if (!((Object) this.dataContext != (Object) null))
        return;
      this.dataContext.UnBind();
    }
  }
}
