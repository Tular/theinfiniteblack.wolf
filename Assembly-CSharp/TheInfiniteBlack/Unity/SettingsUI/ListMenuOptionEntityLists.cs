﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.ListMenuOptionEntityLists
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class ListMenuOptionEntityLists : MonoBehaviour
  {
    public SettingsWindow window;
    public EntityListSettingsViewModel dataContext;
    public GameObject globalListSettingsRoot;
    public EntityListSortController filterSortController;
    public UIToggle defaultPage;
    private GameObject _go;

    public void Show()
    {
      if ((Object) this._go == (Object) null)
        this._go = this.gameObject;
      if ((Object) this.window == (Object) null)
        this.window = NGUITools.FindInParents<SettingsWindow>(this._go);
      this.ResetContent();
      NGUITools.SetActiveSelf(this._go, true);
      this.defaultPage.value = true;
    }

    public void Hide()
    {
      this.dataContext.UnBind();
      NGUITools.SetActiveSelf(this.globalListSettingsRoot, false);
      NGUITools.SetActiveSelf(this.filterSortController.gameObject, false);
      NGUITools.SetActiveSelf(this._go, false);
      this.defaultPage.optionCanBeNone = true;
      this.defaultPage.value = false;
    }

    public void ShowGlobalEntityListSettings()
    {
      NGUITools.SetActiveSelf(this.filterSortController.gameObject, false);
      NGUITools.SetActiveSelf(this.globalListSettingsRoot, true);
    }

    public void ShowColumnSortA()
    {
      NGUITools.SetActiveSelf(this.globalListSettingsRoot, false);
      NGUITools.SetActiveSelf(this.filterSortController.gameObject, true);
      this.filterSortController.Bind(this.dataContext.data.ColumnA);
    }

    public void ShowColumnSortB()
    {
      NGUITools.SetActiveSelf(this.globalListSettingsRoot, false);
      NGUITools.SetActiveSelf(this.filterSortController.gameObject, true);
      this.filterSortController.Bind(this.dataContext.data.ColumnB);
    }

    public void ShowColumnSortC()
    {
      NGUITools.SetActiveSelf(this.globalListSettingsRoot, false);
      NGUITools.SetActiveSelf(this.filterSortController.gameObject, true);
      this.filterSortController.Bind(this.dataContext.data.ColumnC);
    }

    public void ShowColumnSortD()
    {
      NGUITools.SetActiveSelf(this.globalListSettingsRoot, false);
      NGUITools.SetActiveSelf(this.filterSortController.gameObject, true);
      this.filterSortController.Bind(this.dataContext.data.ColumnD);
    }

    public void ResetContent()
    {
      NGUITools.SetActiveSelf(this.globalListSettingsRoot, false);
      NGUITools.SetActiveSelf(this.filterSortController.gameObject, false);
    }

    private void OnEnable()
    {
      if ((Object) this._go == (Object) null)
        this._go = this.gameObject;
      if ((Object) this.window == (Object) null)
        this.window = NGUITools.FindInParents<SettingsWindow>(this._go);
      this.dataContext.Bind(this.window.accountSettings.EntityList);
    }

    private void OnDisable()
    {
      if (!(bool) ((Object) this.dataContext) || !this.dataContext.isDataBound)
        return;
      this.dataContext.UnBind();
    }
  }
}
