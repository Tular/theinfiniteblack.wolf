﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.RelationshipFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  [RequireComponent(typeof (UIWidget))]
  public class RelationshipFilterViewModel : DataBindingBase<RelationFilter>
  {
    public int contentPadding = 5;
    public UILabel filterName;
    public UITable optionsRoot;
    public UISwitchControl mSwitchPrefab;
    [HideInInspector]
    public UISwitchControl selfSwitch;
    [HideInInspector]
    public UISwitchControl friendSwitch;
    [HideInInspector]
    public UISwitchControl enemySwitch;
    [HideInInspector]
    public UISwitchControl neutralSwitch;
    private EventDelegate.Callback _selfCallback;
    private EventDelegate.Callback _friendCallback;
    private EventDelegate.Callback _enemyCallback;
    private EventDelegate.Callback _neutralCallback;

    protected override void BindData()
    {
      if (this.mData == null)
        return;
      this.selfSwitch.Set(this.mData.Self);
      this.friendSwitch.Set(this.mData.Friend);
      this.enemySwitch.Set(this.mData.Enemy);
      this.neutralSwitch.Set(this.mData.Neutral);
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
    }

    protected override void AddDelegates()
    {
      this._selfCallback = (EventDelegate.Callback) (() => this.mData.Self = this.selfSwitch.value);
      this._friendCallback = (EventDelegate.Callback) (() => this.mData.Friend = this.friendSwitch.value);
      this._enemyCallback = (EventDelegate.Callback) (() => this.mData.Enemy = this.enemySwitch.value);
      this._neutralCallback = (EventDelegate.Callback) (() => this.mData.Neutral = this.neutralSwitch.value);
      EventDelegate.Add(this.selfSwitch.onChange, this._selfCallback);
      EventDelegate.Add(this.friendSwitch.onChange, this._friendCallback);
      EventDelegate.Add(this.enemySwitch.onChange, this._enemyCallback);
      EventDelegate.Add(this.neutralSwitch.onChange, this._neutralCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._selfCallback != null)
        EventDelegate.Remove(this.selfSwitch.onChange, this._selfCallback);
      if (this._friendCallback != null)
        EventDelegate.Remove(this.friendSwitch.onChange, this._friendCallback);
      if (this._enemyCallback != null)
        EventDelegate.Remove(this.enemySwitch.onChange, this._enemyCallback);
      if (this._neutralCallback != null)
        EventDelegate.Remove(this.neutralSwitch.onChange, this._neutralCallback);
      this._selfCallback = (EventDelegate.Callback) null;
      this._friendCallback = (EventDelegate.Callback) null;
      this._enemyCallback = (EventDelegate.Callback) null;
      this._neutralCallback = (EventDelegate.Callback) null;
    }
  }
}
