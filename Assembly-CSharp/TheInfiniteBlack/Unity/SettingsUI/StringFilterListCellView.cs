﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.StringFilterListCellView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  [RequireComponent(typeof (StringFilterDataBinding))]
  public class StringFilterListCellView : ListCellViewBase<StringFilter, StringFilterDataBinding>
  {
    public UIInput stringInput;
    protected ListCellViewBase<StringFilter, StringFilterDataBinding> mBinding;

    protected void OnEnable()
    {
      this.dataBinding.stringValue = this.stringInput;
    }

    protected override void OnInitialize()
    {
      this.name = this.data.Value;
      this.SetMode(ListCellViewBase<StringFilter>.CellViewMode.Content);
      this.SetFocus(false);
      EventDelegate.Add(this.stringInput.onSubmit, new EventDelegate.Callback(this.OnSubmit));
    }

    public void OnSubmit()
    {
      this.mController.SetFocused((ListCellViewBase<StringFilter>) null);
    }

    protected void Awake()
    {
      if ((bool) ((Object) this.stringInput))
        return;
      this.stringInput = this.GetComponent<UIInput>();
    }
  }
}
