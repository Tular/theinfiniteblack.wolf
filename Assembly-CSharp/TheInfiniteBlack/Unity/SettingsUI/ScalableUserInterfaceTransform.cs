﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.ScalableUserInterfaceTransform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  [AdvancedInspector.AdvancedInspector]
  public class ScalableUserInterfaceTransform : MonoBehaviour
  {
    [Inspect(0)]
    public bool scaleAllContent = true;
    [Expandable(false)]
    [Inspect(1, MethodName = "ShowContentRootsArray")]
    public Transform[] unscaledContentRoots;

    private bool ShowContentRootsArray()
    {
      return !this.scaleAllContent;
    }

    private void Awake()
    {
      if (!this.enabled)
        return;
      UserInterfaceScaler.Add(this.transform);
    }

    private void OnEnable()
    {
      this.transform.localScale = UserInterfaceScaler.value;
      if (this.scaleAllContent)
        return;
      Vector3 vector3 = (double) UserInterfaceScaler.value.x >= 1.0 ? Vector3.one : new Vector3(1f / UserInterfaceScaler.value.x, 1f / UserInterfaceScaler.value.y, 1f / UserInterfaceScaler.value.z);
      for (int index = 0; index < this.unscaledContentRoots.Length; ++index)
      {
        if (!((Object) this.unscaledContentRoots[index] == (Object) null))
          this.unscaledContentRoots[index].localScale = vector3;
      }
    }

    private void OnDestroy()
    {
      UserInterfaceScaler.Remove(this.transform);
    }
  }
}
