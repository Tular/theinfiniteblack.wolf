﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.StringFilterDataBinding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class StringFilterDataBinding : DataBindingBase<StringFilter>
  {
    public UIInput stringValue;

    protected override void BindData()
    {
      this.stringValue.value = this.mData.Value;
    }

    protected override void DoDataSourceUpdate()
    {
      this.mData.Value = this.stringValue.value;
    }

    protected override void UnBindData()
    {
    }

    protected override void AddDelegates()
    {
    }

    protected override void RemoveDelegates()
    {
    }
  }
}
