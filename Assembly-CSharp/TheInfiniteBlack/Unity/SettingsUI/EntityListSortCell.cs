﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.EntityListSortCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  [RequireComponent(typeof (UISelectableButton))]
  [RequireComponent(typeof (SortedToggleFilterViewModel))]
  public class EntityListSortCell : MonoBehaviour
  {
    public UILabel label;
    private UISelectableButton _btn;
    private SortedToggleFilterViewModel _vm;
    public string filterName;
    private EntityListSortController _controller;
    private UICenterOnChild _centerOnChild;

    public int sortOrder
    {
      get
      {
        return this._vm.sortOrder;
      }
      set
      {
        this._vm.sortOrder = value;
        this.name = string.Format("[{0}] {1}", (object) this._vm.sortOrder.ToString("D2"), (object) this.filterName);
      }
    }

    public void Bind(SortedToggleFilter filter)
    {
      this._vm.Bind(filter);
      this.name = string.Format("[{0}] {1}", (object) this.sortOrder.ToString("D2"), (object) this.filterName);
      this.label.text = this.filterName;
    }

    public void UnBind()
    {
      this.name = "unbound cell";
      this._vm.UnBind();
    }

    public void OnCellSelected()
    {
      this._controller.SetSelected(this);
    }

    public void OnCellDeSelected()
    {
      if (!((Object) this._btn != (Object) null))
        return;
      this._btn.SetState(UISelectableButton.ButtonState.Normal);
    }

    [ContextMenu("UpdateName")]
    public void UpdateName()
    {
      this.name = this.filterName;
      this.label.text = this.filterName;
    }

    private void Awake()
    {
      this._btn = this.GetComponent<UISelectableButton>();
      this._controller = NGUITools.FindInParents<EntityListSortController>(this.gameObject);
      this._vm = this.GetComponent<SortedToggleFilterViewModel>();
    }
  }
}
