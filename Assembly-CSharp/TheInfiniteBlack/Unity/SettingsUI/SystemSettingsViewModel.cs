﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.SystemSettingsViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class SystemSettingsViewModel : DataBindingBase<SystemSettings>
  {
    public UISwitchControl combatFlash;
    public UISwitchControl background;
    public UISwitchControl visuals;
    public UISwitchControl gridLines;
    public UISwitchControl rejectTrades;
    public UISwitchControl rejectInvites;
    public UISwitchControl filterProfanity;
    public UISwitchControl showPortraits;
    public UISwitchControl allowPortraitScreenMode;
    public UISlider combatSound;
    public UISlider music;
    public UISlider uiSound;
    private EventDelegate.Callback _combatFlashCallback;
    private EventDelegate.Callback _combatSoundCallback;
    private EventDelegate.Callback _backgroundCallback;
    private EventDelegate.Callback _musicCallback;
    private EventDelegate.Callback _uiSoundCallback;
    private EventDelegate.Callback _visualsCallback;
    private EventDelegate.Callback _gridLinesCallback;
    private EventDelegate.Callback _rejectTradesCallback;
    private EventDelegate.Callback _rejectInvitesCallback;
    private EventDelegate.Callback _filterProfanityCallback;
    private EventDelegate.Callback _showPortraitsCallback;
    private EventDelegate.Callback _allowPortraitScreenModeCallback;
    private SocialSettings _socialSettings;

    public void Bind(SystemSettings sysSettings, SocialSettings socSettings)
    {
      this._socialSettings = socSettings;
      this.Bind(sysSettings);
    }

    protected override void BindData()
    {
      this.combatFlash.Set(this.mData.CombatFlash);
      this.background.Set(this.mData.Background);
      this.visuals.Set(this.mData.Visuals);
      this.gridLines.Set(this.mData.Gridlines);
      this.allowPortraitScreenMode.Set(this.mData.AllowPortraitScreenMode);
      this.uiSound.value = this.mData.UiSound;
      this.combatSound.value = this.mData.CombatSound;
      this.music.value = this.mData.Music;
      this.showPortraits.value = this.mData.ShowPortraits;
      this.uiSound.ForceUpdate();
      this.music.ForceUpdate();
      this.combatSound.ForceUpdate();
      this.rejectTrades.Set(!this._socialSettings.RejectTrades);
      this.rejectInvites.Set(!this._socialSettings.RejectInvites);
      this.filterProfanity.Set(this._socialSettings.FilterProfanity);
    }

    protected override void UnBindData()
    {
    }

    protected override void AddDelegates()
    {
      this._combatSoundCallback = (EventDelegate.Callback) (() => this.mData.CombatSound = this.combatSound.value);
      this._musicCallback = (EventDelegate.Callback) (() => this.mData.Music = this.music.value);
      this._uiSoundCallback = (EventDelegate.Callback) (() => this.mData.UiSound = this.uiSound.value);
      this._combatFlashCallback = (EventDelegate.Callback) (() => this.mData.CombatFlash = this.combatFlash.value);
      this._backgroundCallback = (EventDelegate.Callback) (() => this.mData.Background = this.background.value);
      this._visualsCallback = (EventDelegate.Callback) (() => this.mData.Visuals = this.visuals.value);
      this._gridLinesCallback = (EventDelegate.Callback) (() => this.mData.Gridlines = this.gridLines.value);
      this._showPortraitsCallback = (EventDelegate.Callback) (() => this.mData.ShowPortraits = this.showPortraits.value);
      this._rejectInvitesCallback = (EventDelegate.Callback) (() => this._socialSettings.RejectInvites = !this.rejectInvites.value);
      this._rejectTradesCallback = (EventDelegate.Callback) (() => this._socialSettings.RejectTrades = !this.rejectTrades.value);
      this._filterProfanityCallback = (EventDelegate.Callback) (() => this._socialSettings.FilterProfanity = this.filterProfanity.value);
      this._allowPortraitScreenModeCallback = (EventDelegate.Callback) (() =>
      {
        this.mData.AllowPortraitScreenMode = this.allowPortraitScreenMode.value;
        Screen.autorotateToPortrait = this.allowPortraitScreenMode.value;
      });
      EventDelegate.Add(this.combatFlash.onChange, this._combatFlashCallback);
      EventDelegate.Add(this.background.onChange, this._backgroundCallback);
      EventDelegate.Add(this.visuals.onChange, this._visualsCallback);
      EventDelegate.Add(this.gridLines.onChange, this._gridLinesCallback);
      EventDelegate.Add(this.rejectInvites.onChange, this._rejectInvitesCallback);
      EventDelegate.Add(this.rejectTrades.onChange, this._rejectTradesCallback);
      EventDelegate.Add(this.filterProfanity.onChange, this._filterProfanityCallback);
      EventDelegate.Add(this.showPortraits.onChange, this._showPortraitsCallback);
      EventDelegate.Add(this.music.onChange, this._musicCallback);
      EventDelegate.Add(this.uiSound.onChange, this._uiSoundCallback);
      EventDelegate.Add(this.combatSound.onChange, this._combatSoundCallback);
      EventDelegate.Add(this.allowPortraitScreenMode.onChange, this._allowPortraitScreenModeCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._combatFlashCallback != null)
        EventDelegate.Remove(this.combatFlash.onChange, this._combatFlashCallback);
      if (this._combatSoundCallback != null)
        EventDelegate.Remove(this.combatSound.onChange, this._combatSoundCallback);
      if (this._backgroundCallback != null)
        EventDelegate.Remove(this.background.onChange, this._backgroundCallback);
      if (this._musicCallback != null)
        EventDelegate.Remove(this.music.onChange, this._musicCallback);
      if (this._uiSoundCallback != null)
        EventDelegate.Remove(this.uiSound.onChange, this._uiSoundCallback);
      if (this._visualsCallback != null)
        EventDelegate.Remove(this.visuals.onChange, this._visualsCallback);
      if (this._gridLinesCallback != null)
        EventDelegate.Remove(this.gridLines.onChange, this._gridLinesCallback);
      if (this._showPortraitsCallback != null)
        EventDelegate.Remove(this.showPortraits.onChange, this._showPortraitsCallback);
      if (this._rejectInvitesCallback != null)
        EventDelegate.Remove(this.rejectInvites.onChange, this._rejectInvitesCallback);
      if (this._rejectTradesCallback != null)
        EventDelegate.Remove(this.rejectTrades.onChange, this._rejectTradesCallback);
      if (this._filterProfanityCallback != null)
        EventDelegate.Remove(this.filterProfanity.onChange, this._filterProfanityCallback);
      if (this._allowPortraitScreenModeCallback != null)
        EventDelegate.Remove(this.allowPortraitScreenMode.onChange, this._allowPortraitScreenModeCallback);
      this._combatFlashCallback = (EventDelegate.Callback) null;
      this._combatSoundCallback = (EventDelegate.Callback) null;
      this._backgroundCallback = (EventDelegate.Callback) null;
      this._musicCallback = (EventDelegate.Callback) null;
      this._uiSoundCallback = (EventDelegate.Callback) null;
      this._visualsCallback = (EventDelegate.Callback) null;
      this._gridLinesCallback = (EventDelegate.Callback) null;
      this._showPortraitsCallback = (EventDelegate.Callback) null;
      this._rejectInvitesCallback = (EventDelegate.Callback) null;
      this._rejectTradesCallback = (EventDelegate.Callback) null;
      this._filterProfanityCallback = (EventDelegate.Callback) null;
      this._allowPortraitScreenModeCallback = (EventDelegate.Callback) null;
    }

    private void Awake()
    {
      this.allowPortraitScreenMode.gameObject.SetActive(false);
    }
  }
}
