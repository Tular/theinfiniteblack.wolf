﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.ListMenuOptionStringFilters
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class ListMenuOptionStringFilters : MonoBehaviour
  {
    public SettingsWindow window;
    public ListMenuOptionStringFilters.StringFilterCollectionType collection;
    public StringFilterListController listController;
    private GameObject _go;

    public void Show()
    {
      if ((Object) this._go == (Object) null)
        this._go = this.gameObject;
      if ((Object) this.window == (Object) null)
        this.window = NGUITools.FindInParents<SettingsWindow>(this._go);
      if ((Object) this.listController != (Object) null)
        this.ShowList();
      NGUITools.SetActiveSelf(this._go, true);
    }

    public void Hide()
    {
      this.listController.Hide();
      NGUITools.SetActiveSelf(this._go, false);
    }

    public void OnAddClicked()
    {
      this.listController.CreateNewCell();
    }

    private void ShowList()
    {
      switch (this.collection)
      {
        case ListMenuOptionStringFilters.StringFilterCollectionType.FriendsList:
          this.listController.Show(this.window.accountSettings.Social.get_FriendList());
          break;
        case ListMenuOptionStringFilters.StringFilterCollectionType.IgnoreList:
          this.listController.Show(this.window.accountSettings.Social.get_IgnoreList());
          break;
      }
    }

    public enum StringFilterCollectionType
    {
      FriendsList,
      IgnoreList,
    }
  }
}
