﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.SocialSettingsViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class SocialSettingsViewModel : DataBindingBase<SocialSettings>
  {
    public GameEventsFilterViewModel universeChatWindowFilters;
    public GameEventsFilterViewModel serverChatWindowFilters;
    public GameEventsFilterViewModel sectorChatWindowFilters;
    public GameEventsFilterViewModel corpChatWindowFilters;
    public GameEventsFilterViewModel allianceChatWindowFilters;
    public GameEventsFilterViewModel marketChatWindowFilters;
    public GameEventsFilterViewModel eventsChatWindowFilters;
    public UISwitchControl profanityFilterSwitch;
    public UISwitchControl rejectInvitesSwitch;
    public UISwitchControl rejectTradesSwitch;
    private EventDelegate.Callback _profanityFilterCallback;
    private EventDelegate.Callback _rejectInvitesCallback;
    private EventDelegate.Callback _rejectTradesCallback;

    protected override void BindData()
    {
      this.universeChatWindowFilters.Bind(this.mData.UniverseChat);
      this.serverChatWindowFilters.Bind(this.mData.ServerChat);
      this.sectorChatWindowFilters.Bind(this.mData.SectorChat);
      this.corpChatWindowFilters.Bind(this.mData.CorpChat);
      this.allianceChatWindowFilters.Bind(this.mData.AllianceChat);
      this.marketChatWindowFilters.Bind(this.mData.MarketChat);
      this.eventsChatWindowFilters.Bind(this.mData.EventsChat);
      this.profanityFilterSwitch.value = this.mData.FilterProfanity;
      this.rejectInvitesSwitch.value = this.mData.RejectInvites;
      this.rejectTradesSwitch.value = this.mData.RejectTrades;
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
      this.universeChatWindowFilters.UnBind();
      this.serverChatWindowFilters.UnBind();
      this.sectorChatWindowFilters.UnBind();
      this.corpChatWindowFilters.UnBind();
      this.allianceChatWindowFilters.UnBind();
      this.marketChatWindowFilters.UnBind();
      this.eventsChatWindowFilters.UnBind();
    }

    protected override void AddDelegates()
    {
      this._profanityFilterCallback = (EventDelegate.Callback) (() => this.mData.FilterProfanity = this.profanityFilterSwitch.value);
      this._rejectInvitesCallback = (EventDelegate.Callback) (() => this.mData.RejectInvites = this.rejectInvitesSwitch.value);
      this._rejectTradesCallback = (EventDelegate.Callback) (() => this.mData.RejectTrades = this.rejectTradesSwitch.value);
      EventDelegate.Add(this.profanityFilterSwitch.onChange, this._profanityFilterCallback);
      EventDelegate.Add(this.rejectInvitesSwitch.onChange, this._rejectInvitesCallback);
      EventDelegate.Add(this.rejectTradesSwitch.onChange, this._rejectTradesCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._profanityFilterCallback != null)
        EventDelegate.Remove(this.profanityFilterSwitch.onChange, this._profanityFilterCallback);
      if (this._rejectInvitesCallback != null)
        EventDelegate.Remove(this.rejectInvitesSwitch.onChange, this._rejectInvitesCallback);
      if (this._rejectTradesCallback == null)
        return;
      EventDelegate.Remove(this.rejectTradesSwitch.onChange, this._rejectTradesCallback);
    }
  }
}
