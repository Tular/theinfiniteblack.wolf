﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.GameEventsFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class GameEventsFilterViewModel : DataBindingBase<GameEventsFilter>
  {
    public CombatFilterViewModel combatFilters;
    public ChatFilterViewModel chatFilters;
    public MiscFilterViewModel miscFilters;

    protected override void BindData()
    {
      this.combatFilters.Bind(this.mData.Combat);
      this.chatFilters.Bind(this.mData.Chat);
      this.miscFilters.Bind(this.mData.Misc);
    }

    protected override void UnBindData()
    {
      this.combatFilters.UnBind();
      this.chatFilters.UnBind();
      this.miscFilters.UnBind();
    }

    protected override void AddDelegates()
    {
    }

    protected override void RemoveDelegates()
    {
    }
  }
}
