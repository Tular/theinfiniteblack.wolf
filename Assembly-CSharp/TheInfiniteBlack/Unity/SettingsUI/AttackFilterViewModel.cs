﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.AttackFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class AttackFilterViewModel : DataBindingBase<AttackFilter>
  {
    public RelationshipFilterViewModel collide;
    public RelationshipFilterViewModel critical;
    public RelationshipFilterViewModel deflect;
    public RelationshipFilterViewModel degrapple;
    public RelationshipFilterViewModel grapple;
    public RelationshipFilterViewModel graze;
    public RelationshipFilterViewModel hit;
    public RelationshipFilterViewModel miss;
    public RelationshipFilterViewModel ram;
    public RelationshipFilterViewModel repair;
    public RelationshipFilterViewModel splash;
    public RelationshipFilterViewModel stun;

    protected override void BindData()
    {
      this.miss.Bind(this.mData.Miss);
      this.graze.Bind(this.mData.Graze);
      this.hit.Bind(this.mData.Hit);
      this.critical.Bind(this.mData.Critical);
      this.splash.Bind(this.mData.Splash);
      this.repair.Bind(this.mData.Repair);
      this.stun.Bind(this.mData.Stun);
      this.collide.Bind(this.mData.Collide);
      this.deflect.Bind(this.mData.Deflect);
      this.ram.Bind(this.mData.Ram);
      this.grapple.Bind(this.mData.Grapple);
      this.degrapple.Bind(this.mData.DeGrapple);
    }

    protected override void UnBindData()
    {
      this.miss.UnBind();
      this.graze.UnBind();
      this.hit.UnBind();
      this.critical.UnBind();
      this.splash.UnBind();
      this.repair.UnBind();
      this.stun.UnBind();
      this.collide.UnBind();
      this.deflect.UnBind();
      this.ram.UnBind();
      this.grapple.UnBind();
      this.degrapple.UnBind();
    }

    protected override void AddDelegates()
    {
    }

    protected override void RemoveDelegates()
    {
    }
  }
}
