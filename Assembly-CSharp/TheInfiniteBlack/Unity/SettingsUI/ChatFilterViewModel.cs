﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.ChatFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class ChatFilterViewModel : DataBindingBase<ChatFilter>
  {
    public UISwitchControl alertSwitch;
    public UISwitchControl allianceSwitch;
    public UISwitchControl corpSwitch;
    public UISwitchControl eventSwitch;
    public UISwitchControl marketSwitch;
    public UISwitchControl marketEventSwitch;
    public UISwitchControl privateSwitch;
    public UISwitchControl sectorSwitch;
    public UISwitchControl serverSwitch;
    public UISwitchControl universeSwitch;
    private EventDelegate.Callback _serverCallback;
    private EventDelegate.Callback _corpCallback;
    private EventDelegate.Callback _sectorCallback;
    private EventDelegate.Callback _privateCallback;
    private EventDelegate.Callback _allianceCallback;
    private EventDelegate.Callback _eventCallback;
    private EventDelegate.Callback _alertCallback;
    private EventDelegate.Callback _marketCallback;
    private EventDelegate.Callback _marketEventCallback;
    private EventDelegate.Callback _universeCallback;

    protected override void BindData()
    {
      this.serverSwitch.value = this.mData.Server;
      this.corpSwitch.value = this.mData.Corp;
      this.sectorSwitch.value = this.mData.Sector;
      this.privateSwitch.value = this.mData.Private;
      this.allianceSwitch.value = this.mData.Alliance;
      this.eventSwitch.value = this.mData.Event;
      this.alertSwitch.value = this.mData.Alert;
      this.marketSwitch.value = this.mData.Market;
      this.marketEventSwitch.value = this.mData.MarketEvent;
      this.universeSwitch.value = this.mData.Universe;
    }

    protected override void UnBindData()
    {
      this.RemoveDelegates();
    }

    protected override void AddDelegates()
    {
      this._serverCallback = (EventDelegate.Callback) (() => this.mData.Server = this.serverSwitch.value);
      this._corpCallback = (EventDelegate.Callback) (() => this.mData.Corp = this.corpSwitch.value);
      this._sectorCallback = (EventDelegate.Callback) (() => this.mData.Sector = this.sectorSwitch.value);
      this._privateCallback = (EventDelegate.Callback) (() => this.mData.Private = this.privateSwitch.value);
      this._allianceCallback = (EventDelegate.Callback) (() => this.mData.Alliance = this.allianceSwitch.value);
      this._eventCallback = (EventDelegate.Callback) (() => this.mData.Event = this.eventSwitch.value);
      this._alertCallback = (EventDelegate.Callback) (() => this.mData.Alert = this.alertSwitch.value);
      this._marketCallback = (EventDelegate.Callback) (() => this.mData.Market = this.marketSwitch.value);
      this._marketEventCallback = (EventDelegate.Callback) (() => this.mData.MarketEvent = this.marketEventSwitch.value);
      this._universeCallback = (EventDelegate.Callback) (() => this.mData.Universe = this.universeSwitch.value);
      EventDelegate.Add(this.serverSwitch.onChange, this._serverCallback);
      EventDelegate.Add(this.corpSwitch.onChange, this._corpCallback);
      EventDelegate.Add(this.sectorSwitch.onChange, this._sectorCallback);
      EventDelegate.Add(this.privateSwitch.onChange, this._privateCallback);
      EventDelegate.Add(this.allianceSwitch.onChange, this._allianceCallback);
      EventDelegate.Add(this.eventSwitch.onChange, this._eventCallback);
      EventDelegate.Add(this.alertSwitch.onChange, this._alertCallback);
      EventDelegate.Add(this.marketSwitch.onChange, this._marketCallback);
      EventDelegate.Add(this.marketEventSwitch.onChange, this._marketEventCallback);
      EventDelegate.Add(this.universeSwitch.onChange, this._universeCallback);
    }

    protected override void RemoveDelegates()
    {
      if (this._serverCallback != null)
        EventDelegate.Remove(this.serverSwitch.onChange, this._serverCallback);
      if (this._corpCallback != null)
        EventDelegate.Remove(this.corpSwitch.onChange, this._corpCallback);
      if (this._sectorCallback != null)
        EventDelegate.Remove(this.sectorSwitch.onChange, this._sectorCallback);
      if (this._privateCallback != null)
        EventDelegate.Remove(this.privateSwitch.onChange, this._privateCallback);
      if (this._allianceCallback != null)
        EventDelegate.Remove(this.allianceSwitch.onChange, this._allianceCallback);
      if (this._eventCallback != null)
        EventDelegate.Remove(this.eventSwitch.onChange, this._eventCallback);
      if (this._alertCallback != null)
        EventDelegate.Remove(this.alertSwitch.onChange, this._alertCallback);
      if (this._marketCallback != null)
        EventDelegate.Remove(this.marketSwitch.onChange, this._marketCallback);
      if (this._marketEventCallback != null)
        EventDelegate.Remove(this.marketEventSwitch.onChange, this._marketEventCallback);
      if (this._universeCallback == null)
        return;
      EventDelegate.Remove(this.universeSwitch.onChange, this._universeCallback);
    }
  }
}
