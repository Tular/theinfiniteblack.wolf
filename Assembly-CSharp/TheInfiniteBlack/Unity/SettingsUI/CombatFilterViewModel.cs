﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsUI.CombatFilterViewModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Unity.SettingsUI
{
  public class CombatFilterViewModel : DataBindingBase<CombatFilter>
  {
    public UnitFilterViewModel unitFilters;
    public AttackFilterViewModel attackFilters;

    protected override void BindData()
    {
      this.unitFilters.Bind(this.mData.Units);
      this.attackFilters.Bind(this.mData.Attacks);
    }

    protected override void UnBindData()
    {
      this.unitFilters.UnBind();
      this.attackFilters.UnBind();
    }

    protected override void AddDelegates()
    {
    }

    protected override void RemoveDelegates()
    {
    }
  }
}
