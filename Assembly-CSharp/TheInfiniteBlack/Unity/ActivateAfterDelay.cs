﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ActivateAfterDelay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client.Events;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class ActivateAfterDelay : MonoBehaviour
  {
    public GameObject[] targets;
    public bool endOfFrame;
    public int delaySeconds;
    public int delayFrames;
    private int _enableFrame;

    [DebuggerHidden]
    private IEnumerator Start()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ActivateAfterDelay.\u003CStart\u003Ec__Iterator25()
      {
        \u003C\u003Ef__this = this
      };
    }

    private void Instance_onDisconnectEvent(object sender, DisconnectEventArgs e)
    {
      for (int index = 0; index < this.targets.Length; ++index)
        this.targets[index].SetActive(false);
    }

    private void Update()
    {
      if (Time.frameCount > this._enableFrame)
        return;
      for (int index = 0; index < this.targets.Length; ++index)
        this.targets[index].SetActive(true);
      this.enabled = false;
    }

    private void OnDestroy()
    {
      TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.Instance_onDisconnectEvent);
    }
  }
}
