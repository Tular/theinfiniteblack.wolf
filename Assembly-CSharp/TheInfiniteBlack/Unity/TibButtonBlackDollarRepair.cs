﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibButtonBlackDollarRepair
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class TibButtonBlackDollarRepair : MonoBehaviour
  {
    public UILabel repairTimer;
    public string helpText;
    private bool _canRepair;
    private UISpriteColorGroupControllerEventListener[] _spriteColorgroupEventListeners;

    private void DoBlackDollarRepair()
    {
      Debug.Log((object) string.Format("TibButtonBlackDollarRepair.DoBlackDollarRepair()"));
      if (TibProxy.gameState == null)
        return;
      TibProxy.gameState.DoPurchase(PurchaseItem.FullRepair);
    }

    private bool canRepair
    {
      get
      {
        return this._canRepair;
      }
      set
      {
        if (this._canRepair == value)
          return;
        if (value)
        {
          this.repairTimer.enabled = false;
          foreach (UISpriteColorGroupControllerEventListener colorgroupEventListener in this._spriteColorgroupEventListeners)
            colorgroupEventListener.SetPressed(false, false);
        }
        else
        {
          this.repairTimer.enabled = true;
          foreach (UISpriteColorGroupControllerEventListener colorgroupEventListener in this._spriteColorgroupEventListeners)
            colorgroupEventListener.SetPressed(true, true);
        }
        this._canRepair = value;
      }
    }

    private void OnPress(bool isPressed)
    {
      if (!isPressed)
        return;
      this.DoBlackDollarRepair();
    }

    private void Awake()
    {
      this._spriteColorgroupEventListeners = this.GetComponents<UISpriteColorGroupControllerEventListener>();
      this._canRepair = false;
      this.canRepair = true;
    }

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    private void Update()
    {
      if (TibProxy.gameState == null)
        return;
      this.canRepair = TibProxy.gameState.RepairCooldown.IsFinished;
      string str = string.Format("{0:D2}", (object) (int) TibProxy.gameState.RepairCooldown.RemainingSeconds);
      if (this.canRepair || this.repairTimer.text.Equals(str))
        return;
      this.repairTimer.text = str;
    }
  }
}
