﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.IMapNodeGeometryBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  public interface IMapNodeGeometryBuilder
  {
    Material material { get; }

    float tileSize { get; }

    void BuildSector(string[] visibleLayers, ref Vector3 sectorPos, ref Vector3[] verts, ref Vector2[] uvs, ref Vector3[] normals, ref int[] tris, ref int startIdx);

    IMapNodeLayerState GetLayerStateInstance();
  }
}
