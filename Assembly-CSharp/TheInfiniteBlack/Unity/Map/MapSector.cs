﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.MapSector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Linq;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  [AdvancedInspector.AdvancedInspector]
  public class MapSector
  {
    private static int _globalStateChangeFrame = int.MinValue;
    private static bool _globalStateValid;
    private static IGameState _gameState;
    private static Sector _myLoc;
    private static ClientPlayer _myPlayer;
    private static ClientCorporation _myCorp;
    private static ClientAlliance _myAlliance;
    private static CourseController _course;
    public readonly int x;
    public readonly int y;
    private Sector _sector;
    private SectorPlayerList _playerList;
    private SectorCountdownTimer _countdownTimer;
    private readonly IMapNodeLayerState _layerState;

    private MapSector(int sectorX, int sectorY, IMapNodeLayerState layerStateInstance)
    {
      this.x = sectorX;
      this.y = sectorY;
      this._layerState = layerStateInstance;
    }

    public static MapSector Build(IMapNodeGeometryBuilder builder, int sectorX, int sectorY)
    {
      return new MapSector(sectorX, sectorY, builder.GetLayerStateInstance());
    }

    private static void SetGlobalState()
    {
      if (MapSector._globalStateChangeFrame == Time.frameCount)
        return;
      MapSector._globalStateChangeFrame = Time.frameCount;
      if ((UnityEngine.Object) PreInstantiatedSingleton<SectorMap>.Instance == (UnityEngine.Object) null || !PreInstantiatedSingleton<SectorMap>.Instance.isInitialized)
      {
        MapSector._globalStateValid = false;
      }
      else
      {
        MapSector._gameState = TibProxy.gameState;
        if (MapSector._gameState == null)
        {
          MapSector._globalStateValid = false;
        }
        else
        {
          MapSector._myPlayer = MapSector._gameState.MyPlayer;
          if (MapSector._myPlayer == null)
          {
            MapSector._globalStateValid = false;
          }
          else
          {
            MapSector._myLoc = MapSector._gameState.MyLoc;
            if (MapSector._myLoc == null)
            {
              MapSector._globalStateValid = false;
            }
            else
            {
              MapSector._course = MapSector._gameState.Course;
              MapSector._myCorp = MapSector._myPlayer.MyCorporation;
              MapSector._myAlliance = MapSector._myPlayer.MyAlliance;
              MapSector._globalStateValid = true;
            }
          }
        }
      }
    }

    public void ResetState()
    {
      if ((bool) ((UnityEngine.Object) this._playerList))
      {
        PreInstantiatedSingleton<SectorMap>.Instance.playerListPool.Despawn(this._playerList.transform);
        this._playerList = (SectorPlayerList) null;
      }
      MapSector._globalStateChangeFrame = int.MinValue;
      MapSector._globalStateValid = false;
      MapSector._gameState = (IGameState) null;
      MapSector._myLoc = (Sector) null;
      MapSector._myPlayer = (ClientPlayer) null;
      MapSector._myCorp = (ClientCorporation) null;
      this._layerState.ResetState();
      this._sector = (Sector) null;
    }

    private void UpdatePlayerList(bool sectorContainsPlayers)
    {
      if (sectorContainsPlayers)
      {
        if (!(bool) ((UnityEngine.Object) this._playerList))
          this._playerList = PreInstantiatedSingleton<SectorMap>.Instance.BuildPlayerList(this);
        this._playerList.UpdatePlayerNames(this._sector.get_Players());
      }
      else
      {
        if ((UnityEngine.Object) this._playerList == (UnityEngine.Object) null)
          return;
        PreInstantiatedSingleton<SectorMap>.Instance.ReleasePlayerList(this._playerList);
        this._playerList = (SectorPlayerList) null;
      }
    }

    private void UpdateCountdownTimer()
    {
      if ((UnityEngine.Object) this._countdownTimer != (UnityEngine.Object) null && this._sector.StatusTimeSeconds <= 0)
      {
        PreInstantiatedSingleton<SectorMap>.Instance.ReleaseCountdownTimer(this._countdownTimer);
        this._countdownTimer = (SectorCountdownTimer) null;
      }
      else
      {
        if (this._sector.StatusTimeSeconds <= 0)
          return;
        if ((UnityEngine.Object) this._countdownTimer == (UnityEngine.Object) null)
          this._countdownTimer = PreInstantiatedSingleton<SectorMap>.Instance.AcquireCountdownTimer(this);
        this._countdownTimer.text = this._sector.StatusTimeSeconds.ToString();
      }
    }

    private void UpdateInvasionLayer()
    {
      switch (this._sector.Status)
      {
        case SectorStatus.PIRATE:
          this._layerState.PushLayer("map_invasion_pirate");
          break;
        case SectorStatus.WYRD:
          this._layerState.PushLayer("map_invasion_wyrd");
          break;
        case SectorStatus.HETEROCLITE:
          this._layerState.PushLayer("map_invasion_heteroclite");
          break;
      }
    }

    private void UpdateConnectorLayers(bool isSectorInCourse)
    {
      if (this._sector.Visibility == SectorVisibility.Unexplored)
        return;
      if (this._sector.N != null)
      {
        if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.N) || ((object) MapSector._myLoc).Equals((object) this._sector.N)))
          this._layerState.PushLayer("map_line_n_on");
        else
          this._layerState.PushLayer("map_line_n");
      }
      if (this._sector.NE != null)
      {
        if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.NE) || ((object) MapSector._myLoc).Equals((object) this._sector.NE)))
          this._layerState.PushLayer("map_line_ne_on");
        else
          this._layerState.PushLayer("map_line_ne");
      }
      if (this._sector.E != null)
      {
        if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.E) || ((object) MapSector._myLoc).Equals((object) this._sector.E)))
          this._layerState.PushLayer("map_line_e_on");
        else
          this._layerState.PushLayer("map_line_e");
      }
      if (this._sector.SE != null)
      {
        if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.SE) || ((object) MapSector._myLoc).Equals((object) this._sector.SE)))
          this._layerState.PushLayer("map_line_se_on");
        else
          this._layerState.PushLayer("map_line_se");
      }
      if (this._sector.S != null)
      {
        if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.S) || ((object) MapSector._myLoc).Equals((object) this._sector.S)))
          this._layerState.PushLayer("map_line_s_on");
        else
          this._layerState.PushLayer("map_line_s");
      }
      if (this._sector.SW != null)
      {
        if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.SW) || ((object) MapSector._myLoc).Equals((object) this._sector.SW)))
          this._layerState.PushLayer("map_line_sw_on");
        else
          this._layerState.PushLayer("map_line_sw");
      }
      if (this._sector.W != null)
      {
        if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.W) || ((object) MapSector._myLoc).Equals((object) this._sector.W)))
          this._layerState.PushLayer("map_line_w_on");
        else
          this._layerState.PushLayer("map_line_w");
      }
      if (this._sector.NW == null)
        return;
      if (isSectorInCourse && (MapSector._course.IsInCourse(this._sector.NW) || ((object) MapSector._myLoc).Equals((object) this._sector.NW)))
        this._layerState.PushLayer("map_line_nw_on");
      else
        this._layerState.PushLayer("map_line_nw");
    }

    private void UpdateAsteroidLayer()
    {
      if (this._sector.Asteroid == AsteroidClass.NULL)
        return;
      switch (this._sector.Asteroid)
      {
        case AsteroidClass.SMALL_ROCK:
        case AsteroidClass.MEDIUM_ROCK:
        case AsteroidClass.LARGE_ROCK:
        case AsteroidClass.VERYLARGE_ROCK:
          this._layerState.PushLayer("map_asteroid_rock");
          break;
        case AsteroidClass.SMALL_METAL:
        case AsteroidClass.MEDIUM_METAL:
        case AsteroidClass.LARGE_METAL:
        case AsteroidClass.VERYLARGE_METAL:
          this._layerState.PushLayer("map_asteroid_metal");
          break;
        case AsteroidClass.SMALL_RADIOACTIVE:
        case AsteroidClass.MEDIUM_RADIOACTIVE:
        case AsteroidClass.LARGE_RADIOACTIVE:
        case AsteroidClass.VERYLARGE_RADIOACTIVE:
          this._layerState.PushLayer("map_asteroid_radioactive");
          break;
        case AsteroidClass.SMALL_ICE:
        case AsteroidClass.MEDIUM_ICE:
        case AsteroidClass.LARGE_ICE:
        case AsteroidClass.VERYLARGE_ICE:
          this._layerState.PushLayer("map_asteroid_ice");
          break;
        case AsteroidClass.SMALL_DARKMATTER:
        case AsteroidClass.MEDIUM_DARKMATTER:
        case AsteroidClass.LARGE_DARKMATTER:
        case AsteroidClass.VERYLARGE_DARKMATTER:
          this._layerState.PushLayer("map_asteroid_darkmatter");
          break;
      }
    }

    private void UpdateEngagementLayer()
    {
      if (this._sector.Status != SectorStatus.PRE_ENGAGE && this._sector.Status != SectorStatus.ENGAGED)
        return;
      this._layerState.PushLayer("map_engagement");
    }

    private void UpdateBorderLayer(bool hasPlayers)
    {
      if (hasPlayers)
      {
        if (this._sector.get_Players().Any<ClientPlayer>((Func<ClientPlayer, bool>) (p => p.Relation == RelationType.FRIEND)))
          this._layerState.PushLayer("map_border_blue");
        else
          this._layerState.PushLayer("map_border_red");
      }
      else
      {
        switch (this._sector.Visibility)
        {
          case SectorVisibility.Encountered:
            this._layerState.PushLayer("map_border_grey");
            break;
          case SectorVisibility.Explored:
            switch (this._sector.Status)
            {
              case SectorStatus.PIRATE:
              case SectorStatus.WYRD:
              case SectorStatus.HETEROCLITE:
                this._layerState.PushLayer("map_border_red");
                return;
              default:
                return;
            }
        }
      }
    }

    private void UpdateTileLayer()
    {
      if (this._sector.Visibility != SectorVisibility.Explored)
        return;
      if (this._sector.Alliance != null)
      {
        if (MapSector._myAlliance == null || !((object) MapSector._myAlliance).Equals((object) this._sector.Alliance))
          this._layerState.PushLayer("map_sector_red");
        else
          this._layerState.PushLayer("map_sector_orange");
      }
      else if (this._sector.Corporation != null)
      {
        if (MapSector._myCorp == null || !((object) MapSector._myCorp).Equals((object) this._sector.Corporation))
          this._layerState.PushLayer("map_sector_red");
        else
          this._layerState.PushLayer("map_sector_orange");
      }
      else
      {
        switch (this._sector.Class)
        {
          case SectorClass.PROTECTED:
            this._layerState.PushLayer("map_sector_blue");
            break;
          case SectorClass.NEUTRAL:
            this._layerState.PushLayer("map_sector_grey");
            break;
          case SectorClass.CONTESTED:
            this._layerState.PushLayer("map_sector_black");
            break;
        }
      }
    }

    private void UpdateLayerState()
    {
      bool flag1 = ((object) MapSector._myLoc).Equals((object) this._sector);
      bool isSectorInCourse = MapSector._gameState.Course.Count > 0 && (flag1 || MapSector._course.IsInCourse(this._sector));
      bool flag2 = this._sector.PlayerCount > 0;
      this.UpdatePlayerList(flag2);
      this.UpdateCountdownTimer();
      if (this._sector.Visibility == SectorVisibility.Unexplored)
      {
        if (this._sector.Alliance != null && this._sector.Alliance.Location == this._sector)
          this._layerState.PushLayer("map_planet");
        this.UpdateBorderLayer(flag2);
        if (this._sector.Corporation != null)
          this._layerState.PushLayer("map_garrison");
        this._layerState.CommitState();
      }
      else
      {
        if (this._sector.Alliance != null && this._sector.Alliance.Location == this._sector || this._sector.IsSol)
          this._layerState.PushLayer("map_planet");
        this.UpdateConnectorLayers(isSectorInCourse);
        this.UpdateTileLayer();
        this.UpdateBorderLayer(flag2);
        if (flag1)
          this._layerState.PushLayer("map_my_sector_a");
        else if (isSectorInCourse)
          this._layerState.PushLayer("map_in_course_a");
        if (this._sector.Corporation != null)
          this._layerState.PushLayer("map_garrison");
        if (this._sector.StarPort)
          this._layerState.PushLayer("map_starport");
        this.UpdateInvasionLayer();
        this.UpdateAsteroidLayer();
        this.UpdateEngagementLayer();
        this._layerState.CommitState();
      }
    }

    public int layerCount
    {
      get
      {
        return this._layerState.layerCount;
      }
    }

    public int lastLayerStateChangeId
    {
      get
      {
        return this._layerState.lastChangedId;
      }
    }

    public string[] visibleLayers
    {
      get
      {
        return this._layerState.GetLayerStack();
      }
    }

    public void OnUpdate(bool riftSpace)
    {
      MapSector.SetGlobalState();
      if (!MapSector._globalStateValid)
        return;
      this._sector = !riftSpace ? MapSector._gameState.Map.get_NormalSpace()[this.x, this.y] : MapSector._gameState.Map.get_RiftSpace()[this.x, this.y];
      this.UpdateLayerState();
    }

    public void OnBecameVisible()
    {
      if (!(bool) ((UnityEngine.Object) this._playerList))
        return;
      this._playerList.enabled = true;
    }

    public void OnBecameInvisible()
    {
      if (!(bool) ((UnityEngine.Object) this._playerList))
        return;
      this._playerList.enabled = false;
    }

    public void OnDisable()
    {
      if (!(bool) ((UnityEngine.Object) this._playerList))
        return;
      PreInstantiatedSingleton<SectorMap>.Instance.playerListPool.Despawn(this._playerList.transform);
      this._playerList = (SectorPlayerList) null;
    }
  }
}
