﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.MapSection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Collections.Generic;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;
using UnityEngine.Rendering;

namespace TheInfiniteBlack.Unity.Map
{
  [AdvancedInspector.AdvancedInspector]
  [RequireComponent(typeof (MeshFilter), typeof (MeshRenderer))]
  public class MapSection : MonoBehaviour
  {
    private readonly List<MapSector> _sectors = new List<MapSector>(100);
    private bool _wasVisible = true;
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;
    private Mesh _mesh;
    private IMapNodeGeometryBuilder _builder;
    private Vector3 _nodeMargin;
    private int _sectionSize;
    private int[] _sectorsLastLayerChangeId;
    private bool _riftSpace;

    [ReadOnly]
    [Group("Section Data", Expandable = false, Priority = 0)]
    [Inspect(0)]
    public int sectionX { get; private set; }

    [ReadOnly]
    [Inspect(1)]
    [Group("Section Data")]
    public int sectionY { get; private set; }

    [Group("Section Data")]
    [ReadOnly]
    [Inspect(2)]
    public int mapStartX { get; private set; }

    [Inspect(3)]
    [ReadOnly]
    [Group("Section Data")]
    public int mapEndX { get; private set; }

    [Group("Section Data")]
    [ReadOnly]
    [Inspect(4)]
    public int mapStartY { get; private set; }

    [Group("Section Data")]
    [ReadOnly]
    [Inspect(5)]
    public int mapEndY { get; private set; }

    [Inspect(6)]
    [Group("Section Data")]
    [ReadOnly]
    public int sectorCount
    {
      get
      {
        if (this._sectors == null)
          return 0;
        return this._sectors.Count;
      }
    }

    public static MapSection Create(GameObject sectionsRoot, SectorGeometryBuilder builder, Vector3 startPos, Vector3 nodeMargin, int sectionSize, int sectionX, int sectionY)
    {
      GameObject gameObject1 = new GameObject();
      gameObject1.name = "[" + (object) sectionX + "," + (object) sectionY + "]";
      GameObject gameObject2 = gameObject1;
      gameObject2.layer = sectionsRoot.layer;
      gameObject2.transform.parent = sectionsRoot.transform;
      gameObject2.transform.localPosition = startPos;
      gameObject2.transform.localRotation = Quaternion.identity;
      gameObject2.transform.localScale = Vector3.one;
      MapSection mapSection = gameObject2.AddComponent<MapSection>();
      mapSection._meshRenderer.sharedMaterial = builder.material;
      mapSection._builder = (IMapNodeGeometryBuilder) builder;
      mapSection._nodeMargin = nodeMargin;
      mapSection._sectionSize = sectionSize;
      mapSection.sectionX = sectionX;
      mapSection.sectionY = sectionY;
      mapSection.mapStartX = sectionX * sectionSize;
      mapSection.mapEndX = sectionX * sectionSize + sectionSize - 1;
      mapSection.mapStartY = sectionY * sectionSize;
      mapSection.mapEndY = sectionY * sectionSize + sectionSize - 1;
      return mapSection;
    }

    public void ResetState()
    {
      for (int index = 0; index < this._sectors.Count; ++index)
      {
        this._sectors[index].ResetState();
        this._sectorsLastLayerChangeId[index] = int.MinValue;
      }
      this._mesh = new Mesh();
      this.BuildPlaceholder(this._mesh);
      this._meshFilter.mesh = this._mesh;
    }

    private void BuildPlaceholder(Mesh mesh)
    {
      float x = this._builder.tileSize * (float) this._sectionSize;
      Vector3[] vector3Array1 = new Vector3[4]
      {
        new Vector3(0.0f, -x, 0.0f),
        new Vector3(x, -x, 0.0f),
        new Vector3(0.0f, 0.0f, 0.0f),
        new Vector3(x, 0.0f, 0.0f)
      };
      Vector2[] vector2Array = new Vector2[4]
      {
        Vector2.zero,
        Vector2.zero,
        Vector2.zero,
        Vector2.zero
      };
      Vector3[] vector3Array2 = new Vector3[4]
      {
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward
      };
      int[] numArray = new int[6]{ 2, 3, 0, 1, 0, 3 };
      mesh.vertices = vector3Array1;
      mesh.normals = vector3Array2;
      mesh.uv = vector2Array;
      mesh.triangles = numArray;
      mesh.name = "placeholder";
    }

    private void Awake()
    {
      this._meshFilter = this.GetComponent<MeshFilter>();
      this._meshRenderer = this.GetComponent<MeshRenderer>();
      this._meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
      this._meshRenderer.receiveShadows = false;
      this._meshRenderer.useLightProbes = false;
      this._meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
    }

    private void Start()
    {
      Mesh mesh = new Mesh();
      mesh.name = "Section [" + (object) this.sectionX + "," + (object) this.sectionY + "]";
      this._mesh = mesh;
      this._sectorsLastLayerChangeId = new int[this._sectionSize * this._sectionSize];
      this._meshFilter.sharedMesh = this._mesh;
      this.BuildPlaceholder(this._mesh);
      int index = 0;
      for (int mapStartY = this.mapStartY; mapStartY <= this.mapEndY; ++mapStartY)
      {
        for (int mapStartX = this.mapStartX; mapStartX <= this.mapEndX; ++mapStartX)
        {
          MapSector mapSector = MapSector.Build(this._builder, mapStartX, mapStartY);
          this._sectors.Add(mapSector);
          this._sectorsLastLayerChangeId[index] = mapSector.lastLayerStateChangeId;
          ++index;
        }
      }
    }

    private void OnDisable()
    {
      for (int index = 0; index < this._sectors.Count; ++index)
        this._sectors[index].OnDisable();
    }

    private bool AreBoundsInFustum()
    {
      return GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(PreInstantiatedSingleton<SectorMap>.Instance.controller.mapCamera), this._meshRenderer.bounds);
    }

    public void OnUpdate(bool riftSpace)
    {
      bool flag1 = this.AreBoundsInFustum();
      if (flag1 && !this._wasVisible)
      {
        for (int index = 0; index < this._sectors.Count; ++index)
          this._sectors[index].OnBecameVisible();
      }
      if (!flag1 && this._wasVisible)
      {
        for (int index = 0; index < this._sectors.Count; ++index)
          this._sectors[index].OnBecameInvisible();
      }
      this._wasVisible = flag1;
      if (!flag1)
        return;
      bool flag2 = this._riftSpace != riftSpace;
      this._riftSpace = riftSpace;
      int num = 0;
      for (int index = 0; index < this._sectors.Count; ++index)
      {
        this._sectors[index].OnUpdate(riftSpace);
        num += this._sectors[index].layerCount;
        if (this._sectors[index].lastLayerStateChangeId != this._sectorsLastLayerChangeId[index])
          flag2 = true;
        if (flag2)
          this._sectorsLastLayerChangeId[index] = this._sectors[index].lastLayerStateChangeId;
      }
      if (!flag2)
        return;
      int length1 = num * 4;
      int length2 = num * 6;
      Vector3[] verts = new Vector3[length1];
      Vector2[] uvs = new Vector2[length1];
      Vector3[] normals = new Vector3[length1];
      int[] tris = new int[length2];
      int startIdx = 0;
      for (int index = 0; index < this._sectors.Count; ++index)
      {
        Vector3 sectorPos = new Vector3(this._builder.tileSize * (float) (this._sectors[index].x - this.mapStartX), -(this._builder.tileSize * (float) (this._sectors[index].y - this.mapStartY)), 0.0f);
        this._builder.BuildSector(this._sectors[index].visibleLayers, ref sectorPos, ref verts, ref uvs, ref normals, ref tris, ref startIdx);
      }
      Mesh mesh = new Mesh();
      this.BuildPlaceholder(mesh);
      Vector3[] vector3Array1 = new Vector3[verts.Length + mesh.vertices.Length];
      Vector2[] vector2Array = new Vector2[uvs.Length + mesh.uv.Length];
      Vector3[] vector3Array2 = new Vector3[normals.Length + mesh.normals.Length];
      int[] numArray = new int[tris.Length + mesh.triangles.Length];
      verts.CopyTo((Array) vector3Array1, 0);
      mesh.vertices.CopyTo((Array) vector3Array1, verts.Length);
      uvs.CopyTo((Array) vector2Array, 0);
      mesh.uv.CopyTo((Array) vector2Array, uvs.Length);
      normals.CopyTo((Array) vector3Array2, 0);
      mesh.normals.CopyTo((Array) vector3Array2, normals.Length);
      tris.CopyTo((Array) numArray, 0);
      mesh.triangles.CopyTo((Array) numArray, tris.Length);
      this._mesh.Clear();
      this._mesh.vertices = vector3Array1;
      this._mesh.uv = vector2Array;
      this._mesh.normals = vector3Array2;
      this._mesh.triangles = numArray;
    }
  }
}
