﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.TileSpriteInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  [AdvancedInspector.AdvancedInspector]
  [Serializable]
  public class TileSpriteInfo
  {
    [SerializeField]
    private string _spriteName;
    [Inspect(1)]
    public int layerId;

    private TileSpriteInfo()
    {
    }

    public static TileSpriteInfo Create(ITileInfoProvider parent)
    {
      return new TileSpriteInfo() { parent = parent };
    }

    [Restrict("ValidAtlasSprites")]
    [Inspect(0)]
    public string spriteName
    {
      get
      {
        return this._spriteName;
      }
      set
      {
        this._spriteName = value;
      }
    }

    public ITileInfoProvider parent { get; set; }

    private IList ValidAtlasSprites()
    {
      if (this.parent != null && !((UnityEngine.Object) null == (UnityEngine.Object) this.parent.mapAtlas))
        return (IList) this.parent.mapAtlas.spriteList.Select<UISpriteData, string>((Func<UISpriteData, string>) (o => o.name)).ToList<string>();
      return (IList) new string[1]{ this._spriteName };
    }
  }
}
