﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.LayerLookup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace TheInfiniteBlack.Unity.Map
{
  public class LayerLookup
  {
    private readonly Dictionary<string, int> _layerDictionary = new Dictionary<string, int>(50);
    private readonly List<string> _orderedLayerList = new List<string>(50);
    private int _count;

    public int Count
    {
      get
      {
        return this._count;
      }
    }

    public int this[string layerName]
    {
      get
      {
        return this._layerDictionary[layerName];
      }
    }

    public string this[int layerIdx]
    {
      get
      {
        return this._orderedLayerList[layerIdx];
      }
    }

    public void PushLayer(string layerName)
    {
      this._layerDictionary.Add(layerName, this._count);
      this._orderedLayerList.Add(layerName);
      ++this._count;
    }
  }
}
