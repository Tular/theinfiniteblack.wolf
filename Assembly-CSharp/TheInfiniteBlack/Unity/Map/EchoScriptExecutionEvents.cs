﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.EchoScriptExecutionEvents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  public class EchoScriptExecutionEvents : MonoBehaviour
  {
    private void Awake()
    {
      Debug.Log((object) string.Format("{0}: TestComponentEvents.Awake() frame:{1} time:{2}", (object) this.name, (object) Time.frameCount, (object) RealTime.time));
    }

    private void Start()
    {
      Debug.Log((object) string.Format("{0}: TestComponentEvents.Start() frame:{1} time:{2}", (object) this.name, (object) Time.frameCount, (object) RealTime.time));
    }

    private void OnEnable()
    {
      Debug.Log((object) string.Format("{0}: TestComponentEvents.OnEnable() frame:{1} time:{2}", (object) this.name, (object) Time.frameCount, (object) RealTime.time));
    }

    private void OnDisable()
    {
      Debug.Log((object) string.Format("{0}: TestComponentEvents.OnDisable() frame:{1} time:{2}", (object) this.name, (object) Time.frameCount, (object) RealTime.time));
    }

    private void OnDestroy()
    {
      Debug.Log((object) string.Format("{0}: TestComponentEvents.OnDestroy() frame:{1} time:{2}", (object) this.name, (object) Time.frameCount, (object) RealTime.time));
    }
  }
}
