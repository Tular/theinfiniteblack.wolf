﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.MapLayerQuad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  public class MapLayerQuad
  {
    private Vector3[] verts;
    private Vector2[] uvs;
    private Vector3[] normals;
    private int[] tris;

    private void BuildQuad(UIAtlas atlas, float depth, float scale, string spriteName)
    {
      int width = atlas.spriteMaterial.mainTexture.width;
      int height = atlas.spriteMaterial.mainTexture.height;
      UISpriteData sprite = atlas.GetSprite(spriteName);
      int num1 = sprite.width + sprite.paddingLeft + sprite.paddingRight;
      int num2 = sprite.height + sprite.paddingTop + sprite.paddingBottom;
      float num3 = (float) num1 * scale;
      float num4 = (float) num2 * scale;
      float num5 = (float) (-(double) num3 * 0.5);
      float num6 = -num5;
      float num7 = -(num4 * 0.5f);
      float x1 = num5 + (float) sprite.paddingLeft * scale;
      float x2 = x1 + (float) sprite.width * scale;
      float y1 = num7 + (float) sprite.paddingBottom * scale;
      float y2 = y1 + (float) sprite.height * scale;
      int num8 = sprite.x + sprite.width;
      int num9 = sprite.y + sprite.height;
      Rect texCoords = NGUIMath.ConvertToTexCoords(new Rect((float) sprite.x, (float) sprite.y, (float) sprite.width, (float) sprite.height), width, height);
      this.uvs = new Vector2[4]
      {
        new Vector2(texCoords.xMin, texCoords.yMin),
        new Vector2(texCoords.xMax, texCoords.yMin),
        new Vector2(texCoords.xMin, texCoords.yMax),
        new Vector2(texCoords.xMax, texCoords.yMax)
      };
      this.verts = new Vector3[4]
      {
        new Vector3(x1, y1, depth),
        new Vector3(x2, y1, depth),
        new Vector3(x1, y2, depth),
        new Vector3(x2, y2, depth)
      };
      this.tris = new int[6]{ 2, 3, 0, 1, 0, 3 };
      this.normals = new Vector3[4]
      {
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward
      };
    }
  }
}
