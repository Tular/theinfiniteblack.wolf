﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SectorCountdownTimer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  [RequireComponent(typeof (UILabel))]
  public class SectorCountdownTimer : MonoBehaviour
  {
    private UILabel _label;

    public string text
    {
      get
      {
        return this._label.text;
      }
      set
      {
        this._label.text = value;
      }
    }

    private void Awake()
    {
      this._label = this.GetComponent<UILabel>();
      this._label.text = string.Empty;
    }

    private void OnSpawn()
    {
    }

    private void OnDespawn()
    {
      this._label.text = string.Empty;
    }
  }
}
