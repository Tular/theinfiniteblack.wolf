﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SectorGeometryBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  public class SectorGeometryBuilder : IMapNodeGeometryBuilder
  {
    private readonly Dictionary<string, SectorGeometryBuilder.LayerInfo> _layerLookup = new Dictionary<string, SectorGeometryBuilder.LayerInfo>();
    private readonly LayerLookup _layerIdxLookup = new LayerLookup();
    private readonly UIAtlas _atlas;
    private readonly int _spriteSize;

    public SectorGeometryBuilder(UIAtlas atlas, string[] layers, float nodeSize, int spriteSize)
    {
      this._atlas = atlas;
      this._spriteSize = spriteSize;
      this.tileSize = nodeSize;
      this.material = atlas.spriteMaterial;
      if (spriteSize == 0)
        throw new ArgumentException("Argument spriteSize must be > 0");
      for (int index = 0; index < layers.Length; ++index)
        this._layerIdxLookup.PushLayer(layers[index]);
    }

    public Material material { get; private set; }

    public float tileSize { get; private set; }

    public bool ContainsLayer(string layerName)
    {
      return this._layerLookup.ContainsKey(layerName);
    }

    public IMapNodeLayerState GetLayerStateInstance()
    {
      return (IMapNodeLayerState) new SectorLayerState(this._layerIdxLookup);
    }

    [DebuggerHidden]
    public IEnumerator CreateSectorGeometryCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SectorGeometryBuilder.\u003CCreateSectorGeometryCoroutine\u003Ec__Iterator24()
      {
        \u003C\u003Ef__this = this
      };
    }

    public void BuildSector(string[] visibleLayers, ref Vector3 sectorPos, ref Vector3[] verts, ref Vector2[] uvs, ref Vector3[] normals, ref int[] tris, ref int startIdx)
    {
      int index1 = startIdx * 4;
      int num1 = startIdx * 6;
      float num2 = 1f / 1000f;
      float z = 2f;
      for (int index2 = 0; index2 < visibleLayers.Length; ++index2)
      {
        SectorGeometryBuilder.LayerInfo layerInfo = this._layerLookup[visibleLayers[index2]];
        for (int index3 = 0; index3 < layerInfo.verts.Length; ++index3)
        {
          verts[index1 + index3] = layerInfo.verts[index3] + sectorPos + new Vector3(0.0f, 0.0f, z);
          z -= num2;
        }
        layerInfo.uvs.CopyTo((Array) uvs, index1);
        layerInfo.normals.CopyTo((Array) normals, index1);
        for (int index3 = 0; index3 < layerInfo.tris.Length; ++index3)
          tris[num1 + index3] = layerInfo.tris[index3] + index1;
        num1 += 6;
        index1 += 4;
      }
      startIdx = startIdx + visibleLayers.Length;
    }

    private void BuildLayer(float scale, UIAtlas atlas, string spriteName)
    {
      int width = atlas.spriteMaterial.mainTexture.width;
      int height = atlas.spriteMaterial.mainTexture.height;
      UISpriteData sprite = atlas.GetSprite(spriteName);
      int num1 = sprite.width + sprite.paddingLeft + sprite.paddingRight;
      int num2 = sprite.height + sprite.paddingTop + sprite.paddingBottom;
      float num3 = (float) num1 * scale;
      float num4 = (float) num2 * scale;
      float num5 = (float) (-(double) num3 * 0.5);
      float num6 = -num5;
      float num7 = -(num4 * 0.5f);
      float x1 = num5 + (float) sprite.paddingLeft * scale;
      float x2 = x1 + (float) sprite.width * scale;
      float y1 = num7 + (float) sprite.paddingBottom * scale;
      float y2 = y1 + (float) sprite.height * scale;
      int num8 = sprite.x + sprite.width;
      int num9 = sprite.y + sprite.height;
      Rect texCoords = NGUIMath.ConvertToTexCoords(new Rect((float) sprite.x, (float) sprite.y, (float) sprite.width, (float) sprite.height), width, height);
      Vector3[] v = new Vector3[4]
      {
        new Vector3(x1, y1, 0.0f),
        new Vector3(x2, y1, 0.0f),
        new Vector3(x1, y2, 0.0f),
        new Vector3(x2, y2, 0.0f)
      };
      Vector3[] n = new Vector3[4]
      {
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward
      };
      Vector2[] u = new Vector2[4]
      {
        new Vector2(texCoords.xMin, texCoords.yMin),
        new Vector2(texCoords.xMax, texCoords.yMin),
        new Vector2(texCoords.xMin, texCoords.yMax),
        new Vector2(texCoords.xMax, texCoords.yMax)
      };
      int[] t = new int[6]{ 2, 3, 0, 1, 0, 3 };
      this._layerLookup.Add(spriteName, new SectorGeometryBuilder.LayerInfo(v, u, n, t));
    }

    private class LayerInfo
    {
      public readonly Vector3[] verts;
      public readonly Vector2[] uvs;
      public readonly Vector3[] normals;
      public readonly int[] tris;

      public LayerInfo(Vector3[] v, Vector2[] u, Vector3[] n, int[] t)
      {
        this.verts = v;
        this.uvs = u;
        this.normals = n;
        this.tris = t;
      }
    }
  }
}
