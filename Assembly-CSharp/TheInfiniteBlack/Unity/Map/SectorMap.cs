﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SectorMap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using PathologicalGames;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  [RequireComponent(typeof (SectorMapController))]
  [AdvancedInspector.AdvancedInspector]
  public class SectorMap : PreInstantiatedSingleton<SectorMap>
  {
    [Inspect(4)]
    [Group("Map Config")]
    public float tileZ = 1f;
    [Group("Map Config")]
    [Inspect(7)]
    public bool initializeSectorsOnStart = true;
    [Inspect(2)]
    [Group("Player List Config")]
    public float playerListScale = 0.005f;
    [Inspect(2)]
    [Group("Timer Config")]
    public float countdownTimerScale = 0.005f;
    private readonly List<MapSection> _sections = new List<MapSection>(64);
    [Inspect(0)]
    [Group("Map Config", Expandable = false, Priority = 10)]
    public SectorMapController controller;
    [Group("Map Config")]
    [Inspect(1)]
    public GameObject mapRoot;
    [Group("Map Config")]
    [Inspect(2)]
    public int spriteSize;
    [Inspect(3)]
    [Group("Map Config")]
    public float tileSize;
    [Group("Map Config")]
    [Inspect(5)]
    public UIAtlas tileAtlas;
    [Group("Map Config")]
    [Inspect(6)]
    public string[] mapLayers;
    [Inspect(0)]
    [Group("Player List Config", Expandable = false, Priority = 20)]
    public Transform playerListPrefab;
    [Group("Player List Config")]
    [Inspect(1)]
    public SpawnPool playerListPool;
    [Group("Player List Config")]
    [Inspect(3)]
    public Vector3 playerListOffset;
    [Inspect(0)]
    [Group("Timer Config", Expandable = false, Priority = 30)]
    public Transform countdownTimerPrefab;
    [Group("Timer Config")]
    [Inspect(1)]
    public SpawnPool countdownTimerPool;
    [Group("Timer Config")]
    [Inspect(3)]
    public Vector3 countdownTimerOffset;
    [HideInInspector]
    public float totalLoadTime;
    [HideInInspector]
    public float loadProgress;
    private bool _riftSpace;
    private bool _isMapInitialized;
    private bool _isBuildingMap;
    private bool _mapupdateIsRunning;

    public bool riftSpace
    {
      get
      {
        return this._riftSpace;
      }
      set
      {
        if (value == this._riftSpace)
          return;
        this._riftSpace = value;
        if ((UnityEngine.Object) SkyboxManager.Instance == (UnityEngine.Object) null)
          return;
        SkyboxManager.Instance.UpdateUniverseBackground(this._riftSpace);
      }
    }

    public void ToggleUniverseView()
    {
      this.riftSpace = !this._riftSpace;
    }

    public void WorldCoordsToSectorCoords(Vector3 worldCoords, out int localX, out int localY)
    {
      Vector3 vector3 = this.transform.InverseTransformPoint(worldCoords);
      localX = Mathf.RoundToInt(vector3.x / this.tileSize);
      localY = Mathf.RoundToInt(-vector3.y / this.tileSize);
    }

    public Vector3 SectorCoordsToWorldCoords(int sectorX, int sectorY)
    {
      Vector3 mapCoords = this.SectorCoordsToMapCoords(sectorX, sectorY);
      return this.transform.TransformPoint(mapCoords.x, -mapCoords.y, mapCoords.z);
    }

    public Vector3 SectorCoordsToMapCoords(int sectorX, int sectorY)
    {
      return new Vector3((float) sectorX * this.tileSize, (float) sectorY * this.tileSize);
    }

    public void ReleasePlayerList(SectorPlayerList list)
    {
      this.playerListPool.Despawn(list.transform);
    }

    public SectorPlayerList BuildPlayerList(MapSector sector)
    {
      Vector3 worldCoords = this.SectorCoordsToWorldCoords(sector.x, sector.y);
      SpawnPool playerListPool = this.playerListPool;
      Transform playerListPrefab = this.playerListPrefab;
      Transform transform1 = playerListPool.transform;
      Transform transform2 = playerListPool.Spawn(playerListPrefab, worldCoords, Quaternion.identity, transform1);
      transform2.localPosition += this.playerListOffset;
      transform2.localScale = new Vector3(this.playerListScale, this.playerListScale, this.playerListScale);
      return transform2.GetComponent<SectorPlayerList>();
    }

    public SectorCountdownTimer AcquireCountdownTimer(MapSector sector)
    {
      Transform transform = this.countdownTimerPool.Spawn(this.countdownTimerPrefab, this.SectorCoordsToWorldCoords(sector.x, sector.y), Quaternion.identity, this.countdownTimerPool.transform);
      transform.localPosition += this.countdownTimerOffset;
      transform.localScale = new Vector3(this.countdownTimerScale, this.countdownTimerScale, this.countdownTimerScale);
      return transform.GetComponent<SectorCountdownTimer>();
    }

    public void ReleaseCountdownTimer(SectorCountdownTimer countdownTimer)
    {
      this.countdownTimerPool.Despawn(countdownTimer.transform);
    }

    private void RegisterEventHandlers()
    {
      TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
      TibProxy.Instance.onLocationChangedEvent += new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
      TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.TibProxy_onDisconnectEvent);
      TibProxy.Instance.onDisconnectEvent += new EventHandler<DisconnectEventArgs>(this.TibProxy_onDisconnectEvent);
    }

    private void UnregisterEventHandlers()
    {
      TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
      TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.TibProxy_onDisconnectEvent);
    }

    public bool isInitialized
    {
      get
      {
        return this._isMapInitialized;
      }
    }

    private void TibProxy_onLocationChangedEvent(object sender, LocationChangedEventArgs e)
    {
      if (this._sections == null || this._isMapInitialized || this._isMapInitialized)
        return;
      if (!this.controller.isCameraActive)
        this.controller.isCameraActive = true;
      this._isMapInitialized = true;
      this.controller.SnapToSector(TibProxy.gameState.MyLoc);
      this._riftSpace = !TibProxy.gameState.MyLoc.Rift;
      this.controller.TranslateToPlayerSector(false);
    }

    private void TibProxy_onDisconnectEvent(object sender, DisconnectEventArgs e)
    {
      this._mapupdateIsRunning = false;
      this._isMapInitialized = false;
      this.controller.isCameraActive = false;
      if (this._sections == null)
        return;
      for (int index = 0; index < this._sections.Count; ++index)
        this._sections[index].ResetState();
    }

    private void Start()
    {
      this.controller = this.GetComponent<SectorMapController>();
      this._isMapInitialized = false;
      this.loadProgress = 0.0f;
      this.totalLoadTime = 0.0f;
      this.riftSpace = false;
      this.controller.isCameraActive = false;
      if (!this.initializeSectorsOnStart)
        return;
      this.StartCoroutine(this.BuildSectorsCoroutine());
    }

    private void OnEnable()
    {
      this.StopAllCoroutines();
      this.RegisterEventHandlers();
      this.StartCoroutine(this.UpdateMapSectionsCoroutine());
    }

    private void OnDisable()
    {
      this.UnregisterEventHandlers();
      this._mapupdateIsRunning = false;
    }

    [DebuggerHidden]
    private IEnumerator BuildSectorsCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SectorMap.\u003CBuildSectorsCoroutine\u003Ec__Iterator26()
      {
        \u003C\u003Ef__this = this
      };
    }

    [DebuggerHidden]
    private IEnumerator UpdateMapSectionsCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SectorMap.\u003CUpdateMapSectionsCoroutine\u003Ec__Iterator27()
      {
        \u003C\u003Ef__this = this
      };
    }
  }
}
