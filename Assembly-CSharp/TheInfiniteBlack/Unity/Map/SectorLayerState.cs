﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SectorLayerState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

namespace TheInfiniteBlack.Unity.Map
{
  public class SectorLayerState : IMapNodeLayerState
  {
    private readonly bool[] _curState;
    private readonly bool[] _prevState;
    private readonly LayerLookup _layerLookup;

    public SectorLayerState(LayerLookup lookup)
    {
      this.lastChangedId = int.MinValue;
      this._layerLookup = lookup;
      this._curState = new bool[lookup.Count];
      this._prevState = new bool[lookup.Count];
      this.ResetState();
    }

    public int lastChangedId { get; private set; }

    public int layerCount { get; private set; }

    public void PushLayer(string layerName)
    {
      this._curState[this._layerLookup[layerName]] = true;
    }

    public string[] GetLayerStack()
    {
      string[] strArray = new string[this.layerCount];
      int index1 = 0;
      for (int index2 = 0; index2 < this._prevState.Length; ++index2)
      {
        if (this._prevState[index2])
        {
          strArray[index1] = this._layerLookup[index2];
          ++index1;
        }
      }
      return strArray;
    }

    public bool CommitState()
    {
      bool flag = false;
      this.layerCount = 0;
      for (int index = 0; index < this._layerLookup.Count; ++index)
      {
        if (this._curState[index])
          ++this.layerCount;
        if (this._curState[index] != this._prevState[index])
        {
          flag = true;
          this._prevState[index] = this._curState[index];
        }
        this._curState[index] = false;
      }
      if (flag)
        this.SetDirty();
      return flag;
    }

    public void SetDirty()
    {
      ++this.lastChangedId;
    }

    public void ResetState()
    {
      this.lastChangedId = int.MinValue;
      this.layerCount = 0;
      for (int index = 0; index < this._layerLookup.Count; ++index)
      {
        this._curState[index] = false;
        this._prevState[index] = false;
      }
      this.SetDirty();
    }
  }
}
