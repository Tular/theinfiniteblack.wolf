﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SectorPlayerList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  [RequireComponent(typeof (UILabel))]
  public class SectorPlayerList : MonoBehaviour
  {
    public int maxLinesPerRelation = 4;
    private readonly HashSet<ClientPlayer> _friendPlayers = new HashSet<ClientPlayer>();
    private readonly HashSet<ClientPlayer> _neutralPlayers = new HashSet<ClientPlayer>();
    private readonly HashSet<ClientPlayer> _enemyPlayers = new HashSet<ClientPlayer>();
    public bool debug;
    public UICamera mapCamera;
    public Color friend;
    public Color neutral;
    public Color enemy;
    private UILabel _label;
    private string _text;
    private string _neutralColor;
    private string _enemyColor;
    private string _friendColor;

    public int depth
    {
      get
      {
        return this._label.depth;
      }
      set
      {
        this._label.depth = value;
      }
    }

    public float alpha
    {
      get
      {
        return this._label.alpha;
      }
      set
      {
        this._label.alpha = value;
      }
    }

    public void UpdatePlayerNames(IEnumerable<ClientPlayer> players)
    {
      this._label.text = string.Empty;
      ClientPlayer[] array = players.ToArray<ClientPlayer>();
      this._friendPlayers.IntersectWith((IEnumerable<ClientPlayer>) array);
      this._neutralPlayers.IntersectWith((IEnumerable<ClientPlayer>) array);
      this._enemyPlayers.IntersectWith((IEnumerable<ClientPlayer>) array);
      for (int index = 0; index < array.Length; ++index)
      {
        ClientPlayer clientPlayer = array[index];
        switch (clientPlayer.Relation)
        {
          case RelationType.NEUTRAL:
            if (this._friendPlayers.Contains(clientPlayer))
              this._friendPlayers.Remove(clientPlayer);
            if (!this._neutralPlayers.Contains(clientPlayer))
              this._neutralPlayers.Add(clientPlayer);
            if (this._enemyPlayers.Contains(clientPlayer))
            {
              this._enemyPlayers.Remove(clientPlayer);
              break;
            }
            break;
          case RelationType.FRIEND:
            if (!this._friendPlayers.Contains(clientPlayer))
              this._friendPlayers.Add(clientPlayer);
            if (this._neutralPlayers.Contains(clientPlayer))
              this._neutralPlayers.Remove(clientPlayer);
            if (this._enemyPlayers.Contains(clientPlayer))
            {
              this._enemyPlayers.Remove(clientPlayer);
              break;
            }
            break;
          case RelationType.ENEMY:
            if (this._friendPlayers.Contains(clientPlayer))
              this._friendPlayers.Remove(clientPlayer);
            if (this._neutralPlayers.Contains(clientPlayer))
              this._neutralPlayers.Remove(clientPlayer);
            if (!this._enemyPlayers.Contains(clientPlayer))
            {
              this._enemyPlayers.Add(clientPlayer);
              break;
            }
            break;
        }
      }
      StringBuilder stringBuilder = new StringBuilder(100);
      int num1 = 0;
      using (HashSet<ClientPlayer>.Enumerator enumerator = this._friendPlayers.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ClientPlayer current = enumerator.Current;
          if (num1 < this.maxLinesPerRelation)
          {
            stringBuilder.AppendFormat("[{0}]{1}", (object) this._friendColor, (object) current.Name);
            ++num1;
            if (num1 < this.maxLinesPerRelation)
              stringBuilder.AppendLine();
          }
          else
          {
            int num2 = this._friendPlayers.Count - num1;
            if (num2 > 0)
            {
              stringBuilder.AppendFormat("\n... {0} more", (object) num2);
              if (this._enemyPlayers.Count <= 0)
              {
                if (this._neutralPlayers.Count <= 0)
                  break;
              }
              stringBuilder.AppendLine();
              break;
            }
            break;
          }
        }
      }
      using (HashSet<ClientPlayer>.Enumerator enumerator = this._enemyPlayers.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ClientPlayer current = enumerator.Current;
          if (num1 < this.maxLinesPerRelation)
          {
            stringBuilder.AppendFormat("[{0}]{1}", (object) this._enemyColor, (object) current.Name);
            ++num1;
            if (num1 < this.maxLinesPerRelation)
              stringBuilder.AppendLine();
          }
          else
          {
            int num2 = this._enemyPlayers.Count - num1;
            if (num2 > 0)
              stringBuilder.AppendFormat("\n... {0} more", (object) num2);
            if (this._neutralPlayers.Count > 0)
            {
              stringBuilder.AppendLine();
              break;
            }
            break;
          }
        }
      }
      using (HashSet<ClientPlayer>.Enumerator enumerator = this._neutralPlayers.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ClientPlayer current = enumerator.Current;
          if (num1 < this.maxLinesPerRelation)
          {
            stringBuilder.AppendFormat("[{0}]{1}", (object) this._neutralColor, (object) current.Name);
            ++num1;
            if (num1 < this.maxLinesPerRelation)
              stringBuilder.AppendLine();
          }
          else
          {
            int num2 = this._neutralPlayers.Count - num1;
            if (num2 > 0)
            {
              stringBuilder.AppendLine();
              stringBuilder.AppendFormat("\n... {0} more", (object) num2);
              break;
            }
            break;
          }
        }
      }
      this._label.text = stringBuilder.ToString();
    }

    private void OnEnable()
    {
      if (string.IsNullOrEmpty(this._text))
        return;
      this._label.text = this._text;
    }

    private void Awake()
    {
      this._neutralColor = NGUIText.EncodeColor(this.neutral);
      this._enemyColor = NGUIText.EncodeColor(this.enemy);
      this._friendColor = NGUIText.EncodeColor(this.friend);
      this._label = this.GetComponent<UILabel>();
    }

    private void OnSpawned()
    {
      this._text = string.Empty;
      this._label.text = string.Empty;
      this._friendPlayers.Clear();
      this._neutralPlayers.Clear();
      this._enemyPlayers.Clear();
    }

    private void OnDespawned()
    {
      this._text = string.Empty;
      this._label.text = string.Empty;
      this._friendPlayers.Clear();
      this._neutralPlayers.Clear();
      this._enemyPlayers.Clear();
    }
  }
}
