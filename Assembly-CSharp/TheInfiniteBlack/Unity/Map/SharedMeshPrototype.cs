﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SharedMeshPrototype
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  [AdvancedInspector.AdvancedInspector]
  public class SharedMeshPrototype : MonoBehaviour, IDataChanged, ISerializationCallbackReceiver, ITileInfoProvider
  {
    [SerializeField]
    private UIAtlas _mapAtlas;
    [Inspect(1, MethodName = "IsAtlasSet")]
    [Constructor("TileSpriteInfoConstructor")]
    public List<TileSpriteInfo> tileInfos;

    public event GenericEventHandler OnDataChanged;

    [Inspect(0)]
    public UIAtlas mapAtlas
    {
      get
      {
        return this._mapAtlas;
      }
      set
      {
        this._mapAtlas = value;
      }
    }

    private object TileSpriteInfoConstructor()
    {
      return (object) TileSpriteInfo.Create((ITileInfoProvider) this);
    }

    private bool IsAtlasSet()
    {
      return (Object) this._mapAtlas != (Object) null;
    }

    public void DataChanged()
    {
    }

    private void FireOnDataChanged()
    {
      if (this.OnDataChanged == null)
        return;
      this.OnDataChanged();
    }

    public void OnBeforeSerialize()
    {
    }

    public void OnAfterDeserialize()
    {
      for (int index = 0; index < this.tileInfos.Count; ++index)
        this.tileInfos[index].parent = (ITileInfoProvider) this;
    }
  }
}
