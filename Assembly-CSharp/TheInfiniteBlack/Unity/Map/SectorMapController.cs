﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SectorMapController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  public class SectorMapController : MonoBehaviour
  {
    public float recenterDelay = 3f;
    public float recenterSnapFactor = 1E-05f;
    public float scrollWheelFactor = 2f;
    public float springStrength = 10f;
    public float zoomSpeed = 10f;
    public float startingZoom = 3f;
    public float maxZoom = 50f;
    public float minZoom = 0.5f;
    public float dragSpeed = 10f;
    public float dragMomentum = 35f;
    public float dampenStrength = 9f;
    private bool _mapControlEventsEnabled = true;
    public FallthroughNGUIEventHandler scrollWhelHandler;
    public Camera mapCamera;
    public SectorMap map;
    private float _recenterTime;
    private SpringMapPosition _mapSpringPosition;
    private bool _cameraWasActive;
    private int _lastMouseWheelFrame;
    private bool _swipeEnding;
    private bool _swipeStarted;
    private Vector3 _dragMomentum;
    private Vector3 _targetPosition;
    private Vector3 _lastTouchWorldPoint;
    private float _targetZoom;
    private bool _zoomStarted;
    private bool _coroutineStarted;

    public event EventHandler onScrollBegin;

    public event EventHandler onScrollEnd;

    public bool isCameraActive
    {
      get
      {
        return this.mapCamera.enabled;
      }
      set
      {
        this.mapCamera.enabled = value;
      }
    }

    public bool isZooming
    {
      get
      {
        return this._zoomStarted;
      }
    }

    public int mapCamLodLevel
    {
      get
      {
        if ((double) this.mapCamera.orthographicSize > 25.0 && (double) this.mapCamera.orthographicSize <= 50.0)
          return 1;
        return (double) this.mapCamera.orthographicSize > 50.0 ? 2 : 0;
      }
    }

    public bool mapControlEventsEnabled
    {
      get
      {
        return this._mapControlEventsEnabled;
      }
      set
      {
        if (value == this._mapControlEventsEnabled)
          return;
        this._mapControlEventsEnabled = value;
        if (value)
          this.StartCoroutine(this.EnableEasyTouchCoroutine());
        else
          EasyTouch.SetEnabled(false);
      }
    }

    private void DisableNGUIEvents()
    {
      UnityEngine.Debug.Log((object) nameof (DisableNGUIEvents));
      for (int index = 0; index < EasyTouch.instance.nGUICameras.Count; ++index)
      {
        UICamera component = EasyTouch.instance.nGUICameras[index].GetComponent<UICamera>();
        if ((bool) ((UnityEngine.Object) component))
          component.enabled = false;
      }
    }

    private void EnableNGUIEvents()
    {
      UnityEngine.Debug.Log((object) nameof (EnableNGUIEvents));
      for (int index = 0; index < EasyTouch.instance.nGUICameras.Count; ++index)
      {
        UICamera component = EasyTouch.instance.nGUICameras[index].GetComponent<UICamera>();
        component.GetComponent<Camera>().enabled = true;
        if ((bool) ((UnityEngine.Object) component))
          component.enabled = true;
      }
    }

    public void SnapToSector(Sector s)
    {
      if (this._swipeStarted)
        return;
      this.SnapToSector(s.X, s.Y);
    }

    protected void SnapToSector(int sectorX, int sectorY)
    {
      if (this._swipeStarted)
        return;
      Vector3 worldCoords = this.map.SectorCoordsToWorldCoords(sectorX, sectorY);
      this.mapCamera.transform.position = new Vector3(worldCoords.x, worldCoords.y, this.mapCamera.transform.position.z);
    }

    public void SnapToEarth()
    {
      this.SnapToSector(39, 39);
    }

    public void ScrollToEarth()
    {
      this.TranslateToSector(39, 39);
    }

    public void TranslateToSector(Sector s)
    {
      if (this._swipeStarted || this._swipeEnding || this._zoomStarted)
        return;
      this._mapSpringPosition.target = this.map.SectorCoordsToWorldCoords(s.X, s.Y);
      this._mapSpringPosition.enabled = true;
    }

    public void TranslateToPlayerSector(bool cancelIfTouchActive)
    {
      this._swipeStarted = false;
      this._swipeEnding = false;
      this._zoomStarted = false;
      PreInstantiatedSingleton<SectorMap>.Instance.riftSpace = TibProxy.gameState.MyLoc.Rift;
      this.TranslateToSector(TibProxy.gameState.MyLoc);
    }

    public void ZoomIn(float sencitivity)
    {
      this._zoomStarted = true;
      this._targetZoom = this.mapCamera.orthographicSize - this.mapCamera.orthographicSize * 0.1f * sencitivity;
      if ((double) this._targetZoom >= (double) this.minZoom)
        return;
      this._targetZoom = this.minZoom;
    }

    public void ZoomEnd()
    {
      this._zoomStarted = false;
      this._targetZoom = this.mapCamera.orthographicSize;
    }

    public void ZoomOut(float sencitivity)
    {
      this._zoomStarted = true;
      this._targetZoom = this.mapCamera.orthographicSize + this.mapCamera.orthographicSize * 0.1f * sencitivity;
      if ((double) this._targetZoom <= (double) this.maxZoom)
        return;
      this._targetZoom = this.maxZoom;
    }

    protected void TranslateToSector(int sectorX, int sectorY)
    {
      if (this._swipeStarted || this._swipeEnding || this._zoomStarted)
        return;
      this._mapSpringPosition.target = this.map.SectorCoordsToWorldCoords(sectorX, sectorY);
      this._mapSpringPosition.enabled = true;
    }

    private void FallthroughNGUIEventHandler_onMouseScrollDelegate(object sender, MouseScrollWheelEventArgs e)
    {
      this._mapSpringPosition.enabled = false;
      this._lastMouseWheelFrame = Time.frameCount;
      float num = e.delta * this.scrollWheelFactor;
      if ((double) num <= 0.0 && (double) num >= 0.0)
        return;
      this.mapCamera.orthographicSize = Mathf.Clamp(this.mapCamera.orthographicSize - num, this.minZoom, this.maxZoom);
    }

    private void TibProxy_onLocationChangedEvent(object sender, LocationChangedEventArgs e)
    {
      if ((UnityEngine.Object) this.mapCamera == (UnityEngine.Object) null || !this.mapCamera.enabled || ((double) this._recenterTime > (double) RealTime.time || EasyTouch.GetTouchCount() > 0) || (this._swipeStarted || this._swipeEnding || (this._zoomStarted || this._lastMouseWheelFrame == Time.frameCount)))
        return;
      PreInstantiatedSingleton<SectorMap>.Instance.riftSpace = TibProxy.gameState.MyLoc.Rift;
      this.TranslateToSector(TibProxy.gameState.MyLoc);
    }

    private void RegisterTouchHandlers()
    {
      EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.EasyTouch_On_TouchStart);
      EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.EasyTouch_On_TouchStart);
      EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.EasyTouch_On_TouchUp);
      EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.EasyTouch_On_TouchUp);
      EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
      EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
      EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.EasyTouch_On_Swipe);
      EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.EasyTouch_On_Swipe);
      EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
      EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
      EasyTouch.On_SimpleTap -= new EasyTouch.SimpleTapHandler(this.EasyTouch_On_SimpleTap);
      EasyTouch.On_SimpleTap += new EasyTouch.SimpleTapHandler(this.EasyTouch_On_SimpleTap);
      EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_On_TouchStart2Fingers);
      EasyTouch.On_TouchStart2Fingers += new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_On_TouchStart2Fingers);
      EasyTouch.On_TouchUp2Fingers -= new EasyTouch.TouchUp2FingersHandler(this.EasyTouch_On_TouchUp2Fingers);
      EasyTouch.On_TouchUp2Fingers += new EasyTouch.TouchUp2FingersHandler(this.EasyTouch_On_TouchUp2Fingers);
      EasyTouch.On_PinchIn -= new EasyTouch.PinchInHandler(this.EasyTouch_On_PinchIn);
      EasyTouch.On_PinchIn += new EasyTouch.PinchInHandler(this.EasyTouch_On_PinchIn);
      EasyTouch.On_PinchOut -= new EasyTouch.PinchOutHandler(this.EasyTouch_On_PinchOut);
      EasyTouch.On_PinchOut += new EasyTouch.PinchOutHandler(this.EasyTouch_On_PinchOut);
      EasyTouch.On_PinchEnd -= new EasyTouch.PinchEndHandler(this.EasyTouch_On_PinchEnd);
      EasyTouch.On_PinchEnd += new EasyTouch.PinchEndHandler(this.EasyTouch_On_PinchEnd);
      FallthroughNGUIEventHandler.onMouseScrollEventHandler = new EventHandler<MouseScrollWheelEventArgs>(this.FallthroughNGUIEventHandler_onMouseScrollDelegate);
    }

    private void UnregisterTouchHandlers()
    {
      EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.EasyTouch_On_TouchStart);
      EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.EasyTouch_On_TouchUp);
      EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
      EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.EasyTouch_On_Swipe);
      EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
      EasyTouch.On_SimpleTap -= new EasyTouch.SimpleTapHandler(this.EasyTouch_On_SimpleTap);
      EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_On_TouchStart2Fingers);
      EasyTouch.On_PinchIn -= new EasyTouch.PinchInHandler(this.EasyTouch_On_PinchIn);
      EasyTouch.On_PinchOut -= new EasyTouch.PinchOutHandler(this.EasyTouch_On_PinchOut);
      EasyTouch.On_PinchEnd -= new EasyTouch.PinchEndHandler(this.EasyTouch_On_PinchEnd);
      FallthroughNGUIEventHandler.onMouseScrollEventHandler = (EventHandler<MouseScrollWheelEventArgs>) null;
    }

    private void EasyTouch_On_TouchStart(Gesture gesture)
    {
      UnityEngine.Debug.Log((object) ("SectorMapController - TouchStart: " + (object) gesture.fingerIndex));
      if (gesture.fingerIndex != 0 || this.onScrollBegin == null)
        return;
      this.onScrollBegin((object) this, new EventArgs());
    }

    private void EasyTouch_On_TouchUp(Gesture gesture)
    {
      UnityEngine.Debug.Log((object) "SectorMapController - TouchUp");
      if (gesture.touchCount > 1)
        return;
      this.EnableNGUIEvents();
      if (this.onScrollEnd == null)
        return;
      this.onScrollEnd((object) this, new EventArgs());
    }

    private void EasyTouch_On_TouchUp2Fingers(Gesture gesture)
    {
      UnityEngine.Debug.Log((object) "SectorMapController - TouchUp2Fingers");
      this.EnableNGUIEvents();
    }

    private void EasyTouch_On_SwipeStart(Gesture gesture)
    {
      if (gesture.fingerIndex != 0 || gesture.touchCount > 1)
        return;
      this._swipeStarted = true;
      this._swipeEnding = false;
      this._dragMomentum = Vector3.zero;
      this._targetPosition = this.mapCamera.transform.position;
      float num = this.mapCamera.orthographicSize / Camera.main.orthographicSize;
      this._lastTouchWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(gesture.position.x * num, gesture.position.y * num, 0.0f));
    }

    private void EasyTouch_On_Swipe(Gesture gesture)
    {
      if (gesture.fingerIndex != 0 || gesture.touchCount > 1 || !this._swipeStarted)
        return;
      float num = this.mapCamera.orthographicSize / Camera.main.orthographicSize;
      Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(gesture.position.x * num, gesture.position.y * num, 0.0f));
      Vector3 vector3 = this._lastTouchWorldPoint - worldPoint;
      this._lastTouchWorldPoint = worldPoint;
      this._targetPosition = this._targetPosition + vector3;
      this._dragMomentum = Vector3.Lerp(this._dragMomentum, this._dragMomentum + vector3 * (0.01f * this.dragMomentum), 0.67f);
    }

    private void EasyTouch_On_SwipeEnd(Gesture gesture)
    {
      if (gesture.fingerIndex != 0 || gesture.touchCount > 1 || !this._swipeStarted)
        return;
      this._swipeStarted = false;
      this._swipeEnding = true;
    }

    private void EasyTouch_On_SimpleTap(Gesture gesture)
    {
      try
      {
        if (TibProxy.gameState == null || TibProxy.gameState.Course == null)
          return;
        int localX;
        int localY;
        this.map.WorldCoordsToSectorCoords(this.mapCamera.ScreenToWorldPoint((Vector3) gesture.position), out localX, out localY);
        TibProxy.gameState.Course.Set(localX + 1, localY + 1);
      }
      catch (Exception ex)
      {
        UnityEngine.Debug.Log((object) string.Format("EasyTouch_On_SimpleTap Exception: {0}\n{1}", (object) ex.Message, (object) ex.StackTrace));
      }
    }

    private void EasyTouch_On_TouchStart2Fingers(Gesture gesture)
    {
      if (UICamera.currentTouch != null && (UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) null)
        return;
      UnityEngine.Debug.Log((object) "SectorMapController - TouchStart2Fingers");
      this._targetPosition = this.mapCamera.transform.position;
      this._swipeStarted = false;
      this._swipeEnding = false;
      this._dragMomentum = Vector3.zero;
      this._targetZoom = this.mapCamera.orthographicSize;
      this.DisableNGUIEvents();
    }

    private void EasyTouch_On_PinchIn(Gesture gesture)
    {
      this._zoomStarted = true;
      this._targetZoom += (float) ((double) gesture.deltaPinch * (double) this.mapCamera.orthographicSize * 0.00999999977648258);
      if ((double) this._targetZoom <= (double) this.maxZoom)
        return;
      this._targetZoom = this.maxZoom;
    }

    private void EasyTouch_On_PinchOut(Gesture gesture)
    {
      this._zoomStarted = true;
      this._targetZoom -= (float) ((double) gesture.deltaPinch * (double) this.mapCamera.orthographicSize * 0.00999999977648258);
      if ((double) this._targetZoom >= (double) this.minZoom)
        return;
      this._targetZoom = this.minZoom;
    }

    private void EasyTouch_On_PinchEnd(Gesture gesture)
    {
      this.EnableNGUIEvents();
      this._zoomStarted = false;
      this._targetZoom = this.mapCamera.orthographicSize;
    }

    private void OnEnable()
    {
      TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
      TibProxy.Instance.onLocationChangedEvent += new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
      this.mapCamera.orthographicSize = this.startingZoom;
      if (!(bool) ((UnityEngine.Object) this._mapSpringPosition))
        this._mapSpringPosition = this.mapCamera.gameObject.AddComponent<SpringMapPosition>();
      this._mapSpringPosition.strength = this.springStrength;
      this._mapSpringPosition.enabled = false;
      this._mapSpringPosition.worldSpace = true;
      this._mapSpringPosition.ignoreTimeScale = true;
      this._mapSpringPosition.snapThresholdFactor = this.recenterSnapFactor;
      FallthroughNGUIEventHandler.onMouseScrollEventHandler = new EventHandler<MouseScrollWheelEventArgs>(this.FallthroughNGUIEventHandler_onMouseScrollDelegate);
      this.RegisterTouchHandlers();
    }

    private void OnDisable()
    {
      TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
      if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<KeyboardMapController>.Instance)
        PreInstantiatedSingleton<KeyboardMapController>.Instance.isEnabled = false;
      this._mapSpringPosition.enabled = false;
      FallthroughNGUIEventHandler.onMouseScrollEventHandler = (EventHandler<MouseScrollWheelEventArgs>) null;
      this.UnregisterTouchHandlers();
    }

    private void Update()
    {
      if (!this.isCameraActive)
      {
        this._cameraWasActive = false;
      }
      else
      {
        if (!this._cameraWasActive)
          this._cameraWasActive = true;
        if (EasyTouch.GetTouchCount() > 0 && (this._swipeStarted || this._swipeEnding || (this._zoomStarted || this._lastMouseWheelFrame == Time.frameCount)))
        {
          if (this._mapSpringPosition.enabled)
            this._mapSpringPosition.enabled = false;
          this._recenterTime = RealTime.time + this.recenterDelay;
        }
        else if ((double) this._recenterTime > 0.0 && (double) RealTime.time >= (double) this._recenterTime)
        {
          if (PreInstantiatedSingleton<SectorMap>.Instance.riftSpace != TibProxy.gameState.MyLoc.Rift)
            PreInstantiatedSingleton<SectorMap>.Instance.ToggleUniverseView();
          this._dragMomentum = Vector3.zero;
          this._swipeEnding = false;
          this._swipeStarted = false;
          this._zoomStarted = false;
          this.TranslateToSector(TibProxy.gameState.MyLoc);
          this._recenterTime = float.MinValue;
        }
        if (PreInstantiatedSingleton<SectorMap>.Instance.riftSpace == TibProxy.gameState.MyLoc.Rift || (double) this._recenterTime > (double) RealTime.time)
          return;
        this._recenterTime = RealTime.time + this.recenterDelay;
      }
    }

    private void LateUpdate()
    {
      float deltaTime = RealTime.deltaTime;
      if (this._zoomStarted)
      {
        this.mapCamera.orthographicSize = NGUIMath.SpringLerp(this.mapCamera.orthographicSize, this._targetZoom, this.zoomSpeed, deltaTime);
        if ((double) Mathf.Abs(this.mapCamera.orthographicSize - this._targetZoom) > 0.00999999977648258)
          return;
        this.mapCamera.orthographicSize = this._targetZoom;
        this._zoomStarted = false;
      }
      else if (this._swipeStarted)
      {
        this.mapCamera.transform.position = NGUIMath.SpringLerp(this.mapCamera.transform.position, this._targetPosition, this.dragSpeed, deltaTime);
        NGUIMath.SpringDampen(ref this._dragMomentum, this.dampenStrength * 2f, deltaTime);
      }
      else
      {
        if (!this._swipeEnding)
          return;
        if ((double) this._dragMomentum.magnitude > 1.0 / 1000.0)
        {
          this._targetPosition = this._targetPosition + NGUIMath.SpringDampen(ref this._dragMomentum, this.dampenStrength, deltaTime);
          this.mapCamera.transform.position = NGUIMath.SpringLerp(this.mapCamera.transform.position, this._targetPosition, this.dragSpeed, deltaTime);
        }
        else
        {
          this._dragMomentum = Vector3.zero;
          this._swipeEnding = false;
        }
      }
    }

    [DebuggerHidden]
    private IEnumerator EnableEasyTouchCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SectorMapController.\u003CEnableEasyTouchCoroutine\u003Ec__Iterator28()
      {
        \u003C\u003Ef__this = this
      };
    }
  }
}
