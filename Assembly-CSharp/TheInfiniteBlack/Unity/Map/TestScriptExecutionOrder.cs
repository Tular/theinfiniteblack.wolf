﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.TestScriptExecutionOrder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  public class TestScriptExecutionOrder : MonoBehaviour
  {
    public Transform childPrefab;
    private GameObject _child;
    private EchoScriptExecutionEvents _childComponent;

    [ContextMenu("Reset Test")]
    public void ResetTest()
    {
      this._childComponent = (EchoScriptExecutionEvents) null;
      if (!(bool) ((Object) this._child))
        return;
      Object.Destroy((Object) this._child);
    }

    [ContextMenu("Instantiate Prefab")]
    public void InstantiateFromPrefab()
    {
      GameObject gameObject = this.gameObject;
      this._child = (GameObject) Object.Instantiate((Object) this.childPrefab, Vector3.zero, Quaternion.identity);
      this._child.name = "prefab child";
      Transform transform = this._child.transform;
      transform.parent = gameObject.transform;
      transform.localPosition = Vector3.zero;
      transform.localRotation = Quaternion.identity;
      transform.localScale = Vector3.one;
      this._child.layer = gameObject.layer;
    }

    [ContextMenu("Instantiate Empty")]
    public void InstantiateEmptyGameObject()
    {
      GameObject gameObject = this.gameObject;
      this._child = new GameObject("empty child");
      Transform transform = this._child.transform;
      transform.parent = gameObject.transform;
      transform.localPosition = Vector3.zero;
      transform.localRotation = Quaternion.identity;
      transform.localScale = Vector3.one;
      this._child.layer = gameObject.layer;
    }

    [ContextMenu("Add Component")]
    public void AddEchoComponent()
    {
      if ((Object) this._child == (Object) null)
        this.InstantiateEmptyGameObject();
      this._childComponent = this._child.AddComponent<EchoScriptExecutionEvents>();
    }

    [ContextMenu("Enable GameObject")]
    public void EnableGameObject()
    {
      this._child.gameObject.SetActive(true);
    }

    [ContextMenu("Enable Component")]
    public void EnableComponent()
    {
      this._childComponent.enabled = true;
    }

    [ContextMenu("Disable GameObject")]
    public void DisableGameObject()
    {
      this._child.gameObject.SetActive(false);
    }

    [ContextMenu("Disable Component")]
    public void DisableComponent()
    {
      this._childComponent.enabled = false;
    }
  }
}
