﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Map.SpringMapPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Map
{
  public class SpringMapPosition : MonoBehaviour
  {
    public Vector3 target = Vector3.zero;
    public float strength = 10f;
    public float snapThresholdFactor = 1f;
    public static SpringMapPosition current;
    public bool worldSpace;
    public bool ignoreTimeScale;
    private Transform mTrans;
    private float mThreshold;

    private void Start()
    {
      this.mTrans = this.transform;
    }

    private void Update()
    {
      float deltaTime = !this.ignoreTimeScale ? Time.deltaTime : RealTime.deltaTime;
      if (this.worldSpace)
      {
        if ((double) this.mThreshold == 0.0)
          this.mThreshold = (this.target - this.mTrans.position).sqrMagnitude * (1f / 1000f) * this.snapThresholdFactor;
        this.mTrans.position = NGUIMath.SpringLerp(this.mTrans.position, this.target, this.strength, deltaTime);
        if ((double) this.mThreshold < (double) (this.target - this.mTrans.position).sqrMagnitude)
          return;
        this.mTrans.position = this.target;
        this.enabled = false;
      }
      else
      {
        if ((double) this.mThreshold == 0.0)
          this.mThreshold = (this.target - this.mTrans.localPosition).sqrMagnitude * 1E-05f * this.snapThresholdFactor;
        this.mTrans.localPosition = NGUIMath.SpringLerp(this.mTrans.localPosition, this.target, this.strength, deltaTime);
        if ((double) this.mThreshold < (double) (this.target - this.mTrans.localPosition).sqrMagnitude)
          return;
        this.mTrans.localPosition = this.target;
        this.enabled = false;
      }
    }
  }
}
