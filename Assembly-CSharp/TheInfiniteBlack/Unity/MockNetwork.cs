﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockNetwork
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Net.Sockets;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Network;
using TheInfiniteBlack.Library.Network.Commands;

namespace TheInfiniteBlack.Unity
{
  public class MockNetwork : INetwork
  {
    public byte[] IV { get; set; }

    public bool Connected
    {
      get
      {
        return true;
      }
    }

    public int Ping { get; private set; }

    public void TryDoAlive(IGameState state)
    {
    }

    public void GotAlive(IGameState state)
    {
    }

    public void Connect(Socket socket)
    {
    }

    public void Disconnect(string reason)
    {
    }

    public void Send(INetworkData data)
    {
    }

    public Command GetNextCommand()
    {
      return (Command) null;
    }
  }
}
