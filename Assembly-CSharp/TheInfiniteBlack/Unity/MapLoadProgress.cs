﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MapLoadProgress
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity.Map;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [RequireComponent(typeof (UISlider))]
  public class MapLoadProgress : MonoBehaviour
  {
    public SectorMap map;
    public UIPlayTween completionTweener;
    public UILabel thumbLabel;
    public BoxCollider screenTouchCollider;
    private UISlider _progressBar;
    private float _endThumbX;
    private float _startThumbX;
    private float _startThumbY;

    private void Awake()
    {
      if ((bool) ((Object) this.screenTouchCollider))
        this.screenTouchCollider.enabled = false;
      this._progressBar = this.GetComponent<UISlider>();
      this._startThumbY = this.thumbLabel.transform.localPosition.y;
      this._startThumbX = this.thumbLabel.transform.localPosition.x;
      this._endThumbX = -this._startThumbX;
    }

    private void Update()
    {
      this._progressBar.value = this.map.loadProgress;
      this.thumbLabel.transform.localPosition = new Vector3(this._startThumbX + this._endThumbX * this._progressBar.value, this._startThumbY, 0.0f);
      if ((double) this._progressBar.value < 1.0)
        return;
      if ((bool) ((Object) this.screenTouchCollider))
        this.screenTouchCollider.enabled = true;
      if ((bool) ((Object) this.completionTweener))
      {
        this.completionTweener.Play(true);
        EventDelegate.Add(this.completionTweener.onFinished, (EventDelegate.Callback) (() => this.enabled = false), true);
      }
      else
        this.enabled = false;
    }
  }
}
