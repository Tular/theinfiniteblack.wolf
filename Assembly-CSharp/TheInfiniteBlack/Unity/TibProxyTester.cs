﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibProxyTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class TibProxyTester : MonoBehaviour
  {
    public string userName = "TUnity";
    public string userPassword = "p@sswerd";
    public TibProxy proxy;
    public TibProxyTester.Server server;
    public TibProxyTester.TestCommand command;
    public bool execute;

    public void Connect()
    {
      TibProxy.Instance.Login((sbyte) this.server, this.userName, this.userPassword);
      TibProxy.Instance.onLoginSuccessEvent += new EventHandler<LoginSuccessEventArgs>(this.proxy_onLoginSuccessEvent);
    }

    private void proxy_onLoginSuccessEvent(object sender, LoginSuccessEventArgs e)
    {
      DialogManager.CloseAllDialogs();
      this.proxy.onLoginSuccessEvent -= new EventHandler<LoginSuccessEventArgs>(this.proxy_onLoginSuccessEvent);
    }

    public void Disconnect()
    {
      TibProxy.Instance.Disconnect("User Disconnect", true);
    }

    private void Update()
    {
      if (!this.execute)
        return;
      this.execute = false;
      switch (this.command)
      {
        case TibProxyTester.TestCommand.Connect:
          this.Connect();
          break;
        case TibProxyTester.TestCommand.Disconnect:
          this.Disconnect();
          break;
      }
    }

    public enum TestCommand
    {
      Connect,
      Disconnect,
    }

    public enum Server
    {
      Red,
      Blue,
      Green,
      Grey,
    }
  }
}
