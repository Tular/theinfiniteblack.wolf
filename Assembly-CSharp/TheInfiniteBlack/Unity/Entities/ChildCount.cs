﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Entities.ChildCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.Entities
{
  [ExecuteInEditMode]
  public class ChildCount : MonoBehaviour
  {
    public int transChildCount;
    public int childWidgetCount;
    public bool updateChildCount;
    public bool updateWidgetCount;

    private void Update()
    {
      if (this.updateChildCount)
      {
        this.transChildCount = this.transform.childCount;
        this.updateChildCount = false;
      }
      if (!this.updateWidgetCount)
        return;
      this.childWidgetCount = this.gameObject.GetComponentsInChildren<UIWidget>().Length;
      this.updateWidgetCount = false;
    }
  }
}
