﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SBUICalloutTooltip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class SBUICalloutTooltip : MonoBehaviour
  {
    public UIWidget pointer;
    public UIWidget tooltipRoot;
    public UILabel tooltipLabel;
    public string labelText;
    public int pointerStartLength;
    public int pointerEndLength;
    public Vector3 tooltipStartScale;
    public Vector3 tooltipEndScale;
    public float pointerTransitionDuration;
    public float tooltipTransitionDuration;
    public float showDuration;
    public float showDelay;
    private TweenWidth _pointerTweener;
    private TweenScale _tooltipTweener;
    private bool _isInitialized;
    private bool _isShowing;
    private float _hideTime;

    [ContextMenu("Show Immediate")]
    public void ShowImmediate()
    {
      if (!this._isShowing)
      {
        this.Show(0.0f, false);
        this._isShowing = true;
      }
      else
      {
        this.Hide();
        this._isShowing = false;
      }
    }

    public void Show(bool autoHide)
    {
      this.Show(0.0f, autoHide);
    }

    [ContextMenu("Reset Animation")]
    public void ResetAnimation()
    {
      this.ResetTweens();
    }

    public void Show(float delay, bool autoHide)
    {
      if (!this._isInitialized)
        this.Init();
      this.pointer.alpha = 1f;
      this.tooltipRoot.alpha = 1f;
      this.ResetTweens();
      this._hideTime = !autoHide ? float.MaxValue : RealTime.time + delay + this.showDuration;
      this._pointerTweener.delay = delay;
      this._pointerTweener.duration = this.pointerTransitionDuration;
      this._tooltipTweener.duration = this.tooltipTransitionDuration;
      EventDelegate.Add(this._pointerTweener.onFinished, new EventDelegate.Callback(((UITweener) this._tooltipTweener).PlayForward), true);
      this._pointerTweener.PlayForward();
      NGUITools.SetActiveSelf(this.gameObject, true);
    }

    [ContextMenu("Hide")]
    public void Hide()
    {
      if (!this._isInitialized)
        return;
      EventDelegate.Add(this._pointerTweener.onFinished, (EventDelegate.Callback) (() =>
      {
        this.pointer.alpha = 0.0f;
        this.tooltipRoot.alpha = 0.0f;
        NGUITools.SetActiveSelf(this.gameObject, false);
      }), true);
      EventDelegate.Add(this._tooltipTweener.onFinished, new EventDelegate.Callback(((UITweener) this._pointerTweener).PlayReverse), true);
      this._pointerTweener.delay = 0.0f;
      this._tooltipTweener.PlayReverse();
      this._hideTime = float.MaxValue;
    }

    private void Init()
    {
      this._pointerTweener.ignoreTimeScale = this._tooltipTweener.ignoreTimeScale = true;
      UIWidget pointer = this.pointer;
      float num1 = 0.0f;
      this.tooltipRoot.alpha = num1;
      double num2 = (double) num1;
      pointer.alpha = (float) num2;
      this.pointerEndLength = this.pointer.width;
      this.ResetTweens();
      this._isInitialized = true;
    }

    private void ResetTweens()
    {
      this.pointer.width = this.pointerStartLength;
      this._pointerTweener.onFinished.Clear();
      this._pointerTweener.PlayForward();
      this._pointerTweener.ResetToBeginning();
      this._pointerTweener.enabled = false;
      this._pointerTweener.from = this.pointerStartLength;
      this._pointerTweener.to = this.pointerEndLength;
      this._tooltipTweener.onFinished.Clear();
      this._tooltipTweener.PlayForward();
      this._tooltipTweener.ResetToBeginning();
      this._tooltipTweener.enabled = false;
      this._tooltipTweener.from = this.tooltipStartScale;
      this._tooltipTweener.to = this.tooltipEndScale;
    }

    private void Awake()
    {
      this.tooltipLabel.text = this.labelText;
      this._pointerTweener = this.pointer.gameObject.AddComponent<TweenWidth>();
      this._tooltipTweener = this.tooltipRoot.gameObject.AddComponent<TweenScale>();
    }

    private void Start()
    {
      if (this._isInitialized)
        return;
      this.Init();
    }

    private void OnEnable()
    {
      if (this._isInitialized)
        return;
      this.Init();
    }

    private void Update()
    {
      if ((double) this._hideTime >= (double) RealTime.time)
        return;
      this.Hide();
    }

    private void OnDisable()
    {
      this.pointer.alpha = 0.0f;
      this.tooltipRoot.alpha = 0.0f;
      this.ResetTweens();
    }
  }
}
