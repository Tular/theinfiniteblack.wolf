﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SceneManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [AdvancedInspector.AdvancedInspector]
  public class SceneManager : PreInstantiatedSingleton<SceneManager>
  {
    [Inspect(0)]
    public bool enableReturnToMotd;
    [Restrict("GetAvaliableSceneAliases")]
    [Inspect(1)]
    public string loginSceneAlias;
    [Inspect(2)]
    [Restrict("GetAvaliableSceneAliases")]
    public string mainSceneAlias;
    [Inspect(10)]
    public List<SceneInfo> sceneInfos;
    private SceneInfo _loginSceneInfo;
    private SceneInfo _mainSceneInfo;
    private SceneInfo _currentSceneInfo;
    private bool _isLoadSceneCoroutineRunning;

    [Inspect(11)]
    [ReadOnly]
    public int activeSceneIndex
    {
      get
      {
        return UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
      }
    }

    public bool isLoadingMainGame { get; private set; }

    public void GotoMainGameScene()
    {
      this.isLoadingMainGame = true;
      this.LogD(string.Format("SceneManager.GotoMainGameScene() Frame: {0}", (object) Time.frameCount));
      this.StartCoroutine(this.LoadScene(this._mainSceneInfo));
    }

    public void GotoLoginScene()
    {
      this.LogD(string.Format("SceneManager.GoToLoginScene()"));
      if (this._currentSceneInfo != null && this._currentSceneInfo == this._loginSceneInfo)
        return;
      this.StartCoroutine(this.LoadScene(this._loginSceneInfo));
    }

    protected override void Awake()
    {
      base.Awake();
      if (string.IsNullOrEmpty(this.loginSceneAlias))
        this.LogE(new Exception("Scene alias not found for login scene."));
      if (string.IsNullOrEmpty(this.mainSceneAlias))
        this.LogE(new Exception("Scene alias not found for main scene."));
      for (int index = 0; index < this.sceneInfos.Count; ++index)
      {
        if (this.sceneInfos[index].sceneAlias == this.loginSceneAlias)
          this._loginSceneInfo = this.sceneInfos[index];
        if (this.sceneInfos[index].sceneAlias == this.mainSceneAlias)
          this._mainSceneInfo = this.sceneInfos[index];
      }
      if (this._loginSceneInfo == null)
        this.LogE(new Exception("SceneInfo not found for login scene."));
      if (this._mainSceneInfo != null)
        return;
      this.LogE(new Exception("SceneInfo not found for main game scene."));
    }

    [DebuggerHidden]
    private IEnumerator LoadScene(SceneInfo sceneInfo)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SceneManager.\u003CLoadScene\u003Ec__Iterator30()
      {
        sceneInfo = sceneInfo,
        \u003C\u0024\u003EsceneInfo = sceneInfo,
        \u003C\u003Ef__this = this
      };
    }

    public string[] GetAvaliableSceneAliases()
    {
      return this.sceneInfos.Select<SceneInfo, string>((Func<SceneInfo, string>) (s => s.sceneAlias)).ToArray<string>();
    }
  }
}
