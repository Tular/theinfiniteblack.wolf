﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockLocalEntities
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Unity
{
  public class MockLocalEntities : LocalEntities
  {
    private static readonly FieldInfo _fldValues = typeof (LocalEntities).GetField("_values", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);

    public MockLocalEntities(MockGameState state)
      : base((GameState) state)
    {
    }

    static MockLocalEntities()
    {
      if (MockLocalEntities._fldValues == null)
        throw new Exception("Values FieldInfo is null");
    }

    public List<Entity> moqList
    {
      get
      {
        return MockLocalEntities._fldValues.GetValue((object) this) as List<Entity>;
      }
    }

    public void AddRange(List<Entity> values)
    {
      using (List<Entity>.Enumerator enumerator = values.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.AddEntity(enumerator.Current);
      }
    }

    public void AddEntity(Entity value)
    {
      this.Add(value);
    }

    public void RemoveEntity(Entity value, EntityExitType type, Direction dir)
    {
      this.Remove(value, type, dir);
    }

    public void ExitAll()
    {
      using (List<Entity>.Enumerator enumerator = this.moqList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.RemoveEntity(enumerator.Current, EntityExitType.DESTROYED, Direction.None);
      }
    }

    public void ClearLocal()
    {
      this.moqList.Clear();
    }

    public void SetLocal(List<Entity> values)
    {
      this.Set(values);
    }
  }
}
