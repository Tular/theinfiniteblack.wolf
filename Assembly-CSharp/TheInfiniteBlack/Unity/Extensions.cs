﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.Extensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public static class Extensions
  {
    public static void LogI(this MonoBehaviour obj, string log)
    {
      ClientLog.Instance.I((UnityEngine.Object) obj, log);
    }

    public static void LogD(this MonoBehaviour obj, string msg, params object[] args)
    {
      ClientLog.Instance.D((UnityEngine.Object) obj, msg, args);
    }

    public static void LogD(this MonoBehaviour obj, string log)
    {
      ClientLog.Instance.D((UnityEngine.Object) obj, log);
    }

    public static void LogW(this MonoBehaviour obj, string log)
    {
      ClientLog.Instance.W((UnityEngine.Object) obj, log);
    }

    public static void LogE(this MonoBehaviour obj, Exception e)
    {
      ClientLog.Instance.E((UnityEngine.Object) obj, e);
    }
  }
}
