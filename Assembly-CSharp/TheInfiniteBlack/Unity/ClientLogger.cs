﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ClientLogger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;

namespace TheInfiniteBlack.Unity
{
  public class ClientLogger : TheInfiniteBlack.Library.ILogger, IUnityLogger
  {
    private const string _iFormat = "{0} I-- {1}.{2}: {3}";
    private const string _dFormat = "{0} D-- {1}.{2}: {3}";
    private const string _wFormat = "{0} W-- {1}.{2}: {3}";
    private const string _eFormat = "{0} E-- {1}.{2}: {3}";
    private ILoggerView _loggerView;
    private bool _hasView;

    public ILoggerView loggerView
    {
      get
      {
        return this._loggerView;
      }
      set
      {
        this._hasView = value != null;
        this._loggerView = value;
      }
    }

    public ClientLogLevel clientLogLevel { get; set; }

    public bool Debug { get; set; }

    public void I(UnityEngine.Object context, string log)
    {
      if (this.clientLogLevel < ClientLogLevel.Info)
        return;
      string line = string.Format("{0} I-- {1}.{2}: {3}", (object) Util.Now, (object) ((object) context).GetType().Name, (object) context.name, (object) log);
      UnityEngine.Debug.Log((object) line, context);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }

    public void D(UnityEngine.Object context, string log, params object[] args)
    {
      if (this.clientLogLevel < ClientLogLevel.Debug)
        return;
      this.D(context, string.Format(log, args));
    }

    public void D(UnityEngine.Object context, string log)
    {
      if (this.clientLogLevel < ClientLogLevel.Debug)
        return;
      string line = string.Format("{0} D-- {1}.{2}: {3}", (object) Util.Now, (object) ((object) context).GetType().Name, (object) context.name, (object) log);
      UnityEngine.Debug.Log((object) line, context);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }

    public void W(UnityEngine.Object context, string log)
    {
      if (this.clientLogLevel < ClientLogLevel.Warning)
        return;
      string line = string.Format("{0} W-- {1}.{2}: {3}", (object) Util.Now, (object) ((object) context).GetType().Name, (object) context.name, (object) log);
      UnityEngine.Debug.Log((object) line, context);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }

    public void E(UnityEngine.Object context, Exception e)
    {
      UnityEngine.Debug.LogException(e, context);
      if (this.clientLogLevel < ClientLogLevel.Error)
        return;
      string line = string.Format("{0} E-- {1}.{2}: {3}", (object) Util.Now, (object) ((object) context).GetType().Name, (object) context.name, (object) e);
      UnityEngine.Debug.Log((object) line);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }

    public void I(object sender, string callerName, string log)
    {
      if (this.clientLogLevel < ClientLogLevel.Info)
        return;
      string line = string.Format("{0} I-- {1}.{2}: {3}", (object) Util.Now, (object) sender.GetType().Name, (object) callerName, (object) log);
      UnityEngine.Debug.Log((object) line);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }

    public void D(object sender, string callerName, string log)
    {
      if (this.clientLogLevel < ClientLogLevel.Debug)
        return;
      string line = string.Format("{0} D-- {1}.{2}: {3}", (object) Util.Now, (object) sender.GetType().Name, (object) callerName, (object) log);
      UnityEngine.Debug.Log((object) line);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }

    public void W(object sender, string callerName, string log)
    {
      if (this.clientLogLevel < ClientLogLevel.Warning)
        return;
      string line = string.Format("{0} W-- {1}.{2}: {3}", (object) Util.Now, (object) sender.GetType().Name, (object) callerName, (object) log);
      UnityEngine.Debug.Log((object) line);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }

    public void E(object sender, string callerName, Exception e)
    {
      UnityEngine.Debug.LogException(e);
      if (this.clientLogLevel < ClientLogLevel.Error)
        return;
      string line = string.Format("{0} I-- {1}.{2}: {3}", (object) Util.Now, (object) sender.GetType().Name, (object) callerName, (object) e);
      UnityEngine.Debug.Log((object) line);
      if (!this._hasView)
        return;
      this.loggerView.AddLine(line);
    }
  }
}
