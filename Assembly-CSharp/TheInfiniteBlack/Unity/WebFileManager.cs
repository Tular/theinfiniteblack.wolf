﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.WebFileManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [AdvancedInspector.AdvancedInspector]
  public class WebFileManager : PreInstantiatedSingleton<WebFileManager>
  {
    private readonly Dictionary<string, WebFileLoadOperation> _pendingOperations = new Dictionary<string, WebFileLoadOperation>();
    private readonly Dictionary<string, WebFileLoadOperation> _completedOperations = new Dictionary<string, WebFileLoadOperation>();
    [Inspect(1)]
    public List<WebFileDefinition> fileDefinitions;
    [Inspect(2)]
    public UriLocationDictionary definedUriLocations;

    public IWebFileLoadOperation LoadFile(string fileAlias)
    {
      WebFileDefinition fileDef = this.fileDefinitions.FirstOrDefault<WebFileDefinition>((Func<WebFileDefinition, bool>) (d => d.fileAlias.Equals(fileAlias)));
      if (fileDef == null)
      {
        KeyNotFoundException notFoundException = new KeyNotFoundException("File ailas " + fileAlias + " not found in file definitions");
        this.LogE((Exception) notFoundException);
        throw notFoundException;
      }
      if (this._pendingOperations.ContainsKey(fileAlias))
        return (IWebFileLoadOperation) this._pendingOperations[fileAlias];
      if (this._completedOperations.ContainsKey(fileAlias))
      {
        WebFileLoadOperation completedOperation = this._completedOperations[fileAlias];
        if (fileDef.isCached && (double) Time.unscaledTime < (double) completedOperation.nextRefreshTime)
          return (IWebFileLoadOperation) completedOperation;
        this.LogI("Refreshing file: " + fileAlias);
        this._completedOperations.Remove(fileAlias);
        completedOperation.Reset();
        this._pendingOperations.Add(fileAlias, completedOperation);
        return (IWebFileLoadOperation) completedOperation;
      }
      WebFileLoadOperation fileLoadOperation = new WebFileLoadOperation(fileDef);
      this._pendingOperations.Add(fileLoadOperation.fileAlias, fileLoadOperation);
      return (IWebFileLoadOperation) fileLoadOperation;
    }

    private void Update()
    {
      using (List<string>.Enumerator enumerator = this._pendingOperations.Keys.ToList<string>().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (!this._pendingOperations[current].Update())
          {
            this._completedOperations.Add(current, this._pendingOperations[current]);
            this._pendingOperations.Remove(current);
          }
        }
      }
    }

    protected override void Awake()
    {
      base.Awake();
      for (int index = 0; index < this.fileDefinitions.Count; ++index)
        this.LoadFile(this.fileDefinitions[index].fileAlias);
    }
  }
}
