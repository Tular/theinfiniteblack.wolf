﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ClientLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;

namespace TheInfiniteBlack.Unity
{
  public static class ClientLog
  {
    private static IUnityLogger _instance;

    public static IUnityLogger Instance
    {
      get
      {
        return ClientLog._instance;
      }
      set
      {
        ClientLog._instance = value;
        Log.Instance = (ILogger) ClientLog._instance;
        Log.Instance.Debug = ClientLog._instance.Debug;
      }
    }
  }
}
