﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SettingsFileManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using TheInfiniteBlack.Library.Client;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class SettingsFileManager : IFileManager
  {
    public SettingsFileManager()
    {
      this.dataPath = Application.persistentDataPath;
    }

    public string dataPath { get; private set; }

    public FileStream Get(string fileName, FileMode mode)
    {
      try
      {
        return new FileStream(Path.Combine(this.dataPath, fileName), mode);
      }
      catch (Exception ex)
      {
        return (FileStream) null;
      }
    }
  }
}
