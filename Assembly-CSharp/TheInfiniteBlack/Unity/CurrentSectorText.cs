﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.CurrentSectorText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [RequireComponent(typeof (UILabel))]
  public class CurrentSectorText : MonoBehaviour
  {
    public Color normalSpace;
    public Color riftSpace;
    private int _x;
    private int _y;
    private string _currentSector;
    private bool _isRift;
    private bool _hasChanged;
    private string _encodedNormalSpaceColor;
    private string _encodedRiftColor;
    private UILabel _label;

    public Sector currentSector
    {
      set
      {
        string str = value.ToString();
        if (this._currentSector.Equals(str) && this._isRift == value.Rift)
          return;
        this._currentSector = str;
        this._isRift = value.Rift;
        this._x = value.X;
        this._y = value.Y;
        this._hasChanged = true;
      }
    }

    private string formattedCurrentSector
    {
      get
      {
        return string.Format("[{0}][{1:D2}/{2:D2}]", !this._isRift ? (object) this._encodedNormalSpaceColor : (object) this._encodedRiftColor, (object) (this._x + 1), (object) (this._y + 1));
      }
    }

    private void OnEnable()
    {
      if (!(bool) ((Object) this._label))
        this._label = this.GetComponent<UILabel>();
      this._encodedNormalSpaceColor = NGUIText.EncodeColor(this.normalSpace);
      this._encodedRiftColor = NGUIText.EncodeColor(this.riftSpace);
      this._currentSector = string.Empty;
      this._label.text = string.Empty;
      this._hasChanged = true;
    }

    private void Update()
    {
      if (TibProxy.gameState == null || TibProxy.gameState.MyLoc == null)
        return;
      this.currentSector = TibProxy.gameState.MyLoc;
      if (!this._hasChanged)
        return;
      this._hasChanged = false;
      this._label.text = this.formattedCurrentSector;
    }
  }
}
