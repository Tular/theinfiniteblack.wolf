﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ScreenManagerDebugWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class ScreenManagerDebugWindow : MonoBehaviour
  {
    public UILabel label;
    public UISlider uiScaleSlider;
    public int minScaleHeight;
    public int maxScaleHeight;
    public UISwitchControl adjustByDpiToggle;
    private int _nextUpdateFrame;
    private Camera _uiCamera;

    public void UpdateUIScale()
    {
      if (!((Object) UIPopupList.current != (Object) null))
        return;
      string key = UIPopupList.current.value;
      if (key == null)
        return;
      // ISSUE: reference to a compiler-generated field
      if (ScreenManagerDebugWindow.\u003C\u003Ef__switch\u0024map4 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ScreenManagerDebugWindow.\u003C\u003Ef__switch\u0024map4 = new Dictionary<string, int>(4)
        {
          {
            "Tiny",
            0
          },
          {
            "Small",
            1
          },
          {
            "Normal",
            2
          },
          {
            "Larger",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (!ScreenManagerDebugWindow.\u003C\u003Ef__switch\u0024map4.TryGetValue(key, out num))
        return;
      switch (num)
      {
      }
    }

    public void OnScaleSliderChange()
    {
      if (!((Object) UIProgressBar.current != (Object) null))
        return;
      int num = Mathf.RoundToInt((float) this.minScaleHeight + (float) (this.maxScaleHeight - this.minScaleHeight) * UIProgressBar.current.value);
      Debug.Log((object) string.Format("New manual height: {0}", (object) num));
      PreInstantiatedSingleton<UIScaleManager>.Instance.manualHeight = num;
    }

    public void OnAdjustByDpiChange()
    {
      PreInstantiatedSingleton<UIScaleManager>.Instance.adjustByDPI = this.adjustByDpiToggle.value;
      UIScaleManager.UpdateUiScale();
    }

    public void ToggleScaleStyle()
    {
      PreInstantiatedSingleton<UIScaleManager>.Instance.scalingStyle = PreInstantiatedSingleton<UIScaleManager>.Instance.scalingStyle != UIRoot.Scaling.Flexible ? UIRoot.Scaling.Flexible : UIRoot.Scaling.Constrained;
      UIScaleManager.UpdateUiScale();
    }

    public void UpdateScreenInfo()
    {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendFormat("Camera Aspect Ratio: {0}\n", (object) this._uiCamera.aspect);
      stringBuilder.AppendFormat("Reported Resolution: {0}x{1}\n", (object) Screen.currentResolution.width, (object) Screen.currentResolution.height);
      stringBuilder.AppendFormat("Reported Height: {0}\n", (object) Screen.height);
      stringBuilder.AppendFormat("Reported Width: {0}\n", (object) Screen.width);
      stringBuilder.AppendFormat("Reported DPI: {0}\n", (object) Screen.dpi);
      stringBuilder.AppendFormat("Height Adjusted By DPI: {0}\n", (object) NGUIMath.AdjustByDPI((float) Screen.height));
      if ((Object) UIRoot.list.FirstOrDefault<UIRoot>() != (Object) null)
      {
        UIRoot uiRoot = UIRoot.list.First<UIRoot>();
        stringBuilder.AppendFormat("UIRoot Active Height: {0}\n", (object) uiRoot.activeHeight);
        stringBuilder.AppendFormat("UIRoot Scaling Style: {0}\n", (object) uiRoot.scalingStyle);
        if (uiRoot.scalingStyle == UIRoot.Scaling.Flexible)
        {
          stringBuilder.AppendFormat("UIRoot Maximum Height: {0}\n", (object) uiRoot.maximumHeight);
          stringBuilder.AppendFormat("UIRoot Minimum Height: {0}\n", (object) uiRoot.minimumHeight);
        }
        else
          stringBuilder.AppendFormat("UIRoot Manual Height: {0}\n", (object) uiRoot.manualHeight);
        stringBuilder.AppendFormat("UIRoot Adjust by DPI: {0}\n", (object) uiRoot.adjustByDPI);
        stringBuilder.AppendFormat("UIRoot Pixel Size Adjustment: {0}\n", (object) uiRoot.pixelSizeAdjustment);
      }
      this.label.text = stringBuilder.ToString();
    }

    private void Awake()
    {
      this._uiCamera = NGUITools.FindCameraForLayer(this.gameObject.layer);
    }

    private void Start()
    {
      float num = (float) (((double) Screen.height - (double) this.minScaleHeight) / ((double) this.maxScaleHeight - (double) this.minScaleHeight));
      Debug.Log((object) string.Format("starting slider value: {0} - {1} {2}", (object) num, (object) this.minScaleHeight, (object) this.maxScaleHeight));
      this.uiScaleSlider.value = num;
    }

    private void Update()
    {
      if (this._nextUpdateFrame >= Time.frameCount)
        return;
      this.UpdateScreenInfo();
      this._nextUpdateFrame = Time.frameCount + 10;
    }
  }
}
