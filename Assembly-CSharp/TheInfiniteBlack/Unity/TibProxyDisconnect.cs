﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibProxyDisconnect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class TibProxyDisconnect : MonoBehaviour
  {
    public bool closeApplication;
    public bool runOnClick;

    public void Disconnect()
    {
      if (TibProxy.Instance == null)
        return;
      if (this.closeApplication && !Application.isEditor)
        Application.Quit();
      else
        TibProxy.Instance.Disconnect("User Logoff", true);
    }

    private void OnClick()
    {
      this.Disconnect();
    }
  }
}
