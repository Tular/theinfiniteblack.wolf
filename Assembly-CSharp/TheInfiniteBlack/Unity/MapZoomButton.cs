﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MapZoomButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Unity.Map;
using TheInfiniteBlack.Unity.UI;

namespace TheInfiniteBlack.Unity
{
  public class MapZoomButton : SBUIButton
  {
    public MapZoomButton.ZoomType type;
    private bool _isPressed;
    private bool _isZooming;

    protected override void OnPress(bool isDown)
    {
      base.OnPress(isDown);
      if (this.type == MapZoomButton.ZoomType.CenterMap)
        PreInstantiatedSingleton<SectorMap>.Instance.controller.TranslateToPlayerSector(false);
      else if (isDown)
      {
        this._isPressed = true;
      }
      else
      {
        if (!this._isPressed)
          return;
        this._isPressed = false;
        this._isZooming = false;
        PreInstantiatedSingleton<KeyboardMapController>.Instance.MapZoomEnd();
      }
    }

    protected override void Update()
    {
      if (!this._isPressed)
        return;
      if (this._isZooming && !PreInstantiatedSingleton<SectorMap>.Instance.controller.isZooming)
      {
        this.OnPress(false);
      }
      else
      {
        this._isZooming = true;
        switch (this.type)
        {
          case MapZoomButton.ZoomType.In:
            PreInstantiatedSingleton<KeyboardMapController>.Instance.MapZoomIn();
            break;
          case MapZoomButton.ZoomType.Out:
            PreInstantiatedSingleton<KeyboardMapController>.Instance.MapZoomOut();
            break;
        }
      }
    }

    public enum ZoomType
    {
      In,
      Out,
      CenterMap,
    }
  }
}
