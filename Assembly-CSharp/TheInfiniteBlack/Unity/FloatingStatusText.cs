﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.FloatingStatusText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class FloatingStatusText : MonoBehaviour
  {
    public UILabel header;
    public UILabel status;
    public Color textColor;
    private GameObject _go;
    private FloatingStatusTextController _controller;

    public bool UpdateStatusText(string text)
    {
      return this.UpdateStatusText(text, string.Empty);
    }

    public bool UpdateStatusText(string headerText, string statusText)
    {
      if ((bool) ((UnityEngine.Object) this.status) && this.header.text.Equals(headerText) && this.status.text.Equals(statusText))
        return false;
      if (string.IsNullOrEmpty(headerText) && string.IsNullOrEmpty(statusText))
      {
        this.header.text = string.Empty;
        if ((bool) ((UnityEngine.Object) this.status))
          this.status.text = string.Empty;
        this.gameObject.SetActive(false);
        return true;
      }
      this.gameObject.SetActive(true);
      this.header.text = headerText;
      if ((bool) ((UnityEngine.Object) this.status) && !string.IsNullOrEmpty(statusText))
        this.status.text = statusText;
      else if ((bool) ((UnityEngine.Object) this.status))
        this.status.text = string.Empty;
      return true;
    }

    private void Awake()
    {
      this._go = this.gameObject;
      this._controller = NGUITools.FindInParents<FloatingStatusTextController>(this.gameObject);
      if (!(bool) ((UnityEngine.Object) this._controller))
        throw new Exception("FloatingStatusTextController not found in parents of " + this.name);
    }

    private void Start()
    {
      this.gameObject.SetActive(false);
    }
  }
}
