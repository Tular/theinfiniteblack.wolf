﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockPlayerCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Reflection;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Unity
{
  public class MockPlayerCollection : PlayerCollection
  {
    private static readonly FieldInfo _fldValues = typeof (PlayerCollection).GetField("_values", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);

    public MockPlayerCollection(IGameState state)
      : base(state)
    {
    }

    public Dictionary<int, ClientPlayer> moqValues
    {
      get
      {
        return MockPlayerCollection._fldValues.GetValue((object) this) as Dictionary<int, ClientPlayer>;
      }
    }

    public void AddPlayer(MockClientPlayer player)
    {
      this.moqValues.Add(player.ID, (ClientPlayer) player);
    }
  }
}
