﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockNpc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Reflection;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Unity
{
  public class MockNpc : Npc
  {
    public static readonly FieldInfo _fldNpcClass = typeof (Npc).GetField("_class", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    public static readonly FieldInfo _fldNpcLevel = typeof (Npc).GetField("_level", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    public Action onClick;
    public Action onLongPress;

    public MockNpc(IGameState state, int id)
      : base(state, id)
    {
    }

    static MockNpc()
    {
      if (MockNpc._fldNpcClass == null)
        throw new Exception("Npc Class FieldInfo is null");
      if (MockNpc._fldNpcLevel == null)
        throw new Exception("Npc Level FieldInfo is null");
    }

    public static MockNpc GenerateRandom(IGameState state)
    {
      MockNpc mockNpc = new MockNpc(state, MockTibProxy.nextEntityId)
      {
        npcLevel = (NpcLevel) UnityEngine.Random.Range(0, 4)
      };
      switch (UnityEngine.Random.Range(0, 2))
      {
        case 0:
          mockNpc.npcClass = (NpcClass) UnityEngine.Random.Range(0, 6);
          break;
        case 1:
          mockNpc.npcClass = (NpcClass) UnityEngine.Random.Range(10, 19);
          break;
        case 2:
          mockNpc.npcClass = (NpcClass) UnityEngine.Random.Range(30, 35);
          break;
      }
      return mockNpc;
    }

    public NpcClass npcClass
    {
      set
      {
        MockNpc._fldNpcClass.SetValue((object) this, (object) value);
      }
    }

    public NpcLevel npcLevel
    {
      set
      {
        MockNpc._fldNpcLevel.SetValue((object) this, (object) value);
      }
    }

    public MockNpc SetLocation(MockSector sector)
    {
      if (TibProxy.gameState != null && object.ReferenceEquals((object) TibProxy.gameState.MyLoc, (object) sector))
        MockTibProxy.MockInstance.moqGameState.moqLocal.AddEntity((Entity) this);
      return this;
    }

    public override void OnClick()
    {
      if (this.onClick != null)
        this.onClick();
      base.OnClick();
    }

    public override void OnLongPress()
    {
      if (this.onLongPress != null)
        this.onLongPress();
      base.OnLongPress();
    }
  }
}
