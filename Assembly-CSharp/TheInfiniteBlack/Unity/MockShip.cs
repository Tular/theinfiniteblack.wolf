﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockShip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Reflection;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Unity
{
  public class MockShip : Ship
  {
    private static readonly FieldInfo _fldShipClass = typeof (Ship).GetField("_class", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldPvPFlag = typeof (Ship).GetField("_pvpFlag", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldXp = typeof (Ship).GetField("_xp", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldPlayer = typeof (Ship).BaseType.GetField("_player", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    public Action onClick;
    public Action onLongPress;

    public MockShip(IGameState state, int id)
      : base(state, id)
    {
    }

    static MockShip()
    {
      if (MockShip._fldShipClass == null)
        throw new Exception("ShipClass FieldInfo is null");
      if (MockShip._fldPvPFlag == null)
        throw new Exception("PvPFlag FieldInfo is null");
      if (MockShip._fldXp == null)
        throw new Exception("Xp FieldInfo is null");
      if (MockShip._fldPlayer == null)
        throw new Exception("Player FieldInfo is null");
    }

    public static MockShip GenerateRandom(IGameState state)
    {
      MockShip mockShip = new MockShip(state, MockTibProxy.nextEntityId)
      {
        xp = UnityEngine.Random.Range(0, 100000),
        shipClass = (ShipClass) UnityEngine.Random.Range(0, 18)
      };
      mockShip.SetPlayer(MockClientPlayer.GenerateRandom(state));
      return mockShip;
    }

    public ShipClass shipClass
    {
      set
      {
        MockShip._fldShipClass.SetValue((object) this, (object) value);
      }
    }

    public bool pvpFlag
    {
      set
      {
        MockShip._fldPvPFlag.SetValue((object) this, (object) value);
      }
    }

    public int xp
    {
      set
      {
        MockShip._fldXp.SetValue((object) this, (object) value);
      }
    }

    public MockClientPlayer moqPlayer { get; private set; }

    public MockShip SetPlayer(MockClientPlayer player)
    {
      this.moqPlayer = player;
      MockShip._fldPlayer.SetValue((object) this, (object) this.moqPlayer);
      return this;
    }

    public MockShip SetLocation(MockSector sector)
    {
      this.moqPlayer.Location = (Sector) sector;
      if (TibProxy.gameState != null && object.ReferenceEquals((object) TibProxy.gameState.MyLoc, (object) sector))
        MockTibProxy.MockInstance.moqGameState.moqLocal.AddEntity((Entity) this);
      return this;
    }

    public override void OnClick()
    {
      if (this.onClick != null)
        this.onClick();
      base.OnClick();
    }

    public override void OnLongPress()
    {
      if (this.onLongPress != null)
        this.onLongPress();
      base.OnLongPress();
    }
  }
}
