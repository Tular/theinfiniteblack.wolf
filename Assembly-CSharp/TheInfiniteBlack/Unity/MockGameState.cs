﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockGameState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Reflection;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;

namespace TheInfiniteBlack.Unity
{
  public class MockGameState : GameState
  {
    private static readonly PropertyInfo _propIsLoggedIn = typeof (GameState).GetProperty("IsLoggedIn");
    private static readonly FieldInfo _fldLocal = typeof (GameState).GetField("_local", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldPlayers = typeof (GameState).GetField("_players", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldMyLoc = typeof (GameState).GetField("_myLoc", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldMyAccount = typeof (GameState).GetField("_myAccount", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);

    public MockGameState(IUserInterface ui, INetwork net, AccountManager accounts, LoginRequest login)
      : base(ui, net, accounts, login)
    {
      this.moqPlayers = new MockPlayerCollection((IGameState) this);
      this.moqLocal = new MockLocalEntities(this);
      MockGameState._fldLocal.SetValue((object) this, (object) this.moqLocal);
      MockGameState._fldPlayers.SetValue((object) this, (object) this.moqPlayers);
    }

    public MockSector moqMyLoc { get; private set; }

    public MockPlayerCollection moqPlayers { get; private set; }

    public MockLocalEntities moqLocal { get; private set; }

    public MockGameState SetMyLocation(MockSector location)
    {
      this.moqMyLoc = location;
      MockGameState._fldMyLoc.SetValue((object) this, (object) this.moqMyLoc);
      return this;
    }

    public MockGameState SetMyAccount(Account testAccount)
    {
      MockGameState._fldMyAccount.SetValue((object) this, (object) testAccount);
      return this;
    }

    public MockGameState SetLocalEntities(MockLocalEntities local)
    {
      this.moqLocal = local;
      MockGameState._fldLocal.SetValue((object) this, (object) this.moqLocal);
      return this;
    }

    public void ExecuteOnLogin(MockClientPlayer myPlayer, MockShip myShip)
    {
      MockGameState._propIsLoggedIn.SetValue((object) this, (object) true, (object[]) null);
      this.OnLogin((ClientPlayer) myPlayer, (Ship) myShip);
    }

    public MockGameState AddPlayer(MockClientPlayer player)
    {
      this.moqPlayers.AddPlayer(player);
      return this;
    }
  }
}
