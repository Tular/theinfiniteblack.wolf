﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.FakeNames
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class FakeNames
  {
    private static List<string> _names = new List<string>()
    {
      "Hugo",
      "William",
      "German",
      "Ruth",
      "Lana",
      "John",
      "Kathie",
      "Willie",
      "Quinton",
      "Kenneth",
      "Gussie",
      "Jody",
      "Linda",
      "Jonathan",
      "Albert",
      "Mary",
      "Willis",
      "Raymond",
      "Henry",
      "Kimberly",
      "Carmen",
      "Michelle",
      "Joseph",
      "Willie",
      "Dawn",
      "Fred",
      "Nancy",
      "Paul",
      "Ruth",
      "James",
      "Nancy",
      "Lawrence",
      "Gail",
      "Joseph",
      "Connie",
      "Ryan",
      "Michele",
      "Latrisha",
      "Melissa",
      "Jacob",
      "James",
      "Thomas",
      "Jayson",
      "John",
      "Samantha",
      "Kathryn",
      "Shawn",
      "Matt",
      "Cary",
      "Morris",
      "Ethel",
      "Edgar",
      "Richard",
      "Barry",
      "Erica",
      "Delmar",
      "James",
      "John",
      "Donovan",
      "Jason",
      "Elizabeth",
      "Ida",
      "Ramona",
      "Alita",
      "Nereida",
      "Georgia",
      "Joseph",
      "Betty",
      "Virginia",
      "Scott",
      "Kenneth",
      "Daniel",
      "Javier",
      "Theresa",
      "Richard",
      "James",
      "Blake",
      "Dorothy",
      "Dortha",
      "Marsha",
      "Sally",
      "Tim",
      "Sara",
      "Dennis",
      "Doris",
      "Tina",
      "Jerome",
      "Herbert",
      "Jose",
      "Adrienne",
      "Karen",
      "Adrienne",
      "Dan",
      "Esther",
      "Billy",
      "Christine",
      "Enrique",
      "Jaime",
      "Ronald",
      "Jennifer"
    };

    public static string GetRandom()
    {
      int index = Random.Range(0, FakeNames._names.Count);
      return FakeNames._names[index];
    }
  }
}
