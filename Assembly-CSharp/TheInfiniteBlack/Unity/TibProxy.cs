﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibProxy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.Steam;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class TibProxy : MonoBehaviour, IUserInterface, ITibProxy
  {
    public float maxPauseSeconds = 30f;
    public int startingEventQueueSize = 200;
    public int maxEventsPerFrame = 10;
    public int prodServerPort = 32040;
    public int devServerPort = 32041;
    private readonly Queue<GameEventArgs> _stage1Queue = new Queue<GameEventArgs>(1000);
    public static bool UseDevServer;
    public ClientLogLevel clientLogLevel;
    public bool libraryDebugLog;
    public LoggerView logView;
    public DistributionType distributionType;
    public string unityClientPrefix;
    public string deviceId;
    public string deviceModel;
    public string deviceOs;
    public string deviceName;
    public string versionIdentity;
    public string prodServerIpWebfileName;
    public string devServerIpWebfileName;
    private bool _queueEvents;
    private GameEventArgs[] _eventsBeingProcessed;
    private TibProxy.FilteredEventQueue _gameEventBuffer;
    protected IGameState state;
    protected IConfigManager configManager;
    private bool _wasHoldingEvents;
    private bool _saveSettingsOnDisconnect;

    public event EventHandler<DisconnectEventArgs> onDisconnectEvent;

    public event EventHandler<LoginSuccessEventArgs> onLoginSuccessEvent;

    public event EventHandler<PopupWindowEventArgs> onPopupWindowEvent;

    public event EventHandler<AcceptDeclineWindowEventArgs> onAcceptDeclineWindowsEvent;

    public event EventHandler<EntityArriveEventArgs> onEntityArriveEvent;

    public event EventHandler<EntityExitEventArgs> onEntityExitEvent;

    public event EventHandler<AttackEventArgs> onAttackEvent;

    public event EventHandler<EntityHullChangedEventArgs> onEntityHullChangedEvent;

    public event EventHandler<ChatEventArgs> onChatEvent;

    public event EventHandler<LocationChangedEventArgs> onLocationChangedEvent;

    public event EventHandler<PvPFlagChangeEventArgs> onPvPFlagChangeEvent;

    public event EventHandler<GainXpEventArgs> onGainXpEvent;

    public event EventHandler<GainItemEventArgs> onGainItemEvent;

    public event EventHandler<MoneyChangeEventArgs> onMoneyChangeEvent;

    public event EventHandler<InventoryChangedEventArgs> onInventoryChangedEvent;

    public event EventHandler<ResourcesChangedEventArgs> onResourcesChangedEvent;

    public event EventHandler<PlayerOnlineStatusChangedEventArgs> onPlayerOnlineStatusChangedEvent;

    public event EventHandler<HarvestEventArgs> onHarvestEvent;

    public event EventHandler<BankChangedEventArgs> onBankChangedEvent;

    public event EventHandler<FollowTargetChangedEventArgs> onFollowTargetChangedEvent;

    public event EventHandler<AttackTargetChangedEventArgs> onAttackTargetChangedEvent;

    public event EventHandler<TradeEventArgs> onTradeEvent;

    public event EventHandler<TradeFailedEventArgs> onTradeFailedEvent;

    public event EventHandler<CourseSetEventArgs> onCourseSetEvent;

    public event EventHandler<MovementStatusChangedEventArgs> onMovementStatusChangedEvent;

    public event EventHandler<AuctionUpdatedEventArgs> onAuctionUpdatedEvent;

    public event EventHandler<AuctionRemovedEventArgs> onAuctionRemovedEvent;

    public event EventHandler<ShowEntityDetailEventArgs> onShowEntityDetailEvent;

    public event EventHandler<CombatFlashEventArgs> onCombatFlashEvent;

    public event EventHandler<SoundEventArgs> onSoundEvent;

    public event EventHandler<ShowUrlEventArgs> onShowUrlEvent;

    public event EventHandler<GameEventArgs> onGameEvent;

    public sbyte serverId { get; private set; }

    public string clientVersion
    {
      get
      {
        return string.Format("{0}_{1}", (object) ((Enum) this.distributionType).ToString().ToLower(), (object) this.versionIdentity);
      }
    }

    public DistributionType distribution
    {
      get
      {
        return this.distributionType;
      }
    }

    public bool isSteamDistribution
    {
      get
      {
        if (this.distributionType != DistributionType.Steam_Windows && this.distributionType != DistributionType.Steam_OSX)
          return this.distributionType == DistributionType.Steam_Linux;
        return true;
      }
    }

    public string currentServerIp { get; private set; }

    public string currentServerWebFile { get; private set; }

    public bool isInDebugMode
    {
      get
      {
        return Log.Instance.Debug;
      }
    }

    public bool isUsingDevServer
    {
      get
      {
        return this.devServerIpWebfileName.Equals(this.currentServerWebFile);
      }
    }

    public int eventsProcessed { get; private set; }

    public int queueCount
    {
      get
      {
        return this._gameEventBuffer.count;
      }
    }

    public int queueMaxSize
    {
      get
      {
        return this._gameEventBuffer.maxBufferSize;
      }
    }

    public GameEventArgs[] pendingEvents
    {
      get
      {
        return this._gameEventBuffer.GetAllPendingEvents(false);
      }
    }

    protected static TibProxy _instance { private get; set; }

    public static ITibProxy Instance
    {
      get
      {
        return (ITibProxy) TibProxy._instance;
      }
    }

    public static IGameState gameState
    {
      get
      {
        return TibProxy._instance.state;
      }
    }

    public static IConfigManager accountManager
    {
      get
      {
        if ((UnityEngine.Object) TibProxy._instance == (UnityEngine.Object) null || TibProxy._instance.configManager == null)
          return (IConfigManager) null;
        return TibProxy._instance.configManager;
      }
    }

    public static AccountSettings mySettings
    {
      get
      {
        if ((UnityEngine.Object) TibProxy._instance == (UnityEngine.Object) null || TibProxy._instance.state == null || TibProxy._instance.state.MySettings == null)
          return (AccountSettings) null;
        return TibProxy._instance.state.MySettings;
      }
    }

    protected virtual void OnAwake()
    {
      this.configManager = (IConfigManager) new ConfigManager();
      this.deviceId = SystemInfo.deviceUniqueIdentifier;
      this.deviceModel = SystemInfo.deviceModel;
      this.deviceOs = SystemInfo.operatingSystem;
      this.deviceName = SystemInfo.deviceName;
    }

    private void Awake()
    {
      TibProxy.UseDevServer = false;
      if ((UnityEngine.Object) TibProxy._instance == (UnityEngine.Object) null)
        TibProxy._instance = this;
      else if ((UnityEngine.Object) TibProxy._instance != (UnityEngine.Object) this)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
        return;
      }
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
      ClientLogger clientLogger = new ClientLogger();
      if ((bool) ((UnityEngine.Object) this.logView))
        clientLogger.loggerView = (ILoggerView) this.logView;
      clientLogger.Debug = this.libraryDebugLog;
      clientLogger.clientLogLevel = this.clientLogLevel;
      ClientLog.Instance = (IUnityLogger) clientLogger;
      Log.Instance = (TheInfiniteBlack.Library.ILogger) clientLogger;
      this._gameEventBuffer = new TibProxy.FilteredEventQueue(this.startingEventQueueSize, new Action<GameEventArgs>(this.ProcessGameEvent));
      this._eventsBeingProcessed = new GameEventArgs[this.maxEventsPerFrame];
      this.OnAwake();
    }

    protected void Start()
    {
      Application.targetFrameRate = 60;
      this.configManager.LoadAll();
    }

    public void SetUseDevServer(bool useDevServer)
    {
      TibProxy.UseDevServer = useDevServer;
    }

    [DebuggerHidden]
    private IEnumerator GetServerIpCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new TibProxy.\u003CGetServerIpCoroutine\u003Ec__Iterator2B()
      {
        \u003C\u003Ef__this = this
      };
    }

    protected virtual void OnEnable()
    {
    }

    private bool _holdEvents
    {
      get
      {
        if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<SceneManager>.Instance)
          return PreInstantiatedSingleton<SceneManager>.Instance.isLoadingMainGame;
        return false;
      }
    }

    protected virtual void Update()
    {
      if (this.state != null && this.state.Net.Connected)
        this.state.OnUpdate();
      else if ((UnityEngine.Object) PreInstantiatedSingleton<SceneManager>.Instance != (UnityEngine.Object) null)
      {
        if (PreInstantiatedSingleton<SceneManager>.Instance.activeSceneIndex <= 1)
          return;
        this.Disconnect("Network Error", true);
        return;
      }
      if (this._holdEvents)
      {
        this.LogD("Holding Events, Frame: " + (object) Time.frameCount);
      }
      else
      {
        if (!this._holdEvents && this._wasHoldingEvents)
          this._gameEventBuffer.ProcessAllPendingEvents(true);
        else
          this._gameEventBuffer.ProcessEvents(this.maxEventsPerFrame);
        this._wasHoldingEvents = this._holdEvents;
      }
    }

    protected virtual void OnApplicationPause(bool pauseStatus)
    {
      if (!pauseStatus)
        return;
      this.configManager.SaveAll();
    }

    protected void OnApplicationQuit()
    {
      Log.D((object) this, nameof (OnApplicationQuit), string.Empty);
      this.configManager.SaveAll();
      this.Disconnect("Application Quit", false);
    }

    public void CreateNewAccount(sbyte svrId, string userName, string password, bool isHardcore)
    {
      this.deviceId = SystemInfo.deviceUniqueIdentifier;
      LoginRequest loginRequest = new LoginRequest()
      {
        ServerID = svrId,
        Name = userName,
        Password = password,
        IsHardcore = isHardcore,
        IsNewAccount = true,
        DeviceID = SystemInfo.deviceUniqueIdentifier,
        DeviceModel = SystemInfo.deviceModel,
        DeviceOS = SystemInfo.operatingSystem,
        DeviceName = SystemInfo.deviceName,
        Distribution = this.distributionType,
        VersionIdentity = this.versionIdentity
      };
      this.configManager.SaveAll();
      this.StartCoroutine(this.Connect(loginRequest));
    }

    public void Login(sbyte svrId, string userName, string password)
    {
      LoginRequest loginRequest = new LoginRequest()
      {
        ServerID = svrId,
        Name = userName,
        Password = password,
        IsNewAccount = false,
        DeviceID = SystemInfo.deviceUniqueIdentifier,
        DeviceModel = SystemInfo.deviceModel,
        DeviceOS = SystemInfo.operatingSystem,
        DeviceName = SystemInfo.deviceName,
        Distribution = this.distributionType,
        VersionIdentity = this.unityClientPrefix + this.versionIdentity
      };
      this.configManager.SaveAll();
      this.StartCoroutine(this.Connect(loginRequest));
    }

    [DebuggerHidden]
    private IEnumerator Connect(LoginRequest loginRequest)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new TibProxy.\u003CConnect\u003Ec__Iterator2C()
      {
        loginRequest = loginRequest,
        \u003C\u0024\u003EloginRequest = loginRequest,
        \u003C\u003Ef__this = this
      };
    }

    public void Disconnect(string reason, bool returnToMotd)
    {
      Log.D((object) this, nameof (Disconnect), string.Format("Disconnect reason - {0}", (object) reason));
      if (this.configManager != null && this._saveSettingsOnDisconnect)
      {
        this.configManager.SaveAll();
        this._saveSettingsOnDisconnect = false;
      }
      IGameState state = this.state;
      this.state = (IGameState) null;
      if (state == null)
        return;
      if (this.onDisconnectEvent != null)
        this.onDisconnectEvent((object) this, new DisconnectEventArgs(state, reason));
      state.Disconnect(reason);
      this.serverId = (sbyte) -1;
      this.currentServerIp = string.Empty;
      if (!returnToMotd || !((UnityEngine.Object) PreInstantiatedSingleton<SceneManager>.Instance != (UnityEngine.Object) null))
        return;
      PreInstantiatedSingleton<SceneManager>.Instance.GotoLoginScene();
    }

    protected virtual void HandleGameEvent<T>(T e) where T : GameEventArgs
    {
      this._gameEventBuffer.AddEvent<T>(e);
    }

    private void ProcessGameEvent(GameEventArgs e)
    {
      if (TibProxy.mySettings != null)
      {
        EventSoundEffect eventSoundEffect = TibProxy.mySettings == null ? e.GetSound(this.configManager.systemSettings) : e.GetSound(this.configManager.systemSettings, TibProxy.mySettings);
        if (eventSoundEffect != null)
          DarkTonic.MasterAudio.MasterAudio.PlaySound(((Enum) eventSoundEffect.Type).ToString(), 1f, new float?(eventSoundEffect.Rate), 0.0f, (string) null, false, false);
      }
      if (this.onGameEvent != null)
        this.onGameEvent((object) this, e);
      switch (e.EventType)
      {
        case GameEventType.Attack:
          if (this.onAttackEvent == null)
            break;
          this.onAttackEvent((object) this, (AttackEventArgs) e);
          break;
        case GameEventType.AttackTargetChanged:
          if (this.onAttackTargetChangedEvent == null)
            break;
          this.onAttackTargetChangedEvent((object) this, (AttackTargetChangedEventArgs) e);
          break;
        case GameEventType.AuctionUpdated:
          if (this.onAuctionUpdatedEvent == null)
            break;
          this.onAuctionUpdatedEvent((object) this, (AuctionUpdatedEventArgs) e);
          break;
        case GameEventType.AuctionRemoved:
          if (this.onAuctionRemovedEvent == null)
            break;
          this.onAuctionRemovedEvent((object) this, (AuctionRemovedEventArgs) e);
          break;
        case GameEventType.BankChanged:
          if (this.onBankChangedEvent == null)
            break;
          this.onBankChangedEvent((object) this, (BankChangedEventArgs) e);
          break;
        case GameEventType.Chat:
          ChatEventArgs e1 = e as ChatEventArgs;
          if (e1 == null)
            break;
          this.LogD(string.Format("ProcessGameEvent Frame: {0} - {1} - {2}", (object) Time.frameCount, (object) e.EventType, (object) e1.Text));
          if (this.onChatEvent == null)
            break;
          this.onChatEvent((object) this, e1);
          break;
        case GameEventType.CourseSet:
          if (this.onCourseSetEvent == null)
            break;
          this.onCourseSetEvent((object) this, (CourseSetEventArgs) e);
          break;
        case GameEventType.Disconnect:
          this.Disconnect(((DisconnectEventArgs) e).Reason, true);
          break;
        case GameEventType.EntityEnter:
          this.LogD(string.Format("ProcessGameEvent Frame: {0} - {1} - {2}", (object) Time.frameCount, (object) e.EventType, (object) ((EntityArriveEventArgs) e).Source.Name));
          if (this.onEntityArriveEvent == null)
            break;
          this.onEntityArriveEvent((object) this, (EntityArriveEventArgs) e);
          break;
        case GameEventType.EntityExit:
          if (this.onEntityExitEvent == null)
            break;
          this.onEntityExitEvent((object) this, (EntityExitEventArgs) e);
          break;
        case GameEventType.EntityHullChanged:
          this.LogD(string.Format("ProcessGameEvent Frame:{0} - {1} - {2}", (object) Time.frameCount, (object) e.EventType, (object) ((EntityHullChangedEventArgs) e).Source.Name));
          if (this.onEntityHullChangedEvent == null)
            break;
          this.onEntityHullChangedEvent((object) this, (EntityHullChangedEventArgs) e);
          break;
        case GameEventType.FollowTargetChanged:
          if (this.onFollowTargetChangedEvent == null)
            break;
          this.onFollowTargetChangedEvent((object) this, (FollowTargetChangedEventArgs) e);
          break;
        case GameEventType.GainItem:
          if (this.onGainItemEvent == null)
            break;
          this.onGainItemEvent((object) this, (GainItemEventArgs) e);
          break;
        case GameEventType.GainXp:
          if (this.onGainXpEvent == null)
            break;
          this.onGainXpEvent((object) this, (GainXpEventArgs) e);
          break;
        case GameEventType.Harvest:
          if (this.onHarvestEvent == null)
            break;
          this.onHarvestEvent((object) this, (HarvestEventArgs) e);
          break;
        case GameEventType.InventoryChanged:
          if (this.onInventoryChangedEvent == null)
            break;
          this.onInventoryChangedEvent((object) this, (InventoryChangedEventArgs) e);
          break;
        case GameEventType.LocationChanged:
          if (this.onLocationChangedEvent == null)
            break;
          this.onLocationChangedEvent((object) this, (LocationChangedEventArgs) e);
          break;
        case GameEventType.LoginSuccess:
          this.LogD(string.Format("ProcessGameEvent Frame: {0} - {1}", (object) Time.frameCount, (object) e.EventType));
          if (this.onLoginSuccessEvent == null)
            break;
          this.onLoginSuccessEvent((object) this, (LoginSuccessEventArgs) e);
          break;
        case GameEventType.MoneyChanged:
          if (this.onMoneyChangeEvent == null)
            break;
          this.onMoneyChangeEvent((object) this, (MoneyChangeEventArgs) e);
          break;
        case GameEventType.MovementChanged:
          if (this.onMovementStatusChangedEvent == null)
            break;
          this.onMovementStatusChangedEvent((object) this, (MovementStatusChangedEventArgs) e);
          break;
        case GameEventType.PlayerOnlineStatus:
          if (this.onPlayerOnlineStatusChangedEvent == null)
            break;
          this.onPlayerOnlineStatusChangedEvent((object) this, (PlayerOnlineStatusChangedEventArgs) e);
          break;
        case GameEventType.AcceptDeclineWindow:
          this.LogD(string.Format("ProcessGameEvent Frame: {0} - {1} - {2}", (object) Time.frameCount, (object) e.EventType, (object) ((AcceptDeclineWindowEventArgs) e).Text));
          if (this.onAcceptDeclineWindowsEvent == null)
            break;
          this.onAcceptDeclineWindowsEvent((object) this, (AcceptDeclineWindowEventArgs) e);
          break;
        case GameEventType.PopupWindow:
          this.LogD(string.Format("ProcessGameEvent Frame: {0} - {1} - {2}", (object) Time.frameCount, (object) e.EventType, (object) ((PopupWindowEventArgs) e).Text));
          if (this.onPopupWindowEvent == null)
            break;
          this.onPopupWindowEvent((object) this, (PopupWindowEventArgs) e);
          break;
        case GameEventType.PvPFlagChange:
          if (this.onPvPFlagChangeEvent == null)
            break;
          this.onPvPFlagChangeEvent((object) this, (PvPFlagChangeEventArgs) e);
          break;
        case GameEventType.ResourcesChanged:
          if (this.onResourcesChangedEvent == null)
            break;
          this.onResourcesChangedEvent((object) this, (ResourcesChangedEventArgs) e);
          break;
        case GameEventType.Trade:
          if (this.onTradeEvent == null)
            break;
          this.onTradeEvent((object) this, (TradeEventArgs) e);
          break;
        case GameEventType.TradeCancelled:
          if (this.onTradeFailedEvent == null)
            break;
          this.onTradeFailedEvent((object) this, (TradeFailedEventArgs) e);
          break;
        case GameEventType.ShowEntityDetail:
          if (this.onShowEntityDetailEvent == null)
            break;
          this.onShowEntityDetailEvent((object) this, (ShowEntityDetailEventArgs) e);
          break;
        case GameEventType.CombatFlash:
          if (this.onCombatFlashEvent == null)
            break;
          this.onCombatFlashEvent((object) this, (CombatFlashEventArgs) e);
          break;
        case GameEventType.Sound:
          if (this.onSoundEvent == null)
            break;
          this.onSoundEvent((object) this, (SoundEventArgs) e);
          break;
      }
    }

    public virtual void Show(DisconnectEventArgs e)
    {
      Log.I((object) this, nameof (Show), string.Format("DisconnectEventArgs: {0}", (object) e.Reason));
      this.Disconnect(e.Reason, true);
    }

    public virtual void Show(LoginSuccessEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(LoginSuccessEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.LogD(string.Format("Show(LoginSuccessEventArgs) Frame: {0}", (object) Time.frameCount));
      this.HandleGameEvent<LoginSuccessEventArgs>(e);
      this.Show(new ChatEventArgs(e.State, e.ToString(), string.Empty, string.Empty, ChatType.EVENT));
    }

    public virtual void Show(PopupWindowEventArgs e)
    {
      this.LogD(string.Format("Show(PopupEventArgs) Frame: {0} - {1}", (object) Time.frameCount, (object) e.Text));
      if (!this._holdEvents)
        this.ProcessGameEvent((GameEventArgs) e);
      else
        this.HandleGameEvent<PopupWindowEventArgs>(e);
    }

    public virtual void Show(AcceptDeclineWindowEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(AcceptDeclineWindowEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.LogD(string.Format("TibProxy.Show(AcceptDeclineEventArgs) Frame: {0} - {1}", (object) Time.frameCount, (object) e.Text));
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(AttackEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(AttackEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.HandleGameEvent<AttackEventArgs>(e);
    }

    public virtual void Show(EntityArriveEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(EntityArriveEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      if (!this.state.IsLoggedIn)
        return;
      this.LogD(string.Format("TibProxy.Show(EntityArriveEventArgs) Frame: {0} - {1}", (object) Time.frameCount, (object) e.Source.Name));
      this.HandleGameEvent<EntityArriveEventArgs>(e);
    }

    public virtual void Show(EntityExitEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(EntityExitEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.HandleGameEvent<EntityExitEventArgs>(e);
    }

    public virtual void Show(EntityHullChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(EntityHullChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.LogD(string.Format("TibProxy.Show(EntityHullChangeEventArgs) Frame: {0} - {1}", (object) Time.frameCount, (object) e.Source.Name));
      this.HandleGameEvent<EntityHullChangedEventArgs>(e);
    }

    public virtual void Show(ChatEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State) && e.State.IsLoggedIn)
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(ChatEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.LogD(string.Format("TibProxy.Show(EntityChatEventArgs) Frame: {0} - {1}", (object) Time.frameCount, (object) e.Text));
      this.HandleGameEvent<ChatEventArgs>(e);
    }

    public virtual void Show(LocationChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(LocationChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.LogD(string.Format("TibProxy.Show(LocationChangedEventArgs) Frame: {0} - {1}", (object) Time.frameCount, (object) e.get_Entites().Count));
      this.HandleGameEvent<LocationChangedEventArgs>(e);
    }

    public virtual void Show(PvPFlagChangeEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(PvPFlagChangeEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.HandleGameEvent<PvPFlagChangeEventArgs>(e);
    }

    public virtual void Show(GainXpEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(GainXpEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      Log.I((object) this, nameof (Show), "GainXpEventArgs");
      this.HandleGameEvent<GainXpEventArgs>(e);
    }

    public virtual void Show(GainItemEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(GainItemEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(MoneyChangeEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(MoneyChangeEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(InventoryChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(InventoryChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(ResourcesChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(ResourcesChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(PlayerOnlineStatusChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(PlayerOnlineStatusChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.HandleGameEvent<PlayerOnlineStatusChangedEventArgs>(e);
    }

    public virtual void Show(HarvestEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(HarvestEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(BankChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(BankChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(FollowTargetChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(FollowTargetChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(AttackTargetChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(AttackTargetChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(TradeEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(TradeEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(TradeFailedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(TradeFailedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(CourseSetEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(CourseSetEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(MovementStatusChangedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(MovementStatusChangedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(AuctionUpdatedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(AuctionUpdatedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(AuctionRemovedEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(AuctionRemovedEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(ShowEntityDetailEventArgs e)
    {
      if (!object.ReferenceEquals((object) this.state, (object) e.State))
      {
        ApplicationException applicationException = new ApplicationException("Proxy GameState not equal to event GameState");
        Log.E((object) this, "Show(ShowEntityDetailEventArgs)", (Exception) applicationException);
        throw applicationException;
      }
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(CombatFlashEventArgs e)
    {
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(SoundEventArgs e)
    {
      this.ProcessGameEvent((GameEventArgs) e);
    }

    public virtual void Show(ShowUrlEventArgs e)
    {
      if (this.onShowUrlEvent == null)
        return;
      this.onShowUrlEvent((object) this, e);
    }

    public void Show(AchievementEventArgs e)
    {
      if (TibProxy.UseDevServer)
        return;
      this.HandleGameEvent<AchievementEventArgs>(e);
      if (this.distributionType != DistributionType.Steam_Windows && this.distributionType != DistributionType.Steam_Linux && this.distributionType != DistributionType.Steam_OSX || !((UnityEngine.Object) null != (UnityEngine.Object) SbSteamComponent<SbSteamAchievements>.Instance))
        return;
      SbSteamComponent<SbSteamAchievements>.Instance.UnlockAchievement(e.Achievement);
    }

    public class FilteredEventQueue
    {
      private GameEventArgs[] _bufferedEvents;
      private GameEventArgs[] _tmpBuffer;
      private readonly Action<GameEventArgs> onProcessGameEvent;

      public FilteredEventQueue(int maxBufferSize, Action<GameEventArgs> onProcessGameEvent)
      {
        this._bufferedEvents = new GameEventArgs[maxBufferSize];
        this._tmpBuffer = new GameEventArgs[maxBufferSize];
        this.maxBufferSize = maxBufferSize;
        this.onProcessGameEvent = onProcessGameEvent;
        this.count = 0;
        this.ClearBuffer(this._bufferedEvents);
        this.ClearBuffer(this._tmpBuffer);
      }

      private bool _holdEvents
      {
        get
        {
          if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<SceneManager>.Instance)
            return PreInstantiatedSingleton<SceneManager>.Instance.isLoadingMainGame;
          return false;
        }
      }

      public int count { get; private set; }

      public int maxBufferSize { get; private set; }

      public void AddEvent<T>(T e) where T : GameEventArgs
      {
        if (this.maxBufferSize < this.count + 1)
          this.PruneBufferedEvents();
        if (this.maxBufferSize < this.count + 1)
          this.GrowBuffer(Mathf.CeilToInt((float) this.maxBufferSize * 0.1f));
        this._bufferedEvents[this.count] = (GameEventArgs) e;
        ++this.count;
      }

      public override string ToString()
      {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.AppendLine("  buffer size: " + (object) this.count);
        stringBuilder.AppendLine("buffer length: " + (object) this._bufferedEvents.Length);
        stringBuilder.AppendLine(" buffer count: " + (object) ((IEnumerable<GameEventArgs>) this._bufferedEvents).Count<GameEventArgs>((Func<GameEventArgs, bool>) (o => o != null)));
        stringBuilder.AppendLine("   tmp length: " + (object) this._tmpBuffer.Length);
        stringBuilder.AppendLine("    tmp count: " + (object) ((IEnumerable<GameEventArgs>) this._tmpBuffer).Count<GameEventArgs>((Func<GameEventArgs, bool>) (o => o != null)));
        return stringBuilder.ToString();
      }

      public void Clear()
      {
        this.ClearBuffer(this._tmpBuffer);
        this.ClearBuffer(this._bufferedEvents);
      }

      private void GrowBuffer(int capIncrease)
      {
        this.maxBufferSize += capIncrease;
        GameEventArgs[] gameEventArgsArray1 = new GameEventArgs[this.maxBufferSize];
        GameEventArgs[] gameEventArgsArray2 = new GameEventArgs[this.maxBufferSize];
        for (int index = 0; index < this.maxBufferSize; ++index)
        {
          gameEventArgsArray1[index] = (GameEventArgs) null;
          gameEventArgsArray2[index] = (GameEventArgs) null;
        }
        this._bufferedEvents.CopyTo((Array) gameEventArgsArray1, 0);
        this._tmpBuffer.CopyTo((Array) gameEventArgsArray2, 0);
        this._bufferedEvents = gameEventArgsArray1;
        this._tmpBuffer = gameEventArgsArray2;
      }

      private void PruneBufferedEvents()
      {
        int num1 = Mathf.CeilToInt((float) this.maxBufferSize * 0.1f);
        int num2 = 0;
        int index1 = 0;
        this.count = 0;
        for (int index2 = 0; index2 < this._bufferedEvents.Length; ++index2)
        {
          if (this._bufferedEvents[index2] != null)
          {
            switch (this._bufferedEvents[index2].EventType)
            {
              case GameEventType.EntityEnter:
              case GameEventType.EntityExit:
              case GameEventType.EntityHullChanged:
              case GameEventType.Attack:
                if (num2 < num1)
                {
                  ++num2;
                  continue;
                }
                this._tmpBuffer[index1] = this._bufferedEvents[index2];
                ++index1;
                ++this.count;
                continue;
              default:
                this._tmpBuffer[index1] = this._bufferedEvents[index2];
                ++index1;
                ++this.count;
                continue;
            }
          }
        }
        GameEventArgs[] tmpBuffer = this._tmpBuffer;
        this._tmpBuffer = this._bufferedEvents;
        this._bufferedEvents = tmpBuffer;
        this.ClearBuffer(this._tmpBuffer);
      }

      private void ClearBuffer(GameEventArgs[] buffer)
      {
        for (int index = 0; index < buffer.Length; ++index)
          buffer[index] = (GameEventArgs) null;
        if (!object.ReferenceEquals((object) this._bufferedEvents, (object) buffer))
          return;
        this.count = 0;
      }

      public void ProcessEvents(int maxProcessCount)
      {
        int num = 0;
        for (int index = 0; index < this.maxBufferSize; ++index)
        {
          if (!this._holdEvents && num < maxProcessCount && this._bufferedEvents[index] != null)
          {
            GameEventArgs bufferedEvent = this._bufferedEvents[index];
            if (this.onProcessGameEvent != null)
              this.onProcessGameEvent(bufferedEvent);
            this._bufferedEvents[index] = (GameEventArgs) null;
            ++num;
          }
          if (index >= num)
          {
            this._bufferedEvents[index - num] = this._bufferedEvents[index];
            this._bufferedEvents[index] = (GameEventArgs) null;
          }
        }
      }

      public void ProcessAllPendingEvents(bool clearProcessedEvents)
      {
        for (int index = 0; index < this._bufferedEvents.Length && !this._holdEvents; ++index)
        {
          if (this._bufferedEvents[index] != null)
          {
            GameEventArgs bufferedEvent = this._bufferedEvents[index];
            if (clearProcessedEvents)
              this._bufferedEvents[index] = (GameEventArgs) null;
            if (this.onProcessGameEvent != null)
              this.onProcessGameEvent(bufferedEvent);
          }
        }
      }

      public GameEventArgs[] GetAllPendingEvents(bool clearPendingEvents)
      {
        GameEventArgs[] gameEventArgsArray = new GameEventArgs[this._bufferedEvents.Length];
        this._bufferedEvents.CopyTo((Array) gameEventArgsArray, 0);
        if (clearPendingEvents)
          this.ClearBuffer(this._bufferedEvents);
        return gameEventArgsArray;
      }
    }
  }
}
