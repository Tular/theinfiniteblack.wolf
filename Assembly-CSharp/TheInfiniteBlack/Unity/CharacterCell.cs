﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.CharacterCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Globalization;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [RequireComponent(typeof (UISpriteColorGroupController))]
  [RequireComponent(typeof (UIToggle))]
  public class CharacterCell : MonoBehaviour
  {
    public string shipSpritePrefix = "entity_ship_";
    public UILabel accountNameLabel;
    public UILabel corpLabel;
    public UILabel lastLoginLabel;
    public UILabel levelLabel;
    public UILabel shipTypeLabel;
    public UILabel hardCoreLabel;
    public UISprite shipSprite;
    public int focusedColorGroup;
    public float focusDuration;
    public Color focusedColor;
    public Color normalColor;
    private AccountCharacterTuple _charInfo;
    private UISpriteColorGroupController _colorController;

    public UIToggle toggle { get; private set; }

    public int toggleGroup
    {
      get
      {
        return this.toggle.group;
      }
      set
      {
        this.toggle.group = value;
        this.toggle.enabled = true;
      }
    }

    public string accountName
    {
      get
      {
        return this._charInfo.accountName;
      }
    }

    public string accountPassword
    {
      get
      {
        return this._charInfo.accountPassword;
      }
    }

    public bool isPasswordSaved
    {
      get
      {
        return this._charInfo.isPasswordSaved;
      }
    }

    public void Initialize(AccountCharacterTuple charInfo)
    {
      this.name = charInfo.accountName;
      this._charInfo = charInfo;
      this.accountNameLabel.text = charInfo.accountName;
      this.levelLabel.text = charInfo.character.Level.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      this.levelLabel.text = string.Format("{0:0.00}", (object) charInfo.character.Level);
      this.shipTypeLabel.text = ((Enum) charInfo.character.Ship).ToString();
      this.corpLabel.text = !string.IsNullOrEmpty(charInfo.character.Corporation) ? charInfo.character.Corporation : string.Empty;
      this.lastLoginLabel.text = !DateTime.MinValue.Equals(charInfo.character.get_LastActivity()) ? string.Format("{0:MM/dd/yy}", (object) charInfo.character.get_LastActivity()) : string.Empty;
      ShipClass ship = charInfo.character.Ship;
      if (((Enum) ship).Equals((object) ShipClass.NULL) || ((Enum) ship).Equals((object) ShipClass.None) || ((Enum) ship).Equals((object) ShipClass.LAST))
      {
        this.shipSprite.enabled = false;
      }
      else
      {
        this.hardCoreLabel.enabled = charInfo.account.Hardcore;
        this.shipSprite.spriteName = string.Format("{0}{1}", (object) this.shipSpritePrefix, (object) ship);
        this.shipSprite.enabled = true;
      }
    }

    public void OnPress(bool isPressed)
    {
      if (!isPressed || !this.toggle.value)
        return;
      UIToggle.current = this.toggle;
      EventDelegate.Execute(this.toggle.onChange);
      UIToggle.current = (UIToggle) null;
    }

    private void Awake()
    {
      if (!(bool) ((UnityEngine.Object) this._colorController))
        this._colorController = this.GetComponent<UISpriteColorGroupController>();
      if (!(bool) ((UnityEngine.Object) this.toggle))
        this.toggle = this.GetComponent<UIToggle>();
      this.toggle.enabled = false;
    }

    private void OnSpawned()
    {
      TheInfiniteBlack.Library.Log.D((object) this, nameof (OnSpawned), string.Empty);
      this._colorController.SetColor(this.focusedColorGroup, 0.0f, this.normalColor);
      EventDelegate.Add(this.toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged), false);
    }

    private void OnDespawned()
    {
      TheInfiniteBlack.Library.Log.D((object) this, nameof (OnDespawned), string.Empty);
      this.toggle.onChange.Clear();
      this.toggle.enabled = false;
      this.toggle.optionCanBeNone = true;
      this.toggle.value = false;
      this.toggle.optionCanBeNone = false;
      this._colorController.SetColor(this.focusedColorGroup, 0.0f, this.normalColor);
      this.hardCoreLabel.enabled = false;
      this.accountNameLabel.text = string.Empty;
      this.levelLabel.text = string.Empty;
      this.levelLabel.text = string.Empty;
      this.shipTypeLabel.text = string.Empty;
      this.corpLabel.text = string.Empty;
      this.lastLoginLabel.text = string.Empty;
      this.name = "Clone";
    }

    private void OnToggleChanged()
    {
      if (this.toggle.value)
        this._colorController.SetColor(this.focusedColorGroup, this.focusDuration, this.focusedColor);
      else
        this._colorController.SetColor(this.focusedColorGroup, this.focusDuration, this.normalColor);
    }
  }
}
