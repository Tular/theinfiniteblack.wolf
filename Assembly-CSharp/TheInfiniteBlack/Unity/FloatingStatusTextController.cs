﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.FloatingStatusTextController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class FloatingStatusTextController : MonoBehaviour
  {
    public UIGrid statusTextRoot;
    public UITable statusTextTable;
    public FloatingStatusText attackStatus;
    public FloatingStatusText courseStatus;
    public FloatingStatusText followStatus;
    public FloatingStatusText harvestStatus;
    private int _nextRepositionFrame;
    private bool _reposition;

    private void Update()
    {
      if (TibProxy.gameState == null || !TibProxy.gameState.IsLoggedIn)
        return;
      IGameState gameState = TibProxy.gameState;
      this.attackStatus.UpdateStatusText(gameState.AttackStatusHeader, gameState.AttackStatus);
      this.followStatus.UpdateStatusText(gameState.FollowStatusHeader, gameState.FollowStatus);
      this.courseStatus.UpdateStatusText(gameState.CourseStatusHeader, gameState.CourseStatus);
      this.harvestStatus.UpdateStatusText(gameState.HarvestStatus);
      this.statusTextRoot.Reposition();
    }
  }
}
