﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SBUITooltipListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class SBUITooltipListener : MonoBehaviour
  {
    public SBUIToolTip toolTip;
    public SBUITooltipListener.ShowEvent showOn;

    private void Awake()
    {
      if ((bool) ((Object) this.toolTip))
        return;
      this.toolTip = this.GetComponentInChildren<SBUIToolTip>();
    }

    private void OnHover(bool isOver)
    {
      if (this.showOn != SBUITooltipListener.ShowEvent.OnHover || !(bool) ((Object) this.toolTip))
        return;
      this.toolTip.OnTooltip(isOver);
    }

    private void OnTooltip(bool show)
    {
      if (this.showOn != SBUITooltipListener.ShowEvent.OnTooltip || !(bool) ((Object) this.toolTip))
        return;
      this.toolTip.OnTooltip(show);
    }

    public enum ShowEvent
    {
      OnHover,
      OnTooltip,
    }
  }
}
