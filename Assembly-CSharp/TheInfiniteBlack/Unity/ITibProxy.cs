﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ITibProxy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Unity
{
  public interface ITibProxy : IUserInterface
  {
    event EventHandler<GameEventArgs> onGameEvent;

    event EventHandler<DisconnectEventArgs> onDisconnectEvent;

    event EventHandler<LoginSuccessEventArgs> onLoginSuccessEvent;

    event EventHandler<AcceptDeclineWindowEventArgs> onAcceptDeclineWindowsEvent;

    event EventHandler<PopupWindowEventArgs> onPopupWindowEvent;

    event EventHandler<EntityArriveEventArgs> onEntityArriveEvent;

    event EventHandler<EntityExitEventArgs> onEntityExitEvent;

    event EventHandler<EntityHullChangedEventArgs> onEntityHullChangedEvent;

    event EventHandler<AttackEventArgs> onAttackEvent;

    event EventHandler<ChatEventArgs> onChatEvent;

    event EventHandler<LocationChangedEventArgs> onLocationChangedEvent;

    event EventHandler<PvPFlagChangeEventArgs> onPvPFlagChangeEvent;

    event EventHandler<GainXpEventArgs> onGainXpEvent;

    event EventHandler<GainItemEventArgs> onGainItemEvent;

    event EventHandler<MoneyChangeEventArgs> onMoneyChangeEvent;

    event EventHandler<InventoryChangedEventArgs> onInventoryChangedEvent;

    event EventHandler<ResourcesChangedEventArgs> onResourcesChangedEvent;

    event EventHandler<PlayerOnlineStatusChangedEventArgs> onPlayerOnlineStatusChangedEvent;

    event EventHandler<HarvestEventArgs> onHarvestEvent;

    event EventHandler<BankChangedEventArgs> onBankChangedEvent;

    event EventHandler<FollowTargetChangedEventArgs> onFollowTargetChangedEvent;

    event EventHandler<AttackTargetChangedEventArgs> onAttackTargetChangedEvent;

    event EventHandler<TradeEventArgs> onTradeEvent;

    event EventHandler<TradeFailedEventArgs> onTradeFailedEvent;

    event EventHandler<CourseSetEventArgs> onCourseSetEvent;

    event EventHandler<MovementStatusChangedEventArgs> onMovementStatusChangedEvent;

    event EventHandler<AuctionUpdatedEventArgs> onAuctionUpdatedEvent;

    event EventHandler<AuctionRemovedEventArgs> onAuctionRemovedEvent;

    event EventHandler<ShowEntityDetailEventArgs> onShowEntityDetailEvent;

    event EventHandler<CombatFlashEventArgs> onCombatFlashEvent;

    event EventHandler<SoundEventArgs> onSoundEvent;

    event EventHandler<ShowUrlEventArgs> onShowUrlEvent;

    string clientVersion { get; }

    DistributionType distribution { get; }

    bool isSteamDistribution { get; }

    bool isInDebugMode { get; }

    sbyte serverId { get; }

    string currentServerWebFile { get; }

    string currentServerIp { get; }

    bool isUsingDevServer { get; }

    void SetUseDevServer(bool isDev);

    void Login(sbyte serverId, string userName, string password);

    void CreateNewAccount(sbyte serverId, string userName, string password, bool isHardcore);

    void Disconnect(string reason, bool returnToMotd);
  }
}
