﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.LoggerView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.Text;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public abstract class LoggerView : MonoBehaviour, ILoggerView
  {
    public float refreshFrequency = 1f;
    private StringBuilder _pendingOutput;

    public void AddLine(string line)
    {
      this._pendingOutput.AppendLine(line);
    }

    protected abstract void UpdateLogView(string line);

    protected virtual void Awake()
    {
      this._pendingOutput = new StringBuilder();
    }

    protected void Start()
    {
      this.StartCoroutine(this.UpdateViewTextCoroutine());
    }

    [DebuggerHidden]
    private IEnumerator UpdateViewTextCoroutine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new LoggerView.\u003CUpdateViewTextCoroutine\u003Ec__Iterator2A()
      {
        \u003C\u003Ef__this = this
      };
    }
  }
}
