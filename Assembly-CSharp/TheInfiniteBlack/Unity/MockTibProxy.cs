﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockTibProxy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Client.Settings;
using TheInfiniteBlack.Library.Entities;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class MockTibProxy : TibProxy
  {
    private static int _nextEntityId;
    public bool autoLogin;
    public string playerName;
    public bool executeDiskIO;
    public bool isEnabledA;
    public bool isEnabledB;
    public bool isEnabledC;
    public bool isEnabledD;
    public bool isLoggedIn;
    private Account _testAccount;
    private MockGameState _mockState;
    private MockNetwork _mockNetwork;
    private MockClientPlayer _mockPlayer;
    private MockShip _mockPlayerShip;
    private MockConfigManager _mockConfigManager;

    public static int nextEntityId
    {
      get
      {
        return ++MockTibProxy._nextEntityId;
      }
    }

    public static MockTibProxy MockInstance
    {
      get
      {
        return TibProxy.Instance as MockTibProxy;
      }
    }

    public MockLocalEntities localEntities
    {
      get
      {
        return this._mockState.moqLocal;
      }
    }

    public MockGameState moqGameState
    {
      get
      {
        return this._mockState;
      }
    }

    protected override void OnAwake()
    {
      this._mockConfigManager = new MockConfigManager()
      {
        executeDiskIO = this.executeDiskIO
      };
      this.configManager = (IConfigManager) this._mockConfigManager;
      this._testAccount = new Account()
      {
        Name = this.playerName
      };
      this.configManager.accountManager.get_Accounts().Add(this._testAccount);
      this._mockNetwork = new MockNetwork();
      this._mockState = new MockGameState((IUserInterface) this, (INetwork) this._mockNetwork, this.configManager.accountManager, new LoginRequest());
      this._mockPlayerShip = MockShip.GenerateRandom((IGameState) this._mockState);
      this._mockPlayer = this._mockPlayerShip.moqPlayer;
      this._mockState.SetMyAccount(this._testAccount).SetMyLocation(new MockSector(40, 40, false));
      this.state = (IGameState) this._mockState;
      UnityEngine.Debug.Log((object) string.Format("Show Asteroids D: {0}", (object) this.state.MySettings.EntityList.ColumnB.Asteroid.Value));
    }

    [ContextMenu("Execute Login")]
    public void Login()
    {
      if (this.state.IsLoggedIn)
        UnityEngine.Debug.Log((object) string.Format("MockTibProxy.Login() - Already logged in as: {0}", (object) this.state.MyPlayer.Name));
      else
        this._mockState.ExecuteOnLogin(this._mockPlayer, this._mockPlayerShip);
    }

    public void Trigger_OnLoginSuccessEvent()
    {
      this.Show(new LoginSuccessEventArgs(this.state, (ClientPlayer) null, (Ship) null));
    }

    protected override void HandleGameEvent<T>(T e)
    {
      base.HandleGameEvent<T>(e);
    }

    [DebuggerHidden]
    private IEnumerator Start()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new MockTibProxy.\u003CStart\u003Ec__Iterator32()
      {
        \u003C\u003Ef__this = this
      };
    }

    protected override void Update()
    {
      this.state.OnUpdate();
      this.isLoggedIn = this.state.IsLoggedIn;
      this.isEnabledA = this.state.MySettings.EntityList.ColumnA.Enabled;
      this.isEnabledB = this.state.MySettings.EntityList.ColumnB.Enabled;
      this.isEnabledC = this.state.MySettings.EntityList.ColumnC.Enabled;
      this.isEnabledD = this.state.MySettings.EntityList.ColumnD.Enabled;
    }
  }
}
