﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SceneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Text;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  [AdvancedInspector.AdvancedInspector]
  [Serializable]
  public class SceneInfo
  {
    [Inspect(0)]
    public string sceneAlias;
    [SerializeField]
    private int _sceneBuildIndex;
    [SerializeField]
    private string _scenePath;

    [Inspect(1)]
    public string scenePath
    {
      get
      {
        return this._scenePath;
      }
      set
      {
        this._scenePath = value;
      }
    }

    [ReadOnly]
    [Inspect(2)]
    public int sceneBuildIndex
    {
      get
      {
        return this._sceneBuildIndex;
      }
    }

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (string.IsNullOrEmpty(this.sceneAlias))
        stringBuilder.Append("[NO ALIAS SPECIFIED]");
      else
        stringBuilder.Append(this.sceneAlias);
      if (string.IsNullOrEmpty(this.scenePath))
        stringBuilder.Append(" [NO SCENE PATH]");
      return stringBuilder.ToString();
    }
  }
}
