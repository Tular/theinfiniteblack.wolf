﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.PreInstantiatedSingleton`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public abstract class PreInstantiatedSingleton<T> : MonoBehaviour where T : PreInstantiatedSingleton<T>
  {
    private static T _instance;

    public static T Instance
    {
      get
      {
        return PreInstantiatedSingleton<T>._instance;
      }
    }

    protected virtual void Awake()
    {
      if ((Object) PreInstantiatedSingleton<T>._instance != (Object) null && (Object) PreInstantiatedSingleton<T>._instance != (Object) this)
      {
        Object.Destroy((Object) this.gameObject);
      }
      else
      {
        if ((Object) PreInstantiatedSingleton<T>._instance == (Object) null)
          PreInstantiatedSingleton<T>._instance = this.gameObject.GetComponent<T>();
        Object.DontDestroyOnLoad((Object) this.gameObject);
      }
    }
  }
}
