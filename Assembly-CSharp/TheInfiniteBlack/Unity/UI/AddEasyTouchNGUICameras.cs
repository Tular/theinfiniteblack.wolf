﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.AddEasyTouchNGUICameras
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (EasyTouch))]
  public class AddEasyTouchNGUICameras : MonoBehaviour
  {
    public LayerMask[] forLayers;

    private void Start()
    {
      EasyTouch component = this.GetComponent<EasyTouch>();
      for (int index = 0; index < this.forLayers.Length; ++index)
      {
        Debug.Log((object) string.Format("add camera for layer: {0}, {1}", (object) this.forLayers[index], (object) this.forLayers[index].value));
        UICamera cameraForLayer = AddEasyTouchNGUICameras.FindCameraForLayer((int) this.forLayers[index]);
        if ((Object) cameraForLayer == (Object) null)
        {
          Debug.Log((object) string.Format("------- cam is null"));
        }
        else
        {
          component.nGUILayers = (LayerMask) (component.nGUILayers.value | this.forLayers[index].value);
          component.nGUICameras.Add(cameraForLayer.GetComponent<Camera>());
        }
      }
    }

    public static UICamera FindCameraForLayer(int layerMask)
    {
      for (int index = 0; index < UICamera.list.size; ++index)
      {
        UICamera uiCamera = UICamera.list.buffer[index];
        Camera cachedCamera = uiCamera.cachedCamera;
        if ((Object) cachedCamera != (Object) null && (cachedCamera.cullingMask & layerMask) != 0)
          return uiCamera;
      }
      return (UICamera) null;
    }
  }
}
