﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.DisableIfUICameraExists
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UICamera))]
  public class DisableIfUICameraExists : MonoBehaviour
  {
    private void Awake()
    {
      this.GetComponent<UICamera>();
      Camera cameraForLayer = NGUITools.FindCameraForLayer(this.gameObject.layer);
      if (!((Object) cameraForLayer != (Object) null) || cameraForLayer.gameObject.Equals((object) this.gameObject))
        return;
      NGUITools.Destroy((Object) this.gameObject);
    }
  }
}
