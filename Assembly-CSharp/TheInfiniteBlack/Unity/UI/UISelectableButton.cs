﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UISelectableButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UIWidget))]
  public class UISelectableButton : MonoBehaviour
  {
    public UIWidget buttonLabel;
    public UIToggle toggle;
    public Color fontColorNormal;
    public Color fontColorPressed;
    public Color fontColorHover;
    public Color fontColorSelected;
    public Color fontColorDisabled;
    public bool toggleSelf;
    [HideInInspector]
    public UISprite target;
    [HideInInspector]
    public string normalSprite;
    [HideInInspector]
    public string pressedSprite;
    [HideInInspector]
    public string hoverSprite;
    [HideInInspector]
    public string selectedSprite;
    [HideInInspector]
    public string disabledSprite;
    [HideInInspector]
    public List<EventDelegate> onSelected;
    [HideInInspector]
    public List<EventDelegate> onDeSelected;
    private UISelectableButton.ButtonState _state;
    public bool _isSelected;
    private bool _isDragging;
    private bool _hasChanged;

    public bool isSelected
    {
      get
      {
        return this.state == UISelectableButton.ButtonState.Selected;
      }
    }

    public UISelectableButton.ButtonState state
    {
      get
      {
        return this._state;
      }
      set
      {
        if (this._state == value)
          return;
        this._state = value;
        this.UpdateState(true);
      }
    }

    public void Select()
    {
      if ((Object) this.toggle != (Object) null)
        this.toggle.value = true;
      else
        this.state = UISelectableButton.ButtonState.Selected;
    }

    public void DeSelect()
    {
      if ((Object) this.toggle != (Object) null)
        this.toggle.value = false;
      else
        this.state = UISelectableButton.ButtonState.Normal;
    }

    public void Enable()
    {
      this.enabled = true;
      this.state = UISelectableButton.ButtonState.Normal;
    }

    public void Disable()
    {
      this.state = UISelectableButton.ButtonState.Disabled;
    }

    public void OnToggleChanged()
    {
      if ((Object) this.toggle == (Object) null)
        return;
      this.state = !UIToggle.current.value ? UISelectableButton.ButtonState.Normal : UISelectableButton.ButtonState.Selected;
    }

    protected virtual void OnEnable()
    {
      if ((Object) this.toggle == (Object) null)
        this.toggle = this.GetComponent<UIToggle>();
      if ((Object) this.toggle != (Object) null)
        EventDelegate.Add(this.toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
      if (!((Object) this.GetComponent<Collider>() != (Object) null))
        return;
      this.GetComponent<Collider>().enabled = true;
    }

    protected virtual void OnDisable()
    {
      if ((Object) this.toggle != (Object) null)
        EventDelegate.Remove(this.toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
      if (!((Object) this.GetComponent<Collider>() != (Object) null))
        return;
      this.GetComponent<Collider>().enabled = false;
    }

    public void SetState(UISelectableButton.ButtonState initState)
    {
      this._state = initState;
      this.UpdateState(false);
    }

    public void UpdateState(bool executeCallbacks)
    {
      switch (this._state)
      {
        case UISelectableButton.ButtonState.Normal:
          this.target.spriteName = this.normalSprite;
          this.buttonLabel.color = this.fontColorNormal;
          if (executeCallbacks && this._isSelected && EventDelegate.IsValid(this.onDeSelected))
            EventDelegate.Execute(this.onDeSelected);
          this._isSelected = false;
          break;
        case UISelectableButton.ButtonState.Pressed:
          this.target.spriteName = this.pressedSprite;
          this.buttonLabel.color = this.fontColorPressed;
          break;
        case UISelectableButton.ButtonState.Hover:
          this.target.spriteName = this.hoverSprite;
          this.buttonLabel.color = this.fontColorHover;
          break;
        case UISelectableButton.ButtonState.Selected:
          this.target.spriteName = this.selectedSprite;
          this.buttonLabel.color = this.fontColorSelected;
          this._isSelected = true;
          if (executeCallbacks && EventDelegate.IsValid(this.onSelected))
          {
            EventDelegate.Execute(this.onSelected);
            break;
          }
          break;
        case UISelectableButton.ButtonState.Disabled:
          this.target.spriteName = this.disabledSprite;
          this.buttonLabel.color = this.fontColorDisabled;
          this.enabled = false;
          break;
      }
      this.target.MakePixelPerfect();
    }

    protected void OnHover(bool isOver)
    {
      if (this.state == UISelectableButton.ButtonState.Disabled || this._isSelected)
        return;
      this.state = !isOver ? UISelectableButton.ButtonState.Normal : UISelectableButton.ButtonState.Hover;
    }

    protected void OnDrag(Vector2 delta)
    {
      if (this.state != UISelectableButton.ButtonState.Disabled && this.state == UISelectableButton.ButtonState.Pressed)
        this.state = UISelectableButton.ButtonState.Normal;
      this._isDragging = true;
    }

    protected void OnClick()
    {
      if (this.state == UISelectableButton.ButtonState.Disabled || this._isSelected && !this.toggleSelf)
        return;
      this.state = !this._isSelected ? UISelectableButton.ButtonState.Selected : UISelectableButton.ButtonState.Normal;
    }

    protected void OnPress(bool isDown)
    {
      if (this.state == UISelectableButton.ButtonState.Disabled || this._isSelected && !this.toggleSelf || (!isDown || string.IsNullOrEmpty(this.pressedSprite)))
        return;
      this.state = UISelectableButton.ButtonState.Pressed;
    }

    public enum ButtonState
    {
      Normal,
      Pressed,
      Hover,
      Selected,
      Disabled,
    }
  }
}
