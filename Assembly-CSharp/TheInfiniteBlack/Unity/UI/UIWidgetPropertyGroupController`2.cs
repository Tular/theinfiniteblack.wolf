﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIWidgetPropertyGroupController`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public abstract class UIWidgetPropertyGroupController<T, V> : MonoBehaviour where T : UIWidgetPropertyGroup<V> where V : UIWidget
  {
    private readonly List<T> _groupObjects = new List<T>(10);
    public int groupId;
    [NonSerialized]
    public bool updateGroup;
    private int _loadedGroupId;

    [ContextMenu("Execute")]
    public void UpdateGroupNow()
    {
      this.RefreshGroupList();
      this.ApplyChange((IEnumerable<T>) this._groupObjects);
      this.updateGroup = false;
    }

    protected abstract void ApplyChange(IEnumerable<T> group);

    private void RefreshGroupList()
    {
      this._groupObjects.Clear();
      T[] componentsInChildren = this.GetComponentsInChildren<T>(true);
      if (componentsInChildren == null)
        return;
      this._groupObjects.AddRange(((IEnumerable<T>) componentsInChildren).Where<T>((Func<T, bool>) (c => c.groupId == this.groupId)));
      this._loadedGroupId = this.groupId;
    }

    private void Start()
    {
      this.RefreshGroupList();
    }

    private void OnEnable()
    {
      this.RefreshGroupList();
    }

    private void Update()
    {
      if (!this.updateGroup)
        return;
      this.updateGroup = false;
      if (this._groupObjects == null || this.groupId != this._loadedGroupId)
        this.RefreshGroupList();
      this.ApplyChange((IEnumerable<T>) this._groupObjects);
    }
  }
}
