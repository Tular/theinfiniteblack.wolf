﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.SBUISimpleSpriteAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UISprite))]
  public class SBUISimpleSpriteAnimation : MonoBehaviour
  {
    public UIAtlas atlas;
    public int framesPerSecond;
    public bool playOnStart;
    public bool loop;
    public string[] spriteFrames;
    protected UISprite mSprite;
    protected bool mActive;
    protected float mDelta;
    protected int mFrameIndex;

    public bool isPlaying
    {
      get
      {
        return this.mActive;
      }
      set
      {
        this.mActive = value;
      }
    }

    protected virtual void Start()
    {
      this.mSprite = this.GetComponent<UISprite>();
      if (!this.playOnStart)
        return;
      this.isPlaying = true;
    }

    protected virtual void Update()
    {
      if (!this.mActive || this.spriteFrames.Length <= 1 || (double) this.framesPerSecond <= 0.0)
        return;
      this.mDelta += RealTime.deltaTime;
      float num = 1f / (float) this.framesPerSecond;
      if ((double) num >= (double) this.mDelta)
        return;
      this.mDelta = (double) num <= 0.0 ? 0.0f : this.mDelta - num;
      if (++this.mFrameIndex >= this.spriteFrames.Length)
      {
        this.mFrameIndex = 0;
        this.mActive = this.loop;
      }
      if (!this.mActive)
        return;
      this.mSprite.spriteName = this.spriteFrames[this.mFrameIndex];
    }
  }
}
