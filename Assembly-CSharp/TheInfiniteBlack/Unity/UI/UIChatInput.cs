﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIChatInput
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UIChatInput : UIInput
  {
    private bool _setCursorToEnd;

    public void BeginMessageTo()
    {
      this.isSelected = true;
    }

    public void BeginMessageTo(string to)
    {
      this.selectAllTextOnFocus = false;
      this.isSelected = true;
      this.value = to;
    }

    protected override void OnPress(bool isPressed)
    {
      base.OnPress(isPressed);
    }

    protected override void OnSelect(bool s)
    {
      base.OnSelect(s);
      this._setCursorToEnd = true;
    }

    protected override void Update()
    {
      base.Update();
      if (this.isSelected || !Input.GetKeyDown(KeyCode.KeypadEnter) && !Input.GetKeyDown(KeyCode.Return) && (!Input.anyKeyDown || !(Input.inputString == Environment.NewLine)))
        return;
      this.isSelected = true;
      this.OnPress(true);
    }

    private void LateUpdate()
    {
      if (!this._setCursorToEnd)
        return;
      this._setCursorToEnd = false;
      this.selectionEnd = this.label.text.Length;
      this.selectionStart = this.mSelectionEnd;
      this.cursorPosition = this.label.text.Length;
      this._setCursorToEnd = false;
    }

    public void OnChange()
    {
    }
  }
}
