﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIPanelDepthGroupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UIPanelDepthGroupController : MonoBehaviour
  {
    public string group;
    public int baseDepth;

    [ContextMenu("Execute")]
    public void UpdatePanelDepths()
    {
      UIPanelDepthGroup[] componentsInChildren = this.GetComponentsInChildren<UIPanelDepthGroup>(true);
      for (int index = 0; index < componentsInChildren.Length; ++index)
      {
        if (this.group.Equals(componentsInChildren[index].group))
          componentsInChildren[index].Rebase(this.baseDepth);
      }
    }

    public void UpdatePanelDepths(int newBase)
    {
      this.baseDepth = newBase;
      this.UpdatePanelDepths();
    }
  }
}
