﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIWidgetSizeGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UIWidget))]
  public class UIWidgetSizeGroup : UIWidgetPropertyGroup<UIWidget>
  {
    public UIWidgetSizeGroup.SizeMatchOptions match;
    public int widthOffset;
    public int heightOffset;

    public int width
    {
      get
      {
        return this.target.width;
      }
      set
      {
        this.target.width = value;
      }
    }

    public int height
    {
      get
      {
        return this.target.height;
      }
      set
      {
        this.target.height = value;
      }
    }

    public void UpdateSize(int baseWidth, int baseHeight)
    {
      switch (this.match)
      {
        case UIWidgetSizeGroup.SizeMatchOptions.WidthOnly:
          this.target.width = baseWidth + this.widthOffset;
          break;
        case UIWidgetSizeGroup.SizeMatchOptions.HeightOnly:
          this.target.height = baseHeight + this.heightOffset;
          break;
        case UIWidgetSizeGroup.SizeMatchOptions.WidthAndHeight:
          this.target.width = baseWidth + this.widthOffset;
          this.target.height = baseHeight + this.heightOffset;
          break;
      }
    }

    public enum SizeMatchOptions
    {
      WidthOnly,
      HeightOnly,
      WidthAndHeight,
    }
  }
}
