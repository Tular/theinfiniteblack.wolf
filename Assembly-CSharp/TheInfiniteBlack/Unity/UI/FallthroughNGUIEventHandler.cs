﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.FallthroughNGUIEventHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class FallthroughNGUIEventHandler : MonoBehaviour
  {
    public bool listIsScrolling;
    public static EventHandler<MouseScrollWheelEventArgs> onMouseScrollEventHandler;
    private int _nextPossibleMoveFrame;

    private void OnPress(bool isDown)
    {
      Debug.Log((object) ("FallthroughNGUIEventHandler.OnPress - " + (object) isDown));
    }

    private void OnScroll(float delta)
    {
      if (PooledEntityListManager.isScrolling)
      {
        this._nextPossibleMoveFrame = Time.frameCount + 20;
      }
      else
      {
        if (this._nextPossibleMoveFrame > Time.frameCount || FallthroughNGUIEventHandler.onMouseScrollEventHandler == null)
          return;
        MouseScrollWheelEventArgs e = new MouseScrollWheelEventArgs(delta, UICamera.current);
        FallthroughNGUIEventHandler.onMouseScrollEventHandler((object) this, e);
      }
    }

    private void OnEnable()
    {
      UICamera.fallThrough = this.gameObject;
    }

    private void OnDisable()
    {
      UICamera.fallThrough = (GameObject) null;
    }
  }
}
