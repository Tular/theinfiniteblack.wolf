﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.DialogBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using PathologicalGames;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UIPanel))]
  public abstract class DialogBase : MonoBehaviour
  {
    public bool isFullScreen;
    private bool _isRegistered;
    private bool _isInitialized;
    private int _originalDepth;
    private bool _eventHandlersRegistered;

    public bool isVisible
    {
      get
      {
        return this.gameObject.activeSelf;
      }
    }

    public virtual int panelDepth
    {
      get
      {
        return this.panel.depth;
      }
      set
      {
        this.panel.depth = value;
      }
    }

    public UIPanel panel { get; protected set; }

    public void Initialize()
    {
      this.LogI("Initializing Dialog: " + this.name);
      if (this._isInitialized)
        return;
      this.OnInitialize();
      this._isInitialized = true;
    }

    public void Cleanup()
    {
      this.UnRegisterEventHandlers();
      this.OnCleanup();
      this._isInitialized = false;
    }

    public void ShowOnTop()
    {
      this.panelDepth = UIPanel.nextUnusedDepth;
      DialogManager.ShowingDialog(this, false);
      this.OnShow();
    }

    public void Show(object[] args)
    {
      DialogManager.ShowingDialog(this, this.ProcessArgs(args));
      this.OnShow();
    }

    protected virtual bool ProcessArgs(object[] args)
    {
      return true;
    }

    public void Show()
    {
      DialogManager.ShowingDialog(this, true);
      this.OnShow();
    }

    public void Hide()
    {
      this.OnHide();
      this.panel.depth = this._originalDepth;
      DialogManager.ClosingDialog(this);
    }

    public void RegisterEventHandlers()
    {
      if (this._eventHandlersRegistered)
        return;
      this.OnRegisterEventHandlers();
      this._eventHandlersRegistered = true;
    }

    public void UnRegisterEventHandlers()
    {
      if (!this._eventHandlersRegistered)
        return;
      this.OnUnRegisterEventHandlers();
      this._eventHandlersRegistered = false;
    }

    protected void Awake()
    {
      DialogManager.RegisterDialog<DialogBase>(this);
      this._isRegistered = true;
      this.panel = this.GetComponent<UIPanel>();
      this._originalDepth = this.panel.depth;
      this.OnAwake();
    }

    protected void OnEnable()
    {
      if (!this._isRegistered)
      {
        DialogManager.RegisterDialog<DialogBase>(this);
        this._isRegistered = true;
      }
      if (!((Object) this.panel == (Object) null))
        return;
      this.panel = this.GetComponent<UIPanel>();
      this._originalDepth = this.panel.depth;
    }

    protected void OnDestroy()
    {
      this.OnUnRegisterEventHandlers();
      foreach (SpawnPool componentsInChild in this.GetComponentsInChildren<SpawnPool>())
        componentsInChild.DespawnAll();
    }

    protected abstract void OnRegisterEventHandlers();

    protected abstract void OnUnRegisterEventHandlers();

    protected virtual void OnAwake()
    {
    }

    protected virtual void OnInitialize()
    {
    }

    protected virtual void OnCleanup()
    {
    }

    protected virtual void OnHide()
    {
    }

    protected virtual void OnShow()
    {
    }
  }
}
