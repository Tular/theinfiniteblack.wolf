﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UILabelDisableChildrenWhenEmpty
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UILabel))]
  public class UILabelDisableChildrenWhenEmpty : MonoBehaviour
  {
    private UILabel _label;
    private bool _childrenAreActive;

    private void OnEnable()
    {
      if (!(bool) ((Object) this._label))
        this._label = this.GetComponent<UILabel>();
      NGUITools.SetActiveChildren(this.gameObject, true);
      this._childrenAreActive = true;
    }

    private void Update()
    {
      if (string.IsNullOrEmpty(this._label.text) && this._childrenAreActive)
      {
        NGUITools.SetActiveChildren(this.gameObject, false);
        this._childrenAreActive = false;
      }
      else
      {
        if (string.IsNullOrEmpty(this._label.text) || this._childrenAreActive)
          return;
        NGUITools.SetActiveChildren(this.gameObject, true);
        this._childrenAreActive = true;
      }
    }
  }
}
