﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UISpriteColorGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UISprite))]
  public class UISpriteColorGroup : UIWidgetPropertyGroup<UISprite>
  {
    public Color color
    {
      get
      {
        return this.target.color;
      }
      set
      {
        this.target.color = value;
      }
    }
  }
}
