﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIPanelDepthGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UIPanel))]
  public class UIPanelDepthGroup : MonoBehaviour
  {
    public string group;
    public int offsetFromBase;
    private UIPanel _panel;

    public void Rebase(int newBase)
    {
      if (!(bool) ((Object) this._panel))
        this._panel = this.GetComponent<UIPanel>();
      this._panel.depth = newBase + this.offsetFromBase;
    }

    private void Awake()
    {
      this._panel = this.GetComponent<UIPanel>();
    }
  }
}
