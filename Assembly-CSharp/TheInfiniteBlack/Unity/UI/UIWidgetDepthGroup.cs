﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIWidgetDepthGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (UIWidget))]
  public class UIWidgetDepthGroup : MonoBehaviour
  {
    public int groupId;
    public int offsetFromBase;
    private UIWidget _widget;

    public void Rebase(int newBase)
    {
      if (!(bool) ((Object) this._widget))
        this._widget = this.GetComponent<UIWidget>();
      this._widget.depth = newBase + this.offsetFromBase;
    }

    private void Awake()
    {
      if ((bool) ((Object) this._widget))
        return;
      this._widget = this.GetComponent<UIWidget>();
    }
  }
}
