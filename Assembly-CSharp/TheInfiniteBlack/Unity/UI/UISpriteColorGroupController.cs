﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UISpriteColorGroupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UISpriteColorGroupController : UIWidgetPropertyGroupController<UIWidgetPropertyGroup<UISprite>, UISprite>
  {
    public Color color;
    private float _duration;

    public void SetColor(int group, Color c)
    {
      this.groupId = group;
      this.color = c;
      this._duration = 0.0f;
      this.UpdateGroupNow();
    }

    public void SetColor(int group, float duration, Color toColor)
    {
      this.groupId = group;
      this.color = toColor;
      this._duration = duration;
      this.UpdateGroupNow();
    }

    protected override void ApplyChange(IEnumerable<UIWidgetPropertyGroup<UISprite>> group)
    {
      foreach (UIWidgetPropertyGroup<UISprite> widgetPropertyGroup in group)
      {
        if ((double) this._duration <= 0.0)
          widgetPropertyGroup.target.color = this.color;
        else
          TweenColor.Begin(widgetPropertyGroup.gameObject, this._duration, this.color);
      }
    }
  }
}
