﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIWidgetPropertyGroup`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public abstract class UIWidgetPropertyGroup<T> : MonoBehaviour where T : UIWidget
  {
    public int groupId;
    private T _target;
    [NonSerialized]
    protected bool mIsInitialized;

    public T target
    {
      get
      {
        if (!this.mIsInitialized)
          this.Initialize();
        return this._target;
      }
    }

    protected virtual void OnInitialize()
    {
    }

    protected void Initialize()
    {
      this._target = this.GetComponent<T>();
      if (!(bool) ((UnityEngine.Object) this._target))
        Debug.Log((object) string.Format("UISpriteColorGroup Target is null"));
      this.OnInitialize();
      this.mIsInitialized = true;
    }

    protected virtual void Start()
    {
      this.Initialize();
    }

    protected virtual void OnEnable()
    {
      if (this.mIsInitialized)
        return;
      this.Initialize();
    }
  }
}
