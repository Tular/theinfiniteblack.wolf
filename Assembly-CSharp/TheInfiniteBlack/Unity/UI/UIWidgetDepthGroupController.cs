﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIWidgetDepthGroupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UIWidgetDepthGroupController : MonoBehaviour
  {
    public int groupId;
    public int baseDepth;

    [ContextMenu("Set To Base")]
    public void SetToBase()
    {
      UIWidget component = this.GetComponent<UIWidget>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.depth = this.baseDepth;
      foreach (UIWidgetDepthGroup widgetDepthGroup in ((IEnumerable<UIWidgetDepthGroup>) this.GetComponentsInChildren<UIWidgetDepthGroup>(true)).Where<UIWidgetDepthGroup>((Func<UIWidgetDepthGroup, bool>) (c => c.groupId == this.groupId)))
        widgetDepthGroup.Rebase(this.baseDepth);
    }

    private void OnEnable()
    {
      this.SetToBase();
    }
  }
}
