﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.SkyboxManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class SkyboxManager : MonoBehaviour
  {
    public Camera skyboxCamera;
    public Skybox skybox;
    public Material normalSkyboxMaterial;
    public Material riftSkyboxMaterial;
    public TweenRotation skyboxTweener;
    public Color backgroundColor;
    public UITexture backgroundTexture;
    public Texture2D normalTexture;
    public Texture2D riftTexture;
    public SkyboxManager.SkyboxStyle style;
    private static SkyboxManager _instance;
    private bool _updateSkybox;
    private bool _isRift;

    public static SkyboxManager Instance
    {
      get
      {
        return SkyboxManager._instance;
      }
    }

    protected virtual void Awake()
    {
      if ((UnityEngine.Object) SkyboxManager._instance != (UnityEngine.Object) null && (UnityEngine.Object) SkyboxManager._instance != (UnityEngine.Object) this)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
      }
      else
      {
        if (!((UnityEngine.Object) SkyboxManager._instance == (UnityEngine.Object) null))
          return;
        SkyboxManager._instance = this.gameObject.GetComponent<SkyboxManager>();
      }
    }

    [ContextMenu("Update Skybox Style")]
    public void UpdateSkyboxStyle()
    {
      switch (this.style)
      {
        case SkyboxManager.SkyboxStyle.Skybox:
          this.skybox.enabled = true;
          this.skyboxTweener.enabled = true;
          NGUITools.SetActive(this.backgroundTexture.gameObject, false);
          break;
        case SkyboxManager.SkyboxStyle.Texture:
          this.skybox.enabled = false;
          this.skyboxTweener.ResetToBeginning();
          this.skyboxTweener.enabled = false;
          this.skybox.transform.rotation = Quaternion.identity;
          NGUITools.SetActive(this.backgroundTexture.gameObject, true);
          break;
        case SkyboxManager.SkyboxStyle.SolidColor:
          this.skybox.enabled = false;
          this.skyboxTweener.ResetToBeginning();
          this.skyboxTweener.enabled = false;
          this.skybox.transform.rotation = Quaternion.identity;
          NGUITools.SetActive(this.backgroundTexture.gameObject, false);
          this.skyboxCamera.backgroundColor = this.backgroundColor;
          break;
      }
    }

    public void UpdateUniverseBackground(bool rift)
    {
      switch (this.style)
      {
        case SkyboxManager.SkyboxStyle.Skybox:
          this.skybox.material = !rift ? this.normalSkyboxMaterial : this.riftSkyboxMaterial;
          break;
        case SkyboxManager.SkyboxStyle.Texture:
          this.backgroundTexture.mainTexture = !rift ? (Texture) this.normalTexture : (Texture) this.riftTexture;
          break;
      }
    }

    private void Start()
    {
      this.style = !TibProxy.accountManager.systemSettings.Background ? SkyboxManager.SkyboxStyle.SolidColor : SkyboxManager.SkyboxStyle.Skybox;
      this.skybox.enabled = false;
      this.skyboxTweener.enabled = false;
      NGUITools.SetActive(this.backgroundTexture.gameObject, false);
      this.skyboxCamera.backgroundColor = this.backgroundColor;
      this._isRift = false;
      this.UpdateSkyboxStyle();
    }

    private void AccountManager_onSettingsUpdatedEvent(object sender, SettingsUpdatedEventEventArgs e)
    {
      SkyboxManager.SkyboxStyle skyboxStyle = !TibProxy.accountManager.systemSettings.Background ? SkyboxManager.SkyboxStyle.SolidColor : SkyboxManager.SkyboxStyle.Skybox;
      if (this.style == skyboxStyle)
        return;
      this.style = skyboxStyle;
      this.UpdateSkyboxStyle();
    }

    private void OnEnable()
    {
      TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
      TibProxy.accountManager.onSettingsUpdatedEvent += new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
    }

    private void OnDisable()
    {
      if (TibProxy.accountManager == null)
        return;
      TibProxy.accountManager.onSettingsUpdatedEvent -= new EventHandler<SettingsUpdatedEventEventArgs>(this.AccountManager_onSettingsUpdatedEvent);
    }

    private bool isRiftView
    {
      get
      {
        return this._isRift;
      }
      set
      {
        if (this._isRift == value)
          return;
        this._isRift = value;
        this._updateSkybox = true;
      }
    }

    public enum SkyboxStyle
    {
      Skybox,
      Texture,
      SolidColor,
    }
  }
}
