﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UISwitchControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [RequireComponent(typeof (BoxCollider))]
  [RequireComponent(typeof (UIWidget))]
  public class UISwitchControl : MonoBehaviour
  {
    [HideInInspector]
    public float transitionTime = 1f;
    [HideInInspector]
    public UISprite switchBackgroundTarget;
    [HideInInspector]
    public UISprite switchTarget;
    [HideInInspector]
    public UILabel onLabel;
    [HideInInspector]
    public UILabel offLabel;
    [HideInInspector]
    public UILabel controlLabel;
    [HideInInspector]
    public int controlLabelPadding;
    [HideInInspector]
    public bool initialState;
    [HideInInspector]
    public int switchPadding;
    [HideInInspector]
    public string switchSprite;
    [HideInInspector]
    public string onBackgroundSprite;
    [HideInInspector]
    public string offBackgroundSprite;
    [HideInInspector]
    public List<EventDelegate> onChange;
    public Vector3 switchPosOn;
    public Vector3 switchPosOff;
    private UIWidget _widget;
    private BoxCollider _boxCollider;
    public bool _value;

    public bool value
    {
      get
      {
        return this._value;
      }
      set
      {
        this.Set(value);
      }
    }

    public bool isEnabled { get; private set; }

    public void Enable()
    {
      this._boxCollider.enabled = true;
      this.isEnabled = true;
    }

    public void Disable()
    {
      this._boxCollider.enabled = false;
      this.isEnabled = false;
    }

    public void Set(bool state)
    {
      if (state)
        this.SetOn();
      else
        this.SetOff();
    }

    public void SetOff()
    {
      this._value = false;
      this.initialState = this._value;
      this.UpdateState();
    }

    public void SetOn()
    {
      this._value = true;
      this.initialState = this._value;
      this.UpdateState();
    }

    public void SetDirty()
    {
      if (!(bool) ((Object) this._widget))
        this._widget = this.GetComponent<UIWidget>();
      this.UpdateSize();
      this.UpdateState();
    }

    protected void Awake()
    {
      this._widget = this.GetComponent<UIWidget>();
      this._boxCollider = this.GetComponent<BoxCollider>();
      this._value = this.initialState;
    }

    protected void OnEnable()
    {
      this.UpdateSize();
      this.UpdateState();
    }

    private void UpdateState()
    {
      if (!(bool) ((Object) this.switchTarget) || !(bool) ((Object) this.switchBackgroundTarget) || (!(bool) ((Object) this.onLabel) || !(bool) ((Object) this.offLabel)))
        return;
      this.switchTarget.transform.localPosition = !this._value ? this.switchPosOff : this.switchPosOn;
      this.switchBackgroundTarget.spriteName = !this._value ? this.offBackgroundSprite : this.onBackgroundSprite;
      this.onLabel.alpha = !this._value ? 0.0f : 1f;
      this.onLabel.enabled = this._value;
      this.offLabel.alpha = !this._value ? 1f : 0.0f;
      this.offLabel.enabled = !this._value;
    }

    private void UpdateSize()
    {
      if (!(bool) ((Object) this.switchTarget) || !(bool) ((Object) this.switchBackgroundTarget))
        return;
      int num1 = this._widget.height - this.switchPadding * 2;
      this.switchTarget.height = num1;
      this.switchTarget.width = num1;
      float num2 = (float) this.switchPadding + (float) num1 * 0.5f;
      this.switchPosOn.x = num2;
      this.switchPosOn.y = 0.0f;
      this.switchPosOff.x = this.switchBackgroundTarget.localCorners[3].x - num2;
      this.switchPosOff.y = 0.0f;
      if (!((Object) this.controlLabel != (Object) null))
        return;
      this.controlLabel.leftAnchor.absolute = this.controlLabelPadding;
      this.controlLabel.rightAnchor.absolute = -this.controlLabelPadding;
    }

    public void Toggle()
    {
      this.OnClick();
    }

    public virtual void Transition(bool toState)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      UISwitchControl.\u003CTransition\u003Ec__AnonStorey45 transitionCAnonStorey45 = new UISwitchControl.\u003CTransition\u003Ec__AnonStorey45();
      // ISSUE: reference to a compiler-generated field
      transitionCAnonStorey45.\u003C\u003Ef__this = this;
      if (toState == this._value)
        return;
      this._value = toState;
      // ISSUE: reference to a compiler-generated field
      transitionCAnonStorey45.fromLabel = !toState ? this.onLabel : this.offLabel;
      // ISSUE: reference to a compiler-generated field
      transitionCAnonStorey45.toLabel = !toState ? this.offLabel : this.onLabel;
      // ISSUE: reference to a compiler-generated field
      transitionCAnonStorey45.toBackground = !toState ? this.offBackgroundSprite : this.onBackgroundSprite;
      // ISSUE: reference to a compiler-generated field
      transitionCAnonStorey45.toLabel.alpha = 0.0f;
      // ISSUE: reference to a compiler-generated field
      transitionCAnonStorey45.toLabel.enabled = true;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      EventDelegate.Add(TweenAlpha.Begin(transitionCAnonStorey45.fromLabel.gameObject, this.transitionTime / 2f, 0.0f).onFinished, new EventDelegate.Callback(transitionCAnonStorey45.\u003C\u003Em__FE), true);
      TweenPosition.Begin(this.switchTarget.gameObject, this.transitionTime, !this._value ? this.switchPosOff : this.switchPosOn);
      if (!EventDelegate.IsValid(this.onChange))
        return;
      EventDelegate.Execute(this.onChange);
    }

    protected void OnClick()
    {
      this.Transition(!this._value);
    }
  }
}
