﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UIWidgetSizeGroupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UIWidgetSizeGroupController : UIWidgetPropertyGroupController<UIWidgetSizeGroup, UIWidget>
  {
    public UIWidget baseWidget;
    public int baseWidth;
    public int baseHeight;
    public bool makePixelPerfect;

    protected override void ApplyChange(IEnumerable<UIWidgetSizeGroup> group)
    {
      if ((Object) this.baseWidget != (Object) null)
      {
        this.baseWidget.width = this.baseWidth;
        this.baseWidget.height = this.baseHeight;
        if (this.makePixelPerfect)
          this.baseWidget.MakePixelPerfect();
      }
      foreach (UIWidgetSizeGroup uiWidgetSizeGroup in group)
      {
        uiWidgetSizeGroup.UpdateSize(this.baseWidth, this.baseHeight);
        if (this.makePixelPerfect)
          uiWidgetSizeGroup.target.MakePixelPerfect();
      }
    }

    private void OnEnable()
    {
      this.baseWidget = this.GetComponent<UIWidget>();
      if (!((Object) this.baseWidget != (Object) null))
        return;
      this.baseWidth = this.baseWidget.width;
      this.baseHeight = this.baseWidget.height;
    }
  }
}
