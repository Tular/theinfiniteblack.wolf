﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.DialogBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

namespace TheInfiniteBlack.Unity.UI
{
  public abstract class DialogBase<T> : DialogBase where T : DialogContextBase
  {
    private int _initialPanelDepth;

    public T context { get; protected set; }

    protected abstract T CreateContext();

    protected override void OnAwake()
    {
      this.context = this.CreateContext();
      this._initialPanelDepth = this.panel.depth;
      foreach (DialogViewBase<T> componentsInChild in this.GetComponentsInChildren<DialogViewBase<T>>(true))
        componentsInChild.SetDialogContext(this.context);
    }

    protected override void OnHide()
    {
      base.OnHide();
      this.panel.depth = this._initialPanelDepth;
      this.context.Reset();
    }

    protected override void OnRegisterEventHandlers()
    {
    }
  }
}
