﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UINotifyOnSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UINotifyOnSelect : MonoBehaviour
  {
    [HideInInspector]
    public List<EventDelegate> onSelect;
    [HideInInspector]
    public List<EventDelegate> onDeSelect;

    public void OnSelect(bool selected)
    {
      EventDelegate.Execute(!selected ? this.onDeSelect : this.onSelect);
    }
  }
}
