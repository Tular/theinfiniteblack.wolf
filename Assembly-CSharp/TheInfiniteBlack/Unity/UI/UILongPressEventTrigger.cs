﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UILongPressEventTrigger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UILongPressEventTrigger : MonoBehaviour
  {
    public float longPressDelay = 1f;
    [HideInInspector]
    public List<EventDelegate> onLongPress = new List<EventDelegate>();
    [HideInInspector]
    public List<EventDelegate> onClick = new List<EventDelegate>();
    public static UILongPressEventTrigger current;
    private float _pressStart;
    private bool _isPressed;
    private float _pressDelta;

    private void OnPress(bool isDown)
    {
      if (isDown)
      {
        UILongPressEventTrigger.current = this;
        this._isPressed = true;
        this._pressStart = RealTime.time;
      }
      else
      {
        if (!this._isPressed)
          return;
        if ((double) this._pressDelta < (double) this.longPressDelay)
        {
          Debug.Log((object) string.Format("UILongPressEventTrigger.OnPress() - click"));
          EventDelegate.Execute(this.onClick);
        }
        else
        {
          Debug.Log((object) string.Format("UILongPressEventTrigger.OnPress() - longPress"));
          EventDelegate.Execute(this.onLongPress);
        }
        this._isPressed = false;
        this._pressStart = 0.0f;
      }
    }

    private void Update()
    {
      if (!this._isPressed)
        return;
      this._pressDelta = RealTime.time - this._pressStart;
      if ((double) this._pressDelta <= (double) this.longPressDelay)
        return;
      this.OnPress(false);
    }
  }
}
