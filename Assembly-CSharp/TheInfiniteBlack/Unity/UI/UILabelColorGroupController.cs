﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UILabelColorGroupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UILabelColorGroupController : UIWidgetPropertyGroupController<UILabelColorGroup, UILabel>
  {
    public Color color;

    public void SetColor(int group, Color c)
    {
      this.groupId = group;
      this.color = c;
      this.updateGroup = true;
    }

    protected override void ApplyChange(IEnumerable<UILabelColorGroup> group)
    {
      foreach (UILabelColorGroup uiLabelColorGroup in group)
        uiLabelColorGroup.color = this.color;
    }
  }
}
