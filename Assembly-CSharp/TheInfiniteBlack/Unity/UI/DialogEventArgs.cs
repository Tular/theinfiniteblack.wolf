﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.DialogEventArgs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;

namespace TheInfiniteBlack.Unity.UI
{
  public class DialogEventArgs : EventArgs
  {
    public DialogEventArgs(DialogBase d, DialogEventArgs.DialogEventType t)
    {
      this.dialog = d;
      this.type = t;
    }

    public DialogEventArgs.DialogEventType type { get; protected set; }

    public DialogBase dialog { get; protected set; }

    public enum DialogEventType
    {
      Open,
      Close,
    }
  }
}
