﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.AcceptDeclineDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using TheInfiniteBlack.Library.Client.Events;

namespace TheInfiniteBlack.Unity.UI
{
  public class AcceptDeclineDialog : DialogBase
  {
    public UILabel messageText;
    public SBUIButton acceptButton;
    public SBUIButton declineButton;

    protected override bool ProcessArgs(object[] args)
    {
      if (args.Length < 1 || 3 < args.Length)
        throw new ArgumentException();
      EventDelegate.Callback onAccept = (EventDelegate.Callback) null;
      EventDelegate.Callback onDecline = (EventDelegate.Callback) null;
      if (2 <= args.Length)
        onAccept = (EventDelegate.Callback) args[1];
      if (3 <= args.Length)
        onDecline = (EventDelegate.Callback) args[2];
      this.Show_Internal((string) args[0], onAccept, onDecline);
      return false;
    }

    private void Show_Internal(string message, EventDelegate.Callback onAccept, EventDelegate.Callback onDecline)
    {
      this.messageText.text = message;
      EventDelegate.Add(this.acceptButton.onClick, (EventDelegate.Callback) (() =>
      {
        if (onAccept != null)
          onAccept();
        this.Hide();
      }));
      EventDelegate.Add(this.declineButton.onClick, (EventDelegate.Callback) (() =>
      {
        if (onDecline != null)
          onDecline();
        this.Hide();
      }));
    }

    protected override void OnRegisterEventHandlers()
    {
    }

    protected override void OnUnRegisterEventHandlers()
    {
      TibProxy.Instance.onAcceptDeclineWindowsEvent -= new EventHandler<AcceptDeclineWindowEventArgs>(this.TibProxy_onAcceptDeclineWindowsEvent);
    }

    protected override void OnAwake()
    {
      base.OnAwake();
      this.messageText.text = string.Empty;
    }

    protected override void OnInitialize()
    {
      base.OnInitialize();
      TibProxy.Instance.onAcceptDeclineWindowsEvent -= new EventHandler<AcceptDeclineWindowEventArgs>(this.TibProxy_onAcceptDeclineWindowsEvent);
      TibProxy.Instance.onAcceptDeclineWindowsEvent += new EventHandler<AcceptDeclineWindowEventArgs>(this.TibProxy_onAcceptDeclineWindowsEvent);
    }

    protected override void OnCleanup()
    {
      base.OnCleanup();
      TibProxy.Instance.onAcceptDeclineWindowsEvent -= new EventHandler<AcceptDeclineWindowEventArgs>(this.TibProxy_onAcceptDeclineWindowsEvent);
    }

    protected override void OnHide()
    {
      base.OnHide();
      this.acceptButton.onClick.Clear();
      this.declineButton.onClick.Clear();
      this.messageText.text = string.Empty;
    }

    private void TibProxy_onAcceptDeclineWindowsEvent(object sender, AcceptDeclineWindowEventArgs e)
    {
      this.messageText.text = e.Text;
      EventDelegate.Add(this.acceptButton.onClick, (EventDelegate.Callback) (() =>
      {
        e.Accept();
        this.Hide();
      }));
      EventDelegate.Add(this.declineButton.onClick, (EventDelegate.Callback) (() =>
      {
        e.Decline();
        this.Hide();
      }));
      DialogManager.ShowingDialog((DialogBase) this, false);
      this.OnShow();
    }
  }
}
