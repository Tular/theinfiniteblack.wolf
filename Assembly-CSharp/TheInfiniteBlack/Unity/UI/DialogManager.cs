﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.DialogManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity.Map;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  [AdvancedInspector.AdvancedInspector]
  public class DialogManager : PreInstantiatedSingleton<DialogManager>
  {
    private static readonly Dictionary<string, DialogBase> _dialogs = new Dictionary<string, DialogBase>();
    [Inspect(0)]
    public int openDlgCount;
    [Inspect(10)]
    public GameObject[] dialogPrefabs;
    public DialogBase ad;
    public DialogBase pd;
    private static DialogBase _activeDialog;
    private static DialogBase _previousDialog;
    private bool _initializingDialogs;
    [Inspect(1)]
    private PopupMessageDialog _popupDialog;

    public event EventHandler<DialogEventArgs> onDialogEvent;

    protected static DialogBase activeDialog
    {
      get
      {
        return DialogManager._activeDialog;
      }
      set
      {
        DialogManager._activeDialog = value;
        PreInstantiatedSingleton<DialogManager>.Instance.ad = DialogManager._activeDialog;
      }
    }

    protected static DialogBase previousDialog
    {
      get
      {
        return DialogManager._previousDialog;
      }
      set
      {
        DialogManager._previousDialog = value;
        PreInstantiatedSingleton<DialogManager>.Instance.pd = DialogManager._previousDialog;
      }
    }

    public static void ShowDialog<T>(params object[] arguments)
    {
      string name = typeof (T).Name;
      if (!DialogManager._dialogs.ContainsKey(name))
        throw new ArgumentException(string.Format("Dialog of type: {0} has not been registered.", (object) name));
      DialogManager._dialogs[name].Show(arguments);
    }

    public static void RegisterDialog<T>(T dialog) where T : DialogBase
    {
      string name = dialog.GetType().Name;
      if (DialogManager._dialogs.ContainsKey(name))
        return;
      DialogManager._dialogs.Add(name, (DialogBase) dialog);
    }

    public static T GetDialog<T>() where T : DialogBase
    {
      string name = typeof (T).Name;
      if (!DialogManager._dialogs.ContainsKey(name))
        return (T) null;
      return DialogManager._dialogs[name] as T;
    }

    public static T ShowDialogAsPopup<T>() where T : DialogBase
    {
      DialogManager.previousDialog = DialogManager.activeDialog;
      T dialog = DialogManager.GetDialog<T>();
      dialog.ShowOnTop();
      return dialog;
    }

    public static void ShowDialog<T>(bool keepHistory) where T : DialogBase
    {
      string name = typeof (T).Name;
      if (!DialogManager._dialogs.ContainsKey(name))
        return;
      DialogManager._dialogs[name].Show();
    }

    public static void CloseDialog<T>() where T : DialogBase
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null || (UnityEngine.Object) DialogManager.activeDialog == (UnityEngine.Object) null)
        return;
      string name = typeof (T).Name;
      if (!DialogManager._dialogs.ContainsKey(name))
        return;
      DialogManager._dialogs[name].Hide();
    }

    public static void ShowingDialog(DialogBase dialog, bool closeExisting)
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        UnityEngine.Debug.Log((object) string.Format("DialogManager.ShowingDialog - Instance is null"));
      else
        PreInstantiatedSingleton<DialogManager>.Instance.OnShowingDialog(dialog, closeExisting);
    }

    public static void ClosingDialog(DialogBase dialog)
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        UnityEngine.Debug.Log((object) string.Format("DialogManager.ClosingDialog - Instance is null"));
      else
        PreInstantiatedSingleton<DialogManager>.Instance.OnClosingDialog(dialog);
    }

    public static void CloseAllDialogs()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        UnityEngine.Debug.Log((object) string.Format("DialogManager.CloseAllDialogs - Instance is null"));
      else
        PreInstantiatedSingleton<DialogManager>.Instance.DoCloseAllDialogs();
    }

    private void DoCloseAllDialogs()
    {
      if (this._initializingDialogs)
        return;
      using (Dictionary<string, DialogBase>.ValueCollection.Enumerator enumerator = DialogManager._dialogs.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DialogBase current = enumerator.Current;
          if (current.isVisible)
            current.Hide();
        }
      }
      DialogManager.activeDialog = (DialogBase) null;
      DialogManager.previousDialog = (DialogBase) null;
      if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<KeyboardMapController>.Instance)
        PreInstantiatedSingleton<KeyboardMapController>.Instance.isEnabled = true;
      if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<SectorMap>.Instance)
        PreInstantiatedSingleton<SectorMap>.Instance.controller.mapControlEventsEnabled = true;
      this.openDlgCount = 0;
    }

    private void OnShowingDialog(DialogBase dialog, bool closeExisting)
    {
      if (closeExisting)
        DialogManager.CloseAllDialogs();
      DialogEventArgs e = new DialogEventArgs(dialog, DialogEventArgs.DialogEventType.Open);
      if (this.onDialogEvent != null)
        this.onDialogEvent((object) this, e);
      DialogManager.activeDialog = dialog;
      if (!dialog.gameObject.activeSelf)
        ++this.openDlgCount;
      NGUITools.SetActiveSelf(dialog.gameObject, true);
      if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<KeyboardMapController>.Instance)
        PreInstantiatedSingleton<KeyboardMapController>.Instance.isEnabled = false;
      if (!((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<SectorMap>.Instance))
        return;
      PreInstantiatedSingleton<SectorMap>.Instance.controller.mapControlEventsEnabled = false;
    }

    private void OnClosingDialog(DialogBase dialog)
    {
      NGUITools.SetActiveSelf(dialog.gameObject, false);
      DialogEventArgs e = new DialogEventArgs(dialog, DialogEventArgs.DialogEventType.Close);
      if (this.onDialogEvent != null)
        this.onDialogEvent((object) this, e);
      if (TibProxy.Instance != null && TibProxy.gameState != null && TibProxy.gameState.IsLoggedIn)
      {
        if ((UnityEngine.Object) DialogManager.activeDialog == (UnityEngine.Object) dialog)
        {
          DialogManager.activeDialog = DialogManager.previousDialog;
          DialogManager.previousDialog = (DialogBase) null;
        }
        else if ((UnityEngine.Object) dialog == (UnityEngine.Object) DialogManager.previousDialog)
          DialogManager.previousDialog = (DialogBase) null;
        if ((UnityEngine.Object) DialogManager.activeDialog == (UnityEngine.Object) null && (UnityEngine.Object) PreInstantiatedSingleton<KeyboardMapController>.Instance != (UnityEngine.Object) null)
          PreInstantiatedSingleton<KeyboardMapController>.Instance.isEnabled = true;
      }
      --this.openDlgCount;
      if (this.openDlgCount > 0)
        return;
      if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<SectorMap>.Instance)
        PreInstantiatedSingleton<SectorMap>.Instance.controller.mapControlEventsEnabled = true;
      if (!((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<KeyboardMapController>.Instance))
        return;
      PreInstantiatedSingleton<KeyboardMapController>.Instance.isEnabled = true;
    }

    private void Start()
    {
      this.StartCoroutine(this.InitializeDialogs());
    }

    private void OnEnable()
    {
      this.LogI(">>>>>>>><b>DialogManager.OnEnable</b><<<<<<<<");
      TibProxy.Instance.onDisconnectEvent -= new EventHandler<DisconnectEventArgs>(this.Instance_onDisconnectEvent);
      TibProxy.Instance.onDisconnectEvent += new EventHandler<DisconnectEventArgs>(this.Instance_onDisconnectEvent);
      TibProxy.Instance.onPopupWindowEvent -= new EventHandler<PopupWindowEventArgs>(this.TibProxy_onPopupWindowEvent);
      TibProxy.Instance.onPopupWindowEvent += new EventHandler<PopupWindowEventArgs>(this.TibProxy_onPopupWindowEvent);
    }

    [DebuggerHidden]
    private IEnumerator InitializeDialogs()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new DialogManager.\u003CInitializeDialogs\u003Ec__Iterator2D()
      {
        \u003C\u003Ef__this = this
      };
    }

    private void Instance_onDisconnectEvent(object sender, DisconnectEventArgs e)
    {
      if (e.State == null || !e.State.IsLoggedIn)
        return;
      this.StartCoroutine(this.InitializeDialogs());
    }

    private void TibProxy_onPopupWindowEvent(object sender, PopupWindowEventArgs e)
    {
      if ((UnityEngine.Object) null == (UnityEngine.Object) this._popupDialog)
        return;
      this._popupDialog.messageText.text = e.Text;
      this._popupDialog.Show(e);
    }

    private void OnDisable()
    {
      TibProxy.Instance.onPopupWindowEvent -= new EventHandler<PopupWindowEventArgs>(this.TibProxy_onPopupWindowEvent);
      using (Dictionary<string, DialogBase>.ValueCollection.Enumerator enumerator = DialogManager._dialogs.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Cleanup();
      }
    }
  }
}
