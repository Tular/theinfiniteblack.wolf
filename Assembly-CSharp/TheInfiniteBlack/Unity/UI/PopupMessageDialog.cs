﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.PopupMessageDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Client.Events;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class PopupMessageDialog : DialogBase
  {
    public UITextList textList;
    public UILabel messageText;
    public UIButton closeButton;
    public GameObject screenOverlay;
    public bool canClose;

    protected override bool ProcessArgs(object[] args)
    {
      if (args.Length != 2)
        throw new ArgumentException("Expected args length is 2");
      this.Show_Internal(new PopupWindowEventArgs(TibProxy.gameState, (string) args[0], true), (bool) args[1]);
      return false;
    }

    public void Show(PopupWindowEventArgs e)
    {
      this.Show_Internal(e, false);
      DialogManager.ShowingDialog((DialogBase) this, false);
      this.OnShow();
    }

    private void Show_Internal(PopupWindowEventArgs e, bool appendMessage)
    {
      this.canClose = e.CanClose;
      if ((bool) ((UnityEngine.Object) this.textList))
      {
        if (!appendMessage)
          this.textList.Clear();
        this.textList.Add(e.Text);
      }
      else if (appendMessage)
        this.messageText.text = this.messageText.text + e.Text;
      else
        this.messageText.text = e.Text;
    }

    public void OnCloseClicked()
    {
      DialogManager.ClosingDialog((DialogBase) this);
      if ((bool) ((UnityEngine.Object) this.textList))
        this.textList.Clear();
      else
        this.messageText.text = string.Empty;
    }

    protected override void OnInitialize()
    {
    }

    protected override void OnRegisterEventHandlers()
    {
    }

    protected override void OnUnRegisterEventHandlers()
    {
    }

    protected override void OnShow()
    {
      this.screenOverlay.SetActive(this.isFullScreen);
      this.closeButton.gameObject.SetActive(this.canClose);
    }

    private void Start()
    {
      EventDelegate.Add(this.closeButton.onClick, new EventDelegate.Callback(this.OnCloseClicked));
    }

    private void Update()
    {
      if (this.closeButton.gameObject.activeSelf == this.canClose)
        return;
      this.closeButton.gameObject.SetActive(this.canClose);
    }
  }
}
