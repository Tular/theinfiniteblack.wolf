﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.MouseScrollWheelEventArgs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;

namespace TheInfiniteBlack.Unity.UI
{
  [AdvancedInspector.AdvancedInspector]
  public class MouseScrollWheelEventArgs : EventArgs
  {
    public MouseScrollWheelEventArgs(float pDelta, UICamera cam)
    {
      this.delta = pDelta;
      this.nguiCamera = cam;
    }

    public float delta { get; private set; }

    public UICamera nguiCamera { get; private set; }

    public bool isHandled { get; private set; }

    public void Handle()
    {
      this.isHandled = true;
    }
  }
}
