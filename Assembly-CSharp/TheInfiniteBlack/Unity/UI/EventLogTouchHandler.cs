﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.EventLogTouchHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Unity.Map;
using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class EventLogTouchHandler : MonoBehaviour
  {
    public int frontDepth;
    public EventLogTouchHandler.AnchorDefinition pressedAnchorDef;
    public EventLogTouchHandler.AnchorDefinition normalAnchorDef;
    public EventLogTouchHandler.AnchorDefinition minimizedAnchorDef;
    private UIPanel _panel;
    private EventLogTouchHandler.AnchorDefinition _currentTopAnchor;
    private int _startDepth;

    public void OnPressDown()
    {
      this.OnPress(true);
    }

    public void OnPressRelease()
    {
      this.OnPress(false);
    }

    private void OnPress(bool isDown)
    {
      Debug.Log((object) ("EventLogTouchHandler.OnPress: " + (object) isDown));
      if (isDown)
      {
        DialogManager.CloseAllDialogs();
        if ((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<SectorMap>.Instance)
          PreInstantiatedSingleton<SectorMap>.Instance.controller.mapControlEventsEnabled = false;
        this._panel.depth = this.frontDepth;
        if (this._currentTopAnchor == this.pressedAnchorDef)
          return;
        this.pressedAnchorDef.SetVertical(this._panel.topAnchor);
      }
      else
      {
        this._panel.depth = this._startDepth;
        this.normalAnchorDef.SetVertical(this._panel.topAnchor);
        if (!((UnityEngine.Object) null != (UnityEngine.Object) PreInstantiatedSingleton<SectorMap>.Instance))
          return;
        PreInstantiatedSingleton<SectorMap>.Instance.controller.mapControlEventsEnabled = true;
      }
    }

    private void DialogManager_onDialogEvent(object sender, DialogEventArgs e)
    {
      switch (e.type)
      {
        case DialogEventArgs.DialogEventType.Open:
          if (!e.dialog.isFullScreen)
          {
            this._panel.depth = this.frontDepth;
            this.minimizedAnchorDef.SetVertical(this._panel.topAnchor);
            this._currentTopAnchor = this.minimizedAnchorDef;
            break;
          }
          this.normalAnchorDef.SetVertical(this._panel.topAnchor);
          this._currentTopAnchor = this.normalAnchorDef;
          break;
        case DialogEventArgs.DialogEventType.Close:
          this._panel.depth = this._startDepth;
          this.normalAnchorDef.SetVertical(this._panel.topAnchor);
          this._currentTopAnchor = this.normalAnchorDef;
          break;
      }
    }

    private void Start()
    {
      this._panel = NGUITools.FindInParents<UIPanel>(this.gameObject);
      this._startDepth = this._panel.depth;
      this.normalAnchorDef.SetVertical(this._panel.topAnchor);
      this._currentTopAnchor = this.normalAnchorDef;
    }

    private void OnEnable()
    {
      PreInstantiatedSingleton<DialogManager>.Instance.onDialogEvent -= new EventHandler<DialogEventArgs>(this.DialogManager_onDialogEvent);
      PreInstantiatedSingleton<DialogManager>.Instance.onDialogEvent += new EventHandler<DialogEventArgs>(this.DialogManager_onDialogEvent);
    }

    private void OnDisable()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance != (UnityEngine.Object) null)
        PreInstantiatedSingleton<DialogManager>.Instance.onDialogEvent -= new EventHandler<DialogEventArgs>(this.DialogManager_onDialogEvent);
      if (!(bool) ((UnityEngine.Object) this._panel))
        return;
      this._panel.depth = this._startDepth;
    }

    [Serializable]
    public class AnchorDefinition
    {
      public bool relativeOffsetFromTarget;
      public bool absoluteOffsetFromTarget;
      public UIRect.AnchorPoint anchor;

      public void SetVertical(UIRect.AnchorPoint to)
      {
        float relative = !this.relativeOffsetFromTarget ? this.anchor.relative : this.anchor.relative * this.anchor.target.localScale.y;
        float absolute = !this.absoluteOffsetFromTarget ? (float) this.anchor.absolute : (float) this.anchor.absolute * this.anchor.target.localScale.y;
        to.Set(relative, absolute);
      }
    }
  }
}
