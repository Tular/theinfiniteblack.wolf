﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UI.UISpriteColorGroupControllerEventListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity.UI
{
  public class UISpriteColorGroupControllerEventListener : MonoBehaviour
  {
    public Color normal = new Color(1f, 1f, 1f, 1f);
    public Color hover = new Color(1f, 1f, 1f, 1f);
    public Color pressed = new Color(1f, 1f, 1f, 1f);
    public Color selected = new Color(1f, 1f, 1f, 1f);
    public Color disabled = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    public float duration = 0.2f;
    private UISpriteColorGroupController target;
    public int groupId;
    public bool stickySelect;
    private bool _cancelHover;
    private bool _stickPress;
    private bool _isEnabled;

    public void SetPressed(bool isPressed, bool holdPressState)
    {
      if (!((Object) this.target != (Object) null))
        return;
      this._stickPress = holdPressState;
      if (isPressed)
        this.target.SetColor(this.groupId, this.duration, this.pressed);
      else
        this.target.SetColor(this.groupId, this.duration, this.normal);
    }

    public bool isEnabled
    {
      get
      {
        if (!this.enabled)
          return false;
        Collider component = this.GetComponent<Collider>();
        if ((bool) ((Object) component))
          return component.enabled;
        return false;
      }
      set
      {
        Collider component = this.GetComponent<Collider>();
        if (!((Object) component != (Object) null))
          return;
        component.enabled = value;
        if (!((Object) this.target != (Object) null))
          return;
        this.target.SetColor(this.groupId, this.duration, this.disabled);
      }
    }

    private void OnPress(bool isPressed)
    {
      if (!this.target.enabled || UICamera.currentTouch == null || (!((Object) this.target != (Object) null) || this._stickPress))
        return;
      if (isPressed)
        this.target.SetColor(this.groupId, this.duration, this.pressed);
      else
        this.target.SetColor(this.groupId, this.duration, this.normal);
    }

    private void OnHover(bool isOver)
    {
      if (!this.target.enabled || this._stickPress)
        return;
      this.target.SetColor(this.groupId, this.duration, !isOver ? this.normal : this.hover);
    }

    private void OnEnable()
    {
      if (!(bool) ((Object) this.target))
        this.target = this.GetComponent<UISpriteColorGroupController>();
      if ((bool) ((Object) this.target))
        return;
      this.enabled = false;
    }

    private void OnDisable()
    {
      if (!((Object) this.target != (Object) null) || this._stickPress)
        return;
      this.target.SetColor(this.groupId, this.normal);
    }
  }
}
