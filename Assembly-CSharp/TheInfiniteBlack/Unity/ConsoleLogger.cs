﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ConsoleLogger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class ConsoleLogger : LoggerView
  {
    public UITextList debugConsole;

    protected override void UpdateLogView(string line)
    {
      this.debugConsole.Add(line);
    }

    protected override void Awake()
    {
      base.Awake();
      if ((bool) ((Object) this.debugConsole))
        return;
      this.debugConsole = this.GetComponent<UITextList>();
    }
  }
}
