﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.SpriteAssetMapping
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;

namespace TheInfiniteBlack.Unity
{
  [Serializable]
  public class SpriteAssetMapping
  {
    public UIAtlas cardAtlas;
    public UIAtlas mapAtlas;
    public string cardSpriteName;
    public string mapSpriteName;
  }
}
