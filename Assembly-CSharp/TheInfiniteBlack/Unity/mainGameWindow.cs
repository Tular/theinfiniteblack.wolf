﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.mainGameWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Unity.Map;
using TheInfiniteBlack.Unity.SettingsUI;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class mainGameWindow : MonoBehaviour
  {
    public void ShowChatClient()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        throw new Exception("DialogManager could not be found");
      UICamera.selectedObject = (GameObject) null;
      DialogManager.ShowDialog<ChatDialog>(false);
    }

    public void CloseChatClient()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        throw new Exception("DialogManager could not be found");
      DialogManager.CloseDialog<ChatDialog>();
    }

    public void ShowSystemDialog()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        throw new Exception("DialogManager could not be found");
      DialogManager.ShowDialog<SystemDialog>(false);
    }

    public void ShowBlackDollarPurchaseDialog()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        throw new Exception("DialogManager could not be found");
      DialogManager.ShowDialog<BlackDollarPurchasesDialog>(false);
    }

    public void ShowSetCourseDialog()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        throw new Exception("DialogManager could not be found");
      DialogManager.ShowDialog<WaypointsDialog>(false);
    }

    public void HideSettingsWindow()
    {
      if ((UnityEngine.Object) PreInstantiatedSingleton<DialogManager>.Instance == (UnityEngine.Object) null)
        throw new Exception("DialogManager could not be found");
      DialogManager.CloseDialog<SettingsWindow>();
    }

    public void ToggleRiftView()
    {
      PreInstantiatedSingleton<SectorMap>.Instance.ToggleUniverseView();
    }

    private void Awake()
    {
    }

    private void OnDestroy()
    {
    }
  }
}
