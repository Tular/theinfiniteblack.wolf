﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.UIScaleManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class UIScaleManager : PreInstantiatedSingleton<UIScaleManager>
  {
    public int manualWidth = 1280;
    public int manualHeight = 720;
    public int minimumHeight = 320;
    public int maximumHeight = 1536;
    public UIRoot.Scaling scalingStyle;
    public bool fitWidth;
    public bool fitHeight;
    public bool adjustByDPI;
    public bool shrinkPortraitUI;
    private bool _hasChanged;
    private int _prevScreenHeight;
    private int _prevScreenWidth;

    public static void UpdateUiScale()
    {
      PreInstantiatedSingleton<UIScaleManager>.Instance.ApplyChanges();
    }

    public void ResetUIScale()
    {
      this.manualHeight = Screen.height;
    }

    private void ApplyChanges()
    {
      Debug.Log((object) string.Format("ApplyChanged - Applying UI Scale Changes"));
      TheInfiniteBlack.Library.Log.I((object) this, "ApplyChanged", "Applying UI Scale Changes");
      using (List<UIRoot>.Enumerator enumerator = UIRoot.list.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UIRoot current = enumerator.Current;
          SBUIRoot sbuiRoot = current as SBUIRoot;
          if ((Object) sbuiRoot != (Object) null && sbuiRoot.managedScale)
          {
            sbuiRoot.scalingStyle = this.scalingStyle;
            sbuiRoot.manualWidth = this.manualWidth;
            sbuiRoot.manualHeight = this.manualHeight;
            sbuiRoot.minimumHeight = this.minimumHeight;
            sbuiRoot.maximumHeight = this.maximumHeight;
            sbuiRoot.fitWidth = this.fitWidth;
            sbuiRoot.fitHeight = this.fitHeight;
            current.adjustByDPI = this.adjustByDPI;
            current.shrinkPortraitUI = this.shrinkPortraitUI;
          }
        }
      }
      this._hasChanged = true;
    }

    protected override void Awake()
    {
      base.Awake();
      int num = (double) ((float) Screen.width / (float) Screen.height) >= 1.5 ? 640 : Mathf.RoundToInt((float) Screen.height * 960f / (float) Screen.width);
      this._prevScreenHeight = Screen.height;
      this._prevScreenWidth = Screen.width;
    }

    private void Update()
    {
      if (Screen.height != this._prevScreenHeight || Screen.width != this._prevScreenWidth || !this._hasChanged)
        return;
      this.ApplyChanges();
      this._hasChanged = false;
    }

    private void OnScreenSizeChanged()
    {
      this.manualHeight = (double) ((float) Screen.width / (float) Screen.height) >= 1.5 ? 640 : Mathf.RoundToInt((float) Screen.height * 960f / (float) Screen.width);
      this._prevScreenHeight = Screen.height;
      this._prevScreenWidth = Screen.width;
    }
  }
}
