﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.IUnityLogger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;

namespace TheInfiniteBlack.Unity
{
  public interface IUnityLogger : TheInfiniteBlack.Library.ILogger
  {
    ClientLogLevel clientLogLevel { get; set; }

    void I(UnityEngine.Object context, string log);

    void D(UnityEngine.Object context, string log, params object[] args);

    void D(UnityEngine.Object context, string log);

    void W(UnityEngine.Object context, string log);

    void E(UnityEngine.Object context, Exception e);
  }
}
