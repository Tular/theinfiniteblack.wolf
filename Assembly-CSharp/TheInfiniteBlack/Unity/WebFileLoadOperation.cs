﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.WebFileLoadOperation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.IO;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Client.Events;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class WebFileLoadOperation : IEnumerator, IWebFileLoadOperation
  {
    private readonly WebFileDefinition _fileDef;
    private WWW _www;
    private bool _isDone;

    public WebFileLoadOperation(WebFileDefinition fileDef)
    {
      this._fileDef = fileDef;
      this.fileName = this._fileDef.fileName;
      this.localPath = Path.Combine(Application.persistentDataPath, this.fileName);
    }

    public string fileAlias
    {
      get
      {
        return this._fileDef.fileAlias;
      }
    }

    public string fileText { get; private set; }

    public string fileName { get; private set; }

    public string localPath { get; private set; }

    public string url
    {
      get
      {
        return this._fileDef.fileUrl;
      }
    }

    public bool wwwLoadSuccess { get; private set; }

    public string wwwError { get; private set; }

    public float nextRefreshTime { get; private set; }

    public bool MoveNext()
    {
      return !this.IsDone();
    }

    public void Reset()
    {
      this._www = (WWW) null;
      this._isDone = false;
      this.wwwError = string.Empty;
      this.wwwLoadSuccess = false;
    }

    public object Current
    {
      get
      {
        return (object) null;
      }
    }

    public bool Update()
    {
      if (this._isDone)
        return false;
      if (this._www == null)
      {
        this._www = new WWW(this.url);
        return true;
      }
      if (!this._www.isDone)
        return true;
      if (string.IsNullOrEmpty(this._www.error))
      {
        this.fileText = this._www.text.Trim();
        this.wwwLoadSuccess = true;
        if (this._fileDef.isCached)
          this.TrySaveLocal();
      }
      if (!string.IsNullOrEmpty(this._www.error))
      {
        this.wwwLoadSuccess = false;
        this.wwwError = this._www.error;
        this.TryLoadLocal();
      }
      this._isDone = true;
      this.nextRefreshTime = Time.unscaledTime + this._fileDef.refreshInterval;
      this._www.Dispose();
      this._www = (WWW) null;
      return false;
    }

    public bool IsDone()
    {
      return this._isDone;
    }

    private bool TryLoadLocal()
    {
      try
      {
        ClientLog.Instance.D((object) this, nameof (TryLoadLocal), "Attempting to load local file: " + this.localPath);
        if (!File.Exists(this.localPath))
        {
          TibProxy.Instance.Show(new PopupWindowEventArgs((IGameState) null, "Unable to load server IP file or find cached IP address.  check your network connection", true));
          return false;
        }
        this.fileText = File.ReadAllText(this.localPath);
        ClientLog.Instance.D((object) this, nameof (TryLoadLocal), "Successfully loaded local file: " + this.localPath);
        return true;
      }
      catch (Exception ex)
      {
        ClientLog.Instance.E((object) this, nameof (TryLoadLocal), ex);
        return false;
      }
    }

    private bool TrySaveLocal()
    {
      try
      {
        ClientLog.Instance.D((object) this, nameof (TrySaveLocal), "Attempting to save local file: " + this.localPath);
        using (FileStream fileStream = new FileStream(this.localPath, FileMode.Create))
        {
          using (StreamWriter streamWriter = new StreamWriter((Stream) fileStream))
            streamWriter.Write(this.fileText);
        }
        ClientLog.Instance.D((object) this, nameof (TrySaveLocal), "Successfully saved local file: " + this.localPath);
        return true;
      }
      catch (Exception ex)
      {
        ClientLog.Instance.E((object) this, nameof (TrySaveLocal), ex);
        return false;
      }
    }
  }
}
