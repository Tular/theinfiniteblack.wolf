﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.TibButtonBuyJump
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

namespace TheInfiniteBlack.Unity
{
  public class TibButtonBuyJump : MonoBehaviour
  {
    public TibButtonBuyJump.JumpType jumpType;
    private UISpriteColorGroupControllerEventListener[] _spriteColorgroupEventListeners;
    private bool _isJumping;

    private void DoJump()
    {
      Debug.Log((object) string.Format("TibButtonJumptoEarth.DoBlackDollarRepair()"));
      if (TibProxy.gameState == null)
        return;
      switch (this.jumpType)
      {
        case TibButtonBuyJump.JumpType.Earth:
          TibProxy.gameState.DoPurchase(PurchaseItem.EarthJump);
          break;
        case TibButtonBuyJump.JumpType.Rift:
          TibProxy.gameState.DoPurchase(PurchaseItem.RiftJump);
          break;
        case TibButtonBuyJump.JumpType.Corp:
          TibProxy.gameState.DoPurchase(PurchaseItem.CorpJump);
          break;
      }
    }

    private bool isJumping
    {
      get
      {
        return this._isJumping;
      }
      set
      {
        if (this._isJumping == value)
          return;
        if (!value)
        {
          foreach (UISpriteColorGroupControllerEventListener colorgroupEventListener in this._spriteColorgroupEventListeners)
            colorgroupEventListener.SetPressed(false, false);
        }
        else
        {
          foreach (UISpriteColorGroupControllerEventListener colorgroupEventListener in this._spriteColorgroupEventListeners)
            colorgroupEventListener.SetPressed(true, true);
        }
        this._isJumping = value;
      }
    }

    private void OnPress(bool isPressed)
    {
      if (!isPressed)
        return;
      this.DoJump();
    }

    private void Awake()
    {
      this._spriteColorgroupEventListeners = this.GetComponents<UISpriteColorGroupControllerEventListener>();
    }

    private void OnEnable()
    {
      if (TibProxy.gameState == null)
      {
        this._isJumping = true;
        this.isJumping = false;
      }
      else
      {
        bool flag = !TibProxy.gameState.EarthJumpTime.IsFinished;
        this._isJumping = !flag;
        this.isJumping = flag;
      }
    }

    private void Update()
    {
      if (TibProxy.gameState == null)
        return;
      this.isJumping = !TibProxy.gameState.EarthJumpTime.IsFinished;
    }

    public enum JumpType
    {
      Earth,
      Rift,
      Corp,
    }
  }
}
