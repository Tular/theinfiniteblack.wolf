﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.MockSector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Reflection;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;

namespace TheInfiniteBlack.Unity
{
  public class MockSector : Sector
  {
    private static readonly FieldInfo _fldPlayers = typeof (Sector).GetField("_players", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldStatus = typeof (Sector).GetField("_status", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private static readonly FieldInfo _fldClass = typeof (Sector).GetField("_class", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
    private readonly bool _isRift;

    public MockSector(int x, int y, bool isRift)
      : base(x, y)
    {
      this._isRift = isRift;
    }

    public List<ClientPlayer> moqPlayers
    {
      get
      {
        return MockSector._fldPlayers.GetValue((object) this) as List<ClientPlayer>;
      }
    }

    public override bool Rift
    {
      get
      {
        return this._isRift;
      }
    }

    public MockSector SetStatus(SectorStatus status)
    {
      MockSector._fldStatus.SetValue((object) this, (object) status);
      return this;
    }

    public MockSector SetClass(SectorClass sectorClass)
    {
      MockSector._fldClass.SetValue((object) this, (object) sectorClass);
      return this;
    }

    public void AddPlayer(ClientPlayer player)
    {
      this.Add(player);
    }

    public void RemovePlayer(ClientPlayer player)
    {
      this.Remove(player);
    }
  }
}
