﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.AccountCharacterTuple
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client.Settings;

namespace TheInfiniteBlack.Unity
{
  public class AccountCharacterTuple
  {
    public AccountCharacterTuple(Account accnt, Character chr)
    {
      this.account = accnt;
      this.character = chr;
    }

    public Account account { get; private set; }

    public Character character { get; private set; }

    public string accountName
    {
      get
      {
        return this.account.Name;
      }
    }

    public string accountPassword
    {
      get
      {
        return this.account.Password;
      }
    }

    public bool isPasswordSaved
    {
      get
      {
        return !string.IsNullOrEmpty(this.account.Password);
      }
    }

    public override string ToString()
    {
      return string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n", (object) this.account.Name, (object) this.account.Hardcore, (object) this.character.ServerID, (object) this.character.Corporation, (object) this.character.Level, (object) this.character.Ship, (object) this.character.get_LastActivity());
    }
  }
}
