﻿// Decompiled with JetBrains decompiler
// Type: TheInfiniteBlack.Unity.ClientLogLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

namespace TheInfiniteBlack.Unity
{
  public enum ClientLogLevel
  {
    None = 1,
    Error = 2,
    Warning = 3,
    Info = 4,
    Debug = 5,
  }
}
