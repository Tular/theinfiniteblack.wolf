﻿// Decompiled with JetBrains decompiler
// Type: TwoDragMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TwoDragMe : MonoBehaviour
{
  private TextMesh textMesh;
  private Vector3 deltaPosition;
  private Color startColor;

  private void OnEnable()
  {
    EasyTouch.On_DragStart2Fingers += new EasyTouch.DragStart2FingersHandler(this.On_DragStart2Fingers);
    EasyTouch.On_Drag2Fingers += new EasyTouch.Drag2FingersHandler(this.On_Drag2Fingers);
    EasyTouch.On_DragEnd2Fingers += new EasyTouch.DragEnd2FingersHandler(this.On_DragEnd2Fingers);
    EasyTouch.On_Cancel2Fingers += new EasyTouch.Cancel2FingersHandler(this.On_Cancel2Fingers);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_DragStart2Fingers -= new EasyTouch.DragStart2FingersHandler(this.On_DragStart2Fingers);
    EasyTouch.On_Drag2Fingers -= new EasyTouch.Drag2FingersHandler(this.On_Drag2Fingers);
    EasyTouch.On_DragEnd2Fingers -= new EasyTouch.DragEnd2FingersHandler(this.On_DragEnd2Fingers);
    EasyTouch.On_Cancel2Fingers -= new EasyTouch.Cancel2FingersHandler(this.On_Cancel2Fingers);
  }

  private void Start()
  {
    this.textMesh = this.GetComponentInChildren<TextMesh>();
    this.startColor = this.gameObject.GetComponent<Renderer>().material.color;
  }

  private void On_DragStart2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.RandomColor();
    this.deltaPosition = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position) - this.transform.position;
  }

  private void On_Drag2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.transform.position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position) - this.deltaPosition;
    this.textMesh.text = gesture.GetSwipeOrDragAngle().ToString("f2") + " / " + gesture.swipe.ToString();
  }

  private void On_DragEnd2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.gameObject.GetComponent<Renderer>().material.color = this.startColor;
    this.textMesh.text = "Drag me";
  }

  private void On_Cancel2Fingers(Gesture gesture)
  {
    this.On_DragEnd2Fingers(gesture);
  }

  private void RandomColor()
  {
    this.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1f), Random.Range(0.0f, 1f), Random.Range(0.0f, 1f));
  }
}
