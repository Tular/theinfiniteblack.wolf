﻿// Decompiled with JetBrains decompiler
// Type: PooledScrollViewBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public abstract class PooledScrollViewBase : UIScrollView
{
  private readonly HashSet<IScrollViewCellInfo> _visibleLastFrame = new HashSet<IScrollViewCellInfo>();
  public SpawnPool cellPool;
  public Vector2 cellPadding;
  public int topBuffer;
  public int bottomBuffer;
  public int leftBuffer;
  public int rightBuffer;
  private int _lastMoveFrame;
  private IPooledScrollViewCellInfoCollection _data;
  private bool _isInitialized;
  public int lastFrameVisible;

  public bool isMoving
  {
    get
    {
      return this._lastMoveFrame == Time.frameCount;
    }
  }

  public bool reposition { get; set; }

  protected bool mBackingDataChanged { get; private set; }

  public IPooledScrollViewCellInfoCollection data
  {
    get
    {
      return this._data;
    }
    set
    {
      if (value == this._data)
        return;
      this._data = value;
      this.mBackingDataChanged = true;
    }
  }

  public override Bounds bounds
  {
    get
    {
      if (!this.mCalculatedBounds)
      {
        this.mCalculatedBounds = true;
        this.mTrans = this.transform;
        this.mBounds = this.CalculateBounds();
      }
      return this.mBounds;
    }
  }

  public Bounds bufferBounds
  {
    get
    {
      Vector2 viewSize = this.panel.GetViewSize();
      bool flag = this.panel.clipping != UIDrawCall.Clipping.None;
      Vector4 finalClipRegion = this.panel.finalClipRegion;
      Vector3 center = new Vector3(finalClipRegion.x, finalClipRegion.y);
      if (!flag)
        return new Bounds(center, new Vector3(viewSize.x, viewSize.y));
      float num1 = finalClipRegion.w / 2f;
      float num2 = finalClipRegion.z / 2f;
      Vector3 max = new Vector3(finalClipRegion.x + num2 + (float) this.rightBuffer, finalClipRegion.y + num1 + (float) this.topBuffer);
      Vector3 min = new Vector3(finalClipRegion.x - num2 - (float) this.leftBuffer, finalClipRegion.y - num1 - (float) this.bottomBuffer);
      Bounds bounds = new Bounds();
      bounds.SetMinMax(min, max);
      return bounds;
    }
  }

  protected virtual void UpdateCellPositions()
  {
    if (this.data == null || this.data.values == null)
      return;
    Vector3 zero = Vector3.zero;
    foreach (IScrollViewCellInfo scrollViewCellInfo in this.data.values)
    {
      switch (this.contentPivot)
      {
        case UIWidget.Pivot.Top:
          scrollViewCellInfo.localPosition = zero - new Vector3(-this.cellPadding.x, scrollViewCellInfo.extents.y);
          zero -= new Vector3(0.0f, 2f * scrollViewCellInfo.extents.y + this.cellPadding.y);
          continue;
        case UIWidget.Pivot.Bottom:
          scrollViewCellInfo.localPosition = zero + new Vector3(this.cellPadding.x, scrollViewCellInfo.extents.y);
          zero += new Vector3(0.0f, 2f * scrollViewCellInfo.extents.y + this.cellPadding.y);
          continue;
        default:
          continue;
      }
    }
  }

  private void Init()
  {
    if (this._isInitialized)
      return;
    this.OnInit();
    this._isInitialized = true;
  }

  protected virtual void OnInit()
  {
  }

  protected void UpdateVisibleCellData()
  {
    if (!Application.isPlaying || this.data == null || this.data.values == null)
      return;
    Bounds bufferBounds = this.bufferBounds;
    IEnumerable<IScrollViewCellInfo> values = this.data.values;
    HashSet<IScrollViewCellInfo> scrollViewCellInfoSet = new HashSet<IScrollViewCellInfo>((IEnumerable<IScrollViewCellInfo>) this._visibleLastFrame);
    this.lastFrameVisible = this._visibleLastFrame.Count;
    float a1 = this.bounds.max.y;
    float a2 = this.bounds.min.y;
    foreach (IScrollViewCellInfo scrollViewCellInfo1 in values)
    {
      IScrollViewCellInfo scrollViewCellInfo2 = scrollViewCellInfo1;
      bool flag = scrollViewCellInfo2.IsContainedIn(bufferBounds);
      if (scrollViewCellInfoSet.Contains(scrollViewCellInfo1))
        scrollViewCellInfoSet.Remove(scrollViewCellInfo1);
      if (!flag && scrollViewCellInfo2.isBound)
      {
        if (this._visibleLastFrame.Contains(scrollViewCellInfo1))
          this._visibleLastFrame.Remove(scrollViewCellInfo1);
        this.cellPool.Despawn(scrollViewCellInfo2.cellTransform, this.cellPool.transform);
        scrollViewCellInfo2.Unbind();
      }
      else if (flag && !scrollViewCellInfo2.isBound)
      {
        Transform cellTrans = this.cellPool.Spawn(scrollViewCellInfo2.cellPrefab, this.transform);
        scrollViewCellInfo2.BindCell(cellTrans);
        this._visibleLastFrame.Add(scrollViewCellInfo2);
      }
      if (flag)
      {
        a1 = Mathf.Min(a1, scrollViewCellInfo1.localPosition.y - scrollViewCellInfo1.extents.y);
        a2 = Mathf.Max(a2, scrollViewCellInfo1.localPosition.y + scrollViewCellInfo1.extents.y);
      }
    }
    using (HashSet<IScrollViewCellInfo>.Enumerator enumerator = scrollViewCellInfoSet.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IScrollViewCellInfo current = enumerator.Current;
        if (this._visibleLastFrame.Contains(current))
        {
          if (current.isBound)
          {
            this.cellPool.Despawn(current.cellTransform);
            current.Unbind();
          }
          this._visibleLastFrame.Remove(current);
        }
      }
    }
    if (this._visibleLastFrame.Count == 0)
    {
      this.RestrictWithinBounds(false);
    }
    else
    {
      if (this.isMoving || this.isDragging)
        return;
      Vector4 finalClipRegion = this.panel.finalClipRegion;
      float f1 = finalClipRegion.y - finalClipRegion.w * 0.5f;
      float f2 = finalClipRegion.y + finalClipRegion.w * 0.5f;
      if ((double) a2 < (double) Mathf.Floor(f2))
        this.SetDragAmount(0.5f, 0.0f, false);
      if ((double) a1 <= (double) Mathf.Ceil(f2) && ((double) a1 <= (double) Mathf.Ceil(f1) || (double) a2 <= (double) Mathf.Ceil(f2)))
        return;
      this.SetDragAmount(0.5f, 1f, false);
    }
  }

  protected virtual void OnMove(UIPanel p)
  {
    if (this.mBackingDataChanged)
      return;
    this.reposition = true;
    this._lastMoveFrame = Time.frameCount;
  }

  protected override void Start()
  {
    base.Start();
    this.Init();
    this.panel.onClipMove = new UIPanel.OnClippingMoved(this.OnMove);
    this.ResetPosition();
  }

  protected override void OnEnable()
  {
    base.OnEnable();
    if (!Application.isPlaying)
      return;
    this.Init();
    this.StartCoroutine(this.RepositionCoroutine());
    this.reposition = true;
  }

  private Bounds CalculateBounds()
  {
    Vector4 finalClipRegion = this.panel.finalClipRegion;
    if (!Application.isPlaying || this.data == null || this.data.Count < 1)
      return new Bounds(Vector3.zero, new Vector3(finalClipRegion.z, 20f));
    IScrollViewCellInfo first = this.data.first;
    IScrollViewCellInfo last = this.data.last;
    Bounds bounds = new Bounds();
    switch (this.contentPivot)
    {
      case UIWidget.Pivot.TopLeft:
      case UIWidget.Pivot.Top:
      case UIWidget.Pivot.TopRight:
        Vector3 max1 = first.localPosition + first.extents;
        Vector3 min1 = last.localPosition - last.extents;
        bounds.SetMinMax(min1, max1);
        break;
      case UIWidget.Pivot.BottomLeft:
      case UIWidget.Pivot.Bottom:
      case UIWidget.Pivot.BottomRight:
        Vector3 max2 = last.localPosition + last.extents;
        Vector3 min2 = first.localPosition - first.extents;
        bounds.SetMinMax(min2, max2);
        break;
    }
    return bounds;
  }

  protected void ResetVisibleInfos()
  {
    using (HashSet<IScrollViewCellInfo>.Enumerator enumerator = this._visibleLastFrame.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IScrollViewCellInfo current = enumerator.Current;
        if ((Object) current.cellTransform != (Object) null)
          this.cellPool.Despawn(current.cellTransform);
        current.Unbind();
      }
    }
    this._visibleLastFrame.Clear();
  }

  protected virtual void OnUpdateCellPositions()
  {
    this.UpdateCellPositions();
    this.InvalidateBounds();
    this.UpdateVisibleCellData();
    if (!this.shouldMoveVertically)
      this.ResetPosition();
    else
      this.UpdatePosition();
  }

  protected virtual void OnResetCellPositions()
  {
    this.ResetVisibleInfos();
    this.ResetPosition();
    this.UpdateCellPositions();
    this.InvalidateBounds();
    this.UpdateVisibleCellData();
    this.ResetPosition();
    this.RestrictWithinBounds(true);
  }

  [DebuggerHidden]
  private IEnumerator RepositionCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PooledScrollViewBase.\u003CRepositionCoroutine\u003Ec__Iterator1A()
    {
      \u003C\u003Ef__this = this
    };
  }
}
