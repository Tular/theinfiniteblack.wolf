﻿// Decompiled with JetBrains decompiler
// Type: ShipStats
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class ShipStats : MonoBehaviour
{
  public string shipSpritePrefix = "entity_ship_";
  public UILabel title;
  public UILabel subtitle;
  public UILabel playerLevel;
  public UILabel shipClass;
  public UISprite shipImage;
  public UILabel equipPointsLabel;
  public UITextList attackStats;
  public UITextList defenseStats;
  private string _currentFormattedOffensiveStats;
  private string _currentFormattedDefensiveStats;

  [ContextMenu("Current Ship Stats")]
  public void ShowCurrentShipStats()
  {
    this.UpdateVisuals(TibProxy.gameState.MyShip);
  }

  public void UpdateVisuals(Ship ship)
  {
    if (ship == null)
      return;
    if ((bool) ((UnityEngine.Object) this.attackStats))
    {
      string str = ShipStatsFormatter.FormattedOffensiveStats(ship);
      if (!str.Equals(this._currentFormattedOffensiveStats))
      {
        this.attackStats.Clear();
        this.attackStats.Add(ShipStatsFormatter.FormattedOffensiveStats(ship));
        this._currentFormattedOffensiveStats = str;
      }
    }
    if ((bool) ((UnityEngine.Object) this.defenseStats))
    {
      string str = ShipStatsFormatter.FormattedDefensiveStats(ship);
      if (!str.Equals(this._currentFormattedDefensiveStats))
      {
        this.defenseStats.Clear();
        this.defenseStats.Add(ShipStatsFormatter.FormattedDefensiveStats(ship));
        this._currentFormattedDefensiveStats = str;
      }
    }
    if ((bool) ((UnityEngine.Object) this.equipPointsLabel))
      this.equipPointsLabel.text = string.Format("{0} of {1} EquipPoints Used", (object) ship.UsedEP, (object) ship.MaxEP);
    if ((bool) ((UnityEngine.Object) this.title))
      this.title.text = ship.Title;
    if ((bool) ((UnityEngine.Object) this.subtitle))
      this.subtitle.text = ship.SubTitle;
    if ((bool) ((UnityEngine.Object) this.playerLevel))
      this.playerLevel.text = string.Format("Level: {0:F2}", (object) ship.Level);
    if ((bool) ((UnityEngine.Object) this.shipClass))
      this.shipClass.text = ((Enum) ship.Class).ToString();
    if (!(bool) ((UnityEngine.Object) this.shipImage))
      return;
    this.shipImage.spriteName = string.Format("{0}{1}", (object) this.shipSpritePrefix, (object) ship.Class);
  }

  public void ResetVisuals()
  {
    this._currentFormattedOffensiveStats = string.Empty;
    this._currentFormattedDefensiveStats = string.Empty;
    if ((bool) ((UnityEngine.Object) this.title))
      this.title.text = string.Empty;
    if ((bool) ((UnityEngine.Object) this.subtitle))
      this.subtitle.text = string.Empty;
    if ((bool) ((UnityEngine.Object) this.playerLevel))
      this.playerLevel.text = string.Empty;
    if ((bool) ((UnityEngine.Object) this.shipClass))
      this.shipClass.text = string.Empty;
    if ((bool) ((UnityEngine.Object) this.equipPointsLabel))
      this.equipPointsLabel.text = string.Empty;
    if ((bool) ((UnityEngine.Object) this.attackStats))
      this.attackStats.Clear();
    if (!(bool) ((UnityEngine.Object) this.defenseStats))
      return;
    this.defenseStats.Clear();
  }

  private void Start()
  {
    this.ResetVisuals();
  }

  private void OnDisable()
  {
    this.ResetVisuals();
  }
}
