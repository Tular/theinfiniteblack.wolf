﻿// Decompiled with JetBrains decompiler
// Type: ETCDPad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ETCDPad : ETCBase, IPointerUpHandler, IEventSystemHandler, IPointerDownHandler, IDragHandler
{
  [SerializeField]
  public ETCDPad.OnMoveStartHandler onMoveStart;
  [SerializeField]
  public ETCDPad.OnMoveHandler onMove;
  [SerializeField]
  public ETCDPad.OnMoveSpeedHandler onMoveSpeed;
  [SerializeField]
  public ETCDPad.OnMoveEndHandler onMoveEnd;
  [SerializeField]
  public ETCDPad.OnTouchStartHandler onTouchStart;
  [SerializeField]
  public ETCDPad.OnTouchUPHandler onTouchUp;
  [SerializeField]
  public ETCDPad.OnDownUpHandler OnDownUp;
  [SerializeField]
  public ETCDPad.OnDownDownHandler OnDownDown;
  [SerializeField]
  public ETCDPad.OnDownLeftHandler OnDownLeft;
  [SerializeField]
  public ETCDPad.OnDownRightHandler OnDownRight;
  [SerializeField]
  public ETCDPad.OnDownUpHandler OnPressUp;
  [SerializeField]
  public ETCDPad.OnDownDownHandler OnPressDown;
  [SerializeField]
  public ETCDPad.OnDownLeftHandler OnPressLeft;
  [SerializeField]
  public ETCDPad.OnDownRightHandler OnPressRight;
  public ETCAxis axisX;
  public ETCAxis axisY;
  public UnityEngine.Sprite normalSprite;
  public Color normalColor;
  public UnityEngine.Sprite pressedSprite;
  public Color pressedColor;
  private Vector2 tmpAxis;
  private Vector2 OldTmpAxis;
  private bool isOnTouch;
  private Image cachedImage;

  public ETCDPad()
  {
    this.axisX = new ETCAxis("Horizontal");
    this.axisY = new ETCAxis("Vertical");
    this._visible = true;
    this._activated = true;
    this.dPadAxisCount = ETCBase.DPadAxis.Two_Axis;
    this.tmpAxis = Vector2.zero;
    this.showPSInspector = true;
    this.showSpriteInspector = false;
    this.showBehaviourInspector = false;
    this.showEventInspector = false;
    this.isOnDrag = false;
    this.isOnTouch = false;
    this.axisX.positivekey = KeyCode.RightArrow;
    this.axisX.negativeKey = KeyCode.LeftArrow;
    this.axisY.positivekey = KeyCode.UpArrow;
    this.axisY.negativeKey = KeyCode.DownArrow;
    this.enableKeySimulation = true;
    this.enableKeySimulation = false;
  }

  private void Start()
  {
    this.tmpAxis = Vector2.zero;
    this.OldTmpAxis = Vector2.zero;
    this.axisX.InitAxis();
    this.axisY.InitAxis();
  }

  protected override void UpdateControlState()
  {
    this.UpdateDPad();
  }

  public void OnPointerDown(PointerEventData eventData)
  {
    if (!this._activated || this.isOnTouch)
      return;
    this.onTouchStart.Invoke();
    this.GetTouchDirection(eventData.position, eventData.pressEventCamera);
    this.isOnTouch = true;
    this.isOnDrag = true;
    this.pointId = eventData.pointerId;
  }

  public void OnDrag(PointerEventData eventData)
  {
    if (!this._activated || this.pointId != eventData.pointerId)
      return;
    this.isOnTouch = true;
    this.isOnDrag = true;
    this.GetTouchDirection(eventData.position, eventData.pressEventCamera);
  }

  public void OnPointerUp(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId)
      return;
    this.isOnTouch = false;
    this.isOnDrag = false;
    this.tmpAxis = Vector2.zero;
    this.OldTmpAxis = Vector2.zero;
    this.axisX.axisState = ETCAxis.AxisState.None;
    this.axisY.axisState = ETCAxis.AxisState.None;
    if (!this.axisX.isEnertia && !this.axisY.isEnertia)
    {
      this.axisX.ResetAxis();
      this.axisY.ResetAxis();
      this.onMoveEnd.Invoke();
    }
    this.pointId = -1;
    this.onTouchUp.Invoke();
  }

  private void UpdateDPad()
  {
    if (this.enableKeySimulation && !this.isOnTouch && (this._activated && this._visible))
    {
      this.isOnDrag = false;
      this.tmpAxis = Vector2.zero;
      if (Input.GetKey(this.axisX.positivekey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(1f, this.tmpAxis.y);
      }
      else if (Input.GetKey(this.axisX.negativeKey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(-1f, this.tmpAxis.y);
      }
      if (Input.GetKey(this.axisY.positivekey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(this.tmpAxis.x, 1f);
      }
      else if (Input.GetKey(this.axisY.negativeKey))
      {
        this.isOnDrag = true;
        this.tmpAxis = new Vector2(this.tmpAxis.x, -1f);
      }
    }
    this.OldTmpAxis.x = this.axisX.axisValue;
    this.OldTmpAxis.y = this.axisY.axisValue;
    this.axisX.UpdateAxis(this.tmpAxis.x, this.isOnDrag, ETCBase.ControlType.DPad, true);
    this.axisY.UpdateAxis(this.tmpAxis.y, this.isOnDrag, ETCBase.ControlType.DPad, true);
    this.axisX.DoGravity();
    this.axisY.DoGravity();
    if (((double) this.axisX.axisValue != 0.0 || (double) this.axisY.axisValue != 0.0) && this.OldTmpAxis == Vector2.zero)
      this.onMoveStart.Invoke();
    if ((double) this.axisX.axisValue != 0.0 || (double) this.axisY.axisValue != 0.0)
    {
      if (this.axisX.actionOn == ETCAxis.ActionOn.Down && (this.axisX.axisState == ETCAxis.AxisState.DownLeft || this.axisX.axisState == ETCAxis.AxisState.DownRight))
        this.axisX.DoDirectAction();
      else if (this.axisX.actionOn == ETCAxis.ActionOn.Press)
        this.axisX.DoDirectAction();
      if (this.axisY.actionOn == ETCAxis.ActionOn.Down && (this.axisY.axisState == ETCAxis.AxisState.DownUp || this.axisY.axisState == ETCAxis.AxisState.DownDown))
        this.axisY.DoDirectAction();
      else if (this.axisY.actionOn == ETCAxis.ActionOn.Press)
        this.axisY.DoDirectAction();
      this.onMove.Invoke(new Vector2(this.axisX.axisValue, this.axisY.axisValue));
      this.onMoveSpeed.Invoke(new Vector2(this.axisX.axisSpeedValue, this.axisY.axisSpeedValue));
    }
    else if ((double) this.axisX.axisValue == 0.0 && (double) this.axisY.axisValue == 0.0 && this.OldTmpAxis != Vector2.zero)
      this.onMoveEnd.Invoke();
    float num1 = 1f;
    if (this.axisX.invertedAxis)
      num1 = -1f;
    if ((double) this.OldTmpAxis.x == 0.0 && (double) Mathf.Abs(this.axisX.axisValue) > 0.0)
    {
      if ((double) this.axisX.axisValue * (double) num1 > 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.DownRight;
        this.OnDownRight.Invoke();
      }
      else if ((double) this.axisX.axisValue * (double) num1 < 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.DownLeft;
        this.OnDownLeft.Invoke();
      }
      else
        this.axisX.axisState = ETCAxis.AxisState.None;
    }
    else if (this.axisX.axisState != ETCAxis.AxisState.None)
    {
      if ((double) this.axisX.axisValue * (double) num1 > 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.PressRight;
        this.OnPressRight.Invoke();
      }
      else if ((double) this.axisX.axisValue * (double) num1 < 0.0)
      {
        this.axisX.axisState = ETCAxis.AxisState.PressLeft;
        this.OnPressLeft.Invoke();
      }
      else
        this.axisX.axisState = ETCAxis.AxisState.None;
    }
    float num2 = 1f;
    if (this.axisY.invertedAxis)
      num2 = -1f;
    if ((double) this.OldTmpAxis.y == 0.0 && (double) Mathf.Abs(this.axisY.axisValue) > 0.0)
    {
      if ((double) this.axisY.axisValue * (double) num2 > 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.DownUp;
        this.OnDownUp.Invoke();
      }
      else if ((double) this.axisY.axisValue * (double) num2 < 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.DownDown;
        this.OnDownDown.Invoke();
      }
      else
        this.axisY.axisState = ETCAxis.AxisState.None;
    }
    else
    {
      if (this.axisY.axisState == ETCAxis.AxisState.None)
        return;
      if ((double) this.axisY.axisValue * (double) num2 > 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.PressUp;
        this.OnPressUp.Invoke();
      }
      else if ((double) this.axisY.axisValue * (double) num2 < 0.0)
      {
        this.axisY.axisState = ETCAxis.AxisState.PressDown;
        this.OnPressDown.Invoke();
      }
      else
        this.axisY.axisState = ETCAxis.AxisState.None;
    }
  }

  protected override void SetVisible()
  {
    this.GetComponent<Image>().enabled = this._visible;
  }

  protected override void SetActivated()
  {
    if (this._activated)
      return;
    this.isOnTouch = false;
    this.isOnDrag = false;
    this.tmpAxis = Vector2.zero;
    this.OldTmpAxis = Vector2.zero;
    this.axisX.axisState = ETCAxis.AxisState.None;
    this.axisY.axisState = ETCAxis.AxisState.None;
    if (!this.axisX.isEnertia && !this.axisY.isEnertia)
    {
      this.axisX.ResetAxis();
      this.axisY.ResetAxis();
    }
    this.pointId = -1;
  }

  private void GetTouchDirection(Vector2 position, Camera cam)
  {
    Vector2 localPoint;
    RectTransformUtility.ScreenPointToLocalPointInRectangle(this.cachedRectTransform, position, cam, out localPoint);
    Vector2 vector2 = this.rectTransform().sizeDelta / 3f;
    this.tmpAxis = Vector2.zero;
    if ((double) localPoint.x < -(double) vector2.x / 2.0 && (double) localPoint.y > -(double) vector2.y / 2.0 && ((double) localPoint.y < (double) vector2.y / 2.0 && this.dPadAxisCount == ETCBase.DPadAxis.Two_Axis) || this.dPadAxisCount == ETCBase.DPadAxis.Four_Axis && (double) localPoint.x < -(double) vector2.x / 2.0)
      this.tmpAxis.x = -1f;
    if ((double) localPoint.x > (double) vector2.x / 2.0 && (double) localPoint.y > -(double) vector2.y / 2.0 && ((double) localPoint.y < (double) vector2.y / 2.0 && this.dPadAxisCount == ETCBase.DPadAxis.Two_Axis) || this.dPadAxisCount == ETCBase.DPadAxis.Four_Axis && (double) localPoint.x > (double) vector2.x / 2.0)
      this.tmpAxis.x = 1f;
    if ((double) localPoint.y > (double) vector2.y / 2.0 && (double) localPoint.x > -(double) vector2.x / 2.0 && ((double) localPoint.x < (double) vector2.x / 2.0 && this.dPadAxisCount == ETCBase.DPadAxis.Two_Axis) || this.dPadAxisCount == ETCBase.DPadAxis.Four_Axis && (double) localPoint.y > (double) vector2.y / 2.0)
      this.tmpAxis.y = 1f;
    if (((double) localPoint.y >= -(double) vector2.y / 2.0 || (double) localPoint.x <= -(double) vector2.x / 2.0 || ((double) localPoint.x >= (double) vector2.x / 2.0 || this.dPadAxisCount != ETCBase.DPadAxis.Two_Axis)) && (this.dPadAxisCount != ETCBase.DPadAxis.Four_Axis || (double) localPoint.y >= -(double) vector2.y / 2.0))
      return;
    this.tmpAxis.y = -1f;
  }

  [Serializable]
  public class OnMoveStartHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnMoveHandler : UnityEvent<Vector2>
  {
  }

  [Serializable]
  public class OnMoveSpeedHandler : UnityEvent<Vector2>
  {
  }

  [Serializable]
  public class OnMoveEndHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnTouchStartHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnTouchUPHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownUpHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownDownHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownLeftHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnDownRightHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressUpHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressDownHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressLeftHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressRightHandler : UnityEvent
  {
  }
}
