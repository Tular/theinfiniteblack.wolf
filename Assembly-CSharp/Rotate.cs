﻿// Decompiled with JetBrains decompiler
// Type: Rotate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Rotate : MonoBehaviour
{
  private Vector3 angle;

  private void Start()
  {
    this.angle = this.transform.eulerAngles;
  }

  private void Update()
  {
    this.angle.y += Time.deltaTime * 100f;
    this.transform.eulerAngles = this.angle;
  }
}
