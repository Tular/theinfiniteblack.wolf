﻿// Decompiled with JetBrains decompiler
// Type: TwoTapMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TwoTapMe : MonoBehaviour
{
  private void OnEnable()
  {
    EasyTouch.On_SimpleTap2Fingers += new EasyTouch.SimpleTap2FingersHandler(this.On_SimpleTap2Fingers);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_SimpleTap2Fingers -= new EasyTouch.SimpleTap2FingersHandler(this.On_SimpleTap2Fingers);
  }

  private void On_SimpleTap2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.RandomColor();
  }

  private void RandomColor()
  {
    this.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1f), Random.Range(0.0f, 1f), Random.Range(0.0f, 1f));
  }
}
