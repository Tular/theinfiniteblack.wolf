﻿// Decompiled with JetBrains decompiler
// Type: DoubleTapMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DoubleTapMe : MonoBehaviour
{
  private void OnEnable()
  {
    EasyTouch.On_DoubleTap += new EasyTouch.DoubleTapHandler(this.On_DoubleTap);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_DoubleTap -= new EasyTouch.DoubleTapHandler(this.On_DoubleTap);
  }

  private void On_DoubleTap(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1f), Random.Range(0.0f, 1f), Random.Range(0.0f, 1f));
  }
}
