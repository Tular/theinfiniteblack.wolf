﻿// Decompiled with JetBrains decompiler
// Type: DialogViewBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class DialogViewBase<T> : DialogViewBase where T : DialogContextBase
{
  protected T mDialogContext;

  public void SetDialogContext(T dlgContext)
  {
    this.mDialogContext = dlgContext;
  }

  protected override sealed void TryInitialize()
  {
    if (this.mIsInitialized)
      return;
    this.OnInitialize();
    this.mIsInitialized = true;
    Debug.Log((object) string.Format("DialogView - {0} Initialized", (object) this.name));
  }
}
