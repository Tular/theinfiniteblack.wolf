﻿// Decompiled with JetBrains decompiler
// Type: TouchTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class TouchTester : MonoBehaviour
{
  public UITextList touchInfoOutput;
  public UILabel frameTouchOutput;
  public UILabel scrollViewInfoOutput;
  public UILabel uiCameraTouchInfoOutput;
  public UIScrollView scrollView;
  public CustomScrollViewPrototype prototype;
  private int _dragStartFrame;
  private int _dragEndFrame;
  private StringBuilder _frameOutput;
  private int _frameMoveCount;
  private UICamera.MouseOrTouch _lastTouchData;

  public void DumpTouchData()
  {
    this.touchInfoOutput.Clear();
  }

  private void OnPress(bool pressed)
  {
    this.UpdateUICameraTouchOutput();
    if ((Object) UICamera.selectedObject == (Object) this.gameObject)
      return;
    this.prototype.Press(pressed);
  }

  private void OnDragStart()
  {
    this._dragStartFrame = Time.frameCount;
    this.UpdateUICameraTouchOutput();
    if ((Object) UICamera.selectedObject == (Object) this.gameObject || !(bool) ((Object) this.touchInfoOutput) || !this.touchInfoOutput.enabled)
      return;
    this.touchInfoOutput.Add(string.Format("OnDragStart(): {0} - {1}", (object) Time.frameCount, (object) UICamera.selectedObject.name));
  }

  private void OnDrag(Vector2 delta)
  {
    this.UpdateUICameraTouchOutput();
    if ((Object) UICamera.selectedObject == (Object) this.gameObject)
    {
      this.touchInfoOutput.OnDrag(delta);
    }
    else
    {
      if ((bool) ((Object) this.prototype) && this.prototype.gameObject.activeInHierarchy)
        this.prototype.Drag();
      if (!(bool) ((Object) this.touchInfoOutput) || !this.touchInfoOutput.enabled)
        return;
      this.touchInfoOutput.Add(string.Format("OnDrag({0}): {1}", (object) Time.frameCount, (object) delta));
    }
  }

  private void OnDragEnd()
  {
    this._dragEndFrame = Time.frameCount;
    this.UpdateUICameraTouchOutput();
    if ((Object) UICamera.selectedObject == (Object) this.gameObject || !(bool) ((Object) this.touchInfoOutput) || !this.touchInfoOutput.enabled)
      return;
    this.touchInfoOutput.Add(string.Format("OnDragEnd(): {0}", (object) Time.frameCount));
  }

  public void OnScrollViewDragFinished()
  {
    if (!(bool) ((Object) this.touchInfoOutput) || !this.touchInfoOutput.enabled)
      return;
    this.touchInfoOutput.Add(string.Format("OnSvDragFinished(): {0}", (object) Time.frameCount));
    this.touchInfoOutput.Add(string.Format("OnSvDragFinished(): {0}", (object) this.scrollView.currentMomentum));
  }

  private void Awake()
  {
    UICamera.genericEventHandler = this.gameObject;
    if (!(bool) ((Object) this.scrollView))
      return;
    this.scrollView.onDragFinished = new UIScrollView.OnDragNotification(this.OnScrollViewDragFinished);
  }

  private void UpdateUICameraTouchOutput()
  {
    if (!(bool) ((Object) this.uiCameraTouchInfoOutput) || !this.uiCameraTouchInfoOutput.enabled)
      return;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.AppendLine("UICamera.currentTouch Data");
    stringBuilder.AppendFormat("Current ID: {0}\n", (object) UICamera.currentTouchID);
    if (UICamera.currentTouch != null)
      this._lastTouchData = UICamera.currentTouch;
    if (this._lastTouchData != null)
    {
      stringBuilder.AppendFormat("DragStart Frame: {0}\n", (object) this._dragStartFrame);
      stringBuilder.AppendFormat("DragEnd Frame: {0}\n", (object) this._dragEndFrame);
      stringBuilder.AppendFormat("Click Time: {0}\n", (object) this._lastTouchData.clickTime);
      stringBuilder.AppendFormat("Position: {0}\n", (object) this._lastTouchData.pos);
      stringBuilder.AppendFormat("Last Position: {0}\n", (object) this._lastTouchData.lastPos);
      stringBuilder.AppendFormat("Delta: {0}\n", (object) this._lastTouchData.delta);
      stringBuilder.AppendFormat("TotDelta: {0}\n", (object) this._lastTouchData.totalDelta);
      stringBuilder.AppendFormat("Press Started: {0}\n", (object) this._lastTouchData.pressStarted);
      stringBuilder.AppendFormat("Touch Began: {0}\n", (object) this._lastTouchData.touchBegan);
      stringBuilder.AppendFormat("Drag Started: {0}", (object) this._lastTouchData.dragStarted);
    }
    this.uiCameraTouchInfoOutput.text = stringBuilder.ToString();
  }

  private void UpdateFrameOutput()
  {
    if (Input.touches.Length == 0 || !(bool) ((Object) this.frameTouchOutput) || !this.frameTouchOutput.enabled)
      return;
    UnityEngine.Touch touch = Input.touches[0];
    switch (touch.phase)
    {
      case TouchPhase.Began:
        this._frameOutput = new StringBuilder(100);
        this._frameMoveCount = 0;
        this._frameOutput.AppendFormat("Frame Began: {0}\n", (object) Time.frameCount);
        this._frameOutput.AppendFormat("Begin Position: {0}\n", (object) touch.position);
        this.frameTouchOutput.text = this._frameOutput.ToString();
        break;
      case TouchPhase.Moved:
        ++this._frameMoveCount;
        this.frameTouchOutput.text = this._frameOutput.ToString();
        break;
      case TouchPhase.Ended:
        this._frameOutput.AppendFormat("Frame Ended: {0}\n", (object) Time.frameCount);
        this._frameOutput.AppendFormat("Touch Move Count: {0}\n", (object) this._frameMoveCount);
        this._frameOutput.AppendFormat("End Position: {0}\n", (object) touch.position);
        this.frameTouchOutput.text = this._frameOutput.ToString();
        break;
    }
  }

  private void UpdateScrollViewOutput()
  {
    if (!(bool) ((Object) this.scrollViewInfoOutput) || !this.scrollViewInfoOutput.enabled)
      return;
    StringBuilder stringBuilder = new StringBuilder();
    UIPanel component;
    if ((bool) ((Object) this.scrollView) && this.scrollView.gameObject.activeInHierarchy)
    {
      stringBuilder.AppendLine("ScrollView Data");
      stringBuilder.AppendFormat("Momentum: {0}\n", (object) this.scrollView.currentMomentum);
      stringBuilder.AppendFormat("Is Dragging: {0}\n", (object) this.scrollView.isDragging);
      stringBuilder.AppendLine();
      component = this.scrollView.GetComponent<UIPanel>();
    }
    else
    {
      stringBuilder.AppendLine("Prototype Data");
      stringBuilder.AppendFormat("Bounds: {0}\n", (object) this.prototype.bounds);
      stringBuilder.AppendFormat("Current Pos: {0}\n", (object) this.prototype.currentPos);
      stringBuilder.AppendFormat("Last Pos: {0}\n", (object) this.prototype.lastPos);
      stringBuilder.AppendFormat("Momentum Magnitude: {0}\n", (object) this.prototype.currentMomentum.magnitude);
      stringBuilder.AppendFormat("Momentum-Y: {0}\n", (object) this.prototype.currentMomentum.y);
      stringBuilder.AppendFormat("Momentum-X: {0}\n", (object) this.prototype.currentMomentum.x);
      stringBuilder.AppendFormat("Scroll: {0}\n", (object) this.prototype.scroll);
      stringBuilder.AppendLine();
      component = this.prototype.GetComponent<UIPanel>();
    }
    this.scrollViewInfoOutput.text = stringBuilder.ToString();
  }

  private void Update()
  {
    this.UpdateFrameOutput();
    this.UpdateScrollViewOutput();
  }
}
