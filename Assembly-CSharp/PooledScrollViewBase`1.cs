﻿// Decompiled with JetBrains decompiler
// Type: PooledScrollViewBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using PathologicalGames;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public abstract class PooledScrollViewBase<T> : UIScrollView where T : class, IScrollViewCellInfo
{
  private readonly HashSet<T> _visibleLastFrame = new HashSet<T>();
  public SpawnPool cellPool;
  public Vector2 cellPadding;
  public int topBuffer;
  public int bottomBuffer;
  public int leftBuffer;
  public int rightBuffer;
  private int _lastMoveFrame;
  private IPooledScrollViewCellInifoCollection<T> _data;
  private bool _isInitialized;
  public int lastFrameVisible;

  public bool isMoving
  {
    get
    {
      return this._lastMoveFrame == Time.frameCount;
    }
  }

  public bool reposition { get; set; }

  protected bool mBackingDataChanged { get; private set; }

  public IPooledScrollViewCellInifoCollection<T> data
  {
    get
    {
      return this._data;
    }
    set
    {
      if (value == this._data)
        return;
      this._data = value;
      this.mBackingDataChanged = true;
    }
  }

  public override Bounds bounds
  {
    get
    {
      if (!this.mCalculatedBounds)
      {
        this.mCalculatedBounds = true;
        this.mTrans = this.transform;
        this.mBounds = this.CalculateBounds();
      }
      return this.mBounds;
    }
  }

  public Bounds bufferBounds
  {
    get
    {
      Vector2 viewSize = this.panel.GetViewSize();
      bool flag = this.panel.clipping != UIDrawCall.Clipping.None;
      Vector4 finalClipRegion = this.panel.finalClipRegion;
      Vector3 center = new Vector3(finalClipRegion.x, finalClipRegion.y);
      if (!flag)
        return new Bounds(center, new Vector3(viewSize.x, viewSize.y));
      float num1 = finalClipRegion.w / 2f;
      float num2 = finalClipRegion.z / 2f;
      Vector3 max = new Vector3(finalClipRegion.x + num2 + (float) this.rightBuffer, finalClipRegion.y + num1 + (float) this.topBuffer);
      Vector3 min = new Vector3(finalClipRegion.x - num2 - (float) this.leftBuffer, finalClipRegion.y - num1 - (float) this.bottomBuffer);
      Bounds bounds = new Bounds();
      bounds.SetMinMax(min, max);
      return bounds;
    }
  }

  public void AddCell(T cellInfo)
  {
    this.Init();
    if (this.data == null)
      return;
    this.data.Add(cellInfo);
  }

  protected virtual void UpdateCellPositions()
  {
    if (this.data == null || this.data.values == null)
      return;
    Vector3 zero = Vector3.zero;
    foreach (T obj in this.data.values)
    {
      switch (this.contentPivot)
      {
        case UIWidget.Pivot.Top:
          obj.localPosition = zero - new Vector3(-this.cellPadding.x, obj.extents.y);
          zero -= new Vector3(0.0f, 2f * obj.extents.y + this.cellPadding.y);
          continue;
        case UIWidget.Pivot.Bottom:
          obj.localPosition = zero + new Vector3(this.cellPadding.x, obj.extents.y);
          zero += new Vector3(0.0f, 2f * obj.extents.y + this.cellPadding.y);
          continue;
        default:
          continue;
      }
    }
  }

  private void Init()
  {
    if (this._isInitialized)
      return;
    this.OnInit();
    this._isInitialized = true;
  }

  protected virtual void OnInit()
  {
  }

  protected void UpdateVisibleCellData()
  {
    if (!Application.isPlaying || this.data == null || this.data.values == null)
      return;
    Bounds bufferBounds = this.bufferBounds;
    IEnumerable<T> values = this.data.values;
    HashSet<T> objSet = new HashSet<T>((IEnumerable<T>) this._visibleLastFrame);
    this.lastFrameVisible = this._visibleLastFrame.Count;
    foreach (T obj1 in values)
    {
      T obj2 = obj1;
      bool flag = obj2.IsContainedIn(bufferBounds);
      if (objSet.Contains(obj1))
        objSet.Remove(obj1);
      if (!flag && obj2.isBound)
      {
        if (this._visibleLastFrame.Contains(obj1))
          this._visibleLastFrame.Remove(obj1);
        this.cellPool.Despawn(obj2.cellTransform, this.cellPool.transform);
        obj2.Unbind();
      }
      else if (flag && !obj2.isBound)
      {
        Transform cellTrans = this.cellPool.Spawn(obj2.cellPrefab, this.transform);
        obj2.BindCell(cellTrans);
        this._visibleLastFrame.Add(obj2);
      }
    }
    using (HashSet<T>.Enumerator enumerator = objSet.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        T current = enumerator.Current;
        if (this._visibleLastFrame.Contains(current))
        {
          if (current.isBound)
          {
            this.cellPool.Despawn(current.cellTransform);
            current.Unbind();
          }
          this._visibleLastFrame.Remove(current);
        }
      }
    }
  }

  protected virtual void OnMove(UIPanel p)
  {
    if (this.mBackingDataChanged)
      return;
    this.reposition = true;
    this._lastMoveFrame = Time.frameCount;
  }

  protected override void Start()
  {
    base.Start();
    this.Init();
    this.panel.onClipMove = new UIPanel.OnClippingMoved(this.OnMove);
    this.ResetPosition();
  }

  protected override void OnEnable()
  {
    base.OnEnable();
    if (!Application.isPlaying)
      return;
    this.Init();
    this.StartCoroutine(this.RepositionCoroutine());
    this.reposition = true;
  }

  private Bounds CalculateBounds()
  {
    Vector4 finalClipRegion = this.panel.finalClipRegion;
    if (!Application.isPlaying || this.data == null || this.data.Count < 1)
      return new Bounds(Vector3.zero, new Vector3(finalClipRegion.z, 20f));
    T first = this.data.first;
    T last = this.data.last;
    Bounds bounds = new Bounds();
    switch (this.contentPivot)
    {
      case UIWidget.Pivot.TopLeft:
      case UIWidget.Pivot.Top:
      case UIWidget.Pivot.TopRight:
        Vector3 max1 = first.localPosition + first.extents;
        Vector3 min1 = last.localPosition - last.extents;
        bounds.SetMinMax(min1, max1);
        break;
      case UIWidget.Pivot.BottomLeft:
      case UIWidget.Pivot.Bottom:
      case UIWidget.Pivot.BottomRight:
        Vector3 max2 = last.localPosition + last.extents;
        Vector3 min2 = first.localPosition - first.extents;
        bounds.SetMinMax(min2, max2);
        break;
    }
    return bounds;
  }

  protected void ResetVisibleInfos()
  {
    using (HashSet<T>.Enumerator enumerator = this._visibleLastFrame.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Unbind();
    }
    this.cellPool.DespawnAll();
    this._visibleLastFrame.Clear();
  }

  protected virtual void OnUpdateCellPositions()
  {
    this.UpdateCellPositions();
    this.InvalidateBounds();
    this.UpdateVisibleCellData();
    if (!this.shouldMoveVertically)
      this.ResetPosition();
    else
      this.UpdatePosition();
  }

  protected virtual void OnResetCellPositions()
  {
    this.ResetVisibleInfos();
    this.ResetPosition();
    this.UpdateCellPositions();
    this.InvalidateBounds();
    this.UpdateVisibleCellData();
    this.ResetPosition();
  }

  [DebuggerHidden]
  private IEnumerator RepositionCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new PooledScrollViewBase<T>.\u003CRepositionCoroutine\u003Ec__Iterator11()
    {
      \u003C\u003Ef__this = this
    };
  }
}
