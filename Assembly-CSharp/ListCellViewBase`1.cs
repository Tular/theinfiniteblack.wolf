﻿// Decompiled with JetBrains decompiler
// Type: ListCellViewBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class ListCellViewBase<T> : MonoBehaviour where T : class, new()
{
  public GameObject deleteView;
  public GameObject contentView;
  [HideInInspector]
  public List<EventDelegate> onGotFocus;
  [HideInInspector]
  public List<EventDelegate> onLostFocus;
  public List<GameObject> activateOnGotFocus;
  public List<GameObject> activateOnLostFocus;
  protected BoundListControllerBase<T> mController;
  private bool _hasFocus;
  private bool _isDirty;

  public virtual T data { get; protected set; }

  public virtual void Initialize(T obj, BoundListControllerBase<T> controller)
  {
    this.data = obj;
    this.mController = controller;
    this.OnInitialize();
  }

  public ListCellViewBase<T>.CellViewMode mode { get; protected set; }

  public bool hasFocus
  {
    get
    {
      return this._hasFocus;
    }
    set
    {
      if (this._hasFocus == value)
        return;
      this._hasFocus = value;
      this.OnFocusChanged();
    }
  }

  public virtual bool isDirty
  {
    get
    {
      return this._isDirty;
    }
  }

  public virtual void SetDirty()
  {
    this._isDirty = true;
  }

  protected virtual void OnClearDirty()
  {
    this._isDirty = false;
  }

  public virtual void OnFocusChanged()
  {
    if (this._hasFocus)
      this.OnGotFocus();
    else
      this.OnLostFocus();
  }

  public virtual void OnGotFocus()
  {
    Debug.Log((object) string.Format("ListCellViewBase.OnGotFocus"));
    if ((bool) ((UnityEngine.Object) this.GetComponent<Collider>()))
      this.GetComponent<Collider>().enabled = false;
    this.SetFocus(true);
    if (!EventDelegate.IsValid(this.onGotFocus))
      return;
    EventDelegate.Execute(this.onGotFocus);
  }

  public virtual void OnLostFocus()
  {
    Debug.Log((object) string.Format("ListCellViewBase.OnLostFocus"));
    if ((UnityEngine.Object) this.GetComponent<Collider>() != (UnityEngine.Object) null)
      this.GetComponent<Collider>().enabled = true;
    this.SetFocus(false);
    if (!EventDelegate.IsValid(this.onLostFocus))
      return;
    EventDelegate.Execute(this.onLostFocus);
  }

  public virtual void Delete()
  {
    T data = this.data;
    if ((UnityEngine.Object) this.mController == (UnityEngine.Object) null)
    {
      Debug.Log((object) string.Format("ListCellViewBase<T, TD>.Delete(): mController is null"));
      throw new NullReferenceException("mController is null");
    }
    this.mController.Remove(this.gameObject, data);
  }

  public virtual void OnClick()
  {
    Debug.Log((object) string.Format("ListCellViewBase.OnClick"));
    if (this.hasFocus || !((UnityEngine.Object) this.mController != (UnityEngine.Object) null))
      return;
    this.mController.SetFocused(this);
  }

  public void ShowContentView()
  {
    Debug.Log((object) string.Format("ListCellViewBase.ShowContentView"));
    this.SetMode(ListCellViewBase<T>.CellViewMode.Content);
  }

  public void ShowDeleteView()
  {
    Debug.Log((object) string.Format("ListCellViewBase.ShowDeleteView"));
    this.SetMode(ListCellViewBase<T>.CellViewMode.Delete);
  }

  public void SetMode(ListCellViewBase<T>.CellViewMode cellMode)
  {
    switch (cellMode)
    {
      case ListCellViewBase<T>.CellViewMode.Content:
        NGUITools.SetActiveSelf(this.deleteView, false);
        NGUITools.SetActiveSelf(this.contentView, true);
        break;
      case ListCellViewBase<T>.CellViewMode.Delete:
        NGUITools.SetActiveSelf(this.contentView, false);
        NGUITools.SetActiveSelf(this.deleteView, true);
        break;
    }
    this.mode = cellMode;
  }

  public void SetFocus(bool focus)
  {
    this._hasFocus = focus;
    using (List<GameObject>.Enumerator enumerator = this.activateOnGotFocus.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetActive(focus);
    }
    using (List<GameObject>.Enumerator enumerator = this.activateOnLostFocus.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetActive(!focus);
    }
  }

  protected abstract void OnInitialize();

  public enum CellViewMode
  {
    Content,
    Delete,
  }
}
