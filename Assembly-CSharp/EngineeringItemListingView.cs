﻿// Decompiled with JetBrains decompiler
// Type: EngineeringItemListingView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class EngineeringItemListingView : MonoBehaviour
{
  public EngineeringItemScrollView itemScrollView;
  private System.Action _listRefreshAction;

  public EngineeringItemListingViewContext viewContext { get; private set; }

  public void ShowEngines()
  {
    this.viewContext.itemType = ItemType.ENGINE;
    this.itemScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowEngines);
  }

  public void ShowWeapons()
  {
    this.viewContext.itemType = ItemType.WEAPON;
    this.itemScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowWeapons);
  }

  public void ShowComputers()
  {
    this.viewContext.itemType = ItemType.COMPUTER;
    this.itemScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowComputers);
  }

  public void ShowArmor()
  {
    this.viewContext.itemType = ItemType.ARMOR;
    this.itemScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowArmor);
  }

  public void ShowHarvesters()
  {
    this.viewContext.itemType = ItemType.HARVESTER;
    this.itemScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowHarvesters);
  }

  public void ShowStorage()
  {
    this.viewContext.itemType = ItemType.STORAGE;
    this.itemScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowStorage);
  }

  public void ShowSpecials()
  {
    this.viewContext.itemType = ItemType.SPECIAL;
    this.itemScrollView.RefreshView();
    this._listRefreshAction = new System.Action(this.ShowSpecials);
  }

  private void Start()
  {
    ItemEngineeringDialog inParents = NGUITools.FindInParents<ItemEngineeringDialog>(this.gameObject);
    if (!(bool) ((UnityEngine.Object) inParents))
      this.LogD(string.Format("EngineeringItemListingView - could not find parent dialog"));
    else if (inParents.dialogContext == null)
      this.LogD(string.Format("EngineeringItemListingView - dialog context is null"));
    this.viewContext = new EngineeringItemListingViewContext(inParents.dialogContext);
    this.itemScrollView.context = this.viewContext;
  }

  private void Update()
  {
    if (!this.viewContext.hasChanged || this._listRefreshAction == null)
      return;
    this._listRefreshAction();
  }
}
