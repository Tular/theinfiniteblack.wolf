﻿// Decompiled with JetBrains decompiler
// Type: DataBindingBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class DataBindingBase<T> : MonoBehaviour where T : class
{
  protected T mData;

  public T data
  {
    get
    {
      return this.mData;
    }
  }

  public bool isDataBound { get; private set; }

  public bool isDirty { get; private set; }

  public virtual void SetDirty()
  {
    this.isDirty = true;
  }

  public void Bind(T obj)
  {
    if (!this.enabled)
      return;
    this.RemoveDelegates();
    this.mData = obj;
    this.BindData();
    this.isDataBound = true;
    this.AddDelegates();
  }

  public void UpdateDataSource()
  {
    if (this.isDataBound)
      this.DoDataSourceUpdate();
    this.isDirty = false;
  }

  protected virtual void DoDataSourceUpdate()
  {
  }

  public void UnBind()
  {
    if (!this.enabled)
      return;
    this.isDataBound = false;
    this.UnBindData();
    this.mData = (T) null;
  }

  protected abstract void BindData();

  protected abstract void UnBindData();

  protected abstract void AddDelegates();

  protected abstract void RemoveDelegates();

  protected virtual void OnDisable()
  {
    this.RemoveDelegates();
    this.mData = (T) null;
  }

  protected virtual void Update()
  {
    if (!this.isDirty)
      return;
    this.UpdateDataSource();
  }
}
