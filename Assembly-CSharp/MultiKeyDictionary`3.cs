﻿// Decompiled with JetBrains decompiler
// Type: MultiKeyDictionary`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class MultiKeyDictionary<T1, T2, T3> : Dictionary<T1, Dictionary<T2, T3>>
{
  public new Dictionary<T2, T3> this[T1 key]
  {
    get
    {
      if (!this.ContainsKey(key))
        this.Add(key, new Dictionary<T2, T3>());
      Dictionary<T2, T3> dictionary;
      this.TryGetValue(key, out dictionary);
      return dictionary;
    }
  }

  public bool ContainsKey(T1 key1, T2 key2)
  {
    Dictionary<T2, T3> dictionary;
    this.TryGetValue(key1, out dictionary);
    if (dictionary == null)
      return false;
    return dictionary.ContainsKey(key2);
  }
}
