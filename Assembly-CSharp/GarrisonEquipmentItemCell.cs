﻿// Decompiled with JetBrains decompiler
// Type: GarrisonEquipmentItemCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using UnityEngine;

public class GarrisonEquipmentItemCell : GeneralEquipmentItemCell
{
  public GameObject takeItem;
  public GameObject bankItem;

  public void ItemToInventory()
  {
    TibProxy.gameState.DoItemFromGarrison(this.item);
  }

  public void ItemToGarrisonBank()
  {
    TibProxy.gameState.DoItemToGarrison(this.item);
  }

  protected override void Update()
  {
    base.Update();
    if ((Object) this.scrollView == (Object) null || (Object) this.takeItem == (Object) null || (Object) this.bankItem == (Object) null)
      return;
    switch (this.scrollView.itemActionContext)
    {
      case EquipmentItemActionContext.InventorySelf:
        if (this.takeItem.activeSelf)
          this.takeItem.SetActive(false);
        if (this.bankItem.activeSelf)
          break;
        this.bankItem.SetActive(true);
        break;
      case EquipmentItemActionContext.GarrisonBank:
        if (this.bankItem.activeSelf)
          this.bankItem.SetActive(false);
        if (this.takeItem.activeSelf)
          break;
        this.takeItem.SetActive(true);
        break;
    }
  }
}
