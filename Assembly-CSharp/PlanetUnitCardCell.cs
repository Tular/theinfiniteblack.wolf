﻿// Decompiled with JetBrains decompiler
// Type: PlanetUnitCardCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;

public class PlanetUnitCardCell : CombatUnitCardCell
{
  public string planetSpritePrefix = "entity_planet_";
  public UISprite planetSprite;
  private PlanetClass _planetClass;

  public PlanetClass planetClass
  {
    get
    {
      return this._planetClass;
    }
    set
    {
      this._planetClass = value;
      this.planetSprite.spriteName = string.Format("{0}{1}", (object) this.planetSpritePrefix, (object) this._planetClass);
    }
  }

  public override void ResetVisuals()
  {
    base.ResetVisuals();
    this.planetSprite.spriteName = string.Empty;
  }
}
