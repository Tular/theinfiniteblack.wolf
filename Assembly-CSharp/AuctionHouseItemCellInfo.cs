﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseItemCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using UnityEngine;

public class AuctionHouseItemCellInfo : IScrollViewCellInfo
{
  private Vector3 _localPosition;
  private readonly ClientAuction _auction;
  private AuctionHouseItemCell _cellInstance;
  private int _cellHeight;
  private int _cellWidth;
  private string _titleText;
  private string _subtitleText;
  private string _descriptionText;
  private string _itemProsText;
  private string _itemConsText;
  private string _equipPointsText;
  private string _durabilityText;
  private string _auctionDetailsText;
  private int _auctionLastChangedId;
  private string _prevOwner;
  private int _prevNextBid;
  private int _prevDays;
  private int _prevHours;
  private int _prevMinutes;

  public AuctionHouseItemCellInfo(Transform prefab, ClientAuction auction)
  {
    this.cellPrefab = prefab;
    this._auction = auction;
    this._auctionLastChangedId = int.MinValue;
    this.UpdateAuctionDetails();
    StringBuilder titleSb;
    StringBuilder subtitleSb;
    StringBuilder descriptionSb;
    StringBuilder prosSb;
    StringBuilder consSb;
    this.extents = prefab.GetComponent<AuctionHouseItemCell>().CalculateExtents(this._auction, out titleSb, out subtitleSb, out descriptionSb, out prosSb, out consSb, this._auctionDetailsText);
    Markup.GetNGUI(titleSb);
    subtitleSb.Append("[-]");
    Markup.GetNGUI(subtitleSb);
    descriptionSb.Insert(0, "[d1f1ff]");
    descriptionSb.Append("[-]");
    descriptionSb.Insert(0, "[d1f1ff]");
    descriptionSb.Append("[-]");
    prosSb.Insert(0, "[00ff00]");
    prosSb.Append("[-]");
    consSb.Insert(0, "[ff4400]");
    consSb.Append("[-]");
    this._durabilityText = ((int) this._auction.Item.Durability).ToString() + "%";
    this._equipPointsText = this._auction.Item.EPCost.ToString();
    this._cellWidth = Mathf.RoundToInt(this.extents.x * 2f);
    this._cellHeight = Mathf.RoundToInt(this.extents.y * 2f);
    this._titleText = titleSb.ToString();
    this._subtitleText = subtitleSb.ToString();
    this._descriptionText = descriptionSb.ToString();
    this._itemProsText = prosSb.ToString();
    this._itemConsText = consSb.ToString();
  }

  public Vector3 localPosition
  {
    get
    {
      return this._localPosition;
    }
    set
    {
      this._localPosition = value;
      if (!((UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null))
        return;
      this._cellInstance.transform.localPosition = this._localPosition;
    }
  }

  public Vector3 extents { get; private set; }

  public object cellData
  {
    get
    {
      return (object) this._auction;
    }
  }

  public System.Type cellType
  {
    get
    {
      return typeof (AuctionHouseItemCell);
    }
  }

  public bool isBound
  {
    get
    {
      return (UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null;
    }
  }

  public Transform cellPrefab { get; private set; }

  public Transform cellTransform
  {
    get
    {
      if ((UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null)
        return this._cellInstance.transform;
      return (Transform) null;
    }
  }

  public ClientAuction auction
  {
    get
    {
      return this._auction;
    }
  }

  private void UpdateAuctionDetails()
  {
    TimeSpan timeSpan = TimeSpan.FromMilliseconds(Convert.ToDouble(this.auction.Time.RemainingMS));
    if (this._prevOwner == this.auction.Owner.Name && this._prevNextBid == this.auction.NextBid && (this._prevDays == timeSpan.Days && this._prevHours == timeSpan.Hours) && this._prevMinutes == timeSpan.Minutes)
      return;
    this._prevOwner = this.auction.Owner.Name;
    this._prevNextBid = this.auction.NextBid;
    this._prevDays = timeSpan.Days;
    this._prevHours = timeSpan.Hours;
    this._prevMinutes = timeSpan.Minutes;
    StringBuilder stringBuilder = new StringBuilder(500);
    if (this._auction.Owner.Name.Equals("Unknown?"))
      stringBuilder.AppendFormat("Seller: -=fetching info=-\n");
    else
      stringBuilder.AppendFormat("Seller: {0}\n", (object) this.auction.Owner.Name);
    if (this.auction.BuyoutCredits == 0 && this.auction.BuyoutBlackDollars == 0)
    {
      stringBuilder.AppendLine("-= No Buyout =-");
    }
    else
    {
      if (this.auction.BuyoutCredits > 0)
        stringBuilder.AppendFormat("Credit Buyout: {0:$###,###,###}\n", (object) this.auction.BuyoutCredits);
      if (this.auction.BuyoutBlackDollars > 0)
        stringBuilder.AppendFormat("BlackDollar Buyout: {0:###,###,###}\n", (object) this.auction.BuyoutBlackDollars);
    }
    stringBuilder.AppendFormat("Next Bid: {0:$###,###,###}\n", (object) this.auction.NextBid);
    if (timeSpan.Days > 0)
      stringBuilder.AppendFormat("{0}d {1}h {2}m Remaining", (object) timeSpan.Days, (object) timeSpan.Hours, (object) timeSpan.Minutes);
    else if (timeSpan.Hours > 0)
      stringBuilder.AppendFormat("{0}h {1}m Remaining", (object) timeSpan.Hours, (object) timeSpan.Minutes);
    else
      stringBuilder.AppendFormat("{0}m Remaining", (object) timeSpan.Minutes);
    this._auctionDetailsText = stringBuilder.ToString();
    if (!(bool) ((UnityEngine.Object) this._cellInstance))
      return;
    this._cellInstance.auctionDetails.text = this._auctionDetailsText;
  }

  public bool IsContainedIn(Bounds bounds)
  {
    if (!bounds.Contains(this.localPosition + this.extents))
      return bounds.Contains(this.localPosition - this.extents);
    return true;
  }

  public void BindCell(Transform cellTrans)
  {
    this._cellInstance = cellTrans.GetComponent<AuctionHouseItemCell>();
    this._cellInstance.transform.localPosition = this.localPosition;
    this._cellInstance.height = this._cellHeight;
    this._cellInstance.width = this._cellWidth;
    this._cellInstance.auction = this._auction;
    this._cellInstance.onUpdateCallback = new EventDelegate.Callback(this.UpdateAuctionDetails);
    if ((bool) ((UnityEngine.Object) this._cellInstance.title))
      this._cellInstance.title.text = this._titleText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.subTitle))
      this._cellInstance.subTitle.text = this._subtitleText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.description))
      this._cellInstance.description.text = this._descriptionText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.itemPros))
      this._cellInstance.itemPros.text = this._itemProsText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.itemCons))
      this._cellInstance.itemCons.text = this._itemConsText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.equipPoints))
      this._cellInstance.equipPoints.text = this._equipPointsText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.durability))
      this._cellInstance.durability.text = this._durabilityText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.auctionDetails))
      this._cellInstance.auctionDetails.text = this._auctionDetailsText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.watchToggle))
      this._cellInstance.watchToggle.value = this._auction.IsWatched;
    this.UpdateAuctionDetails();
  }

  public void Unbind()
  {
    this._cellInstance.ResetVisuals();
    this._cellInstance.auction = (ClientAuction) null;
    this._cellInstance.onUpdateCallback = (EventDelegate.Callback) null;
    this._cellInstance = (AuctionHouseItemCell) null;
    this._prevNextBid = int.MinValue;
    this._prevDays = int.MinValue;
    this._prevHours = int.MinValue;
    this._prevMinutes = int.MinValue;
  }
}
