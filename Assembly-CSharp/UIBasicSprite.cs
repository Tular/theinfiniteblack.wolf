﻿// Decompiled with JetBrains decompiler
// Type: UIBasicSprite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Diagnostics;
using UnityEngine;

public abstract class UIBasicSprite : UIWidget
{
  protected static Vector2[] mTempPos = new Vector2[4];
  protected static Vector2[] mTempUVs = new Vector2[4];
  [HideInInspector]
  [SerializeField]
  protected UIBasicSprite.FillDirection mFillDirection = UIBasicSprite.FillDirection.Radial360;
  [SerializeField]
  [HideInInspector]
  [Range(0.0f, 1f)]
  protected float mFillAmount = 1f;
  [SerializeField]
  [HideInInspector]
  protected Color mGradientTop = Color.white;
  [HideInInspector]
  [SerializeField]
  protected Color mGradientBottom = new Color(0.7f, 0.7f, 0.7f);
  [NonSerialized]
  private Rect mInnerUV = new Rect();
  [NonSerialized]
  private Rect mOuterUV = new Rect();
  public UIBasicSprite.AdvancedType centerType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType leftType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType rightType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType bottomType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType topType = UIBasicSprite.AdvancedType.Sliced;
  [HideInInspector]
  [SerializeField]
  protected UIBasicSprite.Type mType;
  [HideInInspector]
  [SerializeField]
  protected bool mInvert;
  [HideInInspector]
  [SerializeField]
  protected UIBasicSprite.Flip mFlip;
  [HideInInspector]
  [SerializeField]
  protected bool mApplyGradient;

  public virtual UIBasicSprite.Type type
  {
    get
    {
      return this.mType;
    }
    set
    {
      if (this.mType == value)
        return;
      this.mType = value;
      this.MarkAsChanged();
    }
  }

  public UIBasicSprite.Flip flip
  {
    get
    {
      return this.mFlip;
    }
    set
    {
      if (this.mFlip == value)
        return;
      this.mFlip = value;
      this.MarkAsChanged();
    }
  }

  public UIBasicSprite.FillDirection fillDirection
  {
    get
    {
      return this.mFillDirection;
    }
    set
    {
      if (this.mFillDirection == value)
        return;
      this.mFillDirection = value;
      this.mChanged = true;
    }
  }

  public float fillAmount
  {
    get
    {
      return this.mFillAmount;
    }
    set
    {
      float num = Mathf.Clamp01(value);
      if ((double) this.mFillAmount == (double) num)
        return;
      this.mFillAmount = num;
      this.mChanged = true;
    }
  }

  public override int minWidth
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minWidth;
      Vector4 vector4 = this.border * this.pixelSize;
      int num = Mathf.RoundToInt(vector4.x + vector4.z);
      return Mathf.Max(base.minWidth, (num & 1) != 1 ? num : num + 1);
    }
  }

  public override int minHeight
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minHeight;
      Vector4 vector4 = this.border * this.pixelSize;
      int num = Mathf.RoundToInt(vector4.y + vector4.w);
      return Mathf.Max(base.minHeight, (num & 1) != 1 ? num : num + 1);
    }
  }

  public bool invert
  {
    get
    {
      return this.mInvert;
    }
    set
    {
      if (this.mInvert == value)
        return;
      this.mInvert = value;
      this.mChanged = true;
    }
  }

  public bool hasBorder
  {
    get
    {
      Vector4 border = this.border;
      if ((double) border.x == 0.0 && (double) border.y == 0.0 && (double) border.z == 0.0)
        return (double) border.w != 0.0;
      return true;
    }
  }

  public virtual bool premultipliedAlpha
  {
    get
    {
      return false;
    }
  }

  public virtual float pixelSize
  {
    get
    {
      return 1f;
    }
  }

  private Vector4 drawingUVs
  {
    get
    {
      switch (this.mFlip)
      {
        case UIBasicSprite.Flip.Horizontally:
          return new Vector4(this.mOuterUV.xMax, this.mOuterUV.yMin, this.mOuterUV.xMin, this.mOuterUV.yMax);
        case UIBasicSprite.Flip.Vertically:
          return new Vector4(this.mOuterUV.xMin, this.mOuterUV.yMax, this.mOuterUV.xMax, this.mOuterUV.yMin);
        case UIBasicSprite.Flip.Both:
          return new Vector4(this.mOuterUV.xMax, this.mOuterUV.yMax, this.mOuterUV.xMin, this.mOuterUV.yMin);
        default:
          return new Vector4(this.mOuterUV.xMin, this.mOuterUV.yMin, this.mOuterUV.xMax, this.mOuterUV.yMax);
      }
    }
  }

  protected Color drawingColor
  {
    get
    {
      Color c = this.color;
      c.a = this.finalAlpha;
      if (this.premultipliedAlpha)
        c = NGUITools.ApplyPMA(c);
      return c;
    }
  }

  protected void Fill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color> cols, Rect outer, Rect inner)
  {
    this.mOuterUV = outer;
    this.mInnerUV = inner;
    switch (this.type)
    {
      case UIBasicSprite.Type.Simple:
        this.SimpleFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Sliced:
        this.SlicedFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Tiled:
        this.TiledFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Filled:
        this.FilledFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Advanced:
        this.AdvancedFill(verts, uvs, cols);
        break;
    }
  }

  private void SimpleFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color> cols)
  {
    Vector4 drawingDimensions = this.drawingDimensions;
    Vector4 drawingUvs = this.drawingUVs;
    Color drawingColor = this.drawingColor;
    verts.Add(new Vector3(drawingDimensions.x, drawingDimensions.y));
    verts.Add(new Vector3(drawingDimensions.x, drawingDimensions.w));
    verts.Add(new Vector3(drawingDimensions.z, drawingDimensions.w));
    verts.Add(new Vector3(drawingDimensions.z, drawingDimensions.y));
    uvs.Add(new Vector2(drawingUvs.x, drawingUvs.y));
    uvs.Add(new Vector2(drawingUvs.x, drawingUvs.w));
    uvs.Add(new Vector2(drawingUvs.z, drawingUvs.w));
    uvs.Add(new Vector2(drawingUvs.z, drawingUvs.y));
    if (!this.mApplyGradient)
    {
      cols.Add(drawingColor);
      cols.Add(drawingColor);
      cols.Add(drawingColor);
      cols.Add(drawingColor);
    }
    else
    {
      this.AddVertexColours(cols, ref drawingColor, 1, 1);
      this.AddVertexColours(cols, ref drawingColor, 1, 2);
      this.AddVertexColours(cols, ref drawingColor, 2, 2);
      this.AddVertexColours(cols, ref drawingColor, 2, 1);
    }
  }

  private void SlicedFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color> cols)
  {
    Vector4 vector4 = this.border * this.pixelSize;
    if ((double) vector4.x == 0.0 && (double) vector4.y == 0.0 && ((double) vector4.z == 0.0 && (double) vector4.w == 0.0))
    {
      this.SimpleFill(verts, uvs, cols);
    }
    else
    {
      Color drawingColor = this.drawingColor;
      Vector4 drawingDimensions = this.drawingDimensions;
      UIBasicSprite.mTempPos[0].x = drawingDimensions.x;
      UIBasicSprite.mTempPos[0].y = drawingDimensions.y;
      UIBasicSprite.mTempPos[3].x = drawingDimensions.z;
      UIBasicSprite.mTempPos[3].y = drawingDimensions.w;
      if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.z;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.x;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.x;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.z;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMax;
      }
      if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.w;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.y;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.y;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.w;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMax;
      }
      for (int x1 = 0; x1 < 3; ++x1)
      {
        int x2 = x1 + 1;
        for (int y1 = 0; y1 < 3; ++y1)
        {
          if (this.centerType != UIBasicSprite.AdvancedType.Invisible || x1 != 1 || y1 != 1)
          {
            int y2 = y1 + 1;
            verts.Add(new Vector3(UIBasicSprite.mTempPos[x1].x, UIBasicSprite.mTempPos[y1].y));
            verts.Add(new Vector3(UIBasicSprite.mTempPos[x1].x, UIBasicSprite.mTempPos[y2].y));
            verts.Add(new Vector3(UIBasicSprite.mTempPos[x2].x, UIBasicSprite.mTempPos[y2].y));
            verts.Add(new Vector3(UIBasicSprite.mTempPos[x2].x, UIBasicSprite.mTempPos[y1].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[x1].x, UIBasicSprite.mTempUVs[y1].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[x1].x, UIBasicSprite.mTempUVs[y2].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[x2].x, UIBasicSprite.mTempUVs[y2].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[x2].x, UIBasicSprite.mTempUVs[y1].y));
            if (!this.mApplyGradient)
            {
              cols.Add(drawingColor);
              cols.Add(drawingColor);
              cols.Add(drawingColor);
              cols.Add(drawingColor);
            }
            else
            {
              this.AddVertexColours(cols, ref drawingColor, x1, y1);
              this.AddVertexColours(cols, ref drawingColor, x1, y2);
              this.AddVertexColours(cols, ref drawingColor, x2, y2);
              this.AddVertexColours(cols, ref drawingColor, x2, y1);
            }
          }
        }
      }
    }
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  private void AddVertexColours(BetterList<Color> cols, ref Color color, int x, int y)
  {
    if (y == 0 || y == 1)
    {
      cols.Add(color * this.mGradientBottom);
    }
    else
    {
      if (y != 2 && y != 3)
        return;
      cols.Add(color * this.mGradientTop);
    }
  }

  private void TiledFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Vector2 vector2 = new Vector2(this.mInnerUV.width * (float) mainTexture.width, this.mInnerUV.height * (float) mainTexture.height) * this.pixelSize;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || (double) vector2.x < 2.0 || (double) vector2.y < 2.0)
      return;
    Color drawingColor = this.drawingColor;
    Vector4 drawingDimensions = this.drawingDimensions;
    Vector4 vector4;
    if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
    {
      vector4.x = this.mInnerUV.xMax;
      vector4.z = this.mInnerUV.xMin;
    }
    else
    {
      vector4.x = this.mInnerUV.xMin;
      vector4.z = this.mInnerUV.xMax;
    }
    if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
    {
      vector4.y = this.mInnerUV.yMax;
      vector4.w = this.mInnerUV.yMin;
    }
    else
    {
      vector4.y = this.mInnerUV.yMin;
      vector4.w = this.mInnerUV.yMax;
    }
    float x1 = drawingDimensions.x;
    float y1 = drawingDimensions.y;
    float x2 = vector4.x;
    float y2 = vector4.y;
    while ((double) y1 < (double) drawingDimensions.w)
    {
      float x3 = drawingDimensions.x;
      float y3 = y1 + vector2.y;
      float y4 = vector4.w;
      if ((double) y3 > (double) drawingDimensions.w)
      {
        y4 = Mathf.Lerp(vector4.y, vector4.w, (drawingDimensions.w - y1) / vector2.y);
        y3 = drawingDimensions.w;
      }
      while ((double) x3 < (double) drawingDimensions.z)
      {
        float x4 = x3 + vector2.x;
        float x5 = vector4.z;
        if ((double) x4 > (double) drawingDimensions.z)
        {
          x5 = Mathf.Lerp(vector4.x, vector4.z, (drawingDimensions.z - x3) / vector2.x);
          x4 = drawingDimensions.z;
        }
        verts.Add(new Vector3(x3, y1));
        verts.Add(new Vector3(x3, y3));
        verts.Add(new Vector3(x4, y3));
        verts.Add(new Vector3(x4, y1));
        uvs.Add(new Vector2(x2, y2));
        uvs.Add(new Vector2(x2, y4));
        uvs.Add(new Vector2(x5, y4));
        uvs.Add(new Vector2(x5, y2));
        cols.Add(drawingColor);
        cols.Add(drawingColor);
        cols.Add(drawingColor);
        cols.Add(drawingColor);
        x3 += vector2.x;
      }
      y1 += vector2.y;
    }
  }

  private void FilledFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color> cols)
  {
    if ((double) this.mFillAmount < 1.0 / 1000.0)
      return;
    Vector4 drawingDimensions = this.drawingDimensions;
    Vector4 drawingUvs = this.drawingUVs;
    Color drawingColor = this.drawingColor;
    if (this.mFillDirection == UIBasicSprite.FillDirection.Horizontal || this.mFillDirection == UIBasicSprite.FillDirection.Vertical)
    {
      if (this.mFillDirection == UIBasicSprite.FillDirection.Horizontal)
      {
        float num = (drawingUvs.z - drawingUvs.x) * this.mFillAmount;
        if (this.mInvert)
        {
          drawingDimensions.x = drawingDimensions.z - (drawingDimensions.z - drawingDimensions.x) * this.mFillAmount;
          drawingUvs.x = drawingUvs.z - num;
        }
        else
        {
          drawingDimensions.z = drawingDimensions.x + (drawingDimensions.z - drawingDimensions.x) * this.mFillAmount;
          drawingUvs.z = drawingUvs.x + num;
        }
      }
      else if (this.mFillDirection == UIBasicSprite.FillDirection.Vertical)
      {
        float num = (drawingUvs.w - drawingUvs.y) * this.mFillAmount;
        if (this.mInvert)
        {
          drawingDimensions.y = drawingDimensions.w - (drawingDimensions.w - drawingDimensions.y) * this.mFillAmount;
          drawingUvs.y = drawingUvs.w - num;
        }
        else
        {
          drawingDimensions.w = drawingDimensions.y + (drawingDimensions.w - drawingDimensions.y) * this.mFillAmount;
          drawingUvs.w = drawingUvs.y + num;
        }
      }
    }
    UIBasicSprite.mTempPos[0] = new Vector2(drawingDimensions.x, drawingDimensions.y);
    UIBasicSprite.mTempPos[1] = new Vector2(drawingDimensions.x, drawingDimensions.w);
    UIBasicSprite.mTempPos[2] = new Vector2(drawingDimensions.z, drawingDimensions.w);
    UIBasicSprite.mTempPos[3] = new Vector2(drawingDimensions.z, drawingDimensions.y);
    UIBasicSprite.mTempUVs[0] = new Vector2(drawingUvs.x, drawingUvs.y);
    UIBasicSprite.mTempUVs[1] = new Vector2(drawingUvs.x, drawingUvs.w);
    UIBasicSprite.mTempUVs[2] = new Vector2(drawingUvs.z, drawingUvs.w);
    UIBasicSprite.mTempUVs[3] = new Vector2(drawingUvs.z, drawingUvs.y);
    if ((double) this.mFillAmount < 1.0)
    {
      if (this.mFillDirection == UIBasicSprite.FillDirection.Radial90)
      {
        if (!UIBasicSprite.RadialCut(UIBasicSprite.mTempPos, UIBasicSprite.mTempUVs, this.mFillAmount, this.mInvert, 0))
          return;
        for (int index = 0; index < 4; ++index)
        {
          verts.Add((Vector3) UIBasicSprite.mTempPos[index]);
          uvs.Add(UIBasicSprite.mTempUVs[index]);
          cols.Add(drawingColor);
        }
        return;
      }
      if (this.mFillDirection == UIBasicSprite.FillDirection.Radial180)
      {
        for (int index1 = 0; index1 < 2; ++index1)
        {
          float t1 = 0.0f;
          float t2 = 1f;
          float t3;
          float t4;
          if (index1 == 0)
          {
            t3 = 0.0f;
            t4 = 0.5f;
          }
          else
          {
            t3 = 0.5f;
            t4 = 1f;
          }
          UIBasicSprite.mTempPos[0].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t3);
          UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x;
          UIBasicSprite.mTempPos[2].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t4);
          UIBasicSprite.mTempPos[3].x = UIBasicSprite.mTempPos[2].x;
          UIBasicSprite.mTempPos[0].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t1);
          UIBasicSprite.mTempPos[1].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t2);
          UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[1].y;
          UIBasicSprite.mTempPos[3].y = UIBasicSprite.mTempPos[0].y;
          UIBasicSprite.mTempUVs[0].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t3);
          UIBasicSprite.mTempUVs[1].x = UIBasicSprite.mTempUVs[0].x;
          UIBasicSprite.mTempUVs[2].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t4);
          UIBasicSprite.mTempUVs[3].x = UIBasicSprite.mTempUVs[2].x;
          UIBasicSprite.mTempUVs[0].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t1);
          UIBasicSprite.mTempUVs[1].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t2);
          UIBasicSprite.mTempUVs[2].y = UIBasicSprite.mTempUVs[1].y;
          UIBasicSprite.mTempUVs[3].y = UIBasicSprite.mTempUVs[0].y;
          float num = this.mInvert ? this.mFillAmount * 2f - (float) (1 - index1) : this.fillAmount * 2f - (float) index1;
          if (UIBasicSprite.RadialCut(UIBasicSprite.mTempPos, UIBasicSprite.mTempUVs, Mathf.Clamp01(num), !this.mInvert, NGUIMath.RepeatIndex(index1 + 3, 4)))
          {
            for (int index2 = 0; index2 < 4; ++index2)
            {
              verts.Add((Vector3) UIBasicSprite.mTempPos[index2]);
              uvs.Add(UIBasicSprite.mTempUVs[index2]);
              cols.Add(drawingColor);
            }
          }
        }
        return;
      }
      if (this.mFillDirection == UIBasicSprite.FillDirection.Radial360)
      {
        for (int index1 = 0; index1 < 4; ++index1)
        {
          float t1;
          float t2;
          if (index1 < 2)
          {
            t1 = 0.0f;
            t2 = 0.5f;
          }
          else
          {
            t1 = 0.5f;
            t2 = 1f;
          }
          float t3;
          float t4;
          if (index1 == 0 || index1 == 3)
          {
            t3 = 0.0f;
            t4 = 0.5f;
          }
          else
          {
            t3 = 0.5f;
            t4 = 1f;
          }
          UIBasicSprite.mTempPos[0].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t1);
          UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x;
          UIBasicSprite.mTempPos[2].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t2);
          UIBasicSprite.mTempPos[3].x = UIBasicSprite.mTempPos[2].x;
          UIBasicSprite.mTempPos[0].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t3);
          UIBasicSprite.mTempPos[1].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t4);
          UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[1].y;
          UIBasicSprite.mTempPos[3].y = UIBasicSprite.mTempPos[0].y;
          UIBasicSprite.mTempUVs[0].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t1);
          UIBasicSprite.mTempUVs[1].x = UIBasicSprite.mTempUVs[0].x;
          UIBasicSprite.mTempUVs[2].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t2);
          UIBasicSprite.mTempUVs[3].x = UIBasicSprite.mTempUVs[2].x;
          UIBasicSprite.mTempUVs[0].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t3);
          UIBasicSprite.mTempUVs[1].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t4);
          UIBasicSprite.mTempUVs[2].y = UIBasicSprite.mTempUVs[1].y;
          UIBasicSprite.mTempUVs[3].y = UIBasicSprite.mTempUVs[0].y;
          float num = !this.mInvert ? this.mFillAmount * 4f - (float) (3 - NGUIMath.RepeatIndex(index1 + 2, 4)) : this.mFillAmount * 4f - (float) NGUIMath.RepeatIndex(index1 + 2, 4);
          if (UIBasicSprite.RadialCut(UIBasicSprite.mTempPos, UIBasicSprite.mTempUVs, Mathf.Clamp01(num), this.mInvert, NGUIMath.RepeatIndex(index1 + 2, 4)))
          {
            for (int index2 = 0; index2 < 4; ++index2)
            {
              verts.Add((Vector3) UIBasicSprite.mTempPos[index2]);
              uvs.Add(UIBasicSprite.mTempUVs[index2]);
              cols.Add(drawingColor);
            }
          }
        }
        return;
      }
    }
    for (int index = 0; index < 4; ++index)
    {
      verts.Add((Vector3) UIBasicSprite.mTempPos[index]);
      uvs.Add(UIBasicSprite.mTempUVs[index]);
      cols.Add(drawingColor);
    }
  }

  private void AdvancedFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Vector4 vector4 = this.border * this.pixelSize;
    if ((double) vector4.x == 0.0 && (double) vector4.y == 0.0 && ((double) vector4.z == 0.0 && (double) vector4.w == 0.0))
    {
      this.SimpleFill(verts, uvs, cols);
    }
    else
    {
      Color drawingColor = this.drawingColor;
      Vector4 drawingDimensions = this.drawingDimensions;
      Vector2 vector2 = new Vector2(this.mInnerUV.width * (float) mainTexture.width, this.mInnerUV.height * (float) mainTexture.height);
      vector2 *= this.pixelSize;
      if ((double) vector2.x < 1.0)
        vector2.x = 1f;
      if ((double) vector2.y < 1.0)
        vector2.y = 1f;
      UIBasicSprite.mTempPos[0].x = drawingDimensions.x;
      UIBasicSprite.mTempPos[0].y = drawingDimensions.y;
      UIBasicSprite.mTempPos[3].x = drawingDimensions.z;
      UIBasicSprite.mTempPos[3].y = drawingDimensions.w;
      if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.z;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.x;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.x;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.z;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMax;
      }
      if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.w;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.y;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.y;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.w;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMax;
      }
      for (int index1 = 0; index1 < 3; ++index1)
      {
        int index2 = index1 + 1;
        for (int index3 = 0; index3 < 3; ++index3)
        {
          if (this.centerType != UIBasicSprite.AdvancedType.Invisible || index1 != 1 || index3 != 1)
          {
            int index4 = index3 + 1;
            if (index1 == 1 && index3 == 1)
            {
              if (this.centerType == UIBasicSprite.AdvancedType.Tiled)
              {
                float x1 = UIBasicSprite.mTempPos[index1].x;
                float x2 = UIBasicSprite.mTempPos[index2].x;
                float y1 = UIBasicSprite.mTempPos[index3].y;
                float y2 = UIBasicSprite.mTempPos[index4].y;
                float x3 = UIBasicSprite.mTempUVs[index1].x;
                float y3 = UIBasicSprite.mTempUVs[index3].y;
                float v0y = y1;
                while ((double) v0y < (double) y2)
                {
                  float v0x = x1;
                  float num1 = UIBasicSprite.mTempUVs[index4].y;
                  float v1y = v0y + vector2.y;
                  if ((double) v1y > (double) y2)
                  {
                    num1 = Mathf.Lerp(y3, num1, (y2 - v0y) / vector2.y);
                    v1y = y2;
                  }
                  while ((double) v0x < (double) x2)
                  {
                    float v1x = v0x + vector2.x;
                    float num2 = UIBasicSprite.mTempUVs[index2].x;
                    if ((double) v1x > (double) x2)
                    {
                      num2 = Mathf.Lerp(x3, num2, (x2 - v0x) / vector2.x);
                      v1x = x2;
                    }
                    UIBasicSprite.Fill(verts, uvs, cols, v0x, v1x, v0y, v1y, x3, num2, y3, num1, drawingColor);
                    v0x += vector2.x;
                  }
                  v0y += vector2.y;
                }
              }
              else if (this.centerType == UIBasicSprite.AdvancedType.Sliced)
                UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, drawingColor);
            }
            else if (index1 == 1)
            {
              if (index3 == 0 && this.bottomType == UIBasicSprite.AdvancedType.Tiled || index3 == 2 && this.topType == UIBasicSprite.AdvancedType.Tiled)
              {
                float x1 = UIBasicSprite.mTempPos[index1].x;
                float x2 = UIBasicSprite.mTempPos[index2].x;
                float y1 = UIBasicSprite.mTempPos[index3].y;
                float y2 = UIBasicSprite.mTempPos[index4].y;
                float x3 = UIBasicSprite.mTempUVs[index1].x;
                float y3 = UIBasicSprite.mTempUVs[index3].y;
                float y4 = UIBasicSprite.mTempUVs[index4].y;
                float v0x = x1;
                while ((double) v0x < (double) x2)
                {
                  float v1x = v0x + vector2.x;
                  float num = UIBasicSprite.mTempUVs[index2].x;
                  if ((double) v1x > (double) x2)
                  {
                    num = Mathf.Lerp(x3, num, (x2 - v0x) / vector2.x);
                    v1x = x2;
                  }
                  UIBasicSprite.Fill(verts, uvs, cols, v0x, v1x, y1, y2, x3, num, y3, y4, drawingColor);
                  v0x += vector2.x;
                }
              }
              else if (index3 == 0 && this.bottomType != UIBasicSprite.AdvancedType.Invisible || index3 == 2 && this.topType != UIBasicSprite.AdvancedType.Invisible)
                UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, drawingColor);
            }
            else if (index3 == 1)
            {
              if (index1 == 0 && this.leftType == UIBasicSprite.AdvancedType.Tiled || index1 == 2 && this.rightType == UIBasicSprite.AdvancedType.Tiled)
              {
                float x1 = UIBasicSprite.mTempPos[index1].x;
                float x2 = UIBasicSprite.mTempPos[index2].x;
                float y1 = UIBasicSprite.mTempPos[index3].y;
                float y2 = UIBasicSprite.mTempPos[index4].y;
                float x3 = UIBasicSprite.mTempUVs[index1].x;
                float x4 = UIBasicSprite.mTempUVs[index2].x;
                float y3 = UIBasicSprite.mTempUVs[index3].y;
                float v0y = y1;
                while ((double) v0y < (double) y2)
                {
                  float num = UIBasicSprite.mTempUVs[index4].y;
                  float v1y = v0y + vector2.y;
                  if ((double) v1y > (double) y2)
                  {
                    num = Mathf.Lerp(y3, num, (y2 - v0y) / vector2.y);
                    v1y = y2;
                  }
                  UIBasicSprite.Fill(verts, uvs, cols, x1, x2, v0y, v1y, x3, x4, y3, num, drawingColor);
                  v0y += vector2.y;
                }
              }
              else if (index1 == 0 && this.leftType != UIBasicSprite.AdvancedType.Invisible || index1 == 2 && this.rightType != UIBasicSprite.AdvancedType.Invisible)
                UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, drawingColor);
            }
            else if (index3 == 0 && this.bottomType != UIBasicSprite.AdvancedType.Invisible || index3 == 2 && this.topType != UIBasicSprite.AdvancedType.Invisible || (index1 == 0 && this.leftType != UIBasicSprite.AdvancedType.Invisible || index1 == 2 && this.rightType != UIBasicSprite.AdvancedType.Invisible))
              UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, drawingColor);
          }
        }
      }
    }
  }

  private static bool RadialCut(Vector2[] xy, Vector2[] uv, float fill, bool invert, int corner)
  {
    if ((double) fill < 1.0 / 1000.0)
      return false;
    if ((corner & 1) == 1)
      invert = !invert;
    if (!invert && (double) fill > 0.999000012874603)
      return true;
    float num = Mathf.Clamp01(fill);
    if (invert)
      num = 1f - num;
    float f = num * 1.570796f;
    float cos = Mathf.Cos(f);
    float sin = Mathf.Sin(f);
    UIBasicSprite.RadialCut(xy, cos, sin, invert, corner);
    UIBasicSprite.RadialCut(uv, cos, sin, invert, corner);
    return true;
  }

  private static void RadialCut(Vector2[] xy, float cos, float sin, bool invert, int corner)
  {
    int index1 = corner;
    int index2 = NGUIMath.RepeatIndex(corner + 1, 4);
    int index3 = NGUIMath.RepeatIndex(corner + 2, 4);
    int index4 = NGUIMath.RepeatIndex(corner + 3, 4);
    if ((corner & 1) == 1)
    {
      if ((double) sin > (double) cos)
      {
        cos /= sin;
        sin = 1f;
        if (invert)
        {
          xy[index2].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
          xy[index3].x = xy[index2].x;
        }
      }
      else if ((double) cos > (double) sin)
      {
        sin /= cos;
        cos = 1f;
        if (!invert)
        {
          xy[index3].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
          xy[index4].y = xy[index3].y;
        }
      }
      else
      {
        cos = 1f;
        sin = 1f;
      }
      if (!invert)
        xy[index4].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
      else
        xy[index2].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
    }
    else
    {
      if ((double) cos > (double) sin)
      {
        sin /= cos;
        cos = 1f;
        if (!invert)
        {
          xy[index2].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
          xy[index3].y = xy[index2].y;
        }
      }
      else if ((double) sin > (double) cos)
      {
        cos /= sin;
        sin = 1f;
        if (invert)
        {
          xy[index3].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
          xy[index4].x = xy[index3].x;
        }
      }
      else
      {
        cos = 1f;
        sin = 1f;
      }
      if (invert)
        xy[index4].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
      else
        xy[index2].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
    }
  }

  private static void Fill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color> cols, float v0x, float v1x, float v0y, float v1y, float u0x, float u1x, float u0y, float u1y, Color col)
  {
    verts.Add(new Vector3(v0x, v0y));
    verts.Add(new Vector3(v0x, v1y));
    verts.Add(new Vector3(v1x, v1y));
    verts.Add(new Vector3(v1x, v0y));
    uvs.Add(new Vector2(u0x, u0y));
    uvs.Add(new Vector2(u0x, u1y));
    uvs.Add(new Vector2(u1x, u1y));
    uvs.Add(new Vector2(u1x, u0y));
    cols.Add(col);
    cols.Add(col);
    cols.Add(col);
    cols.Add(col);
  }

  public enum Type
  {
    Simple,
    Sliced,
    Tiled,
    Filled,
    Advanced,
  }

  public enum FillDirection
  {
    Horizontal,
    Vertical,
    Radial90,
    Radial180,
    Radial360,
  }

  public enum AdvancedType
  {
    Invisible,
    Sliced,
    Tiled,
  }

  public enum Flip
  {
    Nothing,
    Horizontally,
    Vertically,
    Both,
  }
}
