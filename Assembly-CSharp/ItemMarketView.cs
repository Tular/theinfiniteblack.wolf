﻿// Decompiled with JetBrains decompiler
// Type: ItemMarketView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class ItemMarketView : MonoBehaviour
{
  private readonly List<EquipmentItem> _currentItemList = new List<EquipmentItem>(50);
  public GeneralEquipmentItemScrollView itemScrollView;

  public StarportDialogContext context { get; set; }

  public void Show()
  {
    this._currentItemList.Clear();
    NGUITools.SetActiveSelf(this.gameObject, true);
  }

  public void Hide()
  {
    this._currentItemList.Clear();
    this.itemScrollView.ClearData();
    NGUITools.SetActiveSelf(this.gameObject, false);
  }

  private void Update()
  {
    this._currentItemList.Clear();
    this._currentItemList.AddRange((IEnumerable<EquipmentItem>) this.context.starport.get_Inventory());
    this.itemScrollView.SetData(this._currentItemList, EquipmentItemActionContext.Starport);
  }
}
