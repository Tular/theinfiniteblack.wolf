﻿// Decompiled with JetBrains decompiler
// Type: PrivateChatChannelCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class PrivateChatChannelCellInfo : ScrollViewCellInfoBase<ChatChannel, PrivateChatChannelCell>
{
  public System.Action<PrivateChatChannelCellInfo> onCellClickedAction;
  public System.Action<PrivateChatChannelCellInfo> onCloseClickedAction;

  public PrivateChatChannelCellInfo(Transform prefab, ChatChannel data)
    : base(prefab, data)
  {
  }

  public string key
  {
    get
    {
      return this.mCellData.channelKey;
    }
  }

  public string channelName
  {
    get
    {
      return this.mCellData.channelName;
    }
  }

  public ChatChannel chatChannel
  {
    get
    {
      return this.mCellData;
    }
  }

  protected override void SetVisuals(PrivateChatChannelCell cell)
  {
    cell.label.text = this.mCellData.channelName;
    cell.transform.localPosition = this.localPosition;
    EventDelegate.Add(cell.onClick, new EventDelegate.Callback(this.OnCellClicked));
    EventDelegate.Add(cell.onCloseClicked, new EventDelegate.Callback(this.OnCloseClicked));
    EventDelegate.Add(cell.onLongPress, new EventDelegate.Callback(this.OnCellLongPress));
  }

  protected void OnCloseClicked()
  {
    if (this.onCloseClickedAction == null)
      return;
    this.onCloseClickedAction(this);
  }

  protected void OnCellClicked()
  {
    if (this.onCellClickedAction == null)
      return;
    this.onCellClickedAction(this);
  }

  protected void OnCellLongPress()
  {
    InlineWebBrowser.Show(WebFiles.GetPlayerLeaderboardUrl(this.mCellData.channelKey, TibProxy.Instance.serverId));
  }
}
