﻿// Decompiled with JetBrains decompiler
// Type: EditDialogBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class EditDialogBase : MonoBehaviour
{
  public UIPanel dialogPanel;
  protected bool mIsNew;
  private System.Action<object, EditDialogBase.EditDialogAction> _responseCallback;

  protected abstract bool InputIsValid();

  protected abstract void InitializeDialog(object obj);

  protected abstract void InitializeDialog();

  protected abstract object GetResponseData();

  public void Show(System.Action<object, EditDialogBase.EditDialogAction> responseCallback)
  {
    this.mIsNew = true;
    this._responseCallback = responseCallback;
    this.InitializeDialog();
    this.ShowDialog();
  }

  public void Show(object obj, System.Action<object, EditDialogBase.EditDialogAction> responseCallback)
  {
    this.mIsNew = false;
    this._responseCallback = responseCallback;
    this.InitializeDialog(obj);
    this.ShowDialog();
  }

  public void Save()
  {
    if (!this.InputIsValid())
      return;
    this._responseCallback(this.GetResponseData(), !this.mIsNew ? EditDialogBase.EditDialogAction.Save : EditDialogBase.EditDialogAction.Create);
    this.CloseDialog();
  }

  public void Cancel()
  {
    this._responseCallback(this.GetResponseData(), EditDialogBase.EditDialogAction.Cancel);
    this.CloseDialog();
  }

  public void Delete()
  {
    if (this.mIsNew)
      return;
    this._responseCallback(this.GetResponseData(), EditDialogBase.EditDialogAction.Delete);
    this.CloseDialog();
  }

  protected void ShowDialog()
  {
    NGUITools.SetActiveSelf(this.dialogPanel.gameObject, true);
    NGUITools.SetActiveSelf(this.gameObject, true);
  }

  protected void CloseDialog()
  {
    this._responseCallback = (System.Action<object, EditDialogBase.EditDialogAction>) null;
    NGUITools.SetActiveSelf(this.gameObject, false);
    NGUITools.SetActiveSelf(this.dialogPanel.gameObject, false);
  }

  public enum EditDialogAction
  {
    Create,
    Save,
    Cancel,
    Delete,
  }
}
