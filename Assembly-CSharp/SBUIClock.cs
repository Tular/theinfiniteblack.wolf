﻿// Decompiled with JetBrains decompiler
// Type: SBUIClock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[RequireComponent(typeof (UILabel))]
public class SBUIClock : MonoBehaviour
{
  private UILabel _label;
  private float _nextSampleTime;

  private void Awake()
  {
    this._label = this.GetComponent<UILabel>();
  }

  private void OnEnable()
  {
    this._nextSampleTime = float.MinValue;
  }

  private void Update()
  {
    if (!(bool) ((UnityEngine.Object) this._label) || (double) this._nextSampleTime >= (double) RealTime.time)
      return;
    this._label.text = string.Format("{0:HH:mm:ss}", (object) DateTime.Now);
    this._nextSampleTime = RealTime.time + 0.1f;
  }
}
