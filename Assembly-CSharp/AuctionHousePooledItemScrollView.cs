﻿// Decompiled with JetBrains decompiler
// Type: AuctionHousePooledItemScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Client;
using UnityEngine;

public class AuctionHousePooledItemScrollView : PooledScrollViewBase<AuctionHouseItemCellInfo>
{
  private readonly Dictionary<ClientAuction, AuctionHouseItemCellInfo> _auctionLookup = new Dictionary<ClientAuction, AuctionHouseItemCellInfo>();
  private readonly HashSet<ClientAuction> _auctionIndex = new HashSet<ClientAuction>();
  public Transform prefCard;
  private int _nextUpdateFrame;
  private bool _svContextChangedSinceLastUpdate;

  public AuctionHouseItemListingScrollViewContext scrollViewContext
  {
    get
    {
      return ((AuctionHouseItemCellInfoCollection) this.data).scrollViewContext;
    }
    set
    {
      ((AuctionHouseItemCellInfoCollection) this.data).scrollViewContext = value;
    }
  }

  public void Clear()
  {
    this.data.Clear();
    this.ResetPosition();
    this.ResetVisibleInfos();
    this.ResetPosition();
    this._nextUpdateFrame = int.MinValue;
    this._auctionLookup.Clear();
    this._auctionIndex.Clear();
    this._svContextChangedSinceLastUpdate = true;
  }

  protected override void OnInit()
  {
    base.OnInit();
    this.data = (IPooledScrollViewCellInifoCollection<AuctionHouseItemCellInfo>) new AuctionHouseItemCellInfoCollection(this.prefCard);
    if (this.scrollViewContext == null)
      return;
    ((AuctionHouseItemCellInfoCollection) this.data).scrollViewContext = this.scrollViewContext;
  }

  private void UpdateListing()
  {
    if (this.scrollViewContext == null)
      return;
    HashSet<ClientAuction> clientAuctionSet = new HashSet<ClientAuction>(this.scrollViewContext.items);
    this._auctionIndex.ExceptWith((IEnumerable<ClientAuction>) clientAuctionSet);
    using (HashSet<ClientAuction>.Enumerator enumerator = this._auctionIndex.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ClientAuction current = enumerator.Current;
        if (this._auctionLookup.ContainsKey(current))
        {
          this.data.Remove(this._auctionLookup[current]);
          this._auctionLookup.Remove(current);
        }
      }
    }
    using (HashSet<ClientAuction>.Enumerator enumerator = clientAuctionSet.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ClientAuction current = enumerator.Current;
        if (!this._auctionLookup.ContainsKey(current))
        {
          AuctionHouseItemCellInfo cellInfo = new AuctionHouseItemCellInfo(this.prefCard, current);
          this._auctionLookup.Add(current, cellInfo);
          this.data.Add(cellInfo);
        }
      }
    }
    this._auctionIndex.UnionWith((IEnumerable<ClientAuction>) clientAuctionSet);
    this._auctionIndex.IntersectWith((IEnumerable<ClientAuction>) clientAuctionSet);
  }

  protected override void OnEnable()
  {
    base.OnEnable();
    this.ResetPosition();
  }

  private void Update()
  {
    if (!Application.isPlaying)
      return;
    if (this.scrollViewContext.hasChanged)
      this._svContextChangedSinceLastUpdate = true;
    if (this._nextUpdateFrame > Time.frameCount)
      return;
    this._nextUpdateFrame = Time.frameCount + 10;
    if (this._svContextChangedSinceLastUpdate)
    {
      this.ResetPosition();
      this.ResetVisibleInfos();
      this.ResetPosition();
      this.data.Clear();
      this._nextUpdateFrame = 0;
      this._auctionLookup.Clear();
      this._auctionIndex.Clear();
    }
    this._svContextChangedSinceLastUpdate = false;
    this.UpdateListing();
  }

  private void OnDisable()
  {
    if (!Application.isPlaying)
      return;
    this.Clear();
  }
}
