﻿// Decompiled with JetBrains decompiler
// Type: ResourceMarketView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class ResourceMarketView : MonoBehaviour
{
  public ResourceExchangeButton buyMaxMetals;
  public ResourceExchangeButton buyHalfMetals;
  public ResourceExchangeButton buySingleMetal;
  public ResourceExchangeButton sellAllMetals;
  public ResourceExchangeButton sellHalfMetals;
  public ResourceExchangeButton sellSingleMetal;
  public ResourceExchangeButton buyMaxOrganics;
  public ResourceExchangeButton buyHalfOrganics;
  public ResourceExchangeButton buySingleOrganic;
  public ResourceExchangeButton sellAllOrganics;
  public ResourceExchangeButton sellHalfOrganics;
  public ResourceExchangeButton sellSingleOrganic;
  public ResourceExchangeButton buyMaxGas;
  public ResourceExchangeButton buyHalfGas;
  public ResourceExchangeButton buySingleGas;
  public ResourceExchangeButton sellAllGas;
  public ResourceExchangeButton sellHalfGas;
  public ResourceExchangeButton sellSingleGas;
  public ResourceExchangeButton buyMaxRadioactives;
  public ResourceExchangeButton buyHalfRadioactives;
  public ResourceExchangeButton buySingleRadioactive;
  public ResourceExchangeButton sellAllRadioactives;
  public ResourceExchangeButton sellHalfRadioactives;
  public ResourceExchangeButton sellSingleRadioactive;
  public ResourceExchangeButton buyMaxDarkmatter;
  public ResourceExchangeButton buyHalfDarkmatter;
  public ResourceExchangeButton buySingleDarkmatter;
  public ResourceExchangeButton sellAllDarkmatter;
  public ResourceExchangeButton sellHalfDarkmatter;
  public ResourceExchangeButton sellSingleDarkmatter;
  public StarportDialogContext context;

  public void SellAlllMetals()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, this.context.maxSellMetals, 0, 0);
  }

  public void SellHalfMetals()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, this.context.halfSellMetals, 0, 0);
  }

  public void SellSingleMetal()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, 1, 0, 0);
  }

  public void BuyMaxMetals()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, this.context.maxBuyMetals, 0, 0);
  }

  public void BuyHalfMetals()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, this.context.halfBuyMetals, 0, 0);
  }

  public void BuySingleMetal()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, 1, 0, 0);
  }

  public void SellAlllOrganics()
  {
    TibProxy.gameState.DoResourcesSell(this.context.maxSellOrganics, 0, 0, 0, 0);
  }

  public void SellHalfOrganics()
  {
    TibProxy.gameState.DoResourcesSell(this.context.halfSellOrganics, 0, 0, 0, 0);
  }

  public void SellSingleOrganic()
  {
    TibProxy.gameState.DoResourcesSell(1, 0, 0, 0, 0);
  }

  public void BuyMaxOrganics()
  {
    TibProxy.gameState.DoResourcesBuy(this.context.maxBuyOrganics, 0, 0, 0, 0);
  }

  public void BuyHalfOrganics()
  {
    TibProxy.gameState.DoResourcesBuy(this.context.halfBuyOrganics, 0, 0, 0, 0);
  }

  public void BuySingleOrganic()
  {
    TibProxy.gameState.DoResourcesBuy(1, 0, 0, 0, 0);
  }

  public void SellAlllGas()
  {
    TibProxy.gameState.DoResourcesSell(0, this.context.maxSellGas, 0, 0, 0);
  }

  public void SellHalfGas()
  {
    TibProxy.gameState.DoResourcesSell(0, this.context.halfSellGas, 0, 0, 0);
  }

  public void SellSingleGas()
  {
    TibProxy.gameState.DoResourcesSell(0, 1, 0, 0, 0);
  }

  public void BuyMaxGas()
  {
    TibProxy.gameState.DoResourcesBuy(0, this.context.maxBuyGas, 0, 0, 0);
  }

  public void BuyHalfGas()
  {
    TibProxy.gameState.DoResourcesBuy(0, this.context.halfBuyGas, 0, 0, 0);
  }

  public void BuySingleGas()
  {
    TibProxy.gameState.DoResourcesBuy(0, 1, 0, 0, 0);
  }

  public void SellAlllRadioactives()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, 0, this.context.maxSellRadioactives, 0);
  }

  public void SellHalfRadioactives()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, 0, this.context.halfSellRadioactives, 0);
  }

  public void SellSingleRadioactive()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, 0, 1, 0);
  }

  public void BuyMaxRadioactives()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, 0, this.context.maxBuyRadioactives, 0);
  }

  public void BuyHalfRadioactives()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, 0, this.context.halfBuyRadioactives, 0);
  }

  public void BuySingleRadioactive()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, 0, 1, 0);
  }

  public void SellAlllDarkmatter()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, 0, 0, this.context.maxSellDarkmatter);
  }

  public void SellHalfDarkmatter()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, 0, 0, this.context.halfSellDarkmatter);
  }

  public void SellSingleDarkmatter()
  {
    TibProxy.gameState.DoResourcesSell(0, 0, 0, 0, 1);
  }

  public void BuyMaxDarkmatter()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, 0, 0, this.context.maxBuyDarkmatter);
  }

  public void BuyHalfDarkmatter()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, 0, 0, this.context.halfBuyDarkmatter);
  }

  public void BuySingleDarkmatter()
  {
    TibProxy.gameState.DoResourcesBuy(0, 0, 0, 0, 1);
  }

  public void SellAllNonMetalResources()
  {
    TibProxy.gameState.DoResourcesSellAllExceptMetals();
  }

  public void SellAllResources()
  {
    TibProxy.gameState.DoResourcesSellAll();
  }

  private void UpdateResourceRow(ResourceType resourceType, int space, int current, int maxQty, int halfQty, ResourceExchangeButton maxBtn, ResourceExchangeButton halfBtn, ResourceExchangeButton singleBtn, bool isSelling)
  {
    if (current == space)
    {
      NGUITools.SetActiveSelf(singleBtn.gameObject, isSelling);
      NGUITools.SetActiveSelf(halfBtn.gameObject, isSelling);
      NGUITools.SetActiveSelf(maxBtn.gameObject, isSelling);
    }
    else
    {
      NGUITools.SetActiveSelf(singleBtn.gameObject, !isSelling || current > 0);
      NGUITools.SetActiveSelf(halfBtn.gameObject, halfQty >= 1);
      NGUITools.SetActiveSelf(maxBtn.gameObject, halfQty < maxQty && 2 < maxQty);
    }
    if (!singleBtn.enabled)
      return;
    singleBtn.quantity = !isSelling ? 1 : -1;
    singleBtn.cost = !isSelling ? -resourceType.BuyCost(1) : resourceType.SellValue(1);
    if (!halfBtn.enabled)
      return;
    if (halfQty == 1)
    {
      halfBtn.quantity = !isSelling ? maxQty : -maxQty;
      halfBtn.cost = !isSelling ? -resourceType.BuyCost(maxQty) : resourceType.SellValue(maxQty);
    }
    else
    {
      halfBtn.quantity = !isSelling ? halfQty : -halfQty;
      halfBtn.cost = !isSelling ? -resourceType.BuyCost(halfQty) : resourceType.SellValue(halfQty);
    }
    if (!maxBtn.enabled)
      return;
    maxBtn.quantity = !isSelling ? maxQty : -maxQty;
    maxBtn.cost = !isSelling ? -resourceType.BuyCost(maxQty) : resourceType.SellValue(maxQty);
  }

  public void UpdateVisuals()
  {
    if (this.context == null)
    {
      this.LogD(string.Format("Context is null"));
    }
    else
    {
      this.UpdateResourceRow(ResourceType.METAL, this.context.shipCargoSpace, this.context.shipMetals, this.context.maxBuyMetals, this.context.halfBuyMetals, this.buyMaxMetals, this.buyHalfMetals, this.buySingleMetal, false);
      this.UpdateResourceRow(ResourceType.METAL, this.context.shipCargoSpace, this.context.shipMetals, this.context.maxSellMetals, this.context.halfSellMetals, this.sellAllMetals, this.sellHalfMetals, this.sellSingleMetal, true);
      this.UpdateResourceRow(ResourceType.ORGANIC, this.context.shipCargoSpace, this.context.shipOrganics, this.context.maxBuyOrganics, this.context.halfBuyOrganics, this.buyMaxOrganics, this.buyHalfOrganics, this.buySingleOrganic, false);
      this.UpdateResourceRow(ResourceType.ORGANIC, this.context.shipCargoSpace, this.context.shipOrganics, this.context.maxSellOrganics, this.context.halfSellOrganics, this.sellAllOrganics, this.sellHalfOrganics, this.sellSingleOrganic, true);
      this.UpdateResourceRow(ResourceType.GAS, this.context.shipCargoSpace, this.context.shipGas, this.context.maxBuyGas, this.context.halfBuyGas, this.buyMaxGas, this.buyHalfGas, this.buySingleGas, false);
      this.UpdateResourceRow(ResourceType.GAS, this.context.shipCargoSpace, this.context.shipGas, this.context.maxSellGas, this.context.halfSellGas, this.sellAllGas, this.sellHalfGas, this.sellSingleGas, true);
      this.UpdateResourceRow(ResourceType.RADIOACTIVE, this.context.shipCargoSpace, this.context.shipRadioactives, this.context.maxBuyRadioactives, this.context.halfBuyRadioactives, this.buyMaxRadioactives, this.buyHalfRadioactives, this.buySingleRadioactive, false);
      this.UpdateResourceRow(ResourceType.RADIOACTIVE, this.context.shipCargoSpace, this.context.shipRadioactives, this.context.maxSellRadioactives, this.context.halfSellRadioactives, this.sellAllRadioactives, this.sellHalfRadioactives, this.sellSingleRadioactive, true);
      this.UpdateResourceRow(ResourceType.DARKMATTER, this.context.shipCargoSpace, this.context.shipDarkmatter, this.context.maxBuyDarkmatter, this.context.halfBuyDarkmatter, this.buyMaxDarkmatter, this.buyHalfDarkmatter, this.buySingleDarkmatter, false);
      this.UpdateResourceRow(ResourceType.DARKMATTER, this.context.shipCargoSpace, this.context.shipDarkmatter, this.context.maxSellDarkmatter, this.context.halfSellDarkmatter, this.sellAllDarkmatter, this.sellHalfDarkmatter, this.sellSingleDarkmatter, true);
    }
  }

  private void TibProxy_onResourcesChangedEvent(object sender, ResourcesChangedEventArgs e)
  {
    this.LogD(string.Format("Resource change event"));
    if (e.Source != this.context.ship)
      return;
    this.UpdateVisuals();
  }

  private void OnEnable()
  {
    TibProxy.Instance.onResourcesChangedEvent -= new EventHandler<ResourcesChangedEventArgs>(this.TibProxy_onResourcesChangedEvent);
    TibProxy.Instance.onResourcesChangedEvent += new EventHandler<ResourcesChangedEventArgs>(this.TibProxy_onResourcesChangedEvent);
  }

  private void OnDisable()
  {
    TibProxy.Instance.onResourcesChangedEvent -= new EventHandler<ResourcesChangedEventArgs>(this.TibProxy_onResourcesChangedEvent);
  }
}
