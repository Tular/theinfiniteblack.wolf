﻿// Decompiled with JetBrains decompiler
// Type: ItemEngineeringButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Library;
using UnityEngine;

[RequireComponent(typeof (SBUIButton))]
[RequireComponent(typeof (UIWidget))]
public class ItemEngineeringButton : MonoBehaviour
{
  public EngineeringType engineeringType;
  private UIWidget _widget;
  private SBUIButton _button;
  private ItemEngineeringDialogContext _dialogContext;

  private void Awake()
  {
    this._button = this.GetComponent<SBUIButton>();
    this._widget = this.GetComponent<UIWidget>();
  }

  private void Start()
  {
    ItemEngineeringDialog inParents = NGUITools.FindInParents<ItemEngineeringDialog>(this.gameObject);
    if (!(bool) ((Object) inParents))
      this.LogD(string.Format("EngineeringItemListingView - could not find parent dialog"));
    else if (inParents.dialogContext == null)
      this.LogD(string.Format("EngineeringItemListingView - dialog context is null"));
    this._dialogContext = inParents.dialogContext;
  }

  private void Update()
  {
    if (this._dialogContext == null || this._dialogContext.selectedInventoryItem == null || !this._dialogContext.selectedInventoryItem.CanUpgrade(this.engineeringType))
    {
      this._widget.alpha = 0.0f;
      this._button.SetState(SBUIButtonComponent.State.Disabled);
    }
    else
    {
      this._widget.alpha = 1f;
      if (this._button.isEnabled)
        return;
      this._button.SetState(SBUIButtonComponent.State.Normal);
    }
  }
}
