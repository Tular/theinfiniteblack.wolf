﻿// Decompiled with JetBrains decompiler
// Type: Music
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public class Music : MonoBehaviour
{
  private static float mVolume = 0.3f;
  private float mLastVolume = -1f;
  private static bool mLoaded;
  public AudioClip[] clips;
  private AudioSource mAudio;
  private float mSilence;
  private AudioClip mLastClip;

  public static float volume
  {
    get
    {
      if (!Music.mLoaded)
      {
        Music.mLoaded = true;
        Music.mVolume = PlayerPrefs.GetFloat(nameof (Music), 0.3f);
      }
      return Music.mVolume;
    }
    set
    {
      if ((double) Music.mVolume == (double) value)
        return;
      Music.mLoaded = true;
      Music.mVolume = value;
      PlayerPrefs.SetFloat(nameof (Music), value);
    }
  }

  private void Awake()
  {
    this.mAudio = this.GetComponent<AudioSource>();
    if (this.clips.Length == 0)
      this.enabled = false;
    else
      this.Play();
  }

  private void Update()
  {
    if (!((Object) this.mAudio != (Object) null))
      return;
    if ((double) this.mLastVolume != (double) Music.volume)
    {
      this.mLastVolume = Music.mVolume;
      this.mAudio.volume = Music.mVolume;
      this.mAudio.enabled = (double) Music.mVolume > 0.00999999977648258;
    }
    if ((double) Music.mVolume <= 0.00999999977648258 || this.mAudio.isPlaying)
      return;
    if ((double) this.mSilence == 0.0)
    {
      this.mSilence = Random.Range(50f, 150f);
    }
    else
    {
      this.mSilence -= RealTime.deltaTime;
      if ((double) this.mSilence > 0.0)
        return;
      this.mSilence = 0.0f;
      this.Play();
    }
  }

  private void Play()
  {
    if (this.clips.Length == 1)
    {
      this.mLastClip = this.clips[0];
      this.mAudio.clip = this.clips[0];
    }
    else
    {
      for (int index = 0; index < 100; ++index)
      {
        AudioClip clip = this.clips[Random.Range(0, this.clips.Length)];
        if ((Object) clip != (Object) this.mLastClip)
        {
          this.mLastClip = clip;
          this.mAudio.clip = clip;
          break;
        }
      }
    }
    this.mAudio.Play();
  }
}
