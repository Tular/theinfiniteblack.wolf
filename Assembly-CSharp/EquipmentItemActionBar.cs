﻿// Decompiled with JetBrains decompiler
// Type: EquipmentItemActionBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class EquipmentItemActionBar : MonoBehaviour
{
  public UIGrid buttonGrid;
  public SBUIButton equip;
  public SBUIButton unequip;
  public SBUIButton toBank;
  public SBUIButton toInventory;
  public SBUIButton toAuction;
  public SBUIButton scrap;
  public SBUIButton jettison;
  public SBUIButton upgrade;
  public TweenPosition tweener;
  private bool _itemIsEquipped;
  private EquipmentItemActionContext _actionContext;
  private EquipmentItem _item;

  public void ShowActionBar(EquipmentItem item, EquipmentItemActionContext actionContext)
  {
    this._item = item;
    this._actionContext = actionContext;
    this.UpdateActionButtons();
    if (this.tweener.enabled)
    {
      if ((double) this.tweener.amountPerDelta < 0.0)
        this.tweener.PlayForward();
      else
        this.tweener.PlayReverse();
    }
    else
      this.tweener.PlayForward();
  }

  public void HideActionBar()
  {
    if (this.tweener.enabled)
    {
      if ((double) this.tweener.amountPerDelta < 0.0)
        this.tweener.PlayForward();
      else
        this.tweener.PlayReverse();
    }
    else
      this.tweener.PlayReverse();
  }

  private void UpdateActionButtons()
  {
    bool flag = TibProxy.gameState != null && TibProxy.gameState.MyShip != null && this._item != null && TibProxy.gameState.MyShip.GetEquippedItem(this._item.Type) == this._item;
    switch (this._actionContext)
    {
      case EquipmentItemActionContext.InventorySelf:
        NGUITools.SetActiveSelf(this.unequip.gameObject, false);
        NGUITools.SetActiveSelf(this.equip.gameObject, true);
        NGUITools.SetActiveSelf(this.toBank.gameObject, true);
        NGUITools.SetActiveSelf(this.toInventory.gameObject, false);
        NGUITools.SetActiveSelf(this.toAuction.gameObject, !this._item.NoDrop && this._item.CanAuction);
        NGUITools.SetActiveSelf(this.scrap.gameObject, true);
        NGUITools.SetActiveSelf(this.jettison.gameObject, true);
        NGUITools.SetActiveSelf(this.upgrade.gameObject, this._item.Rarity > ItemRarity.COMMON);
        break;
      case EquipmentItemActionContext.EquippedSelf:
        NGUITools.SetActiveSelf(this.unequip.gameObject, true);
        NGUITools.SetActiveSelf(this.equip.gameObject, false);
        NGUITools.SetActiveSelf(this.toBank.gameObject, false);
        NGUITools.SetActiveSelf(this.toInventory.gameObject, false);
        NGUITools.SetActiveSelf(this.toAuction.gameObject, false);
        NGUITools.SetActiveSelf(this.scrap.gameObject, false);
        NGUITools.SetActiveSelf(this.jettison.gameObject, false);
        NGUITools.SetActiveSelf(this.upgrade.gameObject, false);
        break;
      case EquipmentItemActionContext.Bank:
        NGUITools.SetActiveSelf(this.unequip.gameObject, false);
        NGUITools.SetActiveSelf(this.equip.gameObject, false);
        NGUITools.SetActiveSelf(this.toBank.gameObject, false);
        NGUITools.SetActiveSelf(this.toInventory.gameObject, true);
        NGUITools.SetActiveSelf(this.toAuction.gameObject, !this._item.NoDrop && this._item.CanAuction);
        NGUITools.SetActiveSelf(this.scrap.gameObject, true);
        NGUITools.SetActiveSelf(this.jettison.gameObject, false);
        NGUITools.SetActiveSelf(this.upgrade.gameObject, this._item.Rarity > ItemRarity.COMMON);
        break;
      default:
        this.ResetActionButtons();
        break;
    }
    this.buttonGrid.Reposition();
  }

  private void ResetActionButtons()
  {
    this.equip.SetState(SBUIButtonComponent.State.Normal);
    this.unequip.SetState(SBUIButtonComponent.State.Normal);
    this.toBank.SetState(SBUIButtonComponent.State.Normal);
    this.toInventory.SetState(SBUIButtonComponent.State.Normal);
    this.toAuction.SetState(SBUIButtonComponent.State.Normal);
    this.scrap.SetState(SBUIButtonComponent.State.Normal);
    this.jettison.SetState(SBUIButtonComponent.State.Normal);
    this.upgrade.SetState(SBUIButtonComponent.State.Normal);
    NGUITools.SetActiveSelf(this.unequip.gameObject, false);
    NGUITools.SetActiveSelf(this.equip.gameObject, false);
    NGUITools.SetActiveSelf(this.toBank.gameObject, false);
    NGUITools.SetActiveSelf(this.toInventory.gameObject, false);
    NGUITools.SetActiveSelf(this.toAuction.gameObject, false);
    NGUITools.SetActiveSelf(this.scrap.gameObject, false);
    NGUITools.SetActiveSelf(this.jettison.gameObject, false);
    NGUITools.SetActiveSelf(this.upgrade.gameObject, false);
  }

  private void ResetTweener()
  {
    this.tweener.PlayForward();
    this.tweener.ResetToBeginning();
  }

  private void OnActionBarTweenFinished()
  {
    if (this.tweener.direction != AnimationOrTween.Direction.Reverse)
      return;
    NGUITools.SetActiveSelf(this.tweener.gameObject, false);
  }

  private void Start()
  {
    if (!((Object) this.tweener == (Object) null))
      return;
    this.tweener = this.GetComponent<TweenPosition>();
  }
}
