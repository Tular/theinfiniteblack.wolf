﻿// Decompiled with JetBrains decompiler
// Type: AnimationOrTween.DisableCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

namespace AnimationOrTween
{
  public enum DisableCondition
  {
    DisableAfterReverse = -1,
    DoNotDisable = 0,
    DisableAfterForward = 1,
  }
}
