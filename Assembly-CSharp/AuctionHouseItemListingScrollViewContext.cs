﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseItemListingScrollViewContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;

public class AuctionHouseItemListingScrollViewContext : IGeneralScrollViewContext<ClientAuction>
{
  private IAuctionFilter _filter;
  private AuctionHouseItemListingScrollViewContext.AuctionListingSortOrder _sortOrder;
  private AuctionHouseDialogContext _dialogContext;
  private bool _hasChanged;

  public IAuctionFilter filter
  {
    get
    {
      if (this._filter == this._dialogContext.filter)
        return this._filter;
      this._filter = this._dialogContext.filter;
      this.hasChanged = true;
      return this._filter;
    }
  }

  public AuctionHouseItemListingScrollViewContext.AuctionListingSortOrder sortOrder
  {
    get
    {
      return this._sortOrder;
    }
    set
    {
      if (this._sortOrder == value)
        return;
      this._sortOrder = value;
      this.hasChanged = true;
    }
  }

  public AuctionHouseDialogContext dialogContext
  {
    get
    {
      return this._dialogContext;
    }
    set
    {
      this._dialogContext = value;
      this._filter = this._dialogContext.filter;
      this.hasChanged = true;
    }
  }

  public bool hasChanged
  {
    get
    {
      if (!this._hasChanged)
        return this._hasChanged;
      this._hasChanged = false;
      return true;
    }
    private set
    {
      this._hasChanged = value;
    }
  }

  public IEnumerable<ClientAuction> items
  {
    get
    {
      if (this.filter == null)
      {
        TheInfiniteBlack.Library.Log.D((object) this, "IEnumerable<ClientAuction> items", "filter is null");
        return (IEnumerable<ClientAuction>) null;
      }
      if (this.sortOrder == AuctionHouseItemListingScrollViewContext.AuctionListingSortOrder.Alphabetical)
        return (IEnumerable<ClientAuction>) this.filter.GetFilteredAuctions(TibProxy.gameState.Auctions.get_Values()).OrderBy<ClientAuction, string>((Func<ClientAuction, string>) (a => a.Item.LongName));
      return (IEnumerable<ClientAuction>) this.filter.GetFilteredAuctions(TibProxy.gameState.Auctions.get_Values()).OrderBy<ClientAuction, long>((Func<ClientAuction, long>) (a => a.Time.RemainingMS)).ThenBy<ClientAuction, string>((Func<ClientAuction, string>) (a => a.Item.LongName));
    }
  }

  public enum AuctionListingSortOrder
  {
    Alphabetical,
    TimeRemaining,
  }
}
