﻿// Decompiled with JetBrains decompiler
// Type: AllianceStructure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Diagnostics;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class AllianceStructure
{
  private readonly List<CorpStructure> _corpList = new List<CorpStructure>();
  private ClientAlliance _alliance;
  private CorpStructure _allianceLeader;
  private readonly PlayerDirectoryCellInfo _allianceNameHeading;
  private readonly Transform _headingPrefab;
  private ClientAlliance _lastAlliance;

  public AllianceStructure(Transform headingPrefab)
  {
    this._allianceNameHeading = new PlayerDirectoryCellInfo(headingPrefab, "> NO ALLIANCE <");
    this._headingPrefab = headingPrefab;
  }

  public ClientAlliance alliance
  {
    get
    {
      return this._alliance;
    }
    set
    {
      this._alliance = value;
      if (this._alliance == null)
        return;
      if (TibProxy.gameState.MyPlayer.Rank == CorpRankType.LEADER)
        this._allianceNameHeading.SetDisplayText("- ALLIANCE -", "-ALLIANCE-");
      else
        this._allianceNameHeading.SetDisplayText("- ALLIANCE -");
    }
  }

  [DebuggerHidden]
  public IEnumerable<PlayerDirectoryCellInfo> GetPlayerInfos(Dictionary<int, PlayerDirectoryCellInfo> onlinePlayersLookup)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AllianceStructure.\u003CGetPlayerInfos\u003Ec__Iterator16 infosCIterator16 = new AllianceStructure.\u003CGetPlayerInfos\u003Ec__Iterator16()
    {
      onlinePlayersLookup = onlinePlayersLookup,
      \u003C\u0024\u003EonlinePlayersLookup = onlinePlayersLookup,
      \u003C\u003Ef__this = this
    };
    // ISSUE: reference to a compiler-generated field
    infosCIterator16.\u0024PC = -2;
    return (IEnumerable<PlayerDirectoryCellInfo>) infosCIterator16;
  }
}
