﻿// Decompiled with JetBrains decompiler
// Type: TwoLongTapMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TwoLongTapMe : MonoBehaviour
{
  private TextMesh textMesh;
  private Color startColor;

  private void OnEnable()
  {
    EasyTouch.On_LongTapStart2Fingers += new EasyTouch.LongTapStart2FingersHandler(this.On_LongTapStart2Fingers);
    EasyTouch.On_LongTap2Fingers += new EasyTouch.LongTap2FingersHandler(this.On_LongTap2Fingers);
    EasyTouch.On_LongTapEnd2Fingers += new EasyTouch.LongTapEnd2FingersHandler(this.On_LongTapEnd2Fingers);
    EasyTouch.On_Cancel2Fingers += new EasyTouch.Cancel2FingersHandler(this.On_Cancel2Fingers);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_LongTapStart2Fingers -= new EasyTouch.LongTapStart2FingersHandler(this.On_LongTapStart2Fingers);
    EasyTouch.On_LongTap2Fingers -= new EasyTouch.LongTap2FingersHandler(this.On_LongTap2Fingers);
    EasyTouch.On_LongTapEnd2Fingers -= new EasyTouch.LongTapEnd2FingersHandler(this.On_LongTapEnd2Fingers);
    EasyTouch.On_Cancel2Fingers -= new EasyTouch.Cancel2FingersHandler(this.On_Cancel2Fingers);
  }

  private void Start()
  {
    this.textMesh = this.GetComponentInChildren<TextMesh>();
    this.startColor = this.gameObject.GetComponent<Renderer>().material.color;
  }

  private void On_LongTapStart2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.RandomColor();
  }

  private void On_LongTap2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.textMesh.text = gesture.actionTime.ToString("f2");
  }

  private void On_LongTapEnd2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.gameObject.GetComponent<Renderer>().material.color = this.startColor;
    this.textMesh.text = "Long tap me";
  }

  private void On_Cancel2Fingers(Gesture gesture)
  {
    this.On_LongTapEnd2Fingers(gesture);
  }

  private void RandomColor()
  {
    this.gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1f), Random.Range(0.0f, 1f), Random.Range(0.0f, 1f));
  }
}
