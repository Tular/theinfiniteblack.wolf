﻿// Decompiled with JetBrains decompiler
// Type: UIPlayButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIButton))]
public class UIPlayButton : MonoBehaviour
{
  public UIPlayButton.Type type = UIPlayButton.Type.Single;
  public static UIPlayButton.Type choice;
  public bool startOnClick;
  public bool disableInWebPlayer;
  public bool requireWifi;
  private UIButton mButton;
  private bool mForceDisable;

  public UIPlayButton.Type chosenType
  {
    get
    {
      if (this.type != UIPlayButton.Type.None)
        return this.type;
      return UIPlayButton.choice;
    }
  }

  private void Awake()
  {
    this.mButton = this.GetComponent<UIButton>();
  }

  private void OnEnable()
  {
    this.mForceDisable = this.requireWifi && !PlayerProfile.allowedToAccessInternet;
  }

  private void Update()
  {
    if (this.mForceDisable)
      this.mButton.isEnabled = false;
    else
      this.mButton.isEnabled = !this.startOnClick || this.chosenType < UIPlayButton.Type.Multi;
  }

  private void OnClick()
  {
    if (this.startOnClick)
    {
      switch (this.chosenType)
      {
        case UIPlayButton.Type.Single:
          GameManager.StartSingleGame();
          break;
        case UIPlayButton.Type.Multi:
          Debug.LogError((object) "You need the TNet version of the package to play multiplayer.");
          break;
      }
    }
    else
      UIPlayButton.choice = this.type;
  }

  public enum Type
  {
    None,
    Single,
    Multi,
  }
}
