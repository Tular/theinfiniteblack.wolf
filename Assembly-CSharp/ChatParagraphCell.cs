﻿// Decompiled with JetBrains decompiler
// Type: ChatParagraphCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ChatParagraphCell : MonoBehaviour, IScrollViewCell
{
  public int xPadding = 4;
  public UILabel paragraph;

  public Vector3 extents
  {
    get
    {
      return new Vector3((float) this.paragraph.width / 2f, (float) this.paragraph.height / 2f, 0.0f);
    }
  }

  public int fontSize
  {
    get
    {
      return this.paragraph.fontSize;
    }
    set
    {
      this.paragraph.fontSize = value;
    }
  }

  public void ResetVisuals()
  {
    this.paragraph.text = string.Empty;
  }

  private void Awake()
  {
    if (!(bool) ((Object) this.paragraph))
      this.paragraph = this.GetComponent<UILabel>();
    this.ResetVisuals();
  }
}
