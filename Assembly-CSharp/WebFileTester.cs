﻿// Decompiled with JetBrains decompiler
// Type: WebFileTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Unity;
using UnityEngine;

[AdvancedInspector.AdvancedInspector]
public class WebFileTester : MonoBehaviour
{
  [Inspect(0)]
  public string fileAlias;

  [Inspect(10)]
  private void LoadFile()
  {
    UnityEngine.Debug.Log((object) "Starting Coroutine");
    this.StartCoroutine(this.LoadFileCoroutine());
  }

  private void Awake()
  {
    ClientLog.Instance = (IUnityLogger) new ClientLogger()
    {
      Debug = false,
      clientLogLevel = ClientLogLevel.Debug
    };
  }

  [DebuggerHidden]
  private IEnumerator LoadFileCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new WebFileTester.\u003CLoadFileCoroutine\u003Ec__IteratorB()
    {
      \u003C\u003Ef__this = this
    };
  }
}
