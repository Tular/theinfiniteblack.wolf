﻿// Decompiled with JetBrains decompiler
// Type: InAppPurchasingContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;

public class InAppPurchasingContext : IInAppPurchasingContext
{
  private string _lastNameUsed;

  public InAppPurchasingContext(IIapOrderManager iapManager, IPurchaseProcessor iapProcessor, IStoreInterface iapStore)
  {
    this.iapManager = iapManager;
    this.iapProcessor = iapProcessor;
    this.iapStore = iapStore;
  }

  public InAppPurchasingContext(IIapOrderManager iapManager, IPurchaseProcessor iapProcessor)
  {
    this.iapManager = iapManager;
    this.iapProcessor = iapProcessor;
  }

  public IPurchaseProcessor iapProcessor { get; private set; }

  public IIapOrderManager iapManager { get; private set; }

  public IStoreInterface iapStore { get; private set; }

  public virtual string purchaserName
  {
    get
    {
      if (TibProxy.gameState != null && TibProxy.gameState.MyPlayer != null && !string.IsNullOrEmpty(TibProxy.gameState.MyPlayer.Name))
      {
        this._lastNameUsed = TibProxy.gameState.MyPlayer.Name;
        return TibProxy.gameState.MyPlayer.Name;
      }
      if (!string.IsNullOrEmpty(this._lastNameUsed))
        return this._lastNameUsed;
      return string.Empty;
    }
  }

  public bool isValid
  {
    get
    {
      if (this.iapProcessor != null)
        return null != this.iapManager;
      return false;
    }
  }

  public bool isBillingInitialized
  {
    get
    {
      if (this.isValid)
        return this.iapStore.IsInitialized;
      return false;
    }
  }

  public void TryPurchase(string productId)
  {
    if (!this.iapStore.IsInitialized)
      TheInfiniteBlack.Library.Log.D((object) this, nameof (TryPurchase), "Purchasing system is not initialized");
    else
      this.iapStore.InitiatePurchase(productId, this.purchaserName);
  }
}
