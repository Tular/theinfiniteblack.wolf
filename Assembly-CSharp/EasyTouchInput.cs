﻿// Decompiled with JetBrains decompiler
// Type: EasyTouchInput
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EasyTouchInput
{
  private Vector2[] oldMousePosition = new Vector2[2];
  private int[] tapCount = new int[2];
  private float[] startActionTime = new float[2];
  private float[] deltaTime = new float[2];
  private float[] tapeTime = new float[2];
  private bool bComplex;
  private Vector2 deltaFingerPosition;
  private Vector2 oldFinger2Position;
  private Vector2 complexCenter;

  public int TouchCount()
  {
    return this.getTouchCount(false);
  }

  private int getTouchCount(bool realTouch)
  {
    int num = 0;
    if (realTouch || EasyTouch.instance.enableRemote)
      num = Input.touchCount;
    else if (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))
    {
      num = 1;
      if (EasyTouch.GetSecondeFingerSimulation())
      {
        if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(EasyTouch.instance.twistKey) || (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(EasyTouch.instance.swipeKey)))
          num = 2;
        if (Input.GetKeyUp(KeyCode.LeftAlt) || Input.GetKeyUp(EasyTouch.instance.twistKey) || (Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(EasyTouch.instance.swipeKey)))
          num = 2;
      }
      if (num == 0)
      {
        this.complexCenter = Vector2.zero;
        this.oldMousePosition[0] = new Vector2(-1f, -1f);
        this.oldMousePosition[1] = new Vector2(-1f, -1f);
      }
    }
    return num;
  }

  public Finger GetMouseTouch(int fingerIndex, Finger myFinger)
  {
    Finger finger;
    if (myFinger != null)
    {
      finger = myFinger;
    }
    else
    {
      finger = new Finger();
      finger.gesture = EasyTouch.GestureType.None;
    }
    if (fingerIndex == 1 && (Input.GetKeyUp(KeyCode.LeftAlt) || Input.GetKeyUp(EasyTouch.instance.twistKey) || (Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(EasyTouch.instance.swipeKey))))
    {
      finger.fingerIndex = fingerIndex;
      finger.position = this.oldFinger2Position;
      finger.deltaPosition = finger.position - this.oldFinger2Position;
      finger.tapCount = this.tapCount[fingerIndex];
      finger.deltaTime = Time.realtimeSinceStartup - this.deltaTime[fingerIndex];
      finger.phase = TouchPhase.Ended;
      return finger;
    }
    if (Input.GetMouseButton(0))
    {
      finger.fingerIndex = fingerIndex;
      finger.position = this.GetPointerPosition(fingerIndex);
      if ((double) Time.realtimeSinceStartup - (double) this.tapeTime[fingerIndex] > 0.5)
        this.tapCount[fingerIndex] = 0;
      if (Input.GetMouseButtonDown(0) || fingerIndex == 1 && (Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown(EasyTouch.instance.twistKey) || (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(EasyTouch.instance.swipeKey))))
      {
        finger.position = this.GetPointerPosition(fingerIndex);
        finger.deltaPosition = Vector2.zero;
        this.tapCount[fingerIndex] = this.tapCount[fingerIndex] + 1;
        finger.tapCount = this.tapCount[fingerIndex];
        this.startActionTime[fingerIndex] = Time.realtimeSinceStartup;
        this.deltaTime[fingerIndex] = this.startActionTime[fingerIndex];
        finger.deltaTime = 0.0f;
        finger.phase = TouchPhase.Began;
        if (fingerIndex == 1)
        {
          this.oldFinger2Position = finger.position;
          this.oldMousePosition[fingerIndex] = finger.position;
        }
        else
          this.oldMousePosition[fingerIndex] = finger.position;
        if (this.tapCount[fingerIndex] == 1)
          this.tapeTime[fingerIndex] = Time.realtimeSinceStartup;
        return finger;
      }
      finger.deltaPosition = finger.position - this.oldMousePosition[fingerIndex];
      finger.tapCount = this.tapCount[fingerIndex];
      finger.deltaTime = Time.realtimeSinceStartup - this.deltaTime[fingerIndex];
      finger.phase = (double) finger.deltaPosition.sqrMagnitude >= 1.0 ? TouchPhase.Moved : TouchPhase.Stationary;
      this.oldMousePosition[fingerIndex] = finger.position;
      this.deltaTime[fingerIndex] = Time.realtimeSinceStartup;
      return finger;
    }
    if (!Input.GetMouseButtonUp(0))
      return (Finger) null;
    finger.fingerIndex = fingerIndex;
    finger.position = this.GetPointerPosition(fingerIndex);
    finger.deltaPosition = finger.position - this.oldMousePosition[fingerIndex];
    finger.tapCount = this.tapCount[fingerIndex];
    finger.deltaTime = Time.realtimeSinceStartup - this.deltaTime[fingerIndex];
    finger.phase = TouchPhase.Ended;
    this.oldMousePosition[fingerIndex] = finger.position;
    return finger;
  }

  public Vector2 GetSecondFingerPosition()
  {
    Vector2 vector2 = new Vector2(-1f, -1f);
    if ((Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(EasyTouch.instance.twistKey)) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(EasyTouch.instance.swipeKey)))
    {
      if (!this.bComplex)
      {
        this.bComplex = true;
        this.deltaFingerPosition = (Vector2) Input.mousePosition - this.oldFinger2Position;
      }
      return this.GetComplex2finger();
    }
    if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(EasyTouch.instance.twistKey))
    {
      Vector2 pinchTwist2Finger = this.GetPinchTwist2Finger(false);
      this.bComplex = false;
      return pinchTwist2Finger;
    }
    if (!Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(EasyTouch.instance.swipeKey))
      return vector2;
    Vector2 complex2finger = this.GetComplex2finger();
    this.bComplex = false;
    return complex2finger;
  }

  private Vector2 GetPointerPosition(int index)
  {
    if (index == 0)
      return (Vector2) Input.mousePosition;
    return this.GetSecondFingerPosition();
  }

  private Vector2 GetPinchTwist2Finger(bool newSim = false)
  {
    Vector2 vector2;
    if (this.complexCenter == Vector2.zero)
    {
      vector2.x = (float) ((double) Screen.width / 2.0 - ((double) Input.mousePosition.x - (double) Screen.width / 2.0));
      vector2.y = (float) ((double) Screen.height / 2.0 - ((double) Input.mousePosition.y - (double) Screen.height / 2.0));
    }
    else
    {
      vector2.x = this.complexCenter.x - (Input.mousePosition.x - this.complexCenter.x);
      vector2.y = this.complexCenter.y - (Input.mousePosition.y - this.complexCenter.y);
    }
    this.oldFinger2Position = vector2;
    return vector2;
  }

  private Vector2 GetComplex2finger()
  {
    Vector2 vector2;
    vector2.x = Input.mousePosition.x - this.deltaFingerPosition.x;
    vector2.y = Input.mousePosition.y - this.deltaFingerPosition.y;
    this.complexCenter = new Vector2((float) (((double) Input.mousePosition.x + (double) vector2.x) / 2.0), (float) (((double) Input.mousePosition.y + (double) vector2.y) / 2.0));
    this.oldFinger2Position = vector2;
    return vector2;
  }
}
