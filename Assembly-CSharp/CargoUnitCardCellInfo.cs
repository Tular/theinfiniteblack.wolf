﻿// Decompiled with JetBrains decompiler
// Type: CargoUnitCardCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public class CargoUnitCardCellInfo : UnitCardCellInfoBase<CargoEntity, CargoUnitCardCell>
{
  public CargoUnitCardCellInfo(Transform prefab, CargoEntity data)
    : base(prefab, data)
  {
  }

  protected override void OnUpdateCellVisuals()
  {
    base.OnUpdateCellVisuals();
    switch (this.mCellData.Type)
    {
      case EntityType.CARGO_MONEY:
        this.mCellInstance.SetCargoMoney(((CargoMoney) this.mCellData).BlackDollars > 0);
        break;
      case EntityType.CARGO_ITEM:
        this.mCellInstance.SetCargoItem();
        break;
      case EntityType.CARGO_RESOURCE:
        this.mCellInstance.SetCargoResource(((CargoResource) this.mCellData).Resource);
        break;
      default:
        throw new Exception("Invalid cargo");
    }
  }
}
