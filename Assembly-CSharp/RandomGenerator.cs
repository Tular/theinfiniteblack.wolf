﻿// Decompiled with JetBrains decompiler
// Type: RandomGenerator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

public class RandomGenerator
{
  private const uint B = 1842502087;
  private const uint C = 1357980759;
  private const uint D = 273326509;
  private static uint counter;
  private uint a;
  private uint b;
  private uint c;
  private uint d;

  public RandomGenerator(uint val)
  {
    this.SetSeed(val);
  }

  public RandomGenerator()
  {
    this.SetSeed(RandomGenerator.counter++);
  }

  public uint GenerateUint()
  {
    uint num = this.a ^ this.a << 11;
    this.a = this.b;
    this.b = this.c;
    this.c = this.d;
    return this.d = (uint) ((int) this.d ^ (int) (this.d >> 19) ^ ((int) num ^ (int) (num >> 8)));
  }

  public int Range(int max)
  {
    return (int) ((long) this.GenerateUint() % (long) max);
  }

  public int Range(int min, int max)
  {
    return min + (int) ((long) this.GenerateUint() % (long) (max - min));
  }

  public float Range(float min, float max)
  {
    return min + (max - min) * this.GenerateFloat();
  }

  public float value
  {
    get
    {
      return 2.328306E-10f * (float) this.GenerateUint();
    }
  }

  public float GenerateFloat()
  {
    return 2.328306E-10f * (float) this.GenerateUint();
  }

  public float GenerateRangeFloat()
  {
    return 4.656613E-10f * (float) (int) this.GenerateUint();
  }

  public double GenerateDouble()
  {
    return 2.3283064370808E-10 * (double) this.GenerateUint();
  }

  public double GenerateRangeDouble()
  {
    return 4.65661287416159E-10 * (double) (int) this.GenerateUint();
  }

  public void SetSeed(uint val)
  {
    this.a = val;
    this.b = val ^ 1842502087U;
    this.c = val >> 5 ^ 1357980759U;
    this.d = val >> 7 ^ 273326509U;
    for (uint index = 0; index < 4U; ++index)
      this.a = this.GenerateUint();
  }
}
