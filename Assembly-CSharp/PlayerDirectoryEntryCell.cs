﻿// Decompiled with JetBrains decompiler
// Type: PlayerDirectoryEntryCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using UnityEngine;

[RequireComponent(typeof (UIWidget))]
public class PlayerDirectoryEntryCell : SBUIButton, IScrollViewCell
{
  public UILabel label;
  public UISprite background;
  public UISprite headBackground;
  public UISprite listHeadDecoration;
  private bool _isHead;
  private UIWidget _widget;
  private SBUIButton _button;

  public bool isHead
  {
    get
    {
      return this._isHead;
    }
    set
    {
      this._isHead = value;
      this.background.enabled = !this._isHead || !(bool) ((Object) this.headBackground);
      if ((bool) ((Object) this.headBackground))
        this.headBackground.enabled = this._isHead;
      if (!(bool) ((Object) this.listHeadDecoration))
        return;
      this.listHeadDecoration.enabled = this._isHead;
    }
  }

  public void SetSpriteDepth(int baseDepth)
  {
    if ((bool) ((Object) this.headBackground))
      this.headBackground.depth = baseDepth - 1;
    this.background.depth = baseDepth;
    if ((bool) ((Object) this.listHeadDecoration))
      this.listHeadDecoration.depth = baseDepth + 1;
    this.label.depth = baseDepth + 2;
  }

  public Vector3 extents
  {
    get
    {
      if (!(bool) ((Object) this._widget))
        this._widget = this.GetComponent<UIWidget>();
      return new Vector3((float) this._widget.width / 2f, (float) this._widget.height / 2f, 0.0f);
    }
  }

  public void ResetVisuals()
  {
    this.label.text = string.Empty;
    this.isHead = false;
    this.onClick.Clear();
    this.onLongPress.Clear();
  }

  protected override void OnInit()
  {
    base.OnInit();
    this._widget = this.GetComponent<UIWidget>();
    this._button = this.GetComponent<SBUIButton>();
  }

  protected virtual void OnDrag()
  {
    this.CancelPress();
  }

  private void OnDespawned()
  {
    this.onClick.Clear();
    this.onLongPress.Clear();
    this.isHead = false;
  }

  public enum BackgroundType
  {
    Default,
    ListHead,
    ListTail,
  }
}
