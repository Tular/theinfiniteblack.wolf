﻿// Decompiled with JetBrains decompiler
// Type: UIImageButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/UI/Image Button")]
public class UIImageButton : MonoBehaviour
{
  public bool pixelSnap = true;
  public UISprite target;
  public string normalSprite;
  public string hoverSprite;
  public string pressedSprite;
  public string disabledSprite;

  public bool isEnabled
  {
    get
    {
      Collider component = this.gameObject.GetComponent<Collider>();
      if ((bool) ((Object) component))
        return component.enabled;
      return false;
    }
    set
    {
      Collider component = this.gameObject.GetComponent<Collider>();
      if (!(bool) ((Object) component) || component.enabled == value)
        return;
      component.enabled = value;
      this.UpdateImage();
    }
  }

  private void OnEnable()
  {
    if ((Object) this.target == (Object) null)
      this.target = this.GetComponentInChildren<UISprite>();
    this.UpdateImage();
  }

  private void OnValidate()
  {
    if (!((Object) this.target != (Object) null))
      return;
    if (string.IsNullOrEmpty(this.normalSprite))
      this.normalSprite = this.target.spriteName;
    if (string.IsNullOrEmpty(this.hoverSprite))
      this.hoverSprite = this.target.spriteName;
    if (string.IsNullOrEmpty(this.pressedSprite))
      this.pressedSprite = this.target.spriteName;
    if (!string.IsNullOrEmpty(this.disabledSprite))
      return;
    this.disabledSprite = this.target.spriteName;
  }

  private void UpdateImage()
  {
    if (!((Object) this.target != (Object) null))
      return;
    if (this.isEnabled)
      this.SetSprite(!UICamera.IsHighlighted(this.gameObject) ? this.normalSprite : this.hoverSprite);
    else
      this.SetSprite(this.disabledSprite);
  }

  private void OnHover(bool isOver)
  {
    if (!this.isEnabled || !((Object) this.target != (Object) null))
      return;
    this.SetSprite(!isOver ? this.normalSprite : this.hoverSprite);
  }

  private void OnPress(bool pressed)
  {
    if (pressed)
      this.SetSprite(this.pressedSprite);
    else
      this.UpdateImage();
  }

  private void SetSprite(string sprite)
  {
    if ((Object) this.target.atlas == (Object) null || this.target.atlas.GetSprite(sprite) == null)
      return;
    this.target.spriteName = sprite;
    if (!this.pixelSnap)
      return;
    this.target.MakePixelPerfect();
  }
}
