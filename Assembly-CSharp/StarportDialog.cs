﻿// Decompiled with JetBrains decompiler
// Type: StarportDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;

public class StarportDialog : DialogBase
{
  public UILabel title;
  public ResourceMarketView resourceMarketView;
  public ItemMarketView itemMarketView;
  private StarportDialogContext _dialogContext;

  public void ShowItemMarketView()
  {
    this.title.text = "StarPort Item Market";
    NGUITools.SetActiveSelf(this.resourceMarketView.gameObject, false);
    this.itemMarketView.Show();
  }

  public void ShowResourceMarketView()
  {
    this.title.text = "StarPort Resource Market";
    this.itemMarketView.Hide();
    NGUITools.SetActiveSelf(this.resourceMarketView.gameObject, true);
    this.resourceMarketView.UpdateVisuals();
  }

  public void DoStarportRepair()
  {
    TibProxy.gameState.DoRepairWith(this._dialogContext.starport);
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
    TibProxy.Instance.onEntityExitEvent -= new EventHandler<EntityExitEventArgs>(this.TibProxy_onEntityExitEvent);
    TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this._dialogContext = new StarportDialogContext();
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
    TibProxy.Instance.onShowEntityDetailEvent += new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  protected override void OnCleanup()
  {
    base.OnCleanup();
    TibProxy.Instance.onShowEntityDetailEvent -= new EventHandler<ShowEntityDetailEventArgs>(this.TibProxy_onShowEntityDetailEvent);
  }

  protected override void OnShow()
  {
    base.OnShow();
    this.resourceMarketView.context = this._dialogContext;
    this.itemMarketView.context = this._dialogContext;
    this.ShowResourceMarketView();
    TibProxy.Instance.onEntityExitEvent -= new EventHandler<EntityExitEventArgs>(this.TibProxy_onEntityExitEvent);
    TibProxy.Instance.onEntityExitEvent += new EventHandler<EntityExitEventArgs>(this.TibProxy_onEntityExitEvent);
    TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
    TibProxy.Instance.onLocationChangedEvent += new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
  }

  protected override void OnHide()
  {
    base.OnHide();
    this._dialogContext.starport = (StarPort) null;
    TibProxy.Instance.onEntityExitEvent -= new EventHandler<EntityExitEventArgs>(this.TibProxy_onEntityExitEvent);
    TibProxy.Instance.onLocationChangedEvent -= new EventHandler<LocationChangedEventArgs>(this.TibProxy_onLocationChangedEvent);
  }

  private void TibProxy_onLocationChangedEvent(object sender, LocationChangedEventArgs e)
  {
    if (e.get_Entites().Contains((Entity) this._dialogContext.starport))
      return;
    this.Hide();
  }

  private void TibProxy_onShowEntityDetailEvent(object sender, ShowEntityDetailEventArgs e)
  {
    if (e.Source.Type != EntityType.STARPORT)
      return;
    this._dialogContext.starport = (StarPort) e.Source;
    this.Show();
  }

  private void TibProxy_onEntityExitEvent(object sender, EntityExitEventArgs e)
  {
    if (e.Source.Type != EntityType.STARPORT || e.Source != this._dialogContext.starport)
      return;
    this.Hide();
  }
}
