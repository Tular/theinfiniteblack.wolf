﻿// Decompiled with JetBrains decompiler
// Type: IAuctionFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Client;

public interface IAuctionFilter
{
  string formattedName { get; }

  IEnumerable<ClientAuction> GetFilteredAuctions(IEnumerable<ClientAuction> auctions);

  void ResetFilter();
}
