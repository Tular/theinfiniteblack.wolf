﻿// Decompiled with JetBrains decompiler
// Type: GlobalEasyTouchEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class GlobalEasyTouchEvent : MonoBehaviour
{
  public Text statText;

  private void OnEnable()
  {
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.On_TouchUp);
    EasyTouch.On_OverUIElement += new EasyTouch.OverUIElementHandler(this.On_OverUIElement);
    EasyTouch.On_UIElementTouchUp += new EasyTouch.UIElementTouchUpHandler(this.On_UIElementTouchUp);
  }

  private void OnDestroy()
  {
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.On_TouchUp);
    EasyTouch.On_OverUIElement -= new EasyTouch.OverUIElementHandler(this.On_OverUIElement);
    EasyTouch.On_UIElementTouchUp -= new EasyTouch.UIElementTouchUpHandler(this.On_UIElementTouchUp);
  }

  private void On_TouchDown(Gesture gesture)
  {
    this.statText.transform.SetAsFirstSibling();
    if ((Object) gesture.pickedUIElement != (Object) null)
      this.statText.text = "You touch UI Element : " + gesture.pickedUIElement.name + " (from gesture event)";
    if (!gesture.isOverGui && (Object) gesture.pickedObject == (Object) null)
      this.statText.text = "You touch an empty area";
    if (!((Object) gesture.pickedObject != (Object) null) || gesture.isOverGui)
      return;
    this.statText.text = "You touch a 3D Object";
  }

  private void On_OverUIElement(Gesture gesture)
  {
    this.statText.text = "You touch UI Element : " + gesture.pickedUIElement.name + " (from On_OverUIElement event)";
  }

  private void On_UIElementTouchUp(Gesture gesture)
  {
    this.statText.text = string.Empty;
  }

  private void On_TouchUp(Gesture gesture)
  {
    this.statText.text = string.Empty;
  }
}
