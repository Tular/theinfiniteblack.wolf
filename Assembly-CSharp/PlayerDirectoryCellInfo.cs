﻿// Decompiled with JetBrains decompiler
// Type: PlayerDirectoryCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class PlayerDirectoryCellInfo : ScrollViewCellInfoBase<ClientPlayer, PlayerDirectoryEntryCell>
{
  private int _cellOrder;
  private string _formattedText;
  public System.Action<PlayerDirectoryCellInfo> onCellClickedAction;

  public PlayerDirectoryCellInfo(Transform prefab, string headingText, string chatAddress)
    : this(prefab, headingText)
  {
    this.address = chatAddress;
  }

  public PlayerDirectoryCellInfo(Transform prefab, string headingText)
    : base(prefab, (ClientPlayer) null)
  {
    this.text = headingText;
    this._formattedText = headingText;
    this.isHeading = true;
  }

  public PlayerDirectoryCellInfo(Transform prefab, ClientPlayer data)
    : base(prefab, data)
  {
    this.text = data.Name;
    this.address = data.Name;
    this._formattedText = PlayerDirectoryCellInfo.GetFormattedName(data);
  }

  public ClientPlayer player
  {
    get
    {
      return this.mCellData;
    }
  }

  public string address { get; private set; }

  public bool isHeading { get; private set; }

  public string text { get; private set; }

  public int cellOrder
  {
    get
    {
      return this._cellOrder;
    }
    set
    {
      this._cellOrder = value;
      if (!((UnityEngine.Object) this.mCellInstance != (UnityEngine.Object) null))
        return;
      this.mCellInstance.SetSpriteDepth(this._cellOrder);
      this.mCellInstance.isHead = this.cellOrder == 0;
    }
  }

  public void SetDisplayText(string txt, string chatAddress)
  {
    this._formattedText = txt;
    this.address = chatAddress;
  }

  public void SetDisplayText(string txt)
  {
    this._formattedText = txt;
  }

  protected override void SetVisuals(PlayerDirectoryEntryCell cell)
  {
    cell.isHead = this.isHeading && this.cellOrder == 0;
    cell.label.text = this._formattedText;
    cell.SetSpriteDepth(this.cellOrder);
    EventDelegate.Add(cell.onClick, new EventDelegate.Callback(this.OnCellClicked));
    if (this.player == null)
      return;
    EventDelegate.Add(cell.onLongPress, new EventDelegate.Callback(this.OnCellLongPress));
  }

  protected void OnCellClicked()
  {
    if (this.onCellClickedAction == null)
      return;
    this.onCellClickedAction(this);
  }

  protected void OnCellLongPress()
  {
    if (TibProxy.Instance == null || TibProxy.gameState == null || (!TibProxy.gameState.IsLoggedIn || this.player == null))
      return;
    string text = ":who " + this.player.Name;
    TheInfiniteBlack.Library.Log.D((object) this, "OnCellLongPress()", "Sending chat command: " + text);
    TibProxy.gameState.DoChat(text, ChatType.SECTOR);
  }

  private static string GetFormattedName(ClientPlayer p)
  {
    StringBuilder stringBuilder = new StringBuilder(256);
    p.AppendName(stringBuilder, true);
    Markup.GetNGUI(stringBuilder);
    stringBuilder.Append("[-][-][-][/b][/i][/u][/s]");
    return stringBuilder.ToString();
  }
}
