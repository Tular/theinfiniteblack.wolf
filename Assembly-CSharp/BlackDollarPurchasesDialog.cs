﻿// Decompiled with JetBrains decompiler
// Type: BlackDollarPurchasesDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class BlackDollarPurchasesDialog : DialogBase
{
  public void OpenBlackDollarPurchasePage()
  {
    Application.OpenURL(string.Format("https://spellbook.com/purchase/tib/blackdollars.php?playername={0}&mobile=1", (object) TibProxy.gameState.MyPlayer.Name));
  }

  public void PurchaseTenBlackDollars()
  {
    PreInstantiatedSingleton<InAppPurchaseManager>.Instance.PurchaseTenBlackDollars();
  }

  public void PurchaseOneHundredBlackDollars()
  {
    PreInstantiatedSingleton<InAppPurchaseManager>.Instance.PurchaseOneHundredBlackDollars();
  }

  public void PurchaseFiveHundredBlackDollars()
  {
    PreInstantiatedSingleton<InAppPurchaseManager>.Instance.PurchaseFiveHundredBlackDollars();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
