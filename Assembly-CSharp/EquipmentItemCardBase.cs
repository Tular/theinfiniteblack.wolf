﻿// Decompiled with JetBrains decompiler
// Type: EquipmentItemCardBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public abstract class EquipmentItemCardBase : GeneralScrollViewCardBase
{
  public string resourcePath = "items";
  public UITexture itemIcon;
  public UIToggle toggle;
  protected UIWidget mWidget;
  protected bool mIsEquipped;

  public ItemViewContext viewContext { get; set; }

  public bool isToggle
  {
    get
    {
      return this.toggle.enabled;
    }
    set
    {
      if (!(bool) ((Object) this.toggle))
        this.toggle = this.gameObject.AddComponent<UIToggle>();
      this.toggle.enabled = value;
    }
  }

  public bool toggleOptionCanBeNone
  {
    get
    {
      return this.toggle.optionCanBeNone;
    }
    set
    {
      if (!(bool) ((Object) this.toggle))
        this.toggle = this.gameObject.AddComponent<UIToggle>();
      this.toggle.optionCanBeNone = value;
    }
  }

  public EquipmentItem item { get; protected set; }

  public virtual bool isEquipped
  {
    get
    {
      return this.mIsEquipped;
    }
    set
    {
      if (value)
        this.gameObject.name = "_" + this.item.LongName;
      else
        this.gameObject.name = this.item.LongName;
      this.mIsEquipped = value;
    }
  }

  public virtual void InitializeCard(EquipmentItem itm, ItemViewContext ctx)
  {
    this.viewContext = ctx;
    this.InitializeCard(itm);
  }

  public virtual void InitializeCard(EquipmentItem itm)
  {
    this.item = itm;
    this.name = this.item.LongName;
  }

  public override void Refresh()
  {
    this.UpdateVisuals();
  }

  protected virtual void UpdateVisuals()
  {
    this.LoadItemTexture();
  }

  public virtual void AuctionItem()
  {
    DialogManager.ShowDialog<CreateAuctionDialog>(new object[1]
    {
      (object) this.item
    });
  }

  public virtual void ItemToBank()
  {
  }

  public virtual void ItemToInventory()
  {
  }

  public virtual void ItemToGarrison()
  {
  }

  public virtual void EquipItem()
  {
  }

  public virtual void UnEquipItem()
  {
  }

  public virtual void UpgradeItem()
  {
  }

  public virtual void JettisonItem()
  {
  }

  public virtual void BuyItem()
  {
  }

  public virtual void SellItem()
  {
  }

  protected void LoadItemTexture()
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) this.item.Icon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.itemIcon.mainTexture = (Texture) texture2D;
  }

  protected void UnloadItemTexture()
  {
    Texture mainTexture = this.itemIcon.mainTexture;
    this.itemIcon.mainTexture = (Texture) null;
    Resources.UnloadAsset((Object) mainTexture);
  }

  protected virtual void Awake()
  {
    this.mWidget = this.GetComponent<UIWidget>();
    this.toggle = this.GetComponent<UIToggle>();
    if ((bool) ((Object) this.toggle))
      return;
    this.toggle = this.gameObject.AddComponent<UIToggle>();
  }

  protected virtual void OnSpawned()
  {
    this.LogD(string.Format("Spawning - {0}", (object) this.name));
  }

  protected virtual void OnDespawned()
  {
    this.LogD(string.Format("Despawning - {0}", (object) this.name));
    this.name = "EquipmentItemCard (Clone)";
    this.item = (EquipmentItem) null;
    this.mIsEquipped = false;
    this.UnloadItemTexture();
  }
}
