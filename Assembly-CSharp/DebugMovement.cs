﻿// Decompiled with JetBrains decompiler
// Type: DebugMovement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class DebugMovement : MonoBehaviour
{
  public float momentumAmount = 35f;
  public Vector3 momentum;
  public DebugMovement.GestureInfo startTouch;
  public DebugMovement.GestureInfo swipeStart;
  public DebugMovement.GestureInfo swipe;
  public DebugMovement.GestureInfo endSwipe;

  private void RegisterTouchHandlers()
  {
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.On_SwipeStart);
    EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
  }

  private void UnRegisterTouchHandlers()
  {
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.On_SwipeStart);
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
  }

  public void On_TouchStart(Gesture gesture)
  {
    this.startTouch.Initialize(gesture);
  }

  public void On_SwipeStart(Gesture gesture)
  {
    this.swipeStart.Initialize(gesture);
  }

  public void On_Swipe(Gesture gesture)
  {
    this.swipe.Initialize(gesture);
    this.momentum = Vector3.Lerp(this.momentum, this.momentum + new Vector3(gesture.deltaPosition.x, gesture.deltaPosition.y) * (0.01f + this.momentumAmount), 0.67f);
  }

  public void On_SwipeEnd(Gesture gesture)
  {
    this.endSwipe.Initialize(gesture);
  }

  protected void Update()
  {
    this.startTouch.Update();
    this.swipeStart.Update();
    this.swipe.Update();
    this.endSwipe.Update();
  }

  protected void Start()
  {
  }

  protected void OnEnable()
  {
    this.RegisterTouchHandlers();
  }

  protected void OnDisable()
  {
    this.UnRegisterTouchHandlers();
  }

  protected void OnDestroy()
  {
    this.UnRegisterTouchHandlers();
  }

  [Serializable]
  public class GestureInfo : IEquatable<DebugMovement.GestureInfo>
  {
    public Vector2 touchStartPosition;
    public Vector2 touchPosition;
    public Vector2 normalizedPosition;
    public Vector2 deltaPosition;
    public float actionTime;
    public float deltaTime;
    public float swipeLength;
    public Vector2 swipeVector;
    private Gesture _gesture;

    public bool Equals(DebugMovement.GestureInfo other)
    {
      if (object.ReferenceEquals((object) null, (object) other))
        return false;
      if (object.ReferenceEquals((object) this, (object) other))
        return true;
      return object.Equals((object) this._gesture, (object) other._gesture);
    }

    public override bool Equals(object obj)
    {
      if (object.ReferenceEquals((object) null, obj))
        return false;
      if (object.ReferenceEquals((object) this, obj))
        return true;
      if (obj.GetType() != this.GetType())
        return false;
      return this.Equals((DebugMovement.GestureInfo) obj);
    }

    public override int GetHashCode()
    {
      if (this._gesture != null)
        return this._gesture.GetHashCode();
      return 0;
    }

    public void Update()
    {
      if (this._gesture == null)
        return;
      this.touchStartPosition = this._gesture.startPosition;
      this.touchPosition = this._gesture.position;
      this.normalizedPosition = this._gesture.NormalizedPosition();
      this.deltaPosition = this._gesture.deltaPosition;
      this.deltaTime = this._gesture.deltaTime;
      this.actionTime = this._gesture.actionTime;
      this.swipeLength = this._gesture.swipeLength;
      this.swipeVector = this._gesture.swipeVector;
    }

    public void Initialize(Gesture gesture)
    {
      this._gesture = gesture;
    }
  }
}
