﻿// Decompiled with JetBrains decompiler
// Type: DragPooledScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using UnityEngine;

[AdvancedInspector.AdvancedInspector]
public class DragPooledScrollView : MonoBehaviour
{
  [Inspect(0)]
  private UIScrollView _scrollView;

  private void OnPress(bool pressed)
  {
    if (!(bool) ((Object) this._scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this._scrollView.Press(pressed);
  }

  public void OnSpawned()
  {
    this._scrollView = NGUITools.FindInParents<UIScrollView>(this.transform);
    if (!(bool) ((Object) this._scrollView))
      this._scrollView = NGUITools.FindInParents<UIScrollView>(this.transform);
    if (!((Object) this._scrollView != (Object) null))
      return;
    UIDragScrollView component = this.GetComponent<UIDragScrollView>();
    if (!((Object) component != (Object) null))
      return;
    component.enabled = false;
  }

  private void OnDrag(Vector2 delta)
  {
    if (!(bool) ((Object) this._scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this._scrollView.Drag();
  }

  private void OnScroll(float delta)
  {
    if (!(bool) ((Object) this._scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this._scrollView.Scroll(delta);
  }
}
