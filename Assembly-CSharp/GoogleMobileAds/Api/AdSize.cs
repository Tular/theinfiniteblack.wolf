﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Api.AdSize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

namespace GoogleMobileAds.Api
{
  public class AdSize
  {
    public static readonly AdSize Banner = new AdSize(320, 50);
    public static readonly AdSize MediumRectangle = new AdSize(300, 250);
    public static readonly AdSize IABBanner = new AdSize(468, 60);
    public static readonly AdSize Leaderboard = new AdSize(728, 90);
    public static readonly AdSize SmartBanner = new AdSize(true);
    private bool isSmartBanner;
    private int width;
    private int height;

    public AdSize(int width, int height)
    {
      this.isSmartBanner = false;
      this.width = width;
      this.height = height;
    }

    private AdSize(bool isSmartBanner)
    {
      this.isSmartBanner = isSmartBanner;
      this.width = 0;
      this.height = 0;
    }

    public int Width
    {
      get
      {
        return this.width;
      }
    }

    public int Height
    {
      get
      {
        return this.height;
      }
    }

    public bool IsSmartBanner
    {
      get
      {
        return this.isSmartBanner;
      }
    }
  }
}
