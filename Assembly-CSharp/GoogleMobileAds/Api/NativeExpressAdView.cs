﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Api.NativeExpressAdView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Common;
using System;

namespace GoogleMobileAds.Api
{
  public class NativeExpressAdView
  {
    private INativeExpressAdClient client;

    public NativeExpressAdView(string adUnitId, AdSize adSize, AdPosition position)
    {
      this.client = GoogleMobileAdsClientFactory.BuildNativeExpressAdClient();
      this.client.CreateNativeExpressAdView(adUnitId, adSize, position);
      this.client.OnAdLoaded += (EventHandler<EventArgs>) ((sender, args) => this.OnAdLoaded((object) this, args));
      this.client.OnAdFailedToLoad += (EventHandler<AdFailedToLoadEventArgs>) ((sender, args) => this.OnAdFailedToLoad((object) this, args));
      this.client.OnAdOpening += (EventHandler<EventArgs>) ((sender, args) => this.OnAdOpening((object) this, args));
      this.client.OnAdClosed += (EventHandler<EventArgs>) ((sender, args) => this.OnAdClosed((object) this, args));
      this.client.OnAdLeavingApplication += (EventHandler<EventArgs>) ((sender, args) => this.OnAdLeavingApplication((object) this, args));
    }

    public event EventHandler<EventArgs> OnAdLoaded;

    public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

    public event EventHandler<EventArgs> OnAdOpening;

    public event EventHandler<EventArgs> OnAdClosed;

    public event EventHandler<EventArgs> OnAdLeavingApplication;

    public void LoadAd(AdRequest request)
    {
      this.client.LoadAd(request);
    }

    public void Hide()
    {
      this.client.HideNativeExpressAdView();
    }

    public void Show()
    {
      this.client.ShowNativeExpressAdView();
    }

    public void Destroy()
    {
      this.client.DestroyNativeExpressAdView();
    }
  }
}
