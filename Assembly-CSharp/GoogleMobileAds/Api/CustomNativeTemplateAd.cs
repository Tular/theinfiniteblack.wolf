﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Api.CustomNativeTemplateAd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Common;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleMobileAds.Api
{
  public class CustomNativeTemplateAd
  {
    private ICustomNativeTemplateClient client;

    internal CustomNativeTemplateAd(ICustomNativeTemplateClient client)
    {
      this.client = client;
    }

    public List<string> GetAvailableAssetNames()
    {
      return this.client.GetAvailableAssetNames();
    }

    public string GetCustomTemplateId()
    {
      return this.client.GetTemplateId();
    }

    public Texture2D GetTexture2D(string key)
    {
      byte[] imageByteArray = this.client.GetImageByteArray(key);
      if (imageByteArray == null)
        return (Texture2D) null;
      return Utils.GetTexture2DFromByteArray(imageByteArray);
    }

    public string GetText(string key)
    {
      return this.client.GetText(key);
    }

    public void PerformClick(string assetName)
    {
      this.client.PerformClick(assetName);
    }

    public void RecordImpression()
    {
      this.client.RecordImpression();
    }
  }
}
