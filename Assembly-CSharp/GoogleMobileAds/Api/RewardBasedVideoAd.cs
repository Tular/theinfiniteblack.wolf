﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Api.RewardBasedVideoAd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Common;
using System;

namespace GoogleMobileAds.Api
{
  public class RewardBasedVideoAd
  {
    private IRewardBasedVideoAdClient client;
    private static RewardBasedVideoAd instance;

    private RewardBasedVideoAd()
    {
      this.client = GoogleMobileAdsClientFactory.BuildRewardBasedVideoAdClient();
      this.client.CreateRewardBasedVideoAd();
      this.client.OnAdLoaded += (EventHandler<EventArgs>) ((sender, args) => this.OnAdLoaded((object) this, args));
      this.client.OnAdFailedToLoad += (EventHandler<AdFailedToLoadEventArgs>) ((sender, args) => this.OnAdFailedToLoad((object) this, args));
      this.client.OnAdOpening += (EventHandler<EventArgs>) ((sender, args) => this.OnAdOpening((object) this, args));
      this.client.OnAdStarted += (EventHandler<EventArgs>) ((sender, args) => this.OnAdStarted((object) this, args));
      this.client.OnAdRewarded += (EventHandler<Reward>) ((sender, args) => this.OnAdRewarded((object) this, args));
      this.client.OnAdClosed += (EventHandler<EventArgs>) ((sender, args) => this.OnAdClosed((object) this, args));
      this.client.OnAdLeavingApplication += (EventHandler<EventArgs>) ((sender, args) => this.OnAdLeavingApplication((object) this, args));
    }

    public event EventHandler<EventArgs> OnAdLoaded = (_param0, _param1) => {};

    public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad = (_param0, _param1) => {};

    public event EventHandler<EventArgs> OnAdOpening = (_param0, _param1) => {};

    public event EventHandler<EventArgs> OnAdStarted = (_param0, _param1) => {};

    public event EventHandler<EventArgs> OnAdClosed = (_param0, _param1) => {};

    public event EventHandler<Reward> OnAdRewarded = (_param0, _param1) => {};

    public event EventHandler<EventArgs> OnAdLeavingApplication = (_param0, _param1) => {};

    public static RewardBasedVideoAd Instance
    {
      get
      {
        if (RewardBasedVideoAd.instance == null)
          RewardBasedVideoAd.instance = new RewardBasedVideoAd();
        return RewardBasedVideoAd.instance;
      }
    }

    public void LoadAd(AdRequest request, string adUnitId)
    {
      this.client.LoadAd(request, adUnitId);
    }

    public bool IsLoaded()
    {
      return this.client.IsLoaded();
    }

    public void Show()
    {
      this.client.ShowRewardBasedVideoAd();
    }
  }
}
