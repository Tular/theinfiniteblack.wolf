﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Api.AdRequest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace GoogleMobileAds.Api
{
  public class AdRequest
  {
    public const string Version = "3.1.0";
    public const string TestDeviceSimulator = "SIMULATOR";

    private AdRequest(AdRequest.Builder builder)
    {
      this.TestDevices = new List<string>((IEnumerable<string>) builder.TestDevices);
      this.Keywords = new HashSet<string>((IEnumerable<string>) builder.Keywords);
      this.Birthday = builder.Birthday;
      this.Gender = builder.Gender;
      this.TagForChildDirectedTreatment = builder.ChildDirectedTreatmentTag;
      this.Extras = new Dictionary<string, string>((IDictionary<string, string>) builder.Extras);
    }

    public List<string> TestDevices { get; private set; }

    public HashSet<string> Keywords { get; private set; }

    public DateTime? Birthday { get; private set; }

    public GoogleMobileAds.Api.Gender? Gender { get; private set; }

    public bool? TagForChildDirectedTreatment { get; private set; }

    public Dictionary<string, string> Extras { get; private set; }

    public class Builder
    {
      public Builder()
      {
        this.TestDevices = new List<string>();
        this.Keywords = new HashSet<string>();
        this.Birthday = new DateTime?();
        this.Gender = new GoogleMobileAds.Api.Gender?();
        this.ChildDirectedTreatmentTag = new bool?();
        this.Extras = new Dictionary<string, string>();
      }

      internal List<string> TestDevices { get; private set; }

      internal HashSet<string> Keywords { get; private set; }

      internal DateTime? Birthday { get; private set; }

      internal GoogleMobileAds.Api.Gender? Gender { get; private set; }

      internal bool? ChildDirectedTreatmentTag { get; private set; }

      internal Dictionary<string, string> Extras { get; private set; }

      public AdRequest.Builder AddKeyword(string keyword)
      {
        this.Keywords.Add(keyword);
        return this;
      }

      public AdRequest.Builder AddTestDevice(string deviceId)
      {
        this.TestDevices.Add(deviceId);
        return this;
      }

      public AdRequest Build()
      {
        return new AdRequest(this);
      }

      public AdRequest.Builder SetBirthday(DateTime birthday)
      {
        this.Birthday = new DateTime?(birthday);
        return this;
      }

      public AdRequest.Builder SetGender(GoogleMobileAds.Api.Gender gender)
      {
        this.Gender = new GoogleMobileAds.Api.Gender?(gender);
        return this;
      }

      public AdRequest.Builder TagForChildDirectedTreatment(bool tagForChildDirectedTreatment)
      {
        this.ChildDirectedTreatmentTag = new bool?(tagForChildDirectedTreatment);
        return this;
      }

      public AdRequest.Builder AddExtra(string key, string value)
      {
        this.Extras.Add(key, value);
        return this;
      }
    }
  }
}
