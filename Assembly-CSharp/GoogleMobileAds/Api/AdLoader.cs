﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Api.AdLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Common;
using System;
using System.Collections.Generic;

namespace GoogleMobileAds.Api
{
  public class AdLoader
  {
    private IAdLoaderClient adLoaderClient;

    private AdLoader(AdLoader.Builder builder)
    {
      this.AdUnitId = string.Copy(builder.AdUnitId);
      this.CustomNativeTemplateClickHandlers = new Dictionary<string, Action<CustomNativeTemplateAd, string>>((IDictionary<string, Action<CustomNativeTemplateAd, string>>) builder.CustomNativeTemplateClickHandlers);
      this.TemplateIds = new HashSet<string>((IEnumerable<string>) builder.TemplateIds);
      this.AdTypes = new HashSet<NativeAdType>((IEnumerable<NativeAdType>) builder.AdTypes);
      this.adLoaderClient = GoogleMobileAdsClientFactory.BuildAdLoaderClient(this);
      this.adLoaderClient.OnCustomNativeTemplateAdLoaded += (EventHandler<CustomNativeEventArgs>) ((sender, args) => this.OnCustomNativeTemplateAdLoaded((object) this, args));
      this.adLoaderClient.OnAdFailedToLoad += (EventHandler<AdFailedToLoadEventArgs>) ((sender, args) => this.OnAdFailedToLoad((object) this, args));
    }

    public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

    public event EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded;

    public Dictionary<string, Action<CustomNativeTemplateAd, string>> CustomNativeTemplateClickHandlers { get; private set; }

    public string AdUnitId { get; private set; }

    public HashSet<NativeAdType> AdTypes { get; private set; }

    public HashSet<string> TemplateIds { get; private set; }

    public void LoadAd(AdRequest request)
    {
      this.adLoaderClient.LoadAd(request);
    }

    public class Builder
    {
      public Builder(string adUnitId)
      {
        this.AdUnitId = adUnitId;
        this.AdTypes = new HashSet<NativeAdType>();
        this.TemplateIds = new HashSet<string>();
        this.CustomNativeTemplateClickHandlers = new Dictionary<string, Action<CustomNativeTemplateAd, string>>();
      }

      internal string AdUnitId { get; private set; }

      internal HashSet<NativeAdType> AdTypes { get; private set; }

      internal HashSet<string> TemplateIds { get; private set; }

      internal Dictionary<string, Action<CustomNativeTemplateAd, string>> CustomNativeTemplateClickHandlers { get; private set; }

      public AdLoader.Builder ForCustomNativeAd(string templateId)
      {
        this.TemplateIds.Add(templateId);
        this.AdTypes.Add(NativeAdType.CustomTemplate);
        return this;
      }

      public AdLoader.Builder ForCustomNativeAd(string templateId, Action<CustomNativeTemplateAd, string> callback)
      {
        this.TemplateIds.Add(templateId);
        this.CustomNativeTemplateClickHandlers[templateId] = callback;
        this.AdTypes.Add(NativeAdType.CustomTemplate);
        return this;
      }

      public AdLoader Build()
      {
        return new AdLoader(this);
      }
    }
  }
}
