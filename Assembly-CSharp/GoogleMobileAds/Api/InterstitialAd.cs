﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Api.InterstitialAd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Common;
using System;

namespace GoogleMobileAds.Api
{
  public class InterstitialAd
  {
    private IInterstitialClient client;

    public InterstitialAd(string adUnitId)
    {
      this.client = GoogleMobileAdsClientFactory.BuildInterstitialClient();
      this.client.CreateInterstitialAd(adUnitId);
      this.client.OnAdLoaded += (EventHandler<EventArgs>) ((sender, args) => this.OnAdLoaded((object) this, args));
      this.client.OnAdFailedToLoad += (EventHandler<AdFailedToLoadEventArgs>) ((sender, args) => this.OnAdFailedToLoad((object) this, args));
      this.client.OnAdOpening += (EventHandler<EventArgs>) ((sender, args) => this.OnAdOpening((object) this, args));
      this.client.OnAdClosed += (EventHandler<EventArgs>) ((sender, args) => this.OnAdClosed((object) this, args));
      this.client.OnAdLeavingApplication += (EventHandler<EventArgs>) ((sender, args) => this.OnAdLeavingApplication((object) this, args));
    }

    public event EventHandler<EventArgs> OnAdLoaded = (_param0, _param1) => {};

    public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad = (_param0, _param1) => {};

    public event EventHandler<EventArgs> OnAdOpening = (_param0, _param1) => {};

    public event EventHandler<EventArgs> OnAdClosed = (_param0, _param1) => {};

    public event EventHandler<EventArgs> OnAdLeavingApplication = (_param0, _param1) => {};

    public void LoadAd(AdRequest request)
    {
      this.client.LoadAd(request);
    }

    public bool IsLoaded()
    {
      return this.client.IsLoaded();
    }

    public void Show()
    {
      this.client.ShowInterstitial();
    }

    public void Destroy()
    {
      this.client.DestroyInterstitial();
    }

    public void SetInAppPurchaseProcessor(IDefaultInAppPurchaseProcessor processor)
    {
      this.client.SetDefaultInAppPurchaseProcessor(processor);
    }

    public void SetInAppPurchaseProcessor(ICustomInAppPurchaseProcessor processor)
    {
      this.client.SetCustomInAppPurchaseProcessor(processor);
    }
  }
}
