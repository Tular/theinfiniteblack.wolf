﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.GoogleMobileAdsClientFactory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Api;
using GoogleMobileAds.Common;

namespace GoogleMobileAds
{
  internal class GoogleMobileAdsClientFactory
  {
    internal static IBannerClient BuildBannerClient()
    {
      return (IBannerClient) new DummyClient();
    }

    internal static IInterstitialClient BuildInterstitialClient()
    {
      return (IInterstitialClient) new DummyClient();
    }

    internal static IRewardBasedVideoAdClient BuildRewardBasedVideoAdClient()
    {
      return (IRewardBasedVideoAdClient) new DummyClient();
    }

    internal static IAdLoaderClient BuildAdLoaderClient(AdLoader adLoader)
    {
      return (IAdLoaderClient) new DummyClient();
    }

    internal static INativeExpressAdClient BuildNativeExpressAdClient()
    {
      return (INativeExpressAdClient) new DummyClient();
    }
  }
}
