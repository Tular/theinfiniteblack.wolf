﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Common.DummyClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Api;
using System;
using System.Reflection;
using UnityEngine;

namespace GoogleMobileAds.Common
{
  internal class DummyClient : IAdLoaderClient, IBannerClient, IInterstitialClient, INativeExpressAdClient, IRewardBasedVideoAdClient
  {
    public DummyClient()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public event EventHandler<EventArgs> OnAdLoaded;

    public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

    public event EventHandler<EventArgs> OnAdOpening;

    public event EventHandler<EventArgs> OnAdStarted;

    public event EventHandler<EventArgs> OnAdClosed;

    public event EventHandler<Reward> OnAdRewarded;

    public event EventHandler<EventArgs> OnAdLeavingApplication;

    public event EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded;

    public string UserId
    {
      get
      {
        Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
        return nameof (UserId);
      }
      set
      {
        Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
      }
    }

    public void CreateBannerView(string adUnitId, AdSize adSize, AdPosition position)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void LoadAd(AdRequest request)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void ShowBannerView()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void HideBannerView()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void DestroyBannerView()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void CreateInterstitialAd(string adUnitId)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public bool IsLoaded()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
      return true;
    }

    public void ShowInterstitial()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void DestroyInterstitial()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void CreateRewardBasedVideoAd()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void SetUserId(string userId)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void LoadAd(AdRequest request, string adUnitId)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void DestroyRewardBasedVideoAd()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void ShowRewardBasedVideoAd()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void SetDefaultInAppPurchaseProcessor(IDefaultInAppPurchaseProcessor processor)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void SetCustomInAppPurchaseProcessor(ICustomInAppPurchaseProcessor processor)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void CreateAdLoader(AdLoader.Builder builder)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void Load(AdRequest request)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void CreateNativeExpressAdView(string adUnitId, AdSize adSize, AdPosition position)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void SetAdSize(AdSize adSize)
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void ShowNativeExpressAdView()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void HideNativeExpressAdView()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }

    public void DestroyNativeExpressAdView()
    {
      Debug.Log((object) ("Dummy " + MethodBase.GetCurrentMethod().Name));
    }
  }
}
