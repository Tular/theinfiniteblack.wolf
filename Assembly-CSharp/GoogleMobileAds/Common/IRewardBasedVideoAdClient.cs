﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Common.IRewardBasedVideoAdClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Api;
using System;

namespace GoogleMobileAds.Common
{
  internal interface IRewardBasedVideoAdClient
  {
    event EventHandler<EventArgs> OnAdLoaded;

    event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

    event EventHandler<EventArgs> OnAdOpening;

    event EventHandler<EventArgs> OnAdStarted;

    event EventHandler<Reward> OnAdRewarded;

    event EventHandler<EventArgs> OnAdClosed;

    event EventHandler<EventArgs> OnAdLeavingApplication;

    void CreateRewardBasedVideoAd();

    void LoadAd(AdRequest request, string adUnitId);

    bool IsLoaded();

    void ShowRewardBasedVideoAd();
  }
}
