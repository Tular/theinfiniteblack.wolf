﻿// Decompiled with JetBrains decompiler
// Type: GoogleMobileAds.Common.IBannerClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using GoogleMobileAds.Api;
using System;

namespace GoogleMobileAds.Common
{
  internal interface IBannerClient
  {
    event EventHandler<EventArgs> OnAdLoaded;

    event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

    event EventHandler<EventArgs> OnAdOpening;

    event EventHandler<EventArgs> OnAdClosed;

    event EventHandler<EventArgs> OnAdLeavingApplication;

    void CreateBannerView(string adUnitId, AdSize adSize, AdPosition position);

    void LoadAd(AdRequest request);

    void ShowBannerView();

    void HideBannerView();

    void DestroyBannerView();
  }
}
