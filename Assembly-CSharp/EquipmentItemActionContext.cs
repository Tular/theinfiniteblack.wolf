﻿// Decompiled with JetBrains decompiler
// Type: EquipmentItemActionContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

public enum EquipmentItemActionContext
{
  None,
  InventorySelf,
  EquippedSelf,
  InventoryOther,
  EquippedOther,
  Bank,
  Starport,
  AuctionHouse,
  GarrisonBank,
}
