﻿// Decompiled with JetBrains decompiler
// Type: PortraitCacheInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using UnityEngine;

[AdvancedInspector.AdvancedInspector]
[Serializable]
public class PortraitCacheInfo
{
  [Inspect(0)]
  public int imageSize;
  [Inspect(1)]
  public int maxCacheSize;
  [Inspect(2)]
  public float cacheExpireTime;
  [Inspect(3)]
  public Texture2D defaultTexture;
}
