﻿// Decompiled with JetBrains decompiler
// Type: ItemSelectPopupContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Items;

public class ItemSelectPopupContext : IItemScrollViewContext, IGeneralScrollViewContext<EquipmentItem>
{
  private bool _hasChanged;
  private Func<List<EquipmentItem>> _getItemList;
  private EquipmentItem _selectedItem;
  private List<EquipmentItem> _itemListing;

  public bool hasChanged
  {
    get
    {
      if (this._getItemList == null)
        return this._itemListing == null;
      List<EquipmentItem> equipmentItemList = this.getItemList();
      if (!this._hasChanged)
        return !object.ReferenceEquals((object) equipmentItemList, (object) this._itemListing);
      return true;
    }
  }

  public IEnumerable<EquipmentItem> items
  {
    get
    {
      this._itemListing = this._getItemList != null ? this.getItemList() : new List<EquipmentItem>(1);
      this._hasChanged = false;
      return this._itemListing.Where<EquipmentItem>((Func<EquipmentItem, bool>) (itm => !itm.NoDrop));
    }
  }

  public ItemViewContext viewContext { get; private set; }

  public Func<List<EquipmentItem>> getItemList
  {
    get
    {
      return this._getItemList;
    }
    set
    {
      this._getItemList = value;
      this._hasChanged = true;
    }
  }

  public bool selectedItemChanged { get; private set; }

  public EquipmentItem selectedItem
  {
    get
    {
      return this._selectedItem;
    }
    set
    {
      this._selectedItem = value;
      this.selectedItemChanged = true;
    }
  }

  public void ResetContext()
  {
    this._selectedItem = (EquipmentItem) null;
    this._getItemList = (Func<List<EquipmentItem>>) null;
  }
}
