﻿// Decompiled with JetBrains decompiler
// Type: SpringPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIPanel))]
[AddComponentMenu("NGUI/Internal/Spring Panel")]
public class SpringPanel : MonoBehaviour
{
  public Vector3 target = Vector3.zero;
  public float strength = 10f;
  public static SpringPanel current;
  public SpringPanel.OnFinished onFinished;
  private UIPanel mPanel;
  private Transform mTrans;
  private UIScrollView mDrag;

  private void Start()
  {
    this.mPanel = this.GetComponent<UIPanel>();
    this.mDrag = this.GetComponent<UIScrollView>();
    this.mTrans = this.transform;
  }

  private void Update()
  {
    this.AdvanceTowardsPosition();
  }

  protected virtual void AdvanceTowardsPosition()
  {
    float deltaTime = RealTime.deltaTime;
    bool flag = false;
    Vector3 localPosition = this.mTrans.localPosition;
    Vector3 vector3_1 = NGUIMath.SpringLerp(this.mTrans.localPosition, this.target, this.strength, deltaTime);
    if ((double) (vector3_1 - this.target).sqrMagnitude < 0.00999999977648258)
    {
      vector3_1 = this.target;
      this.enabled = false;
      flag = true;
    }
    this.mTrans.localPosition = vector3_1;
    Vector3 vector3_2 = vector3_1 - localPosition;
    Vector2 clipOffset = this.mPanel.clipOffset;
    clipOffset.x -= vector3_2.x;
    clipOffset.y -= vector3_2.y;
    this.mPanel.clipOffset = clipOffset;
    if ((Object) this.mDrag != (Object) null)
      this.mDrag.UpdateScrollbars(false);
    if (!flag || this.onFinished == null)
      return;
    SpringPanel.current = this;
    this.onFinished();
    SpringPanel.current = (SpringPanel) null;
  }

  public static SpringPanel Begin(GameObject go, Vector3 pos, float strength)
  {
    SpringPanel springPanel = go.GetComponent<SpringPanel>();
    if ((Object) springPanel == (Object) null)
      springPanel = go.AddComponent<SpringPanel>();
    springPanel.target = pos;
    springPanel.strength = strength;
    springPanel.onFinished = (SpringPanel.OnFinished) null;
    springPanel.enabled = true;
    return springPanel;
  }

  public delegate void OnFinished();
}
