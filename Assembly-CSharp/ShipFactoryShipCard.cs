﻿// Decompiled with JetBrains decompiler
// Type: ShipFactoryShipCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using UnityEngine;

[RequireComponent(typeof (UIToggle))]
public class ShipFactoryShipCard : MonoBehaviour
{
  public string shipSpritePrefix = "entity_ship_";
  public UIToggle toggle;
  public UILabel title;
  public UILabel statusMessage;
  public UISprite shipIcon;
  public UISprite statusBackground;
  public UISprite borderSprite;
  public Color borderNormal;
  public Color borderSelected;
  public Color currentShip;
  public Color alreadyOwned;
  public Color avaliableForPurchase;
  public Color notAvaliable;
  public string notUnlockedMessage;
  public string notAvaliableMessage;
  public string alreadyOwnedMessage;
  public string currentShipMessage;
  private ShipClass _shipClass;
  private bool _canBuyWithCredits;
  private bool _canBuyWithBlackDollars;
  private bool _canBuyWithRewardPoints;
  private bool _canBuyWithCombatPoints;

  public ShipClass shipClass
  {
    get
    {
      return this._shipClass;
    }
    set
    {
      this._shipClass = value;
      this.shipIcon.spriteName = string.Format("{0}{1}", (object) this.shipSpritePrefix, (object) this._shipClass);
      this.title.text = this._shipClass.Name();
    }
  }

  private void Awake()
  {
    this.statusMessage.text = string.Empty;
  }

  private void Start()
  {
    NGUITools.SetActiveSelf(this.statusMessage.cachedTransform.parent.gameObject, false);
  }

  private void Update()
  {
    IGameState gameState = TibProxy.gameState;
    if (gameState == null)
      return;
    this.borderSprite.color = !this.toggle.value ? this.borderNormal : this.borderSelected;
    if (gameState.Ships[this.shipClass])
    {
      this._canBuyWithCredits = false;
      this._canBuyWithBlackDollars = false;
      this._canBuyWithRewardPoints = false;
      this._canBuyWithCombatPoints = false;
      NGUITools.SetActiveSelf(this.statusMessage.cachedTransform.parent.gameObject, true);
      if (TibProxy.gameState.MyShip.Class == this.shipClass)
      {
        this.statusMessage.text = this.currentShipMessage;
        this.statusBackground.color = this.currentShip;
      }
      else
      {
        this.statusMessage.text = this.alreadyOwnedMessage;
        this.statusBackground.color = this.alreadyOwned;
      }
    }
    else
    {
      NGUITools.SetActiveSelf(this.statusMessage.cachedTransform.parent.gameObject, false);
      this.statusBackground.color = this.avaliableForPurchase;
    }
  }
}
