﻿// Decompiled with JetBrains decompiler
// Type: UISettingsGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Unity;
using UnityEngine;

[ExecuteInEditMode]
public class UISettingsGroup : MonoBehaviour
{
  public string label;
  public UILabel headingLabel;
  public GameObject detailSelectorRoot;
  public UIToggle headingToggle;
  public Transform selectorRoot;
  public UISettingsDetailSelector detailSelectorPrefab;
  public bool doUpdate;
  public DetailSelectorInfo[] detailSelectors;
  protected UIWidget mWidget;
  protected List<UISettingsDetailSelector> selectors;
  public UISettingsDetailSelector activeSelector;

  public bool isSelected
  {
    get
    {
      return this.headingToggle.value;
    }
    set
    {
      if (this.headingToggle.value == value)
        return;
      this.headingToggle.value = value;
    }
  }

  protected void Awake()
  {
    this.selectors = new List<UISettingsDetailSelector>(10);
    this.mWidget = this.GetComponent<UIWidget>();
  }

  protected void Start()
  {
    this.doUpdate = false;
    this.headingLabel.text = this.label;
  }

  public void OnDisable()
  {
    this.headingToggle.activeSprite.alpha = 0.0f;
    this.isSelected = false;
  }

  public void BuildSelectors(DetailSelectorInfo[] detailInfo)
  {
    this.selectors.Clear();
    foreach (Component componentsInChild in this.detailSelectorRoot.GetComponentsInChildren<UISettingsDetailSelector>(true))
      NGUITools.DestroyImmediate((UnityEngine.Object) componentsInChild.gameObject);
    UITable uiTable = this.detailSelectorRoot.gameObject.AddComponent<UITable>();
    uiTable.columns = 1;
    uiTable.direction = UITable.Direction.Down;
    uiTable.sorting = UITable.Sorting.Alphabetic;
    uiTable.hideInactive = true;
    this.LogD(string.Format("-----creating selectors-----"));
    foreach (DetailSelectorInfo detailSelectorInfo in (IEnumerable<DetailSelectorInfo>) ((IEnumerable<DetailSelectorInfo>) detailInfo).OrderBy<DetailSelectorInfo, int>((Func<DetailSelectorInfo, int>) (s => s.sortOrder)))
    {
      UISettingsDetailSelector component = this.detailSelectorRoot.gameObject.AddChild(this.detailSelectorPrefab.gameObject).GetComponent<UISettingsDetailSelector>();
      component.Initialize();
      component.text = detailSelectorInfo.text;
      this.selectors.Add(component);
      component.UpdateAnchors();
      component.name = string.Format("{0} - {1}", (object) detailSelectorInfo.sortOrder, (object) detailSelectorInfo.text);
    }
    uiTable.Reposition();
    NGUITools.DestroyImmediate((UnityEngine.Object) uiTable);
  }

  protected void Update()
  {
    if (!Application.isEditor || Application.isPlaying || !this.doUpdate)
      return;
    this.doUpdate = false;
    this.headingLabel.text = this.label;
  }

  public void ResetDetailSelectors()
  {
    this.activeSelector = (UISettingsDetailSelector) null;
    if (this.selectors == null)
      return;
    this.selectors.ForEach((System.Action<UISettingsDetailSelector>) (s => s.isSelected = false));
  }

  public void OnToggleChanged()
  {
    if (this.isSelected)
      return;
    this.ResetDetailSelectors();
  }

  public void OnDetailSelectorChanged(UISettingsDetailSelector selector, bool value)
  {
    if ((UnityEngine.Object) this.activeSelector == (UnityEngine.Object) null || this.activeSelector.Equals((object) selector))
    {
      this.activeSelector = !value ? (UISettingsDetailSelector) null : selector;
    }
    else
    {
      if (!value)
        return;
      this.activeSelector.isSelected = false;
      this.activeSelector = selector;
    }
  }
}
