﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseFilterView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;

public class AuctionHouseFilterView : DialogViewBase<AuctionHouseDialogContext>
{
  private ItemRarity _selectedRarity;
  private ItemType _selectedItemType;

  public void OnWeaponSelected()
  {
    this._selectedItemType = ItemType.WEAPON;
  }

  public void OnArmorSelected()
  {
    this._selectedItemType = ItemType.ARMOR;
  }

  public void OnStorageSelected()
  {
    this._selectedItemType = ItemType.STORAGE;
  }

  public void OnEngineSelected()
  {
    this._selectedItemType = ItemType.ENGINE;
  }

  public void OnComputerSelected()
  {
    this._selectedItemType = ItemType.COMPUTER;
  }

  public void OnHarvesterSelected()
  {
    this._selectedItemType = ItemType.HARVESTER;
  }

  public void OnSpecialSelected()
  {
    this._selectedItemType = ItemType.SPECIAL;
  }

  public void OnUncommonSelected()
  {
    this._selectedRarity = ItemRarity.UNCOMMON;
  }

  public void OnRareSelected()
  {
    this._selectedRarity = ItemRarity.RARE;
  }

  public void OnUltraRareSelected()
  {
    this._selectedRarity = ItemRarity.ULTRA_RARE;
  }

  public void OnLegendarySelected()
  {
    this._selectedRarity = ItemRarity.LEGENDARY;
  }

  public void OnPrecursorSelected()
  {
    this._selectedRarity = ItemRarity.PRECURSOR;
  }

  public void OnUltimateSelected()
  {
    this._selectedRarity = ItemRarity.ULTIMATE;
  }

  public void OnSubmitRarityTypeQuery()
  {
    if (this._selectedRarity == ItemRarity.NULL || this._selectedItemType == ItemType.NULL)
      return;
    this.mDialogContext.filter = (IAuctionFilter) new TypeRarityFilter()
    {
      rarity = this._selectedRarity,
      type = this._selectedItemType
    };
    this.mDialogContext.dialog.ShowFilteredItemListing();
  }
}
