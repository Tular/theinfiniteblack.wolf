﻿// Decompiled with JetBrains decompiler
// Type: DragTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DragTester : MonoBehaviour
{
  public float fpsUpdateInterval = 0.5f;
  public SWPSDrag dragObject;
  public UILabel fps;
  public UILabel vsync;
  public UILabel dragUpdateStyle;
  public UILabel targetFrameRate;
  public UITextList easyTouchOutput;
  private float _fpsAccum;
  private int _fpsFrames;
  private float _fpsTimeLeft;

  public void ToggleVsync()
  {
    if (QualitySettings.vSyncCount == 0)
      QualitySettings.vSyncCount = 1;
    else if (QualitySettings.vSyncCount == 1)
      QualitySettings.vSyncCount = 2;
    else
      QualitySettings.vSyncCount = 0;
  }

  public void ToggleUpdateStyle()
  {
    switch (this.dragObject.moveType)
    {
      case SWPSDrag.MoveType.Update:
        this.dragObject.moveType = SWPSDrag.MoveType.FixedUpdate;
        break;
      case SWPSDrag.MoveType.LateUpdate:
        this.dragObject.moveType = SWPSDrag.MoveType.Update;
        break;
      case SWPSDrag.MoveType.FixedUpdate:
        this.dragObject.moveType = SWPSDrag.MoveType.LateUpdate;
        break;
    }
  }

  [ContextMenu("Enable Vsync")]
  public void EnableVsync()
  {
    QualitySettings.vSyncCount = 1;
  }

  [ContextMenu("Enable Half Vsync")]
  public void EnableHalfVsync()
  {
    QualitySettings.vSyncCount = 2;
  }

  [ContextMenu("Disable Vsync")]
  public void DisableVsync()
  {
    QualitySettings.vSyncCount = 0;
  }

  public void ClearEasyTouchOutput()
  {
    if (!(bool) ((Object) this.easyTouchOutput))
      return;
    this.easyTouchOutput.Clear();
  }

  private void Awake()
  {
    QualitySettings.vSyncCount = 1;
    Application.targetFrameRate = 10;
  }

  private void Start()
  {
    this._fpsTimeLeft = this.fpsUpdateInterval;
  }

  private void Update()
  {
    this._fpsTimeLeft -= Time.deltaTime;
    this._fpsAccum += Time.timeScale / Time.deltaTime;
    ++this._fpsFrames;
    if ((double) this._fpsTimeLeft <= 0.0)
    {
      this.fps.text = string.Format("{0:f2}", (object) (float) ((double) this._fpsAccum / (double) this._fpsFrames));
      this._fpsTimeLeft = this.fpsUpdateInterval;
      this._fpsAccum = 0.0f;
      this._fpsFrames = 0;
    }
    this.vsync.text = string.Format("{0}", (object) QualitySettings.vSyncCount);
    this.targetFrameRate.text = string.Format("{0}", (object) Application.targetFrameRate);
    this.dragUpdateStyle.text = string.Format("{0}", (object) this.dragObject.moveType);
  }
}
