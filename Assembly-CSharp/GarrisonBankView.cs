﻿// Decompiled with JetBrains decompiler
// Type: GarrisonBankView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class GarrisonBankView : MonoBehaviour
{
  private readonly List<EquipmentItem> _currentItemList = new List<EquipmentItem>(50);
  public GeneralEquipmentItemScrollView itemScrollView;
  private EquipmentItemActionContext _itemActionContext;
  private int _prevItemCount;

  public GarrisonDialogContext dialogContext { get; set; }

  public void ShowShipInventory()
  {
    this._itemActionContext = EquipmentItemActionContext.InventorySelf;
  }

  public void ShowGarrisonBank()
  {
    this._itemActionContext = EquipmentItemActionContext.GarrisonBank;
  }

  private void OnEnable()
  {
    this._currentItemList.Clear();
  }

  private void OnDisable()
  {
    this._currentItemList.Clear();
  }

  private void Update()
  {
    this._currentItemList.Clear();
    switch (this._itemActionContext)
    {
      case EquipmentItemActionContext.InventorySelf:
        if (TibProxy.gameState != null && TibProxy.gameState.MyShip != null)
        {
          this._currentItemList.AddRange((IEnumerable<EquipmentItem>) TibProxy.gameState.MyShip.get_Inventory());
          break;
        }
        break;
      case EquipmentItemActionContext.GarrisonBank:
        if (this.dialogContext.garrison != null)
        {
          this._currentItemList.AddRange((IEnumerable<EquipmentItem>) this.dialogContext.garrison.get_Inventory());
          break;
        }
        break;
    }
    if (this._prevItemCount != this._currentItemList.Count)
      this.itemScrollView.RestrictWithinBounds(true);
    this._prevItemCount = this._currentItemList.Count;
    this.itemScrollView.SetData(this._currentItemList, this._itemActionContext);
  }
}
