﻿// Decompiled with JetBrains decompiler
// Type: UIPlayerExperience
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIPlayerExperience : MonoBehaviour
{
  public UISlider slider;
  public UILabel label;
  public UILabel title;
  public Animation anim;
  public UILabel expLabel;
  private int mExp;

  private void Start()
  {
    this.mExp = PlayerProfile.experience;
    this.UpdateExp(this.mExp);
  }

  private void UpdateExp(int exp)
  {
    if ((Object) this.label != (Object) null)
      this.label.text = PlayerProfile.GetLevelByExp(exp).ToString();
    if ((Object) this.title != (Object) null)
      this.title.text = PlayerProfile.GetTitleByExp(exp);
    if ((Object) this.slider != (Object) null)
      this.slider.value = PlayerProfile.GetProgressToNextLevel(exp);
    if (!((Object) this.expLabel != (Object) null))
      return;
    this.expLabel.text = exp.ToString("#,##0");
  }

  private void Update()
  {
    if (this.mExp == PlayerProfile.experience)
      return;
    if (this.mExp < PlayerProfile.experience)
    {
      int levelByExp1 = PlayerProfile.GetLevelByExp(this.mExp);
      this.mExp += Mathf.RoundToInt(50000f * RealTime.deltaTime);
      if (this.mExp > PlayerProfile.experience)
        this.mExp = PlayerProfile.experience;
      int levelByExp2 = PlayerProfile.GetLevelByExp(this.mExp);
      if (levelByExp1 != levelByExp2)
      {
        if ((Object) this.anim != (Object) null && !this.anim.isPlaying)
          this.anim.Play();
        UIStatus.Show(string.Format(Localization.Get("Level up"), (object) levelByExp2));
      }
      this.UpdateExp(this.mExp);
    }
    else
    {
      if ((Object) this.anim != (Object) null && !this.anim.isPlaying)
        this.anim.Play();
      this.mExp = PlayerProfile.experience;
      this.UpdateExp(this.mExp);
    }
  }
}
