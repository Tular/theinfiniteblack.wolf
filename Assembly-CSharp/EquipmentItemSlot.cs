﻿// Decompiled with JetBrains decompiler
// Type: EquipmentItemSlot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

[RequireComponent(typeof (SBUIButton))]
[RequireComponent(typeof (BoxCollider))]
public class EquipmentItemSlot : MonoBehaviour
{
  public string resourcePath = "items";
  public ItemType itemType;
  private UITexture _itemIcon;
  private UILabel _itemLabel;
  private BoxCollider _collider;
  private EquipmentItem _item;

  public void SetItem(EquipmentItem item)
  {
    if (this._item == item)
      return;
    this._item = item;
    if (this._item == null)
    {
      this.Clear();
      this._collider.enabled = false;
    }
    else
    {
      this._collider.enabled = true;
      this._itemIcon.enabled = true;
      StringBuilder stringBuilder = new StringBuilder();
      item.AppendName(stringBuilder);
      this._itemLabel.text = stringBuilder.ToString();
      this.StartCoroutine(this.LoadItemTexture());
    }
  }

  private void Awake()
  {
    this._itemIcon = this.GetComponentInChildren<UITexture>();
    this._itemLabel = this.GetComponentInChildren<UILabel>();
    this._collider = this.GetComponent<BoxCollider>();
    this._itemLabel.text = ((Enum) this.itemType).ToString();
  }

  private void OnDisable()
  {
    this.Clear();
  }

  private void Clear()
  {
    this.LogD("----------------- Clearing equipment slot");
    this._item = (EquipmentItem) null;
    this._itemLabel.text = ((Enum) this.itemType).ToString();
    this._itemIcon.enabled = false;
    if ((UnityEngine.Object) this._itemIcon.mainTexture == (UnityEngine.Object) null)
      return;
    Texture mainTexture = this._itemIcon.mainTexture;
    this._itemIcon.mainTexture = (Texture) null;
    Resources.UnloadAsset((UnityEngine.Object) mainTexture);
  }

  [DebuggerHidden]
  private IEnumerator LoadItemTexture()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EquipmentItemSlot.\u003CLoadItemTexture\u003Ec__Iterator1D()
    {
      \u003C\u003Ef__this = this
    };
  }
}
