﻿// Decompiled with JetBrains decompiler
// Type: CubeSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CubeSelect : MonoBehaviour
{
  private GameObject cube;

  private void OnEnable()
  {
    EasyTouch.On_SimpleTap += new EasyTouch.SimpleTapHandler(this.On_SimpleTap);
  }

  private void OnDestroy()
  {
    EasyTouch.On_SimpleTap -= new EasyTouch.SimpleTapHandler(this.On_SimpleTap);
  }

  private void Start()
  {
    this.cube = (GameObject) null;
  }

  private void On_SimpleTap(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject != (Object) null) || !(gesture.pickedObject.name == "Cube"))
      return;
    this.ResteColor();
    this.cube = gesture.pickedObject;
    this.cube.GetComponent<Renderer>().material.color = Color.red;
  }

  private void ResteColor()
  {
    if (!((Object) this.cube != (Object) null))
      return;
    this.cube.GetComponent<Renderer>().material.color = new Color(0.2352941f, 0.5607843f, 0.7882353f);
  }
}
