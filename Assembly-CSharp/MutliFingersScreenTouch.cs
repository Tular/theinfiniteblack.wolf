﻿// Decompiled with JetBrains decompiler
// Type: MutliFingersScreenTouch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MutliFingersScreenTouch : MonoBehaviour
{
  public GameObject touchGameObject;

  private void OnEnable()
  {
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.On_TouchStart);
  }

  private void OnDestroy()
  {
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.On_TouchStart);
  }

  private void On_TouchStart(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) null))
      return;
    (Object.Instantiate((Object) this.touchGameObject, gesture.GetTouchToWorldPoint(5f), Quaternion.identity) as GameObject).GetComponent<FingerTouch>().InitTouch(gesture.fingerIndex);
  }
}
