﻿// Decompiled with JetBrains decompiler
// Type: GameWebRequest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class GameWebRequest : MonoBehaviour
{
  private string mURL;
  private GameWebRequest.OnFinished mCallback;
  private object mParam;

  [DebuggerHidden]
  private IEnumerator Start()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GameWebRequest.\u003CStart\u003Ec__IteratorA()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void End()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public static void Create(string url)
  {
    GameWebRequest.Create(url, (GameWebRequest.OnFinished) null, (object) null);
  }

  public static void Create(string url, GameWebRequest.OnFinished callback)
  {
    GameWebRequest.Create(url, callback, (object) null);
  }

  public static void Create(string url, GameWebRequest.OnFinished callback, object param)
  {
    if (!Application.isPlaying)
      return;
    GameObject gameObject = new GameObject("_Game Web Request");
    Object.DontDestroyOnLoad((Object) gameObject);
    GameWebRequest gameWebRequest = gameObject.AddComponent<GameWebRequest>();
    gameWebRequest.mURL = url;
    gameWebRequest.mCallback = callback;
    gameWebRequest.mParam = param;
  }

  public delegate void OnFinished(bool success, object param, string response);
}
