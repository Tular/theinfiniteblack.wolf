﻿// Decompiled with JetBrains decompiler
// Type: DelegateObjectActivation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DelegateObjectActivation : MonoBehaviour
{
  private bool _isActive;

  public void Deactivate()
  {
    if (!this._isActive)
      return;
    this.gameObject.SetActive(false);
  }

  public void Activate()
  {
    if (this._isActive)
      return;
    this.gameObject.SetActive(true);
  }

  public void Toggle()
  {
    this.gameObject.SetActive(!this._isActive);
  }

  private void OnEnable()
  {
    this._isActive = true;
  }

  private void OnDisable()
  {
    this._isActive = false;
  }
}
