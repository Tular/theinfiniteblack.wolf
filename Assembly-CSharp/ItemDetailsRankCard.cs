﻿// Decompiled with JetBrains decompiler
// Type: ItemDetailsRankCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using UnityEngine;

public class ItemDetailsRankCard : MonoBehaviour
{
  public UILabel label;
  public UISprite borderSprite;
  public Color normalBorderColor;
  public Color selectedBorderColor;
  private UIScrollView _scrollView;
  private sbyte _rank;
  private bool _isSelected;
  private bool _centerOnSelected;

  public int rank
  {
    get
    {
      return (int) this._rank;
    }
    set
    {
      this._rank = (sbyte) value;
      this.label.text = Util.NUMERALS[(int) this._rank];
      this.name = string.Format("{0:D2}-{1}", (object) (int) this._rank, (object) Util.NUMERALS[(int) this._rank]);
    }
  }

  public ItemDetailsContext dialogContext { get; set; }

  private void ScrollToSelected()
  {
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this._scrollView.transform, this.transform);
    Vector3 constrainOffset = this._scrollView.panel.CalculateConstrainOffset((Vector2) relativeWidgetBounds.min, (Vector2) relativeWidgetBounds.max);
    float num1 = this._scrollView.panel.baseClipRegion.y + Mathf.Round(this._scrollView.bounds.max.y) + this._scrollView.panel.clipSoftness.y;
    float num2 = Mathf.Round(this._scrollView.bounds.min.y) + Mathf.Round(this._scrollView.bounds.min.y) - this._scrollView.panel.clipSoftness.y;
    if (1.0 < (double) relativeWidgetBounds.center.y - (double) this._scrollView.panel.finalClipRegion.y)
    {
      this.LogD(string.Format("---ABOVE CENTER--- constraint:{0} magnitude: {1}", (object) constrainOffset, (object) constrainOffset.sqrMagnitude));
      this.LogD(string.Format("spring panel test: {0}", (object) num1));
      if ((double) relativeWidgetBounds.center.y > (double) num1)
      {
        if (Mathf.RoundToInt(constrainOffset.y) == 0)
          return;
        this.LogD(string.Format("-- SPRING PANEL --"));
        Vector3 pos = this._scrollView.transform.localPosition + constrainOffset + relativeWidgetBounds.center;
        pos.x = Mathf.Round(pos.x);
        pos.y = Mathf.Round(pos.y);
        SpringPanel.Begin(this._scrollView.panel.gameObject, pos, 16f);
      }
      else
      {
        this.LogD(string.Format("-- center on child --"));
        Transform cachedTransform = this._scrollView.panel.cachedTransform;
        Vector3[] worldCorners = this._scrollView.panel.worldCorners;
        Vector3 position = (worldCorners[2] + worldCorners[0]) * 0.5f;
        Vector3 vector3 = cachedTransform.InverseTransformPoint(this.transform.position) - cachedTransform.InverseTransformPoint(position);
        if (!this._scrollView.canMoveHorizontally)
          vector3.x = 0.0f;
        if (!this._scrollView.canMoveVertically)
          vector3.y = 0.0f;
        vector3.z = 0.0f;
        SpringPanel.Begin(this._scrollView.panel.cachedGameObject, cachedTransform.localPosition - vector3, 16f);
      }
    }
    else
    {
      if (1.0 <= (double) relativeWidgetBounds.center.y - (double) this._scrollView.panel.finalClipRegion.y)
        return;
      this.LogD(string.Format("---BELOW CENTER---"));
      if ((double) relativeWidgetBounds.center.y < (double) num2)
      {
        if (Mathf.RoundToInt(constrainOffset.y) == 0)
          return;
        this.LogD(string.Format("-- SPRING PANEL --"));
        Vector3 pos = relativeWidgetBounds.center - this._scrollView.transform.localPosition + constrainOffset;
        pos.x = 0.0f;
        double num3 = (double) Mathf.Round(pos.x);
        pos.y = Mathf.Round(pos.y);
        SpringPanel.Begin(this._scrollView.panel.gameObject, pos, 16f);
      }
      else
      {
        this.LogD(string.Format("-- center on child --"));
        Transform cachedTransform = this._scrollView.panel.cachedTransform;
        Vector3[] worldCorners = this._scrollView.panel.worldCorners;
        Vector3 position = (worldCorners[2] + worldCorners[0]) * 0.5f;
        Vector3 vector3 = cachedTransform.InverseTransformPoint(this.transform.position) - cachedTransform.InverseTransformPoint(position);
        if (!this._scrollView.canMoveHorizontally)
          vector3.x = 0.0f;
        if (!this._scrollView.canMoveVertically)
          vector3.y = 0.0f;
        vector3.z = 0.0f;
        SpringPanel.Begin(this._scrollView.panel.cachedGameObject, cachedTransform.localPosition - vector3, 16f);
      }
    }
  }

  private void OnClick()
  {
    this.dialogContext.selectedRank = (int) this._rank;
    this._centerOnSelected = true;
    this._isSelected = true;
  }

  private void Start()
  {
    this._scrollView = NGUITools.FindInParents<UIScrollView>(this.gameObject);
  }

  private void Update()
  {
    if (!this.dialogContext.hasRank)
    {
      this.borderSprite.color = this.normalBorderColor;
      this._isSelected = false;
      this._centerOnSelected = false;
    }
    else
    {
      if (!this._isSelected && this.dialogContext.selectedRank == this.rank)
      {
        this._centerOnSelected = true;
        this._isSelected = true;
      }
      else if (this._isSelected && this.dialogContext.selectedRank != this.rank)
      {
        this._isSelected = false;
        this._centerOnSelected = false;
      }
      this.borderSprite.color = !this._isSelected ? this.normalBorderColor : this.selectedBorderColor;
      if (!this._centerOnSelected)
        return;
      this._centerOnSelected = false;
      this.ScrollToSelected();
    }
  }
}
