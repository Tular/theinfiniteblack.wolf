﻿// Decompiled with JetBrains decompiler
// Type: ItemEngineeringView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class ItemEngineeringView : MonoBehaviour
{
  public SBUIButton execute;
  public SBUITab rankUp;
  public SBUITab rankDown;
  public SBUITab classUp;
  public SBUITab classDown;
  public SBUITab augmentRarity;
  public SBUITab repair;
  public GameObject overlay;
  public GameObject engineeringCostRoot;
  public UILabel engineeringCostLabel;
  public ItemEngineeringViewItemCard currentItemCard;
  public ItemEngineeringViewItemCard engineeredItemCard;
  private ItemEngineeringDialogContext _dialogContext;
  private EngineeringType _pendingEngineeringType;
  private ItemEngineeringDialog _dialog;
  private bool _isInitialized;

  public void ShowRankUpResults()
  {
    this.ShowEngineeringResults(EngineeringType.RANK_UP);
  }

  public void ShowRankDownResults()
  {
    this.ShowEngineeringResults(EngineeringType.RANK_DOWN);
  }

  public void ShowClassUpResults()
  {
    this.ShowEngineeringResults(EngineeringType.CLASS_UP);
  }

  public void ShowClassDownResults()
  {
    this.ShowEngineeringResults(EngineeringType.CLASS_DOWN);
  }

  public void ShowRarityResults()
  {
    this.ShowEngineeringResults(EngineeringType.RARITY);
  }

  public void ShowRepairResults()
  {
    this.ShowEngineeringResults(EngineeringType.REPAIR);
  }

  public void ShowItemDetails()
  {
    if (this._dialogContext.selectedInventoryItem == null)
      return;
    DialogManager.ShowDialog<ItemDetailsPopup>(new object[1]
    {
      (object) this._dialogContext.selectedInventoryItem
    });
  }

  public void RequestDeconstruct()
  {
    this.ShowEngineeringResults(EngineeringType.DECONSTRUCT);
  }

  public void ExecuteEngineering()
  {
    TibProxy.gameState.DoItemEngineer(this._pendingEngineeringType, this._dialogContext.selectedInventoryItem);
    this._dialog.Hide();
  }

  public void CancelEngineering()
  {
    this._pendingEngineeringType = EngineeringType.NULL;
  }

  public void RequistUnbind()
  {
    this.ShowEngineeringResults(EngineeringType.UNBIND);
  }

  private void ShowEngineeringResults(EngineeringType engineeringType)
  {
    this._pendingEngineeringType = engineeringType;
    switch (engineeringType)
    {
      case EngineeringType.DECONSTRUCT:
        NGUITools.SetActiveSelf(this.engineeringCostRoot, false);
        NGUITools.SetActiveSelf(this.execute.gameObject, false);
        TibProxy.gameState.DoItemEngineer(EngineeringType.DECONSTRUCT, this._dialogContext.selectedInventoryItem);
        break;
      case EngineeringType.REPAIR:
      case EngineeringType.RANK_UP:
      case EngineeringType.RANK_DOWN:
      case EngineeringType.CLASS_UP:
      case EngineeringType.CLASS_DOWN:
      case EngineeringType.RARITY:
        EquipmentItem upgrade = this._dialogContext.selectedInventoryItem.GetUpgrade(engineeringType);
        this.engineeringCostLabel.text = string.Format("{0:###,###,###} BlackDollars", (object) this._dialogContext.selectedInventoryItem.GetEngineeringCost(engineeringType));
        NGUITools.SetActiveSelf(this.engineeringCostRoot, true);
        NGUITools.SetActiveSelf(this.execute.gameObject, true);
        this.engineeredItemCard.Show(upgrade);
        break;
      case EngineeringType.UNBIND:
        NGUITools.SetActiveSelf(this.engineeringCostRoot, false);
        NGUITools.SetActiveSelf(this.execute.gameObject, false);
        TibProxy.gameState.DoItemEngineer(EngineeringType.UNBIND, this._dialogContext.selectedInventoryItem);
        break;
    }
  }

  private void ResetButtonToggleStates()
  {
    this.ResetButtonToggleHack(this.rankUp);
    this.ResetButtonToggleHack(this.rankDown);
    this.ResetButtonToggleHack(this.classUp);
    this.ResetButtonToggleHack(this.classDown);
    this.ResetButtonToggleHack(this.augmentRarity);
    this.ResetButtonToggleHack(this.repair);
    NGUITools.SetActiveSelf(this.engineeringCostRoot, false);
    NGUITools.SetActiveSelf(this.execute.gameObject, false);
  }

  private void ResetButtonToggleHack(SBUITab btn)
  {
    if (!btn.isSelected)
      return;
    UIToggle component = btn.GetComponent<UIToggle>();
    component.optionCanBeNone = true;
    btn.isSelected = false;
    component.optionCanBeNone = false;
  }

  public void UpdateContent()
  {
    if (this._dialogContext == null)
      return;
    if (this._dialogContext.selectedInventoryItem == null)
    {
      this.currentItemCard.Hide();
      this.engineeredItemCard.Hide();
    }
    else
      this.currentItemCard.Show(this._dialogContext.selectedInventoryItem);
  }

  private void TibProxy_onInventoryChangedEvent(object sender, InventoryChangedEventArgs e)
  {
    if (e.Source != TibProxy.gameState.MyShip)
      return;
    NGUITools.SetActiveSelf(this.overlay, false);
    if (e.State.MyShip.get_Inventory().Contains(this._dialogContext.selectedInventoryItem))
      return;
    this._dialogContext.selectedInventoryItem = (EquipmentItem) null;
    this.currentItemCard.Hide();
    this.engineeredItemCard.Hide();
    this._dialog.Hide();
  }

  private void TryInitialize()
  {
    this._dialog = NGUITools.FindInParents<ItemEngineeringDialog>(this.gameObject);
    if ((UnityEngine.Object) this._dialog == (UnityEngine.Object) null || this._dialog.dialogContext == null)
      return;
    this._dialogContext = this._dialog.dialogContext;
    NGUITools.SetActiveSelf(this.overlay, false);
    this.currentItemCard.Hide();
    this.engineeredItemCard.Hide();
    TibProxy.Instance.onInventoryChangedEvent -= new EventHandler<InventoryChangedEventArgs>(this.TibProxy_onInventoryChangedEvent);
    TibProxy.Instance.onInventoryChangedEvent += new EventHandler<InventoryChangedEventArgs>(this.TibProxy_onInventoryChangedEvent);
    this._isInitialized = true;
  }

  private void OnEnable()
  {
    if (!this._isInitialized)
      this.TryInitialize();
    if (!this._isInitialized)
      return;
    TibProxy.Instance.onInventoryChangedEvent -= new EventHandler<InventoryChangedEventArgs>(this.TibProxy_onInventoryChangedEvent);
    TibProxy.Instance.onInventoryChangedEvent += new EventHandler<InventoryChangedEventArgs>(this.TibProxy_onInventoryChangedEvent);
    this.engineeringCostLabel.text = string.Empty;
    NGUITools.SetActiveSelf(this.overlay, false);
    NGUITools.SetActiveSelf(this.engineeringCostRoot, false);
    NGUITools.SetActiveSelf(this.execute.gameObject, false);
    this.UpdateContent();
  }

  private void OnDisable()
  {
    TibProxy.Instance.onInventoryChangedEvent -= new EventHandler<InventoryChangedEventArgs>(this.TibProxy_onInventoryChangedEvent);
    NGUITools.SetActiveSelf(this.overlay, false);
    this.currentItemCard.Hide();
    this.engineeredItemCard.Hide();
    this.ResetButtonToggleStates();
  }
}
