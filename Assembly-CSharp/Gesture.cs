﻿// Decompiled with JetBrains decompiler
// Type: Gesture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Gesture : BaseFinger
{
  public EasyTouch.SwipeDirection swipe;
  public float swipeLength;
  public Vector2 swipeVector;
  public float deltaPinch;
  public float twistAngle;
  public float twoFingerDistance;

  public Vector3 GetTouchToWorldPoint(float z)
  {
    return Camera.main.ScreenToWorldPoint(new Vector3(this.position.x, this.position.y, z));
  }

  public Vector3 GetTouchToWorldPoint(Vector3 position3D)
  {
    return Camera.main.ScreenToWorldPoint(new Vector3(this.position.x, this.position.y, Camera.main.transform.InverseTransformPoint(position3D).z));
  }

  public float GetSwipeOrDragAngle()
  {
    return Mathf.Atan2(this.swipeVector.normalized.y, this.swipeVector.normalized.x) * 57.29578f;
  }

  public Vector2 NormalizedPosition()
  {
    return new Vector2((float) (100.0 / (double) Screen.width * (double) this.position.x / 100.0), (float) (100.0 / (double) Screen.height * (double) this.position.y / 100.0));
  }

  public bool IsOverUIElement()
  {
    return EasyTouch.IsFingerOverUIElement(this.fingerIndex);
  }

  public bool IsOverRectTransform(RectTransform tr, Camera camera = null)
  {
    if ((Object) camera == (Object) null)
      camera = Camera.main;
    return RectTransformUtility.RectangleContainsScreenPoint(tr, this.position, camera);
  }

  public GameObject GetCurrentFirstPickedUIElement()
  {
    return EasyTouch.GetCurrentPickedUIElement(this.fingerIndex);
  }

  public GameObject GetCurrentPickedObject()
  {
    return EasyTouch.GetCurrentPickedObject(this.fingerIndex);
  }
}
