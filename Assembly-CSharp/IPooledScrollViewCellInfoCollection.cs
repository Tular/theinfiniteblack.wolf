﻿// Decompiled with JetBrains decompiler
// Type: IPooledScrollViewCellInfoCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public interface IPooledScrollViewCellInfoCollection
{
  int Count { get; }

  IEnumerable<IScrollViewCellInfo> values { get; }

  IScrollViewCellInfo first { get; }

  IScrollViewCellInfo last { get; }

  void Add(IScrollViewCellInfo cellInfo);

  void Remove(IScrollViewCellInfo cellInfo);

  void Clear();

  bool dataWasReset { get; }

  bool dataWasModified { get; }
}
