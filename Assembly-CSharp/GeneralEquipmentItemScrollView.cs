﻿// Decompiled with JetBrains decompiler
// Type: GeneralEquipmentItemScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class GeneralEquipmentItemScrollView : PooledScrollViewBase
{
  public Transform prefCellPrefab;
  public Transform prefItemTypeHeading;
  public EquipmentItemActionBar itemActionBar;
  public int itemCellToggleGroup;
  public GameObject selectedGameObject;
  private EquipmentItemActionContext _itemActionContext;

  public EquipmentItemActionContext itemActionContext
  {
    get
    {
      return this._itemActionContext;
    }
  }

  [ContextMenu("Dump Item Count")]
  public void DumpItemCount()
  {
    this.LogD(string.Format("Item Count: {0}", (object) this.data.Count));
  }

  [ContextMenu("Dump Item List")]
  public void DumpItems()
  {
    this.LogD(string.Format("GameState Inventory Count: {0}", (object) TibProxy.gameState.MyShip.get_Inventory().Count));
    this.LogD(string.Format("GameState Bank Count: {0}", (object) TibProxy.gameState.Bank.get_Items().Count<EquipmentItem>()));
    using (List<IScrollViewCellInfo>.Enumerator enumerator = this.data.values.ToList<IScrollViewCellInfo>().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IScrollViewCellInfo current = enumerator.Current;
        GeneralEquipmentItemCellInfo equipmentItemCellInfo = current as GeneralEquipmentItemCellInfo;
        if (equipmentItemCellInfo == null)
        {
          GeneralEquipmentItemScrollGroupHeadingCellInfo groupHeadingCellInfo = current as GeneralEquipmentItemScrollGroupHeadingCellInfo;
          if (groupHeadingCellInfo == null)
            throw new InvalidCastException();
          this.LogD(groupHeadingCellInfo.headingText);
        }
        else
          this.LogD(string.Format("{0} - BOE:{1} NoDrop:{2}", (object) equipmentItemCellInfo.item.LongName, (object) equipmentItemCellInfo.item.BindOnEquip, (object) equipmentItemCellInfo.item.NoDrop));
      }
    }
  }

  [ContextMenu("Dump Item Dictionary")]
  public void DumpDictionary()
  {
    ((GeneralEquipmentItemCellInfoCollection) this.data).DumpDictionaryData();
  }

  public void SetData(List<EquipmentItem> itemList, EquipmentItemActionContext actionContext)
  {
    if (this.data == null)
      this.data = (IPooledScrollViewCellInfoCollection) new GeneralEquipmentItemCellInfoCollection(this.prefCellPrefab, this.prefItemTypeHeading);
    if (this._itemActionContext != actionContext)
      this.data.Clear();
    this._itemActionContext = actionContext;
    ((GeneralEquipmentItemCellInfoCollection) this.data).UpdateWith(itemList);
  }

  public void ClearData()
  {
    if (this.data == null)
      this.data = (IPooledScrollViewCellInfoCollection) new GeneralEquipmentItemCellInfoCollection(this.prefCellPrefab, this.prefItemTypeHeading);
    this.ResetPosition();
    this.ResetVisibleInfos();
    this.data.Clear();
    this.ResetPosition();
  }

  public void OnItemSelectionChanged()
  {
    if (!((UnityEngine.Object) UIToggle.current != (UnityEngine.Object) null))
      return;
    EquipmentItem equipmentItem = UIToggle.current.GetComponent<GeneralEquipmentItemCell>().item;
    if (equipmentItem == null)
    {
      this.selectedGameObject = (GameObject) null;
      if (!((UnityEngine.Object) this.itemActionBar != (UnityEngine.Object) null))
        return;
      this.itemActionBar.HideActionBar();
    }
    else if (UIToggle.current.value && (UnityEngine.Object) UIToggle.current.gameObject != (UnityEngine.Object) this.selectedGameObject)
    {
      this.selectedGameObject = UIToggle.current.gameObject;
      if (!((UnityEngine.Object) this.itemActionBar != (UnityEngine.Object) null))
        return;
      this.itemActionBar.ShowActionBar(equipmentItem, this._itemActionContext);
    }
    else
    {
      if (!((UnityEngine.Object) UIToggle.current.gameObject == (UnityEngine.Object) this.selectedGameObject))
        return;
      this.selectedGameObject = (GameObject) null;
      if (!((UnityEngine.Object) this.itemActionBar != (UnityEngine.Object) null))
        return;
      this.itemActionBar.HideActionBar();
    }
  }
}
