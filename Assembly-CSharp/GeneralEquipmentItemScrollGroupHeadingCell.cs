﻿// Decompiled with JetBrains decompiler
// Type: GeneralEquipmentItemScrollGroupHeadingCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIWidget))]
public class GeneralEquipmentItemScrollGroupHeadingCell : MonoBehaviour, IScrollViewCell
{
  public UILabel title;
  private UIWidget _widget;
  private int _spawnedFrame;

  public Vector3 extents
  {
    get
    {
      return new Vector3((float) this._widget.width * 0.05f, (float) this._widget.height * 0.5f, 0.0f);
    }
  }

  public int height
  {
    get
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      return this._widget.height;
    }
  }

  public int width
  {
    get
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      return this._widget.width;
    }
  }

  public virtual void ResetVisuals()
  {
    this.title.text = string.Empty;
  }

  protected void OnSpawned()
  {
    this._spawnedFrame = Time.frameCount;
    this.ResetVisuals();
    this._widget.alpha = 0.0f;
  }

  protected void OnDespawned()
  {
    this.ResetVisuals();
    this._widget.alpha = 0.0f;
  }

  private void Awake()
  {
    this._widget = this.GetComponent<UIWidget>();
  }

  private void Update()
  {
    if (Time.frameCount < this._spawnedFrame + 2)
      return;
    this._widget.alpha = 1f;
  }
}
