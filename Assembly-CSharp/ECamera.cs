﻿// Decompiled with JetBrains decompiler
// Type: ECamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class ECamera
{
  public Camera camera;
  public bool guiCamera;

  public ECamera(Camera cam, bool gui)
  {
    this.camera = cam;
    this.guiCamera = gui;
  }
}
