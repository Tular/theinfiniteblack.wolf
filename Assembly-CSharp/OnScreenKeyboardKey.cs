﻿// Decompiled with JetBrains decompiler
// Type: OnScreenKeyboardKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (SBUIButton))]
public class OnScreenKeyboardKey : MonoBehaviour
{
  public UILabel keyLabel;
  private SBUIButton _button;

  public List<EventDelegate> onClick
  {
    get
    {
      return this._button.onClick;
    }
  }

  public void AddKeyBinding(KeyCode keyCode, UIKeyBinding.Action action)
  {
    foreach (UIKeyBinding component in this.GetComponents<UIKeyBinding>())
    {
      if (component.keyCode == keyCode)
        return;
    }
    UIKeyBinding uiKeyBinding = this.gameObject.AddComponent<UIKeyBinding>();
    uiKeyBinding.keyCode = keyCode;
    uiKeyBinding.action = action;
  }

  private void Awake()
  {
    this._button = this.GetComponent<SBUIButton>();
  }
}
