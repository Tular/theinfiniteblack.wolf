﻿// Decompiled with JetBrains decompiler
// Type: EngineeringItemScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class EngineeringItemScrollView : GeneralItemScrollViewBase<EngineeringItemListingViewContext>
{
  protected override GeneralScrollViewCardBase InitializeCard(Transform cardTrans, EquipmentItem item)
  {
    return base.InitializeCard(cardTrans, item);
  }
}
