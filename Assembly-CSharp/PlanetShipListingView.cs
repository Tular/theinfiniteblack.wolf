﻿// Decompiled with JetBrains decompiler
// Type: PlanetShipListingView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class PlanetShipListingView : MonoBehaviour
{
  public string resourcePath = "profile";
  public ShipFactoryShipCard prefShipCard;
  public ShipProfileTexture shipProfile;
  public UIScrollView cardScrollView;
  public UIGrid cardGrid;
  private ShipFactoryShipCard[] _cards;

  public PlanetDialogContext dialogContext { get; set; }

  private void Awake()
  {
    this.CreateShipCards();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    int index = (int) TibProxy.gameState.MyShip.Class;
    if (this._cards[index].toggle.value)
      this.SetSelectedCard(this._cards[index]);
    else
      this._cards[index].toggle.value = true;
  }

  public void Hide()
  {
    this.gameObject.SetActive(false);
  }

  private void SetSelectedCard(ShipFactoryShipCard card)
  {
    this.dialogContext.selectedShipClass = card.shipClass;
    this.dialogContext.dialogSubtitleText = card.shipClass.Name();
    this.shipProfile.Show(this.dialogContext.selectedShipClass);
  }

  private void CreateShipCards()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    PlanetShipListingView.\u003CCreateShipCards\u003Ec__AnonStorey40 cardsCAnonStorey40 = new PlanetShipListingView.\u003CCreateShipCards\u003Ec__AnonStorey40();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card0 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card0.shipClass = ShipClass.Shuttle;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card0.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__9C));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card1 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card1.shipClass = ShipClass.Corvette;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card1.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__9D));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card2 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card2.shipClass = ShipClass.Frigate;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card2.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__9E));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card3 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card3.shipClass = ShipClass.Destroyer;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card3.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__9F));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card4 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card4.shipClass = ShipClass.Cruiser;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card4.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A0));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card5 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card5.shipClass = ShipClass.Battleship;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card5.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A1));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card6 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card6.shipClass = ShipClass.Dreadnought;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card6.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A2));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card7 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card7.shipClass = ShipClass.Titan;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card7.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A3));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card8 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card8.shipClass = ShipClass.Flagship;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card8.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A4));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card9 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card9.shipClass = ShipClass.Carrier;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card9.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A5));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card10 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card10.shipClass = ShipClass.Flayer;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card10.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A6));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card11 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card11.shipClass = ShipClass.Executioner;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card11.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A7));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card12 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card12.shipClass = ShipClass.Devastator;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card12.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A8));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card13 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card13.shipClass = ShipClass.Despoiler;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card13.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__A9));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card14 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card14.shipClass = ShipClass.Annihilator;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card14.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__AA));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card15 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card15.shipClass = ShipClass.WyrdInvader;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card15.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__AB));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card16 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card16.shipClass = ShipClass.WyrdAssassin;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card16.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__AC));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card17 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card17.shipClass = ShipClass.WyrdReaper;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card17.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__AD));
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card18 = this.cardGrid.gameObject.AddChild(this.prefShipCard.gameObject).GetComponent<ShipFactoryShipCard>();
    // ISSUE: reference to a compiler-generated field
    cardsCAnonStorey40.card18.shipClass = ShipClass.WyrdTerminus;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Add(cardsCAnonStorey40.card18.toggle.onChange, new EventDelegate.Callback(cardsCAnonStorey40.\u003C\u003Em__AE));
    this._cards = new ShipFactoryShipCard[19];
    // ISSUE: reference to a compiler-generated field
    this._cards[0] = cardsCAnonStorey40.card0;
    // ISSUE: reference to a compiler-generated field
    this._cards[1] = cardsCAnonStorey40.card1;
    // ISSUE: reference to a compiler-generated field
    this._cards[2] = cardsCAnonStorey40.card2;
    // ISSUE: reference to a compiler-generated field
    this._cards[3] = cardsCAnonStorey40.card3;
    // ISSUE: reference to a compiler-generated field
    this._cards[4] = cardsCAnonStorey40.card4;
    // ISSUE: reference to a compiler-generated field
    this._cards[5] = cardsCAnonStorey40.card5;
    // ISSUE: reference to a compiler-generated field
    this._cards[6] = cardsCAnonStorey40.card6;
    // ISSUE: reference to a compiler-generated field
    this._cards[7] = cardsCAnonStorey40.card7;
    // ISSUE: reference to a compiler-generated field
    this._cards[8] = cardsCAnonStorey40.card8;
    // ISSUE: reference to a compiler-generated field
    this._cards[9] = cardsCAnonStorey40.card9;
    // ISSUE: reference to a compiler-generated field
    this._cards[10] = cardsCAnonStorey40.card10;
    // ISSUE: reference to a compiler-generated field
    this._cards[11] = cardsCAnonStorey40.card11;
    // ISSUE: reference to a compiler-generated field
    this._cards[12] = cardsCAnonStorey40.card12;
    // ISSUE: reference to a compiler-generated field
    this._cards[13] = cardsCAnonStorey40.card13;
    // ISSUE: reference to a compiler-generated field
    this._cards[14] = cardsCAnonStorey40.card14;
    // ISSUE: reference to a compiler-generated field
    this._cards[15] = cardsCAnonStorey40.card15;
    // ISSUE: reference to a compiler-generated field
    this._cards[16] = cardsCAnonStorey40.card16;
    // ISSUE: reference to a compiler-generated field
    this._cards[17] = cardsCAnonStorey40.card17;
    // ISSUE: reference to a compiler-generated field
    this._cards[18] = cardsCAnonStorey40.card18;
  }
}
