﻿// Decompiled with JetBrains decompiler
// Type: GeneralEquipmentItemCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class GeneralEquipmentItemCellInfo : IScrollViewCellInfo
{
  private Vector3 _localPosition;
  private readonly EquipmentItem _item;
  private GeneralEquipmentItemCell _cellInstance;
  private int _cellHeight;
  private int _cellWidth;
  private string _titleText;
  private string _subtitleText;
  private string _descriptionText;
  private string _itemProsText;
  private string _itemConsText;
  private string _equipPointsText;
  private string _durabilityText;
  private string _creditValueText;
  private string _actualBlackDollarValueText;
  private string _actualCreditValueText;

  public GeneralEquipmentItemCellInfo(Transform prefab, EquipmentItem item)
  {
    this.cellPrefab = prefab;
    this._item = item;
    StringBuilder titleSb;
    StringBuilder subtitleSb;
    StringBuilder descriptionSb;
    StringBuilder prosSb;
    StringBuilder consSb;
    this.extents = prefab.GetComponent<GeneralEquipmentItemCell>().CalculateExtents(this._item, out titleSb, out subtitleSb, out descriptionSb, out prosSb, out consSb);
    Markup.GetNGUI(titleSb);
    subtitleSb.Append("[-]");
    Markup.GetNGUI(subtitleSb);
    descriptionSb.Insert(0, "[d1f1ff]");
    descriptionSb.Append("[-]");
    prosSb.Insert(0, "[00ff00]");
    prosSb.Append("[-]");
    consSb.Insert(0, "[ff4400]");
    consSb.Append("[-]");
    this._cellWidth = Mathf.RoundToInt(this.extents.x * 2f);
    this._cellHeight = Mathf.RoundToInt(this.extents.y * 2f);
    this._titleText = titleSb.ToString();
    this._subtitleText = subtitleSb.ToString();
    this._descriptionText = descriptionSb.ToString();
    this._itemProsText = prosSb.ToString();
    this._itemConsText = consSb.ToString();
    this._equipPointsText = string.Format("{0}", (object) this._item.EPCost);
    this._durabilityText = string.Format("{0}%", (object) this._item.Durability);
    this._creditValueText = string.Format("${0}", (object) this._item.StarPortSellValue);
    this._actualBlackDollarValueText = string.Format("{0:###,###}", (object) this._item.ActualBlackDollarValue);
    this._actualCreditValueText = string.Format("{0:$###,###}", (object) this._item.ActualCreditValue);
  }

  public Vector3 localPosition
  {
    get
    {
      return this._localPosition;
    }
    set
    {
      this._localPosition = value;
      if (!((UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null))
        return;
      this._cellInstance.transform.localPosition = this._localPosition;
    }
  }

  public Vector3 extents { get; private set; }

  public object cellData
  {
    get
    {
      return (object) this._item;
    }
  }

  public System.Type cellType
  {
    get
    {
      return typeof (GeneralEquipmentItemCell);
    }
  }

  public bool isBound
  {
    get
    {
      return (UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null;
    }
  }

  public Transform cellPrefab { get; private set; }

  public Transform cellTransform
  {
    get
    {
      if ((UnityEngine.Object) this._cellInstance != (UnityEngine.Object) null)
        return this._cellInstance.transform;
      return (Transform) null;
    }
  }

  public EquipmentItem item
  {
    get
    {
      return this._item;
    }
  }

  public bool IsContainedIn(Bounds bounds)
  {
    if (!bounds.Contains(this.localPosition + this.extents))
      return bounds.Contains(this.localPosition - this.extents);
    return true;
  }

  public virtual void BindCell(Transform cellTrans)
  {
    this._cellInstance = cellTrans.GetComponent<GeneralEquipmentItemCell>();
    this._cellInstance.item = this.item;
    this._cellInstance.itemIconName = ((Enum) this._item.Icon).ToString();
    this._cellInstance.transform.localPosition = this.localPosition;
    if ((bool) ((UnityEngine.Object) this._cellInstance.title))
      this._cellInstance.title.text = this._titleText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.subTitle))
      this._cellInstance.subTitle.text = this._subtitleText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.description))
      this._cellInstance.description.text = this._descriptionText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.itemPros))
      this._cellInstance.itemPros.text = this._itemProsText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.itemCons))
      this._cellInstance.itemCons.text = this._itemConsText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.equipPoints))
      this._cellInstance.equipPoints.text = this._equipPointsText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.durability))
      this._cellInstance.durability.text = this._durabilityText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.creditValue))
      this._cellInstance.creditValue.text = this._creditValueText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.actualCreditValue))
      this._cellInstance.actualCreditValue.text = this._actualCreditValueText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.actualBlackDollarValue))
      this._cellInstance.actualBlackDollarValue.text = this._actualBlackDollarValueText;
    if ((bool) ((UnityEngine.Object) this._cellInstance.specialNote))
      this._cellInstance.specialNote.gameObject.SetActive(false);
    this._cellInstance.height = this._cellHeight;
    this._cellInstance.width = this._cellWidth;
  }

  public void Unbind()
  {
    this._cellInstance.ResetVisuals();
    this._cellInstance = (GeneralEquipmentItemCell) null;
  }
}
