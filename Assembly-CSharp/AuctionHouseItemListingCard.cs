﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseItemListingCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[RequireComponent(typeof (UIWidget))]
[RequireComponent(typeof (UIDragScrollView))]
public class AuctionHouseItemListingCard : GeneralScrollViewCardBase
{
  public string resourcePath = "items";
  public UITexture itemIcon;
  public UILabel title;
  public UILabel subTitle;
  public UILabel description;
  public UILabel itemPros;
  public UILabel itemCons;
  public UILabel equipPoints;
  public UILabel durability;
  public UILabel auctionDetails;
  public UIToggle watchToggle;
  public SBUIButton cancel;
  public SBUIButton bid;
  public SBUIButton buyoutCredits;
  public SBUIButton buyoutBlackDollars;
  public UISprite borderSprite;
  public UIGrid actionBarGrid;
  public int minHeight;
  private UIWidget _widget;
  private string _spawnedName;
  private int _lastChangedId;
  private bool _updateOwner;
  private int _ownerLastChangedId;
  private bool _widgetsAreVisible;
  private Color _defaultBorderColor;

  public int auctionId
  {
    get
    {
      if (this.auction == null)
        return -1;
      return this.auction.ID;
    }
  }

  public ClientAuction auction { get; private set; }

  public override void Refresh()
  {
    this.LoadItemTexture();
    this.UpdateVisuals();
    this.UpdateActionBar();
    this.UpdateLayout();
    this._lastChangedId = this.auction.LastChangeFlagID;
  }

  public void OnToggleWatch()
  {
    if ((UnityEngine.Object) UIToggle.current == (UnityEngine.Object) null || (UnityEngine.Object) UIToggle.current != (UnityEngine.Object) this.watchToggle || UIToggle.current.value == this.auction.IsWatched)
      return;
    this.LogD(string.Format("Setting Auction watch: {0}", (object) UIToggle.current.value));
    this.auction.IsWatched = UIToggle.current.value;
  }

  public void OnItemDetailsClicked()
  {
    DialogManager.ShowDialog<ItemDetailsPopup>(new object[1]
    {
      (object) this.auction.Item
    });
  }

  public void CancelAuction()
  {
    this.auction.DoCancel();
  }

  public void Bid()
  {
    this.auction.DoBid();
  }

  public void BuyoutCredits()
  {
    this.auction.DoBuyout(CurrencyType.Credits);
  }

  public void BuyoutBlackDollars()
  {
    this.auction.DoBuyout(CurrencyType.BlackDollars);
  }

  public virtual void InitializeCard(ClientAuction clientAuction)
  {
    this.auction = clientAuction;
    this.name = this.auction.Item.LongName;
    this.watchToggle.value = this.auction.IsWatched;
    this.UpdateVisuals();
    this.UpdateLayout();
  }

  private void UpdateVisuals()
  {
    StringBuilder stringBuilder1 = new StringBuilder(500);
    StringBuilder stringBuilder2 = new StringBuilder(500);
    StringBuilder stringBuilder3 = new StringBuilder(500);
    StringBuilder stringBuilder4 = new StringBuilder(500);
    StringBuilder stringBuilder5 = new StringBuilder(500);
    this.auction.Item.AppendName(stringBuilder1);
    Markup.GetNGUI(stringBuilder1);
    this.title.text = stringBuilder1.ToString();
    stringBuilder2.Append(this.auction.Item.Rarity.GetMarkup());
    this.auction.Item.AppendSubTitle(stringBuilder2);
    stringBuilder2.Append("[-] ");
    Markup.GetNGUI(stringBuilder2);
    this.subTitle.text = stringBuilder2.ToString();
    stringBuilder3.Append("[d1f1ff]");
    this.auction.Item.AppendDescription(stringBuilder3);
    stringBuilder3.Append("[-]");
    this.description.text = stringBuilder3.ToString();
    this.auction.Item.AppendProsAndCons(stringBuilder4, stringBuilder5);
    stringBuilder4.Insert(0, "[00ff00]");
    stringBuilder5.Insert(0, "[ff4400]");
    stringBuilder4.Append("[-]");
    stringBuilder5.Append("[-]");
    this.itemPros.text = stringBuilder4.ToString();
    this.itemCons.text = stringBuilder5.ToString();
    this.equipPoints.text = string.Format("{0}", (object) this.auction.Item.EPCost);
    this.durability.text = string.Format("{0}%", (object) this.auction.Item.Durability);
    this.UpdateAuctionDetails();
  }

  private void UpdateLayout()
  {
    this.title.MakePixelPerfect();
    this.subTitle.MakePixelPerfect();
    this.description.MakePixelPerfect();
    this.itemPros.UpdateAnchors();
    this.itemCons.UpdateAnchors();
    this.itemPros.MakePixelPerfect();
    this.itemCons.MakePixelPerfect();
    this.auctionDetails.MakePixelPerfect();
    Bounds bounds1 = this.auctionDetails.CalculateBounds(this.transform);
    Bounds bounds2 = this.itemCons.CalculateBounds(this.transform);
    Bounds bounds3 = this.itemPros.CalculateBounds(this.transform);
    int val2 = Mathf.RoundToInt(this._widget.localCorners[1].y - (float) ((double) bounds2.min.y - 6.0 - (double) bounds1.size.y - (double) this.auctionDetails.bottomAnchor.absolute - 6.0));
    this._widget.height = Math.Max(this.minHeight, Math.Max(Mathf.RoundToInt(this._widget.localCorners[1].y - (float) ((double) bounds3.min.y - 6.0 - (double) bounds1.size.y - (double) this.auctionDetails.bottomAnchor.absolute - 6.0)), val2));
    foreach (UIWidget componentsInChild in this.transform.GetComponentsInChildren<UIWidget>())
    {
      componentsInChild.MarkAsChanged();
      componentsInChild.UpdateAnchors();
    }
  }

  private void UpdateAuctionDetails()
  {
    StringBuilder stringBuilder = new StringBuilder(500);
    if (this.auction.Owner.Name.Equals("Unknown?"))
    {
      stringBuilder.AppendFormat("Seller: -=fetching info=-\n");
      this._ownerLastChangedId = this.auction.Owner.LastChangeFlagID;
      this._updateOwner = true;
    }
    else
    {
      this._updateOwner = false;
      stringBuilder.AppendFormat("Seller: {0}\n", (object) this.auction.Owner.Name);
    }
    if (this.auction.BuyoutCredits == 0 && this.auction.BuyoutBlackDollars == 0)
    {
      stringBuilder.AppendLine("-= No Buyout =-");
    }
    else
    {
      if (this.auction.BuyoutCredits > 0)
        stringBuilder.AppendFormat("Credit Buyout: {0:$###,###,###}\n", (object) this.auction.BuyoutCredits);
      if (this.auction.BuyoutBlackDollars > 0)
        stringBuilder.AppendFormat("BlackDollar Buyout: {0:###,###,###}\n", (object) this.auction.BuyoutBlackDollars);
    }
    stringBuilder.AppendFormat("Next Bid: {0:$###,###,###}\n", (object) this.auction.NextBid);
    TimeSpan timeSpan = TimeSpan.FromMilliseconds(Convert.ToDouble(this.auction.Time.RemainingMS));
    if (timeSpan.Days > 0)
      stringBuilder.AppendFormat("{0}d {1}h {2}m Remaining", (object) timeSpan.Days, (object) timeSpan.Hours, (object) timeSpan.Minutes);
    else if (timeSpan.Hours > 0)
      stringBuilder.AppendFormat("{0}h {1}m Remaining", (object) timeSpan.Hours, (object) timeSpan.Minutes);
    else
      stringBuilder.AppendFormat("{0}m Remaining", (object) timeSpan.Minutes);
    this.auctionDetails.text = stringBuilder.ToString();
  }

  private void UpdateActionBar()
  {
    if (this.cancel.gameObject.activeSelf != this.auction.CanCancel)
      NGUITools.SetActiveSelf(this.cancel.gameObject, this.auction.CanCancel);
    if (this.bid.gameObject.activeSelf != this.auction.CanBid)
      NGUITools.SetActiveSelf(this.bid.gameObject, this.auction.CanBid);
    if (this.buyoutCredits.gameObject.activeSelf != this.auction.CanBuyout(CurrencyType.Credits))
      NGUITools.SetActiveSelf(this.buyoutCredits.gameObject, this.auction.CanBuyout(CurrencyType.Credits));
    if (this.buyoutBlackDollars.gameObject.activeSelf != this.auction.CanBuyout(CurrencyType.BlackDollars))
      NGUITools.SetActiveSelf(this.buyoutBlackDollars.gameObject, this.auction.CanBuyout(CurrencyType.BlackDollars));
    this.actionBarGrid.Reposition();
  }

  private void ResetVisualState()
  {
    this.title.text = string.Empty;
    this.subTitle.text = string.Empty;
    this.description.text = string.Empty;
    this.itemPros.text = string.Empty;
    this.itemCons.text = string.Empty;
    this.equipPoints.text = string.Empty;
    this.durability.text = string.Empty;
    this.auctionDetails.text = string.Empty;
    this._widget.height = this.minHeight;
    NGUITools.SetActiveSelf(this.cancel.gameObject, false);
    NGUITools.SetActiveSelf(this.bid.gameObject, false);
    NGUITools.SetActiveSelf(this.buyoutCredits.gameObject, false);
    NGUITools.SetActiveSelf(this.buyoutBlackDollars.gameObject, false);
  }

  protected void LoadItemTexture()
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) this.auction.Item.Icon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((UnityEngine.Object) texture2D == (UnityEngine.Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.itemIcon.mainTexture = (Texture) texture2D;
  }

  private void Awake()
  {
    this._widget = this.GetComponent<UIWidget>();
    EventDelegate.Add(this.watchToggle.onChange, new EventDelegate.Callback(this.OnToggleWatch));
    NGUITools.SetActiveChildren(this.gameObject, false);
    this._widgetsAreVisible = false;
    this._defaultBorderColor = this.borderSprite.color;
  }

  private void Update()
  {
    if (this.auction == null)
      return;
    if (this._widgetsAreVisible != this._widget.isVisible)
    {
      NGUITools.SetActiveChildren(this.gameObject, this._widget.isVisible);
      this._widgetsAreVisible = this._widget.isVisible;
    }
    if (this.auction.Item.BindOnEquip)
      this.borderSprite.color = Color.red;
    else
      this.borderSprite.color = this._defaultBorderColor;
    if (this._lastChangedId == this.auction.LastChangeFlagID && (!this._updateOwner || this._ownerLastChangedId == this.auction.Owner.LastChangeFlagID))
      return;
    this.UpdateAuctionDetails();
    this.UpdateActionBar();
    this._lastChangedId = this.auction.LastChangeFlagID;
  }

  private void OnSpawned()
  {
    this.ResetVisualState();
    this._spawnedName = this.name;
  }

  private void OnDespawned()
  {
    this.name = this._spawnedName;
    this._lastChangedId = int.MinValue;
    this.auction = (ClientAuction) null;
    this.ResetVisualState();
  }
}
