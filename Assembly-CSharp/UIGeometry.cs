﻿// Decompiled with JetBrains decompiler
// Type: UIGeometry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIGeometry
{
  public BetterList<Vector3> verts = new BetterList<Vector3>();
  public BetterList<Vector2> uvs = new BetterList<Vector2>();
  public BetterList<Color> cols = new BetterList<Color>();
  private BetterList<Vector3> mRtpVerts = new BetterList<Vector3>();
  private Vector3 mRtpNormal;
  private Vector4 mRtpTan;

  public bool hasVertices
  {
    get
    {
      return this.verts.size > 0;
    }
  }

  public bool hasTransformed
  {
    get
    {
      if (this.mRtpVerts != null && this.mRtpVerts.size > 0)
        return this.mRtpVerts.size == this.verts.size;
      return false;
    }
  }

  public void Clear()
  {
    this.verts.Clear();
    this.uvs.Clear();
    this.cols.Clear();
    this.mRtpVerts.Clear();
  }

  public void ApplyTransform(Matrix4x4 widgetToPanel, bool generateNormals = true)
  {
    if (this.verts.size > 0)
    {
      this.mRtpVerts.Clear();
      int index = 0;
      for (int size = this.verts.size; index < size; ++index)
        this.mRtpVerts.Add(widgetToPanel.MultiplyPoint3x4(this.verts[index]));
      if (!generateNormals)
        return;
      this.mRtpNormal = widgetToPanel.MultiplyVector(Vector3.back).normalized;
      Vector3 normalized = widgetToPanel.MultiplyVector(Vector3.right).normalized;
      this.mRtpTan = new Vector4(normalized.x, normalized.y, normalized.z, -1f);
    }
    else
      this.mRtpVerts.Clear();
  }

  public void WriteToBuffers(BetterList<Vector3> v, BetterList<Vector2> u, BetterList<Color> c, BetterList<Vector3> n, BetterList<Vector4> t)
  {
    if (this.mRtpVerts == null || this.mRtpVerts.size <= 0)
      return;
    if (n == null)
    {
      for (int index = 0; index < this.mRtpVerts.size; ++index)
      {
        v.Add(this.mRtpVerts.buffer[index]);
        u.Add(this.uvs.buffer[index]);
        c.Add(this.cols.buffer[index]);
      }
    }
    else
    {
      for (int index = 0; index < this.mRtpVerts.size; ++index)
      {
        v.Add(this.mRtpVerts.buffer[index]);
        u.Add(this.uvs.buffer[index]);
        c.Add(this.cols.buffer[index]);
        n.Add(this.mRtpNormal);
        t.Add(this.mRtpTan);
      }
    }
  }
}
