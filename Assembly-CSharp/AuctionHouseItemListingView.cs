﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseItemListingView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using UnityEngine;

public class AuctionHouseItemListingView : DialogViewBase<AuctionHouseDialogContext>
{
  public AuctionHouseItemListingScrollView itemListing;
  public AuctionHousePooledItemScrollView pooledItemListing;
  private AuctionHouseItemListingScrollViewContext _scrollViewContext;
  public UIToggle alphaSortToggle;
  public UIToggle timeSortToggle;

  public void OpenFilterView()
  {
    if ((Object) this.itemListing != (Object) null)
      this.itemListing.Clear();
    this.mDialogContext.dialog.ShowFilterView();
  }

  public void ShowMyWatchList()
  {
    if ((Object) this.itemListing != (Object) null)
      this.itemListing.Clear();
    this._scrollViewContext.dialogContext.filter = (IAuctionFilter) new MyWatchListFilter();
    this.mDialogContext.dialog.subTitle.text = this.mDialogContext.filter.formattedName;
  }

  public void ShowMyAuctions()
  {
    if ((Object) this.itemListing != (Object) null)
      this.itemListing.Clear();
    this._scrollViewContext.dialogContext.filter = (IAuctionFilter) new ClientPlayerAuctionsFilter(TibProxy.gameState.MyPlayer, ClientPlayerAuctionsFilter.AuctionRole.Owner);
    this.mDialogContext.dialog.subTitle.text = this.mDialogContext.filter.formattedName;
  }

  public void OnSortOrderChanged()
  {
    if ((Object) UIToggle.current == (Object) this.alphaSortToggle && UIToggle.current.value)
    {
      this._scrollViewContext.sortOrder = AuctionHouseItemListingScrollViewContext.AuctionListingSortOrder.Alphabetical;
    }
    else
    {
      if (!((Object) UIToggle.current == (Object) this.timeSortToggle) || !UIToggle.current.value)
        return;
      this._scrollViewContext.sortOrder = AuctionHouseItemListingScrollViewContext.AuctionListingSortOrder.TimeRemaining;
    }
  }

  protected void Start()
  {
    this._scrollViewContext.dialogContext = this.mDialogContext;
    this.pooledItemListing.scrollViewContext = this._scrollViewContext;
  }

  protected override void OnShow()
  {
    this.mDialogContext.dialog.subTitle.text = this.mDialogContext.filter.formattedName;
    if (this._scrollViewContext.sortOrder == AuctionHouseItemListingScrollViewContext.AuctionListingSortOrder.Alphabetical)
      this.alphaSortToggle.value = true;
    if (this._scrollViewContext.sortOrder != AuctionHouseItemListingScrollViewContext.AuctionListingSortOrder.TimeRemaining)
      return;
    this.timeSortToggle.value = true;
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.mDialogContext.dialog.subTitle.text = string.Empty;
    if ((Object) this.itemListing != (Object) null)
      this.itemListing.Clear();
    Resources.UnloadUnusedAssets();
  }

  protected override void OnInitialize()
  {
    this._scrollViewContext = new AuctionHouseItemListingScrollViewContext();
    if ((Object) this.itemListing != (Object) null)
      this.itemListing.context = this._scrollViewContext;
    this.mDialogContext.dialog.subTitle.text = string.Empty;
  }
}
