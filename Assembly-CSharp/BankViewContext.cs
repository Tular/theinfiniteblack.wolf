﻿// Decompiled with JetBrains decompiler
// Type: BankViewContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;

public class BankViewContext : IItemScrollViewContext, IGeneralScrollViewContext<EquipmentItem>
{
  public ItemViewContext viewContext
  {
    get
    {
      return ItemViewContext.Bank;
    }
  }

  public ItemType itemType { get; set; }

  public bool hasChanged
  {
    get
    {
      return true;
    }
  }

  public IEnumerable<EquipmentItem> items
  {
    get
    {
      return TibProxy.gameState.Bank.get_Items().Where<EquipmentItem>((Func<EquipmentItem, bool>) (i => i.Type == this.itemType));
    }
  }
}
