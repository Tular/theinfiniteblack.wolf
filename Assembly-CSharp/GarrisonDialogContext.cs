﻿// Decompiled with JetBrains decompiler
// Type: GarrisonDialogContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Entities;

public class GarrisonDialogContext
{
  private Garrison _garrison;

  public string FormattedTitleText { get; private set; }

  public Garrison garrison
  {
    get
    {
      return this._garrison;
    }
    set
    {
      this._garrison = value;
      if (this._garrison == null)
        this.FormattedTitleText = string.Empty;
      else
        this.FormattedTitleText = string.Format("{0}'s GARRISON RANK {1}", (object) this._garrison.SubTitle, (object) Util.NUMERALS[value.Level]);
    }
  }
}
