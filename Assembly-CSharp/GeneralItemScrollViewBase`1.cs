﻿// Decompiled with JetBrains decompiler
// Type: GeneralItemScrollViewBase`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;
using UnityEngine;

public abstract class GeneralItemScrollViewBase<T> : GeneralScrollViewBase<EquipmentItem, T> where T : IItemScrollViewContext
{
  public UITable cardTableRoot;

  protected override void OnRefreshView()
  {
    this.cardTableRoot.repositionNow = true;
  }

  protected override GeneralScrollViewCardBase InitializeCard(Transform cardTrans, EquipmentItem item)
  {
    EquipmentItemCardBase component = cardTrans.GetComponent<EquipmentItemCardBase>();
    component.InitializeCard(item, this.context.viewContext);
    return (GeneralScrollViewCardBase) component;
  }

  protected override void OnDisable()
  {
    base.OnDisable();
    Resources.UnloadUnusedAssets();
  }
}
