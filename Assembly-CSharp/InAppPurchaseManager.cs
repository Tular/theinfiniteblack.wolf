﻿// Decompiled with JetBrains decompiler
// Type: InAppPurchaseManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using Spellbook.Unity.Steam;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;
using UnityEngine.Purchasing;

[AdvancedInspector.AdvancedInspector]
public class InAppPurchaseManager : PreInstantiatedSingleton<InAppPurchaseManager>, IIapOrderManager
{
  [Inspect(3)]
  public string purchaseCompleteMessage = "Your order of {0} has been successful. Please allow a few minutes for delivery.";
  [Inspect(4)]
  public string purchaseCancledMessage = "Your purchase was canceled;";
  [Inspect(5)]
  public string purchaseFailedMessage = "There was an error with your purchase. Your account was not charged.";
  [Inspect(0)]
  public string paymentService;
  [Inspect(1)]
  public float purchasePostInterval;
  [Inspect(2)]
  public string purchaseHistoryFileName;
  [Inspect(9)]
  public bool debugOutput;
  [Inspect(10)]
  public string googlePlayPublicKey;
  [Inspect(11)]
  [Descriptor(Name = "iOS SKU")]
  public string iosSku;
  [Inspect(20)]
  public List<ProductInfo> products;
  private PendingOrders _pendingOrders;
  private PurchaseHistoryFileManager _fileManager;
  private IInAppPurchasingContext _iapContext;
  private string _processingProductId;

  private bool billingSupported
  {
    get
    {
      if (TibProxy.Instance.distribution != DistributionType.Android_GooglePlay && TibProxy.Instance.distribution != DistributionType.iOS && (TibProxy.Instance.distribution != DistributionType.Steam_OSX && TibProxy.Instance.distribution != DistributionType.Steam_Linux))
        return TibProxy.Instance.distribution == DistributionType.Steam_Windows;
      return true;
    }
  }

  [Inspect(30)]
  public void PurchaseTenBlackDollars()
  {
    if (!TibProxy.Instance.isSteamDistribution)
      return;
    this.StartSteamPurchase(this.products.FirstOrDefault<ProductInfo>((Func<ProductInfo, bool>) (p => p.billerProductId == "tibunity.bd10")).steamProductId);
  }

  [Inspect(31)]
  public void PurchaseOneHundredBlackDollars()
  {
    if (!TibProxy.Instance.isSteamDistribution)
      return;
    this.StartSteamPurchase(this.products.FirstOrDefault<ProductInfo>((Func<ProductInfo, bool>) (p => p.billerProductId == "tibunity.bd100")).steamProductId);
  }

  [Inspect(32)]
  public void PurchaseFiveHundredBlackDollars()
  {
    if (!TibProxy.Instance.isSteamDistribution)
      return;
    this.StartSteamPurchase(this.products.FirstOrDefault<ProductInfo>((Func<ProductInfo, bool>) (p => p.billerProductId == "tibunity.bd500")).steamProductId);
  }

  protected void StartSteamPurchase(string productId)
  {
    if (TibProxy.gameState == null || !TibProxy.gameState.IsLoggedIn || (TibProxy.gameState.MyPlayer == null || string.IsNullOrEmpty(TibProxy.gameState.MyPlayer.Name)))
      return;
    if (!SbSteamManager.Initialized)
    {
      PopupMessageDialog popupMessageDialog = DialogManager.ShowDialogAsPopup<PopupMessageDialog>();
      popupMessageDialog.messageText.text = "Error connecting to Steam client. Please close TIB and restart from Steam";
      popupMessageDialog.canClose = true;
    }
    else
      SbSteamComponent<SbSteamMicroTxnManager>.Instance.StartStartSbSteamPurchaseTxn(productId, TibProxy.gameState.MyPlayer.Name);
  }

  protected override void Awake()
  {
    base.Awake();
    if (!this.billingSupported)
    {
      this.enabled = false;
    }
    else
    {
      this._fileManager = new PurchaseHistoryFileManager();
      this.LogD("Loading pending orders file");
      this._pendingOrders = XmlFileHelper.Load<PendingOrders>((IFileManager) this._fileManager, this.purchaseHistoryFileName);
      if (this._pendingOrders != null)
        return;
      this.LogD("Pending orders file not found, creating.");
      this._pendingOrders = new PendingOrders();
      XmlFileHelper.Save<PendingOrders>((IFileManager) this._fileManager, this.purchaseHistoryFileName, this._pendingOrders);
    }
  }

  private void Start()
  {
    if (!this.billingSupported)
      return;
    if (Application.isEditor)
    {
      this._iapContext = Unibiller.Create((IIapOrderManager) this, (IPurchaseProcessor) new MockPurchaseProcessor(), this.products.Cast<IProductInfo>());
      this.LogI("Biller created using MockPurchaseProcessor");
    }
    else
    {
      switch (TibProxy.Instance.distribution)
      {
        case DistributionType.Android_GooglePlay:
          this._iapContext = Unibiller.Create((IIapOrderManager) this, (IPurchaseProcessor) new GooglePlayPurchaseProcessor(this.googlePlayPublicKey), this.products.Cast<IProductInfo>());
          break;
        case DistributionType.iOS:
          this._iapContext = Unibiller.Create((IIapOrderManager) this, (IPurchaseProcessor) new AppleAppStorePurchaseProcessor(), this.products.Cast<IProductInfo>());
          break;
      }
    }
  }

  private void OnEnable()
  {
  }

  private void OnDisable()
  {
    if (!this.billingSupported)
      return;
    XmlFileHelper.Save<PendingOrders>((IFileManager) this._fileManager, this.purchaseHistoryFileName, this._pendingOrders);
  }

  public void OnPurchaseFailedCallback(Product product, PurchaseFailureReason purchaseFailureReason)
  {
    DialogManager.ShowDialog<PopupMessageDialog>((object) ((Enum) purchaseFailureReason).ToString(), (object) false);
    this.LogD("Unibill_OnPurchaseFailedCallback()");
    this._processingProductId = string.Empty;
  }

  public void OnProcessPurchaseCallback(PurchaseEventArgs purchaseEventArgs)
  {
    if (this.debugOutput)
      DialogManager.ShowDialog<PopupMessageDialog>((object) "Processing purchase", (object) true);
    PendingOrderDetail receipt = this._iapContext.iapProcessor.ParseReceipt(this._iapContext, purchaseEventArgs.purchasedProduct.receipt);
    this._pendingOrders.orders.Add(receipt);
    XmlFileHelper.Save<PendingOrders>((IFileManager) this._fileManager, this.purchaseHistoryFileName, this._pendingOrders);
    this.LogI("OnProcessPurchaseCallback() - processingProductId: " + this._processingProductId);
    if (!string.IsNullOrEmpty(this._processingProductId))
      DialogManager.ShowDialog<PopupMessageDialog>((object) string.Format(this.purchaseCompleteMessage, (object) this._iapContext.iapStore.GetProductName(this._processingProductId)), (object) false);
    if (this.debugOutput)
      DialogManager.ShowDialog<PopupMessageDialog>((object) receipt.ToString(), (object) true);
    this.LogD("Saving pending order");
    this._processingProductId = string.Empty;
  }

  public void OnInitializeFailedCallback(InitializationFailureReason initializationFailureReason)
  {
    if (this.debugOutput)
      DialogManager.ShowDialog<PopupMessageDialog>((object) ("Billing initialization failed. " + (object) initializationFailureReason), (object) true);
    this.LogD("Unibill_OnInitializeFailedCallback() - " + (object) initializationFailureReason);
  }

  public void OnInitalizedCallback()
  {
    this.LogD("Unibill_OnInitalizedCallback");
  }

  private void Purchase(string productId)
  {
    if (!this.billingSupported)
      return;
    this._processingProductId = productId;
    this._iapContext.TryPurchase(productId);
  }

  [DebuggerHidden]
  private IEnumerator PostPurchaseCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new InAppPurchaseManager.\u003CPostPurchaseCoroutine\u003Ec__Iterator2E()
    {
      \u003C\u003Ef__this = this
    };
  }
}
