﻿// Decompiled with JetBrains decompiler
// Type: PlanetTerraformingView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public class PlanetTerraformingView : MonoBehaviour
{
  public UILabel researchProgressLabel;
  public UILabel nextUnlockLabel;
  public UILabel researchUnitValue;
  public ShipProfileTexture shipProfile;
  public string viewTitle;

  public PlanetDialogContext dialogContext { get; set; }

  private void UpdateVisuals()
  {
    if (this.dialogContext == null || this.dialogContext.planet == null)
      return;
    this.researchProgressLabel.text = string.Format("{0:0.0#####}", (object) this.dialogContext.planet.Progress);
    this.researchUnitValue.text = string.Format("1 Research Unit (RU) = {0}", (object) this.dialogContext.planet.Class.ResearchUnitProgressValue());
    int num = int.MinValue;
    for (int index = 0; index < 18; ++index)
    {
      if (!this.dialogContext.planet.Class.CanBuildShip((ShipClass) index))
      {
        num = index;
        break;
      }
    }
    if (num == int.MinValue)
    {
      this.nextUnlockLabel.text = "-= All Unlocked =-";
      this.shipProfile.Hide();
    }
    else
    {
      ShipClass shipClass = (ShipClass) num;
      this.nextUnlockLabel.text = ((Enum) shipClass).ToString();
      this.shipProfile.Show(shipClass);
    }
  }

  private void ResetVisuals()
  {
    this.researchProgressLabel.text = string.Empty;
    this.nextUnlockLabel.text = string.Empty;
    this.researchUnitValue.text = string.Empty;
    this.shipProfile.Unload();
  }

  public void Show()
  {
    this.gameObject.SetActive(true);
    this.dialogContext.dialogSubtitleText = this.viewTitle;
    this.UpdateVisuals();
  }

  public void Hide()
  {
    this.ResetVisuals();
    this.gameObject.SetActive(false);
  }

  private void Update()
  {
    this.UpdateVisuals();
  }
}
