﻿// Decompiled with JetBrains decompiler
// Type: IPooledScrollViewCellInifoCollection`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public interface IPooledScrollViewCellInifoCollection<T> where T : class, IScrollViewCellInfo
{
  int Count { get; }

  IEnumerable<T> values { get; }

  T first { get; }

  T last { get; }

  bool dataWasReset { get; }

  bool dataWasModified { get; }

  void Add(T cellInfo);

  void Remove(T cellData);

  void Clear();
}
