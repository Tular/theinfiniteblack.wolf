﻿// Decompiled with JetBrains decompiler
// Type: GeneralItemCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using System.Collections;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class GeneralItemCard : EquipmentItemCardBase
{
  public UILabel title;
  public UILabel subTitle;
  public UILabel description;
  public UILabel itemPros;
  public UILabel itemCons;
  public UILabel equipPoints;
  public UILabel durability;
  public UILabel creditValue;
  public UILabel specialNote;
  public SBUIButton equip;
  public SBUIButton unequip;
  public SBUIButton toBank;
  public SBUIButton toInventory;
  public SBUIButton toAuction;
  public SBUIButton scrap;
  public SBUIButton jettison;
  public SBUIButton upgrade;
  public TweenHeight actionBarTween;
  public UIGrid actionBarGrid;
  public GameObject equippedIndicator;
  public GameObject highlightRoot;
  public int minHeight;
  private bool _actionBarEnabled;

  public override bool isEquipped
  {
    get
    {
      return this.mIsEquipped;
    }
    set
    {
      if (value)
      {
        this.gameObject.name = "_" + this.item.LongName;
        NGUITools.SetActiveSelf(this.equippedIndicator, true);
      }
      else
      {
        this.gameObject.name = this.item.LongName;
        NGUITools.SetActiveSelf(this.equippedIndicator, false);
      }
      this.mIsEquipped = value;
    }
  }

  public override void EquipItem()
  {
    TibProxy.gameState.DoItemEquip(this.item);
  }

  public override void UnEquipItem()
  {
    TibProxy.gameState.DoItemUnequip(this.item);
  }

  public override void ItemToBank()
  {
    TibProxy.gameState.DoItemToBank(this.item);
  }

  public override void ItemToInventory()
  {
    TibProxy.gameState.DoItemFromBank(this.item);
  }

  public override void JettisonItem()
  {
    TibProxy.gameState.DoItemJettison(this.item);
  }

  public override void UpgradeItem()
  {
    DialogManager.ShowDialog<ItemEngineeringDialog>(new object[1]
    {
      (object) this.item
    });
  }

  public override void BuyItem()
  {
  }

  public override void SellItem()
  {
    TibProxy.gameState.DoItemSell(TibProxy.gameState.Local.StarPort, this.item);
  }

  public override void InitializeCard(EquipmentItem itm)
  {
    base.InitializeCard(itm);
    this.ResetVisualState();
  }

  protected override void UpdateVisuals()
  {
    base.UpdateVisuals();
    StringBuilder stringBuilder1 = new StringBuilder(500);
    StringBuilder stringBuilder2 = new StringBuilder(500);
    StringBuilder stringBuilder3 = new StringBuilder(500);
    StringBuilder stringBuilder4 = new StringBuilder(500);
    StringBuilder stringBuilder5 = new StringBuilder(500);
    this.item.AppendName(stringBuilder1);
    Markup.GetNGUI(stringBuilder1);
    this.title.text = stringBuilder1.ToString();
    stringBuilder2.Append(this.item.Rarity.GetMarkup());
    this.item.AppendSubTitle(stringBuilder2);
    stringBuilder2.Append("[-] ");
    Markup.GetNGUI(stringBuilder2);
    this.subTitle.text = stringBuilder2.ToString();
    stringBuilder3.Append("[d1f1ff]");
    this.item.AppendDescription(stringBuilder3);
    stringBuilder3.Append("[-]");
    this.description.text = stringBuilder3.ToString();
    this.item.AppendProsAndCons(stringBuilder4, stringBuilder5);
    stringBuilder4.Insert(0, "[00ff00]");
    stringBuilder5.Insert(0, "[ff4400]");
    stringBuilder4.Append("[-]");
    stringBuilder5.Append("[-]");
    this.itemPros.text = stringBuilder4.ToString();
    this.itemCons.text = stringBuilder5.ToString();
    this.itemPros.UpdateAnchors();
    this.itemCons.UpdateAnchors();
    int num = this.creditValue.height - this.creditValue.topAnchor.absolute + this.creditValue.bottomAnchor.absolute;
    int val2 = Mathf.RoundToInt((float) (-(double) this.itemCons.transform.localPosition.y + (double) this.itemCons.height + (double) num + 4.0));
    this.mWidget.height = Math.Max(this.minHeight, Math.Max(Mathf.RoundToInt((float) (-(double) this.itemPros.transform.localPosition.y + (double) this.itemPros.height + (double) num + 4.0)), val2)) + 12;
    this.mWidget.UpdateAnchors();
    this.creditValue.parent.transform.localPosition = new Vector3(this.creditValue.parent.transform.localPosition.x, (float) ((double) this.mWidget.localCorners[3].y + (double) num + 12.0));
    this.specialNote.parent.transform.localPosition = new Vector3(0.0f, (float) ((double) this.mWidget.localCorners[3].y + (double) num + 12.0));
    this.actionBarTween.transform.localPosition = new Vector3(this.actionBarTween.transform.localPosition.x, this.mWidget.localCorners[0].y + (float) this.actionBarTween.to - (float) this.actionBarTween.from);
    this.equipPoints.text = string.Format("{0}", (object) this.item.EPCost);
    this.durability.text = string.Format("{0}%", (object) this.item.Durability);
    this.creditValue.text = string.Format("${0}", (object) this.item.StarPortSellValue);
    this.UpdateActionBar();
    IEnumerator enumerator = this.transform.GetEnumerator();
    try
    {
      while (enumerator.MoveNext())
      {
        UIWidget component = ((Component) enumerator.Current).GetComponent<UIWidget>();
        if ((bool) ((UnityEngine.Object) component))
          component.UpdateAnchors();
      }
    }
    finally
    {
      IDisposable disposable = enumerator as IDisposable;
      if (disposable != null)
        disposable.Dispose();
    }
  }

  protected void UpdateActionBar()
  {
    switch (this.viewContext)
    {
      case ItemViewContext.InventorySelf:
        this._actionBarEnabled = true;
        NGUITools.SetActiveSelf(this.equippedIndicator.gameObject, this.isEquipped);
        NGUITools.SetActiveSelf(this.unequip.gameObject, this.isEquipped);
        NGUITools.SetActiveSelf(this.equip.gameObject, !this.isEquipped);
        NGUITools.SetActiveSelf(this.toBank.gameObject, !this.isEquipped);
        NGUITools.SetActiveSelf(this.toInventory.gameObject, false);
        NGUITools.SetActiveSelf(this.toAuction.gameObject, !this.isEquipped && this.item.CanAuction);
        NGUITools.SetActiveSelf(this.scrap.gameObject, !this.isEquipped);
        NGUITools.SetActiveSelf(this.jettison.gameObject, !this.isEquipped);
        NGUITools.SetActiveSelf(this.upgrade.gameObject, !this.isEquipped && this.item.Rarity > ItemRarity.COMMON);
        break;
      case ItemViewContext.InventoryOther:
        this._actionBarEnabled = false;
        NGUITools.SetActiveSelf(this.equippedIndicator.gameObject, this.isEquipped);
        NGUITools.SetActiveSelf(this.unequip.gameObject, false);
        NGUITools.SetActiveSelf(this.equip.gameObject, false);
        NGUITools.SetActiveSelf(this.toBank.gameObject, false);
        NGUITools.SetActiveSelf(this.toInventory.gameObject, false);
        NGUITools.SetActiveSelf(this.toAuction.gameObject, false);
        NGUITools.SetActiveSelf(this.scrap.gameObject, false);
        NGUITools.SetActiveSelf(this.jettison.gameObject, false);
        NGUITools.SetActiveSelf(this.upgrade.gameObject, false);
        break;
      case ItemViewContext.Bank:
        this._actionBarEnabled = true;
        NGUITools.SetActiveSelf(this.equippedIndicator.gameObject, false);
        NGUITools.SetActiveSelf(this.unequip.gameObject, false);
        NGUITools.SetActiveSelf(this.equip.gameObject, false);
        NGUITools.SetActiveSelf(this.toBank.gameObject, false);
        NGUITools.SetActiveSelf(this.toInventory.gameObject, true);
        NGUITools.SetActiveSelf(this.scrap.gameObject, true);
        NGUITools.SetActiveSelf(this.jettison.gameObject, false);
        NGUITools.SetActiveSelf(this.upgrade.gameObject, !this.isEquipped && this.item.Rarity > ItemRarity.COMMON);
        if (this.item.Rarity == ItemRarity.COMMON || this.item.NoDrop || !this.item.CanAuction)
        {
          NGUITools.SetActiveSelf(this.toAuction.gameObject, false);
          this.LogD(string.Format("{0} - setting specialNoteText", (object) this.name));
          this.specialNote.text = "ITEM CANNOT BE AUCTIONED";
        }
        else
        {
          NGUITools.SetActiveSelf(this.toAuction.gameObject, true);
          this.specialNote.text = string.Empty;
        }
        NGUITools.SetActiveSelf(this.specialNote.parent.gameObject, !string.IsNullOrEmpty(this.specialNote.text));
        break;
    }
  }

  protected override void OnDespawned()
  {
    base.OnDespawned();
    this.ResetVisualState();
  }

  private void OnToggleChanged()
  {
    if (!this.enabled || !this._actionBarEnabled)
      return;
    if ((UnityEngine.Object) UIToggle.current != (UnityEngine.Object) null && UIToggle.current.value)
    {
      NGUITools.SetActiveSelf(this.actionBarTween.gameObject, true);
      this.actionBarGrid.Reposition();
      this.actionBarTween.PlayForward();
    }
    else
    {
      if (!((UnityEngine.Object) UIToggle.current == (UnityEngine.Object) null) && UIToggle.current.value)
        return;
      this.actionBarTween.PlayReverse();
    }
  }

  private void ResetVisualState()
  {
    this.title.text = string.Empty;
    this.subTitle.text = string.Empty;
    this.description.text = string.Empty;
    this.itemPros.text = string.Empty;
    this.itemCons.text = string.Empty;
    this.equipPoints.text = string.Empty;
    this.durability.text = string.Empty;
    this.creditValue.text = string.Empty;
    this.specialNote.text = string.Empty;
    this.equip.SetState(SBUIButtonComponent.State.Normal);
    this.unequip.SetState(SBUIButtonComponent.State.Normal);
    this.toBank.SetState(SBUIButtonComponent.State.Normal);
    this.toInventory.SetState(SBUIButtonComponent.State.Normal);
    this.toAuction.SetState(SBUIButtonComponent.State.Normal);
    this.scrap.SetState(SBUIButtonComponent.State.Normal);
    this.jettison.SetState(SBUIButtonComponent.State.Normal);
    this.upgrade.SetState(SBUIButtonComponent.State.Normal);
    this.mWidget.height = this.minHeight;
    this.mIsEquipped = false;
    this.ResetActionBarState();
    NGUITools.SetActiveSelf(this.highlightRoot.gameObject, false);
    NGUITools.SetActiveSelf(this.equippedIndicator.gameObject, false);
    NGUITools.SetActiveSelf(this.unequip.gameObject, false);
    NGUITools.SetActiveSelf(this.equip.gameObject, false);
    NGUITools.SetActiveSelf(this.toBank.gameObject, false);
    NGUITools.SetActiveSelf(this.toInventory.gameObject, false);
    NGUITools.SetActiveSelf(this.toAuction.gameObject, false);
    NGUITools.SetActiveSelf(this.scrap.gameObject, false);
    NGUITools.SetActiveSelf(this.jettison.gameObject, false);
    NGUITools.SetActiveSelf(this.upgrade.gameObject, false);
    NGUITools.SetActiveSelf(this.specialNote.gameObject, false);
  }

  private void ResetActionBarState()
  {
    this._actionBarEnabled = false;
    this.toggle.value = false;
    this.actionBarTween.PlayForward();
    this.actionBarTween.ResetToBeginning();
    NGUITools.SetActiveSelf(this.actionBarTween.gameObject, false);
  }

  private void OnActionBarTweenFinished()
  {
    if (!this.enabled || !this._actionBarEnabled)
      return;
    UIScrollView inParents = NGUITools.FindInParents<UIScrollView>(this.actionBarGrid.transform);
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(inParents.transform, this.transform);
    if (this.actionBarTween.direction == AnimationOrTween.Direction.Reverse)
    {
      NGUITools.SetActiveSelf(this.actionBarTween.gameObject, false);
    }
    else
    {
      Vector3 constrainOffset = inParents.panel.CalculateConstrainOffset((Vector2) relativeWidgetBounds.min, (Vector2) relativeWidgetBounds.max);
      if ((double) constrainOffset.sqrMagnitude <= 0.100000001490116)
        return;
      Vector3 pos = inParents.transform.localPosition + constrainOffset;
      pos.x = Mathf.Round(pos.x);
      pos.y = Mathf.Round(pos.y);
      SpringPosition.Begin(inParents.panel.gameObject, pos, 26f);
    }
  }

  private void Start()
  {
    EventDelegate.Add(this.equip.onClick, new EventDelegate.Callback(((EquipmentItemCardBase) this).EquipItem));
    EventDelegate.Add(this.unequip.onClick, new EventDelegate.Callback(((EquipmentItemCardBase) this).UnEquipItem));
    EventDelegate.Add(this.toggle.onChange, new EventDelegate.Callback(this.OnToggleChanged));
    EventDelegate.Add(this.actionBarTween.onFinished, new EventDelegate.Callback(this.OnActionBarTweenFinished), false);
  }
}
