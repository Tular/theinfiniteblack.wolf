﻿// Decompiled with JetBrains decompiler
// Type: ShipStatsFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;

public class ShipStatsFormatter
{
  public static string FormattedOffensiveStats(Ship ship)
  {
    float num1 = (float) ship.AttackSpeedMS / 1000f;
    int minimumDamage = ship.MinimumDamage;
    float hitChance = ship.HitChance;
    float num2 = ship.OutgoingDamageMod * 100f;
    float num3 = (float) (100.0 + (double) ship.CriticalDamageMod * 100.0);
    float criticalChance = ship.CriticalChance;
    float splashChance = ship.SplashChance;
    float grappleChance = ship.GrappleChance;
    float stunChance = ship.StunChance;
    float num4 = (float) ship.StunDurationMSAdjust / 1000f;
    float num5 = ship.XpMod * 100f;
    float num6 = (float) ship.MoveSpeedMS / 1000f;
    if (ship.Player == TibProxy.gameState.MyPlayer)
    {
      ClientBuyables buyables = TibProxy.gameState.Buyables;
      if (buyables.SuperCharge > 0 && !buyables.SuspendSuperCharge)
        criticalChance += 5f;
      if (buyables.HyperDrive > 0 && !buyables.SuspendHyperDrive)
        num6 /= 2f;
    }
    StringBuilder stringBuilder = new StringBuilder(500);
    stringBuilder.AppendLine(string.Format("{0} to {1} Damage / {2:F2}s", (object) minimumDamage, (object) (minimumDamage * 2), (object) num1));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Hit Chance", (object) hitChance));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0}% Damage", (object) num2));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0}% Critical Damage", (object) num3));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Critical Chance", (object) criticalChance));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Splash Chance", (object) splashChance));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Grapple Chance", (object) grappleChance));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Stun Chance", (object) stunChance));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Stun Duration\n", (object) num4));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0}% XP Gain", (object) num5));
    stringBuilder.Append(string.Format("{0:+0.#;-0.#;+0}s Move Speed", (object) num6));
    return stringBuilder.ToString();
  }

  public static string FormattedDefensiveStats(Ship ship)
  {
    int maxHull = ship.MaxHull;
    float evasionChance = ship.EvasionChance;
    float num1 = ship.IncomingDamageMod * 100f;
    float num2 = (float) (100.0 + (double) ship.MetalRepairMod * 100.0);
    int resourceCapacity = ship.ResourceCapacity;
    int harvestCapacity = ship.HarvestCapacity;
    float stunResist = ship.StunResist;
    float grappleResist = ship.GrappleResist;
    float criticalResist = ship.CriticalResist;
    float splashResist = ship.SplashResist;
    float num3 = (float) ship.HarvestSpeedMS / 1000f;
    if (ship.Player == TibProxy.gameState.MyPlayer)
    {
      ClientBuyables buyables = TibProxy.gameState.Buyables;
      if (buyables.Tactics > 0 && !buyables.SuspendTactics)
        evasionChance += 5f;
      if (buyables.Accelerate > 0)
        num3 /= 2f;
    }
    StringBuilder stringBuilder = new StringBuilder(500);
    stringBuilder.AppendLine(string.Format("{0:#,###,###} Max Hull", (object) maxHull));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Evasion", (object) evasionChance));
    stringBuilder.AppendLine((double) num1 > 0.0 ? string.Format("{0:+0.#;-0.#;+0}% Damage Vulnerable", (object) num1) : string.Format("{0:+0.#;-0.#;+0}% Damage Reduction", (object) num1));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0}% Metal Hull Repair", (object) num2));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Critical Resist", (object) criticalResist));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Splash Resist", (object) splashResist));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Grapple Resist", (object) grappleResist));
    stringBuilder.AppendLine(string.Format("{0:+0.#;-0.#;+0} Stun Resist", (object) stunResist));
    stringBuilder.AppendLine(string.Format("{0:+#.#;-#.#;+0}s Harvest Speed", (object) num3));
    stringBuilder.AppendLine(string.Format("{0:#,###,###} Harvest Capacity", (object) harvestCapacity));
    stringBuilder.Append(string.Format("{0:#,###,###} Resource Capacity", (object) resourceCapacity));
    return stringBuilder.ToString();
  }
}
