﻿// Decompiled with JetBrains decompiler
// Type: Unibiller
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using TheInfiniteBlack.Unity;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

public class Unibiller : IStoreListener, IStoreInterface
{
  private readonly Dictionary<string, string> _productNameLookup = new Dictionary<string, string>(10);
  private readonly System.Action<PurchaseEventArgs> OnProcessPurchaseCallback;
  private readonly System.Action<Product, PurchaseFailureReason> OnPurchaseFailedCallback;
  private readonly System.Action OnInitalizedCallback;
  private readonly System.Action<InitializationFailureReason> OnInitializeFailedCallback;
  private IStoreController _controller;
  private IExtensionProvider _extensions;

  private Unibiller(IIapOrderManager iapManager, IPurchaseProcessor iapProcessor, IEnumerable<IProductInfo> products)
  {
    ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance((IPurchasingModule) StandardPurchasingModule.Instance());
    foreach (IProductInfo product in products)
    {
      product.AddProductConfig(configurationBuilder);
      this._productNameLookup.Add(product.billerProductId, product.productName);
    }
    if (iapProcessor != null)
      iapProcessor.ConfigureBuilder(configurationBuilder);
    this.OnInitalizedCallback += new System.Action(iapManager.OnInitalizedCallback);
    this.OnInitializeFailedCallback += new System.Action<InitializationFailureReason>(iapManager.OnInitializeFailedCallback);
    this.OnProcessPurchaseCallback += new System.Action<PurchaseEventArgs>(iapManager.OnProcessPurchaseCallback);
    this.OnPurchaseFailedCallback += new System.Action<Product, PurchaseFailureReason>(iapManager.OnPurchaseFailedCallback);
    UnityPurchasing.Initialize((IStoreListener) this, configurationBuilder);
  }

  public static IInAppPurchasingContext Create(IIapOrderManager iapManager, IPurchaseProcessor iapProcessor, IEnumerable<IProductInfo> products)
  {
    if (iapManager == null || iapProcessor == null || products == null)
      throw new Exception("Parameters are invalid.");
    Unibiller unibiller = new Unibiller(iapManager, iapProcessor, products);
    return (IInAppPurchasingContext) new InAppPurchasingContext(iapManager, iapProcessor, (IStoreInterface) unibiller);
  }

  public bool IsInitialized
  {
    get
    {
      if (this._controller != null)
        return null != this._extensions;
      return false;
    }
  }

  public string GetProductName(string productId)
  {
    if (this._productNameLookup.ContainsKey(productId))
      return this._productNameLookup[productId];
    ClientLog.Instance.D((object) this, nameof (GetProductName), "ProductId not found in productNameLookup: " + productId);
    return string.Empty;
  }

  public void InitiatePurchase(string productId, string payload)
  {
    try
    {
      if (this.IsInitialized)
      {
        Product product = this._controller.products.WithID(productId);
        if (product != null && product.availableToPurchase)
          this._controller.InitiatePurchase(product, payload);
        else
          Debug.Log((object) ("Unibiller.InitiatePurchase() - Product lookup failed on Id: " + productId));
      }
      else
        Debug.Log((object) "Unibiller.InitiatePurchase() - Purchasing has not succeeded initializing yet");
    }
    catch (Exception ex)
    {
      Debug.LogException(ex);
    }
  }

  public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
  {
    TheInfiniteBlack.Library.Log.D((object) this, "OnInitialize", string.Empty);
    this._controller = controller;
    this._extensions = extensions;
    if (this.OnInitalizedCallback == null)
      return;
    this.OnInitalizedCallback();
  }

  public void OnInitializeFailed(InitializationFailureReason error)
  {
    TheInfiniteBlack.Library.Log.D((object) this, nameof (OnInitializeFailed), ((Enum) error).ToString());
    if (this.OnInitializeFailedCallback == null)
      return;
    this.OnInitializeFailedCallback(error);
  }

  public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
  {
    TheInfiniteBlack.Library.Log.D((object) this, nameof (ProcessPurchase), e.purchasedProduct.receipt);
    if (this.OnProcessPurchaseCallback != null)
      this.OnProcessPurchaseCallback(e);
    return PurchaseProcessingResult.Complete;
  }

  public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
  {
    if (this.OnPurchaseFailedCallback == null)
      return;
    this.OnPurchaseFailedCallback(i, p);
  }
}
