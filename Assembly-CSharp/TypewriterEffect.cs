﻿// Decompiled with JetBrains decompiler
// Type: TypewriterEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text;
using UnityEngine;

[RequireComponent(typeof (UILabel))]
[AddComponentMenu("NGUI/Interaction/Typewriter Effect")]
public class TypewriterEffect : MonoBehaviour
{
  public int charsPerSecond = 20;
  public List<EventDelegate> onFinished = new List<EventDelegate>();
  private string mFullText = string.Empty;
  private bool mReset = true;
  private BetterList<TypewriterEffect.FadeEntry> mFade = new BetterList<TypewriterEffect.FadeEntry>();
  public static TypewriterEffect current;
  public float fadeInTime;
  public float delayOnPeriod;
  public float delayOnNewLine;
  public UIScrollView scrollView;
  public bool keepFullDimensions;
  private UILabel mLabel;
  private int mCurrentOffset;
  private float mNextChar;
  private bool mActive;

  public bool isActive
  {
    get
    {
      return this.mActive;
    }
  }

  public void ResetToBeginning()
  {
    this.Finish();
    this.mReset = true;
    this.mActive = true;
    this.mNextChar = 0.0f;
    this.mCurrentOffset = 0;
    this.Update();
  }

  public void Finish()
  {
    if (!this.mActive)
      return;
    this.mActive = false;
    if (!this.mReset)
    {
      this.mCurrentOffset = this.mFullText.Length;
      this.mFade.Clear();
      this.mLabel.text = this.mFullText;
    }
    if (this.keepFullDimensions && (Object) this.scrollView != (Object) null)
      this.scrollView.UpdatePosition();
    TypewriterEffect.current = this;
    EventDelegate.Execute(this.onFinished);
    TypewriterEffect.current = (TypewriterEffect) null;
  }

  private void OnEnable()
  {
    this.mReset = true;
    this.mActive = true;
  }

  private void OnDisable()
  {
    this.Finish();
  }

  private void Update()
  {
    if (!this.mActive)
      return;
    if (this.mReset)
    {
      this.mCurrentOffset = 0;
      this.mReset = false;
      this.mLabel = this.GetComponent<UILabel>();
      this.mFullText = this.mLabel.processedText;
      this.mFade.Clear();
      if (this.keepFullDimensions && (Object) this.scrollView != (Object) null)
        this.scrollView.UpdatePosition();
    }
    if (string.IsNullOrEmpty(this.mFullText))
      return;
    while (this.mCurrentOffset < this.mFullText.Length && (double) this.mNextChar <= (double) RealTime.time)
    {
      int mCurrentOffset = this.mCurrentOffset;
      this.charsPerSecond = Mathf.Max(1, this.charsPerSecond);
      if (this.mLabel.supportEncoding)
      {
        while (NGUIText.ParseSymbol(this.mFullText, ref this.mCurrentOffset))
          ;
      }
      ++this.mCurrentOffset;
      if (this.mCurrentOffset <= this.mFullText.Length)
      {
        float num = 1f / (float) this.charsPerSecond;
        char ch = mCurrentOffset >= this.mFullText.Length ? '\n' : this.mFullText[mCurrentOffset];
        if ((int) ch == 10)
          num += this.delayOnNewLine;
        else if (mCurrentOffset + 1 == this.mFullText.Length || (int) this.mFullText[mCurrentOffset + 1] <= 32)
        {
          if ((int) ch == 46)
          {
            if (mCurrentOffset + 2 < this.mFullText.Length && (int) this.mFullText[mCurrentOffset + 1] == 46 && (int) this.mFullText[mCurrentOffset + 2] == 46)
            {
              num += this.delayOnPeriod * 3f;
              mCurrentOffset += 2;
            }
            else
              num += this.delayOnPeriod;
          }
          else if ((int) ch == 33 || (int) ch == 63)
            num += this.delayOnPeriod;
        }
        if ((double) this.mNextChar == 0.0)
          this.mNextChar = RealTime.time + num;
        else
          this.mNextChar += num;
        if ((double) this.fadeInTime != 0.0)
        {
          this.mFade.Add(new TypewriterEffect.FadeEntry()
          {
            index = mCurrentOffset,
            alpha = 0.0f,
            text = this.mFullText.Substring(mCurrentOffset, this.mCurrentOffset - mCurrentOffset)
          });
        }
        else
        {
          this.mLabel.text = !this.keepFullDimensions ? this.mFullText.Substring(0, this.mCurrentOffset) : this.mFullText.Substring(0, this.mCurrentOffset) + "[00]" + this.mFullText.Substring(this.mCurrentOffset);
          if (!this.keepFullDimensions && (Object) this.scrollView != (Object) null)
            this.scrollView.UpdatePosition();
        }
      }
      else
        break;
    }
    if (this.mCurrentOffset >= this.mFullText.Length)
    {
      this.mLabel.text = this.mFullText;
      TypewriterEffect.current = this;
      EventDelegate.Execute(this.onFinished);
      TypewriterEffect.current = (TypewriterEffect) null;
      this.mActive = false;
    }
    else
    {
      if (this.mFade.size == 0)
        return;
      int index1 = 0;
      while (index1 < this.mFade.size)
      {
        TypewriterEffect.FadeEntry fadeEntry = this.mFade[index1];
        fadeEntry.alpha += RealTime.deltaTime / this.fadeInTime;
        if ((double) fadeEntry.alpha < 1.0)
        {
          this.mFade[index1] = fadeEntry;
          ++index1;
        }
        else
          this.mFade.RemoveAt(index1);
      }
      if (this.mFade.size == 0)
      {
        if (this.keepFullDimensions)
          this.mLabel.text = this.mFullText.Substring(0, this.mCurrentOffset) + "[00]" + this.mFullText.Substring(this.mCurrentOffset);
        else
          this.mLabel.text = this.mFullText.Substring(0, this.mCurrentOffset);
      }
      else
      {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index2 = 0; index2 < this.mFade.size; ++index2)
        {
          TypewriterEffect.FadeEntry fadeEntry = this.mFade[index2];
          if (index2 == 0)
            stringBuilder.Append(this.mFullText.Substring(0, fadeEntry.index));
          stringBuilder.Append('[');
          stringBuilder.Append(NGUIText.EncodeAlpha(fadeEntry.alpha));
          stringBuilder.Append(']');
          stringBuilder.Append(fadeEntry.text);
        }
        if (this.keepFullDimensions)
        {
          stringBuilder.Append("[00]");
          stringBuilder.Append(this.mFullText.Substring(this.mCurrentOffset));
        }
        this.mLabel.text = stringBuilder.ToString();
      }
    }
  }

  private struct FadeEntry
  {
    public int index;
    public string text;
    public float alpha;
  }
}
