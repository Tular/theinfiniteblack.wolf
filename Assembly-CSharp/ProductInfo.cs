﻿// Decompiled with JetBrains decompiler
// Type: ProductInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using System;
using UnityEngine;
using UnityEngine.Purchasing;

[AdvancedInspector.AdvancedInspector]
[Serializable]
public class ProductInfo : IProductInfo
{
  [SerializeField]
  private string _googleOverrideId;
  [SerializeField]
  private string _iosOverrideId;
  [SerializeField]
  private string _steamOverrideId;
  [SerializeField]
  private string _productName;
  [SerializeField]
  private string _billerProductId;
  [Inspect(2)]
  public ProductType productType;
  [Inspect(0)]
  [Group("Google Play", Expandable = false, Priority = 10)]
  public bool useGoogleIdOverride;
  [Inspect(3)]
  [Group("Apple App Store", Expandable = false, Priority = 20)]
  public bool useIosIdOverride;
  [Inspect(5)]
  [Group("Steam InApp", Expandable = false, Priority = 30)]
  public bool useSteamIdOverride;

  [Inspect(0)]
  public string productName
  {
    get
    {
      return this._productName;
    }
    set
    {
      this._productName = value;
    }
  }

  [Inspect(1)]
  public string billerProductId
  {
    get
    {
      return this._billerProductId;
    }
    set
    {
      this._billerProductId = value;
    }
  }

  [Group("Google Play")]
  [ReadOnly("IsGoogleIdSameAsProductId")]
  [Inspect(1)]
  public string googlePlayId
  {
    get
    {
      if (this.useGoogleIdOverride)
        return this._googleOverrideId;
      return this.billerProductId;
    }
    set
    {
      this._googleOverrideId = value;
    }
  }

  [ReadOnly("IsIosIdSameAsProductId")]
  [Inspect(4)]
  [Group("Apple App Store")]
  public string appleAppStoreId
  {
    get
    {
      if (this.useIosIdOverride)
        return this._iosOverrideId;
      return this.billerProductId;
    }
    set
    {
      this._iosOverrideId = value;
    }
  }

  [Group("Steam InApp")]
  [Inspect(6)]
  [ReadOnly("IsSteamIdSameAsProductId")]
  public string steamProductId
  {
    get
    {
      if (this.useSteamIdOverride)
        return this._steamOverrideId;
      return this.billerProductId;
    }
    set
    {
      this._steamOverrideId = value;
    }
  }

  public void AddProductConfig(ConfigurationBuilder configBuilder)
  {
    ConfigurationBuilder configurationBuilder = configBuilder;
    string billerProductId = this.billerProductId;
    int productType = (int) this.productType;
    IDs ids = new IDs();
    ids.Add(this.googlePlayId, new string[1]{ "GooglePlay" });
    ids.Add(this.appleAppStoreId, new string[1]
    {
      "AppleAppStore"
    });
    IDs storeIDs = ids;
    configurationBuilder.AddProduct(billerProductId, (ProductType) productType, storeIDs);
  }

  public override string ToString()
  {
    if (string.IsNullOrEmpty(this.productName))
      return "[NOT DEFINED]";
    return this.productName;
  }

  private bool IsGoogleIdSameAsProductId()
  {
    return !this.useGoogleIdOverride;
  }

  private bool IsIosIdSameAsProductId()
  {
    return !this.useIosIdOverride;
  }

  private bool IsSteamIdSameAsProductId()
  {
    return !this.useSteamIdOverride;
  }
}
