﻿// Decompiled with JetBrains decompiler
// Type: BlackDollarItemCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity;
using UnityEngine;

[ExecuteInEditMode]
public class BlackDollarItemCard : MonoBehaviour
{
  public GameObject visualNode;
  public UILabel itemName;
  public UILabel itemDescription;
  public UILabel itemCost;
  public SBUIButton purchaseButton;
  public SBUIButton activateButton;
  public UISprite isActiveFill;
  public UILabel isActiveLabel;
  public Color activeColor;
  public Color inactiveColor;
  public UISprite icon;
  public int sortOrder;
  public BlackDollarItemCard.BlackDollarItemType itemType;
  protected PurchaseItem mItem;
  private int _clientBuyablesLastChangedId;
  private System.Action _onUpdateVisuals;
  private System.Action _onActivateClicked;
  private Func<string> _formattedItemName;
  private Vector3 _iconOriginalLoc;

  public string ItemName
  {
    get
    {
      if (this.mItem == null)
        return string.Empty;
      if (this._formattedItemName == null || !Application.isPlaying)
        return this.mItem.Name;
      return this._formattedItemName();
    }
  }

  public string ItemDescription
  {
    get
    {
      if (this.mItem != null)
        return this.mItem.Description;
      TheInfiniteBlack.Library.Log.D((object) this, nameof (ItemDescription), string.Format("{0} - ItemDescription, mItem is null", (object) this.name));
      return string.Empty;
    }
  }

  public string ItemCost
  {
    get
    {
      return this.mItem.Cost.ToString();
    }
  }

  private void UpdateVisuals()
  {
    this.itemName.text = this.ItemName;
    this.itemDescription.text = this.ItemDescription;
    this.itemCost.text = this.ItemCost;
    if (!Application.isPlaying || this._onUpdateVisuals == null)
      return;
    this._onUpdateVisuals();
  }

  private void Initialize()
  {
    this._clientBuyablesLastChangedId = int.MinValue;
    switch (this.itemType)
    {
      case BlackDollarItemCard.BlackDollarItemType.EarthJump:
        this.mItem = PurchaseItem.EarthJump;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.CorpJump:
        this.mItem = PurchaseItem.CorpJump;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.RiftJump:
        this.mItem = PurchaseItem.RiftJump;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.HyperDrive:
        this.mItem = PurchaseItem.HyperDrive;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, true);
        this._onUpdateVisuals = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.HyperDrive <= 0)
          {
            this.isActiveFill.enabled = false;
            this.isActiveLabel.enabled = false;
          }
          else
          {
            this.isActiveFill.enabled = true;
            this.isActiveLabel.enabled = true;
            this.isActiveFill.color = !TibProxy.gameState.Buyables.SuspendHyperDrive ? this.activeColor : this.inactiveColor;
            this.isActiveLabel.text = !TibProxy.gameState.Buyables.SuspendHyperDrive ? "ON" : "OFF";
          }
        });
        this._formattedItemName = (Func<string>) (() => string.Format("{0} ({1})", (object) this.mItem.Name, (object) TibProxy.gameState.Buyables.HyperDrive));
        this._onActivateClicked = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.HyperDrive <= 0)
            return;
          TibProxy.gameState.Buyables.ToggleHyperDrive();
        });
        this.icon.transform.localPosition = this._iconOriginalLoc;
        break;
      case BlackDollarItemCard.BlackDollarItemType.SuperCharge:
        this.mItem = PurchaseItem.SuperCharge;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, true);
        this._onUpdateVisuals = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.SuperCharge <= 0)
          {
            this.isActiveFill.enabled = false;
            this.isActiveLabel.enabled = false;
          }
          else
          {
            this.isActiveFill.enabled = true;
            this.isActiveLabel.enabled = true;
            this.isActiveFill.color = !TibProxy.gameState.Buyables.SuspendSuperCharge ? this.activeColor : this.inactiveColor;
            this.isActiveLabel.text = !TibProxy.gameState.Buyables.SuspendSuperCharge ? "ON" : "OFF";
          }
        });
        this._formattedItemName = (Func<string>) (() => string.Format("{0} ({1})", (object) this.mItem.Name, (object) TibProxy.gameState.Buyables.SuperCharge));
        this._onActivateClicked = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.SuperCharge <= 0)
            return;
          TibProxy.gameState.Buyables.ToggleSuperCharge();
        });
        this.icon.transform.localPosition = this._iconOriginalLoc;
        break;
      case BlackDollarItemCard.BlackDollarItemType.Tactics:
        this.mItem = PurchaseItem.Tactics;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, true);
        this._onUpdateVisuals = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.Tactics <= 0)
          {
            this.isActiveFill.enabled = false;
            this.isActiveLabel.enabled = false;
          }
          else
          {
            this.isActiveFill.enabled = true;
            this.isActiveLabel.enabled = true;
            this.isActiveFill.color = !TibProxy.gameState.Buyables.SuspendTactics ? this.activeColor : this.inactiveColor;
            this.isActiveLabel.text = !TibProxy.gameState.Buyables.SuspendTactics ? "ON" : "OFF";
          }
        });
        this._formattedItemName = (Func<string>) (() => string.Format("{0} ({1})", (object) this.mItem.Name, (object) TibProxy.gameState.Buyables.Tactics));
        this._onActivateClicked = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.Tactics <= 0)
            return;
          TibProxy.gameState.Buyables.ToggleTactics();
        });
        this.icon.transform.localPosition = this._iconOriginalLoc;
        break;
      case BlackDollarItemCard.BlackDollarItemType.MindSurge:
        this.mItem = PurchaseItem.MindSurge;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, true);
        this._onUpdateVisuals = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.MindSurge <= 0)
          {
            this.isActiveFill.enabled = false;
            this.isActiveLabel.enabled = false;
          }
          else
          {
            this.isActiveFill.enabled = true;
            this.isActiveLabel.enabled = true;
            this.isActiveFill.color = !TibProxy.gameState.Buyables.SuspendMindSurge ? this.activeColor : this.inactiveColor;
            this.isActiveLabel.text = !TibProxy.gameState.Buyables.SuspendMindSurge ? "ON" : "OFF";
          }
        });
        this._formattedItemName = (Func<string>) (() => string.Format("{0} ({1})", (object) this.mItem.Name, (object) TibProxy.gameState.Buyables.MindSurge));
        this._onActivateClicked = (System.Action) (() =>
        {
          if (TibProxy.gameState.Buyables.MindSurge <= 0)
            return;
          TibProxy.gameState.Buyables.ToggleMindSurge();
        });
        this.icon.transform.localPosition = this._iconOriginalLoc;
        break;
      case BlackDollarItemCard.BlackDollarItemType.Accelerate:
        this.mItem = PurchaseItem.Accelerate;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this._formattedItemName = (Func<string>) (() => string.Format("{0} ({1})", (object) this.mItem.Name, (object) TibProxy.gameState.Buyables.Accelerate));
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.CreditExchange:
        this.mItem = PurchaseItem.CreditExchange;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.RaiseBankCap:
        this.mItem = PurchaseItem.RaiseBankCap;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this._formattedItemName = (Func<string>) (() => string.Format("{0} ({1}/{2})", (object) this.mItem.Name, (object) TibProxy.gameState.Bank.ItemCount, (object) TibProxy.gameState.Buyables.BankCap));
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.RaiseInventoryCap:
        this.mItem = PurchaseItem.RaiseInventoryCap;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this._formattedItemName = (Func<string>) (() => string.Format("{0} ({1}/{2})", (object) this.mItem.Name, (object) TibProxy.gameState.MyShip.get_Inventory().Count, (object) TibProxy.gameState.Buyables.InventoryCap));
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.Fighter:
        this.mItem = PurchaseItem.Fighter;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.RepairDrone:
        this.mItem = PurchaseItem.RepairDrone;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.Mines:
        this.mItem = PurchaseItem.Mines;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.WyrdInvader:
        this.mItem = PurchaseItem.WyrdInvader;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.WyrdAssassin:
        this.mItem = PurchaseItem.WyrdAssassin;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.WyrdReaper:
        this.mItem = PurchaseItem.WyrdReaper;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.AllianceHyperDrive:
        this.mItem = PurchaseItem.AllianceHyperDrive;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.AllianceSuperCharge:
        this.mItem = PurchaseItem.AllianceSuperCharge;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.AllianceTactics:
        this.mItem = PurchaseItem.AllianceTactics;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.AllianceMindSurge:
        this.mItem = PurchaseItem.AllianceMindSurge;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
      case BlackDollarItemCard.BlackDollarItemType.AllianceAccelerate:
        this.mItem = PurchaseItem.AllianceAccelerate;
        NGUITools.SetActiveSelf(this.activateButton.gameObject, false);
        this.icon.transform.localPosition = Vector3.zero;
        break;
    }
  }

  private void OnPurchaseClicked()
  {
    TibProxy.gameState.DoPurchase(this.mItem);
  }

  private void OnActivateClicked()
  {
    if (!this.activateButton.enabled || this._onActivateClicked == null)
      return;
    this._onActivateClicked();
  }

  private void Awake()
  {
    this._iconOriginalLoc = this.icon.transform.localPosition;
    this.Initialize();
    EventDelegate.Add(this.activateButton.onClick, new EventDelegate.Callback(this.OnActivateClicked));
    EventDelegate.Add(this.purchaseButton.onClick, new EventDelegate.Callback(this.OnPurchaseClicked));
  }

  private void OnEnable()
  {
    this._clientBuyablesLastChangedId = int.MinValue;
    this.isActiveFill.enabled = false;
    this.isActiveLabel.enabled = false;
  }

  private void Update()
  {
    if (!Application.isPlaying)
    {
      this.name = string.Format("{0:00}-{1}", (object) this.sortOrder, (object) this.itemType);
      this.Initialize();
      this.UpdateVisuals();
    }
    else
    {
      if (TibProxy.gameState == null || TibProxy.gameState.Buyables == null || this._clientBuyablesLastChangedId == TibProxy.gameState.Buyables.LastChangeFlagID)
        return;
      this.UpdateVisuals();
      this._clientBuyablesLastChangedId = TibProxy.gameState.Buyables.LastChangeFlagID;
    }
  }

  public enum BlackDollarItemType
  {
    EarthJump,
    CorpJump,
    RiftJump,
    HyperDrive,
    SuperCharge,
    Tactics,
    MindSurge,
    Accelerate,
    CreditExchange,
    RaiseBankCap,
    RaiseInventoryCap,
    Fighter,
    RepairDrone,
    Mines,
    WyrdInvader,
    WyrdAssassin,
    WyrdReaper,
    AllianceHyperDrive,
    AllianceSuperCharge,
    AllianceTactics,
    AllianceMindSurge,
    AllianceAccelerate,
  }
}
