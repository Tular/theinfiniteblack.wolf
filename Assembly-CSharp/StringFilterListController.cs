﻿// Decompiled with JetBrains decompiler
// Type: StringFilterListController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Client.Settings;

public class StringFilterListController : BoundListControllerBase<StringFilter>
{
  public List<string> testList;
  private bool _willShow;

  public void Show(List<StringFilter> data)
  {
    this._willShow = true;
    if (!this.gameObject.activeSelf)
      NGUITools.SetActiveSelf(this.gameObject, true);
    this.Reset();
    this.Bind(data);
  }

  public void Hide()
  {
    if (this._willShow)
      return;
    this.Reset();
    NGUITools.SetActiveSelf(this.gameObject, false);
  }

  public override void CreateNewCell()
  {
    this.SetFocused(this.Add(new StringFilter()));
  }

  private void LateUpdate()
  {
    this._willShow = false;
  }

  private void OnDisable()
  {
    this._willShow = false;
  }
}
