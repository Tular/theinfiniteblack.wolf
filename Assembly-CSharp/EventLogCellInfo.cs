﻿// Decompiled with JetBrains decompiler
// Type: EventLogCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using UnityEngine;

public class EventLogCellInfo : ScrollViewCellInfoBase<GameEventArgs, EventLogEntryCell>
{
  private Vector3 _extents;
  private int _width;
  private int _height;
  private readonly string _formattedText;

  public EventLogCellInfo(Transform prefab, GameEventArgs data, int initialWidth)
    : base(prefab, data)
  {
    this._formattedText = EventLogCellInfo.GetFormattedText(data);
    if (this.mCellData.EventType == GameEventType.Chat)
    {
      ChatEventArgs mCellData = this.mCellData as ChatEventArgs;
      this.isSticky = false;
      if (mCellData != null)
        this.isSticky = mCellData.Type == ChatType.UNIVERSE || mCellData.Type == ChatType.SERVER || (mCellData.Type == ChatType.CORP || mCellData.Type == ChatType.ALLIANCE) || (mCellData.Type == ChatType.SECTOR || mCellData.Type == ChatType.PRIVATE) || mCellData.Type == ChatType.CONSOLE;
    }
    this.UpdateDimensions(initialWidth);
  }

  public bool isSticky { get; private set; }

  public override Vector3 extents
  {
    get
    {
      return this._extents;
    }
  }

  public int width
  {
    get
    {
      return this._width;
    }
    set
    {
      if (this._width == value)
        return;
      this._width = value;
      this._extents.x = (float) this._width / 2f;
    }
  }

  public int height
  {
    get
    {
      return this._height;
    }
    set
    {
      if (this._height == value)
        return;
      this._height = value;
      this._extents.y = (float) this._height / 2f;
    }
  }

  public void UpdateDimensions()
  {
    this.UpdateDimensions(this.width);
  }

  public void UpdateDimensions(int pWidth)
  {
    EventLogEntryCell eventLogEntryCell = this.mCellInstance ?? this.cellPrefab.GetComponent<EventLogEntryCell>();
    eventLogEntryCell.entry.width = pWidth;
    eventLogEntryCell.entry.text = this._formattedText;
    string processedText = eventLogEntryCell.entry.processedText;
    this.width = pWidth;
    this.height = eventLogEntryCell.entry.height;
    if (!((Object) this.mCellInstance == (Object) null))
      return;
    eventLogEntryCell.entry.text = string.Empty;
  }

  protected override void SetVisuals(EventLogEntryCell cell)
  {
    cell.entry.text = this._formattedText;
    cell.entry.width = this.width;
    cell.entry.height = this.height;
    cell.transform.localPosition = this.localPosition;
  }

  private static string GetFormattedText(GameEventArgs e)
  {
    StringBuilder stringBuilder = new StringBuilder(256);
    e.AppendText(stringBuilder);
    Markup.GetNGUI(stringBuilder);
    stringBuilder.Append("[-][-][-][/b][/i][/u][/s]");
    return stringBuilder.ToString();
  }
}
