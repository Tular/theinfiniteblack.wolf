﻿// Decompiled with JetBrains decompiler
// Type: MultiCameraUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MultiCameraUI : MonoBehaviour
{
  public Camera cam2;
  public Camera cam3;

  public void AddCamera2(bool value)
  {
    this.AddCamera(this.cam2, value);
  }

  public void AddCamera3(bool value)
  {
    this.AddCamera(this.cam3, value);
  }

  public void AddCamera(Camera cam, bool value)
  {
    if (value)
      EasyTouch.AddCamera(cam, false);
    else
      EasyTouch.RemoveCamera(cam);
  }
}
