﻿// Decompiled with JetBrains decompiler
// Type: GameManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GameManager : MonoBehaviour
{
  public static RandomGenerator random = new RandomGenerator();
  public static GameManager.GameType gameType = GameManager.GameType.None;
  public static bool enableTooltips = true;
  public static float timeLimit = 900f;
  public static float gameTime = 0.0f;
  private static int mPause = 0;
  private static float mTargetTimeScale = 1f;
  private static GameManager mInstance;

  private static float savedTimeLimit
  {
    get
    {
      string s = PlayerPrefs.GetString("Time Limit", "15");
      float result = 15f;
      float.TryParse(s, out result);
      return result * 60f;
    }
  }

  public static void Pause()
  {
    ++GameManager.mPause;
    GameManager.mTargetTimeScale = 0.0f;
  }

  public static void Unpause()
  {
    if (--GameManager.mPause >= 1)
      return;
    GameManager.mTargetTimeScale = 1f;
    GameManager.mPause = 0;
  }

  public static void StartSingleGame()
  {
    if (!((Object) GameManager.mInstance != (Object) null))
      return;
    GameManager.gameType = GameManager.GameType.SinglePlayer;
    GameManager.timeLimit = GameManager.savedTimeLimit;
    GameManager.gameTime = 0.0f;
    Application.LoadLevel("Game Scene");
  }

  public static void EndGame()
  {
    if (GameManager.gameType == GameManager.GameType.None)
      return;
    GameManager.gameType = GameManager.GameType.None;
    Time.timeScale = 0.0f;
    GameManager.mTargetTimeScale = 0.0f;
    GameManager.mPause = 0;
    GameManager.LoadMenu();
  }

  public static void Forfeit()
  {
    if (GameManager.gameType != GameManager.GameType.None)
      GameManager.EndGame();
    else
      GameManager.LoadMenu();
  }

  private static void LoadMenu()
  {
    GameManager.gameType = GameManager.GameType.None;
    Time.timeScale = 1f;
    GameManager.mTargetTimeScale = 1f;
    GameManager.mPause = 0;
    if (!(Application.loadedLevelName != "Menu Scene"))
      return;
    if ((Object) GameManager.mInstance != (Object) null)
    {
      Object.Destroy((Object) GameManager.mInstance);
      GameManager.mInstance = (GameManager) null;
    }
    Application.LoadLevel("Menu Scene");
  }

  private void Awake()
  {
    if ((Object) GameManager.mInstance == (Object) null)
    {
      Screen.sleepTimeout = -1;
      Application.targetFrameRate = !PlayerProfile.powerSavingMode ? 60 : 30;
      GameManager.gameTime = 0.0f;
      GameManager.mInstance = this;
    }
    else
      Object.Destroy((Object) this);
  }

  private void OnDestroy()
  {
    if (!((Object) GameManager.mInstance == (Object) this))
      return;
    GameManager.mInstance = (GameManager) null;
  }

  private void Update()
  {
    Time.timeScale = Mathf.Lerp(Time.timeScale, GameManager.mTargetTimeScale, 8f * RealTime.deltaTime);
    GameManager.gameTime += Time.deltaTime;
    if ((double) GameManager.timeLimit <= 0.0 || (double) GameManager.gameTime <= (double) GameManager.timeLimit)
      return;
    GameManager.EndGame();
  }

  public enum GameType
  {
    None,
    SinglePlayer,
    Multiplayer,
  }
}
