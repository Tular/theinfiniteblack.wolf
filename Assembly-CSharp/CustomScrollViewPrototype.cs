﻿// Decompiled with JetBrains decompiler
// Type: CustomScrollViewPrototype
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[RequireComponent(typeof (UIPanel))]
public class CustomScrollViewPrototype : MonoBehaviour
{
  public CustomScrollViewPrototype.DragEffect dragEffect = CustomScrollViewPrototype.DragEffect.MomentumAndSpring;
  public bool restrictWithinPanel = true;
  public bool smoothDragStart = true;
  public bool iOSDragEmulation = true;
  public float scrollWheelFactor = 0.25f;
  public float momentumAmount = 35f;
  public float springStrength = 13f;
  public Vector2 customMovement = new Vector2(1f, 0.0f);
  [SerializeField]
  [HideInInspector]
  private Vector3 scale = new Vector3(1f, 0.0f, 0.0f);
  [HideInInspector]
  [SerializeField]
  private Vector2 relativePositionOnReset = Vector2.zero;
  protected Vector3 mMomentum = Vector3.zero;
  protected int mDragID = -10;
  protected Vector2 mDragStartOffset = Vector2.zero;
  public Transform tableRoot;
  public CustomScrollViewPrototype.Movement movement;
  public bool disableDragIfFits;
  public UIWidget.Pivot contentPivot;
  public CustomScrollViewPrototype.OnDragFinished onDragFinished;
  protected Transform mTrans;
  protected UIPanel mPanel;
  protected Plane mPlane;
  protected Vector3 mLastPos;
  protected bool mPressed;
  protected float mScroll;
  protected Bounds mBounds;
  protected bool mCalculatedBounds;
  protected bool mShouldMove;
  protected bool mIgnoreCallbacks;
  protected bool mDragStarted;

  public UIPanel panel
  {
    get
    {
      return this.mPanel;
    }
  }

  public bool isDragging
  {
    get
    {
      if (this.mPressed)
        return this.mDragStarted;
      return false;
    }
  }

  public virtual Bounds bounds
  {
    get
    {
      if (!this.mCalculatedBounds)
      {
        this.mCalculatedBounds = true;
        this.mTrans = this.transform;
        this.mBounds = NGUIMath.CalculateRelativeWidgetBounds(this.mTrans, this.tableRoot, true, true);
      }
      return this.mBounds;
    }
  }

  public bool canMoveHorizontally
  {
    get
    {
      if (this.movement == CustomScrollViewPrototype.Movement.Horizontal || this.movement == CustomScrollViewPrototype.Movement.Unrestricted)
        return true;
      if (this.movement == CustomScrollViewPrototype.Movement.Custom)
        return (double) this.customMovement.x != 0.0;
      return false;
    }
  }

  public bool canMoveVertically
  {
    get
    {
      if (this.movement == CustomScrollViewPrototype.Movement.Vertical || this.movement == CustomScrollViewPrototype.Movement.Unrestricted)
        return true;
      if (this.movement == CustomScrollViewPrototype.Movement.Custom)
        return (double) this.customMovement.y != 0.0;
      return false;
    }
  }

  public virtual bool shouldMoveHorizontally
  {
    get
    {
      float x = this.bounds.size.x;
      if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
        x += this.mPanel.clipSoftness.x * 2f;
      return Mathf.RoundToInt(x - this.mPanel.width) > 0;
    }
  }

  public virtual bool shouldMoveVertically
  {
    get
    {
      float y = this.bounds.size.y;
      if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
        y += this.mPanel.clipSoftness.y * 2f;
      return Mathf.RoundToInt(y - this.mPanel.height) > 0;
    }
  }

  protected virtual bool shouldMove
  {
    get
    {
      if (!this.disableDragIfFits)
        return true;
      if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
        this.mPanel = this.GetComponent<UIPanel>();
      Vector4 finalClipRegion = this.mPanel.finalClipRegion;
      Bounds bounds = this.bounds;
      float num1 = (double) finalClipRegion.z != 0.0 ? finalClipRegion.z * 0.5f : (float) Screen.width;
      float num2 = (double) finalClipRegion.w != 0.0 ? finalClipRegion.w * 0.5f : (float) Screen.height;
      return this.canMoveHorizontally && ((double) bounds.min.x < (double) finalClipRegion.x - (double) num1 || (double) bounds.max.x > (double) finalClipRegion.x + (double) num1) || this.canMoveVertically && ((double) bounds.min.y < (double) finalClipRegion.y - (double) num2 || (double) bounds.max.y > (double) finalClipRegion.y + (double) num2);
    }
  }

  public Vector3 currentMomentum
  {
    get
    {
      return this.mMomentum;
    }
    set
    {
      this.mMomentum = value;
      this.mShouldMove = true;
    }
  }

  private void Awake()
  {
    this.mTrans = this.transform;
    this.mPanel = this.GetComponent<UIPanel>();
    if (this.mPanel.clipping != UIDrawCall.Clipping.None)
      return;
    this.mPanel.clipping = UIDrawCall.Clipping.ConstrainButDontClip;
  }

  private void OnEnable()
  {
  }

  private void OnDisable()
  {
  }

  protected virtual void Start()
  {
  }

  public bool RestrictWithinBounds(bool instant)
  {
    return this.RestrictWithinBounds(instant, true, true);
  }

  public bool RestrictWithinBounds(bool instant, bool horizontal, bool vertical)
  {
    Bounds bounds = this.bounds;
    Vector3 constrainOffset = this.mPanel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!horizontal)
      constrainOffset.x = 0.0f;
    if (!vertical)
      constrainOffset.y = 0.0f;
    if ((double) constrainOffset.sqrMagnitude <= 1.0)
      return false;
    if (!instant && this.dragEffect == CustomScrollViewPrototype.DragEffect.MomentumAndSpring)
    {
      Vector3 pos = this.mTrans.localPosition + constrainOffset;
      pos.x = Mathf.Round(pos.x);
      pos.y = Mathf.Round(pos.y);
      SpringPanel.Begin(this.mPanel.gameObject, pos, this.springStrength);
    }
    else
    {
      this.MoveRelative(constrainOffset);
      this.mMomentum = Vector3.zero;
      this.mScroll = 0.0f;
    }
    return true;
  }

  public void DisableSpring()
  {
    SpringPanel component = this.GetComponent<SpringPanel>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.enabled = false;
  }

  public virtual void SetDragAmount(float x, float y, bool updateScrollbars)
  {
    if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
      this.mPanel = this.GetComponent<UIPanel>();
    this.DisableSpring();
    Bounds bounds = this.bounds;
    if ((double) bounds.min.x == (double) bounds.max.x || (double) bounds.min.y == (double) bounds.max.y)
      return;
    Vector4 finalClipRegion = this.mPanel.finalClipRegion;
    float num1 = finalClipRegion.z * 0.5f;
    float num2 = finalClipRegion.w * 0.5f;
    float a1 = bounds.min.x + num1;
    float b1 = bounds.max.x - num1;
    float b2 = bounds.min.y + num2;
    float a2 = bounds.max.y - num2;
    if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
    {
      a1 -= this.mPanel.clipSoftness.x;
      b1 += this.mPanel.clipSoftness.x;
      b2 -= this.mPanel.clipSoftness.y;
      a2 += this.mPanel.clipSoftness.y;
    }
    float num3 = Mathf.Lerp(a1, b1, x);
    float num4 = Mathf.Lerp(a2, b2, y);
    if (!updateScrollbars)
    {
      Vector3 localPosition = this.mTrans.localPosition;
      if (this.canMoveHorizontally)
        localPosition.x += finalClipRegion.x - num3;
      if (this.canMoveVertically)
        localPosition.y += finalClipRegion.y - num4;
      this.mTrans.localPosition = localPosition;
    }
    if (this.canMoveHorizontally)
      finalClipRegion.x = num3;
    if (this.canMoveVertically)
      finalClipRegion.y = num4;
    Vector4 baseClipRegion = this.mPanel.baseClipRegion;
    this.mPanel.clipOffset = new Vector2(finalClipRegion.x - baseClipRegion.x, finalClipRegion.y - baseClipRegion.y);
  }

  [ContextMenu("Reset Clipping Position")]
  public void ResetPosition()
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    this.mCalculatedBounds = false;
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.contentPivot);
    this.SetDragAmount(pivotOffset.x, 1f - pivotOffset.y, false);
    this.SetDragAmount(pivotOffset.x, 1f - pivotOffset.y, true);
  }

  public void UpdatePosition()
  {
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.contentPivot);
    this.SetDragAmount(pivotOffset.x, pivotOffset.y, false);
  }

  public virtual void MoveRelative(Vector3 relative)
  {
    this.mTrans.localPosition += relative;
    Vector2 clipOffset = this.mPanel.clipOffset;
    clipOffset.x -= relative.x;
    clipOffset.y -= relative.y;
    this.mPanel.clipOffset = clipOffset;
  }

  public void MoveAbsolute(Vector3 absolute)
  {
    this.MoveRelative(this.mTrans.InverseTransformPoint(absolute) - this.mTrans.InverseTransformPoint(Vector3.zero));
  }

  public void Press(bool pressed)
  {
    if (this.smoothDragStart && pressed)
    {
      this.mDragStarted = false;
      this.mDragStartOffset = Vector2.zero;
    }
    if (!this.enabled || !NGUITools.GetActive(this.gameObject))
      return;
    if (!pressed && this.mDragID == UICamera.currentTouchID)
      this.mDragID = -10;
    this.mCalculatedBounds = false;
    this.mShouldMove = this.shouldMove;
    if (!this.mShouldMove)
      return;
    this.mPressed = pressed;
    if (pressed)
    {
      this.mMomentum = Vector3.zero;
      this.mScroll = 0.0f;
      this.DisableSpring();
      this.mLastPos = UICamera.lastHit.point;
      this.mPlane = new Plane(this.mTrans.rotation * Vector3.back, this.mLastPos);
      Vector2 clipOffset = this.mPanel.clipOffset;
      clipOffset.x = Mathf.Round(clipOffset.x);
      clipOffset.y = Mathf.Round(clipOffset.y);
      this.mPanel.clipOffset = clipOffset;
      Vector3 localPosition = this.mTrans.localPosition;
      localPosition.x = Mathf.Round(localPosition.x);
      localPosition.y = Mathf.Round(localPosition.y);
      this.mTrans.localPosition = localPosition;
    }
    else
    {
      if ((double) this.mMomentum.magnitude < 0.100000001490116)
        this.mMomentum = Vector3.zero;
      if (this.restrictWithinPanel && this.mPanel.clipping != UIDrawCall.Clipping.None && this.dragEffect == CustomScrollViewPrototype.DragEffect.MomentumAndSpring)
        this.RestrictWithinBounds(false, this.canMoveHorizontally, this.canMoveVertically);
      if (this.onDragFinished == null)
        return;
      this.onDragFinished();
    }
  }

  public Vector3 currentPos { get; private set; }

  public Vector3 lastPos
  {
    get
    {
      return this.mLastPos;
    }
  }

  public float scroll
  {
    get
    {
      return this.mScroll;
    }
  }

  public float lastDragTime { get; private set; }

  public void Drag()
  {
    if (!this.enabled || !NGUITools.GetActive(this.gameObject) || !this.mShouldMove)
      return;
    this.lastDragTime = RealTime.time;
    if (this.mDragID == -10)
      this.mDragID = UICamera.currentTouchID;
    UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;
    if (this.smoothDragStart && !this.mDragStarted)
    {
      this.mDragStarted = true;
      this.mDragStartOffset = UICamera.currentTouch.totalDelta;
    }
    Ray ray = !this.smoothDragStart ? UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos) : UICamera.currentCamera.ScreenPointToRay((Vector3) (UICamera.currentTouch.pos - this.mDragStartOffset));
    float enter = 0.0f;
    if (!this.mPlane.Raycast(ray, out enter))
      return;
    this.currentPos = ray.GetPoint(enter);
    Vector3 vector3 = this.currentPos - this.mLastPos;
    this.mLastPos = this.currentPos;
    if ((double) vector3.x != 0.0 || (double) vector3.y != 0.0 || (double) vector3.z != 0.0)
    {
      Vector3 direction = this.mTrans.InverseTransformDirection(vector3);
      if (this.movement == CustomScrollViewPrototype.Movement.Horizontal)
      {
        direction.y = 0.0f;
        direction.z = 0.0f;
      }
      else if (this.movement == CustomScrollViewPrototype.Movement.Vertical)
      {
        direction.x = 0.0f;
        direction.z = 0.0f;
      }
      else if (this.movement == CustomScrollViewPrototype.Movement.Unrestricted)
        direction.z = 0.0f;
      else
        direction.Scale((Vector3) this.customMovement);
      vector3 = this.mTrans.TransformDirection(direction);
    }
    this.mMomentum = Vector3.Lerp(this.mMomentum, this.mMomentum + vector3 * (0.01f * this.momentumAmount), 0.67f);
    if (!this.iOSDragEmulation || this.dragEffect != CustomScrollViewPrototype.DragEffect.MomentumAndSpring)
      this.MoveAbsolute(vector3);
    else if ((double) this.mPanel.CalculateConstrainOffset((Vector2) this.bounds.min, (Vector2) this.bounds.max).magnitude > 1.0)
    {
      this.MoveAbsolute(vector3 * 0.5f);
      this.mMomentum *= 0.5f;
    }
    else
      this.MoveAbsolute(vector3);
    if ((double) this.mMomentum.magnitude < 9.99999974737875E-05)
      this.mMomentum = Vector3.zero;
    if (!this.restrictWithinPanel || this.mPanel.clipping == UIDrawCall.Clipping.None)
      return;
    this.RestrictWithinBounds(true, this.canMoveHorizontally, this.canMoveVertically);
  }

  public void Scroll(float delta)
  {
    if (!this.enabled || !NGUITools.GetActive(this.gameObject) || (double) this.scrollWheelFactor == 0.0)
      return;
    this.DisableSpring();
    this.mShouldMove = this.shouldMove;
    if ((double) Mathf.Sign(this.mScroll) != (double) Mathf.Sign(delta))
      this.mScroll = 0.0f;
    this.mScroll += delta * this.scrollWheelFactor;
  }

  private void LateUpdate()
  {
    if (!Application.isPlaying || this.mMomentum == Vector3.zero && (double) Math.Abs(this.mScroll) <= 0.0)
      return;
    float deltaTime = RealTime.deltaTime;
    if (this.mShouldMove && !this.mPressed)
    {
      if (this.movement == CustomScrollViewPrototype.Movement.Horizontal)
        this.mMomentum -= this.mTrans.TransformDirection(new Vector3(this.mScroll * 0.05f, 0.0f, 0.0f));
      else if (this.movement == CustomScrollViewPrototype.Movement.Vertical)
        this.mMomentum -= this.mTrans.TransformDirection(new Vector3(0.0f, this.mScroll * 0.05f, 0.0f));
      else if (this.movement == CustomScrollViewPrototype.Movement.Unrestricted)
        this.mMomentum -= this.mTrans.TransformDirection(new Vector3(this.mScroll * 0.05f, this.mScroll * 0.05f, 0.0f));
      else
        this.mMomentum -= this.mTrans.TransformDirection(new Vector3((float) ((double) this.mScroll * (double) this.customMovement.x * 0.0500000007450581), (float) ((double) this.mScroll * (double) this.customMovement.y * 0.0500000007450581), 0.0f));
      if ((double) this.mMomentum.magnitude > 9.99999974737875E-05)
      {
        this.mScroll = NGUIMath.SpringLerp(this.mScroll, 0.0f, 20f, deltaTime);
        this.MoveAbsolute(NGUIMath.SpringDampen(ref this.mMomentum, 9f, deltaTime));
        if (this.restrictWithinPanel && this.mPanel.clipping != UIDrawCall.Clipping.None)
          this.RestrictWithinBounds(false, this.canMoveHorizontally, this.canMoveVertically);
        if ((double) this.mMomentum.magnitude >= 9.99999974737875E-05 || this.onDragFinished == null)
          return;
        this.onDragFinished();
        return;
      }
      this.mScroll = 0.0f;
      this.mMomentum = Vector3.zero;
    }
    else
      this.mScroll = 0.0f;
    NGUIMath.SpringDampen(ref this.mMomentum, 9f, deltaTime);
  }

  public enum Movement
  {
    Horizontal,
    Vertical,
    Unrestricted,
    Custom,
  }

  public enum DragEffect
  {
    None,
    Momentum,
    MomentumAndSpring,
  }

  public delegate void OnDragFinished();
}
