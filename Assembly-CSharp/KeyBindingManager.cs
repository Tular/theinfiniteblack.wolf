﻿// Decompiled with JetBrains decompiler
// Type: KeyBindingManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class KeyBindingManager : PreInstantiatedSingleton<KeyBindingManager>
{
  private void OnEnable()
  {
    if (Application.platform != RuntimePlatform.IPhonePlayer)
      return;
    this.enabled = false;
  }

  private void Update()
  {
    if (TibProxy.Instance == null || TibProxy.gameState == null || !TibProxy.gameState.IsLoggedIn)
      return;
    bool keyDown = Input.GetKeyDown(KeyCode.Home);
    if (Input.GetKeyDown(KeyCode.Escape))
    {
      Debug.Log((object) "Escape pressed");
      if ((Object) UIInput.selection != (Object) null)
        UIInput.selection.isSelected = false;
      else if (TibProxy.gameState != null && TibProxy.gameState.IsLoggedIn)
      {
        if ((Object) PreInstantiatedSingleton<DialogManager>.Instance.ad == (Object) null)
        {
          PreInstantiatedSingleton<KeyboardMapController>.Instance.MapZoomEnd();
          DialogManager.ShowDialog<SystemDialog>(true);
        }
        else
        {
          if ((Object) UICamera.selectedObject != (Object) null)
            UICamera.selectedObject = (GameObject) null;
          DialogManager.CloseAllDialogs();
        }
      }
    }
    if (!keyDown || !((Object) UIInput.selection == (Object) null))
      return;
    DialogBase ad = PreInstantiatedSingleton<DialogManager>.Instance.ad;
    if (((Object) ad == (Object) null || (Object) ad != (Object) DialogManager.GetDialog<SystemDialog>()) && (TibProxy.gameState != null && TibProxy.gameState.IsLoggedIn))
    {
      DialogManager.ShowDialog<SystemDialog>(true);
    }
    else
    {
      if (!((Object) ad == (Object) DialogManager.GetDialog<SystemDialog>()))
        return;
      if ((Object) UICamera.selectedObject != (Object) null)
        UICamera.selectedObject = (GameObject) null;
      DialogManager.CloseAllDialogs();
    }
  }
}
