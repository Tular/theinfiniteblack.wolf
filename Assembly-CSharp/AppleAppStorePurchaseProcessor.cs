﻿// Decompiled with JetBrains decompiler
// Type: AppleAppStorePurchaseProcessor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine.Purchasing;

public class AppleAppStorePurchaseProcessor : IPurchaseProcessor
{
  public string GetProductId(IProductInfo product)
  {
    return product.appleAppStoreId;
  }

  public void ConfigureBuilder(ConfigurationBuilder builder)
  {
  }

  public PendingOrderDetail ParseReceipt(IInAppPurchasingContext context, string strReceipt)
  {
    Dictionary<string, object> dictionary = MiniJsonExtensions.HashtableFromJson(strReceipt);
    string str1 = dictionary["Store"] as string;
    string str2 = dictionary["TransactionID"] as string;
    string str3 = dictionary["Payload"] as string;
    return new PendingOrderDetail()
    {
      characterName = context.purchaserName,
      orderId = string.Empty,
      packageName = string.Empty,
      productId = string.Empty,
      purchaseToken = str3
    };
  }

  public bool TryGetPostString(PendingOrderDetail order, out string postString)
  {
    postString = string.Empty;
    if (string.IsNullOrEmpty(order.characterName))
      return false;
    postString = string.Format("{0},{1}", (object) order.purchaseToken, (object) order.characterName);
    return true;
  }
}
