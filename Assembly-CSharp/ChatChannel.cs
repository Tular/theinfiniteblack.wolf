﻿// Decompiled with JetBrains decompiler
// Type: ChatChannel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Library.Client.Settings;
using UnityEngine;

public class ChatChannel
{
  private readonly Transform _prefab;
  private UIScrollView _view;

  public ChatChannel(ChatType chatType, string channelKey, int sortOrder, Transform prefab, GameEventsFilter filter, UIScrollView view)
    : this(chatType, channelKey, channelKey, sortOrder, prefab, filter, view)
  {
    this.paragraphs = new ChatParagraphCellInfoCollection();
  }

  public ChatChannel(ChatType chatType, string channelKey, string channelName, int sortOrder, Transform prefab, GameEventsFilter filter, UIScrollView view)
  {
    this.chatType = chatType;
    this.channelKey = channelKey;
    this.channelName = channelName;
    this.sortOrder = sortOrder;
    this.filter = filter;
    this.isPrivate = chatType == ChatType.PRIVATE;
    this.channelAddress = channelName;
    this._prefab = prefab;
    this._view = view;
    this.paragraphs = new ChatParagraphCellInfoCollection();
    this.lastPostTime = DateTime.MinValue;
  }

  public ChatParagraphCellInfoCollection paragraphs { get; private set; }

  public GameEventsFilter filter { get; set; }

  public bool isPrivate { get; private set; }

  public string channelKey { get; private set; }

  public string channelName { get; private set; }

  public int maxSize { get; set; }

  public int pruneSize { get; set; }

  public string channelAddress { get; set; }

  public int sortOrder { get; private set; }

  public DateTime lastPostTime { get; private set; }

  public ChatType chatType { get; private set; }

  public void AddParagraph(GameEventArgs e)
  {
    this.paragraphs.Add(new ChatParagraphCellInfo(this._prefab, e, Mathf.RoundToInt(this._view.panel.width)));
    this.lastPostTime = DateTime.UtcNow;
  }

  public void AddParagraph(ChatEventArgs e)
  {
    this.paragraphs.Add(new ChatParagraphCellInfo(this._prefab, (GameEventArgs) e, Mathf.RoundToInt(this._view.panel.width)));
    this.lastPostTime = DateTime.UtcNow;
  }

  public void PruneHistory()
  {
    if (this.paragraphs == null)
      return;
    this.paragraphs.PruneHistory(this.maxSize, this.pruneSize);
  }
}
