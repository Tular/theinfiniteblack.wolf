﻿// Decompiled with JetBrains decompiler
// Type: PortraitCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class PortraitCache
{
  private PortraitCacheEntry[] _cache;
  private float _expireTime;
  private int _nextOpenCacheIdx;

  private PortraitCache()
  {
  }

  public static PortraitCache Build(PortraitCacheInfo def)
  {
    return new PortraitCache()
    {
      defaultTexture = def.defaultTexture,
      _expireTime = def.cacheExpireTime,
      imageSize = def.imageSize,
      _cache = new PortraitCacheEntry[def.maxCacheSize]
    };
  }

  public Texture2D defaultTexture { get; private set; }

  public int imageSize { get; private set; }

  public bool TryGetPortrait(string playerName, out PortraitCacheEntry portrait)
  {
    for (int index = 0; index < this._cache.Length; ++index)
    {
      if (this._cache[index] != null && this._cache[index].playerName.Equals(playerName))
      {
        portrait = this._cache[index];
        portrait.expireTime = RealTime.time + this._expireTime;
        return true;
      }
    }
    portrait = (PortraitCacheEntry) null;
    return false;
  }

  public bool Contains(string playerName)
  {
    for (int index = 0; index < this._cache.Length; ++index)
    {
      if (this._cache[index] != null && this._cache[index].playerName.Equals(playerName))
        return true;
    }
    return false;
  }

  public PortraitCacheEntry AddDefault(string playerName)
  {
    return this.Add(playerName, this.defaultTexture);
  }

  public PortraitCacheEntry Add(string playerName, Texture2D texture)
  {
    PortraitCacheEntry portrait;
    if (this.TryGetPortrait(playerName, out portrait))
    {
      portrait.SetTexture(texture);
      portrait.expireTime = RealTime.time + this._expireTime;
    }
    else
    {
      portrait = new PortraitCacheEntry(playerName, texture);
      this.Add(portrait);
    }
    this.SortCache();
    return portrait;
  }

  private void SortCache()
  {
    this._nextOpenCacheIdx = this._cache.Length - 1;
    for (int index = 0; index < this._cache.Length; ++index)
    {
      for (int a = 0; a < this._cache.Length - 1; ++a)
      {
        if (this._cache[a + 1] != null)
        {
          if (this._cache[a] == null)
          {
            this.Swap(a, a + 1);
            this._nextOpenCacheIdx = a + 1;
          }
          else if ((double) this._cache[a].expireTime < (double) this._cache[a + 1].expireTime)
            this.Swap(a, a + 1);
        }
      }
    }
  }

  private void Swap(int a, int b)
  {
    PortraitCacheEntry portraitCacheEntry = this._cache[a];
    this._cache[a] = this._cache[b];
    this._cache[b] = portraitCacheEntry;
  }

  private void Add(PortraitCacheEntry portrait)
  {
    portrait.expireTime = RealTime.time + this._expireTime;
    if (this._cache[this._cache.Length - 1] != null)
      this.Remove(this._cache.Length - 1);
    this._cache[this._cache.Length - 1] = portrait;
    this.SortCache();
  }

  public void UpdatePortrait(PortraitCacheEntry portrait, Texture2D texture2D)
  {
    portrait.SetTexture(texture2D);
    portrait.expireTime = RealTime.time + this._expireTime;
    this.SortCache();
  }

  private void Remove(int cacheIdx)
  {
    if (cacheIdx > this._cache.Length - 1 || this._cache[cacheIdx] == null)
      return;
    Texture2D texture = this._cache[cacheIdx].texture;
    this._cache[cacheIdx].texture = (Texture2D) null;
    if ((Object) texture != (Object) null && (Object) texture != (Object) this.defaultTexture)
      Object.Destroy((Object) texture);
    this._cache[cacheIdx] = (PortraitCacheEntry) null;
  }

  public void Flush()
  {
    for (int cacheIdx = 0; cacheIdx < this._cache.Length; ++cacheIdx)
    {
      if (this._cache[cacheIdx] != null)
        this.Remove(cacheIdx);
      this._cache[cacheIdx] = (PortraitCacheEntry) null;
    }
  }

  public void DumpContents()
  {
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < this._cache.Length; ++index)
    {
      if (this._cache[index] == null)
        stringBuilder.Append("[null]");
      else if ((Object) this._cache[index].texture == (Object) null)
        stringBuilder.Append("[" + this._cache[index].playerName + "nullTex:" + (object) this._cache[index].expireTime + "]");
      else if ((Object) this._cache[index].texture == (Object) this.defaultTexture)
        stringBuilder.Append("[" + this._cache[index].playerName + "- defaultTex:" + (object) this._cache[index].expireTime + "]");
      else
        stringBuilder.Append("[" + this._cache[index].playerName + ":" + (object) this._cache[index].expireTime + "]");
    }
    Debug.Log((object) (this.imageSize.ToString() + ": " + stringBuilder.ToString()));
  }
}
