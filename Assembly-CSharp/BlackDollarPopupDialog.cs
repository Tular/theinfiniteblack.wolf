﻿// Decompiled with JetBrains decompiler
// Type: BlackDollarPopupDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class BlackDollarPopupDialog : DialogBase
{
  public UILabel messageText;
  public SBUIButton buyTenBlackDollars;
  public SBUIButton buyOneHundredBlackDollars;
  public SBUIButton buyFiveHundredBlackDollars;
  public SBUIButton openBlackDollarPurchasePage;
  private string _defaultMessageText;

  public void PurchaseTenBlackDollars()
  {
    this.Hide();
    PreInstantiatedSingleton<InAppPurchaseManager>.Instance.PurchaseTenBlackDollars();
  }

  public void PurchaseOneHundredBlackDollars()
  {
    this.Hide();
    PreInstantiatedSingleton<InAppPurchaseManager>.Instance.PurchaseOneHundredBlackDollars();
  }

  public void PurchaseFiveHundredBlackDollars()
  {
    this.Hide();
    PreInstantiatedSingleton<InAppPurchaseManager>.Instance.PurchaseFiveHundredBlackDollars();
  }

  public void OpenBlackDollarPurchasePage()
  {
    Application.OpenURL(string.Format("https://spellbook.com/purchase/tib/blackdollars.php?playername={0}&mobile=1", (object) TibProxy.gameState.MyPlayer.Name));
    this.Hide();
  }

  public void OnCloseClicked()
  {
    DialogManager.ClosingDialog((DialogBase) this);
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this._defaultMessageText = this.messageText.text;
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.messageText.text = this._defaultMessageText;
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
