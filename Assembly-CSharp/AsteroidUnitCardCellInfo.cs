﻿// Decompiled with JetBrains decompiler
// Type: AsteroidUnitCardCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class AsteroidUnitCardCellInfo : UnitCardCellInfoBase<Asteroid, AsteroidUnitCardCell>
{
  public AsteroidUnitCardCellInfo(Transform prefab, Asteroid data)
    : base(prefab, data)
  {
  }

  protected override void OnUpdateCellVisuals()
  {
    base.OnUpdateCellVisuals();
    this.mCellInstance.asteroidClass = this.mCellData.Class;
    this.mCellInstance.harvestTimerText = TibProxy.gameState == null || TibProxy.gameState.HarvestCooldown.IsFinished ? string.Empty : TibProxy.gameState.HarvestCooldown.RemainingSecondsString;
  }
}
