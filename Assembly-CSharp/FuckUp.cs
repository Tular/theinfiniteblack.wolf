﻿// Decompiled with JetBrains decompiler
// Type: FuckUp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class FuckUp
{
  private readonly List<GeneralEquipmentItemCellInfo> _itemInfos = new List<GeneralEquipmentItemCellInfo>(10);
  private readonly EquipmentItem _item;
  private readonly Transform _prefab;
  private int _currentFrameInfoCount;
  private int _prevFrameInfoCount;

  public FuckUp(EquipmentItem item, Transform prefab)
  {
    this._item = item;
    this._prefab = prefab;
  }

  public bool hasItems
  {
    get
    {
      return 0 < this._itemInfos.Count;
    }
  }

  public void Add()
  {
    if (this._currentFrameInfoCount < this._itemInfos.Count)
    {
      ++this._currentFrameInfoCount;
    }
    else
    {
      this._itemInfos.Add(new GeneralEquipmentItemCellInfo(this._prefab, this._item));
      ++this._currentFrameInfoCount;
    }
  }

  public void Clear()
  {
    this._itemInfos.Clear();
    this._currentFrameInfoCount = 0;
    this._prevFrameInfoCount = 0;
  }

  public void AddTo(List<IScrollViewCellInfo> infoList)
  {
    if (this._currentFrameInfoCount == 0)
    {
      this._itemInfos.Clear();
      this._prevFrameInfoCount = 0;
    }
    else
    {
      if (this._currentFrameInfoCount > this._prevFrameInfoCount)
      {
        int num = this._currentFrameInfoCount - this._prevFrameInfoCount;
        for (int index = 0; index < num; ++index)
          this._itemInfos.Add(new GeneralEquipmentItemCellInfo(this._prefab, this._item));
      }
      List<GeneralEquipmentItemCellInfo> equipmentItemCellInfoList = new List<GeneralEquipmentItemCellInfo>(10);
      if (this._currentFrameInfoCount < this._prevFrameInfoCount)
      {
        int num1 = this._prevFrameInfoCount - this._currentFrameInfoCount;
        int num2 = 0;
        for (int index = 0; index < this._itemInfos.Count; ++index)
        {
          if (!this._itemInfos[index].isBound)
          {
            equipmentItemCellInfoList.Add(this._itemInfos[index]);
            ++num2;
            if (num2 == num1)
              break;
          }
        }
        if (num2 < num1)
        {
          for (int index = 0; index < this._itemInfos.Count; ++index)
          {
            if (this._itemInfos[index].isBound)
            {
              equipmentItemCellInfoList.Add(this._itemInfos[index]);
              ++num2;
              if (num2 == num1)
                break;
            }
          }
        }
        for (int index = 0; index < equipmentItemCellInfoList.Count; ++index)
          this._itemInfos.Remove(equipmentItemCellInfoList[index]);
      }
      for (int index = 0; index < this._itemInfos.Count; ++index)
        infoList.Add((IScrollViewCellInfo) this._itemInfos[index]);
      this._currentFrameInfoCount = 0;
      this._prevFrameInfoCount = this._itemInfos.Count;
    }
  }
}
