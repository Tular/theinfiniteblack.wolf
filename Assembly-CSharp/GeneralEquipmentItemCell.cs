﻿// Decompiled with JetBrains decompiler
// Type: GeneralEquipmentItemCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;
using UnityEngine;

[RequireComponent(typeof (UIWidget))]
public class GeneralEquipmentItemCell : MonoBehaviour, IScrollViewCell
{
  public string resourcePath = "items";
  public UITexture itemIcon;
  public GameObject equippedIndicatorIcon;
  public UILabel title;
  public UILabel subTitle;
  public UILabel description;
  public UILabel itemPros;
  public UILabel itemCons;
  public UILabel equipPoints;
  public UILabel durability;
  public UILabel creditValue;
  public UILabel actualBlackDollarValue;
  public UILabel actualCreditValue;
  public UILabel specialNote;
  public GeneralEquipmentItemScrollView scrollView;
  public int minHeight;
  private string _itemIconName;
  private bool _isEquipped;
  private UIWidget _widget;
  private UIToggle _toggle;
  private UIWidget[] _widgets;
  private int _spawnedFrame;

  public EquipmentItem item { get; set; }

  public string itemIconName
  {
    get
    {
      return this._itemIconName;
    }
    set
    {
      if (value.Equals(this._itemIconName))
        return;
      this._itemIconName = value;
      if ((Object) this.itemIcon == (Object) null)
        return;
      if (string.IsNullOrEmpty(this._itemIconName))
      {
        this.itemIcon.mainTexture = (Texture) null;
      }
      else
      {
        this.StopCoroutine(this.LoadItemTexture());
        this.StartCoroutine(this.LoadItemTexture());
      }
    }
  }

  public bool isEquipped
  {
    get
    {
      return this._isEquipped;
    }
    set
    {
      this._isEquipped = value;
      if (!(bool) ((Object) this.equippedIndicatorIcon))
        return;
      NGUITools.SetActiveSelf(this.equippedIndicatorIcon, this._isEquipped);
    }
  }

  public int width
  {
    get
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      return this._widget.width;
    }
    set
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      this._widget.width = value;
    }
  }

  public int height
  {
    get
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      return this._widget.height;
    }
    set
    {
      if ((Object) this._widget == (Object) null)
        this._widget = this.GetComponent<UIWidget>();
      this._widget.height = value;
    }
  }

  public Vector3 extents
  {
    get
    {
      return new Vector3((float) this._widget.width * 0.05f, (float) this._widget.height * 0.5f, 0.0f);
    }
  }

  public virtual void ResetVisuals()
  {
    this.item = (EquipmentItem) null;
    this.itemIconName = string.Empty;
    if ((bool) ((Object) this.title))
      this.title.text = string.Empty;
    if ((bool) ((Object) this.subTitle))
      this.subTitle.text = string.Empty;
    if ((bool) ((Object) this.description))
      this.description.text = string.Empty;
    if ((bool) ((Object) this.itemPros))
      this.itemPros.text = string.Empty;
    if ((bool) ((Object) this.itemCons))
      this.itemCons.text = string.Empty;
    if ((bool) ((Object) this.equipPoints))
      this.equipPoints.text = string.Empty;
    if ((bool) ((Object) this.durability))
      this.durability.text = string.Empty;
    if ((bool) ((Object) this.creditValue))
      this.creditValue.text = string.Empty;
    if ((bool) ((Object) this.actualBlackDollarValue))
      this.actualBlackDollarValue.text = string.Empty;
    if ((bool) ((Object) this.actualCreditValue))
      this.actualCreditValue.text = string.Empty;
    if ((bool) ((Object) this.specialNote))
      this.specialNote.text = string.Empty;
    if ((bool) ((Object) this.equippedIndicatorIcon))
      this.equippedIndicatorIcon.gameObject.SetActive(false);
    if ((Object) this._toggle != (Object) null)
    {
      this._toggle.group = 0;
      this._toggle.value = false;
      if ((Object) this.scrollView != (Object) null)
        EventDelegate.Remove(this._toggle.onChange, new EventDelegate.Callback(this.scrollView.OnItemSelectionChanged));
    }
    for (int index = 0; index < this._widgets.Length; ++index)
    {
      if ((Object) this._widgets[index] == (Object) this._widget)
        this._widget.MarkAsChanged();
      else
        this._widgets[index].enabled = false;
    }
  }

  [DebuggerHidden]
  private IEnumerator LoadItemTexture()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralEquipmentItemCell.\u003CLoadItemTexture\u003Ec__Iterator34()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void Awake()
  {
    this._toggle = this.GetComponent<UIToggle>();
    this._widget = this.GetComponent<UIWidget>();
    this._widgets = this.GetComponentsInChildren<UIWidget>(true);
    if (!(bool) ((Object) this.equippedIndicatorIcon))
      return;
    NGUITools.SetActiveSelf(this.equippedIndicatorIcon, false);
  }

  protected virtual void Update()
  {
    if ((Object) this.scrollView == (Object) null)
      return;
    if (Time.frameCount >= this._spawnedFrame + 2)
      this._widget.alpha = 1f;
    this.isEquipped = TibProxy.gameState != null && this.item != null && (this.scrollView.itemActionContext == EquipmentItemActionContext.EquippedSelf || this.scrollView.itemActionContext == EquipmentItemActionContext.EquippedOther);
    if (this.scrollView.itemActionContext == EquipmentItemActionContext.EquippedOther || this.scrollView.itemActionContext == EquipmentItemActionContext.InventoryOther || this.scrollView.itemActionContext == EquipmentItemActionContext.None)
    {
      if (!((Object) this._toggle != (Object) null))
        return;
      this._toggle.group = 0;
      this._toggle.value = false;
      this._toggle.GetComponent<Collider>().enabled = false;
    }
    else
    {
      if (!((Object) this._toggle != (Object) null) || !this._toggle.value || this._widget.isVisible)
        return;
      this._toggle.value = false;
    }
  }

  private void LateUpdate()
  {
    if (Time.frameCount != this._spawnedFrame + 2)
      return;
    for (int index = 0; index < this._widgets.Length; ++index)
    {
      UILabel widget = this._widgets[index] as UILabel;
      if ((Object) widget != (Object) null)
        widget.ProcessText();
      this._widgets[index].MarkAsChanged();
      this._widgets[index].enabled = true;
    }
  }

  protected virtual void OnSpawned()
  {
    if ((Object) this.scrollView == (Object) null)
      this.scrollView = NGUITools.FindInParents<GeneralEquipmentItemScrollView>(this.gameObject);
    this._spawnedFrame = Time.frameCount;
    this.ResetVisuals();
    if ((Object) this._toggle != (Object) null && (Object) this.scrollView != (Object) null)
    {
      this._toggle.optionCanBeNone = true;
      this._toggle.group = this.scrollView.itemCellToggleGroup;
      this._toggle.GetComponent<Collider>().enabled = true;
      EventDelegate.Add(this._toggle.onChange, new EventDelegate.Callback(this.scrollView.OnItemSelectionChanged));
    }
    this._widget.alpha = 0.0f;
  }

  protected virtual void OnDespawned()
  {
    this.height = this.minHeight;
    this.ResetVisuals();
    this._widget.alpha = 0.0f;
  }
}
