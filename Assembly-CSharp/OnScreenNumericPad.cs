﻿// Decompiled with JetBrains decompiler
// Type: OnScreenNumericPad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Text;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class OnScreenNumericPad : DialogBase
{
  private const string _creditsFormat = "{0:$#,###0}";
  public OnScreenKeyboardKey keyPrefab;
  public UILabel currentValue;
  public UIGrid buttonGrid;
  public OnScreenKeyboardKey number1;
  public OnScreenKeyboardKey number2;
  public OnScreenKeyboardKey number3;
  public OnScreenKeyboardKey number4;
  public OnScreenKeyboardKey number5;
  public OnScreenKeyboardKey number6;
  public OnScreenKeyboardKey number7;
  public OnScreenKeyboardKey number8;
  public OnScreenKeyboardKey number9;
  public OnScreenKeyboardKey number0;
  public System.Action onCancel;
  public System.Action<string, int> onAccept;
  private string _promptFormat;
  private StringBuilder _stringValue;

  protected override bool ProcessArgs(object[] args)
  {
    if (2 > args.Length || 3 < args.Length)
      throw new ArgumentException("Expected args length is 2 or 3");
    this.onAccept = (System.Action<string, int>) args[0];
    this.onCancel = (System.Action) args[1];
    this._promptFormat = args.Length != 3 ? "{0:$#,###0}" : (string) args[2];
    this.panelDepth = UIPanel.nextUnusedDepth;
    return false;
  }

  public void OnClearClicked()
  {
    this._stringValue = (StringBuilder) null;
    this.currentValue.text = string.Empty;
  }

  public void OnSubmitClicked()
  {
    if (this._stringValue == null)
      throw new NullReferenceException("_stringValue is null");
    string s = this._stringValue.ToString().Trim();
    if (string.IsNullOrEmpty(s))
      return;
    int result;
    int.TryParse(s, out result);
    if (this.onAccept != null)
      this.onAccept(s, result);
    this.Hide();
  }

  public void OnCancelClicked()
  {
    if (this.onCancel != null)
      this.onCancel();
    this.Hide();
  }

  private void OnKeyClicked(KeyCode keyCode)
  {
    if (this._stringValue == null)
      this._stringValue = new StringBuilder(20);
    string str = string.Empty;
    switch (keyCode)
    {
      case KeyCode.Keypad0:
        str = "0";
        break;
      case KeyCode.Keypad1:
        str = "1";
        break;
      case KeyCode.Keypad2:
        str = "2";
        break;
      case KeyCode.Keypad3:
        str = "3";
        break;
      case KeyCode.Keypad4:
        str = "4";
        break;
      case KeyCode.Keypad5:
        str = "5";
        break;
      case KeyCode.Keypad6:
        str = "6";
        break;
      case KeyCode.Keypad7:
        str = "7";
        break;
      case KeyCode.Keypad8:
        str = "8";
        break;
      case KeyCode.Keypad9:
        str = "9";
        break;
    }
    int result;
    if (!int.TryParse(this._stringValue.ToString().Trim() + str, out result))
      return;
    this.currentValue.text = string.Format(this._promptFormat, (object) result);
    this._stringValue.Append(str);
  }

  private void ResetVisuals()
  {
    this.currentValue.text = string.Empty;
    this._stringValue = (StringBuilder) null;
    this.onAccept = (System.Action<string, int>) null;
    this.onCancel = (System.Action) null;
  }

  private void CreateKeys()
  {
    this.CreateKey(1, "1", KeyCode.Keypad1, KeyCode.Alpha1, UIKeyBinding.Action.PressAndClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad1)));
    this.CreateKey(2, "2", KeyCode.Keypad2, KeyCode.Alpha2, UIKeyBinding.Action.PressAndClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad2)));
    this.CreateKey(3, "3", KeyCode.Keypad1, KeyCode.Alpha3, UIKeyBinding.Action.PressAndClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad3)));
    this.CreateKey(4, "4", KeyCode.Keypad4, KeyCode.Alpha4, UIKeyBinding.Action.PressAndClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad4)));
    this.CreateKey(5, "5", KeyCode.Keypad1, KeyCode.Alpha5, UIKeyBinding.Action.PressAndClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad5)));
    this.CreateKey(10, "0", KeyCode.Keypad0, KeyCode.Alpha0, UIKeyBinding.Action.PressAndClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad0)));
  }

  private OnScreenKeyboardKey CreateKey(int sortOrder, string keyLabel, KeyCode padCode, KeyCode alphaCode, UIKeyBinding.Action bindingAction, EventDelegate.Callback onBindingAction)
  {
    OnScreenKeyboardKey key = this.CreateKey(keyLabel, padCode, alphaCode, bindingAction, onBindingAction);
    key.name = string.Format("{0:0000}", (object) sortOrder);
    return key;
  }

  private OnScreenKeyboardKey CreateKey(string keyLabel, KeyCode padCode, KeyCode alphaCode, UIKeyBinding.Action bindingAction, EventDelegate.Callback onClick)
  {
    OnScreenKeyboardKey component = this.gameObject.AddChild(this.keyPrefab.gameObject).GetComponent<OnScreenKeyboardKey>();
    component.keyLabel.text = keyLabel;
    component.AddKeyBinding(padCode, bindingAction);
    component.AddKeyBinding(alphaCode, bindingAction);
    EventDelegate.Add(component.onClick, onClick);
    return component;
  }

  protected override void OnInitialize()
  {
    this.number1.keyLabel.text = "1";
    this.number2.keyLabel.text = "2";
    this.number3.keyLabel.text = "3";
    this.number4.keyLabel.text = "4";
    this.number5.keyLabel.text = "5";
    this.number6.keyLabel.text = "6";
    this.number7.keyLabel.text = "7";
    this.number8.keyLabel.text = "8";
    this.number9.keyLabel.text = "9";
    this.number0.keyLabel.text = "0";
    EventDelegate.Add(this.number1.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad1)));
    EventDelegate.Add(this.number2.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad2)));
    EventDelegate.Add(this.number3.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad3)));
    EventDelegate.Add(this.number4.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad4)));
    EventDelegate.Add(this.number5.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad5)));
    EventDelegate.Add(this.number6.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad6)));
    EventDelegate.Add(this.number7.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad7)));
    EventDelegate.Add(this.number8.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad8)));
    EventDelegate.Add(this.number9.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad9)));
    EventDelegate.Add(this.number0.onClick, (EventDelegate.Callback) (() => this.OnKeyClicked(KeyCode.Keypad0)));
  }

  protected override void OnHide()
  {
    this.ResetVisuals();
  }

  protected override void OnShow()
  {
    UICamera.selectedObject = (GameObject) null;
    this.OnClearClicked();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  private void OnDisable()
  {
    this._stringValue = (StringBuilder) null;
  }
}
