﻿// Decompiled with JetBrains decompiler
// Type: ScrollViewCellInfoBase`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class ScrollViewCellInfoBase<TData, TCell> : IScrollViewCellInfo where TData : class where TCell : MonoBehaviour, IScrollViewCell
{
  private Vector3 _localPosition;
  protected TData mCellData;
  private readonly TCell _cellPrefab;
  protected TCell mCellInstance;
  private Transform _cellTransform;

  protected ScrollViewCellInfoBase(Transform prefab, TData data)
  {
    this.cellPrefab = prefab;
    this._cellPrefab = this.cellPrefab.GetComponent<TCell>();
    this.mCellData = data;
  }

  public virtual Vector3 extents
  {
    get
    {
      return this.mCellView.extents;
    }
  }

  public Transform cellPrefab { get; private set; }

  public System.Type cellType
  {
    get
    {
      return typeof (TCell);
    }
  }

  public bool isBound
  {
    get
    {
      return (UnityEngine.Object) this.mCellInstance != (UnityEngine.Object) null;
    }
  }

  public Vector3 localPosition
  {
    get
    {
      return this._localPosition;
    }
    set
    {
      this._localPosition = value;
      if (!this.isBound)
        return;
      this.cellTransform.localPosition = this._localPosition;
    }
  }

  protected abstract void SetVisuals(TCell cell);

  public void Unbind()
  {
    if ((UnityEngine.Object) this.mCellInstance != (UnityEngine.Object) null)
    {
      this.mCellInstance.ResetVisuals();
      this.mCellInstance = (TCell) null;
    }
    this._cellTransform = (Transform) null;
  }

  public void BindCell(Transform cellTrans)
  {
    this.mCellInstance = cellTrans.GetComponent<TCell>();
    this.SetVisuals(this.mCellInstance);
    this._cellTransform = cellTrans;
    this._cellTransform.localPosition = this.localPosition;
  }

  public object cellData
  {
    get
    {
      return (object) this.mCellData;
    }
  }

  public TCell mCellView
  {
    get
    {
      return this.mCellInstance ?? this._cellPrefab;
    }
  }

  public Transform cellTransform
  {
    get
    {
      return this._cellTransform;
    }
  }

  public virtual bool IsContainedIn(Bounds bounds)
  {
    if (!bounds.Contains(this.localPosition + this.extents))
      return bounds.Contains(this.localPosition - this.extents);
    return true;
  }
}
