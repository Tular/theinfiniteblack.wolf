﻿// Decompiled with JetBrains decompiler
// Type: PrivateChatChannelCellInfoCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PrivateChatChannelCellInfoCollection : IPooledScrollViewCellInifoCollection<PrivateChatChannelCellInfo>
{
  private int _lastValuesEvaluationFrame = int.MinValue;
  private readonly List<PrivateChatChannelCellInfo> _channels = new List<PrivateChatChannelCellInfo>();
  private bool _dataWasReset;

  public bool dataWasReset
  {
    get
    {
      return this._dataWasReset;
    }
    protected set
    {
      this._dataWasReset = value;
      if (!this._dataWasReset)
        return;
      this.dataWasModified = true;
    }
  }

  public bool dataWasModified { get; private set; }

  public int Count
  {
    get
    {
      return this._channels.Count;
    }
  }

  public PrivateChatChannelCellInfo first
  {
    get
    {
      return this.values.FirstOrDefault<PrivateChatChannelCellInfo>();
    }
  }

  public PrivateChatChannelCellInfo last
  {
    get
    {
      return this.values.LastOrDefault<PrivateChatChannelCellInfo>();
    }
  }

  public IEnumerable<PrivateChatChannelCellInfo> values
  {
    get
    {
      if (this._lastValuesEvaluationFrame != Time.frameCount && this.dataWasModified)
      {
        this._channels.Sort((Comparison<PrivateChatChannelCellInfo>) ((a, b) => string.Compare(a.channelName, b.channelName, StringComparison.OrdinalIgnoreCase)));
        this._lastValuesEvaluationFrame = Time.frameCount;
        this.dataWasModified = false;
      }
      return (IEnumerable<PrivateChatChannelCellInfo>) this._channels;
    }
  }

  public void Add(PrivateChatChannelCellInfo cellInfo)
  {
    if (this._channels.FirstOrDefault<PrivateChatChannelCellInfo>((Func<PrivateChatChannelCellInfo, bool>) (c => c.key == cellInfo.key)) != null)
      return;
    this._channels.Add(cellInfo);
    this.dataWasModified = true;
  }

  public void Remove(PrivateChatChannelCellInfo cellInfo)
  {
    if (this._channels.FirstOrDefault<PrivateChatChannelCellInfo>((Func<PrivateChatChannelCellInfo, bool>) (c => c.key == cellInfo.key)) == null)
      return;
    this._channels.Remove(cellInfo);
    this.dataWasModified = true;
  }

  public void Remove(string channelKey)
  {
    PrivateChatChannelCellInfo cellInfo = this._channels.FirstOrDefault<PrivateChatChannelCellInfo>((Func<PrivateChatChannelCellInfo, bool>) (c => c.key == channelKey));
    if (cellInfo == null)
      return;
    this.Remove(cellInfo);
  }

  public void Clear()
  {
    this._channels.Clear();
    this.dataWasReset = true;
  }

  public bool TryGetChannel(string channelKey, out PrivateChatChannelCellInfo cellInfo)
  {
    cellInfo = this._channels.FirstOrDefault<PrivateChatChannelCellInfo>((Func<PrivateChatChannelCellInfo, bool>) (c => c.key == channelKey));
    return cellInfo != null;
  }
}
