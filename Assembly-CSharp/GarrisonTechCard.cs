﻿// Decompiled with JetBrains decompiler
// Type: GarrisonTechCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Unity;
using UnityEngine;

public class GarrisonTechCard : MonoBehaviour
{
  public UILabel titleLabel;
  public UILabel descriptionLabel;
  public UISprite headingBackground;
  public UISprite cellBackground;
  public Color normalHeadingColor;
  public Color normalCellColor;
  public Color activeHeadingColor;
  public Color activeCellColor;
  private TechnologyType _techType;
  public GarrisonDialogContext context;

  public string title
  {
    get
    {
      if ((bool) ((UnityEngine.Object) this.titleLabel))
        return this.titleLabel.text;
      return string.Empty;
    }
    set
    {
      if (!(bool) ((UnityEngine.Object) this.titleLabel))
        return;
      this.titleLabel.text = value;
    }
  }

  public string description
  {
    get
    {
      if ((bool) ((UnityEngine.Object) this.descriptionLabel))
        return this.descriptionLabel.text;
      return string.Empty;
    }
    set
    {
      if (!(bool) ((UnityEngine.Object) this.descriptionLabel))
        return;
      this.descriptionLabel.text = value;
    }
  }

  public TechnologyType techType
  {
    get
    {
      return this._techType;
    }
    set
    {
      this._techType = value;
      this.title = this.techType.Name();
      this.description = this.techType.Description();
      this.name = ((Enum) this._techType).ToString();
    }
  }

  private void OnClick()
  {
    this.ResearchTech();
  }

  private void ResearchTech()
  {
    if (this.context == null || this.context.garrison == null || (this.context.garrison.Tech[this.techType] || this.context.garrison.Tech[this.techType]))
      return;
    TibProxy.gameState.DoResearch(this.techType);
  }

  private void Update()
  {
    if (this.context == null || this.context.garrison == null)
      return;
    bool flag = this.context.garrison.Tech[this.techType];
    this.headingBackground.color = !flag ? this.normalHeadingColor : this.activeHeadingColor;
    this.cellBackground.color = !flag ? this.normalCellColor : this.activeCellColor;
  }
}
