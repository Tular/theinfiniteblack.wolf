﻿// Decompiled with JetBrains decompiler
// Type: UIPlayersSelection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIPopupList))]
public class UIPlayersSelection : MonoBehaviour
{
  public UIPopupList galaxySize;
  private UIPopupList mAllowed;

  private void Awake()
  {
    this.mAllowed = this.GetComponent<UIPopupList>();
  }

  private void Start()
  {
    EventDelegate.Add(this.galaxySize.onChange, new EventDelegate.Callback(this.OnSelection));
    this.OnSelection();
  }

  private void OnEnable()
  {
    this.OnSelection();
  }

  private void OnSelection()
  {
    string str1 = this.galaxySize.value;
    string str2 = this.mAllowed.value;
    this.mAllowed.items.Clear();
    if (str1 == "Small")
    {
      this.mAllowed.items.Add("2");
      this.mAllowed.items.Add("3");
    }
    else if (str1 == "Medium" || str1 == "Defensible")
    {
      this.mAllowed.items.Add("2");
      this.mAllowed.items.Add("3");
      this.mAllowed.items.Add("4");
      this.mAllowed.items.Add("5");
      this.mAllowed.items.Add("2 vs 2");
    }
    else
    {
      this.mAllowed.items.Add("2");
      this.mAllowed.items.Add("3");
      this.mAllowed.items.Add("4");
      this.mAllowed.items.Add("5");
      this.mAllowed.items.Add("6");
      this.mAllowed.items.Add("2 vs 2");
      this.mAllowed.items.Add("3 vs 3");
      this.mAllowed.items.Add("2 vs 2 vs 2");
    }
    this.mAllowed.value = !this.mAllowed.items.Contains(str2) ? "2" : str2;
  }
}
