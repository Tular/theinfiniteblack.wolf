﻿// Decompiled with JetBrains decompiler
// Type: TriggerOnMapScroll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Unity;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnMapScroll : MonoBehaviour
{
  public UnityEvent onMapScrollStart;
  public UnityEvent onMapScrollEnd;

  private void EasyTouch_On_SwipeEnd(Gesture gesture)
  {
    if (TibProxy.mySettings == null || !TibProxy.mySettings.EntityList.HideOnMapScroll || (!this.enabled || this.onMapScrollEnd == null))
      return;
    this.onMapScrollEnd.Invoke();
  }

  private void EasyTouch_On_SwipeStart(Gesture gesture)
  {
    if (TibProxy.mySettings == null || !TibProxy.mySettings.EntityList.HideOnMapScroll || (!this.enabled || this.onMapScrollStart == null))
      return;
    this.onMapScrollStart.Invoke();
  }

  private void Awake()
  {
    if (this.onMapScrollStart == null)
      this.onMapScrollStart = new UnityEvent();
    if (this.onMapScrollStart != null)
      return;
    this.onMapScrollStart = new UnityEvent();
  }

  private void Start()
  {
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
    EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
    EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
  }

  private void OnDestroy()
  {
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.EasyTouch_On_SwipeEnd);
  }
}
