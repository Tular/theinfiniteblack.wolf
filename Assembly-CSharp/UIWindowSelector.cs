﻿// Decompiled with JetBrains decompiler
// Type: UIWindowSelector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIWindowSelector : MonoBehaviour
{
  private static bool mFirstTime = true;
  public UIPanel gameType;
  public UIPanel newGame;

  private void Start()
  {
    if (!UIWindowSelector.mFirstTime)
    {
      UIWindow.Show(this.gameType);
      UIWindow.Show(this.newGame);
    }
    UIWindowSelector.mFirstTime = false;
  }
}
