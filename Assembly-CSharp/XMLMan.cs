﻿// Decompiled with JetBrains decompiler
// Type: XMLMan
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class XMLMan
{
  private static System.Type[] extraTypes = new System.Type[5]
  {
    typeof (ValueWrapper<int>),
    typeof (ValueWrapper<float>),
    typeof (ValueWrapper<double>),
    typeof (ValueWrapper<Decimal>),
    typeof (ValueWrapper<bool>)
  };

  public static string Serialize<T>(T value)
  {
    if ((object) value == null)
      return (string) null;
    XmlSerializer xmlSerializer = new XmlSerializer(typeof (T));
    XmlWriterSettings settings = new XmlWriterSettings();
    settings.Indent = false;
    settings.OmitXmlDeclaration = false;
    using (StringWriter stringWriter = new StringWriter())
    {
      using (XmlWriter xmlWriter = XmlWriter.Create((TextWriter) stringWriter, settings))
        xmlSerializer.Serialize(xmlWriter, (object) value);
      return stringWriter.ToString();
    }
  }

  public static void SaveFile(string directoryName, string fileName, System.Type type, object data)
  {
    string path1 = Path.Combine(XMLMan.GetRoot(), directoryName);
    TheInfiniteBlack.Library.Log.D((object) new XMLMan(), nameof (SaveFile), string.Format("fullDirectoryPath: {0}", (object) path1));
    using (FileStream fileStream = new FileStream(Path.Combine(path1, fileName), FileMode.Create))
      new XmlSerializer(type).Serialize((Stream) fileStream, data);
  }

  public static bool DoesFileExist(string directoryName, string fileName)
  {
    return File.Exists(Path.Combine(Path.Combine(XMLMan.GetRoot(), directoryName), fileName));
  }

  public static object LoadFileIfExists(string directoryName, string fileName, System.Type type)
  {
    string path1 = Path.Combine(XMLMan.GetRoot(), directoryName);
    if (!File.Exists(Path.Combine(path1, fileName)))
      return (object) null;
    using (FileStream fileStream = new FileStream(Path.Combine(path1, fileName), FileMode.Open))
      return new XmlSerializer(type, XMLMan.extraTypes).Deserialize((Stream) fileStream);
  }

  public static object LoadDataFromResourcesIfExists(string nameWithoutExtension, System.Type type)
  {
    TextAsset textAsset = (TextAsset) Resources.Load(nameWithoutExtension, typeof (TextAsset));
    if ((UnityEngine.Object) textAsset != (UnityEngine.Object) null)
      return new XmlSerializer(type, XMLMan.extraTypes).Deserialize((XmlReader) new XmlTextReader((TextReader) new StringReader(textAsset.text)));
    return (object) null;
  }

  public static string GetRoot()
  {
    if (Application.platform == RuntimePlatform.IPhonePlayer)
      return Application.persistentDataPath;
    return Application.persistentDataPath;
  }
}
