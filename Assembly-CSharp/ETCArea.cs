﻿// Decompiled with JetBrains decompiler
// Type: ETCArea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ETCArea : MonoBehaviour
{
  public bool show;

  public ETCArea()
  {
    this.show = true;
  }

  public void Awake()
  {
    this.GetComponent<Image>().enabled = this.show;
  }

  public void ApplyPreset(ETCArea.AreaPreset preset)
  {
    RectTransform component = this.transform.parent.GetComponent<RectTransform>();
    switch (preset)
    {
      case ETCArea.AreaPreset.TopLeft:
        this.rectTransform().anchoredPosition = new Vector2((float) (-(double) component.rect.width / 4.0), component.rect.height / 4f);
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, component.rect.width / 2f);
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, component.rect.height / 2f);
        this.rectTransform().anchorMin = new Vector2(0.0f, 1f);
        this.rectTransform().anchorMax = new Vector2(0.0f, 1f);
        this.rectTransform().anchoredPosition = new Vector2(this.rectTransform().sizeDelta.x / 2f, (float) (-(double) this.rectTransform().sizeDelta.y / 2.0));
        break;
      case ETCArea.AreaPreset.TopRight:
        this.rectTransform().anchoredPosition = new Vector2(component.rect.width / 4f, component.rect.height / 4f);
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, component.rect.width / 2f);
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, component.rect.height / 2f);
        this.rectTransform().anchorMin = new Vector2(1f, 1f);
        this.rectTransform().anchorMax = new Vector2(1f, 1f);
        this.rectTransform().anchoredPosition = new Vector2((float) (-(double) this.rectTransform().sizeDelta.x / 2.0), (float) (-(double) this.rectTransform().sizeDelta.y / 2.0));
        break;
      case ETCArea.AreaPreset.BottomLeft:
        this.rectTransform().anchoredPosition = new Vector2((float) (-(double) component.rect.width / 4.0), (float) (-(double) component.rect.height / 4.0));
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, component.rect.width / 2f);
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, component.rect.height / 2f);
        this.rectTransform().anchorMin = new Vector2(0.0f, 0.0f);
        this.rectTransform().anchorMax = new Vector2(0.0f, 0.0f);
        this.rectTransform().anchoredPosition = new Vector2(this.rectTransform().sizeDelta.x / 2f, this.rectTransform().sizeDelta.y / 2f);
        break;
      case ETCArea.AreaPreset.BottomRight:
        this.rectTransform().anchoredPosition = new Vector2(component.rect.width / 4f, (float) (-(double) component.rect.height / 4.0));
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, component.rect.width / 2f);
        this.rectTransform().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, component.rect.height / 2f);
        this.rectTransform().anchorMin = new Vector2(1f, 0.0f);
        this.rectTransform().anchorMax = new Vector2(1f, 0.0f);
        this.rectTransform().anchoredPosition = new Vector2((float) (-(double) this.rectTransform().sizeDelta.x / 2.0), this.rectTransform().sizeDelta.y / 2f);
        break;
    }
  }

  public enum AreaPreset
  {
    Choose,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
  }
}
