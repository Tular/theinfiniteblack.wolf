﻿// Decompiled with JetBrains decompiler
// Type: GeneralScrollViewBase`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using PathologicalGames;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using TheInfiniteBlack.Unity;
using UnityEngine;

public abstract class GeneralScrollViewBase<T1, T2> : MonoBehaviour where T2 : IGeneralScrollViewContext<T1>
{
  public Transform prefItemCard;
  public UIScrollView cardScrollView;
  public SpawnPool cardSpawnPool;
  public T2 context;
  private bool _refreshView;

  protected virtual void CreateCards()
  {
    foreach (T1 obj in this.context.items)
      this.InitializeCard(this.cardSpawnPool.Spawn(this.prefItemCard.transform), obj).Refresh();
  }

  [ContextMenu("RefreshView")]
  public void RefreshView()
  {
    if (!this.context.hasChanged)
      return;
    this.LogD("Context Has Changed");
    this._refreshView = true;
    this.Clear();
    this.cardScrollView.ResetPosition();
    this.CreateCards();
    this.OnRefreshView();
    this.cardScrollView.ResetPosition();
  }

  [ContextMenu("Dump Context Items")]
  public void DumpContextItems()
  {
    this.LogD(string.Format("Context Count: {0}", (object) this.context.items.Count<T1>()));
    foreach (T1 obj in this.context.items)
      this.LogD(string.Format("{0}", (object) obj.ToString()));
  }

  public virtual void Clear()
  {
    this.cardSpawnPool.DespawnAll();
  }

  protected abstract GeneralScrollViewCardBase InitializeCard(Transform spawnedTrans, T1 item);

  protected abstract void OnRefreshView();

  protected virtual void Awake()
  {
    if ((bool) ((Object) this.cardSpawnPool))
      return;
    this.cardSpawnPool = this.GetComponentInChildren<SpawnPool>();
  }

  protected virtual void OnEnable()
  {
  }

  protected virtual void OnDisable()
  {
    Resources.UnloadUnusedAssets();
  }

  [DebuggerHidden]
  private IEnumerator RefreshViewCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GeneralScrollViewBase<T1, T2>.\u003CRefreshViewCoroutine\u003Ec__IteratorE()
    {
      \u003C\u003Ef__this = this
    };
  }
}
