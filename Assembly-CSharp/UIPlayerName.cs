﻿// Decompiled with JetBrains decompiler
// Type: UIPlayerName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIPlayerName : MonoBehaviour
{
  public string playerName = "Guest";
  public UISprite background;
  public UISprite icon;
  public UILabel label;
  private bool mIsVisible;

  public void UpdateInfo(bool isVisible)
  {
    this.icon.spriteName = "Circle - Human";
    this.label.text = this.playerName;
    Color white = Color.white;
    this.icon.color = white;
    white.a = this.label.alpha;
    this.label.color = white;
    if (this.mIsVisible == isVisible)
      return;
    this.mIsVisible = isVisible;
    TweenAlpha.Begin(this.icon.gameObject, 0.25f, !isVisible ? 1f : 0.0f).method = UITweener.Method.EaseInOut;
  }
}
