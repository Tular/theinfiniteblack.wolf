﻿// Decompiled with JetBrains decompiler
// Type: WaypointsDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[AdvancedInspector.AdvancedInspector(InspectDefaultItems = true)]
public class WaypointsDialog : DialogBase
{
  [Inspect(0)]
  public UILabel inputX;
  [Inspect(1)]
  public UILabel inputY;
  [Inspect(2)]
  public SBUIButton setCourseBtn;
  private int _x;
  private int _y;
  private UILabel _selectedInput;
  private UIToggle _toggleX;
  private UIToggle _toggleY;
  private UIToggle _toggleSetCourse;
  private GameObject _hovered;

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  public void DoSetCourse()
  {
    this.LogD(string.Format("Int values: [{0},{1}]", (object) this._x, (object) this._y));
    if (TibProxy.gameState == null || 0 >= this._x || (this._x > 80 || 0 >= this._y) || this._y > 80)
      return;
    TibProxy.gameState.Course.Set(this._x, this._y);
    this.Hide();
  }

  public void DoCancel()
  {
    if (TibProxy.gameState == null)
      return;
    this.Hide();
  }

  public void OnSelectedInputChanged()
  {
    if ((Object) UIToggle.current != (Object) this._toggleSetCourse)
      this._hovered = (GameObject) null;
    this.LogD(UIToggle.current.name + " " + (object) UIToggle.current.value);
  }

  public void OnKeypad_0()
  {
    this.OnKeyClicked(KeyCode.Keypad0);
  }

  public void OnKeypad_1()
  {
    this.OnKeyClicked(KeyCode.Keypad1);
  }

  public void OnKeypad_2()
  {
    this.OnKeyClicked(KeyCode.Keypad2);
  }

  public void OnKeypad_3()
  {
    this.OnKeyClicked(KeyCode.Keypad3);
  }

  public void OnKeypad_4()
  {
    this.OnKeyClicked(KeyCode.Keypad4);
  }

  public void OnKeypad_5()
  {
    this.OnKeyClicked(KeyCode.Keypad5);
  }

  public void OnKeypad_6()
  {
    this.OnKeyClicked(KeyCode.Keypad6);
  }

  public void OnKeypad_7()
  {
    this.OnKeyClicked(KeyCode.Keypad7);
  }

  public void OnKeypad_8()
  {
    this.OnKeyClicked(KeyCode.Keypad8);
  }

  public void OnKeypad_9()
  {
    this.OnKeyClicked(KeyCode.Keypad9);
  }

  protected override void OnAwake()
  {
    base.OnAwake();
    this._toggleX = this.inputX.GetComponent<UIToggle>();
    this._toggleY = this.inputY.GetComponent<UIToggle>();
    this._toggleSetCourse = this.setCourseBtn.GetComponent<UIToggle>();
  }

  protected override void OnShow()
  {
    base.OnShow();
    this._x = 0;
    this._y = 0;
    this._hovered = (GameObject) null;
    this.inputX.text = string.Empty;
    this.inputY.text = string.Empty;
    this._toggleX.value = true;
  }

  protected override void OnHide()
  {
    base.OnHide();
    this._x = 0;
    this._y = 0;
  }

  private void Update()
  {
    if (Input.GetKeyUp(KeyCode.Tab) || Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return))
    {
      if (this._toggleX.value)
        this._toggleY.value = true;
      else if (this._toggleY.value)
      {
        this._toggleSetCourse.value = true;
        this._hovered = this.setCourseBtn.gameObject;
        UICamera.selectedObject = this.setCourseBtn.gameObject;
      }
      else if (Input.GetKeyUp(KeyCode.Tab))
      {
        this._toggleX.value = true;
        this._hovered = (GameObject) null;
      }
      else
      {
        if (!((Object) this._hovered == (Object) this.setCourseBtn.gameObject))
          return;
        this.DoSetCourse();
      }
    }
    else
    {
      if (this._toggleSetCourse.value && (Object) UICamera.selectedObject == (Object) this._hovered)
        UICamera.hoveredObject = this._hovered;
      else
        this._hovered = (GameObject) null;
      if (!Input.GetKeyDown(KeyCode.Backspace) && !Input.GetKeyDown(KeyCode.Delete))
        return;
      this.OnKeyClicked(KeyCode.Backspace);
    }
  }

  private void OnKeyClicked(KeyCode keyCode)
  {
    if (this._toggleX.value)
    {
      this._selectedInput = this.inputX;
    }
    else
    {
      if (!this._toggleY.value)
        return;
      this._selectedInput = this.inputY;
    }
    string s = string.Empty;
    switch (keyCode)
    {
      case KeyCode.Keypad0:
        s = "0";
        break;
      case KeyCode.Keypad1:
        s = "1";
        break;
      case KeyCode.Keypad2:
        s = "2";
        break;
      case KeyCode.Keypad3:
        s = "3";
        break;
      case KeyCode.Keypad4:
        s = "4";
        break;
      case KeyCode.Keypad5:
        s = "5";
        break;
      case KeyCode.Keypad6:
        s = "6";
        break;
      case KeyCode.Keypad7:
        s = "7";
        break;
      case KeyCode.Keypad8:
        s = "8";
        break;
      case KeyCode.Keypad9:
        s = "9";
        break;
      case KeyCode.Backspace:
      case KeyCode.Delete:
        int result1;
        if (1 < this._selectedInput.text.Length)
        {
          this._selectedInput.text = this._selectedInput.text[0].ToString().Trim();
          if (!int.TryParse(this._selectedInput.text, out result1))
            result1 = 0;
        }
        else
        {
          this._selectedInput.text = string.Empty;
          result1 = 0;
        }
        if ((Object) this._selectedInput == (Object) this.inputX)
        {
          this._x = result1;
          return;
        }
        this._y = result1;
        return;
    }
    int result2;
    if (!int.TryParse(this._selectedInput.text.Trim() + s, out result2))
      return;
    if (0 < result2 && result2 <= 80)
      this._selectedInput.text = result2.ToString();
    else if (int.TryParse(s, out result2))
    {
      this._selectedInput.text = result2.ToString();
    }
    else
    {
      result2 = 0;
      this._selectedInput.text = "0";
    }
    if ((Object) this._selectedInput == (Object) this.inputX)
      this._x = result2;
    else
      this._y = result2;
  }
}
