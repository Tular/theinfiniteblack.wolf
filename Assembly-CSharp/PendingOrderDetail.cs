﻿// Decompiled with JetBrains decompiler
// Type: PendingOrderDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;

public class PendingOrderDetail
{
  public string characterName;
  public string orderId;
  public string packageName;
  public string productId;
  public string purchaseToken;
  public string json;
  public string signature;

  public override string ToString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.AppendFormat("{0}:{1}:{2}:{3}:{4}:{5}:{6}", (object) this.characterName, (object) this.orderId, (object) this.packageName, (object) this.productId, (object) this.purchaseToken, (object) this.json, (object) this.signature);
    return stringBuilder.ToString();
  }
}
