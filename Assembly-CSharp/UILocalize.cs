﻿// Decompiled with JetBrains decompiler
// Type: UILocalize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/UI/Localize")]
[RequireComponent(typeof (UIWidget))]
[ExecuteInEditMode]
public class UILocalize : MonoBehaviour
{
  public string key;
  private bool mStarted;

  public string value
  {
    set
    {
      if (string.IsNullOrEmpty(value))
        return;
      UIWidget component = this.GetComponent<UIWidget>();
      UILabel uiLabel = component as UILabel;
      UISprite uiSprite = component as UISprite;
      if ((Object) uiLabel != (Object) null)
      {
        UIInput inParents = NGUITools.FindInParents<UIInput>(uiLabel.gameObject);
        if ((Object) inParents != (Object) null && (Object) inParents.label == (Object) uiLabel)
          inParents.defaultText = value;
        else
          uiLabel.text = value;
      }
      else
      {
        if (!((Object) uiSprite != (Object) null))
          return;
        UIButton inParents = NGUITools.FindInParents<UIButton>(uiSprite.gameObject);
        if ((Object) inParents != (Object) null && (Object) inParents.tweenTarget == (Object) uiSprite.gameObject)
          inParents.normalSprite = value;
        uiSprite.spriteName = value;
        uiSprite.MakePixelPerfect();
      }
    }
  }

  private void OnEnable()
  {
    if (!this.mStarted)
      return;
    this.OnLocalize();
  }

  private void Start()
  {
    this.mStarted = true;
    this.OnLocalize();
  }

  private void OnLocalize()
  {
    if (string.IsNullOrEmpty(this.key))
    {
      UILabel component = this.GetComponent<UILabel>();
      if ((Object) component != (Object) null)
        this.key = component.text;
    }
    if (string.IsNullOrEmpty(this.key))
      return;
    this.value = Localization.Get(this.key);
  }
}
