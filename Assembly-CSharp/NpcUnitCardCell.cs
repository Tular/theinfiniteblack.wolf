﻿// Decompiled with JetBrains decompiler
// Type: NpcUnitCardCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public class NpcUnitCardCell : CombatUnitCardCell
{
  public string npcSpritePrefix = "entity_npc_";
  public string entityShipSpritePrefix = "entity_ship_";
  public UISprite entitySprite;
  public UILabel factionLabel;

  public NpcClass npcClass { get; set; }

  public string factionLabelText
  {
    get
    {
      if ((bool) ((Object) this.factionLabel))
        return this.factionLabel.text;
      return string.Empty;
    }
    set
    {
      if (!(bool) ((Object) this.factionLabel))
        return;
      this.factionLabel.text = value;
    }
  }

  public string npcSprite
  {
    get
    {
      return string.Format("{0}{1}", (object) this.npcSpritePrefix, (object) this.npcClass);
    }
  }

  public override void ResetVisuals()
  {
    base.ResetVisuals();
    this.factionLabelText = string.Empty;
    this.entitySprite.spriteName = string.Empty;
  }

  public override void UpdateVisuals()
  {
    base.UpdateVisuals();
    this.SetNpcIcon();
  }

  private string entityShipSprite(ShipClass sc)
  {
    return string.Format("{0}{1}", (object) this.entityShipSpritePrefix, (object) sc);
  }

  private void SetNpcIcon()
  {
    switch (this.npcClass)
    {
      case NpcClass.Raider:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Destroyer);
        break;
      case NpcClass.Bandit:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Cruiser);
        break;
      case NpcClass.Outlaw:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Dreadnought);
        break;
      case NpcClass.Hijacker:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Titan);
        break;
      case NpcClass.Brigand:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Flagship);
        break;
      case NpcClass.Prowler:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Flayer);
        break;
      case NpcClass.Marauder:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Executioner);
        break;
      case NpcClass.Corsair:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Devastator);
        break;
      case NpcClass.Enforcer:
        this.entitySprite.spriteName = this.entityShipSprite(ShipClass.Despoiler);
        break;
      default:
        this.entitySprite.spriteName = this.npcSprite;
        break;
    }
  }
}
