﻿// Decompiled with JetBrains decompiler
// Type: UIUpgradeWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIUpgradeWindow : MonoBehaviour
{
  public UIPanel panel;
  private static UIPanel mPanel;

  private void Awake()
  {
    UIUpgradeWindow.mPanel = this.panel;
  }

  private void OnDestroy()
  {
    UIUpgradeWindow.mPanel = (UIPanel) null;
  }

  public static void Show()
  {
    if (!((Object) UIUpgradeWindow.mPanel != (Object) null))
      return;
    UIWindow.Show(UIUpgradeWindow.mPanel);
  }

  public static void Hide()
  {
    if (!((Object) UIUpgradeWindow.mPanel != (Object) null))
      return;
    UIWindow.Hide(UIUpgradeWindow.mPanel);
  }
}
