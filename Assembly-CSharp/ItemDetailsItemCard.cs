﻿// Decompiled with JetBrains decompiler
// Type: ItemDetailsItemCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

[RequireComponent(typeof (SBUITab))]
public class ItemDetailsItemCard : MonoBehaviour
{
  private const int WeaponSortOrder = 1;
  private const int ArmorSortOrder = 2;
  private const int StorageSortOrder = 3;
  private const int HarvesterSortOrder = 4;
  private const int EngineSortOrder = 5;
  private const int ComputerSortOrder = 6;
  private const int SpecialSortOrder = 7;
  public UITexture icon;
  public UILabel text;
  public string resourcePath;
  private EquipmentItem _item;
  private SBUITab _toggle;
  protected ItemDetailsContext dialogContext;
  private UIToggle _hack;

  public virtual EquipmentItem item
  {
    get
    {
      return this._item;
    }
    set
    {
      this._item = value;
      this.LoadItemTexture(value.Icon);
      this.SetTextField();
      this._toggle.isSelected = false;
    }
  }

  public int toggleGroup
  {
    get
    {
      return this._toggle.toggleGroup;
    }
    set
    {
      this._toggle.toggleGroup = value;
    }
  }

  private void LoadItemTexture(ItemIcon itemIcon)
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) itemIcon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      this.icon.mainTexture = (Texture) texture2D;
  }

  protected virtual void SetTextField()
  {
    switch (this._item.Type)
    {
      case ItemType.WEAPON:
        WeaponItem weaponItem = (WeaponItem) this._item;
        this.name = ItemDetailsItemCard.ItemSortKey(1, (int) weaponItem.Class);
        this.text.text = weaponItem.Class.Name();
        break;
      case ItemType.ARMOR:
        ArmorItem armorItem = (ArmorItem) this._item;
        this.name = ItemDetailsItemCard.ItemSortKey(2, (int) armorItem.Class);
        this.text.text = armorItem.Class.Name();
        break;
      case ItemType.STORAGE:
        StorageItem storageItem = (StorageItem) this._item;
        this.name = ItemDetailsItemCard.ItemSortKey(3, (int) storageItem.Class);
        this.text.text = storageItem.Class.Name();
        break;
      case ItemType.HARVESTER:
        HarvesterItem harvesterItem = (HarvesterItem) this._item;
        this.name = ItemDetailsItemCard.ItemSortKey(4, (int) harvesterItem.Class);
        this.text.text = harvesterItem.Class.Name();
        break;
      case ItemType.ENGINE:
        EngineItem engineItem = (EngineItem) this._item;
        this.name = ItemDetailsItemCard.ItemSortKey(5, (int) engineItem.Class);
        this.text.text = engineItem.Class.Name();
        break;
      case ItemType.COMPUTER:
        ComputerItem computerItem = (ComputerItem) this._item;
        this.name = ItemDetailsItemCard.ItemSortKey(6, (int) computerItem.Class);
        this.text.text = computerItem.Class.Name();
        break;
      case ItemType.SPECIAL:
        SpecialItem specialItem = (SpecialItem) this._item;
        this.name = ItemDetailsItemCard.ItemSortKey(7, (int) specialItem.Class);
        this.text.text = specialItem.Class.Name();
        break;
    }
  }

  private static string ItemSortKey(int typeSortOrder, int typeClass)
  {
    return string.Format("{0:D2}-{1:D2}", (object) typeSortOrder, (object) typeClass);
  }

  protected virtual void Awake()
  {
    this._toggle = this.GetComponent<SBUITab>();
    EventDelegate.Add(this._toggle.onClick, new EventDelegate.Callback(this.SetContextItem));
  }

  private void SetContextItem()
  {
    if (this.dialogContext == null || !this._toggle.isSelected || this.dialogContext.baseItem == this._item)
      return;
    this.dialogContext.baseItem = this._item;
  }

  protected virtual void Start()
  {
    if (this.dialogContext != null)
      return;
    this.dialogContext = NGUITools.FindInParents<ItemDetailsPopup>(this.gameObject).context;
  }

  protected virtual void OnSpawned()
  {
    if (this.dialogContext != null)
      return;
    this.dialogContext = NGUITools.FindInParents<ItemDetailsPopup>(this.gameObject).context;
  }

  protected virtual void OnDespawned()
  {
    Texture mainTexture = this.icon.mainTexture;
    this.icon.mainTexture = (Texture) null;
    Resources.UnloadAsset((Object) mainTexture);
    this.ResetToggle();
  }

  private void ResetToggle()
  {
    if (!(bool) ((Object) this._hack))
      this._hack = this._toggle.GetComponent<UIToggle>();
    this._hack.optionCanBeNone = true;
    this._toggle.isSelected = false;
    this._hack.optionCanBeNone = false;
  }
}
