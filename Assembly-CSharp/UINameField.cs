﻿// Decompiled with JetBrains decompiler
// Type: UINameField
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIInput))]
public class UINameField : MonoBehaviour
{
  private UIInput mInput;

  private void Awake()
  {
    this.mInput = this.GetComponent<UIInput>();
    EventDelegate.Set(this.mInput.onSubmit, new EventDelegate.Callback(this.OnSubmit));
  }

  private void OnEnable()
  {
    this.mInput.value = PlayerProfile.playerName;
  }

  private void OnSubmit()
  {
    string str = UIInput.current.value;
    PlayerProfile.playerName = str;
    if (!(str != PlayerProfile.playerName))
      return;
    this.mInput.value = PlayerProfile.playerName;
  }
}
