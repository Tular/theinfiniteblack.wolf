﻿// Decompiled with JetBrains decompiler
// Type: UISettingsMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UISettingsMenu : MonoBehaviour
{
  public string headingText;
  public UISettingsGroup settingsGroupPrefab;
  public UISettingsDetailSelector detailSelectorPrefab;
  public GameObject optionsRoot;
  public SettingsGroupInfo[] settingGroupInfos;
  protected UITable mTable;

  [ContextMenu("Build")]
  public void BuildMenu()
  {
    foreach (Component componentsInChild in this.optionsRoot.GetComponentsInChildren<UISettingsDetailSelector>(true))
      NGUITools.DestroyImmediate((UnityEngine.Object) componentsInChild.gameObject);
    foreach (SettingsGroupInfo settingsGroupInfo in (IEnumerable<SettingsGroupInfo>) ((IEnumerable<SettingsGroupInfo>) this.settingGroupInfos).OrderBy<SettingsGroupInfo, int>((Func<SettingsGroupInfo, int>) (g => g.sortOrder)))
    {
      UISettingsGroup component = this.optionsRoot.gameObject.AddChild(this.settingsGroupPrefab.gameObject).GetComponent<UISettingsGroup>();
      component.detailSelectorPrefab = this.detailSelectorPrefab;
      component.BuildSelectors(settingsGroupInfo.detailSelectors);
      component.name = string.Format("{0} - {1}", (object) settingsGroupInfo.sortOrder, (object) settingsGroupInfo.headingText);
    }
    this.GetComponent<UITable>().Reposition();
  }

  protected void Awake()
  {
    this.mTable = this.GetComponent<UITable>();
  }

  protected void Start()
  {
  }

  protected void Update()
  {
  }
}
