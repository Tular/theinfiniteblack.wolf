﻿// Decompiled with JetBrains decompiler
// Type: PlayerDirectoryIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Unity;
using UnityEngine;

public abstract class PlayerDirectoryIndex : IDisposable, IPooledScrollViewCellInifoCollection<PlayerDirectoryCellInfo>
{
  protected static readonly Dictionary<int, PlayerDirectoryCellInfo> OnlinePlayerInfos = new Dictionary<int, PlayerDirectoryCellInfo>(1000);
  protected static readonly List<PlayerDirectoryCellInfo> ModList = new List<PlayerDirectoryCellInfo>();
  protected static readonly List<PlayerDirectoryCellInfo> FriendsList = new List<PlayerDirectoryCellInfo>();
  private static readonly List<PlayerDirectoryIndex> DirectoryIndexList = new List<PlayerDirectoryIndex>();
  private readonly List<PlayerDirectoryCellInfo> _values = new List<PlayerDirectoryCellInfo>();
  private int _lastValuesEvaluationFrame = int.MinValue;
  private readonly PlayerDirectoryCellInfo _modHeading;
  private readonly PlayerDirectoryCellInfo _friendsHeading;
  private int _dataModifiedFrame;
  private bool _dataWasModified;
  private int _dataResetFrame;
  private bool _dataWasReset;

  protected PlayerDirectoryIndex(Transform headingPrefab)
  {
    PlayerDirectoryIndex.DirectoryIndexList.Add(this);
    this.mHeadingPrefab = headingPrefab;
    this._modHeading = new PlayerDirectoryCellInfo(this.mHeadingPrefab, "- MODERATORS -");
    this._friendsHeading = new PlayerDirectoryCellInfo(this.mHeadingPrefab, "- FRIENDS -");
  }

  public int indexCount
  {
    get
    {
      return PlayerDirectoryIndex.DirectoryIndexList.Count;
    }
  }

  public int onlinePlayerInfoCount
  {
    get
    {
      return PlayerDirectoryIndex.OnlinePlayerInfos.Count;
    }
  }

  public virtual int Count
  {
    get
    {
      return this.values.Count<PlayerDirectoryCellInfo>();
    }
  }

  public bool dataWasModified
  {
    get
    {
      this.EvaluateValuesExpression();
      return this._dataModifiedFrame == this._lastValuesEvaluationFrame;
    }
    set
    {
      this._dataWasModified = value;
    }
  }

  public bool dataWasReset
  {
    get
    {
      this.EvaluateValuesExpression();
      return this._dataResetFrame == this._lastValuesEvaluationFrame;
    }
    set
    {
      this._dataWasReset = value;
      if (!this._dataWasReset)
        return;
      this._dataWasModified = true;
    }
  }

  public IEnumerable<PlayerDirectoryCellInfo> values
  {
    get
    {
      this.EvaluateValuesExpression();
      return (IEnumerable<PlayerDirectoryCellInfo>) this._values;
    }
  }

  private void AddFilteredValues(ref int cellOrder)
  {
    foreach (PlayerDirectoryCellInfo filteredValue in this.FilteredValues())
    {
      PlayerDirectoryCellInfo directoryCellInfo = filteredValue;
      int num1;
      cellOrder = (num1 = cellOrder) + 1;
      int num2 = num1;
      directoryCellInfo.cellOrder = num2;
      filteredValue.onCellClickedAction = new System.Action<PlayerDirectoryCellInfo>(this.OnCellClicked);
      this._values.Add(filteredValue);
    }
  }

  public PlayerDirectoryCellInfo first
  {
    get
    {
      return this.values.FirstOrDefault<PlayerDirectoryCellInfo>();
    }
  }

  public PlayerDirectoryCellInfo last
  {
    get
    {
      return this.values.LastOrDefault<PlayerDirectoryCellInfo>();
    }
  }

  protected Transform mHeadingPrefab { get; set; }

  protected virtual void OnAdd(PlayerDirectoryCellInfo cellInfo)
  {
  }

  protected virtual void OnRemove(PlayerDirectoryCellInfo cellInfo)
  {
  }

  protected virtual void OnClear()
  {
  }

  protected abstract IEnumerable<PlayerDirectoryCellInfo> FilteredValues();

  public void Add(PlayerDirectoryCellInfo cellInfo)
  {
    if (!PlayerDirectoryIndex.OnlinePlayerInfos.ContainsKey(cellInfo.player.ID))
      PlayerDirectoryIndex.OnlinePlayerInfos.Add(cellInfo.player.ID, cellInfo);
    if (TibProxy.gameState.MySettings != null && TibProxy.gameState.MySettings.Social.IsFriend(cellInfo.player.Name))
      PlayerDirectoryIndex.FriendsList.Add(cellInfo);
    foreach (string moderator in GameSettings.ModeratorList)
    {
      if (string.Compare(cellInfo.player.Name, moderator, StringComparison.OrdinalIgnoreCase) == 0 && !PlayerDirectoryIndex.ModList.Contains(cellInfo))
        PlayerDirectoryIndex.ModList.Add(cellInfo);
    }
    using (List<PlayerDirectoryIndex>.Enumerator enumerator = PlayerDirectoryIndex.DirectoryIndexList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerDirectoryIndex current = enumerator.Current;
        current.OnAdd(cellInfo);
        current.dataWasModified = true;
      }
    }
  }

  public void Remove(PlayerDirectoryCellInfo cellInfo)
  {
    this.Remove(cellInfo.player);
  }

  public PlayerDirectoryCellInfo Remove(ClientPlayer player)
  {
    if (!PlayerDirectoryIndex.OnlinePlayerInfos.ContainsKey(player.ID))
      return (PlayerDirectoryCellInfo) null;
    PlayerDirectoryCellInfo onlinePlayerInfo = PlayerDirectoryIndex.OnlinePlayerInfos[player.ID];
    if (PlayerDirectoryIndex.FriendsList.Contains(onlinePlayerInfo))
      PlayerDirectoryIndex.FriendsList.Remove(onlinePlayerInfo);
    if (PlayerDirectoryIndex.ModList.Contains(onlinePlayerInfo))
      PlayerDirectoryIndex.ModList.Remove(onlinePlayerInfo);
    PlayerDirectoryIndex.OnlinePlayerInfos.Remove(player.ID);
    using (List<PlayerDirectoryIndex>.Enumerator enumerator = PlayerDirectoryIndex.DirectoryIndexList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerDirectoryIndex current = enumerator.Current;
        current.OnRemove(onlinePlayerInfo);
        current.dataWasModified = true;
      }
    }
    return onlinePlayerInfo;
  }

  public void Clear()
  {
    PlayerDirectoryIndex.ModList.Clear();
    PlayerDirectoryIndex.FriendsList.Clear();
    using (List<PlayerDirectoryIndex>.Enumerator enumerator = PlayerDirectoryIndex.DirectoryIndexList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerDirectoryIndex current = enumerator.Current;
        current.OnClear();
        current.dataWasReset = true;
      }
    }
    PlayerDirectoryIndex.OnlinePlayerInfos.Clear();
  }

  protected virtual void OnCellClicked(PlayerDirectoryCellInfo cellInfo)
  {
    if (string.IsNullOrEmpty(cellInfo.address))
      return;
    if (!cellInfo.isBound)
      throw new Exception("Trying to set chat address of unbound cell");
    NGUITools.FindInParents<ChatDialog>(cellInfo.cellTransform).BeginMessageInput(cellInfo.address);
  }

  public bool ContainsInfoFor(ClientPlayer player)
  {
    return PlayerDirectoryIndex.OnlinePlayerInfos.ContainsKey(player.ID);
  }

  public bool ContainsInfoFor(int playerId)
  {
    return PlayerDirectoryIndex.OnlinePlayerInfos.ContainsKey(playerId);
  }

  public void Dispose()
  {
    PlayerDirectoryIndex.DirectoryIndexList.Remove(this);
  }

  protected void AddFriendsList(ref int cellDepth)
  {
    PlayerDirectoryCellInfo friendsHeading = this._friendsHeading;
    int num1;
    cellDepth = (num1 = cellDepth) + 1;
    int num2 = num1;
    friendsHeading.cellOrder = num2;
    this._values.Add(this._friendsHeading);
    using (List<PlayerDirectoryCellInfo>.Enumerator enumerator = PlayerDirectoryIndex.FriendsList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerDirectoryCellInfo current = enumerator.Current;
        if (!PlayerDirectoryIndex.ModList.Contains(current))
        {
          PlayerDirectoryCellInfo directoryCellInfo = current;
          int num3;
          cellDepth = (num3 = cellDepth) + 1;
          int num4 = num3;
          directoryCellInfo.cellOrder = num4;
          current.onCellClickedAction = new System.Action<PlayerDirectoryCellInfo>(this.OnCellClicked);
          this._values.Add(current);
        }
      }
    }
  }

  private void AddModList(ref int cellOrder)
  {
    PlayerDirectoryCellInfo modHeading = this._modHeading;
    int num1;
    cellOrder = (num1 = cellOrder) + 1;
    int num2 = num1;
    modHeading.cellOrder = num2;
    this._values.Add(this._modHeading);
    using (List<PlayerDirectoryCellInfo>.Enumerator enumerator = PlayerDirectoryIndex.ModList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerDirectoryCellInfo current = enumerator.Current;
        PlayerDirectoryCellInfo directoryCellInfo = current;
        int num3;
        cellOrder = (num3 = cellOrder) + 1;
        int num4 = num3;
        directoryCellInfo.cellOrder = num4;
        current.onCellClickedAction = new System.Action<PlayerDirectoryCellInfo>(this.OnCellClicked);
        this._values.Add(current);
      }
    }
  }

  private void EvaluateValuesExpression()
  {
    if (this._lastValuesEvaluationFrame != Time.frameCount && this._dataWasModified)
    {
      int num = 1000;
      this._lastValuesEvaluationFrame = Time.frameCount;
      if (this._dataWasModified)
        this._dataModifiedFrame = this._lastValuesEvaluationFrame;
      if (this._dataWasReset)
        this._dataResetFrame = this._lastValuesEvaluationFrame;
      this._values.Clear();
      this.AddModList(ref num);
      this.AddFriendsList(ref num);
      int cellOrder = 0;
      this.AddFilteredValues(ref cellOrder);
      this._dataWasModified = false;
      this._dataWasReset = false;
    }
    this._lastValuesEvaluationFrame = Time.frameCount;
  }
}
