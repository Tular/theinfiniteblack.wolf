﻿// Decompiled with JetBrains decompiler
// Type: UIFPS
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UILabel))]
public class UIFPS : MonoBehaviour
{
  private const float fpsMeasurePeriod = 0.5f;
  private const string display = "{0} FPS";
  private int m_FpsAccumulator;
  private float m_FpsNextPeriod;
  private int m_CurrentFps;
  private UILabel m_Text;

  private void Start()
  {
    this.m_FpsNextPeriod = Time.realtimeSinceStartup + 0.5f;
    this.m_Text = this.GetComponent<UILabel>();
  }

  private void Update()
  {
    ++this.m_FpsAccumulator;
    if ((double) Time.realtimeSinceStartup <= (double) this.m_FpsNextPeriod)
      return;
    this.m_CurrentFps = (int) ((double) this.m_FpsAccumulator / 0.5);
    this.m_FpsAccumulator = 0;
    this.m_FpsNextPeriod += 0.5f;
    this.m_Text.text = string.Format("{0} FPS", (object) this.m_CurrentFps);
  }
}
