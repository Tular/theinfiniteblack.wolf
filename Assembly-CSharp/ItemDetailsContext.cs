﻿// Decompiled with JetBrains decompiler
// Type: ItemDetailsContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class ItemDetailsContext
{
  private EquipmentItem _baseItem;
  private ShipClass _contextShipClass;
  private ItemRarity _selectedRarity;
  private int _selectedRank;

  public ItemDetailsContext()
  {
    this._contextShipClass = ShipClass.NULL;
  }

  public int lastUpdateFrame { get; private set; }

  public EquipmentItem baseItem
  {
    get
    {
      return this._baseItem;
    }
    set
    {
      this.lastUpdateFrame = Time.frameCount;
      if (value == null)
      {
        this._baseItem = (EquipmentItem) null;
        this.hasRank = false;
      }
      else
      {
        this._baseItem = value;
        switch (this._baseItem.Type)
        {
          case ItemType.WEAPON:
            this.hasRank = true;
            this._selectedRarity = this._baseItem.Rarity;
            this._selectedRank = (int) (this._baseItem.EPCost + this._baseItem.Rarity - 1);
            break;
          case ItemType.ARMOR:
            this.hasRank = true;
            this._selectedRarity = this._baseItem.Rarity;
            this._selectedRank = (int) (this._baseItem.EPCost + this._baseItem.Rarity - 1);
            break;
          case ItemType.STORAGE:
            this.hasRank = true;
            this._selectedRarity = this._baseItem.Rarity;
            this._selectedRank = (int) (this._baseItem.EPCost + this._baseItem.Rarity - 1);
            break;
          case ItemType.HARVESTER:
            this.hasRank = false;
            this._selectedRarity = this._baseItem.Rarity;
            break;
          case ItemType.ENGINE:
            this.hasRank = false;
            this._selectedRarity = this._baseItem.Rarity;
            break;
          case ItemType.COMPUTER:
            this.hasRank = false;
            this._selectedRarity = this._baseItem.Rarity;
            this.contextShipClass = ((ComputerItem) this._baseItem).RequiredShip;
            break;
          case ItemType.SPECIAL:
            this.hasRank = false;
            this._selectedRarity = this._baseItem.Rarity;
            this.contextShipClass = ((SpecialItem) this._baseItem).RequiredShip;
            break;
        }
      }
    }
  }

  public ShipClass contextShipClass
  {
    get
    {
      if (this._contextShipClass == ShipClass.NULL)
        return ShipClass.Shuttle;
      return this._contextShipClass;
    }
    set
    {
      this._contextShipClass = value;
    }
  }

  public bool hasRank { get; private set; }

  public EquipmentItem engineeredItem
  {
    get
    {
      if (this.baseItem == null)
        return (EquipmentItem) null;
      return this.EngineeredItem(this.selectedRarity, (sbyte) this.selectedRank);
    }
  }

  public EquipmentItem EngineeredItem(ItemRarity rarity, sbyte rank)
  {
    return this.EngineeredItem(rarity, rank, this.contextShipClass);
  }

  private EquipmentItem EngineeredItem(ItemRarity rarity, sbyte rank, ShipClass shipClass)
  {
    if (this._baseItem == null)
      return (EquipmentItem) null;
    EquipmentItem equipmentItem = (EquipmentItem) null;
    switch (this._baseItem.Type)
    {
      case ItemType.WEAPON:
        equipmentItem = (EquipmentItem) WeaponCache.Get(((WeaponItem) this.baseItem).Class, rank, rarity, this._baseItem.Durability, this._baseItem.NoDrop, this._baseItem.BindOnEquip);
        break;
      case ItemType.ARMOR:
        equipmentItem = (EquipmentItem) ArmorCache.Get(((ArmorItem) this._baseItem).Class, rank, rarity, this._baseItem.Durability, this._baseItem.NoDrop, this._baseItem.BindOnEquip);
        break;
      case ItemType.STORAGE:
        equipmentItem = (EquipmentItem) StorageCache.Get(((StorageItem) this._baseItem).Class, rank, rarity, this._baseItem.Durability, this._baseItem.NoDrop, this._baseItem.BindOnEquip);
        break;
      case ItemType.HARVESTER:
        equipmentItem = (EquipmentItem) HarvesterCache.Get(((HarvesterItem) this._baseItem).Class, rarity, this._baseItem.Durability, this._baseItem.NoDrop, this._baseItem.BindOnEquip);
        break;
      case ItemType.ENGINE:
        equipmentItem = (EquipmentItem) EngineCache.Get(((EngineItem) this._baseItem).Class, rarity, this._baseItem.Durability, this._baseItem.NoDrop, this._baseItem.BindOnEquip);
        break;
      case ItemType.COMPUTER:
        equipmentItem = (EquipmentItem) ComputerCache.Get(((ComputerItem) this._baseItem).Class, shipClass, rarity, this._baseItem.Durability, this._baseItem.NoDrop, this._baseItem.BindOnEquip);
        break;
      case ItemType.SPECIAL:
        equipmentItem = (EquipmentItem) SpecialCache.Get(((SpecialItem) this._baseItem).Class, shipClass, rarity, this.baseItem.Durability, this.baseItem.NoDrop, this.baseItem.BindOnEquip);
        break;
    }
    return equipmentItem;
  }

  public ItemIcon GetBaseItemIconAtRarity(ItemRarity rarity)
  {
    return this.EngineeredItem(rarity, (sbyte) 1).Icon;
  }

  public ItemRarity selectedRarity
  {
    get
    {
      return this._selectedRarity;
    }
    set
    {
      this.lastUpdateFrame = Time.frameCount;
      this._selectedRarity = value;
    }
  }

  public int selectedRank
  {
    get
    {
      return this._selectedRank;
    }
    set
    {
      this.lastUpdateFrame = Time.frameCount;
      this._selectedRank = value;
    }
  }

  public enum DisplayMode
  {
    Dialog,
    Popup,
  }
}
