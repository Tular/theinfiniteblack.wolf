﻿// Decompiled with JetBrains decompiler
// Type: Finger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Finger : BaseFinger
{
  public float startTimeAction;
  public Vector2 oldPosition;
  public int tapCount;
  public TouchPhase phase;
  public EasyTouch.GestureType gesture;
  public EasyTouch.SwipeDirection oldSwipeType;
}
