﻿// Decompiled with JetBrains decompiler
// Type: AsteroidUnitCardCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;

public class AsteroidUnitCardCell : UnitCardCellBase
{
  public string darkMatterTypeSprite = "entity_asteroid_darkmatter";
  public string iceTypeSprite = "entity_asteroid_ice";
  public string metalTypeSprite = "entity_asteroid_metal";
  public string radioactiveTypeSprite = "entity_asteroid_radioactive";
  public string rockTypeSprite = "entity_asteroid_rock";
  public UISprite entitySprite;
  public UILabel harvestTimer;
  private string _spriteName;

  public AsteroidClass asteroidClass { get; set; }

  public string harvestTimerText
  {
    get
    {
      return this.harvestTimer.text;
    }
    set
    {
      this.harvestTimer.text = value;
    }
  }

  public override void ResetVisuals()
  {
    base.ResetVisuals();
    this.harvestTimer.enabled = false;
    this.harvestTimer.text = string.Empty;
  }

  public override void UpdateVisuals()
  {
    base.UpdateVisuals();
    this.SetAsteroidIcon();
    this.harvestTimer.enabled = !string.IsNullOrEmpty(this.harvestTimerText);
  }

  private void SetAsteroidIcon()
  {
    switch (this.asteroidClass)
    {
      case AsteroidClass.SMALL_ROCK:
      case AsteroidClass.MEDIUM_ROCK:
      case AsteroidClass.LARGE_ROCK:
      case AsteroidClass.VERYLARGE_ROCK:
        this.entitySprite.spriteName = this.rockTypeSprite;
        break;
      case AsteroidClass.SMALL_METAL:
      case AsteroidClass.MEDIUM_METAL:
      case AsteroidClass.LARGE_METAL:
      case AsteroidClass.VERYLARGE_METAL:
        this.entitySprite.spriteName = this.metalTypeSprite;
        break;
      case AsteroidClass.SMALL_RADIOACTIVE:
      case AsteroidClass.MEDIUM_RADIOACTIVE:
      case AsteroidClass.LARGE_RADIOACTIVE:
      case AsteroidClass.VERYLARGE_RADIOACTIVE:
        this.entitySprite.spriteName = this.radioactiveTypeSprite;
        break;
      case AsteroidClass.SMALL_ICE:
      case AsteroidClass.MEDIUM_ICE:
      case AsteroidClass.LARGE_ICE:
      case AsteroidClass.VERYLARGE_ICE:
        this.entitySprite.spriteName = this.iceTypeSprite;
        break;
      case AsteroidClass.SMALL_DARKMATTER:
      case AsteroidClass.MEDIUM_DARKMATTER:
      case AsteroidClass.LARGE_DARKMATTER:
      case AsteroidClass.VERYLARGE_DARKMATTER:
        this.entitySprite.spriteName = this.darkMatterTypeSprite;
        break;
    }
  }
}
