﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseDialogContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;

public class AuctionHouseDialogContext : DialogContextBase<AuctionHouseDialog>
{
  public AuctionHouseDialogContext(AuctionHouseDialog dlg)
    : base(dlg)
  {
  }

  public ItemRarity rarity { get; set; }

  public ItemType type { get; set; }

  public IAuctionFilter filter { get; set; }

  public override void Reset()
  {
  }
}
