﻿// Decompiled with JetBrains decompiler
// Type: TransferResourcesDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using Spellbook.Unity.SBUI;
using System;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;

public class TransferResourcesDialog : DialogBase
{
  public UILabel heading;
  public UILabel metalsCargo;
  public UILabel organicsCargo;
  public UILabel gasCargo;
  public UILabel radioactivesCargo;
  public UILabel darkmatterCargo;
  public SBUIButton metalsSingle;
  public SBUIButton metalsTen;
  public SBUIButton metalsAll;
  public SBUIButton organicsSingle;
  public SBUIButton organicsTen;
  public SBUIButton organicsAll;
  public SBUIButton gasSingle;
  public SBUIButton gasTen;
  public SBUIButton gasAll;
  public SBUIButton radioactivesSingle;
  public SBUIButton radioactivesTen;
  public SBUIButton radioactivesAll;
  public SBUIButton darkmatterSingle;
  public SBUIButton darkmatterTen;
  public SBUIButton darkmatterAll;
  public UILabel metalsToTransfer;
  public UILabel organicsToTransfer;
  public UILabel gasToTransfer;
  public UILabel radioactivesToTransfer;
  public UILabel darkmatterToTransfer;
  private Ship _target;
  private int _cargoMetals;
  private int _cargoOrganics;
  private int _cargoGas;
  private int _cargoRadioactives;
  private int _cargoDarkmatter;
  private int _transferMetals;
  private int _transferOrganics;
  private int _transferGas;
  private int _transferRadioactives;
  private int _transferDarkmatter;

  public Ship target
  {
    get
    {
      return this._target;
    }
    set
    {
      this._target = value;
      if (this._target == null)
        return;
      this.heading.text = string.Format("TRANSFER RESOURCES TO {0}", (object) this._target.Title);
    }
  }

  protected override bool ProcessArgs(object[] args)
  {
    if (args.Length != 1)
      throw new ArgumentException("Expected args length is 1.");
    this.target = (Ship) args[0];
    return true;
  }

  public void MoveSingleMetal()
  {
    --this._cargoMetals;
    ++this._transferMetals;
    this.RefreshVisuals();
  }

  public void MoveTenMetals()
  {
    if (10 <= this._cargoMetals)
    {
      this._cargoMetals -= 10;
      this._transferMetals += 10;
    }
    this.RefreshVisuals();
  }

  public void MoveAllMetals()
  {
    this._transferMetals = (int) TibProxy.gameState.MyShip.Metals;
    this._cargoMetals = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleOrganic()
  {
    --this._cargoOrganics;
    ++this._transferOrganics;
    this.RefreshVisuals();
  }

  public void MoveTenOrganics()
  {
    if (10 <= this._cargoOrganics)
    {
      this._cargoOrganics -= 10;
      this._transferOrganics += 10;
    }
    this.RefreshVisuals();
  }

  public void MoveAllOrganics()
  {
    this._transferOrganics = (int) TibProxy.gameState.MyShip.Organics;
    this._cargoOrganics = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleGas()
  {
    --this._cargoGas;
    ++this._transferGas;
    this.RefreshVisuals();
  }

  public void MoveTenGas()
  {
    if (10 <= this._cargoGas)
    {
      this._cargoGas -= 10;
      this._transferGas += 10;
    }
    this.RefreshVisuals();
  }

  public void MoveAllGas()
  {
    this._transferGas = (int) TibProxy.gameState.MyShip.Gas;
    this._cargoGas = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleRadioactive()
  {
    --this._cargoRadioactives;
    ++this._transferRadioactives;
    this.RefreshVisuals();
  }

  public void MoveTenRadioactives()
  {
    if (10 <= this._cargoRadioactives)
    {
      this._cargoRadioactives -= 10;
      this._transferRadioactives += 10;
    }
    this.RefreshVisuals();
  }

  public void MoveAllRadioactives()
  {
    this._transferRadioactives = (int) TibProxy.gameState.MyShip.Radioactives;
    this._cargoRadioactives = 0;
    this.RefreshVisuals();
  }

  public void MoveSingleDarkmatter()
  {
    --this._cargoDarkmatter;
    ++this._transferDarkmatter;
    this.RefreshVisuals();
  }

  public void MoveTenDarkmatter()
  {
    if (10 <= this._cargoDarkmatter)
    {
      this._cargoDarkmatter -= 10;
      this._transferDarkmatter += 10;
    }
    this.RefreshVisuals();
  }

  public void MoveAllDarkmatter()
  {
    this._transferDarkmatter = (int) TibProxy.gameState.MyShip.DarkMatter;
    this._cargoDarkmatter = 0;
    this.RefreshVisuals();
  }

  public void AcceptRequest()
  {
    TibProxy.gameState.DoResourcesTransfer(this._target, this._transferOrganics, this._transferGas, this._transferMetals, this._transferRadioactives, this._transferDarkmatter);
    this.Hide();
  }

  public void ClearRequest()
  {
    this._transferMetals = 0;
    this._transferOrganics = 0;
    this._transferGas = 0;
    this._transferRadioactives = 0;
    this._transferDarkmatter = 0;
    this.RefreshState();
    this.RefreshVisuals();
  }

  public void CancelnRequest()
  {
    this.Hide();
  }

  protected void RefreshVisuals()
  {
    this.metalsCargo.text = string.Format("{0}", (object) this._cargoMetals);
    this.organicsCargo.text = string.Format("{0}", (object) this._cargoOrganics);
    this.gasCargo.text = string.Format("{0}", (object) this._cargoGas);
    this.radioactivesCargo.text = string.Format("{0}", (object) this._cargoRadioactives);
    this.darkmatterCargo.text = string.Format("{0}", (object) this._cargoDarkmatter);
    this.metalsToTransfer.text = string.Format("{0}", (object) this._transferMetals);
    this.organicsToTransfer.text = string.Format("{0}", (object) this._transferOrganics);
    this.gasToTransfer.text = string.Format("{0}", (object) this._transferGas);
    this.radioactivesToTransfer.text = string.Format("{0}", (object) this._transferRadioactives);
    this.darkmatterToTransfer.text = string.Format("{0}", (object) this._transferDarkmatter);
    SBUIButton metalsSingle = this.metalsSingle;
    bool flag1 = this._cargoMetals > 0;
    this.metalsAll.isEnabled = flag1;
    int num1 = flag1 ? 1 : 0;
    metalsSingle.isEnabled = num1 != 0;
    this.metalsTen.isEnabled = this._cargoMetals >= 10;
    SBUIButton organicsSingle = this.organicsSingle;
    bool flag2 = this._cargoOrganics > 0;
    this.organicsAll.isEnabled = flag2;
    int num2 = flag2 ? 1 : 0;
    organicsSingle.isEnabled = num2 != 0;
    this.organicsTen.isEnabled = this._cargoOrganics >= 10;
    SBUIButton gasSingle = this.gasSingle;
    bool flag3 = this._cargoGas > 0;
    this.gasAll.isEnabled = flag3;
    int num3 = flag3 ? 1 : 0;
    gasSingle.isEnabled = num3 != 0;
    this.gasTen.isEnabled = this._cargoGas >= 10;
    SBUIButton radioactivesSingle = this.radioactivesSingle;
    bool flag4 = this._cargoRadioactives > 0;
    this.radioactivesAll.isEnabled = flag4;
    int num4 = flag4 ? 1 : 0;
    radioactivesSingle.isEnabled = num4 != 0;
    this.radioactivesTen.isEnabled = this._cargoRadioactives >= 10;
    SBUIButton darkmatterSingle = this.darkmatterSingle;
    bool flag5 = this._cargoDarkmatter > 0;
    this.darkmatterAll.isEnabled = flag5;
    int num5 = flag5 ? 1 : 0;
    darkmatterSingle.isEnabled = num5 != 0;
    this.darkmatterTen.isEnabled = this._cargoDarkmatter >= 10;
  }

  protected void ResetVisuals()
  {
    this.metalsCargo.text = string.Empty;
    this.organicsCargo.text = string.Empty;
    this.gasCargo.text = string.Empty;
    this.radioactivesCargo.text = string.Empty;
    this.darkmatterCargo.text = string.Empty;
    this.metalsToTransfer.text = string.Empty;
    this.organicsToTransfer.text = string.Empty;
    this.gasToTransfer.text = string.Empty;
    this.radioactivesToTransfer.text = string.Empty;
    this.darkmatterToTransfer.text = string.Empty;
    this.metalsSingle.isEnabled = false;
    this.metalsTen.isEnabled = false;
    this.metalsAll.isEnabled = false;
    this.organicsSingle.isEnabled = false;
    this.organicsTen.isEnabled = false;
    this.organicsAll.isEnabled = false;
    this.gasSingle.isEnabled = false;
    this.gasTen.isEnabled = false;
    this.gasAll.isEnabled = false;
    this.radioactivesSingle.isEnabled = false;
    this.radioactivesTen.isEnabled = false;
    this.radioactivesAll.isEnabled = false;
    this.darkmatterSingle.isEnabled = false;
    this.darkmatterTen.isEnabled = false;
    this.darkmatterAll.isEnabled = false;
  }

  protected void ResetState()
  {
    this.target = (Ship) null;
    this._transferMetals = 0;
    this._transferOrganics = 0;
    this._transferGas = 0;
    this._transferRadioactives = 0;
    this._transferDarkmatter = 0;
    this._cargoMetals = 0;
    this._cargoOrganics = 0;
    this._cargoGas = 0;
    this._cargoRadioactives = 0;
    this._cargoDarkmatter = 0;
  }

  private void ClampResources(ref int cargo, ref int transfer, int total)
  {
    if (cargo + transfer == total)
      return;
    if (transfer <= total)
    {
      cargo = total - transfer;
    }
    else
    {
      cargo = 0;
      transfer = total;
    }
  }

  protected void RefreshState()
  {
    this.ClampResources(ref this._cargoMetals, ref this._transferMetals, (int) TibProxy.gameState.MyShip.Metals);
    this.ClampResources(ref this._cargoOrganics, ref this._transferOrganics, (int) TibProxy.gameState.MyShip.Organics);
    this.ClampResources(ref this._cargoGas, ref this._transferGas, (int) TibProxy.gameState.MyShip.Gas);
    this.ClampResources(ref this._cargoRadioactives, ref this._transferRadioactives, (int) TibProxy.gameState.MyShip.Radioactives);
    this.ClampResources(ref this._cargoDarkmatter, ref this._transferDarkmatter, (int) TibProxy.gameState.MyShip.DarkMatter);
  }

  protected override void OnShow()
  {
    base.OnShow();
    this._transferMetals = 0;
    this._transferOrganics = 0;
    this._transferGas = 0;
    this._transferRadioactives = 0;
    this._transferDarkmatter = 0;
    this.RefreshState();
    this.RefreshVisuals();
  }

  protected override void OnHide()
  {
    this.ResetVisuals();
    this.ResetState();
  }

  private void Update()
  {
    this.RefreshState();
    this.RefreshVisuals();
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }
}
