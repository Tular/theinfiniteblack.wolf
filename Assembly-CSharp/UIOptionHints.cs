﻿// Decompiled with JetBrains decompiler
// Type: UIOptionHints
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIToggle))]
public class UIOptionHints : MonoBehaviour
{
  public UILabel info;
  private UIToggle mCheck;

  private void OnClick()
  {
    this.info.text = Localization.Get("Hints Info");
  }

  private void Awake()
  {
    this.mCheck = this.GetComponent<UIToggle>();
    EventDelegate.Add(this.mCheck.onChange, new EventDelegate.Callback(this.SaveState));
  }

  private void OnEnable()
  {
    this.mCheck.value = PlayerProfile.hints;
  }

  private void SaveState()
  {
    PlayerProfile.hints = UIToggle.current.value;
  }
}
