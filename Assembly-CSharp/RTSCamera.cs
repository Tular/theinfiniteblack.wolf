﻿// Decompiled with JetBrains decompiler
// Type: RTSCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RTSCamera : MonoBehaviour
{
  private Vector3 delta;

  private void OnEnable()
  {
    EasyTouch.On_Swipe += new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_Drag += new EasyTouch.DragHandler(this.On_Drag);
    EasyTouch.On_Twist += new EasyTouch.TwistHandler(this.On_Twist);
    EasyTouch.On_Pinch += new EasyTouch.PinchHandler(this.On_Pinch);
  }

  private void On_Twist(Gesture gesture)
  {
    this.transform.Rotate(Vector3.up * gesture.twistAngle);
  }

  private void OnDestroy()
  {
    EasyTouch.On_Swipe -= new EasyTouch.SwipeHandler(this.On_Swipe);
    EasyTouch.On_Drag -= new EasyTouch.DragHandler(this.On_Drag);
    EasyTouch.On_Twist -= new EasyTouch.TwistHandler(this.On_Twist);
  }

  private void On_Drag(Gesture gesture)
  {
    this.On_Swipe(gesture);
  }

  private void On_Swipe(Gesture gesture)
  {
    this.transform.Translate(Vector3.left * gesture.deltaPosition.x / (float) Screen.width);
    this.transform.Translate(Vector3.back * gesture.deltaPosition.y / (float) Screen.height);
  }

  private void On_Pinch(Gesture gesture)
  {
    Camera.main.fieldOfView += gesture.deltaPinch * Time.deltaTime;
  }
}
