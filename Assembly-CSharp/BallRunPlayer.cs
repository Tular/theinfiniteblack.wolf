﻿// Decompiled with JetBrains decompiler
// Type: BallRunPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BallRunPlayer : MonoBehaviour
{
  public Transform ballModel;
  private bool start;
  private Vector3 moveDirection;
  private CharacterController characterController;
  private Vector3 startPosition;
  private bool isJump;

  private void OnEnable()
  {
    EasyTouch.On_SwipeEnd += new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
  }

  private void OnDestroy()
  {
    EasyTouch.On_SwipeEnd -= new EasyTouch.SwipeEndHandler(this.On_SwipeEnd);
  }

  private void Start()
  {
    this.characterController = this.GetComponent<CharacterController>();
    this.startPosition = this.transform.position;
  }

  private void Update()
  {
    if (this.start)
    {
      this.moveDirection = this.transform.TransformDirection(Vector3.forward) * 10f * Time.deltaTime;
      this.moveDirection.y -= 9.81f * Time.deltaTime;
      if (this.isJump)
      {
        this.moveDirection.y = 8f;
        this.isJump = false;
      }
      int num = (int) this.characterController.Move(this.moveDirection);
      this.ballModel.Rotate(Vector3.right * 400f * Time.deltaTime);
    }
    if ((double) this.transform.position.y >= 0.5)
      return;
    this.start = false;
    this.transform.position = this.startPosition;
  }

  private void OnCollision()
  {
    Debug.Log((object) "ok");
  }

  private void On_SwipeEnd(Gesture gesture)
  {
    if (!this.start)
      return;
    switch (gesture.swipe)
    {
      case EasyTouch.SwipeDirection.Left:
      case EasyTouch.SwipeDirection.UpLeft:
      case EasyTouch.SwipeDirection.DownLeft:
        this.transform.Rotate(Vector3.up * -90f);
        break;
      case EasyTouch.SwipeDirection.Right:
      case EasyTouch.SwipeDirection.UpRight:
      case EasyTouch.SwipeDirection.DownRight:
        this.transform.Rotate(Vector3.up * 90f);
        break;
      case EasyTouch.SwipeDirection.Up:
        if (!this.characterController.isGrounded)
          break;
        this.isJump = true;
        break;
    }
  }

  public void StartGame()
  {
    this.start = true;
  }
}
