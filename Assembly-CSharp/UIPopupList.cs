﻿// Decompiled with JetBrains decompiler
// Type: UIPopupList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/Interaction/Popup List")]
public class UIPopupList : UIWidgetContainer
{
  public int fontSize = 16;
  public NGUIText.Alignment alignment = NGUIText.Alignment.Left;
  public List<string> items = new List<string>();
  public List<object> itemData = new List<object>();
  public Vector2 padding = (Vector2) new Vector3(4f, 4f);
  public Color textColor = Color.white;
  public Color backgroundColor = Color.white;
  public Color highlightColor = new Color(0.8823529f, 0.7843137f, 0.5882353f, 1f);
  public bool isAnimated = true;
  public bool separatePanel = true;
  public List<EventDelegate> onChange = new List<EventDelegate>();
  [HideInInspector]
  [SerializeField]
  protected List<UILabel> mLabelList = new List<UILabel>();
  [HideInInspector]
  [SerializeField]
  private string functionName = "OnSelectionChange";
  private const float animSpeed = 0.15f;
  public static UIPopupList current;
  protected static GameObject mChild;
  protected static float mFadeOutComplete;
  public UIAtlas atlas;
  public UIFont bitmapFont;
  public Font trueTypeFont;
  public FontStyle fontStyle;
  public string backgroundSprite;
  public string highlightSprite;
  public UnityEngine.Sprite background2DSprite;
  public UnityEngine.Sprite highlight2DSprite;
  public UIPopupList.Position position;
  public bool isLocalized;
  public int overlap;
  public UIPopupList.OpenOn openOn;
  [HideInInspector]
  [SerializeField]
  protected string mSelectedItem;
  [HideInInspector]
  [SerializeField]
  protected UIPanel mPanel;
  [SerializeField]
  [HideInInspector]
  protected UIBasicSprite mBackground;
  [HideInInspector]
  [SerializeField]
  protected UIBasicSprite mHighlight;
  [HideInInspector]
  [SerializeField]
  protected UILabel mHighlightedLabel;
  [HideInInspector]
  [SerializeField]
  protected float mBgBorder;
  [NonSerialized]
  protected GameObject mSelection;
  [NonSerialized]
  protected int mOpenFrame;
  [SerializeField]
  [HideInInspector]
  private GameObject eventReceiver;
  [HideInInspector]
  [SerializeField]
  private float textScale;
  [HideInInspector]
  [SerializeField]
  private UIFont font;
  [SerializeField]
  [HideInInspector]
  private UILabel textLabel;
  [NonSerialized]
  public Vector3 startingPosition;
  private UIPopupList.LegacyEvent mLegacyEvent;
  [NonSerialized]
  protected bool mExecuting;
  protected bool mUseDynamicFont;
  [NonSerialized]
  protected bool mStarted;
  protected bool mTweening;
  public GameObject source;

  public UnityEngine.Object ambigiousFont
  {
    get
    {
      if ((UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null)
        return (UnityEngine.Object) this.trueTypeFont;
      if ((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null)
        return (UnityEngine.Object) this.bitmapFont;
      return (UnityEngine.Object) this.font;
    }
    set
    {
      if (value is Font)
      {
        this.trueTypeFont = value as Font;
        this.bitmapFont = (UIFont) null;
        this.font = (UIFont) null;
      }
      else
      {
        if (!(value is UIFont))
          return;
        this.bitmapFont = value as UIFont;
        this.trueTypeFont = (Font) null;
        this.font = (UIFont) null;
      }
    }
  }

  [Obsolete("Use EventDelegate.Add(popup.onChange, YourCallback) instead, and UIPopupList.current.value to determine the state")]
  public UIPopupList.LegacyEvent onSelectionChange
  {
    get
    {
      return this.mLegacyEvent;
    }
    set
    {
      this.mLegacyEvent = value;
    }
  }

  public static bool isOpen
  {
    get
    {
      if (!((UnityEngine.Object) UIPopupList.current != (UnityEngine.Object) null))
        return false;
      if (!((UnityEngine.Object) UIPopupList.mChild != (UnityEngine.Object) null))
        return (double) UIPopupList.mFadeOutComplete > (double) Time.unscaledTime;
      return true;
    }
  }

  public virtual string value
  {
    get
    {
      return this.mSelectedItem;
    }
    set
    {
      this.Set(value, true);
    }
  }

  public virtual object data
  {
    get
    {
      int index = this.items.IndexOf(this.mSelectedItem);
      if (index > -1 && index < this.itemData.Count)
        return this.itemData[index];
      return (object) null;
    }
  }

  public bool isColliderEnabled
  {
    get
    {
      Collider component1 = this.GetComponent<Collider>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
        return component1.enabled;
      Collider2D component2 = this.GetComponent<Collider2D>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        return component2.enabled;
      return false;
    }
  }

  [Obsolete("Use 'value' instead")]
  public string selection
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  protected bool isValid
  {
    get
    {
      if (!((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null))
        return (UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null;
      return true;
    }
  }

  protected int activeFontSize
  {
    get
    {
      if ((UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null || (UnityEngine.Object) this.bitmapFont == (UnityEngine.Object) null)
        return this.fontSize;
      return this.bitmapFont.defaultSize;
    }
  }

  protected float activeFontScale
  {
    get
    {
      if ((UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null || (UnityEngine.Object) this.bitmapFont == (UnityEngine.Object) null)
        return 1f;
      return (float) this.fontSize / (float) this.bitmapFont.defaultSize;
    }
  }

  public void Set(string value, bool notify = true)
  {
    if (!(this.mSelectedItem != value))
      return;
    this.mSelectedItem = value;
    if (this.mSelectedItem == null)
      return;
    if (notify && this.mSelectedItem != null)
      this.TriggerCallbacks();
    this.mSelectedItem = (string) null;
  }

  public virtual void Clear()
  {
    this.items.Clear();
    this.itemData.Clear();
  }

  public virtual void AddItem(string text)
  {
    this.items.Add(text);
    this.itemData.Add((object) null);
  }

  public virtual void AddItem(string text, object data)
  {
    this.items.Add(text);
    this.itemData.Add(data);
  }

  public virtual void RemoveItem(string text)
  {
    int index = this.items.IndexOf(text);
    if (index == -1)
      return;
    this.items.RemoveAt(index);
    this.itemData.RemoveAt(index);
  }

  public virtual void RemoveItemByData(object data)
  {
    int index = this.itemData.IndexOf(data);
    if (index == -1)
      return;
    this.items.RemoveAt(index);
    this.itemData.RemoveAt(index);
  }

  protected void TriggerCallbacks()
  {
    if (this.mExecuting)
      return;
    this.mExecuting = true;
    UIPopupList current = UIPopupList.current;
    UIPopupList.current = this;
    if (this.mLegacyEvent != null)
      this.mLegacyEvent(this.mSelectedItem);
    if (EventDelegate.IsValid(this.onChange))
      EventDelegate.Execute(this.onChange);
    else if ((UnityEngine.Object) this.eventReceiver != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.functionName))
      this.eventReceiver.SendMessage(this.functionName, (object) this.mSelectedItem, SendMessageOptions.DontRequireReceiver);
    UIPopupList.current = current;
    this.mExecuting = false;
  }

  protected virtual void OnEnable()
  {
    if (EventDelegate.IsValid(this.onChange))
    {
      this.eventReceiver = (GameObject) null;
      this.functionName = (string) null;
    }
    if ((UnityEngine.Object) this.font != (UnityEngine.Object) null)
    {
      if (this.font.isDynamic)
      {
        this.trueTypeFont = this.font.dynamicFont;
        this.fontStyle = this.font.dynamicFontStyle;
        this.mUseDynamicFont = true;
      }
      else if ((UnityEngine.Object) this.bitmapFont == (UnityEngine.Object) null)
      {
        this.bitmapFont = this.font;
        this.mUseDynamicFont = false;
      }
      this.font = (UIFont) null;
    }
    if ((double) this.textScale != 0.0)
    {
      this.fontSize = !((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null) ? 16 : Mathf.RoundToInt((float) this.bitmapFont.defaultSize * this.textScale);
      this.textScale = 0.0f;
    }
    if (!((UnityEngine.Object) this.trueTypeFont == (UnityEngine.Object) null) || !((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null) || !this.bitmapFont.isDynamic)
      return;
    this.trueTypeFont = this.bitmapFont.dynamicFont;
    this.bitmapFont = (UIFont) null;
  }

  protected virtual void OnValidate()
  {
    Font trueTypeFont = this.trueTypeFont;
    UIFont bitmapFont = this.bitmapFont;
    this.bitmapFont = (UIFont) null;
    this.trueTypeFont = (Font) null;
    if ((UnityEngine.Object) trueTypeFont != (UnityEngine.Object) null && ((UnityEngine.Object) bitmapFont == (UnityEngine.Object) null || !this.mUseDynamicFont))
    {
      this.bitmapFont = (UIFont) null;
      this.trueTypeFont = trueTypeFont;
      this.mUseDynamicFont = true;
    }
    else if ((UnityEngine.Object) bitmapFont != (UnityEngine.Object) null)
    {
      if (bitmapFont.isDynamic)
      {
        this.trueTypeFont = bitmapFont.dynamicFont;
        this.fontStyle = bitmapFont.dynamicFontStyle;
        this.fontSize = bitmapFont.defaultSize;
        this.mUseDynamicFont = true;
      }
      else
      {
        this.bitmapFont = bitmapFont;
        this.mUseDynamicFont = false;
      }
    }
    else
    {
      this.trueTypeFont = trueTypeFont;
      this.mUseDynamicFont = true;
    }
  }

  public virtual void Start()
  {
    if (this.mStarted)
      return;
    this.mStarted = true;
    if (!((UnityEngine.Object) this.textLabel != (UnityEngine.Object) null))
      return;
    EventDelegate.Add(this.onChange, new EventDelegate.Callback(this.textLabel.SetCurrentSelection));
    this.textLabel = (UILabel) null;
  }

  protected virtual void OnLocalize()
  {
    if (!this.isLocalized)
      return;
    this.TriggerCallbacks();
  }

  protected virtual void Highlight(UILabel lbl, bool instant)
  {
    if (!((UnityEngine.Object) this.mHighlight != (UnityEngine.Object) null))
      return;
    this.mHighlightedLabel = lbl;
    Vector3 highlightPosition = this.GetHighlightPosition();
    if (!instant && this.isAnimated)
    {
      TweenPosition.Begin(this.mHighlight.gameObject, 0.1f, highlightPosition).method = UITweener.Method.EaseOut;
      if (this.mTweening)
        return;
      this.mTweening = true;
      this.StartCoroutine("UpdateTweenPosition");
    }
    else
      this.mHighlight.cachedTransform.localPosition = highlightPosition;
  }

  protected virtual Vector3 GetHighlightPosition()
  {
    if ((UnityEngine.Object) this.mHighlightedLabel == (UnityEngine.Object) null || (UnityEngine.Object) this.mHighlight == (UnityEngine.Object) null)
      return Vector3.zero;
    Vector4 border = this.mHighlight.border;
    float num = !((UnityEngine.Object) this.atlas != (UnityEngine.Object) null) ? 1f : this.atlas.pixelSize;
    return this.mHighlightedLabel.cachedTransform.localPosition + new Vector3(-(border.x * num), border.w * num, 1f);
  }

  [DebuggerHidden]
  protected virtual IEnumerator UpdateTweenPosition()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UIPopupList.\u003CUpdateTweenPosition\u003Ec__Iterator3()
    {
      \u003C\u003Ef__this = this
    };
  }

  protected virtual void OnItemHover(GameObject go, bool isOver)
  {
    if (!isOver)
      return;
    this.Highlight(go.GetComponent<UILabel>(), false);
  }

  protected virtual void OnItemPress(GameObject go, bool isPressed)
  {
    if (!isPressed)
      return;
    this.Select(go.GetComponent<UILabel>(), true);
    this.value = go.GetComponent<UIEventListener>().parameter as string;
    UIPlaySound[] components = this.GetComponents<UIPlaySound>();
    int index = 0;
    for (int length = components.Length; index < length; ++index)
    {
      UIPlaySound uiPlaySound = components[index];
      if (uiPlaySound.trigger == UIPlaySound.Trigger.OnClick)
        NGUITools.PlaySound(uiPlaySound.audioClip, uiPlaySound.volume, 1f);
    }
    this.CloseSelf();
  }

  private void Select(UILabel lbl, bool instant)
  {
    this.Highlight(lbl, instant);
  }

  protected virtual void OnNavigate(KeyCode key)
  {
    if (!this.enabled || !((UnityEngine.Object) UIPopupList.current == (UnityEngine.Object) this))
      return;
    int num1 = this.mLabelList.IndexOf(this.mHighlightedLabel);
    if (num1 == -1)
      num1 = 0;
    int num2;
    if (key == KeyCode.UpArrow)
    {
      if (num1 <= 0)
        return;
      this.Select(this.mLabelList[num2 = num1 - 1], false);
    }
    else
    {
      if (key != KeyCode.DownArrow || num1 + 1 >= this.mLabelList.Count)
        return;
      this.Select(this.mLabelList[num2 = num1 + 1], false);
    }
  }

  protected virtual void OnKey(KeyCode key)
  {
    if (!this.enabled || !((UnityEngine.Object) UIPopupList.current == (UnityEngine.Object) this) || key != UICamera.current.cancelKey0 && key != UICamera.current.cancelKey1)
      return;
    this.OnSelect(false);
  }

  protected virtual void OnDisable()
  {
    this.CloseSelf();
  }

  protected virtual void OnSelect(bool isSelected)
  {
    if (isSelected)
      return;
    this.CloseSelf();
  }

  public static void Close()
  {
    if (!((UnityEngine.Object) UIPopupList.current != (UnityEngine.Object) null))
      return;
    UIPopupList.current.CloseSelf();
    UIPopupList.current = (UIPopupList) null;
  }

  public virtual void CloseSelf()
  {
    if (!((UnityEngine.Object) UIPopupList.mChild != (UnityEngine.Object) null) || !((UnityEngine.Object) UIPopupList.current == (UnityEngine.Object) this))
      return;
    this.StopCoroutine("CloseIfUnselected");
    this.mSelection = (GameObject) null;
    this.mLabelList.Clear();
    if (this.isAnimated)
    {
      UIWidget[] componentsInChildren1 = UIPopupList.mChild.GetComponentsInChildren<UIWidget>();
      int index1 = 0;
      for (int length = componentsInChildren1.Length; index1 < length; ++index1)
      {
        UIWidget uiWidget = componentsInChildren1[index1];
        Color color = uiWidget.color;
        color.a = 0.0f;
        TweenColor.Begin(uiWidget.gameObject, 0.15f, color).method = UITweener.Method.EaseOut;
      }
      Collider[] componentsInChildren2 = UIPopupList.mChild.GetComponentsInChildren<Collider>();
      int index2 = 0;
      for (int length = componentsInChildren2.Length; index2 < length; ++index2)
        componentsInChildren2[index2].enabled = false;
      UnityEngine.Object.Destroy((UnityEngine.Object) UIPopupList.mChild, 0.15f);
      UIPopupList.mFadeOutComplete = Time.unscaledTime + Mathf.Max(0.1f, 0.15f);
    }
    else
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) UIPopupList.mChild);
      UIPopupList.mFadeOutComplete = Time.unscaledTime + 0.1f;
    }
    this.mBackground = (UIBasicSprite) null;
    this.mHighlight = (UIBasicSprite) null;
    UIPopupList.mChild = (GameObject) null;
    UIPopupList.current = (UIPopupList) null;
  }

  protected virtual void AnimateColor(UIWidget widget)
  {
    Color color = widget.color;
    widget.color = new Color(color.r, color.g, color.b, 0.0f);
    TweenColor.Begin(widget.gameObject, 0.15f, color).method = UITweener.Method.EaseOut;
  }

  protected virtual void AnimatePosition(UIWidget widget, bool placeAbove, float bottom)
  {
    Vector3 localPosition = widget.cachedTransform.localPosition;
    Vector3 vector3 = !placeAbove ? new Vector3(localPosition.x, 0.0f, localPosition.z) : new Vector3(localPosition.x, bottom, localPosition.z);
    widget.cachedTransform.localPosition = vector3;
    TweenPosition.Begin(widget.gameObject, 0.15f, localPosition).method = UITweener.Method.EaseOut;
  }

  protected virtual void AnimateScale(UIWidget widget, bool placeAbove, float bottom)
  {
    GameObject gameObject = widget.gameObject;
    Transform cachedTransform = widget.cachedTransform;
    float num = (float) ((double) this.activeFontSize * (double) this.activeFontScale + (double) this.mBgBorder * 2.0);
    cachedTransform.localScale = new Vector3(1f, num / (float) widget.height, 1f);
    TweenScale.Begin(gameObject, 0.15f, Vector3.one).method = UITweener.Method.EaseOut;
    if (!placeAbove)
      return;
    Vector3 localPosition = cachedTransform.localPosition;
    cachedTransform.localPosition = new Vector3(localPosition.x, localPosition.y - (float) widget.height + num, localPosition.z);
    TweenPosition.Begin(gameObject, 0.15f, localPosition).method = UITweener.Method.EaseOut;
  }

  protected void Animate(UIWidget widget, bool placeAbove, float bottom)
  {
    this.AnimateColor(widget);
    this.AnimatePosition(widget, placeAbove, bottom);
  }

  protected virtual void OnClick()
  {
    if (this.mOpenFrame == Time.frameCount)
      return;
    if ((UnityEngine.Object) UIPopupList.mChild == (UnityEngine.Object) null)
    {
      if (this.openOn == UIPopupList.OpenOn.DoubleClick || this.openOn == UIPopupList.OpenOn.Manual || this.openOn == UIPopupList.OpenOn.RightClick && UICamera.currentTouchID != -2)
        return;
      this.Show();
    }
    else
    {
      if (!((UnityEngine.Object) this.mHighlightedLabel != (UnityEngine.Object) null))
        return;
      this.OnItemPress(this.mHighlightedLabel.gameObject, true);
    }
  }

  protected virtual void OnDoubleClick()
  {
    if (this.openOn != UIPopupList.OpenOn.DoubleClick)
      return;
    this.Show();
  }

  [DebuggerHidden]
  private IEnumerator CloseIfUnselected()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UIPopupList.\u003CCloseIfUnselected\u003Ec__Iterator4()
    {
      \u003C\u003Ef__this = this
    };
  }

  public virtual void Show()
  {
    if (this.enabled && NGUITools.GetActive(this.gameObject) && ((UnityEngine.Object) UIPopupList.mChild == (UnityEngine.Object) null && this.isValid) && this.items.Count > 0)
    {
      this.mLabelList.Clear();
      this.StopCoroutine("CloseIfUnselected");
      UICamera.selectedObject = UICamera.hoveredObject ?? this.gameObject;
      this.mSelection = UICamera.selectedObject;
      this.source = UICamera.selectedObject;
      if ((UnityEngine.Object) this.source == (UnityEngine.Object) null)
      {
        UnityEngine.Debug.LogError((object) "Popup list needs a source object...");
      }
      else
      {
        this.mOpenFrame = Time.frameCount;
        if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
        {
          this.mPanel = UIPanel.Find(this.transform);
          if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
            return;
        }
        UIPopupList.mChild = new GameObject("Drop-down List");
        UIPopupList.mChild.layer = this.gameObject.layer;
        if (this.separatePanel)
        {
          if ((UnityEngine.Object) this.GetComponent<Collider>() != (UnityEngine.Object) null)
            UIPopupList.mChild.AddComponent<Rigidbody>().isKinematic = true;
          else if ((UnityEngine.Object) this.GetComponent<Collider2D>() != (UnityEngine.Object) null)
            UIPopupList.mChild.AddComponent<Rigidbody2D>().isKinematic = true;
          UIPopupList.mChild.AddComponent<UIPanel>().depth = 1000000;
        }
        UIPopupList.current = this;
        Transform transform = UIPopupList.mChild.transform;
        transform.parent = this.mPanel.cachedTransform;
        Vector3 position1;
        Vector3 position2;
        if (this.openOn == UIPopupList.OpenOn.Manual && (UnityEngine.Object) this.mSelection != (UnityEngine.Object) this.gameObject)
        {
          this.startingPosition = (Vector3) UICamera.lastEventPosition;
          position1 = this.mPanel.cachedTransform.InverseTransformPoint(this.mPanel.anchorCamera.ScreenToWorldPoint(this.startingPosition));
          position2 = position1;
          transform.localPosition = position1;
          this.startingPosition = transform.position;
        }
        else
        {
          Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.mPanel.cachedTransform, this.transform, false, false);
          position1 = relativeWidgetBounds.min;
          position2 = relativeWidgetBounds.max;
          transform.localPosition = position1;
          this.startingPosition = transform.position;
        }
        this.StartCoroutine("CloseIfUnselected");
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
        int depth = !this.separatePanel ? NGUITools.CalculateNextDepth(this.mPanel.gameObject) : 0;
        if ((UnityEngine.Object) this.background2DSprite != (UnityEngine.Object) null)
        {
          UI2DSprite ui2Dsprite = UIPopupList.mChild.AddWidget<UI2DSprite>(depth);
          ui2Dsprite.sprite2D = this.background2DSprite;
          this.mBackground = (UIBasicSprite) ui2Dsprite;
        }
        else
        {
          if (!((UnityEngine.Object) this.atlas != (UnityEngine.Object) null))
            return;
          this.mBackground = (UIBasicSprite) UIPopupList.mChild.AddSprite(this.atlas, this.backgroundSprite, depth);
        }
        bool placeAbove = this.position == UIPopupList.Position.Above;
        if (this.position == UIPopupList.Position.Auto)
        {
          UICamera cameraForLayer = UICamera.FindCameraForLayer(this.mSelection.layer);
          if ((UnityEngine.Object) cameraForLayer != (UnityEngine.Object) null)
            placeAbove = (double) cameraForLayer.cachedCamera.WorldToViewportPoint(this.startingPosition).y < 0.5;
        }
        this.mBackground.pivot = UIWidget.Pivot.TopLeft;
        this.mBackground.color = this.backgroundColor;
        Vector4 border = this.mBackground.border;
        this.mBgBorder = border.y;
        this.mBackground.cachedTransform.localPosition = new Vector3(0.0f, !placeAbove ? (float) this.overlap : border.y * 2f - (float) this.overlap, 0.0f);
        int num1;
        if ((UnityEngine.Object) this.highlight2DSprite != (UnityEngine.Object) null)
        {
          UI2DSprite ui2Dsprite = UIPopupList.mChild.AddWidget<UI2DSprite>(num1 = depth + 1);
          ui2Dsprite.sprite2D = this.highlight2DSprite;
          this.mHighlight = (UIBasicSprite) ui2Dsprite;
        }
        else
        {
          if (!((UnityEngine.Object) this.atlas != (UnityEngine.Object) null))
            return;
          this.mHighlight = (UIBasicSprite) UIPopupList.mChild.AddSprite(this.atlas, this.highlightSprite, num1 = depth + 1);
        }
        float num2 = 0.0f;
        float num3 = 0.0f;
        if (this.mHighlight.hasBorder)
        {
          num2 = this.mHighlight.border.w;
          num3 = this.mHighlight.border.x;
        }
        this.mHighlight.pivot = UIWidget.Pivot.TopLeft;
        this.mHighlight.color = this.highlightColor;
        float num4 = (float) this.activeFontSize * this.activeFontScale;
        float num5 = num4 + this.padding.y;
        float a = 0.0f;
        float y = !placeAbove ? -this.padding.y - border.y + (float) this.overlap : border.y - this.padding.y - (float) this.overlap;
        float f1 = border.y * 2f + this.padding.y;
        List<UILabel> uiLabelList = new List<UILabel>();
        if (!this.items.Contains(this.mSelectedItem))
          this.mSelectedItem = (string) null;
        int index1 = 0;
        for (int count = this.items.Count; index1 < count; ++index1)
        {
          string key = this.items[index1];
          UILabel lbl = UIPopupList.mChild.AddWidget<UILabel>(this.mBackground.depth + 2);
          lbl.name = index1.ToString();
          lbl.pivot = UIWidget.Pivot.TopLeft;
          lbl.bitmapFont = this.bitmapFont;
          lbl.trueTypeFont = this.trueTypeFont;
          lbl.fontSize = this.fontSize;
          lbl.fontStyle = this.fontStyle;
          lbl.text = !this.isLocalized ? key : Localization.Get(key);
          lbl.color = this.textColor;
          lbl.cachedTransform.localPosition = new Vector3(border.x + this.padding.x - lbl.pivotOffset.x, y, -1f);
          lbl.overflowMethod = UILabel.Overflow.ResizeFreely;
          lbl.alignment = this.alignment;
          uiLabelList.Add(lbl);
          f1 += num5;
          y -= num5;
          a = Mathf.Max(a, lbl.printedSize.x);
          UIEventListener uiEventListener = UIEventListener.Get(lbl.gameObject);
          uiEventListener.onHover = new UIEventListener.BoolDelegate(this.OnItemHover);
          uiEventListener.onPress = new UIEventListener.BoolDelegate(this.OnItemPress);
          uiEventListener.parameter = (object) key;
          if (this.mSelectedItem == key || index1 == 0 && string.IsNullOrEmpty(this.mSelectedItem))
            this.Highlight(lbl, true);
          this.mLabelList.Add(lbl);
        }
        float f2 = Mathf.Max(a, (float) ((double) position2.x - (double) position1.x - ((double) border.x + (double) this.padding.x) * 2.0));
        float x = f2;
        Vector3 vector3_1 = new Vector3(x * 0.5f, (float) (-(double) num4 * 0.5), 0.0f);
        Vector3 vector3_2 = new Vector3(x, num4 + this.padding.y, 1f);
        int index2 = 0;
        for (int count = uiLabelList.Count; index2 < count; ++index2)
        {
          UILabel uiLabel = uiLabelList[index2];
          NGUITools.AddWidgetCollider(uiLabel.gameObject);
          uiLabel.autoResizeBoxCollider = false;
          BoxCollider component1 = uiLabel.GetComponent<BoxCollider>();
          if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
          {
            vector3_1.z = component1.center.z;
            component1.center = vector3_1;
            component1.size = vector3_2;
          }
          else
          {
            BoxCollider2D component2 = uiLabel.GetComponent<BoxCollider2D>();
            component2.offset = (Vector2) vector3_1;
            component2.size = (Vector2) vector3_2;
          }
        }
        int num6 = Mathf.RoundToInt(f2);
        float f3 = f2 + (float) (((double) border.x + (double) this.padding.x) * 2.0);
        float num7 = y - border.y;
        this.mBackground.width = Mathf.RoundToInt(f3);
        this.mBackground.height = Mathf.RoundToInt(f1);
        int index3 = 0;
        for (int count = uiLabelList.Count; index3 < count; ++index3)
        {
          UILabel uiLabel = uiLabelList[index3];
          uiLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
          uiLabel.width = num6;
        }
        float num8 = !((UnityEngine.Object) this.atlas != (UnityEngine.Object) null) ? 2f : 2f * this.atlas.pixelSize;
        float f4 = (float) ((double) f3 - ((double) border.x + (double) this.padding.x) * 2.0 + (double) num3 * (double) num8);
        float f5 = num4 + num2 * num8;
        this.mHighlight.width = Mathf.RoundToInt(f4);
        this.mHighlight.height = Mathf.RoundToInt(f5);
        if (this.isAnimated)
        {
          this.AnimateColor((UIWidget) this.mBackground);
          if ((double) Time.timeScale == 0.0 || (double) Time.timeScale >= 0.100000001490116)
          {
            float bottom = num7 + num4;
            this.Animate((UIWidget) this.mHighlight, placeAbove, bottom);
            int index4 = 0;
            for (int count = uiLabelList.Count; index4 < count; ++index4)
              this.Animate((UIWidget) uiLabelList[index4], placeAbove, bottom);
            this.AnimateScale((UIWidget) this.mBackground, placeAbove, bottom);
          }
        }
        if (placeAbove)
        {
          position1.y = position2.y - border.y;
          position2.y = position1.y + (float) this.mBackground.height;
          position2.x = position1.x + (float) this.mBackground.width;
          transform.localPosition = new Vector3(position1.x, position2.y - border.y, position1.z);
        }
        else
        {
          position2.y = position1.y + border.y;
          position1.y = position2.y - (float) this.mBackground.height;
          position2.x = position1.x + (float) this.mBackground.width;
        }
        Transform parent = this.mPanel.cachedTransform.parent;
        if ((UnityEngine.Object) parent != (UnityEngine.Object) null)
        {
          Vector3 position3 = this.mPanel.cachedTransform.TransformPoint(position1);
          position2 = this.mPanel.cachedTransform.TransformPoint(position2);
          position1 = parent.InverseTransformPoint(position3);
          position2 = parent.InverseTransformPoint(position2);
        }
        Vector3 vector3_3 = !this.mPanel.hasClipping ? this.mPanel.CalculateConstrainOffset((Vector2) position1, (Vector2) position2) : Vector3.zero;
        Vector3 vector3_4 = transform.localPosition + vector3_3;
        vector3_4.x = Mathf.Round(vector3_4.x);
        vector3_4.y = Mathf.Round(vector3_4.y);
        transform.localPosition = vector3_4;
      }
    }
    else
      this.OnSelect(false);
  }

  public enum Position
  {
    Auto,
    Above,
    Below,
  }

  public enum OpenOn
  {
    ClickOrTap,
    RightClick,
    DoubleClick,
    Manual,
  }

  public delegate void LegacyEvent(string val);
}
