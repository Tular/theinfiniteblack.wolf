﻿// Decompiled with JetBrains decompiler
// Type: ETWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ETWindow : MonoBehaviour
{
  private bool drag;

  private void OnEnable()
  {
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.On_TouchStart);
  }

  private void OnDestroy()
  {
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.On_TouchStart);
  }

  private void On_TouchStart(Gesture gesture)
  {
    this.drag = false;
    if (!gesture.isOverGui || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform))
      return;
    this.transform.SetAsLastSibling();
    this.drag = true;
  }

  private void On_TouchDown(Gesture gesture)
  {
    if (!gesture.isOverGui || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform) || !this.drag)
      return;
    this.transform.position += (Vector3) gesture.deltaPosition;
  }
}
