﻿// Decompiled with JetBrains decompiler
// Type: DamageFlash
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

[RequireComponent(typeof (UIPanel))]
[RequireComponent(typeof (TweenAlpha))]
public class DamageFlash : MonoBehaviour
{
  public int cornerGroupId;
  public int backgroundGroupId;
  public float backgroundAlpha;
  public Color miss;
  public Color graze;
  public Color hit;
  public Color critical;
  public Color splash;
  public Color repair;
  public Color stun;
  public Color collide;
  public Color deflect;
  public Color ram;
  public Color grapple;
  public Color degrapple;
  public bool test;
  private UIPanel _panel;
  private UISpriteColorGroupController _colorController;
  private bool _eventHandlersRegistered;
  private Color _cornerColor;
  private Color _backgroundColor;
  private bool _doFlash;
  private TweenAlpha _tweener;

  private Color flashColor
  {
    set
    {
      if (Mathf.Approximately(value.a, 0.0f))
      {
        this._doFlash = false;
      }
      else
      {
        this._doFlash = true;
        if (value == this._cornerColor)
          return;
        this._cornerColor = value;
        this._backgroundColor = value;
        this._backgroundColor.a = this.backgroundAlpha;
        this._colorController.SetColor(this.cornerGroupId, this._cornerColor);
        this._colorController.SetColor(this.backgroundGroupId, this._backgroundColor);
      }
    }
  }

  private void RegisterEventHandlers()
  {
    TibProxy.Instance.onCombatFlashEvent -= new EventHandler<CombatFlashEventArgs>(this.TibProxy_onCombatFlashEvent);
    TibProxy.Instance.onCombatFlashEvent += new EventHandler<CombatFlashEventArgs>(this.TibProxy_onCombatFlashEvent);
  }

  private void UnregisterEventHandlers()
  {
    TibProxy.Instance.onCombatFlashEvent -= new EventHandler<CombatFlashEventArgs>(this.TibProxy_onCombatFlashEvent);
  }

  private void TibProxy_onCombatFlashEvent(object sender, CombatFlashEventArgs e)
  {
    switch (e.AttackType)
    {
      case AttackEventType.MISS:
        this.flashColor = this.miss;
        break;
      case AttackEventType.GRAZE:
        this.flashColor = this.graze;
        break;
      case AttackEventType.HIT:
        this.flashColor = this.hit;
        break;
      case AttackEventType.CRITICAL:
        this.flashColor = this.critical;
        break;
      case AttackEventType.SPLASH:
        this.flashColor = this.splash;
        break;
      case AttackEventType.REPAIR:
        this.flashColor = this.repair;
        break;
      case AttackEventType.STUN:
        this.flashColor = this.stun;
        break;
      case AttackEventType.COLLIDE:
        this.flashColor = this.collide;
        break;
      case AttackEventType.DEFLECT:
        this.flashColor = this.deflect;
        break;
      case AttackEventType.RAM:
        this.flashColor = this.ram;
        break;
      case AttackEventType.GRAPPLE:
        this.flashColor = this.grapple;
        break;
      case AttackEventType.DEGRAPPLE:
        this.flashColor = this.degrapple;
        break;
      default:
        this._doFlash = false;
        break;
    }
    if (!this._doFlash)
      return;
    this._tweener.ResetToBeginning();
    this._tweener.PlayForward();
    this._doFlash = false;
  }

  private void Awake()
  {
    this._panel = this.GetComponent<UIPanel>();
    this._tweener = this.GetComponent<TweenAlpha>();
    this._tweener.enabled = false;
    this._colorController = this.gameObject.AddComponent<UISpriteColorGroupController>();
  }

  private void Start()
  {
    this._panel.alpha = 0.0f;
  }

  private void OnEnable()
  {
    this.RegisterEventHandlers();
  }

  private void OnDisable()
  {
    this.UnregisterEventHandlers();
  }

  private void Update()
  {
    if (!this.test)
      return;
    this._tweener.ResetToBeginning();
    this._tweener.PlayForward();
    this.test = false;
  }
}
