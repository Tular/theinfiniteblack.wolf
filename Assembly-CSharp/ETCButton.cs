﻿// Decompiled with JetBrains decompiler
// Type: ETCButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class ETCButton : ETCBase, IPointerUpHandler, IPointerEnterHandler, IEventSystemHandler, IPointerDownHandler, IPointerExitHandler
{
  [SerializeField]
  public ETCButton.OnDownHandler onDown;
  [SerializeField]
  public ETCButton.OnPressedHandler onPressed;
  [SerializeField]
  public ETCButton.OnPressedValueandler onPressedValue;
  [SerializeField]
  public ETCButton.OnUPHandler onUp;
  public ETCAxis axis;
  public UnityEngine.Sprite normalSprite;
  public Color normalColor;
  public UnityEngine.Sprite pressedSprite;
  public Color pressedColor;
  private Image cachedImage;
  private bool isOnPress;
  private GameObject previousDargObject;
  private bool isOnTouch;

  public ETCButton()
  {
    this.axis = new ETCAxis("Button");
    this._visible = true;
    this._activated = true;
    this.isOnTouch = false;
    this.enableKeySimulation = true;
    this.enableKeySimulation = false;
    this.axis.positivekey = KeyCode.Space;
    this.showPSInspector = true;
    this.showSpriteInspector = false;
    this.showBehaviourInspector = false;
    this.showEventInspector = false;
  }

  protected override void Awake()
  {
    base.Awake();
    this.cachedImage = this.GetComponent<Image>();
  }

  private void Start()
  {
    this.isOnPress = false;
  }

  protected override void UpdateControlState()
  {
    this.UpdateButton();
  }

  public void OnPointerEnter(PointerEventData eventData)
  {
    if (!this.isSwipeIn || this.isOnTouch)
      return;
    if ((UnityEngine.Object) eventData.pointerDrag != (UnityEngine.Object) null && (bool) ((UnityEngine.Object) eventData.pointerDrag.GetComponent<ETCBase>()) && (UnityEngine.Object) eventData.pointerDrag != (UnityEngine.Object) this.gameObject)
      this.previousDargObject = eventData.pointerDrag;
    eventData.pointerDrag = this.gameObject;
    eventData.pointerPress = this.gameObject;
    this.OnPointerDown(eventData);
  }

  public void OnPointerDown(PointerEventData eventData)
  {
    if (!this._activated || this.isOnTouch)
      return;
    this.pointId = eventData.pointerId;
    this.axis.ResetAxis();
    this.axis.axisState = ETCAxis.AxisState.Down;
    this.isOnPress = false;
    this.isOnTouch = true;
    this.onDown.Invoke();
    this.ApllyState();
  }

  public void OnPointerUp(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId)
      return;
    this.isOnPress = false;
    this.isOnTouch = false;
    this.axis.axisState = ETCAxis.AxisState.Up;
    this.axis.axisValue = 0.0f;
    this.onUp.Invoke();
    this.ApllyState();
    if (!(bool) ((UnityEngine.Object) this.previousDargObject))
      return;
    ExecuteEvents.Execute<IPointerUpHandler>(this.previousDargObject, (BaseEventData) eventData, ExecuteEvents.pointerUpHandler);
    this.previousDargObject = (GameObject) null;
  }

  public void OnPointerExit(PointerEventData eventData)
  {
    if (this.pointId != eventData.pointerId || this.axis.axisState != ETCAxis.AxisState.Press || this.isSwipeOut)
      return;
    this.OnPointerUp(eventData);
  }

  private void UpdateButton()
  {
    if (this.axis.axisState == ETCAxis.AxisState.Down)
    {
      this.isOnPress = true;
      this.axis.axisState = ETCAxis.AxisState.Press;
    }
    if (this.isOnPress)
    {
      this.axis.UpdateButton();
      this.onPressed.Invoke();
      this.onPressedValue.Invoke(this.axis.axisValue);
    }
    if (this.axis.axisState == ETCAxis.AxisState.Up)
    {
      this.isOnPress = false;
      this.axis.axisState = ETCAxis.AxisState.None;
    }
    if (!this.enableKeySimulation || !this._activated || (!this._visible || this.isOnTouch))
      return;
    if (Input.GetKey(this.axis.positivekey) && this.axis.axisState == ETCAxis.AxisState.None)
      this.axis.axisState = ETCAxis.AxisState.Down;
    if (Input.GetKey(this.axis.positivekey) || this.axis.axisState != ETCAxis.AxisState.Press)
      return;
    this.axis.axisState = ETCAxis.AxisState.Up;
    this.onUp.Invoke();
  }

  protected override void SetVisible()
  {
    this.GetComponent<Image>().enabled = this._visible;
  }

  private void ApllyState()
  {
    if (!(bool) ((UnityEngine.Object) this.cachedImage))
      return;
    switch (this.axis.axisState)
    {
      case ETCAxis.AxisState.Down:
      case ETCAxis.AxisState.Press:
        this.cachedImage.sprite = this.pressedSprite;
        this.cachedImage.color = this.pressedColor;
        break;
      default:
        this.cachedImage.sprite = this.normalSprite;
        this.cachedImage.color = this.normalColor;
        break;
    }
  }

  protected override void SetActivated()
  {
    if (this._activated)
      return;
    this.isOnPress = false;
    this.isOnTouch = false;
    this.axis.axisState = ETCAxis.AxisState.None;
    this.axis.axisValue = 0.0f;
    this.ApllyState();
  }

  [Serializable]
  public class OnDownHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressedHandler : UnityEvent
  {
  }

  [Serializable]
  public class OnPressedValueandler : UnityEvent<float>
  {
  }

  [Serializable]
  public class OnUPHandler : UnityEvent
  {
  }
}
