﻿// Decompiled with JetBrains decompiler
// Type: KeyboardMapController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Entities;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.Map;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class KeyboardMapController : PreInstantiatedSingleton<KeyboardMapController>
{
  public float mapZoomDelta;
  private bool _isPressedNorth;
  private bool _isPressedNortheast;
  private bool _isPressedEast;
  private bool _isPressedSoutheast;
  private bool _isPressedSouth;
  private bool _isPressedSouthwest;
  private bool _isPressedWest;
  private bool _isPressedNorthwest;
  private bool _isPressedMapZoomIn;
  private bool _isPressedMapZoomOut;
  private bool _isEnterPressed;
  private bool _isPressedJump;
  private bool _overrideMapZoomAxis;
  private Camera _mapCamera;
  private bool _isPressedBlackDollarRepair;
  private bool _isPressedMetalRepair;
  private bool _isPressedStarportRepair;
  private bool _isPressedEarthJump;
  private bool _isPressedCorpJump;
  private bool _isPressedRiftJump;
  private bool _isPressedUniverseQuickView;
  private bool _isPressedBlackDollarMenu;

  public bool isEnabled
  {
    get
    {
      return this.enabled;
    }
    set
    {
      this.enabled = value;
    }
  }

  private bool isPressedNorth
  {
    set
    {
      if (value == this._isPressedNorth)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.North);
      this._isPressedNorth = value;
    }
  }

  private bool isPressedNortheast
  {
    set
    {
      if (value == this._isPressedNortheast)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.Northeast);
      this._isPressedNortheast = value;
    }
  }

  private bool isPressedEast
  {
    set
    {
      if (value == this._isPressedEast)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.East);
      this._isPressedEast = value;
    }
  }

  private bool isPressedSoutheast
  {
    set
    {
      if (value == this._isPressedSoutheast)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.Southeast);
      this._isPressedSoutheast = value;
    }
  }

  private bool isPressedSouth
  {
    set
    {
      if (value == this._isPressedSouth)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.South);
      this._isPressedSouth = value;
    }
  }

  private bool isPressedSouthwest
  {
    set
    {
      if (value == this._isPressedSouthwest)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.Southwest);
      this._isPressedSouthwest = value;
    }
  }

  public bool isPressedWest
  {
    set
    {
      if (value == this._isPressedWest)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.West);
      this._isPressedWest = value;
    }
  }

  private bool isPressedNorthwest
  {
    set
    {
      if (value == this._isPressedNorthwest)
        return;
      if (!value)
        KeyboardMapController.DoJump(KeyboardMapController.JumpDirection.Northwest);
      this._isPressedNorthwest = value;
    }
  }

  private bool isPressedMapZoomIn
  {
    set
    {
      if (this._isPressedMapZoomIn && !this._isPressedMapZoomOut && !value)
        PreInstantiatedSingleton<SectorMap>.Instance.controller.ZoomEnd();
      this._isPressedMapZoomIn = value;
      if (!this._isPressedMapZoomIn)
        return;
      PreInstantiatedSingleton<SectorMap>.Instance.controller.ZoomIn(this.mapZoomDelta);
    }
  }

  private bool isPressedMapZoomOut
  {
    set
    {
      if (this._isPressedMapZoomOut && !this._isPressedMapZoomIn && !value)
        PreInstantiatedSingleton<SectorMap>.Instance.controller.ZoomEnd();
      this._isPressedMapZoomOut = value;
      if (!this._isPressedMapZoomOut)
        return;
      PreInstantiatedSingleton<SectorMap>.Instance.controller.ZoomOut(this.mapZoomDelta);
    }
  }

  private bool isEnterPressed
  {
    set
    {
      if (this._isEnterPressed == value)
        return;
      this._isEnterPressed = value;
      if (!this._isEnterPressed)
        return;
      if ((UnityEngine.Object) UICamera.selectedObject != (UnityEngine.Object) null)
        UICamera.selectedObject = (GameObject) null;
      this.MapZoomEnd();
      DialogManager.ShowDialog<ChatDialog>(false);
    }
  }

  private bool isPressedJump
  {
    set
    {
      if (value == this._isPressedJump)
        return;
      if (!value)
      {
        CourseController course = TibProxy.gameState.Course;
        if (course.Count > 0)
        {
          if (course.Paused)
            course.Resume();
          else
            course.Pause();
        }
      }
      this._isPressedJump = value;
    }
  }

  public void MapZoomIn()
  {
    this.isPressedMapZoomIn = true;
    this.isPressedMapZoomOut = false;
    this._overrideMapZoomAxis = true;
  }

  public void MapZoomOut()
  {
    this.isPressedMapZoomOut = true;
    this.isPressedMapZoomIn = false;
    this._overrideMapZoomAxis = true;
  }

  public void MapZoomEnd()
  {
    this._overrideMapZoomAxis = false;
    this._isPressedMapZoomOut = false;
    this._isPressedMapZoomIn = false;
    PreInstantiatedSingleton<SectorMap>.Instance.controller.ZoomEnd();
  }

  private bool isPressedBlackDollarRepair
  {
    set
    {
      if (value == this._isPressedBlackDollarRepair)
        return;
      if (!value)
        TibProxy.gameState.DoPurchase(PurchaseItem.FullRepair);
      this._isPressedBlackDollarRepair = value;
    }
  }

  private bool isPressedMetalRepair
  {
    set
    {
      if (value == this._isPressedMetalRepair)
        return;
      if (!value)
        TibProxy.gameState.DoRepair((PlayerCombatEntity) TibProxy.gameState.MyShip);
      this._isPressedMetalRepair = value;
    }
  }

  private bool isPressedStarportRepair
  {
    set
    {
      if (value == this._isPressedStarportRepair)
        return;
      if (!value)
      {
        StarPort starPort = TibProxy.gameState.Local.StarPort;
        if (starPort == null)
          return;
        TibProxy.gameState.DoRepairWith(starPort);
      }
      this._isPressedStarportRepair = value;
    }
  }

  private bool isPressedEarthJump
  {
    set
    {
      if (value == this._isPressedEarthJump)
        return;
      if (!value)
        TibProxy.gameState.DoPurchase(PurchaseItem.EarthJump);
      this._isPressedEarthJump = value;
    }
  }

  private bool isPressedCorpJump
  {
    set
    {
      if (value == this._isPressedCorpJump)
        return;
      if (!value)
        TibProxy.gameState.DoPurchase(PurchaseItem.CorpJump);
      this._isPressedCorpJump = value;
    }
  }

  private bool isPressedRiftJump
  {
    set
    {
      if (value == this._isPressedRiftJump)
        return;
      if (!value)
        TibProxy.gameState.DoPurchase(PurchaseItem.RiftJump);
      this._isPressedRiftJump = value;
    }
  }

  private bool isPressedUniverseQuickView
  {
    set
    {
      if (value == this._isPressedUniverseQuickView)
        return;
      if (!value)
        PreInstantiatedSingleton<SectorMap>.Instance.ToggleUniverseView();
      this._isPressedUniverseQuickView = value;
    }
  }

  private bool isPressedBlackDollarMenu
  {
    set
    {
      if (value == this._isPressedBlackDollarMenu)
        return;
      if (!value)
        DialogManager.ShowDialog<BlackDollarPurchasesDialog>(false);
      this._isPressedBlackDollarMenu = value;
    }
  }

  private void Start()
  {
    this._mapCamera = PreInstantiatedSingleton<SectorMap>.Instance.GetComponentInChildren<Camera>();
  }

  private void OnDisable()
  {
    this.isPressedNorth = false;
    this.isPressedNortheast = false;
    this.isPressedEast = false;
    this.isPressedSoutheast = false;
    this.isPressedSouth = false;
    this.isPressedSouthwest = false;
    this.isPressedWest = false;
    this.isPressedNorthwest = false;
    this.isPressedMapZoomIn = false;
    this._isPressedMapZoomOut = false;
    this.isPressedJump = false;
    this.isEnterPressed = false;
    this.isPressedBlackDollarRepair = false;
    this.isPressedMetalRepair = false;
    this.isPressedStarportRepair = false;
    this.isPressedEarthJump = false;
    this.isPressedCorpJump = false;
    this.isPressedRiftJump = false;
    this.isPressedUniverseQuickView = false;
    this.isPressedBlackDollarMenu = false;
  }

  private void Update()
  {
    if ((UnityEngine.Object) this._mapCamera == (UnityEngine.Object) null || !this._mapCamera.enabled)
      return;
    this.isPressedNorth = (double) Input.GetAxis("North") > 0.0;
    this.isPressedNortheast = (double) Input.GetAxis("Northeast") > 0.0;
    this.isPressedEast = (double) Input.GetAxis("East") > 0.0;
    this.isPressedSoutheast = (double) Input.GetAxis("Southeast") > 0.0;
    this.isPressedSouth = (double) Input.GetAxis("South") > 0.0;
    this.isPressedSouthwest = (double) Input.GetAxis("Southwest") > 0.0;
    this.isPressedWest = (double) Input.GetAxis("West") > 0.0;
    this.isPressedNorthwest = (double) Input.GetAxis("Northwest") > 0.0;
    if (!this._overrideMapZoomAxis)
    {
      this.isPressedMapZoomIn = (double) Input.GetAxis("MapZoom") > 0.0;
      this.isPressedMapZoomOut = (double) Input.GetAxis("MapZoom") < 0.0;
    }
    this.isPressedJump = (double) Input.GetAxis("Jump") > 0.0;
    this.isEnterPressed = Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Return) || Input.inputString == Environment.NewLine;
    this.isPressedBlackDollarRepair = (double) Input.GetAxis("BlackDollar Repair") > 0.0;
    this.isPressedMetalRepair = (double) Input.GetAxis("Metal Repair") > 0.0;
    this.isPressedStarportRepair = (double) Input.GetAxis("Starport Repair") > 0.0;
    this.isPressedEarthJump = (double) Input.GetAxis("Earth Jump") > 0.0;
    this.isPressedCorpJump = (double) Input.GetAxis("Corp Jump") > 0.0;
    this.isPressedRiftJump = (double) Input.GetAxis("Rift Jump") > 0.0;
    this.isPressedUniverseQuickView = (double) Input.GetAxis("Universe Quick View") > 0.0;
    this.isPressedBlackDollarMenu = (double) Input.GetAxis("BlackDollar Menu") > 0.0;
  }

  private static void DoJump(KeyboardMapController.JumpDirection direction)
  {
    IGameState gameState = TibProxy.gameState;
    switch (direction)
    {
      case KeyboardMapController.JumpDirection.North:
        gameState.Course.Set(gameState.MyLoc.N);
        break;
      case KeyboardMapController.JumpDirection.Northeast:
        gameState.Course.Set(gameState.MyLoc.NE);
        break;
      case KeyboardMapController.JumpDirection.East:
        gameState.Course.Set(gameState.MyLoc.E);
        break;
      case KeyboardMapController.JumpDirection.Southeast:
        gameState.Course.Set(gameState.MyLoc.SE);
        break;
      case KeyboardMapController.JumpDirection.South:
        gameState.Course.Set(gameState.MyLoc.S);
        break;
      case KeyboardMapController.JumpDirection.Southwest:
        gameState.Course.Set(gameState.MyLoc.SW);
        break;
      case KeyboardMapController.JumpDirection.West:
        gameState.Course.Set(gameState.MyLoc.W);
        break;
      case KeyboardMapController.JumpDirection.Northwest:
        gameState.Course.Set(gameState.MyLoc.NW);
        break;
    }
    gameState.Course.Resume();
  }

  private enum JumpDirection
  {
    North,
    Northeast,
    East,
    Southeast,
    South,
    Southwest,
    West,
    Northwest,
  }
}
