﻿// Decompiled with JetBrains decompiler
// Type: ItemDetailsPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity.UI;

public class ItemDetailsPopup : DialogBase
{
  public UIPanel rarityPanel;
  public UIPanel rankPanel;
  public ItemDetailsCard itemDetails;
  public UIGrid rarityListingGrid;
  public UIGrid rankListingGrid;
  public ItemDetailsItemCard itemCardPrefab;
  public ItemDetailsRarityCard rarityCardPrefab;
  public ItemDetailsRankCard rankCardPrefab;

  public ItemDetailsContext context { get; private set; }

  public int baseDepth
  {
    get
    {
      return this.panel.depth;
    }
    set
    {
      this.panel.depth = value;
      this.rarityPanel.depth = this.panel.depth + 1;
      this.rankPanel.depth = this.rarityPanel.depth + 1;
    }
  }

  protected override bool ProcessArgs(object[] args)
  {
    if (args.Length != 1)
      throw new ArgumentException("Expected args length is 1.");
    this.ShowAsPopupInternal(UIPanel.nextUnusedDepth, (EquipmentItem) args[0]);
    return false;
  }

  private void ShowAsPopupInternal(int basePanelDepth, EquipmentItem item)
  {
    this.baseDepth = basePanelDepth;
    this.context.baseItem = item;
    this.rankListingGrid.gameObject.SetActive(this.context.hasRank);
  }

  private void ShowInternal(int basePanelDepth, EquipmentItem item)
  {
    this.context.baseItem = item;
    this.rankListingGrid.gameObject.SetActive(this.context.hasRank);
    this.baseDepth = basePanelDepth;
    this.Show();
  }

  protected void SetSelectedItem(EquipmentItem item)
  {
    this.context.baseItem = item;
  }

  protected override void OnRegisterEventHandlers()
  {
  }

  protected override void OnUnRegisterEventHandlers()
  {
  }

  protected override void OnAwake()
  {
    this.panel = this.GetComponent<UIPanel>();
    this.context = new ItemDetailsContext();
    this.itemDetails.dialogContext = this.context;
    this.InitializeRarityListing();
    this.InitializeRankListing();
  }

  protected override void OnHide()
  {
    base.OnHide();
    this.context.baseItem = (EquipmentItem) null;
  }

  private void InitializeRarityListing()
  {
    foreach (object obj in Enum.GetValues(typeof (ItemRarity)))
    {
      if ((int) obj != (int) sbyte.MinValue)
      {
        ItemDetailsRarityCard component = this.rarityListingGrid.gameObject.AddChild(this.rarityCardPrefab.gameObject).GetComponent<ItemDetailsRarityCard>();
        component.rarity = (ItemRarity) obj;
        component.dialogContext = this.context;
      }
    }
    this.rarityListingGrid.Reposition();
  }

  private void InitializeRankListing()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ItemDetailsPopup.\u003CInitializeRankListing\u003Ec__AnonStorey3F listingCAnonStorey3F = new ItemDetailsPopup.\u003CInitializeRankListing\u003Ec__AnonStorey3F();
    for (int index = 1; index < Util.NUMERALS.Length; ++index)
    {
      ItemDetailsRankCard component = this.rankListingGrid.gameObject.AddChild(this.rankCardPrefab.gameObject).GetComponent<ItemDetailsRankCard>();
      component.rank = index;
      component.dialogContext = this.context;
    }
    this.rankListingGrid.Reposition();
    this.rankListingGrid.transform.parent.GetComponent<UIScrollView>().ResetPosition();
    // ISSUE: reference to a compiler-generated field
    listingCAnonStorey3F.cc = this.rankListingGrid.GetComponent<UICenterOnChild>();
    // ISSUE: reference to a compiler-generated field
    if (!(bool) ((UnityEngine.Object) listingCAnonStorey3F.cc))
      return;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    listingCAnonStorey3F.cc.onFinished = new SpringPanel.OnFinished(listingCAnonStorey3F.\u003C\u003Em__86);
  }
}
