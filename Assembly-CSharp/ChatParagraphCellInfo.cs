﻿// Decompiled with JetBrains decompiler
// Type: ChatParagraphCellInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client.Events;
using UnityEngine;

public class ChatParagraphCellInfo : ScrollViewCellInfoBase<GameEventArgs, ChatParagraphCell>
{
  private readonly string _formattedText;
  private Vector3 _extents;
  private int _width;
  private int _height;

  public ChatParagraphCellInfo(Transform prefab, GameEventArgs e, int initialWidth)
    : base(prefab, e)
  {
    this._formattedText = ChatParagraphCellInfo.GetFormattedText(e);
    this.UpdateDimensions(initialWidth);
  }

  public override Vector3 extents
  {
    get
    {
      return this._extents;
    }
  }

  public string formattedText
  {
    get
    {
      return this._formattedText;
    }
  }

  public int width
  {
    get
    {
      return this._width;
    }
    set
    {
      if (this._width == value)
        return;
      this._width = value;
      this._extents.x = (float) this._width / 2f;
    }
  }

  public int height
  {
    get
    {
      return this._height;
    }
    set
    {
      if (this._height == value)
        return;
      this._height = value;
      this._extents.y = (float) this._height / 2f;
    }
  }

  protected override void SetVisuals(ChatParagraphCell cell)
  {
    cell.paragraph.text = this.formattedText;
    cell.paragraph.width = this.width;
    cell.paragraph.height = this.height;
    cell.transform.localPosition = this.localPosition;
  }

  public void UpdateAvaliableWidth(int avaliableWidth)
  {
    if (avaliableWidth == this.width)
      return;
    this.UpdateDimensions(avaliableWidth);
  }

  public void UpdateFontSize(int newFontSize)
  {
    if ((bool) ((Object) this.mCellInstance))
      this.mCellInstance.fontSize = newFontSize;
    this.UpdateDimensions(this.width);
  }

  public void UpdateDimensions()
  {
    this.UpdateDimensions(this.width);
  }

  public void UpdateDimensions(int pWidth)
  {
    ChatParagraphCell chatParagraphCell = this.mCellInstance ?? this.cellPrefab.GetComponent<ChatParagraphCell>();
    int width = chatParagraphCell.paragraph.width;
    chatParagraphCell.paragraph.width = pWidth;
    chatParagraphCell.paragraph.text = this.formattedText;
    string processedText = chatParagraphCell.paragraph.processedText;
    this.width = chatParagraphCell.paragraph.width;
    this.height = chatParagraphCell.paragraph.height;
    if (!((Object) this.mCellInstance == (Object) null))
      return;
    chatParagraphCell.paragraph.width = width;
    chatParagraphCell.paragraph.text = string.Empty;
  }

  private static string GetFormattedText(GameEventArgs e)
  {
    StringBuilder stringBuilder = new StringBuilder(256);
    e.AppendText(stringBuilder);
    Markup.GetNGUI(stringBuilder);
    stringBuilder.Append("[-][-][-][/b][/i][/u][/s]");
    return stringBuilder.ToString();
  }
}
