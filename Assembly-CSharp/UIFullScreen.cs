﻿// Decompiled with JetBrains decompiler
// Type: UIFullScreen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIFullScreen : MonoBehaviour
{
  public UIButton button;
  public UILabel label;

  private void Awake()
  {
    if (!((Object) this.button != (Object) null))
      return;
    UIEventListener.Get(this.button.gameObject).onClick = new UIEventListener.VoidDelegate(this.OnClickBtn);
  }

  private void Start()
  {
    if (PlayerPrefs.GetInt("FS", 0) == 1)
    {
      this.Set(true);
    }
    else
    {
      if (!((Object) this.label != (Object) null))
        return;
      this.label.text = Localization.Get("Full Screen");
    }
  }

  private void OnClick()
  {
    this.Set(!Screen.fullScreen);
  }

  private void OnClickBtn(GameObject go)
  {
    this.Set(!Screen.fullScreen);
  }

  private void Set(bool full)
  {
    if (Screen.fullScreen == full)
      return;
    if (full)
    {
      Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
      if ((Object) this.label != (Object) null)
        this.label.text = Localization.Get("Windowed");
      PlayerPrefs.SetInt("FS", 1);
    }
    else
    {
      Screen.SetResolution(1280, 720, false);
      if ((Object) this.label != (Object) null)
        this.label.text = Localization.Get("Full Screen");
      PlayerPrefs.SetInt("FS", 0);
    }
  }
}
