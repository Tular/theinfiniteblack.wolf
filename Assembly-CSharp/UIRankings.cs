﻿// Decompiled with JetBrains decompiler
// Type: UIRankings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (UIGrid))]
public class UIRankings : MonoBehaviour
{
  public Color normalColor = Color.white;
  public Color highlightColor = Color.white;
  private BetterList<UIRankings.Entry> mFullList = new BetterList<UIRankings.Entry>();
  private BetterList<UIRankings.Entry> mCleanList = new BetterList<UIRankings.Entry>();
  public GameObject prefab;
  public UILabel info;
  public UIButton columnA;
  public UIButton columnB;
  public UIButton columnC;
  public UISprite backgroundA;
  public UISprite backgroundB;
  public UISprite backgroundC;
  public UIWidget[] ornaments;
  private string mData;
  private UIGrid mGrid;
  private GameObject mSortColumn;
  private UIScoreRow[] mChildren;
  private bool mDownloading;

  private void Awake()
  {
    this.mGrid = this.GetComponent<UIGrid>();
    UIEventListener.Get(this.columnA.gameObject).onClick = new UIEventListener.VoidDelegate(this.SortNow);
    UIEventListener.Get(this.columnB.gameObject).onClick = new UIEventListener.VoidDelegate(this.SortNow);
    UIEventListener.Get(this.columnC.gameObject).onClick = new UIEventListener.VoidDelegate(this.SortNow);
    this.columnA.isEnabled = false;
    this.columnB.isEnabled = false;
    this.columnC.isEnabled = false;
  }

  private void OnEnable()
  {
    bool flag = this.mFullList.size > 0;
    this.columnA.isEnabled = flag;
    this.columnB.isEnabled = flag;
    this.columnC.isEnabled = flag;
    this.backgroundA.enabled = flag && (UnityEngine.Object) this.mSortColumn == (UnityEngine.Object) this.columnA.gameObject;
    this.backgroundB.enabled = flag && (UnityEngine.Object) this.mSortColumn == (UnityEngine.Object) this.columnB.gameObject;
    this.backgroundC.enabled = flag && (UnityEngine.Object) this.mSortColumn == (UnityEngine.Object) this.columnC.gameObject;
    if (string.IsNullOrEmpty(this.mData))
    {
      if (PlayerProfile.allowedToAccessInternet)
      {
        if (this.mDownloading)
          return;
        this.mDownloading = true;
        this.info.text = Localization.Get("Loading");
        this.info.enabled = true;
        for (int index = 0; index < this.ornaments.Length; ++index)
          this.ornaments[index].enabled = false;
        this.StartCoroutine(this.DownloadData());
      }
      else
      {
        this.info.text = Localization.Get("Wifi Required");
        this.info.enabled = true;
        for (int index = 0; index < this.ornaments.Length; ++index)
          this.ornaments[index].enabled = false;
      }
    }
    else
      this.Show();
  }

  [DebuggerHidden]
  private IEnumerator DownloadData()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UIRankings.\u003CDownloadData\u003Ec__Iterator9()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void ParseData(string data)
  {
    this.mData = data;
    this.mFullList.Clear();
    string[] strArray1 = this.mData.Split(new char[1]
    {
      '\n'
    }, StringSplitOptions.RemoveEmptyEntries);
    int index = 0;
    for (int length = strArray1.Length; index < length; ++index)
    {
      string[] strArray2 = strArray1[index].Split(new char[1]
      {
        '\t'
      }, StringSplitOptions.RemoveEmptyEntries);
      if (strArray2.Length == 6)
      {
        UIRankings.Entry entry = new UIRankings.Entry();
        entry.name = strArray2[1];
        int.TryParse(strArray2[2], out entry.exp);
        int.TryParse(strArray2[3], out entry.valueA);
        int.TryParse(strArray2[4], out entry.valueB);
        int.TryParse(strArray2[5], out entry.valueC);
        this.mFullList.Add(entry);
      }
    }
  }

  private void SortNow(GameObject go)
  {
    if (this.mFullList.size == 0)
    {
      this.info.text = Localization.Get("No Data");
      this.info.enabled = true;
      for (int index = 0; index < this.ornaments.Length; ++index)
        this.ornaments[index].enabled = false;
    }
    else
    {
      if (!((UnityEngine.Object) this.mSortColumn != (UnityEngine.Object) go))
        return;
      this.mSortColumn = go;
      this.mCleanList.Clear();
      if ((UnityEngine.Object) go == (UnityEngine.Object) this.columnC.gameObject)
      {
        for (int index = 0; index < this.mFullList.size; ++index)
        {
          UIRankings.Entry mFull = this.mFullList[index];
          if (mFull.valueC != 0)
            this.mCleanList.Add(mFull);
        }
      }
      else if ((UnityEngine.Object) go == (UnityEngine.Object) this.columnB.gameObject)
      {
        for (int index = 0; index < this.mFullList.size; ++index)
        {
          UIRankings.Entry mFull = this.mFullList[index];
          if (mFull.valueB != 0)
            this.mCleanList.Add(mFull);
        }
      }
      else
      {
        for (int index = 0; index < this.mFullList.size; ++index)
        {
          UIRankings.Entry mFull = this.mFullList[index];
          if (mFull.valueA != 0)
            this.mCleanList.Add(mFull);
        }
      }
      UIToggle component = go.GetComponent<UIToggle>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.value = true;
      this.columnA.isEnabled = true;
      this.columnB.isEnabled = true;
      this.columnC.isEnabled = true;
      if ((UnityEngine.Object) go == (UnityEngine.Object) this.columnA.gameObject)
        this.mCleanList.Sort((BetterList<UIRankings.Entry>.CompareFunc) ((a, b) => b.valueA.CompareTo(a.valueA)));
      else if ((UnityEngine.Object) go == (UnityEngine.Object) this.columnB.gameObject)
        this.mCleanList.Sort((BetterList<UIRankings.Entry>.CompareFunc) ((a, b) => b.valueB.CompareTo(a.valueB)));
      else if ((UnityEngine.Object) go == (UnityEngine.Object) this.columnC.gameObject)
        this.mCleanList.Sort((BetterList<UIRankings.Entry>.CompareFunc) ((a, b) => b.valueC.CompareTo(a.valueC)));
      this.Show();
    }
  }

  private void Show()
  {
    this.backgroundA.enabled = (UnityEngine.Object) this.mSortColumn == (UnityEngine.Object) this.columnA.gameObject;
    this.backgroundB.enabled = (UnityEngine.Object) this.mSortColumn == (UnityEngine.Object) this.columnB.gameObject;
    this.backgroundC.enabled = (UnityEngine.Object) this.mSortColumn == (UnityEngine.Object) this.columnC.gameObject;
    if (this.mChildren == null)
    {
      this.mChildren = new UIScoreRow[7];
      for (int index = 0; index < 7; ++index)
      {
        GameObject gameObject = this.gameObject.AddChild(this.prefab);
        this.mChildren[index] = gameObject.GetComponent<UIScoreRow>();
        this.mChildren[index].name = (index + 1).ToString();
      }
      this.mGrid.Reposition();
    }
    int num1 = this.mCleanList.size != 0 ? this.mCleanList.size - 1 : 0;
    for (int index = 0; index < this.mCleanList.size; ++index)
    {
      if (this.mCleanList[index].name == PlayerProfile.playerName)
      {
        num1 = index;
        break;
      }
    }
    int num2 = num1 - 3;
    int num3 = num1 + 4;
    while (num3 > this.mCleanList.size)
    {
      --num3;
      --num2;
    }
    while (num2 < 0)
    {
      ++num2;
      ++num3;
    }
    for (int index = num2; index < num3; ++index)
    {
      UIScoreRow mChild = this.mChildren[index - num2];
      bool flag = index < this.mCleanList.size;
      if (flag)
      {
        UIRankings.Entry mClean = this.mCleanList[index];
        mChild.nameLabel.text = mClean.name;
        mChild.rankLabel.text = (index + 1).ToString();
        mChild.titleLabel.text = PlayerProfile.GetTitleByExp(mClean.exp);
        mChild.labelA.text = mClean.valueA.ToString();
        mChild.labelB.text = mClean.valueB.ToString();
        mChild.labelC.text = mClean.valueC.ToString();
        mChild.background.color = !(mClean.name == PlayerProfile.playerName) ? this.normalColor : this.highlightColor;
      }
      else
        mChild.background.color = this.normalColor;
      mChild.nameLabel.enabled = flag;
      mChild.rankLabel.enabled = flag;
      mChild.titleLabel.enabled = flag;
      mChild.labelA.enabled = flag;
      mChild.labelB.enabled = flag;
      mChild.labelC.enabled = flag;
    }
    this.info.enabled = false;
    for (int index = 0; index < this.ornaments.Length; ++index)
      this.ornaments[index].enabled = true;
  }

  private class Entry
  {
    public string name;
    public int exp;
    public int valueA;
    public int valueB;
    public int valueC;
  }
}
