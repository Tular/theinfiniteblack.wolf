﻿// Decompiled with JetBrains decompiler
// Type: MockPurchaseProcessor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class MockPurchaseProcessor : IPurchaseProcessor
{
  private const string _testGoogleIapData = "{ \\\"orderId\\\":\\\"12999763169054705758.1372475030843663\\\",\r\n\\\"packageName\\\":\\\"theinfiniteblack.free.mmo\\\",\r\n\\\"productId\\\":\\\"tibunity.bd10\\\",\r\n\\\"purchaseTime\\\":1424195781688,\r\n\\\"purchaseState\\\":0,\r\n\\\"developerPayload\\\":\\\"TUnity\\\",\r\n\\\"purchaseToken\\\":\\\"jjglojnmielkllfbhjkiloia.AO-J1OxTJ1YLCrQexwGQ26DFPaNFYefYWxEG57pM5WNFe8sxTXBA-BWN0jwxcEeAvs3bGJsdM0f32ESAbYg7j3ejkrGgc2scenwsVStu8sz8tbyZVAJ_BSlGJT4zUTtvBFd6Mgd6GS1X\\\" }";
  private const string _testGoogleSignature = "JfCtP0zp9Y1JzA1VBLYeP8mRF1d1yS3J8TynnlsIhr6X9RUOyeIly/qerb2c33/f+2hfkU9NEJcANWB3OZ/W5c587IOOcdbDZEk637BhjHTvP7OwAsiGbsKjMbhzpiGDzEmF06ZCVhg+xxeGItLsji59nb1tv33nkgfKWzeQaJ2OozdVXj35j30AR//lIFMVjDTH8QVD1vpi/Y4QY8QUGARaech6SUMpf+slw0dg+FGkdO/QBHAlLd7PFrtfWTM1Ftk8139iH9Zuw1mi9X6quCsdBa6NBvVb+156zVexhV+xgfQ+I7Yavq3Zu1WlQVDMMiChOEIFjMm/IdmJwg+cpw==";
  public MockPurchaseProcessor.TestMode testMode;

  public string GetProductId(IProductInfo product)
  {
    throw new NotImplementedException();
  }

  public PendingOrderDetail ParseReceipt(IInAppPurchasingContext context, string strReceipt)
  {
    Debug.Log((object) "++++ GooglePlayPurchasingConfig.CompletePurchase");
    Dictionary<string, object> dictionary1 = MiniJsonExtensions.HashtableFromJson(strReceipt);
    string str1 = string.Format("{{ \"json\":\"{0}\",\"signature\":\"{1}\" }}", (object) "{ \\\"orderId\\\":\\\"12999763169054705758.1372475030843663\\\",\r\n\\\"packageName\\\":\\\"theinfiniteblack.free.mmo\\\",\r\n\\\"productId\\\":\\\"tibunity.bd10\\\",\r\n\\\"purchaseTime\\\":1424195781688,\r\n\\\"purchaseState\\\":0,\r\n\\\"developerPayload\\\":\\\"TUnity\\\",\r\n\\\"purchaseToken\\\":\\\"jjglojnmielkllfbhjkiloia.AO-J1OxTJ1YLCrQexwGQ26DFPaNFYefYWxEG57pM5WNFe8sxTXBA-BWN0jwxcEeAvs3bGJsdM0f32ESAbYg7j3ejkrGgc2scenwsVStu8sz8tbyZVAJ_BSlGJT4zUTtvBFd6Mgd6GS1X\\\" }", (object) "JfCtP0zp9Y1JzA1VBLYeP8mRF1d1yS3J8TynnlsIhr6X9RUOyeIly/qerb2c33/f+2hfkU9NEJcANWB3OZ/W5c587IOOcdbDZEk637BhjHTvP7OwAsiGbsKjMbhzpiGDzEmF06ZCVhg+xxeGItLsji59nb1tv33nkgfKWzeQaJ2OozdVXj35j30AR//lIFMVjDTH8QVD1vpi/Y4QY8QUGARaech6SUMpf+slw0dg+FGkdO/QBHAlLd7PFrtfWTM1Ftk8139iH9Zuw1mi9X6quCsdBa6NBvVb+156zVexhV+xgfQ+I7Yavq3Zu1WlQVDMMiChOEIFjMm/IdmJwg+cpw==");
    dictionary1["Payload"] = (object) str1;
    Dictionary<string, object> dictionary2 = MiniJsonExtensions.HashtableFromJson(dictionary1["Payload"] as string);
    using (Dictionary<string, object>.Enumerator enumerator = dictionary2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, object> current = enumerator.Current;
        Debug.LogFormat("{0}:{1}", new object[2]
        {
          (object) current.Key,
          current.Value
        });
      }
    }
    string str2 = dictionary2["json"] as string;
    string str3 = dictionary2["signature"] as string;
    return new PendingOrderDetail()
    {
      json = str2,
      signature = str3
    };
  }

  public void ConfigureBuilder(ConfigurationBuilder builder)
  {
  }

  public bool TryGetPostString(PendingOrderDetail order, out string postString)
  {
    postString = string.Empty;
    return false;
  }

  public enum TestMode
  {
    GooglePlay,
    AppleAppStore,
  }
}
