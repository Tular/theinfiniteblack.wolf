﻿// Decompiled with JetBrains decompiler
// Type: ItemSelectPopupItemListingScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Items;
using UnityEngine;

[RequireComponent(typeof (UIScrollView))]
public class ItemSelectPopupItemListingScrollView : GeneralScrollViewBase<EquipmentItem, ItemSelectPopupContext>
{
  public UIGrid cardGrid;
  public int itemCardToggleGroup;
  private UIPanel _panel;
  private UIScrollView _scrollView;

  public int panelDepth
  {
    get
    {
      return this._panel.depth;
    }
    set
    {
      this._panel.depth = value;
    }
  }

  protected override void CreateCards()
  {
    foreach (EquipmentItem equipmentItem in this.context.items)
      this.InitializeCard(this.cardSpawnPool.Spawn(this.prefItemCard.transform), equipmentItem);
  }

  protected override GeneralScrollViewCardBase InitializeCard(Transform spawnedTrans, EquipmentItem item)
  {
    ItemSelectPopupItemListingCard component = spawnedTrans.GetComponent<ItemSelectPopupItemListingCard>();
    component.item = item;
    component.toggleGroup = this.itemCardToggleGroup;
    component.setSelectedItem = (System.Action<EquipmentItem>) (itm => this.context.selectedItem = itm);
    return (GeneralScrollViewCardBase) component;
  }

  protected override void OnRefreshView()
  {
    this.cardGrid.Reposition();
  }

  protected override void Awake()
  {
    base.Awake();
    this._scrollView = this.GetComponent<UIScrollView>();
    this._panel = this._scrollView.panel;
  }

  protected override void OnEnable()
  {
    for (int group = 1; group < 100; ++group)
    {
      if (!((UnityEngine.Object) UIToggle.GetActiveToggle(group) != (UnityEngine.Object) null))
      {
        this.itemCardToggleGroup = group;
        break;
      }
    }
  }

  protected override void OnDisable()
  {
    base.OnDisable();
    this.cardSpawnPool.DespawnAll();
  }
}
