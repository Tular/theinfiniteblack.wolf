﻿// Decompiled with JetBrains decompiler
// Type: UniWebViewHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UniWebViewHelper
{
  public static int screenHeight
  {
    get
    {
      return Screen.height;
    }
  }

  public static int screenWidth
  {
    get
    {
      return Screen.width;
    }
  }

  public static int screenScale
  {
    get
    {
      return 1;
    }
  }

  public static string streamingAssetURLForPath(string path)
  {
    return Application.streamingAssetsPath + "/" + path;
  }
}
