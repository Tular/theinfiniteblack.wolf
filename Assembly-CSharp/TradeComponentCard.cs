﻿// Decompiled with JetBrains decompiler
// Type: TradeComponentCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Items;
using UnityEngine;

public class TradeComponentCard : MonoBehaviour
{
  public string resourcePath = "items";
  private const string MsgNoBlackDollarsOffered = "No BlackDollars Offered";
  private const string MsgNoCreditsOffered = "No Credits Offered";
  private const string MsgNoItemOffered = "No Item Offered";
  public UILabel title;
  public UILabel creditsLabel;
  public UILabel blackDollarsLabel;
  public UILabel itemLabel;
  public UITexture itemIcon;
  private TradeComponent _tradeComponent;

  public TradeComponent tradeComponent
  {
    set
    {
      this._tradeComponent = value;
      if (this._tradeComponent == null)
      {
        this.ResetState();
      }
      else
      {
        this.title.text = value.Player != null ? value.Player.Name : "[Error:Target is Null]";
        this.credits = value.Credits;
        this.blackDollars = value.BlackDollars;
        this.item = value.Item;
      }
    }
  }

  public int credits
  {
    get
    {
      if (this._tradeComponent == null)
        return 0;
      return this._tradeComponent.Credits;
    }
    set
    {
      if (this._tradeComponent == null)
        return;
      this._tradeComponent.Credits = value;
      this.creditsLabel.text = this._tradeComponent.Credits <= 0 ? "No Credits Offered" : string.Format("{0:$###,###,###} Credits", (object) this._tradeComponent.Credits);
    }
  }

  public int blackDollars
  {
    get
    {
      if (this._tradeComponent == null)
        return 0;
      return this._tradeComponent.BlackDollars;
    }
    set
    {
      if (this._tradeComponent == null)
        return;
      this._tradeComponent.BlackDollars = value;
      UILabel blackDollarsLabel = this.blackDollarsLabel;
      string str1;
      if (this._tradeComponent.BlackDollars > 0)
      {
        string str2 = string.Format("{0:###,###,###} BlackDollars", (object) this._tradeComponent.BlackDollars);
        this.blackDollarsLabel.text = str2;
        str1 = str2;
      }
      else
        str1 = "No BlackDollars Offered";
      blackDollarsLabel.text = str1;
    }
  }

  public EquipmentItem item
  {
    get
    {
      if (this._tradeComponent == null)
        return (EquipmentItem) null;
      return this._tradeComponent.Item;
    }
    set
    {
      if (this._tradeComponent == null)
        return;
      this._tradeComponent.Item = value;
      if (this._tradeComponent.Item == null)
      {
        this.itemIcon.mainTexture = (Texture) null;
        this.itemLabel.text = "No Item Offered";
      }
      else
      {
        this.LoadItemTexture(this._tradeComponent.Item, this.itemIcon);
        StringBuilder stringBuilder = new StringBuilder(200);
        this._tradeComponent.Item.AppendName(stringBuilder);
        stringBuilder.Append(" [" + (object) this._tradeComponent.Item.Durability + "%]");
        stringBuilder.AppendLine();
        stringBuilder.Append(this._tradeComponent.Item.Rarity.GetMarkup());
        this._tradeComponent.Item.AppendSubTitle(stringBuilder);
        stringBuilder.Append("[-]");
        Markup.GetNGUI(stringBuilder);
        stringBuilder.AppendLine();
        switch (this._tradeComponent.Item.Type)
        {
          case ItemType.COMPUTER:
            stringBuilder.Append("[b6d4ff]");
            stringBuilder.Append(((int) ((ComputerItem) this._tradeComponent.Item).RequiredShip).ToString() + "-Class Ships");
            stringBuilder.Append("[-]");
            stringBuilder.AppendLine();
            break;
          case ItemType.SPECIAL:
            stringBuilder.Append("[b6d4ff]");
            stringBuilder.Append(((int) ((SpecialItem) this._tradeComponent.Item).RequiredShip).ToString() + "-Class Ships");
            stringBuilder.Append("[-]");
            stringBuilder.AppendLine();
            break;
        }
        if (this._tradeComponent.Item.BindOnEquip)
        {
          stringBuilder.Append("[ff0000]");
          stringBuilder.Append("Bind on Equip");
          stringBuilder.Append("[-]");
        }
        this.itemLabel.text = stringBuilder.ToString();
      }
    }
  }

  private void ResetState()
  {
    this._tradeComponent = (TradeComponent) null;
    this.title.text = string.Empty;
    this.credits = 0;
    this.blackDollars = 0;
    this.item = (EquipmentItem) null;
    this.creditsLabel.text = "No Credits Offered";
    this.blackDollarsLabel.text = "No BlackDollars Offered";
    this.itemLabel.text = "No Item Offered";
  }

  private void LoadItemTexture(EquipmentItem itm, UITexture tex)
  {
    string path = string.Format("{0}/{1}", (object) this.resourcePath, (object) itm.Icon);
    Texture2D texture2D = Resources.Load<Texture2D>(path);
    if ((Object) texture2D == (Object) null)
      this.LogD(string.Format("did not load {0}", (object) path));
    else
      tex.mainTexture = (Texture) texture2D;
  }
}
