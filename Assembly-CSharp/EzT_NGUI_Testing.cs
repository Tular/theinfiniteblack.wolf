﻿// Decompiled with JetBrains decompiler
// Type: EzT_NGUI_Testing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using AdvancedInspector;
using UnityEngine;

[AdvancedInspector.AdvancedInspector]
public class EzT_NGUI_Testing : MonoBehaviour, IDataChanged
{
  private int _nguiTouchCount;
  private int _ezTouchCount;
  private int _ezNguiCams;
  private GameObject _nguiSelecteGameObject;
  private bool _hasChanged;

  public event GenericEventHandler OnDataChanged;

  [Inspect(0)]
  public int nguiTouchCount
  {
    get
    {
      return this._nguiTouchCount;
    }
    set
    {
      this._nguiTouchCount = value;
      this._hasChanged = true;
    }
  }

  [Inspect(1)]
  public int ezTouchCount
  {
    get
    {
      return this._ezTouchCount;
    }
    set
    {
      this._ezTouchCount = value;
      this._hasChanged = true;
    }
  }

  [Inspect(2)]
  public int ezNguiCams
  {
    get
    {
      return this._ezNguiCams;
    }
    set
    {
      this._ezNguiCams = value;
      this._hasChanged = true;
    }
  }

  [Inspect(3)]
  public GameObject nguiSelectedObject
  {
    get
    {
      return this._nguiSelecteGameObject;
    }
    set
    {
      this._nguiSelecteGameObject = value;
      this._hasChanged = true;
    }
  }

  private void Start()
  {
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.EasyTouch_OnTouchStart);
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.EasyTouch_OnTouchStart);
    EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_OnTouchStart2Fingers);
    EasyTouch.On_TouchStart2Fingers += new EasyTouch.TouchStart2FingersHandler(this.EasyTouch_OnTouchStart2Fingers);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.EasyTouch_OnTouchUp);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.EasyTouch_OnTouchUp);
    EasyTouch.On_TouchUp2Fingers -= new EasyTouch.TouchUp2FingersHandler(this.EasyTouch_OnTouchUp2Fingers);
    EasyTouch.On_TouchUp2Fingers += new EasyTouch.TouchUp2FingersHandler(this.EasyTouch_OnTouchUp2Fingers);
    EasyTouch.On_Pinch -= new EasyTouch.PinchHandler(this.EasyTouch_OnPinch);
    EasyTouch.On_Pinch += new EasyTouch.PinchHandler(this.EasyTouch_OnPinch);
    EasyTouch.On_PinchEnd -= new EasyTouch.PinchEndHandler(this.EasyTouch_OnPinchEnd);
    EasyTouch.On_PinchEnd += new EasyTouch.PinchEndHandler(this.EasyTouch_OnPinchEnd);
    EasyTouch.On_SwipeStart -= new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
    EasyTouch.On_SwipeStart += new EasyTouch.SwipeStartHandler(this.EasyTouch_On_SwipeStart);
  }

  private void EasyTouch_On_SwipeStart(Gesture gesture)
  {
    Debug.Log((object) ">>>>>>>> EzTouch - SwipeStart");
  }

  private void Update()
  {
    this.nguiTouchCount = UICamera.activeTouches.Count;
    this.ezNguiCams = EasyTouch.instance.nGUICameras.Count;
    this.nguiSelectedObject = UICamera.selectedObject;
    if (this._hasChanged)
      this.FireOnDataChanged();
    this._hasChanged = false;
  }

  private void EasyTouch_OnPinch(Gesture gesture)
  {
    Debug.Log((object) ">>>>>>>> EzTouch - PinchStart");
    this.ezTouchCount = gesture.touchCount;
  }

  private void EasyTouch_OnPinchEnd(Gesture gesture)
  {
    Debug.Log((object) ">>>>>>>> EzTouch - PinchEnd");
    this.ezTouchCount = gesture.touchCount;
  }

  private void EasyTouch_OnTouchStart(Gesture gesture)
  {
    Debug.Log((object) ">>>>>>>> EzTouch - TouchStart");
    this.ezTouchCount = gesture.touchCount;
  }

  private void EasyTouch_OnTouchStart2Fingers(Gesture gesture)
  {
    Debug.Log((object) ">>>>>>>> EzTouch - TouchStart2Fingers");
    this.ezTouchCount = gesture.touchCount;
  }

  private void EasyTouch_OnTouchUp(Gesture gesture)
  {
    Debug.Log((object) ">>>>>>>> EzTouch - TouchUp");
    this.ezTouchCount = gesture.touchCount;
  }

  private void EasyTouch_OnTouchUp2Fingers(Gesture gesture)
  {
    Debug.Log((object) ">>>>>>>> EzTouch - TouchUp2Fingers");
    this.ezTouchCount = gesture.touchCount;
  }

  private void FireOnDataChanged()
  {
    if (this.OnDataChanged == null)
      return;
    this.OnDataChanged();
  }

  public void DataChanged()
  {
  }
}
