﻿// Decompiled with JetBrains decompiler
// Type: AuctionHouseItemListingScrollView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TheInfiniteBlack.Library.Client;
using UnityEngine;

public class AuctionHouseItemListingScrollView : GeneralScrollViewBase<ClientAuction, AuctionHouseItemListingScrollViewContext>
{
  private readonly Dictionary<int, AuctionHouseItemListingCard> _currentItems = new Dictionary<int, AuctionHouseItemListingCard>();
  public UITable cardTableRoot;
  public int cardToggleGroup;
  public float updateFrequency;
  private bool _isInitialized;
  private bool _isLoading;
  private float _nextUpdateTime;

  public void UpdateListing()
  {
    if (!this._isInitialized)
    {
      if (this._isLoading)
        return;
      this._isLoading = true;
      this.StartCoroutine(this.CreateCardsCoroutine());
    }
    else
    {
      List<ClientAuction> list1 = this.context.items.ToList<ClientAuction>();
      List<AuctionHouseItemListingCard> list2 = this._currentItems.Values.ToList<AuctionHouseItemListingCard>();
      bool flag = false;
      using (List<AuctionHouseItemListingCard>.Enumerator enumerator = list2.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AuctionHouseItemListingCard current = enumerator.Current;
          if (!list1.Contains(current.auction))
          {
            this._currentItems.Remove(current.auction.ID);
            this.cardSpawnPool.Despawn(current.transform);
            flag = true;
          }
        }
      }
      using (List<ClientAuction>.Enumerator enumerator = list1.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ClientAuction current = enumerator.Current;
          if (!this._currentItems.ContainsKey(current.ID))
          {
            this.InitializeCard(this.cardSpawnPool.Spawn(this.prefItemCard.transform), current).Refresh();
            flag = true;
          }
        }
      }
      if (!flag)
        return;
      this.cardTableRoot.Reposition();
      this._nextUpdateTime = (float) int.MinValue;
    }
  }

  protected override GeneralScrollViewCardBase InitializeCard(Transform spawnedTrans, ClientAuction auction)
  {
    AuctionHouseItemListingCard component = spawnedTrans.GetComponent<AuctionHouseItemListingCard>();
    component.InitializeCard(auction);
    this._currentItems.Add(auction.ID, component);
    return (GeneralScrollViewCardBase) component;
  }

  protected override void OnRefreshView()
  {
    this.cardTableRoot.Reposition();
    this.cardScrollView.ResetPosition();
  }

  public override void Clear()
  {
    this._isInitialized = false;
    this._isLoading = false;
    this.cardScrollView.ResetPosition();
    base.Clear();
    this.cardScrollView.ResetPosition();
    this._currentItems.Clear();
    this._nextUpdateTime = float.MinValue;
  }

  [DebuggerHidden]
  private IEnumerator CreateCardsCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AuctionHouseItemListingScrollView.\u003CCreateCardsCoroutine\u003Ec__IteratorF()
    {
      \u003C\u003Ef__this = this
    };
  }

  protected override void OnEnable()
  {
    this._nextUpdateTime = float.MinValue;
  }

  protected override void OnDisable()
  {
    this.Clear();
    this._isInitialized = false;
    this._isLoading = false;
    base.OnDisable();
  }

  private void Update()
  {
    if (this._currentItems.Count > 0 && (double) RealTime.time < (double) this._nextUpdateTime || this.cardScrollView.isDragging)
      return;
    this._nextUpdateTime = !this._isInitialized ? (float) int.MinValue : RealTime.time + this.updateFrequency;
    this.UpdateListing();
  }
}
