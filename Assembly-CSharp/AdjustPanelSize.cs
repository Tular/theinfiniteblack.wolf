﻿// Decompiled with JetBrains decompiler
// Type: AdjustPanelSize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof (UIGrid))]
public class AdjustPanelSize : MonoBehaviour
{
  public UIWidget container;
  public UIGrid grid;
  public Bounds gridBounds;
  public int topPadding;
  public int bottomPadding;
  public int leftPadding;
  public int rightPadding;
  public int containerHeight;
  public int containerWidth;
  public Bounds computedBounds;
  public Bounds encapsulatedBounds;
  public float calch;
  public bool execute;

  protected void Start()
  {
  }

  protected void Update()
  {
    if (!this.execute)
      return;
    this.execute = false;
    this.gridBounds = NGUIMath.CalculateRelativeWidgetBounds(this.grid.transform, false);
    this.computedBounds = this.container.CalculateBounds();
    this.encapsulatedBounds = this.container.CalculateBounds();
    this.encapsulatedBounds.Encapsulate(this.gridBounds);
    this.calch = (float) this.topPadding + this.gridBounds.extents.y * 2f;
  }
}
