﻿// Decompiled with JetBrains decompiler
// Type: PlayerShipUnitCardCell
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using TheInfiniteBlack.Library.Entities;
using UnityEngine;

public class PlayerShipUnitCardCell : CombatUnitCardCell
{
  public string shipSpritePrefix = "entity_ship_";
  public UISprite shipSprite;
  public UISprite techSprite;
  public UISprite specialItemSprite;
  public UILabel levelLabel;
  public UILabel shipTypeLabel;
  public Color pvpTitleColor;
  public Color defaultTitleColor;
  private float _level;
  private ShipClass _shipClass;
  private bool _isPvP;
  private bool _isSpecialVisible;

  public float level
  {
    get
    {
      return this._level;
    }
    set
    {
      this._level = value;
      this.levelLabel.text = string.Format("Lv {0:0.00}", (object) this._level);
    }
  }

  public ShipClass shipClass
  {
    get
    {
      return this._shipClass;
    }
    set
    {
      this._shipClass = value;
      this.shipSprite.spriteName = string.Format("{0}{1}", (object) this.shipSpritePrefix, (object) this._shipClass);
      this.shipTypeLabel.text = this.shipClass.Name();
    }
  }

  public bool isPvP
  {
    get
    {
      return this._isPvP;
    }
    set
    {
      this._isPvP = value;
      this.titleLabel.color = !this._isPvP ? this.defaultTitleColor : this.pvpTitleColor;
    }
  }

  public bool isSpecialVisible
  {
    get
    {
      return this._isSpecialVisible;
    }
    set
    {
      this._isSpecialVisible = value;
      this.specialItemSprite.cachedGameObject.SetActive(this._isSpecialVisible);
    }
  }

  public override void ResetVisuals()
  {
    base.ResetVisuals();
    this.levelLabel.text = string.Empty;
    this.shipTypeLabel.text = string.Empty;
    this.isPvP = false;
    this.isSpecialVisible = false;
  }
}
