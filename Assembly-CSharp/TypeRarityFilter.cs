﻿// Decompiled with JetBrains decompiler
// Type: TypeRarityFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheInfiniteBlack.Library;
using TheInfiniteBlack.Library.Client;
using TheInfiniteBlack.Library.Items;
using TheInfiniteBlack.Unity;

public class TypeRarityFilter : IAuctionFilter
{
  public ItemRarity rarity { get; set; }

  public ItemType type { get; set; }

  public string formattedName
  {
    get
    {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.Append(this.rarity.GetMarkup());
      stringBuilder.Append(this.rarity.Name());
      switch (this.type)
      {
        case ItemType.ARMOR:
        case ItemType.STORAGE:
          stringBuilder.AppendFormat(" {0}", (object) this.type.Name());
          break;
        default:
          stringBuilder.AppendFormat(" {0}s", (object) this.type.Name());
          break;
      }
      stringBuilder.Append("[-]");
      Markup.GetNGUI(stringBuilder);
      return stringBuilder.ToString();
    }
  }

  public IEnumerable<ClientAuction> GetFilteredAuctions(IEnumerable<ClientAuction> auctions)
  {
    if (this.rarity == ItemRarity.NULL || this.type == ItemType.NULL)
      return (IEnumerable<ClientAuction>) new List<ClientAuction>(1);
    TibProxy.gameState.DoAuctionRequestPage(this.type, this.rarity);
    return TibProxy.gameState.Auctions.get_Values().Where<ClientAuction>((Func<ClientAuction, bool>) (a =>
    {
      if (a.Item.Type == this.type)
        return a.Item.Rarity == this.rarity;
      return false;
    }));
  }

  public void ResetFilter()
  {
    this.rarity = ItemRarity.NULL;
    this.type = ItemType.NULL;
  }
}
