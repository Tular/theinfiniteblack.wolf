﻿// Decompiled with JetBrains decompiler
// Type: TestDialogManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using System;
using TheInfiniteBlack.Unity;
using TheInfiniteBlack.Unity.SettingsUI;
using TheInfiniteBlack.Unity.UI;
using UnityEngine;

public class TestDialogManager : MonoBehaviour
{
  public void ShowChatDialog()
  {
    DialogManager.ShowDialog<ChatDialog>(false);
  }

  public void CloseChatDialog()
  {
    DialogManager.CloseDialog<ChatDialog>();
  }

  public void ShowSettingsDialog()
  {
    DialogManager.ShowDialog<SettingsWindow>(false);
  }

  public void CloseSettingsDialog()
  {
    DialogManager.CloseDialog<SettingsWindow>();
  }

  public void ShowSystemDialog()
  {
    DialogManager.ShowDialog<SystemDialog>(false);
  }

  public void CloseSystemDialog()
  {
    DialogManager.CloseDialog<SystemDialog>();
  }

  public void ShowUiScaleDialog()
  {
    DialogManager.ShowDialog<UIScreenScaleControl>(false);
  }

  public void OpenScreenInfoDialog()
  {
    DialogManager.ShowDialog<ScreenInfoDialog>(false);
  }

  public void CloseScreenInfoDialog()
  {
    DialogManager.CloseDialog<ScreenInfoDialog>();
  }

  public void CloseUiScaleDialog()
  {
    DialogManager.CloseDialog<UIScreenScaleControl>();
  }

  public void CloseCurrentDialog()
  {
    throw new NotImplementedException();
  }

  public void CloseAllDialogs()
  {
    DialogManager.CloseAllDialogs();
  }
}
