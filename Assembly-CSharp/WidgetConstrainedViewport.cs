﻿// Decompiled with JetBrains decompiler
// Type: WidgetConstrainedViewport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (Camera))]
[ExecuteInEditMode]
public class WidgetConstrainedViewport : MonoBehaviour
{
  public float fullSize = 1f;
  public Camera sourceCamera;
  public UIWidget container;
  public Transform topLeft;
  public Transform bottomRight;
  private Camera mCam;

  private void Start()
  {
    this.mCam = this.GetComponent<Camera>();
    if (!((Object) this.sourceCamera == (Object) null))
      return;
    this.sourceCamera = Camera.main;
  }

  public Rect rect { get; private set; }

  private void LateUpdate()
  {
    if ((Object) this.topLeft != (Object) null && (Object) this.bottomRight != (Object) null)
    {
      Vector3 screenPoint1 = this.sourceCamera.WorldToScreenPoint(this.topLeft.position);
      Vector3 screenPoint2 = this.sourceCamera.WorldToScreenPoint(this.bottomRight.position);
      this.rect = new Rect(screenPoint1.x / (float) Screen.width, screenPoint2.y / (float) Screen.height, (screenPoint2.x - screenPoint1.x) / (float) Screen.width, (screenPoint1.y - screenPoint2.y) / (float) Screen.height);
      float num = this.fullSize * this.rect.height;
      if (this.rect != this.mCam.rect)
        this.mCam.rect = this.rect;
      if ((double) this.mCam.orthographicSize == (double) num)
        return;
      this.mCam.orthographicSize = num;
    }
    else
    {
      Vector3[] worldCorners = this.container.worldCorners;
      Vector3 screenPoint1 = this.sourceCamera.WorldToScreenPoint(worldCorners[1]);
      Vector3 screenPoint2 = this.sourceCamera.WorldToScreenPoint(worldCorners[3]);
      this.rect = new Rect(screenPoint1.x / (float) Screen.width, screenPoint2.y / (float) Screen.height, (screenPoint2.x - screenPoint1.x) / (float) Screen.width, (screenPoint1.y - screenPoint2.y) / (float) Screen.height);
      float num = this.fullSize * this.rect.height;
      if (this.rect != this.mCam.rect)
        this.mCam.rect = this.rect;
      if ((double) this.mCam.orthographicSize == (double) num)
        return;
      this.mCam.orthographicSize = num;
    }
  }
}
