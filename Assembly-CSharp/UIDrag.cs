﻿// Decompiled with JetBrains decompiler
// Type: UIDrag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UIDrag : MonoBehaviour
{
  private int fingerId = -1;
  private bool drag = true;

  private void OnEnable()
  {
    EasyTouch.On_TouchDown += new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchStart += new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_TouchUp += new EasyTouch.TouchUpHandler(this.On_TouchUp);
    EasyTouch.On_TouchStart2Fingers += new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    EasyTouch.On_TouchDown2Fingers += new EasyTouch.TouchDown2FingersHandler(this.On_TouchDown2Fingers);
    EasyTouch.On_TouchUp2Fingers += new EasyTouch.TouchUp2FingersHandler(this.On_TouchUp2Fingers);
  }

  private void OnDestroy()
  {
    EasyTouch.On_TouchDown -= new EasyTouch.TouchDownHandler(this.On_TouchDown);
    EasyTouch.On_TouchStart -= new EasyTouch.TouchStartHandler(this.On_TouchStart);
    EasyTouch.On_TouchUp -= new EasyTouch.TouchUpHandler(this.On_TouchUp);
    EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    EasyTouch.On_TouchDown2Fingers -= new EasyTouch.TouchDown2FingersHandler(this.On_TouchDown2Fingers);
    EasyTouch.On_TouchUp2Fingers -= new EasyTouch.TouchUp2FingersHandler(this.On_TouchUp2Fingers);
  }

  private void On_TouchStart(Gesture gesture)
  {
    if (!gesture.isOverGui || !this.drag || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform) || this.fingerId != -1)
      return;
    this.fingerId = gesture.fingerIndex;
    this.transform.SetAsLastSibling();
  }

  private void On_TouchDown(Gesture gesture)
  {
    if (this.fingerId != gesture.fingerIndex || !this.drag || !gesture.isOverGui || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform))
      return;
    this.transform.position += (Vector3) gesture.deltaPosition;
  }

  private void On_TouchUp(Gesture gesture)
  {
    if (this.fingerId != gesture.fingerIndex)
      return;
    this.fingerId = -1;
  }

  private void On_TouchStart2Fingers(Gesture gesture)
  {
    if (!gesture.isOverGui || !this.drag || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform) || this.fingerId != -1)
      return;
    this.transform.SetAsLastSibling();
  }

  private void On_TouchDown2Fingers(Gesture gesture)
  {
    if (!gesture.isOverGui || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform))
      return;
    if ((Object) gesture.pickedUIElement == (Object) this.gameObject || gesture.pickedUIElement.transform.IsChildOf(this.transform))
      this.transform.position += (Vector3) gesture.deltaPosition;
    this.drag = false;
  }

  private void On_TouchUp2Fingers(Gesture gesture)
  {
    if (!gesture.isOverGui || !((Object) gesture.pickedUIElement == (Object) this.gameObject) && !gesture.pickedUIElement.transform.IsChildOf(this.transform))
      return;
    this.drag = true;
  }
}
