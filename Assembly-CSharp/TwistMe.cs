﻿// Decompiled with JetBrains decompiler
// Type: TwistMe
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 78C09D85-3C0E-4A4D-870C-A6C291FD102F
// Assembly location: C:\Users\Pascal\Documents\Games\The Infinite Black\The Infinite Black\tib-windows_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TwistMe : MonoBehaviour
{
  private TextMesh textMesh;

  private void OnEnable()
  {
    EasyTouch.On_TouchStart2Fingers += new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    EasyTouch.On_Twist += new EasyTouch.TwistHandler(this.On_Twist);
    EasyTouch.On_TwistEnd += new EasyTouch.TwistEndHandler(this.On_TwistEnd);
    EasyTouch.On_Cancel2Fingers += new EasyTouch.Cancel2FingersHandler(this.On_Cancel2Fingers);
  }

  private void OnDisable()
  {
    this.UnsubscribeEvent();
  }

  private void OnDestroy()
  {
    this.UnsubscribeEvent();
  }

  private void UnsubscribeEvent()
  {
    EasyTouch.On_TouchStart2Fingers -= new EasyTouch.TouchStart2FingersHandler(this.On_TouchStart2Fingers);
    EasyTouch.On_Twist -= new EasyTouch.TwistHandler(this.On_Twist);
    EasyTouch.On_TwistEnd -= new EasyTouch.TwistEndHandler(this.On_TwistEnd);
    EasyTouch.On_Cancel2Fingers -= new EasyTouch.Cancel2FingersHandler(this.On_Cancel2Fingers);
  }

  private void Start()
  {
    this.textMesh = this.GetComponentInChildren<TextMesh>();
  }

  private void On_TouchStart2Fingers(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    EasyTouch.SetEnableTwist(true);
    EasyTouch.SetEnablePinch(false);
  }

  private void On_Twist(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    this.transform.Rotate(new Vector3(0.0f, 0.0f, gesture.twistAngle));
    this.textMesh.text = "Delta angle : " + gesture.twistAngle.ToString();
  }

  private void On_TwistEnd(Gesture gesture)
  {
    if (!((Object) gesture.pickedObject == (Object) this.gameObject))
      return;
    EasyTouch.SetEnablePinch(true);
    this.transform.rotation = Quaternion.identity;
    this.textMesh.text = "Twist me";
  }

  private void On_Cancel2Fingers(Gesture gesture)
  {
    EasyTouch.SetEnablePinch(true);
    this.transform.rotation = Quaternion.identity;
    this.textMesh.text = "Twist me";
  }
}
